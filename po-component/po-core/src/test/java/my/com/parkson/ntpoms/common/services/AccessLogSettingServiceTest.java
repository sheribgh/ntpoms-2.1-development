/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.test.context.junit4.SpringRunner;

import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.entities.AccessLogSetting;
import my.com.parkson.ntpoms.common.repositories.AccessLogSettingRepository;
import my.com.parkson.ntpoms.common.services.impl.AccessLogSettingServiceImpl;

/**
 * The Class AccessLogSettingServiceTest.
 */
@RunWith(SpringRunner.class)
public class AccessLogSettingServiceTest {
	
	/** The exception. */
	@Rule
	public final ExpectedException exception = ExpectedException.none();

	/** The log. */
	@MockBean
	private AccessLogService log;	

	/** The access log setting repository. */
	@MockBean
	private AccessLogSettingRepository accessLogSettingRepository;

	/** The test entity manager. */
	@MockBean
	TestEntityManager testEntityManager;

	/** The access log setting service. */
	private AccessLogSettingService accessLogSettingService;

	/** The setting. */
	private int setting = 2;
	
	/** The id. */
	private int id = 1;

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		AccessLogSetting accessLogSetting = new AccessLogSetting();
		accessLogSetting.setId(id);
		accessLogSetting.setSetting(setting);
		testEntityManager.persist(accessLogSetting);
		List<AccessLogSetting> accessLogSettingList = Arrays.asList(accessLogSetting);
		when(accessLogSettingRepository.findAllByOrderByIdAsc()).thenReturn(accessLogSettingList);
		accessLogSettingService = new AccessLogSettingServiceImpl();
	}

	/**
	 * Test get all staging log.
	 */
	@Test(expected=POException.class)
	public void testGetAllStagingLog() {
		try {
			when(accessLogSettingService.getAllStagingLog()).thenReturn(null);
			List<AccessLogSetting> accessLogSettingList = accessLogSettingService.getAllStagingLog();
			assertTrue(accessLogSettingList.size() == 1);
			exception.expectMessage("Failed JUNIT_FIND_All");
			doNothing().when(exception).expectMessage("Failed JUNIT_FIND_All");
		} catch (Exception ex) {
	        assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}

	/**
	 * Test get last staging log.
	 */
	@Test(expected=POException.class)
	public void testGetLastStagingLog() {
		try {
			when(accessLogSettingService.getLastStagingLog()).thenReturn(null);
			AccessLogSetting accessLogSetting = accessLogSettingService.getLastStagingLog();
			assertNotNull(accessLogSetting);
			exception.expectMessage("Failed JUNIT_FIND_All");
			doNothing().when(exception).expectMessage("Failed JUNIT_FIND_All");
		} catch (Exception ex) {
	         assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}

	/**
	 * Test create staging log.
	 */
	@Test
	public void testCreateStagingLog() {
		AccessLogSetting accessLogSetting = new AccessLogSetting();
		accessLogSetting.setId(id);
		accessLogSetting.setSetting(setting);
		doNothing().when(accessLogSettingService).createStagingLog(accessLogSetting);
		assertNotNull(accessLogSetting);
	}
}