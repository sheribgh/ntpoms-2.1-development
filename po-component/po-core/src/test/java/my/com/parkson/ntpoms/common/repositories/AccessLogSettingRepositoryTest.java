/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.repositories;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.test.context.junit4.SpringRunner;

import my.com.parkson.ntpoms.common.entities.AccessLog;
import my.com.parkson.ntpoms.common.entities.AccessLogSetting;

/**
 * The Class AccessLogSettingRepositoryTest.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class AccessLogSettingRepositoryTest {
	
	/** The test entity manager. */
	@Autowired
	TestEntityManager testEntityManager;
	
	/** The access log setting repository. */
	@Autowired
	AccessLogSettingRepository accessLogSettingRepository;

	/** The setting. */
	private int setting = 2;
	
	/** The id. */
	private int id = 1;

	/**
	 * Test find all by order by id desc.
	 */
	@Test
	public void testFindAllByOrderByIdDesc() {
		AccessLogSetting accessLogSetting = new AccessLogSetting();
		accessLogSetting.setId(id);
		accessLogSetting.setSetting(setting);
		testEntityManager.persist(accessLogSetting);
		
		List<AccessLogSetting> accessLogSettingList = accessLogSettingRepository.findAllByOrderByIdAsc();
		assertEquals(1, accessLogSettingList.size());
	}
}