/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.test.context.junit4.SpringRunner;

import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.entities.AccessLog;
import my.com.parkson.ntpoms.common.repositories.AccessLogRepository;
import my.com.parkson.ntpoms.common.services.impl.AccessLogServiceImpl;

/**
 * The Class AccessLogServiceTest.
 */
@RunWith(SpringRunner.class)
public class AccessLogServiceTest {

	/** The access log repository. */
	@MockBean
	private AccessLogRepository accessLogRepository;

	/** The test entity manager. */
	@MockBean
	private TestEntityManager testEntityManager;

	/** The messages. */
	@MockBean
	private MessageService messages;

	/** The access log service. */
	private AccessLogService accessLogService;

	/** The message. */
	private String message = "JUNIT Debug";
	
	/** The level. */
	private String level = "DEBUG";
	
	/** The file name. */
	private String fileName = "JUNIT fileName";

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {		
		AccessLog accessLog = new AccessLog();
		accessLog.setLevel(level);
		accessLog.setMessage(message);
		testEntityManager.persist(accessLog);
		List<AccessLog> accessLogList = Arrays.asList(accessLog);	
		when(accessLogRepository.findAllByOrderByIdDesc()).thenReturn(accessLogList);
		accessLogService = new AccessLogServiceImpl();
	}
	
	/**
	 * Test get all access log.
	 */
	@Test(expected=POException.class)
	public void testGetAllAccessLog() {
		try {
			when(accessLogRepository.findAll()).thenReturn(null);
			List<AccessLog> accessLogList = accessLogService.getAllAccessLog();
			assertNull(accessLogList);
		} catch (Exception ex) {
	        assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}

	/**
	 * Test create access log.
	 */
	@Test
	public void testCreateAccessLog() {
		AccessLog accessLog = new AccessLog();
		accessLog.setLevel(level);
		accessLog.setMessage(message);
		accessLog.setClassFileName(fileName);
		doNothing().when(accessLogService).createAccessLog(accessLog);
		assertNotNull(accessLog);
	}
}