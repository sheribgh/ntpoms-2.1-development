/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.entities.Permission;
import my.com.parkson.ntpoms.common.entities.Role;
import my.com.parkson.ntpoms.common.entities.User;
import my.com.parkson.ntpoms.common.repositories.PermissionRepository;
import my.com.parkson.ntpoms.common.repositories.RoleRepository;
import my.com.parkson.ntpoms.common.repositories.UsersRepository;
import my.com.parkson.ntpoms.common.services.impl.SecurityServiceImpl;

/**
 * The Class SecurityServiceTest.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class SecurityServiceTest {

	/** The Constant email. */
	private static final String email = "JUNIT_EMAIL@EMAIL.COM";
	
	/** The Constant name. */
	private static final String name = "JUNIT_NAME";
	
	/** The Constant password. */
	private static final String password = "JUNIT_PASSWORD";
	
	/** The Constant description. */
	private static final String description = "JUNIT_DESCRIPTION";

	/** The test entity manager. */
	@Autowired
	TestEntityManager testEntityManager;
	
	/** The user repository. */
	@Autowired
	private UsersRepository userRepository;
	
	/** The permission repository. */
	@Autowired
	private PermissionRepository permissionRepository;
	
	/** The role repository. */
	@Autowired
	private RoleRepository roleRepository;

	/** The security service. */
	SecurityService securityService;
	
	/** The user. */
	private User user = new User();

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		user.setUsername(name);
		user.setPassword(password);
		user.setRoles(new ArrayList<Role>());
		user.setPasswordResetToken("token");
		testEntityManager.persist(user);
		securityService = new SecurityServiceImpl();
	}

	/**
	 * Test reset password.
	 */
	@Test(expected = POException.class)
	public void testResetPassword() {
		String uuid = securityService.resetPassword(email);
		assertNotNull(uuid);
		securityService.resetPassword(name);
	}

	/**
	 * Test update password in valid password.
	 */
	@Test(expected = POException.class)
	public void testUpdatePasswordInValidPassword() {
		securityService.updatePassword(email, "token1s", password);
	}

	/**
	 * Test verify password reset token.
	 */
	@Test(expected = POException.class)
	public void testVerifyPasswordResetToken() {
		boolean verified = securityService.verifyPasswordResetToken(email, "token");
		assertTrue(verified);
		boolean verified1 = securityService.verifyPasswordResetToken(email, "token1");
		assertFalse(verified1);
		securityService.verifyPasswordResetToken("ABC@ABC.COM", "token");
	}

	/**
	 * Test get all permissions.
	 */
	@Test
	public void testGetAllPermissions() {
		List<Permission> perList = securityService.getAllPermissions();
		assertTrue(perList.isEmpty());
	}

	/**
	 * Test get all roles.
	 */
	@Test
	public void testGetAllRoles() {
		List<Role> rolesList = securityService.getAllRoles();
		assertTrue(rolesList.isEmpty());
	}

	/**
	 * Test get all users.
	 */
	@Test
	public void testGetAllUsers() {
		List<User> userList = securityService.getAllUsers();
		assertFalse(userList.isEmpty());
	}

	/**
	 * Test create user.
	 */
	@Test(expected = POException.class)
	public void testCreateUser() {
		User user = new User();
		user.setUsername("Name");
		user.setPassword("Passw");
		user.setPasswordResetToken("resetToken");

		Role role = new Role();
		role.setName(name);
		List<Permission> permissions = new ArrayList<>();
		role.setPermissions(permissions);
		Role dbRole = securityService.createRole(role);
		assertThat(dbRole).isNotNull();
		dbRole.setDescription(description);
		securityService.updateRole(dbRole);
		dbRole = securityService.getRoleById(dbRole.getId());
		assertEquals(name, dbRole.getName());
		assertEquals(description, dbRole.getDescription());
		List<Role> roles = new ArrayList<>();
		roles.add(dbRole);
		user.setRoles(roles);

		User dbUser = securityService.createUser(user);
		assertThat(dbUser.getUsername()).isNotNull();
		securityService.updatePassword("JUNIT", "resetToken", password);

		User dbUser1 = securityService.getUserByUsername(user.getUsername());
		assertEquals(dbUser1.getPassword(), password);

		dbUser1.setUsername("JUNIT1");
		User dbUser2 = securityService.updateUser(dbUser1);
		assertEquals("JUNIT1", dbUser2.getUsername());
		securityService.createUser(user);
	}

	/**
	 * Test create role.
	 */
	@Test(expected = POException.class)
	public void testCreateRole() {
		Role role = new Role();
		role.setName(name);
		List<Permission> permissions = new ArrayList<>();
		role.setPermissions(permissions);
		Role dbrole = securityService.createRole(role);
		assertThat(dbrole).isNotNull();
		assertThat(dbrole.getName()).isEqualTo(name);
		securityService.createRole(role);
	}

	/**
	 * Test update user.
	 */
	@Test(expected = POException.class)
	public void testUpdateUser() {
		User newUser = new User();
		newUser.setUsername("JUNIT11");
		securityService.updateUser(newUser);
	}

	/**
	 * Test update role.
	 */
	@Test(expected = POException.class)
	public void testUpdateRole() {
		Role newRole = new Role();
		newRole.setId(50);
		securityService.updateRole(newRole);
	}

	/**
	 * Test update password.
	 */
	@Test(expected = POException.class)
	public void testUpdatePassword() {
		securityService.updatePassword("JUNIT", null, password);
	}

}
