/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
/* Name : audit_Service                                      */
/* Usage: Code for the audit service                        */
/* Description: This code creates an interface for audit that will */
/*              used for connecting to data. This code*/
/*              interacts with the table <audit>.        */
/*************************************************************/

package my.com.parkson.ntpoms.common.repositories;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.test.context.junit4.SpringRunner;

import my.com.parkson.ntpoms.common.entities.AccessLogSetting;
import my.com.parkson.ntpoms.common.entities.Audit;

/**
 * The Class AuditRepositoryTest.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class AuditRepositoryTest {
	
	/** The test entity manager. */
	@Autowired
	TestEntityManager testEntityManager;
	
	/** The audit repository. */
	@Autowired
	AuditRepository auditRepository;

	/** The username. */
	private String username = "JUNIT";
	
	/** The message. */
	private String message = "JUNIT Message";

	/**
	 * Test find all by order by id desc.
	 */
	@Test
	public void testFindAllByOrderByIdDesc() {
		Audit audit = new Audit();
		audit.setUsername(username);
		audit.setMessage(message);
		testEntityManager.persist(audit);
		
		List<Audit> auditList = auditRepository.findAllByOrderByCreatedOnDesc();
		assertEquals(1, auditList.size());
	}
}
