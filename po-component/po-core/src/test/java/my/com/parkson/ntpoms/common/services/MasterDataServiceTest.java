/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.sql.SQLException;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.test.context.junit4.SpringRunner;

import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.entities.MasterData;
import my.com.parkson.ntpoms.common.repositories.MasterDataRepository;
import my.com.parkson.ntpoms.common.services.impl.MasterDataServiceImpl;

/**
 * The Class MasterDataServiceTest.
 */
@RunWith(SpringRunner.class)
public class MasterDataServiceTest {

	/** The exception. */
	@Rule
	public final ExpectedException exception = ExpectedException.none();

	/** The test entity manager. */
	@MockBean
	private TestEntityManager testEntityManager;

	/** The master data repository. */
	@MockBean
	private MasterDataRepository masterDataRepository;

	/** The masterdata service. */
	private MasterDataService masterdataService;

	/** The code desc. */
	private String codeDesc = "JUNIT Desc";
	
	/** The code type. */
	private String codeType = "JUNIT Type";
	
	/** The code value. */
	private String codeValue = "JUNIT Value";
	
	/** The id. */
	private int id = 2;


	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		MasterData masterData = new MasterData();
		masterData.setCodeDesc(codeDesc);
		masterData.setCodeType(codeType);
		masterData.setCodeValue(codeValue);
		testEntityManager.persist(masterData);
		when(masterDataRepository.findById(id)).thenReturn(masterData);
		masterdataService = new MasterDataServiceImpl();
	}

	/**
	 * Test get all master data.
	 */
	@Test(expected=POException.class)
	public void testGetAllMasterData() {
		try {
			List<MasterData> masterDataList = null;
			when(masterdataService.getAllMasterData()).thenReturn(masterDataList);
			assertNull(masterDataList);
			masterDataList = masterdataService.getAllMasterData();
			assertNotNull(masterDataList);
			exception.expectMessage("Failed JUNIT_FIND_All");
		} catch (Exception ex) {
	        assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}

	/**
	 * Test get all active master data.
	 */
	@Test(expected=POException.class)
	public void testGetAllActiveMasterData() {
		try {
			List<MasterData> masterDataList = null;
			when(masterdataService.getAllActiveMasterData()).thenReturn(masterDataList);
			assertNull(masterDataList);
			masterDataList = masterdataService.getAllActiveMasterData();
			assertNotNull(masterDataList);
			exception.expectMessage("Failed JUNIT_FIND_All");
		} catch (Exception ex) {
	        assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}

	/**
	 * Test get master data by id.
	 */
	@Test(expected=POException.class)
	public void testGetMasterDataById() {
		try {
			MasterData masterData = null;
			when(masterdataService.getMasterDataById(id)).thenReturn(masterData);
			assertNull(masterData);
			masterData = masterdataService.getMasterDataById(id);
			assertNotNull(masterData);
			exception.expectMessage("Failed JUNIT_FIND_All");
		} catch (Exception ex) {
	        assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}

	/**
	 * Test create master data.
	 */
	@Test
	public void testCreateMasterData() {
		MasterData masterData = new MasterData();
		masterData.setCodeDesc(codeDesc);
		masterData.setCodeType(codeType);
		masterData.setCodeValue(codeValue);
		testEntityManager.persist(masterData);
		doNothing().when(masterdataService).createMasterData(masterData);
		assertNotNull(masterData);
	}
}