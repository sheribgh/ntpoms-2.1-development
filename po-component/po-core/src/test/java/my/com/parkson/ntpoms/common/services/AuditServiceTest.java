/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;
import my.com.parkson.ntpoms.POException;

import my.com.parkson.ntpoms.common.entities.Audit;
import  my.com.parkson.ntpoms.common.repositories.AuditRepository;
import my.com.parkson.ntpoms.common.services.impl.AuditServiceImpl;

/**
 * The Class AuditServiceTest.
 */
@RunWith(SpringRunner.class)
public class AuditServiceTest {

	/** The exception. */
	@Rule
	public final ExpectedException exception = ExpectedException.none();

	/** The audit repository. */
	@MockBean
	private AuditRepository auditRepository;

	/** The test entity manager. */
	@MockBean
	TestEntityManager testEntityManager;

	/** The audit service. */
	private AuditService auditService;

	/** The messages. */
	@MockBean
	private MessageService messages;

	/** The username. */
	private String username = "JUNIT_USERNAME";
	
	/** The message. */
	private String message = "JUNIT Message";

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		Audit audit = new Audit();
		audit.setUsername(username);
		audit.setMessage(message);
		testEntityManager.persist(audit);
		List<Audit> auditList = Arrays.asList(audit);
		when(auditRepository.findAllByOrderByCreatedOnDesc()).thenReturn(auditList);
		auditService = new AuditServiceImpl();
	}

	/**
	 * Test put audit.
	 */
	@Test
	public void testPutAudit() {
		Audit audit = new Audit();
		audit.setUsername(username);
		audit.setMessage(message);
		testEntityManager.persist(audit);
		doNothing().when(auditService).putAudit(new Date(), "audit1", "audit2", "audit3");
		assertNotNull(audit);
	}

	/**
	 * Test get all audit.
	 */
	@Test(expected=POException.class)
	public void testGetAllAudit() {
		try {
			when(auditRepository.findAllByOrderByCreatedOnDesc()).thenReturn(null);
			List<Audit> auditList = auditService.getAllAudit();
			exception.expectMessage("Failed JUNIT_FIND_All");
			assertNotNull(auditList);
		} catch (Exception ex) {
	        assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}

	/**
	 * Test get records by search code.
	 */
	@Test
	public void testGetRecordsBySearchCode() {
		Page<Audit> audit = auditService.getRecordsBySearchCode(username, new PageRequest(1, 5, Sort.Direction.DESC, "id"));
		assertTrue(audit.isFirst());
	}
}