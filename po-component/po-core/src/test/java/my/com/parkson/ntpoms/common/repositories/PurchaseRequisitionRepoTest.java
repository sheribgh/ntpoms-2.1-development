/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.repositories;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import my.com.parkson.ntpoms.common.entities.PurchaseRequisition;

/**
 * The Class PurchaseRequisitionRepoTest.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class PurchaseRequisitionRepoTest {

	/** The test entity manager. */
	@Autowired
	TestEntityManager testEntityManager;

	/** The purchase requisition. */
	@Autowired
	PurchaseRequisitionRepository purchaseRequisition;

	/** The type code. */
	private String typeCode = "JUNIT typeCode";
	
	/** The desc. */
	private String desc = "JUNIT Description";


	/**
	 * Test find by name.
	 */
	@Test
	public void testFindByName() {
		PurchaseRequisition PurchaseRequisition = new PurchaseRequisition();
	
		PurchaseRequisition.setTypeCode(typeCode);
		PurchaseRequisition.setDesc(desc);
		testEntityManager.persist(PurchaseRequisition);

		PurchaseRequisition = purchaseRequisition.findByTypeCode(typeCode);
		assertNotNull(PurchaseRequisition.getTypeCode());
		assertTrue(PurchaseRequisition.getDesc().equals(desc));
		List<PurchaseRequisition> PurchaseRequisitionList = purchaseRequisition.findAllByOrderByLastModifiedOnDesc();
		assertEquals(1, PurchaseRequisitionList.size());
		PurchaseRequisitionList = purchaseRequisition.findAllByIsActiveOrderByLastModifiedOnDesc(true);
		assertEquals(1, PurchaseRequisitionList.size());
		}
}
