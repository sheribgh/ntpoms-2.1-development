/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.sql.SQLException;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.test.context.junit4.SpringRunner;

import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.entities.SystemParameter;
import my.com.parkson.ntpoms.common.repositories.SystemParameterRepository;
import my.com.parkson.ntpoms.common.services.impl.SystemParameterServiceImpl;

/**
 * The Class SystemParameterServiceTest.
 */
@RunWith(SpringRunner.class)
public class SystemParameterServiceTest {

	/** The exception. */
	@Rule
	public final ExpectedException exception = ExpectedException.none();

	/** The test entity manager. */
	@MockBean
	private TestEntityManager testEntityManager;

	/** The system parameter repository. */
	@MockBean
	private SystemParameterRepository systemParameterRepository;

	/** The system parameter service. */
	private SystemParameterService systemParameterService;

	/** The code. */
	private int code = 1;
	
	/** The prop name. */
	private String propName = "JUNIT propName";
	
	/** The prop value. */
	private String propValue = "JUNIT propValue";	

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		SystemParameter systemParameter = new SystemParameter();
		systemParameter.setPropName(propName);
		systemParameter.setPropValue(propValue);
		testEntityManager.persist(systemParameter);
		when(systemParameterRepository.findByCode(systemParameter.getCode())).thenReturn(systemParameter);
		systemParameterService = new SystemParameterServiceImpl();
	}

	/**
	 * Test get all system parameter.
	 */
	@Test(expected=POException.class)
	public void testGetAllSystemParameter() {
		try {
			List<SystemParameter> systemParameterList = null;
			when(systemParameterService.getAllSystemParameter()).thenReturn(systemParameterList);
			assertNull(systemParameterList);
			systemParameterList = systemParameterService.getAllSystemParameter();
			assertNotNull(systemParameterList);
			exception.expectMessage("Failed JUNIT_FIND_All");
		} catch (Exception ex) {
	       assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}

	/**
	 * Test get all active system parameter.
	 */
	@Test(expected=POException.class)
	public void testGetAllActiveSystemParameter() {
		try {
			List<SystemParameter> systemParameterList = null;
			when(systemParameterService.getAllActiveSystemParameter()).thenReturn(systemParameterList);
			assertNull(systemParameterList);
			systemParameterList = systemParameterService.getAllActiveSystemParameter();
			assertNotNull(systemParameterList);
			exception.expectMessage("Failed JUNIT_FIND_All");
		} catch (Exception ex) {
	       assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}

	/**
	 * Test get system parameter by code.
	 */
	@Test(expected=POException.class)
	public void testGetSystemParameterByCode() {
		try {
			SystemParameter systemParameter = null;
			when(systemParameterService.getSystemParameterByCode(code)).thenReturn(systemParameter);
			assertNull(systemParameter);
			systemParameter = systemParameterService.getSystemParameterByCode(code);
			assertNotNull(systemParameter);
			exception.expectMessage("Failed JUNIT_FIND_Record");
		} catch (Exception ex) {
	       assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}

	/**
	 * Test create system parameter.
	 */
	@Test
	public void testCreateSystemParameter() {
		SystemParameter systemParameter = new SystemParameter();
		systemParameter.setPropName(propName);
		systemParameter.setPropValue(propValue);
		testEntityManager.persist(systemParameter);
		doNothing().when(systemParameterService).createSystemParameter(systemParameter);
		assertNotNull(systemParameter);
	}

	/**
	 * Test get system parameter by prop name.
	 */
	@Test(expected=POException.class)
	public void testGetSystemParameterByPropName() {
		try {
			SystemParameter systemParameter = null;
			when(systemParameterService.getSystemParameterByPropName(propName)).thenReturn(systemParameter);
			assertNull(systemParameter);
			systemParameter = systemParameterService.getSystemParameterByPropName(propName);
			assertNotNull(systemParameter);
			exception.expectMessage("Failed JUNIT_FIND_Record");
		} catch (Exception ex) {
	        assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}
}