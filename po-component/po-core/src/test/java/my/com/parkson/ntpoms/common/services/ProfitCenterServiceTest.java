/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.sql.SQLException;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.test.context.junit4.SpringRunner;

import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.entities.ProfitCenter;
import my.com.parkson.ntpoms.common.repositories.CompanyRepository;
import my.com.parkson.ntpoms.common.repositories.ProfitCenterRepository;
import my.com.parkson.ntpoms.common.services.impl.ProfitCenterServiceImpl;

/**
 * The Class ProfitCenterServiceTest.
 */
@RunWith(SpringRunner.class)
public class ProfitCenterServiceTest {

	/** The exception. */
	@Rule
	public final ExpectedException exception = ExpectedException.none();

	/** The test entity manager. */
	@MockBean
	private TestEntityManager testEntityManager;

	/** The company repository. */
	@MockBean
	private CompanyRepository companyRepository;

	/** The profit center repository. */
	@MockBean
	private ProfitCenterRepository profitCenterRepository;

	/** The profit center service. */
	private ProfitCenterService profitCenterService;

	/** The code. */
	private String code = "JUNIT";
	
	/** The name. */
	private String name = "JUNIT Name";
	
	/** The company code. */
	private String companyCode = "JUNIT CompanyCode";

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		ProfitCenter ProfitCenter = new ProfitCenter();
		ProfitCenter.setCode(code);
		ProfitCenter.setName(name);
		testEntityManager.persist(ProfitCenter);
		when(profitCenterRepository.findByCode(code)).thenReturn(ProfitCenter);
		profitCenterService = new ProfitCenterServiceImpl();
	}

	/**
	 * Test get all profit center.
	 */
	@Test(expected=POException.class)
	public void testGetAllProfitCenter() {
		try {
			List<ProfitCenter> profitCenterList = null;
			when(profitCenterService.getAllProfitCenter()).thenReturn(profitCenterList);
			assertNull(profitCenterList);
			profitCenterList = profitCenterService.getAllProfitCenter();
			assertNotNull(profitCenterList);
			exception.expectMessage("Failed JUNIT_FIND_All");
		} catch (Exception ex) {
	       assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}

	/**
	 * Test get all active profit center.
	 */
	@Test(expected=POException.class)
	public void testGetAllActiveProfitCenter() {
		try {
			List<ProfitCenter> profitCenterList = null;
			when(profitCenterService.getAllActiveProfitCenter()).thenReturn(profitCenterList);
			assertNull(profitCenterList);
			profitCenterList = profitCenterService.getAllActiveProfitCenter();
			assertNotNull(profitCenterList);
			exception.expectMessage("Failed JUNIT_FIND_All");
		} catch (Exception ex) {
	        assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}

	/**
	 * Test get profit center by code.
	 */
	@Test(expected=POException.class)
	public void testGetProfitCenterByCode() {
		try {
			ProfitCenter profitCenter = null;
			when(profitCenterService.getProfitCenterByCode(code)).thenReturn(profitCenter);
			assertNull(profitCenter);
			profitCenter = profitCenterService.getProfitCenterByCode(code);
			assertNotNull(profitCenter);
			exception.expectMessage("Failed JUNIT_FIND_Record");
		} catch (Exception ex) {
	       assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}

	/**
	 * Test create profit center.
	 */
	@Test
	public void testCreateProfitCenter() {
		ProfitCenter profitCenter = new ProfitCenter();
		profitCenter.setCode(code);
		profitCenter.setName(name);
		doNothing().when(profitCenterService).createProfitCenter(profitCenter);
		assertNotNull(profitCenter);
	}

	/**
	 * Test get all profit center by company code.
	 */
	@Test(expected=POException.class)
	public void testGetAllProfitCenterByCompanyCode() {
		try {
			List<ProfitCenter> profitCenterList = null;
			when(profitCenterService.getAllProfitCenterByCompanyCode(companyCode, true)).thenReturn(profitCenterList);
			assertNull(profitCenterList);
			profitCenterList = profitCenterService.getAllProfitCenterByCompanyCode(companyCode, true);
			assertNotNull(profitCenterList);
			exception.expectMessage("Failed JUNIT_FIND_Record");
		} catch (Exception ex) {
	        assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}
}