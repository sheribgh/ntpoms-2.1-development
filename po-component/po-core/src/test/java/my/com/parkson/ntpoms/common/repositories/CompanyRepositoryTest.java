/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.repositories;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import my.com.parkson.ntpoms.common.entities.Company;

/**
 * The Class CompanyRepositoryTest.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class CompanyRepositoryTest {

	/** The test entity manager. */
	@Autowired
	TestEntityManager testEntityManager;

	/** The company repository. */
	@Autowired
	CompanyRepository companyRepository;

	/** The code HRIS. */
	private String codeHRIS = "JUNIT HRIS";
	
	/** The code. */
	private String code = "JUNIT";
	
	/** The name. */
	private String name = "JUNIT Name";

	/**
	 * Test find by name.
	 */
	@Test
	public void testFindByName() {
		Company company = new Company();
		company.setCode(code);
		company.setCodeHRIS(codeHRIS);
		company.setName(name);
		testEntityManager.persist(company);

		company = companyRepository.findByCode(code);
		assertNotNull(company.getCode());
		assertTrue(company.getName().equals(name));
		company = companyRepository.findByCodeHRIS(codeHRIS);
		assertTrue(company.getCodeHRIS().equals(codeHRIS));

		List<Company> companyList = companyRepository.findAllByOrderByLastModifiedOnDesc();
		assertEquals(1, companyList.size());
		List<Company> productList = companyRepository.findAllByIsActiveOrderByLastModifiedOnDesc(true);
		assertEquals(1, productList.size());
	}	
}