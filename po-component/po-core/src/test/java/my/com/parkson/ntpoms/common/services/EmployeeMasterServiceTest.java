/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.sql.SQLException;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.test.context.junit4.SpringRunner;

import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.entities.Employee;
import my.com.parkson.ntpoms.common.entities.EmployeeMaster;
import my.com.parkson.ntpoms.common.repositories.EmployeeMasterRepository;
import my.com.parkson.ntpoms.common.services.impl.EmployeeMasterServiceImpl;

/**
 * The Class EmployeeMasterServiceTest.
 */
@RunWith(SpringRunner.class)
public class EmployeeMasterServiceTest {

	/** The exception. */
	@Rule
	public final ExpectedException exception = ExpectedException.none();

	/** The test entity manager. */
	@MockBean
	private TestEntityManager testEntityManager;

	/** The employee master repository. */
	@MockBean
	private EmployeeMasterRepository employeeMasterRepository;

	/** The messages. */
	@MockBean
	private MessageService messages;

	/** The employee master service. */
	private EmployeeMasterService employeeMasterService;

	/** The name. */
	private String name = "JUNIT Name";
	
	/** The new ic no. */
	private String newIcNo = "JK01111111";
	
	/** The gender. */
	private String gender = "Male";
	
	/** The ps id. */
	private String psId = "PO011111";
	
	/** The code. */
	private String code = "PC011111";


	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		EmployeeMaster employeeMaster = new EmployeeMaster();
		employeeMaster.setName(name);
		employeeMaster.setNewIcNo(newIcNo);
		employeeMaster.setGender(gender);
		testEntityManager.persist(employeeMaster);
		when(employeeMasterRepository.findByPsId(psId)).thenReturn(employeeMaster);
		employeeMasterService = new EmployeeMasterServiceImpl();
	}

	/**
	 * Test get all employee.
	 */
	@Test(expected=POException.class)
	public void testGetAllEmployee() {
		try {
			List<Employee> employeeList = null;
			when(employeeMasterService.getAllEmployee()).thenReturn(employeeList);
			assertNull(employeeList);
			employeeList = employeeMasterService.getAllEmployee();
			assertNotNull(employeeList);
			exception.expectMessage("Failed JUNIT_FIND_All");
		} catch (Exception ex) {
	        assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}

	/**
	 * Test get all active employee master.
	 */
	@Test(expected=POException.class)
	public void testGetAllActiveEmployeeMaster() {
		try {
			List<EmployeeMaster> employeeMasterList = null;
			when(employeeMasterService.getAllActiveEmployeeMaster()).thenReturn(employeeMasterList);
			assertNull(employeeMasterList);
			employeeMasterList = employeeMasterService.getAllActiveEmployeeMaster();
			exception.expectMessage("Failed JUNIT_FIND_All");
			assertNotNull(employeeMasterList);
		} catch (Exception ex) {
	        assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}

	/**
	 * Test get employee master by ps id.
	 */
	@Test(expected=POException.class)
	public void testGetEmployeeMasterByPsId() {
		try {
			EmployeeMaster employeeMaster = null;
			when(employeeMasterService.getEmployeeMasterByPsId(psId)).thenReturn(employeeMaster);
			assertNull(employeeMaster);
			employeeMaster = employeeMasterService.getEmployeeMasterByPsId(psId);
			assertNotNull(employeeMaster);
			exception.expectMessage("Failed JUNIT_FIND_Record");
		} catch (Exception ex) {
			assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}
	
	/**
	 * Test get all active employee master by profit center code.
	 */
	@Test(expected=POException.class)
	public void testGetAllActiveEmployeeMasterByProfitCenterCode() {
		try {
			List<EmployeeMaster> employeeMasterList = null;
			when(employeeMasterService.getAllActiveEmployeeMasterByProfitCenterCode(code)).thenReturn(employeeMasterList);
			assertNull(employeeMasterList);
			employeeMasterList = employeeMasterService.getAllActiveEmployeeMasterByProfitCenterCode(code);
			exception.expectMessage("Failed JUNIT_FIND_Record");
			assertNotNull(employeeMasterList);
		} catch (Exception ex) {
	        assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}

	/**
	 * Test get all active profit center ids.
	 */
	@Test(expected=POException.class)
	public void testGetAllActiveProfitCenterIds() {
		try {
			List<String> profitCenterIdsList = null;
			when(employeeMasterService.getAllActiveProfitCenterIds()).thenReturn(profitCenterIdsList);
			assertNull(profitCenterIdsList);
			profitCenterIdsList = employeeMasterService.getAllActiveProfitCenterIds();
			assertNotNull(profitCenterIdsList);
			exception.expectMessage("Failed JUNIT_FIND_Record");
		} catch (Exception ex) {
	         assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}

	/**
	 * Test create employee master.
	 */
	@Test
	public void testCreateEmployeeMaster() {
		EmployeeMaster employeeMaster = new EmployeeMaster();
		employeeMaster.setName(name);
		employeeMaster.setNewIcNo(newIcNo);
		employeeMaster.setGender(gender);
		doNothing().when(employeeMasterService).createEmployeeMaster(employeeMaster);
		assertNotNull(employeeMaster);
	}
}