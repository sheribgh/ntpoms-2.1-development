/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.sql.SQLException;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.test.context.junit4.SpringRunner;

import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.entities.Job;
import my.com.parkson.ntpoms.common.repositories.JobRepository;
import my.com.parkson.ntpoms.common.services.impl.JobServiceImpl;

/**
 * The Class JobServiceTest.
 */
@RunWith(SpringRunner.class)
public class JobServiceTest {

	/** The exception. */
	@Rule
	public final ExpectedException exception = ExpectedException.none();

	/** The test entity manager. */
	@MockBean
	private TestEntityManager testEntityManager;

	/** The job repository. */
	@MockBean
	private JobRepository jobRepository;

	/** The job service. */
	private JobService jobService;

	/** The code. */
	private String code = "JUNIT";
	
	/** The name. */
	private String name = "JUNIT name";

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		Job job = new Job();
		job.setCode(code);
		job.setName(name);
		testEntityManager.persist(job);
		when(jobRepository.findByCode(code)).thenReturn(job);
		jobService = new JobServiceImpl();
	}

	/**
	 * Test get all job.
	 */
	@Test(expected=POException.class)
	public void testGetAllJob() {
		try {
			List<Job> jobList = null;
			when(jobService.getAllJob()).thenReturn(jobList);
			assertNull(jobList);
			jobList = jobService.getAllJob();
			assertNotNull(jobList);
			exception.expectMessage("Failed JUNIT_FIND_All");
		} catch (Exception ex) {
	       assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}

	/**
	 * Test get all active job.
	 */
	@Test(expected=POException.class)
	public void testGetAllActiveJob() {
		try {
			List<Job> jobList = null;
			when(jobService.getAllActiveJob()).thenReturn(jobList);
			assertNull(jobList);
			jobList = jobService.getAllActiveJob();
			assertNotNull(jobList);
			exception.expectMessage("Failed JUNIT_FIND_All");
		} catch (Exception ex) {
	        assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}

	/**
	 * Test get job by code.
	 */
	@Test(expected=POException.class)
	public void testGetJobByCode() {
		try {
			Job job = null;
			when(jobService.getJobByCode(code)).thenReturn(job);
			assertNull(job);
			job = jobService.getJobByCode(code);
			assertNotNull(job);
			exception.expectMessage("Failed JUNIT_FIND_All");
		} catch (Exception ex) {
	       assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}

	/**
	 * Test create job.
	 */
	@Test
	public void testCreateJob() {
		Job job = new Job();
		job.setCode(code);
		job.setName(name);
		doNothing().when(jobService).createJob(job);
		assertNotNull(job);
	}
}