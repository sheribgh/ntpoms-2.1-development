/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.sql.SQLException;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.test.context.junit4.SpringRunner;

import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.entities.Scheduler;
import my.com.parkson.ntpoms.common.repositories.SchedulerRepository;
import my.com.parkson.ntpoms.common.services.impl.SchedulerServiceImpl;

/**
 * The Class SchedulerServiceTest.
 */
@RunWith(SpringRunner.class)
public class SchedulerServiceTest {

	/** The exception. */
	@Rule
	public final ExpectedException exception = ExpectedException.none();

	/** The test entity manager. */
	@MockBean
	private TestEntityManager testEntityManager;

	/** The scheduler repository. */
	@MockBean
	private SchedulerRepository schedulerRepository;

	/** The scheduler service. */
	private SchedulerService schedulerService;

	/** The task. */
	private String task = "JUNIT task";
	
	/** The status. */
	private String status = "JUNIT status";


	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		Scheduler scheduler = new Scheduler();
		scheduler.setTask(task);
		scheduler.setStatus(status);
		testEntityManager.persist(scheduler);
		when(schedulerRepository.findById(scheduler.getId())).thenReturn(scheduler);
		schedulerService = new SchedulerServiceImpl();
	}

	/**
	 * Test create scheduler.
	 */
	@Test
	public void testCreateScheduler() {
		Scheduler scheduler = new Scheduler();
		scheduler.setTask(task);
		scheduler.setStatus(status);
		doNothing().when(schedulerService).createScheduler(scheduler);
		assertNotNull(scheduler);
	}

	/**
	 * Test get scheduler by id.
	 */
	@Test(expected=POException.class)
	public void testGetSchedulerById() {
		try {
			Scheduler scheduler = null;
			when(schedulerService.getSchedulerById(1)).thenReturn(scheduler);
			assertNull(scheduler);
			scheduler = schedulerService.getSchedulerById(1);
			assertNotNull(scheduler);
			exception.expectMessage("Failed JUNIT_FIND_Record");
		} catch (Exception ex) {
	       assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}

	/**
	 * Test get all scheduler.
	 */
	@Test(expected=POException.class)
	public void testGetAllScheduler() {
		try {
			List<Scheduler> schedulerList = null;
			when(schedulerService.getAllScheduler()).thenReturn(schedulerList);
			assertNull(schedulerList);
			schedulerList = schedulerService.getAllScheduler();
			assertNotNull(schedulerList);
			exception.expectMessage("Failed JUNIT_FIND_All");
		} catch (Exception ex) {
	      assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}

	/**
	 * Test get all scheduler in order.
	 */
	@Test(expected=POException.class)
	public void testGetAllSchedulerInOrder() {
		try {
			List<Scheduler> schedulerList = null;
			when(schedulerService.getAllSchedulerInOrder()).thenReturn(schedulerList);
			assertNull(schedulerList);
			schedulerList = schedulerService.getAllSchedulerInOrder();
			assertNotNull(schedulerList);
			exception.expectMessage("Failed JUNIT_FIND_All");
		} catch (Exception ex) {
	        assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}
}