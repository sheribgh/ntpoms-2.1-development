/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.sql.SQLException;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.test.context.junit4.SpringRunner;

import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.entities.Department;
import my.com.parkson.ntpoms.common.repositories.DepartmentRepository;
import my.com.parkson.ntpoms.common.services.impl.DepartmentServiceImpl;

/**
 * The Class DepartmentServiceTest.
 */
@RunWith(SpringRunner.class)
public class DepartmentServiceTest {
	
	/** The exception. */
	@Rule
	public final ExpectedException exception = ExpectedException.none();

	/** The test entity manager. */
	@MockBean
	private TestEntityManager testEntityManager;

	/** The department repository. */
	@MockBean
	private DepartmentRepository departmentRepository;

	/** The department service. */
	private DepartmentService departmentService;

	/** The name. */
	private String name = "JUNIT Name";
	
	/** The code. */
	private String code = "JUNIT";

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		Department department = new Department();
		department.setName(name);
		department.setCode(code);
		testEntityManager.persist(department);
		when(departmentRepository.findByCode(code)).thenReturn(department);
		departmentService = new DepartmentServiceImpl();
	}

	/**
	 * Test get all department.
	 */
	@Test(expected=POException.class)
	public void testGetAllDepartment() {
		try {
			List<Department> departmentList = null;
			when(departmentService.getAllDepartment()).thenReturn(departmentList);
			assertNull(departmentList);
			departmentList = departmentService.getAllDepartment();
			exception.expectMessage("Failed JUNIT_FIND_All");
			assertNotNull(departmentList);
		} catch (Exception ex) {
	        assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}

	/**
	 * Test get all active department.
	 */
	@Test(expected=POException.class)
	public void testGetAllActiveDepartment() {
		try {
			List<Department> departmentList = null;
			when(departmentService.getAllActiveDepartment()).thenReturn(departmentList);
			assertNull(departmentList);
			departmentList = departmentService.getAllActiveDepartment();
			assertNull(departmentList);
			exception.expectMessage("Failed JUNIT_FIND_All");
		} catch (Exception ex) {
	         assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}

	/**
	 * Test get department by code.
	 */
	@Test(expected=POException.class)
	public void testGetDepartmentByCode() {
		try {
			Department department = null;
			when(departmentRepository.findByCode(code)).thenReturn(department);
			assertNull(department);
			department = departmentService.getDepartmentByCode(code);
			assertNotNull(department);
			exception.expectMessage("Failed JUNIT_FIND_Record");
		} catch (Exception ex) {
	        assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}

	/**
	 * Test create department.
	 */
	@Test
	public void testCreateDepartment() {
		Department department = new Department();
		department.setName(name);
		department.setCode(code);
		doNothing().when(departmentService).createDepartment(department);
		assertNotNull(department);
	}
}