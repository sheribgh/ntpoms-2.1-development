/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.repositories;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import my.com.parkson.ntpoms.common.entities.User;

/**
 * The Class AdminMasterRepositoryTest.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class AdminMasterRepositoryTest {
	
	/** The test entity manager. */
	@Autowired
	TestEntityManager testEntityManager;

	/** The user repository. */
	@Autowired
	UsersRepository userRepository;

	/** The username. */
	private String username = "JUNIT Name";
	
	/** The start date. */
	private String startDate = "2017-09-22";

	/** The username except. */
	private String usernameExcept = "JUNIT NameExcept";
	

/**
 * Test find by name.
 */
	@Test
	public void testFindByName() {
		User user = new User();
		user.setUsername(username);
		user.setStartDate(new Date());
		testEntityManager.persist(user);

		user = userRepository.findByUsernameAndIsActiveAndIsLocked(username, true, false);
		assertNotNull(user.getUsername());
		assertTrue(user.getStartDate().equals(startDate));

		List<User> userList = userRepository.findAll();
		assertNotEquals(1, userList.size());
		User users = userRepository.findByUsernameAndIsActiveAndIsLocked(username, true, false);
		assertEquals(1, userList.size());
		userList = userRepository.findAll();
		assertEquals(1, userList.size());
	}

}
