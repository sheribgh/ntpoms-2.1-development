/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.sql.SQLException;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.test.context.junit4.SpringRunner;

import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.entities.Company;
import my.com.parkson.ntpoms.common.repositories.CompanyRepository;
import my.com.parkson.ntpoms.common.services.impl.CompanyServiceImpl;

/**
 * The Class CompanyServiceTest.
 */
@RunWith(SpringRunner.class)
public class CompanyServiceTest {
	
	/** The exception. */
	@Rule
	public final ExpectedException exception = ExpectedException.none();

	/** The test entity manager. */
	@MockBean
	private TestEntityManager testEntityManager;

	/** The company repository. */
	@MockBean
	private CompanyRepository companyRepository;

	/** The company service. */
	private CompanyService companyService;

	/** The code HRIS. */
	private String codeHRIS = "JUNIT HRIS";
	
	/** The code. */
	private String code = "JUNIT";
	
	/** The name. */
	private String name = "JUNIT Name";

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		Company company = new Company();
		company.setCode(code);
		company.setCodeHRIS(codeHRIS);
		company.setName(name);
		testEntityManager.persist(company);
		when(companyRepository.findByCode(code)).thenReturn(company);
		companyService = new CompanyServiceImpl();
	}

	/**
	 * Test get all company.
	 */
	@Test(expected=POException.class)
	public void testGetAllCompany() {
		try {
			List<Company> companyList = null;
			when(companyService.getAllCompany()).thenReturn(companyList);
			assertNull(companyList);
			companyList = companyService.getAllCompany();
			assertNotNull(companyList);
			exception.expectMessage("Failed JUNIT_FIND_All");
		} catch (Exception ex) {
	         assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}

	/**
	 * Test get all active company.
	 */
	@Test(expected=POException.class)
	public void testGetAllActiveCompany() {
		try {
			List<Company> companyList = null;			
			when(companyService.getAllActiveCompany()).thenReturn(companyList);
			assertNull(companyList);
			companyList = companyService.getAllActiveCompany();
			assertNotNull(companyList);
			exception.expectMessage("Failed JUNIT_FIND_All");
		} catch (Exception ex) {
	        assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}

	/**
	 * Test get company by code.
	 */
	@Test(expected=POException.class)
	public void testGetCompanyByCode() {
		try {
			Company company = null;
			when(companyService.getCompanyByCode(code)).thenReturn(company);
			assertNull(company);
			company = companyService.getCompanyByCode(code);
			assertNotNull(company);
			exception.expectMessage("Failed JUNIT_FIND_Record");
		} catch (Exception ex) {
	        assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}

	/**
	 * Test get company by code HRIS.
	 */
	@Test(expected=POException.class)
	public void testGetCompanyByCodeHRIS() {
		try {
			Company company = null;
			when(companyService.getCompanyByCodeHRIS(codeHRIS)).thenReturn(null);
			assertNull(company);
			company = companyService.getCompanyByCodeHRIS(codeHRIS);
			exception.expectMessage("Failed JUNIT_FIND_Record");
			assertNotNull(company);
		} catch (Exception ex) {
	        assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}

	/**
	 * Test get by search code.
	 */
	@Test(expected=POException.class)
	public void testGetBySearchCode() {
		try {
			List<Company> companyList = null;
			when(companyService.getBySearchCode(code)).thenReturn(companyList);
			assertNull(companyList);
			companyList = companyService.getBySearchCode(code);
			exception.expectMessage("Failed JUNIT_FIND_Record");
			assertNotNull(companyList);
		} catch (Exception ex) {
	        assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}

	/**
	 * Test create company.
	 */
	@Test(expected=POException.class)
	public void testCreateCompany() {
		Company company = new Company();
		company.setCode(code);
		company.setCodeHRIS(codeHRIS);
		company.setName(name);
		testEntityManager.persist(company);
		doNothing().when(companyService).createCompany(company);
		assertNotNull(company);
	}
}