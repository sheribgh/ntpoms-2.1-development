/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.sql.SQLException;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.test.context.junit4.SpringRunner;

import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.entities.OrderCategory;
import my.com.parkson.ntpoms.common.repositories.OrderCategoryRepository;
import my.com.parkson.ntpoms.common.services.impl.OrderCategoryServiceImpl;

/**
 * The Class OrderCategoryServiceTest.
 */
@RunWith(SpringRunner.class)
public class OrderCategoryServiceTest {

	/** The exception. */
	@Rule
	public final ExpectedException exception = ExpectedException.none();

	/** The test entity manager. */
	@MockBean
	private TestEntityManager testEntityManager;

	/** The ordercategory repository. */
	@MockBean
	private OrderCategoryRepository ordercategoryRepository;

	/** The order category service. */
	private OrderCategoryService orderCategoryService;

	/** The code. */
	private String code = "JUNIT";
	
	/** The desc. */
	private String desc = "JUNIT Description";


	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		OrderCategory OrderCategory = new OrderCategory();
		OrderCategory.setCode(code);
		OrderCategory.setDesc(desc);
		testEntityManager.persist(OrderCategory);
		when(ordercategoryRepository.findByCode(code)).thenReturn(OrderCategory);
		orderCategoryService = new OrderCategoryServiceImpl();
	}

	/**
	 * Test get all order category.
	 */
	@Test(expected=POException.class)
	public void testGetAllOrderCategory() {
		try {
			List<OrderCategory> orderCategoryList = null;
			when(orderCategoryService.getAllOrderCategory()).thenReturn(orderCategoryList);
			assertNull(orderCategoryList);
			orderCategoryList = orderCategoryService.getAllOrderCategory();
			assertNotNull(orderCategoryList);
			exception.expectMessage("Failed JUNIT_FIND_All");
		} catch (Exception ex) {
	         assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}

	/**
	 * Test get all active order category.
	 */
	@Test(expected=POException.class)
	public void testGetAllActiveOrderCategory() {
		try {
			List<OrderCategory> orderCategoryList = null;
			when(orderCategoryService.getAllActiveOrderCategory()).thenReturn(orderCategoryList);
			assertNull(orderCategoryList);
			orderCategoryList = orderCategoryService.getAllActiveOrderCategory();
			assertNotNull(orderCategoryList);
			exception.expectMessage("Failed JUNIT_FIND_All");
		} catch (Exception ex) {
	        assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}

	/**
	 * Test get order category by code.
	 */
	@Test(expected=POException.class)
	public void testGetOrderCategoryByCode() {
		try {
			OrderCategory orderCategory = null;
			when(ordercategoryRepository.findByCode(code)).thenReturn(orderCategory);
			assertNull(orderCategory);
			orderCategory = orderCategoryService.getOrderCategoryByCode(code);
			assertNotNull(orderCategory);
			exception.expectMessage("Failed JUNIT_FIND_All");
		} catch (Exception ex) {
	        assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}

	/**
	 * Test create order category.
	 */
	@Test
	public void testCreateOrderCategory() {
		OrderCategory orderCategory = new OrderCategory();
		orderCategory.setCode(code);
		orderCategory.setDesc(desc);
		testEntityManager.persist(orderCategory);
		doNothing().when(orderCategoryService).createOrderCategory(orderCategory);
		assertNotNull(orderCategory);
	}
}