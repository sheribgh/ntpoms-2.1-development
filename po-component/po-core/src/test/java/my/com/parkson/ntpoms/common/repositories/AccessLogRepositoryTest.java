/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.repositories;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import my.com.parkson.ntpoms.common.entities.AccessLog;

/**
 * The Class AccessLogRepositoryTest.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class AccessLogRepositoryTest {
	
	/** The test entity manager. */
	@Autowired
	TestEntityManager testEntityManager;
	
	/** The access log repository. */
	@Autowired
	AccessLogRepository accessLogRepository;

	/** The message. */
	private String message = "JUNIT Debug";
	
	/** The level. */
	private String level = "DEBUG";

	/**
	 * Test find all by order by id desc.
	 */
	@Test
	public void testFindAllByOrderByIdDesc() {
		AccessLog accessLog = new AccessLog();
		accessLog.setLevel(level);
		accessLog.setMessage(message);
		testEntityManager.persist(accessLog);
		
		List<AccessLog> accessLogList = accessLogRepository.findAllByOrderByIdDesc();
		assertEquals(1, accessLogList.size());
	}
}