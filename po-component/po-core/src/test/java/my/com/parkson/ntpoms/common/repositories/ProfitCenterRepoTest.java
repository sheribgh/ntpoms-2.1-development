/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.repositories;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import my.com.parkson.ntpoms.common.entities.ProfitCenter;

/**
 * The Class ProfitCenterRepoTest.
 */
@RunWith(SpringRunner.class)
@DataJpaTest

public class ProfitCenterRepoTest {

	/** The test entity manager. */
	@Autowired
	TestEntityManager testEntityManager;

	/** The profit center repository. */
	@Autowired
	ProfitCenterRepository profitCenterRepository;

	/** The company repository. */
	@Autowired
	CompanyRepository companyRepository;

	/** The code. */
	private String code = "JUNIT";
	
	/** The name. */
	private String name = "JUNIT Name";
	
	/** The company code. */
	private String companyCode = "JUNIT CompanyCode";

	/**
	 * Test find by name.
	 */
	@Test
	public void testFindByName() {
		ProfitCenter ProfitCenter = new ProfitCenter();
		ProfitCenter.setCode(code);
		ProfitCenter.setName(name);
		testEntityManager.persist(ProfitCenter);

		ProfitCenter = profitCenterRepository.findByCode(code);
		assertNotNull(ProfitCenter.getCode());
		assertTrue(ProfitCenter.getName().equals(name));
		List<ProfitCenter> ProfitCenterList = profitCenterRepository.findAllByOrderByLastModifiedOnDesc();
		assertEquals(1, ProfitCenterList.size());
		ProfitCenterList = profitCenterRepository.findAllByIsActiveOrderByLastModifiedOnDesc(true);
		assertEquals(1, ProfitCenterList.size());
		ProfitCenterList = profitCenterRepository.findAllByCompanyCodeAndIsactiveOrderByLastModifiedOnDesc(companyCode,true);
		assertEquals(1, ProfitCenterList.size());
	}

}
