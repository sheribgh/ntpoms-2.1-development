/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.repositories;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import my.com.parkson.ntpoms.common.entities.DataSource;

/**
 * The Class DataSourceRepositoryTest.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class DataSourceRepositoryTest {

	/** The test entity manager. */
	@Autowired
	TestEntityManager testEntityManager;

	/** The data source repository. */
	@Autowired
	DataSourceRepository dataSourceRepository;

	/** The name. */
	private String name = "JUNIT";
	
	/** The company. */
	private String company = "JUNIT";
	
	/** The year. */
	private String year = "2018";

	/**
	 * Test find by name.
	 */
	@Test
	public void testFindByName() {
		DataSource dataSource = new DataSource();
		dataSource.setName(name);
		dataSource.setCompany(company);
		dataSource.setYear(year);
		testEntityManager.persist(company);

		dataSource = dataSourceRepository.findByCompanyAndYear(company, year);
		assertNotNull(dataSource.getId());
		assertTrue(dataSource.getName().equals(name));

		List<DataSource> dataSourceList = dataSourceRepository.findAllByIsActiveAndYear(true, year);
		assertEquals(1, dataSourceList.size());
		dataSourceList = dataSourceRepository.findAllAfterYear(year);
		assertEquals(1, dataSourceList.size());
		dataSourceList = dataSourceRepository.findAll();
		assertEquals(1, dataSourceList.size());
	}	
}