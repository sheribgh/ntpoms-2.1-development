/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.repositories;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.test.context.junit4.SpringRunner;

import my.com.parkson.ntpoms.common.entities.Job;
import my.com.parkson.ntpoms.common.entities.MasterData;

/**
 * The Class MasterDataRepositoryTest.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class MasterDataRepositoryTest {

	/** The test entity manager. */
	@Autowired
	TestEntityManager testEntityManager;

	/** The master data repository. */
	@Autowired
	MasterDataRepository masterDataRepository;

	/** The code desc. */
	private String codeDesc = "JUNIT Desc";
	
	/** The code type. */
	private String codeType = "JUNIT Type";
	
	/** The code value. */
	private String codeValue = "JUNIT Value";
	
	/** The id. */
	private int id = 2;

	/**
	 * Test find by name.
	 */
	@Test
	public void testFindByName() {
		MasterData masterData = new MasterData();
		masterData.setCodeDesc(codeDesc);
		masterData.setCodeType(codeType);
		masterData.setCodeValue(codeValue);
		testEntityManager.persist(masterData);

		masterData = masterDataRepository.findById(id);
		assertNotNull(masterData.getId());
		assertTrue(masterData.getCodeDesc().equals(codeDesc));

		List<MasterData> masterDataList = masterDataRepository.findAllByOrderByLastModifiedOnDesc();
		assertEquals(1, masterDataList.size());
		masterDataList = masterDataRepository.findAllByIsActiveOrderByLastModifiedOnDesc(true);
		assertEquals(1, masterDataList.size());
	}
}