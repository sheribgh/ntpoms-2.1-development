/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.test.context.junit4.SpringRunner;

import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.entities.User;
import my.com.parkson.ntpoms.common.repositories.UsersRepository;
import my.com.parkson.ntpoms.common.services.impl.UserServiceImpl;

/**
 * The Class UsersServiceTest.
 */
@RunWith(SpringRunner.class)
public class UsersServiceTest {

	/** The exception. */
	@Rule
	public final ExpectedException exception = ExpectedException.none();

	/** The test entity manager. */
	@MockBean
	private TestEntityManager testEntityManager;

	/** The user repository. */
	@MockBean
	private UsersRepository userRepository;

	/** The users service. */
	private UsersService usersService;

	/** The username. */
	private String username = "JUNIT Name";	

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		User user = new User();
		user.setUsername(username);
		user.setStartDate(new Date());
		testEntityManager.persist(user);
		when(userRepository.findByUsername(username)).thenReturn(user);
		usersService = new UserServiceImpl();
	}

	/**
	 * Test create users.
	 */
	@Test
	public void testCreateUsers() {
		User user = new User();
		user.setUsername(username);
		user.setStartDate(new Date());
		doNothing().when(usersService).createUsers(user);
		assertNotNull(user);
	}
	
	/**
	 * Test get users by username.
	 */
	@Test(expected=POException.class)
	public void testGetUsersByUsername() {
		try {
			User user = null;
			when(usersService.getUsersByUsername(username)).thenReturn(user);
			assertNull(user);
			user = usersService.getUsersByUsername(username);
			assertNotNull(user);
			exception.expectMessage("Failed JUNIT_FIND_Record");
		} catch (Exception ex) {
	        assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}

	/**
	 * Test get all users.
	 */
	@Test(expected=POException.class)
	public void testGetAllUsers() {
		try {
			List<User> userList = null;
			when(usersService.getAllUsers()).thenReturn(userList);
			assertNull(userList);
			userList = usersService.getAllUsers();
			assertNotNull(userList);
			exception.expectMessage("Failed JUNIT_FIND_Record");
		} catch (Exception ex) {
	       assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}

/**
 * Test get all users exclude superadmin.
 */
@Test(expected=POException.class)
	public void testGetAllUsersExcludeSuperadmin() {
		try {
			List<User> userList = null;
			when(usersService.getAllUsersExcludeSuperadmin()).thenReturn(userList);
			assertNull(userList);
			userList = usersService.getAllUsersExcludeSuperadmin();
			assertNotNull(userList);
			exception.expectMessage("Failed JUNIT_FIND_Record");
		} catch (Exception ex) {
	        assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}

	/**
	 * Test get all active users.
	 */
	@Test(expected=POException.class)
	public void testGetAllActiveUsers() {
		try {
			List<User> userList = null;
			when(usersService.getAllActiveUsers()).thenReturn(userList);
			assertNull(userList);
			userList = usersService.getAllActiveUsers();
			assertNotNull(userList);
			exception.expectMessage("Failed JUNIT_FIND_Record");
		} catch (Exception ex) {
	       assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}
}