/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.repositories;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import my.com.parkson.ntpoms.common.entities.Job;

/**
 * The Class JobRepositoryTest.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class JobRepositoryTest {

	/** The test entity manager. */
	@Autowired
	TestEntityManager testEntityManager;

	/** The job repository. */
	@Autowired
	JobRepository jobRepository;

	/** The code. */
	private String code = "JUNIT";
	
	/** The name. */
	private String name = "JUNIT name";

	/**
	 * Test find by name.
	 */
	@Test
	public void testFindByName() {
		Job job = new Job();
		job.setCode(code);
		job.setName(name);
		testEntityManager.persist(job);

		job = jobRepository.findByCode(code);
		assertNotNull(job.getCode());
		assertTrue(job.getName().equals(name));

		List<Job> jobList = jobRepository.findAllByOrderByLastModifiedOnDesc();
		assertEquals(1, jobList.size());
		jobList = jobRepository.findAllByIsActiveOrderByLastModifiedOnDesc(true);
		assertEquals(1, jobList.size());
	}
}