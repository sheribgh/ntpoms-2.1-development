/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services;
/*
 * ********************************************************************************
 * Copyright (c) 2018 - Brentin Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "eCommerce-core" is used for the core features and functionalities inside
 * eCommerce application.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : eCommerce-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Brentin Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */


import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;

import java.io.IOException;

import javax.mail.MessagingException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSendException;
import org.springframework.test.context.junit4.SpringRunner;

import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.ServerSetupTest;

/**
 * The Class EmailServiceTest.
 */
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class EmailServiceTest {

	/** The email service. */
	@MockBean
	private EmailService emailService;

	/** The test smtp. */
	private GreenMail testSmtp;

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		testSmtp = new GreenMail(ServerSetupTest.SMTP);
		testSmtp.start();
	}

	/**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception {
		testSmtp.stop();
	}

	/**
	 * Test send email.
	 */
	@Test
	public void testSendEmail() {
		try {
			doNothing().when(emailService).sendEmail(any(), anyObject(), null, null);
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//emailService.sendEmail("sagar.subhash@gmail.com", "eCommerce - Test Mail", "This is a test email from Brentin eCommerce");
		assertNotNull(emailService);
	}

	/**
	 * Test send email with exception.
	 */
	@Test(expected = MailException.class)
	public void testSendEmailWithException() {
		try {
			doThrow(new MailSendException("JUNIT EMAIL")).when(emailService).sendEmail(any(), anyObject(), null, null);
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//emailService.sendEmail("sagar.subhash@gmail.com", "eCommerce - Test Mail", "This is a test email from Brentin eCommerce");
		assertNotNull(emailService);
	}

}
