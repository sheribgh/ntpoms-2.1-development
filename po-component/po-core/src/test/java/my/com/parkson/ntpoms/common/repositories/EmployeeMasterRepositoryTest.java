/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.repositories;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import my.com.parkson.ntpoms.common.entities.EmployeeMaster;

/**
 * The Class EmployeeMasterRepositoryTest.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class EmployeeMasterRepositoryTest {

	/** The test entity manager. */
	@Autowired
	TestEntityManager testEntityManager;

	/** The employee master repository. */
	@Autowired
	EmployeeMasterRepository employeeMasterRepository;

	/** The name. */
	private String name = "JUNIT Name";
	
	/** The new ic no. */
	private String newIcNo = "JK01111111";
	
	/** The gender. */
	private String gender = "Male";
	
	/** The ps id. */
	private String psId = "PO011111";
	
	/** The code. */
	private String code = "PC011111";

	/**
	 * Test find by name.
	 */
	@Test
	public void testFindByName() {
		EmployeeMaster employeeMaster = new EmployeeMaster();
		employeeMaster.setName(name);
		employeeMaster.setNewIcNo(newIcNo);
		employeeMaster.setGender(gender);
		testEntityManager.persist(employeeMaster);

		employeeMaster = employeeMasterRepository.findByPsId(psId);
		assertNotNull(employeeMaster.getPsId());
		assertTrue(employeeMaster.getName().equals(name));

		List<EmployeeMaster> departmentList = employeeMasterRepository.findAll();
		assertEquals(1, departmentList.size());
		departmentList = employeeMasterRepository.findAllByIsActive(true);
		assertEquals(1, departmentList.size());
		departmentList = employeeMasterRepository.findAllActiveByProfitCenterCode(code);
		assertEquals(1, departmentList.size());
		departmentList = employeeMasterRepository.findAll();
		assertEquals(1, departmentList.size());
		List<String> psIdList = employeeMasterRepository.findAllActivePsIds();
		assertEquals(1, psIdList.size());
	}
}