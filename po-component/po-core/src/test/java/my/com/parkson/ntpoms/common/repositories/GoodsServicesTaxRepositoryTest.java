/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.repositories;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import my.com.parkson.ntpoms.common.entities.GoodsServicesTax;

/**
 * The Class GoodsServicesTaxRepositoryTest.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class GoodsServicesTaxRepositoryTest {

	/** The test entity manager. */
	@Autowired
	TestEntityManager testEntityManager;

	/** The goods services tax repository. */
	@Autowired
	GoodsServicesTaxRepository goodsServicesTaxRepository;

	/** The code. */
	private String code = "JUNIT";
	
	/** The desc. */
	private String desc = "JUNIT Description";
	
	/** The rate. */
	private Double rate = 2.5;

	/**
	 * Test find by name.
	 */
	@Test
	public void testFindByName() {
		GoodsServicesTax goodsServicesTax = new GoodsServicesTax();
		goodsServicesTax.setCode(code);
		goodsServicesTax.setDesc(desc);
		goodsServicesTax.setRate(rate);
		testEntityManager.persist(goodsServicesTax);

		goodsServicesTax = goodsServicesTaxRepository.findByCode(code);
		assertNotNull(goodsServicesTax.getCode());
		assertTrue(goodsServicesTax.getDesc().equals(desc));

		List<GoodsServicesTax> goodsServicesTaxList = goodsServicesTaxRepository.findAllByOrderByLastModifiedOnDesc();
		assertEquals(1, goodsServicesTaxList.size());
		goodsServicesTaxList = goodsServicesTaxRepository.findAllByIsActiveOrderByLastModifiedOnDesc(true);
		assertEquals(1, goodsServicesTaxList.size());
	}
}