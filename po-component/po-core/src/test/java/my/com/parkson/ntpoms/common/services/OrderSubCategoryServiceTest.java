/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.sql.SQLException;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.test.context.junit4.SpringRunner;

import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.entities.OrderSubCategory;
import my.com.parkson.ntpoms.common.repositories.OrderSubCategoryRepository;
import my.com.parkson.ntpoms.common.services.impl.OrderSubCategoryServiceImpl;

/**
 * The Class OrderSubCategoryServiceTest.
 */
@RunWith(SpringRunner.class)
public class OrderSubCategoryServiceTest {

	/** The exception. */
	@Rule
	public final ExpectedException exception = ExpectedException.none();

	/** The test entity manager. */
	@MockBean
	private TestEntityManager testEntityManager;

	/** The order sub category repository. */
	@MockBean
	private OrderSubCategoryRepository orderSubCategoryRepository;

	/** The order sub category service. */
	private OrderSubCategoryService orderSubCategoryService;

	/** The code. */
	private String code = "JUNIT";	
	
	/** The desc. */
	private String desc = "JUNIT Description";
	
	/** The order catcode. */
	private String orderCatcode = "JUNIT orderCatcode";

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		OrderSubCategory orderSubCategory = new OrderSubCategory();
		orderSubCategory.setCode(code);
		orderSubCategory.setDesc(desc);
		testEntityManager.persist(orderSubCategory);
		when(orderSubCategoryRepository.findByCode(code)).thenReturn(orderSubCategory);
		orderSubCategoryService = new OrderSubCategoryServiceImpl();
	}

	/**
	 * Test get all order sub category.
	 */
	@Test(expected=POException.class)
	public void testGetAllOrderSubCategory() {
		try {
			List<OrderSubCategory> orderSubCategoryList = null;
			when(orderSubCategoryService.getAllOrderSubCategory()).thenReturn(orderSubCategoryList);
			assertNull(orderSubCategoryList);
			orderSubCategoryList = orderSubCategoryService.getAllOrderSubCategory();
			assertNotNull(orderSubCategoryList);
			exception.expectMessage("Failed JUNIT_FIND_All");
		} catch (Exception ex) {
	         assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}

	/**
	 * Test get all active order sub category.
	 */
	@Test(expected=POException.class)
	public void testGetAllActiveOrderSubCategory() {
		try {
			List<OrderSubCategory> orderSubCategoryList = null;
			when(orderSubCategoryService.getAllActiveOrderSubCategory()).thenReturn(orderSubCategoryList);
			assertNull(orderSubCategoryList);
			orderSubCategoryList = orderSubCategoryService.getAllActiveOrderSubCategory();
			assertNotNull(orderSubCategoryList);
			exception.expectMessage("Failed JUNIT_FIND_All");
		} catch (Exception ex) {
	        assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}

	/**
	 * Test get order sub category by code.
	 */
	@Test(expected=POException.class)
	public void testGetOrderSubCategoryByCode() {
		try {
			OrderSubCategory orderSubCategory = null;
			when(orderSubCategoryService.getOrderSubCategoryByCode(code)).thenReturn(orderSubCategory);
			assertNull(orderSubCategory);
			orderSubCategory = orderSubCategoryService.getOrderSubCategoryByCode(code);
			assertNotNull(orderSubCategory);
			exception.expectMessage("Failed JUNIT_FIND_All");
		} catch (Exception ex) {
	        assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}

	/**
	 * Test create order sub category.
	 */
	@Test
	public void testCreateOrderSubCategory() {
		OrderSubCategory orderSubCategory = new OrderSubCategory();
		orderSubCategory.setCode(code);
		orderSubCategory.setDesc(desc);
		doNothing().when(orderSubCategoryService).createOrderSubCategory(orderSubCategory);
		assertNotNull(orderSubCategory);
	}

	/**
	 * Test get all order sub category by order category code.
	 */
	@Test(expected=POException.class)
	public void testGetAllOrderSubCategoryByOrderCategoryCode() {
		try {
			List<OrderSubCategory> orderSubCategoryList = null;
			when(orderSubCategoryService.getAllOrderSubCategoryByOrderCategoryCode(orderCatcode)).thenReturn(orderSubCategoryList);
			assertNull(orderSubCategoryList);
			orderSubCategoryList = orderSubCategoryService.getAllOrderSubCategoryByOrderCategoryCode(orderCatcode);
			assertNotNull(orderSubCategoryList);
			exception.expectMessage("Failed JUNIT_FIND_All");
		} catch (Exception ex) {
	        assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}

	/**
	 * Test get all active order sub category by order category code.
	 */
	@Test(expected=POException.class)
	public void testGetAllActiveOrderSubCategoryByOrderCategoryCode() {
		try {
			List<OrderSubCategory> orderSubCategoryList = null;
			when(orderSubCategoryService.getAllActiveOrderSubCategoryByOrderCategoryCode(orderCatcode)).thenReturn(orderSubCategoryList);
			assertNull(orderSubCategoryList);
			orderSubCategoryList = orderSubCategoryService.getAllActiveOrderSubCategoryByOrderCategoryCode(orderCatcode);
			assertNotNull(orderSubCategoryList);
			exception.expectMessage("Failed JUNIT_FIND_All");
		} catch (Exception ex) {
	        assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}

}
