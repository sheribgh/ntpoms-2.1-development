/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.repositories;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import my.com.parkson.ntpoms.common.entities.Store;

/**
 * The Class StoreRepoTest.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class StoreRepoTest {

	/** The test entity manager. */
	@Autowired
	TestEntityManager testEntityManager;

	/** The store repository. */
	@Autowired
	StoreRepository storeRepository;

	/** The company repository. */
	@Autowired
	CompanyRepository companyRepository;

	/** The code. */
	private String code = "JUNIT";
	
	/** The name. */
	private String name = "JUNIT Name";

	/** The company code. */
	private String companyCode = "JUNIT companyCode";


	/**
	 * Test find by name.
	 */
	@Test
	public void testFindByName() {
		Store Store = new Store();
		Store.setCode(code);
		Store.setName(name);
		testEntityManager.persist(Store);

		Store = storeRepository.findByCode(code);
		assertNotNull(Store.getCode());
		assertTrue(Store.getName().equals(name));
		List<Store> StoreList = storeRepository.findAllByOrderByLastModifiedOnDesc();
		assertEquals(1, StoreList.size());
		StoreList = storeRepository.findByIsActiveOrderByLastModifiedOnDesc(true);
		assertEquals(1, StoreList.size());
		StoreList = storeRepository.findAllActiveByCompanyCode(companyCode);
		assertEquals(1, StoreList.size());
	}
}