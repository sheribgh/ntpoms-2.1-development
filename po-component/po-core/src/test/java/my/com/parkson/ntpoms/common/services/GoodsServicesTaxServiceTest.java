/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.sql.SQLException;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.test.context.junit4.SpringRunner;

import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.entities.GoodsServicesTax;
import my.com.parkson.ntpoms.common.repositories.GoodsServicesTaxRepository;
import my.com.parkson.ntpoms.common.services.impl.GoodsServicesTaxServiceImpl;

/**
 * The Class GoodsServicesTaxServiceTest.
 */
@RunWith(SpringRunner.class)
public class GoodsServicesTaxServiceTest {

	/** The exception. */
	@Rule
	public final ExpectedException exception = ExpectedException.none();

	/** The messages. */
	@MockBean
	private MessageService messages;

	/** The goods services tax repository. */
	@MockBean
	private GoodsServicesTaxRepository goodsServicesTaxRepository;

	/** The test entity manager. */
	@MockBean
	private TestEntityManager testEntityManager;

   	/** The goods services tax service. */
	   private GoodsServicesTaxService goodsServicesTaxService; 

   	/** The code. */
	   private String code = "JUNIT";
	
	/** The desc. */
	private String desc = "JUNIT Description";
	
	/** The rate. */
	private Double rate = 2.5;

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		GoodsServicesTax goodsServicesTax = new GoodsServicesTax();
		goodsServicesTax.setCode(code);
		goodsServicesTax.setDesc(desc);
		goodsServicesTax.setRate(rate);
		testEntityManager.persist(goodsServicesTax);
		when(goodsServicesTaxRepository.findByCode(code)).thenReturn(goodsServicesTax);
		goodsServicesTaxService = new GoodsServicesTaxServiceImpl();
	}

	/**
	 * Test create goods services tax.
	 */
	@Test
	public void testCreateGoodsServicesTax() {
		GoodsServicesTax goodsServicesTax = new GoodsServicesTax();
		goodsServicesTax.setCode(code);
		goodsServicesTax.setDesc(desc);
		goodsServicesTax.setRate(rate);
		doNothing().when(goodsServicesTaxService).createGoodsServicesTax(goodsServicesTax);
		assertNotNull(goodsServicesTax);
	}

	/**
	 * Test get goods services tax by code.
	 */
	@Test(expected=POException.class)
	public void testGetGoodsServicesTaxByCode() {
		try {
			GoodsServicesTax goodsServicesTax = null;
			when(goodsServicesTaxService.getGoodsServicesTaxByCode(code)).thenReturn(goodsServicesTax);
			assertNull(goodsServicesTax);
			goodsServicesTax = goodsServicesTaxService.getGoodsServicesTaxByCode(code);
			assertNotNull(goodsServicesTax);
			exception.expectMessage("Failed JUNIT_FIND_Record");
		} catch (Exception ex) {
	        assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}

	/**
	 * Test get all goods services tax.
	 */
	@Test(expected=POException.class)
	public void testGetAllGoodsServicesTax() {
		try {
			List<GoodsServicesTax> goodsServicesTaxList = null;
			when(goodsServicesTaxService.getAllGoodsServicesTax()).thenReturn(goodsServicesTaxList);
			assertNull(goodsServicesTaxList);
			goodsServicesTaxList = goodsServicesTaxService.getAllGoodsServicesTax();
			assertNull(goodsServicesTaxList);
			exception.expectMessage("Failed JUNIT_FIND_All");
		} catch (Exception ex) {
	        assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}

	/**
	 * Test get all active goods services tax.
	 */
	@Test(expected=POException.class)
	public void testGetAllActiveGoodsServicesTax() {
		try {
			List<GoodsServicesTax> goodsServicesTaxList = null;
			when(goodsServicesTaxService.getAllActiveGoodsServicesTax()).thenReturn(goodsServicesTaxList);
			assertNull(goodsServicesTaxList);
			goodsServicesTaxList = goodsServicesTaxService.getAllActiveGoodsServicesTax();
			assertNull(goodsServicesTaxList);
			exception.expectMessage("Failed JUNIT_FIND_All");
		} catch (Exception ex) {
	        assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}

}
