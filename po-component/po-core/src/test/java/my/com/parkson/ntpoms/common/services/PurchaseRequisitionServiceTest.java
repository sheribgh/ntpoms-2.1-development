/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.sql.SQLException;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.test.context.junit4.SpringRunner;

import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.entities.PurchaseRequisition;
import my.com.parkson.ntpoms.common.repositories.PurchaseRequisitionRepository;
import my.com.parkson.ntpoms.common.services.impl.PurchaseRequisitionServiceImpl;

/**
 * The Class PurchaseRequisitionServiceTest.
 */
@RunWith(SpringRunner.class)
public class PurchaseRequisitionServiceTest {

	/** The exception. */
	@Rule
	public final ExpectedException exception = ExpectedException.none();

	/** The test entity manager. */
	@MockBean
	private TestEntityManager testEntityManager;

	/** The purchase requisition repository. */
	@MockBean
	private PurchaseRequisitionRepository purchaseRequisitionRepository;

	/** The purchase requisition service. */
	private PurchaseRequisitionService purchaseRequisitionService;

	/** The type code. */
	private String typeCode = "JUNIT typeCode";
	
	/** The desc. */
	private String desc = "JUNIT Description";

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		PurchaseRequisition purchaseRequisition = new PurchaseRequisition();	
		purchaseRequisition.setTypeCode(typeCode);
		purchaseRequisition.setDesc(desc);
		testEntityManager.persist(purchaseRequisition);
		when(purchaseRequisitionRepository.findByTypeCode(typeCode)).thenReturn(purchaseRequisition);
		purchaseRequisitionService = new PurchaseRequisitionServiceImpl();
	}

	/**
	 * Test create purchase requisition.
	 */
	@Test
	public void testCreatePurchaseRequisition() {
		PurchaseRequisition purchaseRequisition = new PurchaseRequisition();	
		purchaseRequisition.setTypeCode(typeCode);
		purchaseRequisition.setDesc(desc);
		doNothing().when(purchaseRequisitionService).createPurchaseRequisition(purchaseRequisition);
		assertNotNull(purchaseRequisition);
	}

	/**
	 * Test get purchase requisition by type code.
	 */
	@Test(expected=POException.class)
	public void testGetPurchaseRequisitionByTypeCode() {
		try {
			PurchaseRequisition purchaseRequisition = null;
			when(purchaseRequisitionService.getPurchaseRequisitionByTypeCode(typeCode)).thenReturn(null);
			assertNull(purchaseRequisition);
			purchaseRequisition = purchaseRequisitionService.getPurchaseRequisitionByTypeCode(typeCode);
			assertNotNull(purchaseRequisition);
			exception.expectMessage("Failed JUNIT_FIND_Record");
		} catch (Exception ex) {
	       assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}

	/**
	 * Test get all purchase requisition.
	 */
	@Test(expected=POException.class)
	public void testGetAllPurchaseRequisition() {
		try {
			List<PurchaseRequisition> purchaseRequisitionList = null;
			when(purchaseRequisitionService.getAllPurchaseRequisition()).thenReturn(purchaseRequisitionList);
			assertNull(purchaseRequisitionList);
			purchaseRequisitionList = purchaseRequisitionService.getAllPurchaseRequisition();
			assertNotNull(purchaseRequisitionList);
			exception.expectMessage("Failed JUNIT_FIND_Record");
		} catch (Exception ex) {
	       assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}

	/**
	 * Test get all active purchase requisition.
	 */
	@Test(expected=POException.class)
	public void testGetAllActivePurchaseRequisition() {
		try {
			List<PurchaseRequisition> purchaseRequisitionList = null;
			when(purchaseRequisitionService.getAllActivePurchaseRequisition()).thenReturn(purchaseRequisitionList);
			assertNull(purchaseRequisitionList);
			purchaseRequisitionList = purchaseRequisitionService.getAllActivePurchaseRequisition();
			assertNotNull(purchaseRequisitionList);
			exception.expectMessage("Failed JUNIT_FIND_Record");
		} catch (Exception ex) {
	        assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}
}