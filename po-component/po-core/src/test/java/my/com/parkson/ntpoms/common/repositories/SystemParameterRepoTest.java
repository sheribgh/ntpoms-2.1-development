/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.repositories;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import my.com.parkson.ntpoms.common.entities.SystemParameter;

/**
 * The Class SystemParameterRepoTest.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class SystemParameterRepoTest {

	/** The test entity manager. */
	@Autowired
	TestEntityManager testEntityManager;

	/** The system parameter repository. */
	@Autowired
	SystemParameterRepository systemParameterRepository;

	/** The prop name. */
	private String propName = "JUNIT propName";
	
	/** The prop value. */
	private String propValue = "JUNIT propValue";
	

	/**
	 * Test find by name.
	 */
	@Test
	public void testFindByName() {
		SystemParameter SystemParameter = new SystemParameter();
		SystemParameter.setPropName(propName);
		SystemParameter.setPropValue(propValue);
		testEntityManager.persist(SystemParameter);

		SystemParameter = systemParameterRepository.findByCode(SystemParameter.getCode());
		assertNotNull(SystemParameter.getCode());
		assertTrue(SystemParameter.getPropValue().equals(propValue));

		SystemParameter = systemParameterRepository.findByPropName(propName);
		assertNotNull(SystemParameter.getPropName());

	
		List<SystemParameter> systemParameterList = systemParameterRepository.findAllByIsActiveOrderByLastModifiedOnDesc(true);
		assertEquals(1, systemParameterList.size());
	}
}