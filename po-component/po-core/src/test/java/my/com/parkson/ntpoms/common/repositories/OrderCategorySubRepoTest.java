/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.repositories;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import my.com.parkson.ntpoms.common.entities.OrderCategory;
import my.com.parkson.ntpoms.common.entities.OrderSubCategory;


/**
 * The Class OrderCategorySubRepoTest.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class OrderCategorySubRepoTest {

	/** The test entity manager. */
	@Autowired
	TestEntityManager testEntityManager;

	/** The order category sub repository. */
	@Autowired
	OrderSubCategoryRepository orderCategorySubRepository;

	/** The order category repository. */
	@Autowired
	OrderCategoryRepository orderCategoryRepository;

	/** The code. */
	private String code = "JUNIT";
	
	/** The desc. */
	private String desc = "JUNIT Description";

	/** The order catcode. */
	private String orderCatcode = "JUNIT orderCatcode";

	/**
	 * Test find by name.
	 */
	@Test
	public void testFindByName() {
		OrderSubCategory OrderSubCategory = new OrderSubCategory();
		OrderSubCategory.setCode(code);
		OrderSubCategory.setDesc(desc);
		testEntityManager.persist(OrderSubCategory);

		OrderSubCategory = orderCategorySubRepository.findByCode(code);
		assertNotNull(OrderSubCategory.getCode());
		assertTrue(OrderSubCategory.getDesc().equals(desc));

		List<OrderSubCategory> OrderSubCategoryList = orderCategorySubRepository.findAllByOrderByLastModifiedOnDesc();
		assertEquals(1, OrderSubCategoryList.size());
		OrderSubCategoryList = orderCategorySubRepository.findAllByIsActiveOrderByLastModifiedOnDesc(true);
		assertEquals(1, OrderSubCategoryList.size());
		OrderSubCategoryList = orderCategorySubRepository.findAllByOrderCategoryCode(orderCatcode);
		assertEquals(1, OrderSubCategoryList.size());
		OrderSubCategoryList = orderCategorySubRepository.findAllActiveByOrderCategoryCode(orderCatcode);
		assertEquals(1, OrderSubCategoryList.size());
	}

}
