/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.sql.SQLException;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.test.context.junit4.SpringRunner;

import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.entities.Store;
import my.com.parkson.ntpoms.common.repositories.StoreRepository;
import my.com.parkson.ntpoms.common.services.impl.StoreServiceImpl;

/**
 * The Class StoreServiceTest.
 */
@RunWith(SpringRunner.class)
public class StoreServiceTest {

	/** The exception. */
	@Rule
	public final ExpectedException exception = ExpectedException.none();

	/** The test entity manager. */
	@MockBean
	private TestEntityManager testEntityManager;

	/** The store repository. */
	@MockBean
	private StoreRepository storeRepository;

	/** The store service. */
	private StoreService storeService;

	/** The code. */
	private String code = "JUNIT";	
	
	/** The name. */
	private String name = "JUNIT Name";
	
	/** The company code. */
	private String companyCode = "JUNIT companyCode";

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		Store store = new Store();
		store.setCode(code);
		store.setName(name);
		testEntityManager.persist(store);
		when(storeRepository.findByCode(code)).thenReturn(store);
		storeService = new StoreServiceImpl();
	}

	/**
	 * Test get all store.
	 */
	@Test(expected=POException.class)
	public void testGetAllStore() {
		try {
			List<Store> storeList  = null;
			when(storeService.getAllStore()).thenReturn(storeList);
			assertNull(storeList);
			storeList  = storeService.getAllStore();
			assertNotNull(storeList);
			exception.expectMessage("Failed JUNIT_FIND_All");
		} catch (Exception ex) {
	        assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}

	/**
	 * Test get all active store.
	 */
	@Test(expected=POException.class)
	public void testGetAllActiveStore() {
		try {
			List<Store> storeList  = null;
			when(storeService.getAllActiveStore()).thenReturn(storeList);
			assertNull(storeList);
			storeList  = storeService.getAllActiveStore();
			assertNotNull(storeList);
			exception.expectMessage("Failed JUNIT_FIND_All");
		} catch (Exception ex) {
	      assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}

	/**
	 * Test get store by code.
	 */
	@Test(expected=POException.class)
	public void testGetStoreByCode() {
		try {
			Store store  = null;
			when(storeService.getStoreByCode(code)).thenReturn(store);
			assertNull(store);
			store  = storeService.getStoreByCode(code);
			assertNotNull(store);
			exception.expectMessage("Failed JUNIT_FIND_All");
		} catch (Exception ex) {
	       assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}

	/**
	 * Test create store.
	 */
	@Test
	public void testCreateStore() {
		Store store = new Store();
		store.setCode(code);
		store.setName(name);
		doNothing().when(storeService).createStore(store);
		assertNotNull(store);
	}

	/**
	 * Test get all active store by company code.
	 */
	@Test(expected=POException.class)
	public void testGetAllActiveStoreByCompanyCode() {
		try {
			List<Store> storeList  = null;
			when(storeService.getAllActiveStoreByCompanyCode(companyCode)).thenReturn(storeList);
			assertNull(storeList);
			storeList = storeService.getAllActiveStoreByCompanyCode(companyCode);
			assertNotNull(storeList);
			exception.expectMessage("Failed JUNIT_FIND_All");
		} catch (Exception ex) {
	       assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
	    }
	}
}