/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
/* Name : MF14_M_DEPTMAST_Service                                      */
/* Usage: Code for the department service                        */
/* Description: This code creates an interface for department that will */
/*              used for connecting to data. This code*/
/*              interacts with the table <mf14_m_deptmast>.        */
/*************************************************************/

package my.com.parkson.ntpoms.common.repositories;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import my.com.parkson.ntpoms.common.entities.Department;

/**
 * The Class DepartmentRepositoryTest.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class DepartmentRepositoryTest {

	/** The test entity manager. */
	@Autowired
	TestEntityManager testEntityManager;

	/** The department repository. */
	@Autowired
	DepartmentRepository departmentRepository;

	/** The name. */
	private String name = "JUNIT Name";
	
	/** The code. */
	private String code = "JUNIT";

	/**
	 * Test find by name.
	 */
	@Test
	public void testFindByName() {
		Department department = new Department();
		department.setName(name);
		department.setCode(code);
		testEntityManager.persist(department);

		department = departmentRepository.findByCode(code);
		assertNotNull(department.getCode());
		assertTrue(department.getName().equals(name));

		List<Department> departmentList = departmentRepository.findAllByOrderByLastModifiedOnDesc();
		assertEquals(1, departmentList.size());
		departmentList = departmentRepository.findAllByIsActiveOrderByLastModifiedOnDesc(true);
		assertEquals(1, departmentList.size());
		departmentList = departmentRepository.findAll();
		assertEquals(1, departmentList.size());
	}
}