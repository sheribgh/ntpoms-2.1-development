/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.scheduler;

import static my.com.parkson.ntpoms.common.MessageCodes.FAILED_PARSETOINT_EXCEPTION;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.LogMessages;
import my.com.parkson.ntpoms.common.entities.SystemParameter;
import my.com.parkson.ntpoms.common.services.AccessLogService;
import my.com.parkson.ntpoms.common.services.MessageService;
import my.com.parkson.ntpoms.common.services.SystemParameterService;
import my.com.parkson.ntpoms.main.entities.PurchaseOrder;
import my.com.parkson.ntpoms.main.services.PurchaseOrderService;

/**
 * The Class PurchaseOrderExpirationJob.
 */
public class PurchaseOrderExpirationJob implements Job {

	private static Logger LOGGER = Logger.getLogger(PurchaseOrderExpirationJob.class);
	/** The messages. */
	@Autowired
	private MessageService messages;

	/** The system parameter service. */
	@Autowired
	private SystemParameterService systemParameterService;

	/** The purchase order service. */
	@Autowired
	private PurchaseOrderService purchaseOrderService;

	/**
	 * Expire purchase order.
	 */
	public void expirePurchaseOrder() {
		SystemParameter expirationParameter = systemParameterService
				.getSystemParameterByPropName("MF_EXPIRATION_LENGTH");
		String expirationValue = expirationParameter.getPropValue();
		int expirationLength = 7;

		try {
			expirationLength = Integer.parseInt(expirationValue);
		} catch (POException e) {
			LOGGER.error(e);
		}

		List<PurchaseOrder> purchaseOrderList = purchaseOrderService.getAllPurchaseOrder();
		for (PurchaseOrder purchaseOrder : purchaseOrderList) {
			Date expiration = null;
			Date lastActionDate = null;
			if (purchaseOrder.getStatus() == "Issued" || purchaseOrder.getStatus().equalsIgnoreCase("Issued")) {
				lastActionDate = purchaseOrder.getIssuedOn();
			} else if (purchaseOrder.getStatus() == "Verified"
					|| purchaseOrder.getStatus().equalsIgnoreCase("Verified")) {
				lastActionDate = purchaseOrder.getVerifiedOn();
			}

			if (lastActionDate != null) {
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(lastActionDate);
				calendar.add(Calendar.DATE, expirationLength);
				expiration = calendar.getTime();
			}

			if (expiration != null && expiration.after(new Date())) {
				purchaseOrder.setExpiryStatus(true);
				purchaseOrder.setExpireDate(new Date());
				purchaseOrder.setExpiryDays(daysBetween(expiration, new Date()));
				purchaseOrderService.createPurchaseOrder(purchaseOrder);
			}
		}
	}

	/**
	 * Days between.
	 *
	 * @param expiration
	 *            the expiration
	 * @param today
	 *            the today
	 * @return the int
	 */
	public int daysBetween(Date expiration, Date today) {
		return (int) ((today.getTime() - expiration.getTime()) / (1000 * 60 * 60 * 24));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.quartz.Job#execute(org.quartz.JobExecutionContext)
	 */
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		expirePurchaseOrder();
	}
}