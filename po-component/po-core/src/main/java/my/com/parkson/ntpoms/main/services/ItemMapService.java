package my.com.parkson.ntpoms.main.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import my.com.parkson.ntpoms.main.entities.ItemMap;

public interface ItemMapService {
	
	public List<ItemMap> getAllItemMap();	
	public List<ItemMap> getAllItemMapByPurchaseOrderNo(String poNo);	
	public Page<ItemMap> getAllPageableItemMap(Specification<ItemMap> s, Pageable pageRequest);
	public ItemMap getItemMapByCode(int code);	
	public void createItemMap(ItemMap itemMap);
	public void removeItemMap(ItemMap itemMap);
	public Page<ItemMap> getAllPageableItemMapOrdered(Pageable pageRequest);
	public ItemMap getLatestItemMapCode();
}