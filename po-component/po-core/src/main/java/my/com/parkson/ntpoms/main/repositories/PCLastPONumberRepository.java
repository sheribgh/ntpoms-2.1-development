package my.com.parkson.ntpoms.main.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import my.com.parkson.ntpoms.main.entities.ProfitCenterLastPONumber;


public interface PCLastPONumberRepository extends JpaRepository<ProfitCenterLastPONumber, Integer>, JpaSpecificationExecutor<ProfitCenterLastPONumber> {

	public ProfitCenterLastPONumber findAllByPcCodeAndYear(String pcCode, String year);
}
