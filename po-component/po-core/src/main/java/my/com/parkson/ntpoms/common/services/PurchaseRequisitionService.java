/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import my.com.parkson.ntpoms.common.entities.PurchaseRequisition;

/**
 * The Interface PurchaseRequisitionService.
 */
public interface PurchaseRequisitionService {

	/**
	 * Creates the purchase requisition.
	 *
	 * @param purchaseRequisition the purchase requisition
	 */
	public void createPurchaseRequisition(PurchaseRequisition purchaseRequisition);

	/**
	 * Gets the all pageable purchase requisition.
	 *
	 * @param pageRequest the page request
	 * @return the all pageable purchase requisition
	 */
	public Page<PurchaseRequisition> getAllPageablePurchaseRequisition(Pageable pageRequest);
	
	
	
	
	
	public long getCount();

	/**
	 * Gets the all pageable purchase requisition.
	 *
	 * @param s the s
	 * @param pageRequest the page request
	 * @return the all pageable purchase requisition
	 */
	public Page<PurchaseRequisition> getAllPageablePurchaseRequisition(Specification<PurchaseRequisition> s,
			Pageable pageRequest);

	
	/**
	 * Gets the all pageable purchase requisition.
	 *
	 * @param pageRequest the page request
	 * @return the all pageable purchase requisition
	 */
	public Page<PurchaseRequisition> getAllPageablePurchaseRequisitionOrdered(Pageable pageRequest);

	/**
	 * Gets the all pageable purchase requisition.
	 *
	 * @param s the s
	 * @param pageRequest the page request
	 * @return the all pageable purchase requisition
	 */
	public Page<PurchaseRequisition> getAllPageablePurchaseRequisitionOrdered(Specification<PurchaseRequisition> s,
			Pageable pageRequest);

	/**
	 * Gets the purchase requisition by type code.
	 *
	 * @param typeCode the type code
	 * @return the purchase requisition by type code
	 */
	public PurchaseRequisition getPurchaseRequisitionByTypeCode(String typeCode);

	/**
	 * Gets the all purchase requisition.
	 *
	 * @return the all purchase requisition
	 */
	public List<PurchaseRequisition> getAllPurchaseRequisition();

	/**
	 * Gets the all active purchase requisition.
	 *
	 * @return the all active purchase requisition
	 */
	public List<PurchaseRequisition> getAllActivePurchaseRequisition();
}