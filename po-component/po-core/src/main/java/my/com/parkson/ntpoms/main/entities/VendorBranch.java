package my.com.parkson.ntpoms.main.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "mf01_m_vendorbranch")
@Data
@NoArgsConstructor
public class VendorBranch implements Serializable {

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	VendorBranchId vendorBranchId;
	
	/*@Id
	@Column(name = "mf01_vendorCode")
	private String code;

	@Id
	@Column(name = "mf01_vendorBranchCode")
	private String branchCode;

	@Id
	@Column(name="\"mf11mcompmast_mf01_compcode\"")
	private String companyCode;*/

	@Column(name = "\"mf01_vendorname\"")
	private String name;

	@Column(name = "mf01_branchdescription")
	private String branchDescription;

	@Column(name = "mf01_vendoraddressline1")
	private String addressLine1;

	@Column(name = "mf01_vendoraddressline2")
	private String addressLine2;

	@Column(name = "mf01_vendoraddressline3")
	private String addressLine3;

	@Column(name = "mf01_postcode")
	private String postCode;

	@Column(name = "mf01_postcodearea")
	private String postCodeArea;

	@Column(name = "mf01_lastcontactpersonname")
	private String lastContactPersonName;

	@Column(name = "mf01_lastcontactpersoncontactnumber")
	private String lastContactPersonContactNumber;

	@Column(name = "mf01_lastcontactpersonemailaddress")
	private String lastContactPersonEmailAddress;

	@Column(name = "mf01_lastpocurrency")
	private String lastPOCurrency;

	@Column(name = "mf01_createdby")
	private String createdBy;

	@Column(name = "mf01_createdon")
	private Date createdOn;

	@Column(name = "mf01_lastmodifiedby")
	private String lastModifiedBy;

	@Column(name = "mf01_lastmodifiedon")
	private Date lastModifiedOn;

	@Column(name = "mf01_deactivatedby")
	private String deactivatedBy;

	@Column(name = "mf01_deactivatedon")
	private Date deactivatedOn;

	@Column(name = "mf01_reactivatedby")
	private String reactivatedBy;

	@Column(name = "mf01_reactivatedon")
	private Date reactivatedOn;

	@Column(name = "\"mf01_isactive\"")
	private boolean isActive;
	
	@Column(name = "mf02_creditterms_mf02_lastcredittermscode")
	private String creditTermsCode;
	

	public VendorBranchId getVendorBranchId() {
		return vendorBranchId;
	}

	public void setVendorBranchId(VendorBranchId vendorBranchId) {
		this.vendorBranchId = vendorBranchId;
	}

	public String getCreditTermsCode() {
		return creditTermsCode;
	}

	public void setCreditTermsCode(String creditTermsCode) {
		this.creditTermsCode = creditTermsCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBranchDescription() {
		return branchDescription;
	}

	public void setBranchDescription(String branchDescription) {
		this.branchDescription = branchDescription;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getAddressLine3() {
		return addressLine3;
	}

	public void setAddressLine3(String addressLine3) {
		this.addressLine3 = addressLine3;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	public String getPostCodeArea() {
		return postCodeArea;
	}

	public void setPostCodeArea(String postCodeArea) {
		this.postCodeArea = postCodeArea;
	}

	public String getLastContactPersonName() {
		return lastContactPersonName;
	}

	public void setLastContactPersonName(String lastContactPersonName) {
		this.lastContactPersonName = lastContactPersonName;
	}

	public String getLastContactPersonContactNumber() {
		return lastContactPersonContactNumber;
	}

	public void setLastContactPersonContactNumber(String lastContactPersonContactNumber) {
		this.lastContactPersonContactNumber = lastContactPersonContactNumber;
	}

	public String getLastContactPersonEmailAddress() {
		return lastContactPersonEmailAddress;
	}

	public void setLastContactPersonEmailAddress(String lastContactPersonEmailAddress) {
		this.lastContactPersonEmailAddress = lastContactPersonEmailAddress;
	}

	public String getLastPOCurrency() {
		return lastPOCurrency;
	}

	public void setLastPOCurrency(String lastPOCurrency) {
		this.lastPOCurrency = lastPOCurrency;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public Date getLastModifiedOn() {
		return lastModifiedOn;
	}

	public void setLastModifiedOn(Date lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

	public String getDeactivatedBy() {
		return deactivatedBy;
	}

	public void setDeactivatedBy(String deactivatedBy) {
		this.deactivatedBy = deactivatedBy;
	}

	public Date getDeactivatedOn() {
		return deactivatedOn;
	}

	public void setDeactivatedOn(Date deactivatedOn) {
		this.deactivatedOn = deactivatedOn;
	}

	public String getReactivatedBy() {
		return reactivatedBy;
	}

	public void setReactivatedBy(String reactivatedBy) {
		this.reactivatedBy = reactivatedBy;
	}

	public Date getReactivatedOn() {
		return reactivatedOn;
	}

	public void setReactivatedOn(Date reactivatedOn) {
		this.reactivatedOn = reactivatedOn;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

/*	public CreditTerms getCreditTermsDetails() {
		return creditTermsDetails;
	}

	public void setCreditTermsDetails(CreditTerms creditTermsDetails) {
		this.creditTermsDetails = creditTermsDetails;
	}*/

/*	@Override
	public String toString() {		
		StringBuilder sb = new StringBuilder();
		sb.append(code).append(" - ").append(name).append("-").append(branchCode);
		return sb.toString();
	}*/

	/*public static class VendorBranchPk implements Serializable {

		private static final long serialVersionUID = 1L;

		protected String code;

		protected String branchCode;	

		protected String companyCode;	

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public String getBranchCode() {
			return branchCode;
		}

		public void setBranchCode(String branchCode) {
			this.branchCode = branchCode;
		}

		public String getCompanyCode() {
			return companyCode;
		}

		public void setCompanyCode(String companyCode) {
			this.companyCode = companyCode;
		}

		public VendorBranchPk(){

		}

		public VendorBranchPk(String code,String branchCode,String companyCode) {
			this.code = code;
			this.branchCode = branchCode;
			this.companyCode = companyCode;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((branchCode == null) ? 0 : branchCode.hashCode());
			result = prime * result + ((code == null) ? 0 : code.hashCode());
			result = prime * result + ((companyCode == null) ? 0 : companyCode.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			VendorBranchPk other = (VendorBranchPk) obj;
			if (branchCode == null) {
				if (other.branchCode != null)
					return false;
			} else if (!branchCode.equals(other.branchCode))
				return false;
			if (code == null) {
				if (other.code != null)
					return false;
			} else if (!code.equals(other.code))
				return false;
			if (companyCode == null) {
				if (other.companyCode != null)
					return false;
			} else if (!companyCode.equals(other.companyCode))
				return false;

			return true;
		}		
	}		*/
}