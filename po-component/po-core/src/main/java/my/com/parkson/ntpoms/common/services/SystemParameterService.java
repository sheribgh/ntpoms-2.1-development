/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import my.com.parkson.ntpoms.common.entities.SystemParameter;

/**
 * The Interface SystemParameterService.
 */
public interface SystemParameterService {

	/**
	 * Gets the all system parameter.
	 *
	 * @return the all system parameter
	 */
	public List<SystemParameter> getAllSystemParameter();
	
	
	
	
	public long getCount();

	/**
	 * Gets the all pageable system parameter.
	 *
	 * @param pageRequest the page request
	 * @return the all pageable system parameter
	 */
	public Page<SystemParameter> getAllPageableSystemParameter(Pageable pageRequest);

	/**
	 * Gets the all pageable system parameter.
	 *
	 * @param s the s
	 * @param pageRequest the page request
	 * @return the all pageable system parameter
	 */
	public Page<SystemParameter> getAllPageableSystemParameter(Specification<SystemParameter> s, Pageable pageRequest);

	/**
	 * Gets the all pageable system parameter.
	 *
	 * @param pageRequest the page request
	 * @return the all pageable system parameter
	 */
	public Page<SystemParameter> getAllPageableSystemParameterOrdered(Pageable pageRequest);

	/**
	 * Gets the all pageable system parameter.
	 *
	 * @param s the s
	 * @param pageRequest the page request
	 * @return the all pageable system parameter
	 */
	public Page<SystemParameter> getAllPageableSystemParameterOrdered(Specification<SystemParameter> s, Pageable pageRequest);

	/**
	 * Gets the all active system parameter.
	 *
	 * @return the all active system parameter
	 */
	public List<SystemParameter> getAllActiveSystemParameter();

	/**
	 * Gets the system parameter by code.
	 *
	 * @param code the code
	 * @return the system parameter by code
	 */
	public SystemParameter getSystemParameterByCode(int code);

	/**
	 * Creates the system parameter.
	 *
	 * @param systemParameter the system parameter
	 */
	public void createSystemParameter(SystemParameter systemParameter);

	/**
	 * Gets the system parameter by prop name.
	 *
	 * @param mf07_propName the mf 07 prop name
	 * @return the system parameter by prop name
	 */
	public SystemParameter getSystemParameterByPropName(String mf07_propName);
	
	/**
	 * Gets the system parameter by prop name.
	 *
	 * @param mf07_propName the mf 07 prop name
	 * @return the system parameter by prop name
	 */
	public SystemParameter getSystemParameterByPropValue(String propValue);
	
	/**
	 * Gets the system parameter by prop name.
	 *
	 * @param mf07_propName the mf 07 prop name
	 * @return the system parameter by prop name
	 */
	public String getSystemParameterValueByPropName(String propName);
}