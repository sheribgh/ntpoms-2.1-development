/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.model;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class Menu.
 */
public class Menu {
	
	/** The menu item. */
	public String menuItem;
	
	/** The menu url. */
	public String menuUrl;
	
	/** The name. */
	public String name;
	
	/** The list menu. */
	public List<Menu> listMenu = new ArrayList<>();

	/**
	 * Gets the menu item.
	 *
	 * @return the menu item
	 */
	public String getMenuItem() {
		return menuItem;
	}

	/**
	 * Sets the menu item.
	 *
	 * @param menuItem the new menu item
	 */
	public void setMenuItem(String menuItem) {
		this.menuItem = menuItem;
	}
	
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the menu url.
	 *
	 * @return the menu url
	 */
	public String getMenuUrl() {
		return menuUrl;
	}

	/**
	 * Sets the menu url.
	 *
	 * @param menuUrl the new menu url
	 */
	public void setMenuUrl(String menuUrl) {
		this.menuUrl = menuUrl;
	}

	/**
	 * Gets the list menu.
	 *
	 * @return the list menu
	 */
	public List<Menu> getListMenu() {
		return listMenu;
	}

	/**
	 * Sets the list menu.
	 *
	 * @param listMenu the new list menu
	 */
	public void setListMenu(List<Menu> listMenu) {
		this.listMenu = listMenu;
	}
}