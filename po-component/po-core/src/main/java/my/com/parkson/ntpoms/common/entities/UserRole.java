/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The Class PurchaseOrderUser.
 */
@Entity
@Table(name="user_role")
@Data
@NoArgsConstructor
public class UserRole implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	@Id
	@Column(name = "user_id")
	private String user;
	
	/** The emp name. */
	@Column(name = "role_id")
	private int roleId;

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}




}