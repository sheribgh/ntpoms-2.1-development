/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services.impl;

import static my.com.parkson.ntpoms.common.MessageCodes.FAILED_FETCH_RECORDS_EXCEPTION;
import static my.com.parkson.ntpoms.common.MessageCodes.FAILED_PERSIST_EXCEPTION;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.LogMessages;
import my.com.parkson.ntpoms.common.entities.SystemParameter;
import my.com.parkson.ntpoms.common.repositories.SystemParameterRepository;
import my.com.parkson.ntpoms.common.services.SystemParameterService;

/**
 * The Class SystemParameterServiceImpl.
 */
@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class SystemParameterServiceImpl implements SystemParameterService {

	private static final Logger LOGGER = Logger.getLogger(SystemParameterServiceImpl.class);

	/** The system parameter repository. */
	@Autowired
	private SystemParameterRepository systemParameterRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.SystemParameterService#
	 * getAllActiveSystemParameter()
	 */
	@Override
	public List<SystemParameter> getAllActiveSystemParameter() {
		List<SystemParameter> systemParameterList = new ArrayList<>();
		try {
			systemParameterList = systemParameterRepository.findAllByIsActiveOrderByLastModifiedOnDesc(true);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return systemParameterList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.SystemParameterService#
	 * getSystemParameterByCode(int)
	 */
	@Override
	public SystemParameter getSystemParameterByCode(int code) {
		try {
			return systemParameterRepository.findByCode(code);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.SystemParameterService#
	 * createSystemParameter(my.com.parkson.ntpoms.common.entities.
	 * SystemParameter)
	 */
	@Override
	public void createSystemParameter(SystemParameter systemParameter) {
		try {
			systemParameterRepository.save(systemParameter);
		} catch (POException e) {
			LOGGER.error(e);
		}
	}

	@Override
	public long getCount() {
		return systemParameterRepository.count();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.SystemParameterService#
	 * getSystemParameterByPropName(java.lang.String)
	 */
	@Override
	public SystemParameter getSystemParameterByPropName(String propName) {
		try {
			return systemParameterRepository.findByPropName(propName);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.SystemParameterService#
	 * getAllPageableSystemParameter(org.springframework.data.domain.Pageable)
	 */
	@Override
	public Page<SystemParameter> getAllPageableSystemParameter(Pageable pageRequest) {
		return systemParameterRepository.findAll(pageRequest);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.SystemParameterService#
	 * getAllSystemParameter()
	 */
	@Override
	public List<SystemParameter> getAllSystemParameter() {
		return systemParameterRepository.findAll();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.SystemParameterService#
	 * getAllPageableSystemParameter(org.springframework.data.jpa.domain.
	 * Specification, org.springframework.data.domain.Pageable)
	 */
	@Override
	public Page<SystemParameter> getAllPageableSystemParameter(Specification<SystemParameter> s, Pageable pageRequest) {
		return systemParameterRepository.findAll(s, pageRequest);
	}

	@Override
	public Page<SystemParameter> getAllPageableSystemParameterOrdered(Pageable pageRequest) {
		return systemParameterRepository.findAllByOrderByLastModifiedOnDesc(pageRequest);
	}

	@Override
	public Page<SystemParameter> getAllPageableSystemParameterOrdered(Specification<SystemParameter> s,
			Pageable pageRequest) {
		return systemParameterRepository.findAllByOrderByLastModifiedOnDesc(s, pageRequest);
	}

	@Override
	public String getSystemParameterValueByPropName(String propName) {
		try {
			return systemParameterRepository.findValueByPropName(propName);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	@Override
	public SystemParameter getSystemParameterByPropValue(String propValue) {
		return systemParameterRepository.findByPropValue(propValue);
	}
}