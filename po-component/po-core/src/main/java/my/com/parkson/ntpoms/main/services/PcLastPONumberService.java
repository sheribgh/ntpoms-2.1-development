package my.com.parkson.ntpoms.main.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import my.com.parkson.ntpoms.main.entities.ProfitCenterLastPONumber;

public interface PcLastPONumberService {

	public void createProfitCenterLastPONumber(ProfitCenterLastPONumber profitCenterLastPONumber);
	public Page<ProfitCenterLastPONumber> getAllPagablePc(Specification<ProfitCenterLastPONumber> s,Pageable pageRequest);
	public Page<ProfitCenterLastPONumber> getAllPagablePc(Pageable pageRequest);
	public ProfitCenterLastPONumber getPcLastPONumberByPcCodeAndYear(String poNo, String year);
}