/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.query.Param;

import my.com.parkson.ntpoms.common.entities.Company;

/**
 * The Interface CompanyService.
 */
public interface CompanyService {

	/**
	 * Gets the all company.
	 *
	 * @return the all company
	 */
	public List<Company> getAllCompany();
	
	
	public long getCountActive();
	
	
	public long getCount();

	/**
	 * Gets the all pageable company.
	 *
	 * @param pageRequest the page request
	 * @return the all pageable company
	 */
	public Page<Company> getAllPageableCompany(Pageable pageRequest);

	/**
	 * Gets the all pageable company.
	 *
	 * @param s the s
	 * @param pageRequest the page request
	 * @return the all pageable company
	 */
	public Page<Company> getAllPageableCompany(Specification<Company> s, Pageable pageRequest);

	/**
	 * Gets the all pageable company.
	 *
	 * @param pageRequest the page request
	 * @return the all pageable company
	 */
	public Page<Company> getAllPageableCompanyOrdered(Pageable pageRequest);
	
	/**
	 * Gets the all pageable company.
	 *
	 * @param s the s
	 * @param pageRequest the page request
	 * @return the all pageable company
	 */
	public Page<Company> getAllPageableCompanyOrdered(Specification<Company> s, Pageable pageRequest);

	/**
	 * Gets the all active company.
	 *
	 * @return the all active company
	 */
	public List<Company> getAllActiveCompany();

	/**
	 * Gets the company by code.
	 *
	 * @param code the code
	 * @return the company by code
	 */
	public Company getCompanyByCode(String code);

	/**
	 * Gets the company by code HRIS.
	 *
	 * @param codeHRIS the code HRIS
	 * @return the company by code HRIS
	 */
	public Company getCompanyByCodeHRIS(String codeHRIS);

	/**
	 * Gets the by search code.
	 *
	 * @param searchval the searchval
	 * @return the by search code
	 */
	public List<Company> getBySearchCode(@Param("searchval") String searchval);

	/**
	 * Creates the company.
	 *
	 * @param company the company
	 */
	public void createCompany(Company company);

	/**
	 * Find by search code.
	 *
	 * @param code the code
	 * @return the list
	 */
	public List<Company> findBySearchCode(String code);
}