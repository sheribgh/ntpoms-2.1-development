/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import my.com.parkson.ntpoms.common.entities.OrderCategory;

/**
 * The Interface OrderCategoryService.
 */
public interface OrderCategoryService {

	/**
	 * Gets the all order category.
	 *
	 * @return the all order category
	 */
	public List<OrderCategory> getAllOrderCategory();
	
	
	
	
	
	public long getCount();

	/**
	 * Gets the all pageable order category.
	 *
	 * @param pageRequest the page request
	 * @return the all pageable order category
	 */
	public Page<OrderCategory> getAllPageableOrderCategory(Pageable pageRequest);

	/**
	 * Gets the all pageable order category.
	 *
	 * @param s the s
	 * @param pageRequest the page request
	 * @return the all pageable order category
	 */
	public Page<OrderCategory> getAllPageableOrderCategory(Specification<OrderCategory> s, Pageable pageRequest);

	
	/**
	 * Gets the all pageable order category.
	 *
	 * @param pageRequest the page request
	 * @return the all pageable order category
	 */
	public Page<OrderCategory> getAllPageableOrderCategoryOrdered(Pageable pageRequest);

	/**
	 * Gets the all pageable order category.
	 *
	 * @param s the s
	 * @param pageRequest the page request
	 * @return the all pageable order category
	 */
	public Page<OrderCategory> getAllPageableOrderCategoryOrdered(Specification<OrderCategory> s, Pageable pageRequest);

	/**
	 * Gets the all active order category.
	 *
	 * @return the all active order category
	 */
	public List<OrderCategory> getAllActiveOrderCategory();

	/**
	 * Gets the order category by code.
	 *
	 * @param code the code
	 * @return the order category by code
	 */
	public OrderCategory getOrderCategoryByCode(String code);

	/**
	 * Creates the order category.
	 *
	 * @param orderCategory the order category
	 */
	public void createOrderCategory(OrderCategory orderCategory);
}