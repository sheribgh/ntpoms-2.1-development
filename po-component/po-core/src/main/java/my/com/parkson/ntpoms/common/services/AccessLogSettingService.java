/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import my.com.parkson.ntpoms.common.entities.AccessLogSetting;

/**
 * The Interface AccessLogSettingService.
 */
public interface AccessLogSettingService {

	/**
	 * Gets the all staging log.
	 *
	 * @return the all staging log
	 */
	public List<AccessLogSetting> getAllStagingLog();
	
	/**
	 * Gets the last staging log.
	 *
	 * @return the last staging log
	 */
	public AccessLogSetting getLastStagingLog();	
	
	/**
	 * Creates the staging log.
	 *
	 * @param stagingLog the staging log
	 */
	public void createStagingLog(AccessLogSetting stagingLog);
	
	/**
	 * Gets the all pageable staging log.
	 *
	 * @param pageRequest the page request
	 * @return the all pageable staging log
	 */
	public Page<AccessLogSetting> getAllPageableStagingLog(Pageable pageRequest);
}