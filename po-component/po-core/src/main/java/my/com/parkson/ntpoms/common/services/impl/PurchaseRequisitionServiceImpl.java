/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
/* Name			:	MF15_M_PRMAST_ServiceImpl											*/
/* Usage		:	Creating service layer for Purchase Requisition						*/
/* Description	:	This code creates a connection between Purchase Requisition Screen	*/
/* 					and Purchase Requisition table in database. This code interacts 	*/
/*					with the table MF15_M_PRMAST										*/
/****************************************************************************************/

package my.com.parkson.ntpoms.common.services.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.entities.PurchaseRequisition;
import my.com.parkson.ntpoms.common.repositories.PurchaseRequisitionRepository;
import my.com.parkson.ntpoms.common.services.PurchaseRequisitionService;

/**
 * The Class PurchaseRequisitionServiceImpl.
 */
@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class PurchaseRequisitionServiceImpl implements PurchaseRequisitionService {

	private static final Logger LOGGER = Logger.getLogger(PurchaseRequisitionServiceImpl.class);

	/** The purchase requisition repository. */
	@Autowired
	private PurchaseRequisitionRepository purchaseRequisitionRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.PurchaseRequisitionService#
	 * createPurchaseRequisition(my.com.parkson.ntpoms.common.entities.
	 * PurchaseRequisition)
	 */
	@Override
	public void createPurchaseRequisition(PurchaseRequisition purchaseRequisition) {
		try {
			purchaseRequisitionRepository.save(purchaseRequisition);
		} catch (NonUniqueResultException e) {
			LOGGER.error(e);
		} catch (PersistenceException e) {
			LOGGER.error(e);
		} catch (POException e) {
			LOGGER.error(e);
		}
	}

	@Override
	public long getCount() {
		return purchaseRequisitionRepository.count();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.PurchaseRequisitionService#
	 * getPurchaseRequisitionByTypeCode(java.lang.String)
	 */
	@Override
	public PurchaseRequisition getPurchaseRequisitionByTypeCode(String typeCode) {
		try {
			return purchaseRequisitionRepository.findByTypeCode(typeCode);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.PurchaseRequisitionService#
	 * getAllPurchaseRequisition()
	 */
	@Override
	public List<PurchaseRequisition> getAllPurchaseRequisition() {
		List<PurchaseRequisition> purchaseRequisitionList = new ArrayList<PurchaseRequisition>();
		try {
			purchaseRequisitionList = purchaseRequisitionRepository.findAllByOrderByLastModifiedOnDesc();
		} catch (POException e) {
			LOGGER.error(e);
		}
		return purchaseRequisitionList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.PurchaseRequisitionService#
	 * getAllActivePurchaseRequisition()
	 */
	@Override
	public List<PurchaseRequisition> getAllActivePurchaseRequisition() {
		List<PurchaseRequisition> purchaseRequisitionList = new ArrayList<PurchaseRequisition>();
		try {
			purchaseRequisitionList = purchaseRequisitionRepository.findAllByIsActiveOrderByLastModifiedOnDesc(true);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return purchaseRequisitionList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.PurchaseRequisitionService#
	 * getAllPageablePurchaseRequisition(org.springframework.data.domain.
	 * Pageable)
	 */
	@Override
	public Page<PurchaseRequisition> getAllPageablePurchaseRequisition(Pageable pageRequest) {
		return purchaseRequisitionRepository.findAll(pageRequest);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.PurchaseRequisitionService#
	 * getAllPageablePurchaseRequisition(org.springframework.data.jpa.domain.
	 * Specification, org.springframework.data.domain.Pageable)
	 */
	@Override
	public Page<PurchaseRequisition> getAllPageablePurchaseRequisition(Specification<PurchaseRequisition> s,
			Pageable pageRequest) {
		return purchaseRequisitionRepository.findAll(s, pageRequest);
	}

	@Override
	public Page<PurchaseRequisition> getAllPageablePurchaseRequisitionOrdered(Pageable pageRequest) {
		return purchaseRequisitionRepository.findAllByOrderByLastModifiedOnDesc(pageRequest);
	}

	@Override
	public Page<PurchaseRequisition> getAllPageablePurchaseRequisitionOrdered(Specification<PurchaseRequisition> s,
			Pageable pageRequest) {
		return purchaseRequisitionRepository.findAllByOrderByLastModifiedOnDesc(s, pageRequest);
	}
}