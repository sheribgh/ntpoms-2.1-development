/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
/* Name			:	MF16_M_GSTMAST_ServiceImpl											*/
/* Usage		:	Creating service layer for GST									*/
/* Description	:	This code creates a connection between GST Screen				*/
/* 					and GST table in database. This code interacts with				*/
/* 					the table MF16_M_GSTMAST											*/
/****************************************************************************************/

package my.com.parkson.ntpoms.common.services.impl;

import static my.com.parkson.ntpoms.common.MessageCodes.FAILED_FETCH_RECORDS_EXCEPTION;

import java.util.List;

import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.LogMessages;
import my.com.parkson.ntpoms.common.entities.GoodsServicesTax;
import my.com.parkson.ntpoms.common.repositories.GoodsServicesTaxRepository;
import my.com.parkson.ntpoms.common.services.GoodsServicesTaxService;
import my.com.parkson.ntpoms.common.services.MessageService;

/**
 * The Class GoodsServicesTaxServiceImpl.
 */
@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class GoodsServicesTaxServiceImpl implements GoodsServicesTaxService {
	private static final Logger LOGGER = Logger.getLogger(GoodsServicesTaxServiceImpl.class);

	/** The messages. */
	@Autowired
	private MessageService messages;

	/** The goods services tax repository. */
	@Autowired
	private GoodsServicesTaxRepository goodsServicesTaxRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.GoodsServicesTaxService#
	 * createGoodsServicesTax(my.com.parkson.ntpoms.common.entities.
	 * GoodsServicesTax)
	 */
	@Override
	public void createGoodsServicesTax(GoodsServicesTax goodsServicesTax) {
		try {
			goodsServicesTaxRepository.save(goodsServicesTax);
		} catch (NonUniqueResultException e) {
			LOGGER.error(e);
		} catch (PersistenceException e) {
			LOGGER.error(e);
		} //
		catch (POException e) {
			LOGGER.error(e);
		}
	}

	@Override
	public long getCount() {
		return goodsServicesTaxRepository.count();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.GoodsServicesTaxService#
	 * getGoodsServicesTaxByCode(java.lang.String)
	 */
	@Override
	public GoodsServicesTax getGoodsServicesTaxByCode(String code) {
		try {
			return goodsServicesTaxRepository.findByCode(code);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.GoodsServicesTaxService#
	 * getAllGoodsServicesTax()
	 */
	@Override
	public List<GoodsServicesTax> getAllGoodsServicesTax() {
		try {
			return goodsServicesTaxRepository.findAllByOrderByLastModifiedOnDesc();
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.GoodsServicesTaxService#
	 * getAllActiveGoodsServicesTax()
	 */
	@Override
	public List<GoodsServicesTax> getAllActiveGoodsServicesTax() {
		try {
			return goodsServicesTaxRepository.findAllByIsActiveOrderByLastModifiedOnDesc(true);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.GoodsServicesTaxService#
	 * getAllPagableGoodsServicesTax(org.springframework.data.domain.Pageable)
	 */
	@Override
	public Page<GoodsServicesTax> getAllPagableGoodsServicesTax(Pageable pageRequest) {
		return goodsServicesTaxRepository.findAll(pageRequest);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.GoodsServicesTaxService#
	 * getAllPagableGoodsServicesTax(org.springframework.data.jpa.domain.
	 * Specification, org.springframework.data.domain.Pageable)
	 */
	@Override
	public Page<GoodsServicesTax> getAllPagableGoodsServicesTax(Specification<GoodsServicesTax> s,
			Pageable pageRequest) {
		return goodsServicesTaxRepository.findAll(s, pageRequest);
	}

	@Override
	public Page<GoodsServicesTax> getAllPagableGoodsServicesTaxOrdered(Pageable pageRequest) {
		return goodsServicesTaxRepository.findAllByOrderByLastModifiedOnDesc(pageRequest);
	}

	@Override
	public Page<GoodsServicesTax> getAllPagableGoodsServicesTaxOrdered(Specification<GoodsServicesTax> s,
			Pageable pageRequest) {
		return goodsServicesTaxRepository.findAllByOrderByLastModifiedOnDesc(s, pageRequest);
	}
}