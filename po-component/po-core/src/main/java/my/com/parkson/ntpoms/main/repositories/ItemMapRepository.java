package my.com.parkson.ntpoms.main.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import my.com.parkson.ntpoms.main.entities.ItemMap;
import my.com.parkson.ntpoms.main.entities.PurchaseOrder;

public interface ItemMapRepository extends JpaRepository<ItemMap, String>, JpaSpecificationExecutor<ItemMap> {
	
	
	public List<ItemMap> findAllByPoNo(String poNo);
	public Page<ItemMap> findAll(Pageable pageRequest);
	public Page<ItemMap> findAll(Specification<ItemMap> s, Pageable pageRequest);
	public ItemMap findByCode(int code);
	public ItemMap findTopByOrderByCodeDesc();
	
	
}
