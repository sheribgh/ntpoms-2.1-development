package my.com.parkson.ntpoms.main.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.main.entities.ItemMap;
import my.com.parkson.ntpoms.main.repositories.ItemMapRepository;
import my.com.parkson.ntpoms.main.services.ItemMapService;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class ItemMapServiceImpl implements ItemMapService {

	private static final Logger LOGGER = Logger.getLogger(ItemMapServiceImpl.class);
	@Autowired
	private ItemMapRepository itemMapRepository;

	@Override
	public List<ItemMap> getAllItemMap() {
		List<ItemMap> itemMapList = new ArrayList<>();
		try {
			itemMapList = itemMapRepository.findAll();
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return itemMapList;
	}

	@Override
	public List<ItemMap> getAllItemMapByPurchaseOrderNo(String poNo) {
		List<ItemMap> itemMapList = new ArrayList<>();
		try {
			itemMapList = itemMapRepository.findAllByPoNo(poNo);
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return itemMapList;
	}

	@Override
	public ItemMap getItemMapByCode(int code) {
		try {
			return itemMapRepository.findByCode(code);
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return null;
	}

	@Override
	public void createItemMap(ItemMap itemMap) {
		try {
			itemMapRepository.save(itemMap);
		} catch (Exception e) {
			LOGGER.error(e);
		}
	}

	@Override
	public void removeItemMap(ItemMap itemMap) {
		try {
			itemMapRepository.delete(itemMap);
		} catch (Exception e) {
			LOGGER.error(e);
		}
	}

	@Override
	public Page<ItemMap> getAllPageableItemMap(Specification<ItemMap> s, Pageable pageRequest) {
		return itemMapRepository.findAll(s, pageRequest);
	}

	@Override
	public Page<ItemMap> getAllPageableItemMapOrdered(Pageable pageRequest) {
		return itemMapRepository.findAll(pageRequest);

	}

	@Override
	public ItemMap getLatestItemMapCode() {
		return itemMapRepository.findTopByOrderByCodeDesc();
	}
}