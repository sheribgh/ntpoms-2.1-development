/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import my.com.parkson.ntpoms.common.entities.MasterData;

/**
 * The Interface MasterDataRepository.
 */
public interface MasterDataRepository extends JpaRepository<MasterData, Integer>, JpaSpecificationExecutor<MasterData> {

	/**
	 * Find all by order by last modified on desc.
	 *
	 * @return the list
	 */
	public List<MasterData> findAllByOrderByLastModifiedOnDesc();

	/**
	 * Find all by is active order by last modified on desc.
	 *
	 * @param isActive the is active
	 * @return the list
	 */
	public List<MasterData> findAllByIsActiveOrderByLastModifiedOnDesc(boolean isActive);

	/**
	 * Find by id.
	 *
	 * @param id the id
	 * @return the master data
	 */
	public MasterData findById(int id);

	
	
	public MasterData findByCodeType(String id);
	

	/**
	 * Find all by order by last modified on desc.
	 *
	 * @return the list
	 */
	public Page<MasterData> findAllByOrderByLastModifiedOnDesc(Pageable pageRequest);

	/**
	 * Find all by order by last modified on desc.
	 *
	 * @return the list
	 */
	public Page<MasterData> findAllByOrderByLastModifiedOnDesc(Specification<MasterData> s, Pageable pageRequest);

}
