package my.com.parkson.ntpoms.main.repositories;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

import my.com.parkson.ntpoms.main.entities.ItemInformation;

public interface ItemInformationRepository extends JpaRepository<ItemInformation, Integer> {
	
	public List<ItemInformation> findAll();	
	public ItemInformation findByItemNo(String itemNo);
}
