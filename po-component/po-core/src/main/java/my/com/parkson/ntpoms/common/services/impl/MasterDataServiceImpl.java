/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services.impl;

import static my.com.parkson.ntpoms.common.MessageCodes.FAILED_PERSIST_EXCEPTION;
import static my.com.parkson.ntpoms.common.MessageCodes.FAILED_FETCH_RECORDS_EXCEPTION;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.LogMessages;
import my.com.parkson.ntpoms.common.entities.MasterData;
import my.com.parkson.ntpoms.common.repositories.MasterDataRepository;
import my.com.parkson.ntpoms.common.services.AccessLogService;
import my.com.parkson.ntpoms.common.services.MasterDataService;
import my.com.parkson.ntpoms.common.services.MessageService;

/**
 * The Class MasterDataServiceImpl.
 */
@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class MasterDataServiceImpl implements MasterDataService {

	private static final Logger LOGGER = Logger.getLogger(MasterDataServiceImpl.class);

	/** The messages. */
	@Autowired
	private MessageService messages;

	/** The master data repository. */
	@Autowired
	private MasterDataRepository masterDataRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.com.parkson.ntpoms.common.services.MasterDataService#getAllMasterData(
	 * )
	 */
	@Override
	public List<MasterData> getAllMasterData() {
		List<MasterData> masterDataList = new ArrayList<>();
		try {
			masterDataList = masterDataRepository.findAllByOrderByLastModifiedOnDesc();
		} catch (POException e) {
			LOGGER.error(e);
		}
		return masterDataList;
	}

	@Override
	public long getCount() {
		return masterDataRepository.count();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.MasterDataService#
	 * getAllActiveMasterData()
	 */
	@Override
	public List<MasterData> getAllActiveMasterData() {
		List<MasterData> masterDataList = new ArrayList<>();
		try {
			masterDataList = masterDataRepository.findAllByIsActiveOrderByLastModifiedOnDesc(true);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return masterDataList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.com.parkson.ntpoms.common.services.MasterDataService#getMasterDataById
	 * (int)
	 */
	@Override
	public MasterData getMasterDataById(int id) {
		try {
			return masterDataRepository.findById(id);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.com.parkson.ntpoms.common.services.MasterDataService#createMasterData(
	 * my.com.parkson.ntpoms.common.entities.MasterData)
	 */
	@Override
	public void createMasterData(MasterData masterData) {
		try {
			masterDataRepository.save(masterData);
		} catch (NonUniqueResultException e) {
			LOGGER.error(e);
		} catch (PersistenceException e) {
			LOGGER.error(e);
		} catch (POException e) {
			LOGGER.error(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.MasterDataService#
	 * getAllPageableMasterData(org.springframework.data.domain.Pageable)
	 */
	@Override
	public Page<MasterData> getAllPageableMasterData(Pageable pageRequest) {
		return masterDataRepository.findAll(pageRequest);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.MasterDataService#
	 * getAllPageableMasterData(org.springframework.data.jpa.domain.
	 * Specification, org.springframework.data.domain.Pageable)
	 */
	@Override
	public Page<MasterData> getAllPageableMasterData(Specification<MasterData> s, Pageable pageRequest) {
		return masterDataRepository.findAll(s, pageRequest);
	}

	@Override
	public Page<MasterData> getAllPageableMasterDataOrdered(Pageable pageRequest) {
		return masterDataRepository.findAllByOrderByLastModifiedOnDesc(pageRequest);
	}

	@Override
	public Page<MasterData> getAllPageableMasterDataOrdered(Specification<MasterData> s, Pageable pageRequest) {
		return masterDataRepository.findAllByOrderByLastModifiedOnDesc(s, pageRequest);
	}

	@Override
	public MasterData getMasterDataByCodeType(String codeType) {
		try {
			return masterDataRepository.findByCodeType(codeType);
		} catch (NonUniqueResultException e) {
			LOGGER.error(e);
		} catch (PersistenceException e) {
			LOGGER.error(e);
		} //
		catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}
}