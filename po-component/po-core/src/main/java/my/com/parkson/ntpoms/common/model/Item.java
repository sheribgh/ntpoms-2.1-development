package my.com.parkson.ntpoms.common.model;

public class Item {
	
	private int itemOrder;
	
	private String mappingId;
	
	private String profitCenter;
	
	private String itemNo;
	
	private String particulars;
	
	private String packing;
	
	private double quantity;
	
	private double unitPriceExclGST;
	
	private double gst;
	
	private double unitPriceInclGST;
	
	private double discount;
	
	private double discountAmount;
	
	private double totExclGST;
	
	private double totGST;
	
	private double totInclGST;
	
	private String taxCode;
	
	private double unitGST;
	
	private double overAllDiscount;
	

	public int getItemOrder() {
		return itemOrder;
	}

	public void setItemOrder(int itemOrder) {
		this.itemOrder = itemOrder;
	}

	public String getMappingId() {
		return mappingId;
	}

	public void setMappingId(String mappingId) {
		this.mappingId = mappingId;
	}

	public String getProfitCenter() {
		return profitCenter;
	}

	public void setProfitCenter(String profitCenter) {
		this.profitCenter = profitCenter;
	}

	public String getItemNo() {
		return itemNo;
	}

	public void setItemNo(String itemNo) {
		this.itemNo = itemNo;
	}

	public String getParticulars() {
		return particulars;
	}

	public void setParticulars(String particulars) {
		this.particulars = particulars;
	}

	public String getPacking() {
		return packing;
	}

	public void setPacking(String packing) {
		this.packing = packing;
	}

	public double getQuantity() {
		return quantity;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	public double getUnitPriceExclGST() {
		return unitPriceExclGST;
	}

	public void setUnitPriceExclGST(double unitPriceExclGST) {
		this.unitPriceExclGST = unitPriceExclGST;
	}

	public double getGst() {
		return gst;
	}

	public void setGst(double gst) {
		this.gst = gst;
	}

	public double getUnitPriceInclGST() {
		return unitPriceInclGST;
	}

	public void setUnitPriceInclGST(double unitPriceInclGST) {
		this.unitPriceInclGST = unitPriceInclGST;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public double getTotExclGST() {
		return totExclGST;
	}

	public void setTotExclGST(double totExclGST) {
		this.totExclGST = totExclGST;
	}

	public double getTotGST() {
		return totGST;
	}

	public void setTotGST(double totGST) {
		this.totGST = totGST;
	}

	public double getTotInclGST() {
		return totInclGST;
	}

	public void setTotInclGST(double totInclGST) {
		this.totInclGST = totInclGST;
	}

	public String getTaxCode() {
		return taxCode;
	}

	public void setTaxCode(String taxCode) {
		this.taxCode = taxCode;
	}

	public double getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(double discountAmount) {
		this.discountAmount = discountAmount;
	}


	public double getUnitGST() {
		return unitGST;
	}

	public void setUnitGST(double unitGST) {
		this.unitGST = unitGST;
	}

	public double getOverAllDiscount() {
		return overAllDiscount;
	}

	public void setOverAllDiscount(double overAllDiscount) {
		this.overAllDiscount = overAllDiscount;
	}
	
	

}