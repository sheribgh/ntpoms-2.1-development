/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */

package my.com.parkson.ntpoms.common.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The Class ProfitCenter.
 */
@Entity
@Table(name = "\"MF13_M_PCMAST\"")
@Data
@NoArgsConstructor
public class ProfitCenter implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The code. */
	@Id
	@Column(name = "\"mf13_pcCode\"" , insertable=false, updatable=false)
	private String  code;
	
	/** The abbr name. */
	@Column(name = "\"mf13_pcAbbrName\"")
	private String abbrName;
	
	 /** The name. */
 	@Column(name = "\"mf13_pcName\"")
	 private String name;
	 
	 /** The address line 1. */
 	@Column(name = "\"mf13_addressLine1\"")
	 private String  addressLine1;
	 
	 /** The address line 2. */
 	@Column(name = "\"mf13_addressLine2\"")
	 private String  addressLine2;
	 
	 /** The address line 3. */
 	@Column(name = "\"mf13_addressLine3\"")
	 private String  addressLine3;
	 
	 /** The isactive. */
 	@Column(name = "\"mf13_isActive\"")
	 private boolean isactive;
	 
	 /** The created on. */
 	@Column(name = "\"mf13_createdOn\"")
	 private Date createdOn;
	 
	 /** The created by. */
 	@Column(name = "\"mf13_createdBy\"")
	 private String createdBy;
	 
	 /** The last modified on. */
 	@Column(name = "\"mf13_lastModifiedOn\"")
	 private Date lastModifiedOn;
	 
	 /** The last modified by. */
 	@Column(name = "\"mf13_lastModifiedBy\"")
	 private String lastModifiedBy;
	 
	 /** The deactivated by. */
 	@Column(name = "\"mf13_deactivatedBy\"")
	 private String deactivatedBy;
	 
	 /** The deactivated on. */
 	@Column(name = "\"mf13_deactivatedOn\"")
	 private Date deactivatedOn;
	 
	 /** The reactivated by. */
 	@Column(name = "\"mf13_reactivatedBy\"")
	 private String reactivatedBy;
	 
	 /** The reactivated on. */
 	@Column(name = "\"mf13_reactivatedOn\"")
	 private Date reactivatedOn;
	 
	 /** The head. */
 	@Column(name = "\"mf13_pcHead\"")
	 private String head;
	 
	 /** The view rights. */
 	@Column(name = "\"mf13_pcViewRights\"")
	 private String viewRights;
	 
	 /** The post code. */
 	@Column(name = "\"mf13_postCode\"")
	 private String postCode;
	 
	 /** The post area. */
 	@Column(name = "\"mf13_postArea\"")
	 private String postArea;
	 
	 /** The store type. */
 	@Column(name = "\"mf13_storeType\"")	 
	 private String storeType;
	 
	 /** The start date. */
 	@Column(name = "\"mf13_startDate\"")
	 private Date startDate;
	 
	 /** The end date. */
 	@Column(name = "\"mf13_endDate\"")
	 private Date endDate;

	 /** The company. */
 	@ManyToOne
	 @JoinColumn(name="\"mf11MCompmast_mf11_compCode\"",nullable=false)
	 private Company company;	 
	 
	 /** The store. */
 	@ManyToOne
	 @JoinColumn(name="\"mf02MStrmast_mf02_storeCode\"",nullable=false)
	 private Store store;

	 /** The department. */
 	@ManyToOne
	 @JoinColumn(name="\"mf14MDeptmast_mf14_deptCode\"",nullable=false)
	 private Department department;	 

	 /**
 	 * Gets the code.
 	 *
 	 * @return the code
 	 */
 	public String getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * Gets the abbr name.
	 *
	 * @return the abbr name
	 */
	public String getAbbrName() {
		return abbrName;
	}

	/**
	 * Sets the abbr name.
	 *
	 * @param abbrName the new abbr name
	 */
	public void setAbbrName(String abbrName) {
		this.abbrName = abbrName;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the address line 1.
	 *
	 * @return the address line 1
	 */
	public String getAddressLine1() {
		return addressLine1;
	}

	/**
	 * Sets the address line 1.
	 *
	 * @param addressLine1 the new address line 1
	 */
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	/**
	 * Gets the address line 2.
	 *
	 * @return the address line 2
	 */
	public String getAddressLine2() {
		return addressLine2;
	}

	/**
	 * Sets the address line 2.
	 *
	 * @param addressLine2 the new address line 2
	 */
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	/**
	 * Gets the address line 3.
	 *
	 * @return the address line 3
	 */
	public String getAddressLine3() {
		return addressLine3;
	}

	/**
	 * Sets the address line 3.
	 *
	 * @param addressLine3 the new address line 3
	 */
	public void setAddressLine3(String addressLine3) {
		this.addressLine3 = addressLine3;
	}

	/**
	 * Checks if is isactive.
	 *
	 * @return true, if is isactive
	 */
	public boolean isIsactive() {
		return isactive;
	}

	/**
	 * Sets the isactive.
	 *
	 * @param isactive the new isactive
	 */
	public void setIsactive(boolean isactive) {
		this.isactive = isactive;
	}

	/**
	 * Gets the created on.
	 *
	 * @return the created on
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * Sets the created on.
	 *
	 * @param createdOn the new created on
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * Gets the created by.
	 *
	 * @return the created by
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * Sets the created by.
	 *
	 * @param createdBy the new created by
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Gets the last modified on.
	 *
	 * @return the last modified on
	 */
	public Date getLastModifiedOn() {
		return lastModifiedOn;
	}

	/**
	 * Sets the last modified on.
	 *
	 * @param lastModifiedOn the new last modified on
	 */
	public void setLastModifiedOn(Date lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

	/**
	 * Gets the last modified by.
	 *
	 * @return the last modified by
	 */
	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	/**
	 * Sets the last modified by.
	 *
	 * @param lastModifiedBy the new last modified by
	 */
	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	/**
	 * Gets the deactivated by.
	 *
	 * @return the deactivated by
	 */
	public String getDeactivatedBy() {
		return deactivatedBy;
	}

	/**
	 * Sets the deactivated by.
	 *
	 * @param deactivatedBy the new deactivated by
	 */
	public void setDeactivatedBy(String deactivatedBy) {
		this.deactivatedBy = deactivatedBy;
	}

	/**
	 * Gets the deactivated on.
	 *
	 * @return the deactivated on
	 */
	public Date getDeactivatedOn() {
		return deactivatedOn;
	}

	/**
	 * Sets the deactivated on.
	 *
	 * @param deactivatedOn the new deactivated on
	 */
	public void setDeactivatedOn(Date deactivatedOn) {
		this.deactivatedOn = deactivatedOn;
	}

	/**
	 * Gets the reactivated by.
	 *
	 * @return the reactivated by
	 */
	public String getReactivatedBy() {
		return reactivatedBy;
	}

	/**
	 * Sets the reactivated by.
	 *
	 * @param reactivatedBy the new reactivated by
	 */
	public void setReactivatedBy(String reactivatedBy) {
		this.reactivatedBy = reactivatedBy;
	}

	/**
	 * Gets the reactivated on.
	 *
	 * @return the reactivated on
	 */
	public Date getReactivatedOn() {
		return reactivatedOn;
	}

	/**
	 * Sets the reactivated on.
	 *
	 * @param reactivatedOn the new reactivated on
	 */
	public void setReactivatedOn(Date reactivatedOn) {
		this.reactivatedOn = reactivatedOn;
	}

	/**
	 * Gets the head.
	 *
	 * @return the head
	 */
	public String getHead() {
		return head;
	}

	/**
	 * Sets the head.
	 *
	 * @param head the new head
	 */
	public void setHead(String head) {
		this.head = head;
	}

	/**
	 * Gets the view rights.
	 *
	 * @return the view rights
	 */
	public String getViewRights() {
		return viewRights;
	}

	/**
	 * Sets the view rights.
	 *
	 * @param viewRights the new view rights
	 */
	public void setViewRights(String viewRights) {
		this.viewRights = viewRights;
	}

	/**
	 * Gets the post code.
	 *
	 * @return the post code
	 */
	public String getPostCode() {
		return postCode;
	}

	/**
	 * Sets the post code.
	 *
	 * @param postCode the new post code
	 */
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	/**
	 * Gets the post area.
	 *
	 * @return the post area
	 */
	public String getPostArea() {
		return postArea;
	}

	/**
	 * Sets the post area.
	 *
	 * @param postArea the new post area
	 */
	public void setPostArea(String postArea) {
		this.postArea = postArea;
	}

	/**
	 * Gets the store type.
	 *
	 * @return the store type
	 */
	public String getStoreType() {
		return storeType;
	}

	/**
	 * Sets the store type.
	 *
	 * @param storeType the new store type
	 */
	public void setStoreType(String storeType) {
		this.storeType = storeType;
	}

	/**
	 * Gets the start date.
	 *
	 * @return the start date
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * Sets the start date.
	 *
	 * @param startDate the new start date
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * Gets the end date.
	 *
	 * @return the end date
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * Sets the end date.
	 *
	 * @param endDate the new end date
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	/**
	 * Gets the company.
	 *
	 * @return the company
	 */
	public Company getCompany() {
		return company;
	}

	/**
	 * Sets the company.
	 *
	 * @param company the new company
	 */
	public void setCompany(Company company) {
		this.company = company;
	}

	/**
	 * Gets the store.
	 *
	 * @return the store
	 */
	public Store getStore() {
		return store;
	}

	/**
	 * Sets the store.
	 *
	 * @param store the new store
	 */
	public void setStore(Store store) {
		this.store = store;
	}

	/**
	 * Gets the department.
	 *
	 * @return the department
	 */
	public Department getDepartment() {
		return department;
	}

	/**
	 * Sets the department.
	 *
	 * @param department the new department
	 */
	public void setDepartment(Department department) {
		this.department = department;
	}
	
	@Override
	public String toString () {
		
		return code + "-" + name;
	}


	
}