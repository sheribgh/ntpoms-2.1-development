package my.com.parkson.ntpoms.main.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Data;

@Data
@Embeddable
public class VendorCreditTermsId implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Column(name = "\"mf01_vendorbranch_mf01_vendorcode\"")
	public String vendorCode ;
    @Column(name = "\"mf01_vendorbranch_mf01_vendorbranchcode\"")
    public String vendorBranchCode ;
    @Column(name = "\"mf02_creditterms_mf02_credittermscode\"")
    public String creditTermsCode;
    @Column(name = "\"mf11mcompmast_mf03_compcode\"")
    public String companyCode;
	public String getVendorCode() {
		return vendorCode;
	}
	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}
	public String getVendorBranchCode() {
		return vendorBranchCode;
	}
	public void setVendorBranchCode(String vendorBranchCode) {
		this.vendorBranchCode = vendorBranchCode;
	}
	public String getCreditTermsCode() {
		return creditTermsCode;
	}
	public void setCreditTermsCode(String creditTermsCode) {
		this.creditTermsCode = creditTermsCode;
	}
	public String getCompanyCode() {
		return companyCode;
	}
	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}
	
	public VendorCreditTermsId(){
		System.out.println("Empty Constructor Caalled");
	}
	
	public VendorCreditTermsId(String vendorCode,String vendorBranchCode,String creditTermsCode,String companyCode ){
		this.vendorCode =vendorCode;
		this.vendorBranchCode =vendorBranchCode;
		this.creditTermsCode =creditTermsCode;
		this.companyCode =companyCode;
	}
    
    
    
}
