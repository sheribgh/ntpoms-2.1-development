/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
/* Name			:	MF11_M_COMPMAST_ServiceImpl											*/
/* Usage		:	Creating service layer for Company									*/
/* Description	:	This code creates a connection between Company Screen				*/
/* 					and Company table in database. This code interacts with				*/
/* 					the table MF11_M_COMPMAST											*/
/****************************************************************************************/

package my.com.parkson.ntpoms.common.services.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.entities.Company;
import my.com.parkson.ntpoms.common.repositories.CompanyRepository;
import my.com.parkson.ntpoms.common.services.CompanyService;
import my.com.parkson.ntpoms.common.services.MessageService;

/**
 * The Class CompanyServiceImpl.
 */
@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class CompanyServiceImpl implements CompanyService {

	private static final Logger LOGGER = Logger.getLogger(CompanyServiceImpl.class);
	/** The messages. */
	@Autowired
	private MessageService messages;

	/** The company repository. */
	@Autowired
	private CompanyRepository companyRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.CompanyService#getAllCompany()
	 */
	@Override
	public List<Company> getAllCompany() {
		try {
			return companyRepository.findAllByOrderByLastModifiedOnDesc();
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	@Override
	public long getCount() {
		return companyRepository.count();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.com.parkson.ntpoms.common.services.CompanyService#getAllActiveCompany(
	 * )
	 */
	@Override
	public List<Company> getAllActiveCompany() {
		try {
			return companyRepository.findAllByIsActiveOrderByLastModifiedOnDesc(true);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.com.parkson.ntpoms.common.services.CompanyService#getCompanyByCode(
	 * java.lang.String)
	 */
	@Override
	public Company getCompanyByCode(String code) {
		try {
			return companyRepository.findByCode(code);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.com.parkson.ntpoms.common.services.CompanyService#createCompany(my.com
	 * .parkson.ntpoms.common.entities.Company)
	 */
	@Override
	public void createCompany(Company company) {
		try {
			companyRepository.save(company);
		} catch (NonUniqueResultException e) {
			LOGGER.error(e);
		} catch (PersistenceException e) {
			LOGGER.error(e);
		} //
		catch (POException e) {
			LOGGER.error(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.com.parkson.ntpoms.common.services.CompanyService#getCompanyByCodeHRIS
	 * (java.lang.String)
	 */
	@Override
	public Company getCompanyByCodeHRIS(String codeHRIS) {
		try {
			return companyRepository.findByCodeHRIS(codeHRIS);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.CompanyService#
	 * getAllPageableCompany(org.springframework.data.domain.Pageable)
	 */
	@Override
	public Page<Company> getAllPageableCompany(Pageable pageRequest) {
		return companyRepository.findAll(pageRequest);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.com.parkson.ntpoms.common.services.CompanyService#getBySearchCode(java
	 * .lang.String)
	 */
	@Override
	public List<Company> getBySearchCode(String searchval) {
		try {
			return companyRepository.findBySearchCode(searchval);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.com.parkson.ntpoms.common.services.CompanyService#findBySearchCode(
	 * java.lang.String)
	 */
	@Override
	public List<Company> findBySearchCode(String code) {
		try {
			return companyRepository.findBySearchCode(code);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return new ArrayList<>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.CompanyService#
	 * getAllPageableCompany(org.springframework.data.jpa.domain.Specification,
	 * org.springframework.data.domain.Pageable)
	 */
	@Override
	public Page<Company> getAllPageableCompany(Specification<Company> s, Pageable pageRequest) {
		return companyRepository.findAll(s, pageRequest);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.CompanyService#
	 * getAllPageableCompany(org.springframework.data.jpa.domain.Specification,
	 * org.springframework.data.domain.Pageable)
	 */
	@Override
	public Page<Company> getAllPageableCompanyOrdered(Pageable pageRequest) {
		return companyRepository.findAllByOrderByLastModifiedOnDesc(pageRequest);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.CompanyService#
	 * getAllPageableCompany(org.springframework.data.jpa.domain.Specification,
	 * org.springframework.data.domain.Pageable)
	 */
	@Override
	public Page<Company> getAllPageableCompanyOrdered(Specification<Company> s, Pageable pageRequest) {
		return companyRepository.findAllByOrderByLastModifiedOnDesc(s, pageRequest);
	}

	@Override
	public long getCountActive() {

		List<Company> list = companyRepository.findAllByIsActiveOrderByLastModifiedOnDesc(true);

		return list.size();
	}
}