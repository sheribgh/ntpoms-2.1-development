/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services.impl;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import my.com.parkson.ntpoms.common.services.MessageService;

/**
 * The Class MessageServiceImpl.
 */
@Component
public class MessageServiceImpl implements MessageService {

    /** The message source. */
    @Autowired
    private MessageSource messageSource;

    /* (non-Javadoc)
     * @see my.com.parkson.ntpoms.common.services.MessageService#getMessage(java.lang.String)
     */
    @Override
    public String getMessage(String code) {
        Locale locale = LocaleContextHolder.getLocale();
        return messageSource.getMessage(code, null, locale);
    }
    
    /* (non-Javadoc)
     * @see my.com.parkson.ntpoms.common.services.MessageService#getMessage(java.lang.String, java.lang.String)
     */
    @Override
	public String getMessage(String code, String defaultMsg) {
        Locale locale = LocaleContextHolder.getLocale();
		return messageSource.getMessage(code, null, defaultMsg, locale);
	}
}