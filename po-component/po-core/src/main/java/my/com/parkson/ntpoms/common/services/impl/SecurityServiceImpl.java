/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services.impl;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import lombok.RequiredArgsConstructor;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.entities.Permission;
import my.com.parkson.ntpoms.common.entities.Role;
import my.com.parkson.ntpoms.common.entities.User;
import my.com.parkson.ntpoms.common.repositories.PermissionRepository;
import my.com.parkson.ntpoms.common.repositories.RoleRepository;
import my.com.parkson.ntpoms.common.repositories.UsersRepository;
import my.com.parkson.ntpoms.common.services.MessageService;
import my.com.parkson.ntpoms.common.services.SecurityService;

/**
 * The Class SecurityServiceImpl.
 */
@Service
@Transactional
@RequiredArgsConstructor
public class SecurityServiceImpl implements SecurityService {
	private static final Logger LOGGER = Logger.getLogger(SecurityServiceImpl.class);

	/** The messages. */
	@Autowired
	private MessageService messages;

	/** The Constant INVALID_USERNAME. */
	private static final String INVALID_USERNAME = "Invalid username";

	/** The user repository. */
	@Autowired
	private UsersRepository userRepository;

	/** The permission repository. */
	@Autowired
	private PermissionRepository permissionRepository;

	/** The role repository. */
	@Autowired
	private RoleRepository roleRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.mimos.atl.mibis.core.security.SecurityService#findUserByEmail(java.
	 * lang.String)
	 */
	@Override
	@Transactional(readOnly = true)
	public User findUserByUsername(String username) {
		try {
			return userRepository.findByUsernameAndIsActive(username, true);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.com.parkson.ntpoms.common.services.SecurityService#getAllPermissions()
	 */
	@Override
	@Transactional(readOnly = true)
	public List<Permission> getAllPermissions() {
		try {
			return permissionRepository.findAll();
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.SecurityService#getAllRoles()
	 */
	@Override
	@Transactional(readOnly = true)
	public List<Role> getAllRoles() {
		try {
			return roleRepository.findAll();
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.SecurityService#
	 * getAllPageablePermissions(org.springframework.data.domain.Pageable)
	 */
	@Override
	@Transactional(readOnly = true)
	public Page<Permission> getAllPageablePermissions(Pageable pageRequest) {
		try {
			return permissionRepository.findAll(pageRequest);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.com.parkson.ntpoms.common.services.SecurityService#getAllPageableRoles
	 * (org.springframework.data.domain.Pageable)
	 */
	@Override
	@Transactional(readOnly = true)
	public Page<Role> getAllPageableRoles(Pageable pageRequest) {
		try {
			return roleRepository.findAll(pageRequest);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.mimos.atl.mibis.core.security.SecurityService#createRole(my.mimos.atl.
	 * mibis.core.entities.Role)
	 */
	@Override
	@Transactional
	public Role createRole(Role role) {
		Role roleByName = getRoleByName(role.getName());
		if (nonNull(roleByName)) {
			throw new POException("Role " + role.getName() + " already exist");
		}
		List<Permission> persistedPermissions = role.getPermissions().stream()
				.filter(permission -> nonNull(permission.getId()))
				.map(permission -> permissionRepository.findOne(permission.getId())).collect(toList());
		role.setPermissions(persistedPermissions);
		return roleRepository.save(role);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.mimos.atl.mibis.core.security.SecurityService#getRoleById(java.lang.
	 * Integer)
	 */
	@Override
	@Transactional(readOnly = true)
	public Role getRoleById(Integer id) {
		try {
			return roleRepository.findOne(id);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.mimos.atl.mibis.core.security.SecurityService#updateRole(my.mimos.atl.
	 * mibis.core.entities.Role)
	 */
	@Override
	@Transactional
	public Role updateRole(Role role) {
		Role persistedRole = getRoleById(role.getId());
		if (isNull(persistedRole)) {
			throw new POException("Role " + role.getId() + " doesn't exist");
		}
		persistedRole.setDescription(role.getDescription());

		List<Permission> updatedPermissions = role.getPermissions().stream()
				.filter(permission -> nonNull(permission.getId()))
				.map(permission -> permissionRepository.findOne(permission.getId())).collect(toList());
		persistedRole.setPermissions(updatedPermissions);
		return roleRepository.save(persistedRole);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.mimos.atl.mibis.core.security.SecurityService#resetPassword(java.lang.
	 * String)
	 */
	@Override
	@Transactional
	public String resetPassword(String username) {
		User user = null;
		try {
			user = findUserByUsername(username);
		} catch (POException e) {
			LOGGER.error(e);
		}
		if (isNull(user)) {
			throw new POException(INVALID_USERNAME);
		}
		String uuid = UUID.randomUUID().toString();
		user.setPasswordResetToken(uuid);
		return uuid;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.mimos.atl.mibis.core.security.SecurityService#verifyPasswordResetToken
	 * (java.lang.String, java.lang.String)
	 */
	@Override
	@Transactional(readOnly = true)
	public boolean verifyPasswordResetToken(String username, String token) {
		User user = null;
		try {
			user = findUserByUsername(username);
		} catch (POException e) {
			LOGGER.error(e);
		}
		if (isNull(user)) {
			throw new POException(INVALID_USERNAME);
		}
		return (StringUtils.hasText(token) && token.equals(user.getPasswordResetToken()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.mimos.atl.mibis.core.security.SecurityService#verifyPasswordResetToken
	 * (java.lang.String, java.lang.String)
	 */
	@Override
	@Transactional
	public boolean verifyCurrentPassword(String username, String currentPassword) {
		User user = null;
		try {
			user = findUserByUsername(username);
		} catch (POException e) {
			LOGGER.error(e);
		}
		if (isNull(user)) {
			throw new POException(INVALID_USERNAME);
		}
		return (StringUtils.hasText(currentPassword) && currentPassword.equals(user.getPassword()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.mimos.atl.mibis.core.security.SecurityService#updatePassword(java.lang
	 * .String, java.lang.String, java.lang.String)
	 */
	@Override
	@Transactional
	public void updatePassword(String username, String token, String encodedPwd) {
		User user = findUserByUsername(username);
		if (isNull(user)) {
			throw new POException(INVALID_USERNAME);
		}
		/*
		 * if (!StringUtils.isEmpty(token) && (!StringUtils.hasText(token) ||
		 * !token.equals(user.getPasswordResetToken()))) { throw new
		 * POException("Invalid password reset token"); }
		 */
		user.setPassword(encodedPwd);
		user.setPasswordResetToken(null);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.SecurityService#getAllUsers()
	 */
	@Override
	@Transactional(readOnly = true)
	public List<User> getAllUsers() {
		try {
			return userRepository.findAll();
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.com.parkson.ntpoms.common.services.SecurityService#getAllPageableUser(
	 * org.springframework.data.domain.Pageable)
	 */
	@Override
	@Transactional(readOnly = true)
	public Page<User> getAllPageableUser(Pageable pageRequest) {
		try {
			return userRepository.findAll(pageRequest);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.mimos.atl.mibis.core.security.SecurityService#createUser(my.mimos.atl.
	 * mibis.core.entities.User)
	 */
	@Override
	@Transactional
	public User createUser(User user) {
		try {
			User userByEmail = findUserByUsername(user.getUsername());
			if (nonNull(userByEmail)) {
				throw new POException("Username " + user.getUsername() + " already in use");
			}
			List<Role> persistedRoles = user.getRoles().stream().filter(role -> nonNull(role.getId()))
					.map(role -> roleRepository.findOne(role.getId())).collect(toList());
			user.setRoles(persistedRoles);

			return userRepository.save(user);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.mimos.atl.mibis.core.security.SecurityService#getUserById(java.lang.
	 * Integer)
	 */
	@Override
	@Transactional(readOnly = true)
	public User getUserByUsername(String username) {
		try {
			return userRepository.findByUsernameAndIsActive(username, true);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.mimos.atl.mibis.core.security.SecurityService#updateUser(my.mimos.atl.
	 * mibis.core.entities.User)
	 */
	@Override
	@Transactional
	public User updateUser(User user) {
		try {
			User persistedUser = getUserByUsername(user.getUsername());
			if (isNull(persistedUser)) {
				throw new POException("User " + user.getUsername() + " doesn't exist");
			}

			List<Role> updatedRoles = user.getRoles().stream().filter(role -> nonNull(role.getId()))
					.map(role -> roleRepository.findOne(role.getId())).collect(toList());
			persistedUser.setRoles(updatedRoles);
			return userRepository.save(persistedUser);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.mimos.atl.mibis.core.security.SecurityService#getRoleByName(java.lang.
	 * String)
	 */
	@Override
	@Transactional(readOnly = true)
	public Role getRoleByName(String roleName) {
		try {
			return roleRepository.findByName(roleName);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	@Override
	@Transactional
	public void updateuserlock(User obj) {
		try {
			userRepository.save(obj);
		} catch (POException e) {
			LOGGER.error(e);
		}
	}
}
