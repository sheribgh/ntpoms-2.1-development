/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import my.com.parkson.ntpoms.common.entities.PurchaseOrderUser;

/**
 * The Interface PurchaseOrderUserRepository.
 */
public interface PurchaseOrderUserRepository
		extends JpaRepository<PurchaseOrderUser, Integer>, JpaSpecificationExecutor<PurchaseOrderUser> {

	/**
	 * Find by id.
	 *
	 * @param id the id
	 * @return the purchase order user
	 */
	public PurchaseOrderUser findById(String id);

	/**
	 * Find all by order by last modified on desc.
	 *
	 * @return the list
	 */
	public List<PurchaseOrderUser> findAllByOrderByLastModifiedOnDesc();
	
	/**
	 * Find all by order by last modified on desc.
	 *
	 * @return the list
	 */
	public Page<PurchaseOrderUser> findAllByOrderByLastModifiedOnDesc(Pageable pageRequest);
	
	/**
	 * Find all by order by last modified on desc.
	 *
	 * @return the list
	 */
	public Page<PurchaseOrderUser> findAllByOrderByLastModifiedOnDesc(Specification<PurchaseOrderUser> s, Pageable pageRequest);

	/**
	 * Find all by active order by last modified on desc.
	 *
	 * @param isActive the is active
	 * @return the list
	 */
	@Query("select o from PurchaseOrderUser o where o.isActive=:at")
	public List<PurchaseOrderUser> findAllByActiveOrderByLastModifiedOnDesc(@Param("at") boolean isActive);

	/**
	 * Gets the all ids.
	 *
	 * @return the all ids
	 */
	@Query("select o.id from PurchaseOrderUser o")
	public List<String> getAllIds();

}