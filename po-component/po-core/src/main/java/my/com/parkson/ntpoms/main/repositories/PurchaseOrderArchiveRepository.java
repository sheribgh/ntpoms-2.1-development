package my.com.parkson.ntpoms.main.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import my.com.parkson.ntpoms.main.entities.PurchaseOrderArchiveDetails;

public interface PurchaseOrderArchiveRepository 
extends JpaRepository<PurchaseOrderArchiveDetails, String>, JpaSpecificationExecutor<PurchaseOrderArchiveDetails>{
	public List<PurchaseOrderArchiveDetails> findAll();
}
