/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.entities.Employee;
import my.com.parkson.ntpoms.common.entities.EmployeeMaster;
import my.com.parkson.ntpoms.common.repositories.EmployeeMasterRepository;
import my.com.parkson.ntpoms.common.services.EmployeeMasterService;
import my.com.parkson.ntpoms.common.services.MessageService;

/**
 * The Class EmployeeMasterServiceImpl.
 */
@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class EmployeeMasterServiceImpl implements EmployeeMasterService {

	private static final Logger LOGGER = Logger.getLogger(EmployeeMasterServiceImpl.class);

	/** The messages. */
	@Autowired
	private MessageService messages;

	/** The employee master repository. */
	@Autowired
	private EmployeeMasterRepository employeeMasterRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.EmployeeMasterService#
	 * createEmployeeMaster(my.com.parkson.ntpoms.common.entities.
	 * EmployeeMaster)
	 */
	@Override
	public void createEmployeeMaster(EmployeeMaster employeeMaster) {
		try {
			employeeMasterRepository.save(employeeMaster);
		} catch (POException e) {
			LOGGER.error(e);
		}
	}

	@Override
	public long getCount() {
		return employeeMasterRepository.count();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.EmployeeMasterService#
	 * getAllActiveEmployeeMaster()
	 */
	@Override
	public List<EmployeeMaster> getAllActiveEmployeeMaster() {
		try {
			return employeeMasterRepository.findAllByIsActive(true);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.EmployeeMasterService#
	 * getEmployeeMasterByPsId(java.lang.String)
	 */
	@Override
	public EmployeeMaster getEmployeeMasterByPsId(String psId) {
		try {
			return employeeMasterRepository.findByPsId(psId);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.EmployeeMasterService#
	 * getAllActiveEmployeeMasterByProfitCenterCode(java.lang.String)
	 */
	@Override
	public List<EmployeeMaster> getAllActiveEmployeeMasterByProfitCenterCode(String profitCenterCode) {
		try {
			employeeMasterRepository.findAllActiveByProfitCenterCode(profitCenterCode);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.EmployeeMasterService#
	 * getAllEmployee()
	 */
	@Override
	public List<Employee> getAllEmployee() {
		List<Employee> employeeList = new ArrayList<Employee>();
		try {
			for (EmployeeMaster employeeMaster : employeeMasterRepository.findAll()) {
				Employee employee = new Employee();
				employee.setPsId(employeeMaster.getPsId());
				employee.setAltId(employeeMaster.getAltId());
				employee.setName(employeeMaster.getName());
				employee.setPosTitle(employeeMaster.getPosTitle());
				employee.setEmail(employeeMaster.getEmail());
				employee.setPaygrp(employeeMaster.getPaygrp());
				employee.setCompany(employeeMaster.getCompany());
				employee.setProfitCenter(employeeMaster.getProfitCenter());
				employeeList.add(employee);
			}
		} catch (POException e) {
			LOGGER.error(e);
		}
		return employeeList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.EmployeeMasterService#
	 * getAllActiveProfitCenterIds()
	 */
	@Override
	public List<String> getAllActiveProfitCenterIds() {
		try {
			return employeeMasterRepository.findAllActivePsIds();
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.EmployeeMasterService#
	 * getAllPagableEmployee(org.springframework.data.domain.Pageable)
	 */
	@Override
	public Page<EmployeeMaster> getAllPagableEmployee(Pageable pageRequest) {
		return employeeMasterRepository.findAll(pageRequest);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.EmployeeMasterService#
	 * getAllPagableEmployee(org.springframework.data.jpa.domain.Specification,
	 * org.springframework.data.domain.Pageable)
	 */
	@Override
	public Page<EmployeeMaster> getAllPagableEmployee(Specification<EmployeeMaster> s, Pageable pageRequest) {
		return employeeMasterRepository.findAll(s, pageRequest);
	}

	@Override
	public Page<EmployeeMaster> getAllPagableEmployeeOrdered(Pageable pageRequest) {
		return employeeMasterRepository.findAllByOrderByLastModifiedOnDesc(pageRequest);
	}

	@Override
	public Page<EmployeeMaster> getAllPagableEmployeeOrdered(Specification<EmployeeMaster> s, Pageable pageRequest) {
		return employeeMasterRepository.findAllByOrderByLastModifiedOnDesc(s, pageRequest);
	}

	@Override
	public List<EmployeeMaster> getAllEmpNameByPsId(String psid) {
		return employeeMasterRepository.findAllEmpNameByPsId(psid);
	}

	@Override
	public List<EmployeeMaster> getAllEmployeebyProfitCenterCode(String profitCenterCode, boolean isActive) {
		List<EmployeeMaster> EmployeeMasterList = new ArrayList<>();
		try {
			EmployeeMasterList = employeeMasterRepository
					.findAllByProfitCenterCodeAndIsActiveOrderByLastModifiedOnDesc(profitCenterCode, isActive);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return EmployeeMasterList;
	}
}