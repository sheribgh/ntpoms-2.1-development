package my.com.parkson.ntpoms.main.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Data;

@Data
@Embeddable
public class PurchaseOrderUserAccessPk implements Serializable {

	private static final long serialVersionUID = 8115259443210365705L;
	
	@Column(name = "po04_pono")
	private String pono;
	
	@Column(name = "po04_psid")
	private String psID;
	
	@Column(name = "po04_action")
	private String poAction;
	
	public PurchaseOrderUserAccessPk(){
		
		
	}

	public PurchaseOrderUserAccessPk(String pono, String psID, String poAction ) {
		this.pono = pono;
		this.psID = psID;
		this.poAction = poAction;
	}

	public String getPono() {
		return pono;
	}

	public void setPono(String pono) {
		this.pono = pono;
	}

	

	public String getPsID() {
		return psID;
	}

	public void setPsID(String psID) {
		this.psID = psID;
	}

	public String getPoAction() {
		return poAction;
	}

	public void setPoAction(String poAction) {
		this.poAction = poAction;
	}

	
	
}