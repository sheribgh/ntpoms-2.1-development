/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
/* Name : MF14_M_DEPTMAST_Service                                      */
/* Usage: Code for the department service                        */
/* Description: This code creates an interface for department that will */
/*              used for connecting to data. This code*/
/*              interacts with the table <mf14_m_deptmast>.        */
/*************************************************************/

package my.com.parkson.ntpoms.common.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import my.com.parkson.ntpoms.common.entities.Department;

/**
 * The Interface DepartmentService.
 */
public interface DepartmentService {

	/**
	 * Gets the all department.
	 *
	 * @return the all department
	 */
	public List<Department> getAllDepartment();
	
	
	
	
	
	public long getCount();

	/**
	 * Gets the all active department.
	 *
	 * @return the all active department
	 */
	public List<Department> getAllActiveDepartment();

	/**
	 * Gets the all pagable department.
	 *
	 * @param pageRequest the page request
	 * @return the all pagable department
	 */
	public Page<Department> getAllPagableDepartment(Pageable pageRequest);

	/**
	 * Gets the all pagable department.
	 *
	 * @param s the s
	 * @param pageRequest the page request
	 * @return the all pagable department
	 */
	public Page<Department> getAllPagableDepartment(Specification<Department> s, Pageable pageRequest);
	
	/**
	 * Gets the all pageable company.
	 *
	 * @param pageRequest the page request
	 * @return the all pageable company
	 */
	public Page<Department> getAllPageableDepartmentOrdered(Pageable pageRequest);
	
	/**
	 * Gets the all pageable company.
	 *
	 * @param s the s
	 * @param pageRequest the page request
	 * @return the all pageable company
	 */
	public Page<Department> getAllPageableDepartmentOrdered(Specification<Department> s, Pageable pageRequest);


	/**
	 * Gets the department by code.
	 *
	 * @param code the code
	 * @return the department by code
	 */
	public Department getDepartmentByCode(String code);

	/**
	 * Creates the department.
	 *
	 * @param department the department
	 */
	public void createDepartment(Department department);
}