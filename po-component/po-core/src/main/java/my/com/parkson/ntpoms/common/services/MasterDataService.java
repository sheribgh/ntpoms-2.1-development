/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import my.com.parkson.ntpoms.common.entities.MasterData;

/**
 * The Interface MasterDataService.
 */
public interface MasterDataService {

	/**
	 * Gets the all master data.
	 *
	 * @return the all master data
	 */
	public List<MasterData> getAllMasterData();
	
	
	
	
	
	public long getCount();

	/**
	 * Gets the all pageable master data.
	 *
	 * @param pageRequest the page request
	 * @return the all pageable master data
	 */
	public Page<MasterData> getAllPageableMasterData(Pageable pageRequest);

	/**
	 * Gets the all pageable master data.
	 *
	 * @param s the s
	 * @param pageRequest the page request
	 * @return the all pageable master data
	 */
	public Page<MasterData> getAllPageableMasterData(Specification<MasterData> s, Pageable pageRequest);
	
	/**
	 * Gets the all pageable master data.
	 *
	 * @param pageRequest the page request
	 * @return the all pageable master data
	 */
	public Page<MasterData> getAllPageableMasterDataOrdered(Pageable pageRequest);

	/**
	 * Gets the all pageable master data.
	 *
	 * @param s the s
	 * @param pageRequest the page request
	 * @return the all pageable master data
	 */
	public Page<MasterData> getAllPageableMasterDataOrdered(Specification<MasterData> s, Pageable pageRequest);

	/**
	 * Gets the all active master data.
	 *
	 * @return the all active master data
	 */
	public List<MasterData> getAllActiveMasterData();

	/**
	 * Gets the master data by id.
	 *
	 * @param id the id
	 * @return the master data by id
	 */
	public MasterData getMasterDataById(int id);
	
	
	
	
	public MasterData getMasterDataByCodeType(String id);
	


	/**
	 * Creates the master data.
	 *
	 * @param masterData the master data
	 */
	public void createMasterData(MasterData masterData);
}