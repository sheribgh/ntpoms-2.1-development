/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import my.com.parkson.ntpoms.common.entities.Employee;
import my.com.parkson.ntpoms.common.entities.EmployeeMaster;
import my.com.parkson.ntpoms.common.entities.ProfitCenter;
import my.com.parkson.ntpoms.common.entities.PurchaseOrderUser;

/**
 * The Interface EmployeeMasterService.
 */
public interface EmployeeMasterService {

	/**
	 * Gets the all pagable employee.
	 *
	 * @param pageRequest the page request
	 * @return the all pagable employee
	 */
	public Page<EmployeeMaster> getAllPagableEmployee(Pageable pageRequest);
	
	
	
	
	public long getCount();

	/**
	 * Gets the all pagable employee.
	 *
	 * @param s the s
	 * @param pageRequest the page request
	 * @return the all pagable employee
	 */
	public Page<EmployeeMaster> getAllPagableEmployee(Specification<EmployeeMaster> s, Pageable pageRequest);

	
	/**
	 * Gets the all pagable employee.
	 *
	 * @param pageRequest the page request
	 * @return the all pagable employee
	 */
	public Page<EmployeeMaster> getAllPagableEmployeeOrdered(Pageable pageRequest);

	/**
	 * Gets the all pagable employee.
	 *
	 * @param s the s
	 * @param pageRequest the page request
	 * @return the all pagable employee
	 */
	public Page<EmployeeMaster> getAllPagableEmployeeOrdered(Specification<EmployeeMaster> s, Pageable pageRequest);

	/**
	 * Gets the all employee.
	 *
	 * @return the all employee
	 */
	public List<Employee> getAllEmployee();

	/**
	 * Gets the all active employee master.
	 *
	 * @return the all active employee master
	 */
	public List<EmployeeMaster> getAllActiveEmployeeMaster();

	/**
	 * Creates the employee master.
	 *
	 * @param employeeMaster the employee master
	 */
	public void createEmployeeMaster(EmployeeMaster employeeMaster);

	/**
	 * Gets the employee master by ps id.
	 *
	 * @param psId the ps id
	 * @return the employee master by ps id
	 */
	public EmployeeMaster getEmployeeMasterByPsId(String psId);

	/**
	 * Gets the all active employee master by profit center code.
	 *
	 * @param profitCenterCode the profit center code
	 * @return the all active employee master by profit center code
	 */
	public List<EmployeeMaster> getAllActiveEmployeeMasterByProfitCenterCode(String profitCenterCode);
	
	public List<EmployeeMaster> getAllEmployeebyProfitCenterCode(String profitCenterCode,boolean isActive);

	/**
	 * Gets the all active profit center ids.
	 *
	 * @return the all active profit center ids
	 */
	public List<String> getAllActiveProfitCenterIds();
	
	public List<EmployeeMaster> getAllEmpNameByPsId(String psid);
}