package my.com.parkson.ntpoms.main.services.impl;

import java.util.List;

import javax.persistence.NoResultException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.main.entities.PurchaseOrderUserAccess;
import my.com.parkson.ntpoms.main.entities.PurchaseOrderUserAccessPk;
import my.com.parkson.ntpoms.main.repositories.PurchaseOrderUserAccessRepository;
import my.com.parkson.ntpoms.main.services.PurchaseOrderService;
import my.com.parkson.ntpoms.main.services.PurchaseOrderUserAccessService;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class PurchaseOrderUserAccessServiceImpl implements PurchaseOrderUserAccessService {

	private static final Logger LOGGER = Logger.getLogger(PurchaseOrderUserAccessServiceImpl.class);

	@Autowired
	private PurchaseOrderService mainService;

	@Autowired
	private PurchaseOrderUserAccessRepository purchaseOrderUserAccessRepository;

	@Override
	public List<PurchaseOrderUserAccess> getBypsID(String psID) {
		List<PurchaseOrderUserAccess> purchaseUserAcc = purchaseOrderUserAccessRepository
				.findAllByPurchaseorderuseraccessIdPsID(psID);
		return purchaseUserAcc;
	}

	@Override
	public void createPurchaseOrderUserAccess(PurchaseOrderUserAccess purchaseOrderUserAccess) {
		try {
			purchaseOrderUserAccessRepository.save(purchaseOrderUserAccess);
		} catch (Exception e) {
			LOGGER.error(e);
		}
	}

	@Override
	public PurchaseOrderUserAccess getPurchaseOrderUserAccessById(String id) {
		try {
			return purchaseOrderUserAccessRepository.findByPurchaseorderuseraccessIdPsID(id);
		} catch (NoResultException e) {
			LOGGER.error(e);
			PurchaseOrderUserAccess userAccess = new PurchaseOrderUserAccess();
			PurchaseOrderUserAccessPk pkey = new PurchaseOrderUserAccessPk();
			pkey.setPsID(id);
			userAccess.setPurchaseorderuseraccessId(pkey);
			return userAccess;
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return null;
	}

	/*
	 * @Override public List<PurchaseOrder>
	 * getAllToBeVerifiedPurchaseOrderById(String id) { List<PurchaseOrder>
	 * toBeVerifiedList = new ArrayList<>(); try { PurchaseOrderUserAccess
	 * userAccess = purchaseOrderUserAccessRepository.findByPsID(id); if
	 * (userAccess.getToBeVerified() != null) { for (String purchaseOrderNo :
	 * userAccess.getToBeVerified().split(",")) { if (purchaseOrderNo != null &&
	 * !purchaseOrderNo.isEmpty()) { PurchaseOrder tempPurchaseOrder =
	 * mainService.getPurchaseOrderByPoNo(purchaseOrderNo); if
	 * (tempPurchaseOrder != null &&
	 * tempPurchaseOrder.getStatus().equalsIgnoreCase("Issued")) {
	 * toBeVerifiedList.add(tempPurchaseOrder); } } } } } catch (Exception e) {
	 * log.log(DEBUG_ERROR, DEBUG_DES, e); } return toBeVerifiedList; }
	 * 
	 * @Override public List<PurchaseOrder>
	 * getAllToBeAuthorizedPurchaseOrderById(String id) { List<PurchaseOrder>
	 * toBeAuthorized = new ArrayList<>(); try { PurchaseOrderUserAccess
	 * userAccess = purchaseOrderUserAccessRepository.findByPsID(id); if
	 * (userAccess.getToBeAuthorized() != null) { for (String purchaseOrderNo :
	 * userAccess.getToBeAuthorized().split(",")) { if (purchaseOrderNo != null
	 * && !purchaseOrderNo.isEmpty()) { PurchaseOrder tempPurchaseOrder =
	 * mainService.getPurchaseOrderByPoNo(purchaseOrderNo); if
	 * (tempPurchaseOrder != null &&
	 * tempPurchaseOrder.getStatus().equalsIgnoreCase("Verified")) {
	 * toBeAuthorized.add(tempPurchaseOrder); } } } } } catch (Exception e) {
	 * log.log(DEBUG_ERROR, DEBUG_DES, e); } return toBeAuthorized; }
	 * 
	 * @Override public List<PurchaseOrder>
	 * getAllUnactionablePurchaseOrderById(String id) { List<PurchaseOrder>
	 * unactionable = new ArrayList<>(); try { PurchaseOrderUserAccess
	 * userAccess = purchaseOrderUserAccessRepository.findByPsID(id); if
	 * (userAccess.getMyPOS() != null) { for (String purchaseOrderNo :
	 * userAccess.getMyPOS().split(",")) { PurchaseOrder tempUnactionable =
	 * mainService.getPurchaseOrderByPoNo(purchaseOrderNo); if (tempUnactionable
	 * != null) { unactionable.add(tempUnactionable); } } }
	 * 
	 * } catch (Exception e) { log.log(DEBUG_ERROR, DEBUG_DES, e); } return
	 * unactionable; }
	 * 
	 * @Override public List<PurchaseOrder> getAllDraftPurchaseOrderById(String
	 * id) { List<PurchaseOrder> draft = new ArrayList<>(); try {
	 * PurchaseOrderUserAccess userAccess =
	 * purchaseOrderUserAccessRepository.findByPsID(id); if
	 * (userAccess.getDraft() != null) { for (String purchaseOrderNo :
	 * userAccess.getDraft().split(",")) { PurchaseOrder tempDraft =
	 * mainService.getPurchaseOrderByPoNo(purchaseOrderNo); if (tempDraft !=
	 * null && tempDraft.getStatus().equalsIgnoreCase("Draft")) {
	 * draft.add(tempDraft); } } } } catch (Exception e) { log.log(DEBUG_ERROR,
	 * DEBUG_DES, e); } return draft; }
	 * 
	 * @Override public List<PurchaseOrder>
	 * getAllRedraftByPurchaseOrderById(String id) { List<PurchaseOrder> redraft
	 * = new ArrayList<>(); try { PurchaseOrderUserAccess userAccess =
	 * purchaseOrderUserAccessRepository.findByPsID(id); if
	 * (userAccess.getRedraft() != null) { for (String purchaseOrderNo :
	 * userAccess.getRedraft().split(",")) { PurchaseOrder tempRedraft =
	 * mainService.getPurchaseOrderByPoNo(purchaseOrderNo); if (tempRedraft !=
	 * null && tempRedraft.getStatus().equalsIgnoreCase("Redraft")) {
	 * redraft.add(tempRedraft); } } } } catch (Exception e) {
	 * log.log(DEBUG_ERROR, DEBUG_DES, e); } return redraft; }
	 * 
	 * @Override public List<PurchaseOrder>
	 * getAllToBeInvoicedPurchaseOrderById(String id) { List<PurchaseOrder>
	 * toBeInvoicedList = new ArrayList<>(); try { PurchaseOrderUserAccess
	 * userAccess = purchaseOrderUserAccessRepository.findByPsID(id); if
	 * (userAccess.getToBeInvoiced() != null) { for (String purchaseOrderNo :
	 * userAccess.getToBeInvoiced().split(",")) { if (purchaseOrderNo != null &&
	 * !purchaseOrderNo.isEmpty()) { PurchaseOrder tempToBeInvoicedList =
	 * mainService.getPurchaseOrderByPoNo(purchaseOrderNo); if
	 * (tempToBeInvoicedList != null &&
	 * tempToBeInvoicedList.getStatus().equalsIgnoreCase("Authorized")) {
	 * toBeInvoicedList.add(tempToBeInvoicedList); } } } } } catch (Exception e)
	 * { log.log(DEBUG_ERROR, DEBUG_DES, e); } return toBeInvoicedList; }
	 * 
	 * @Override public int countToBeVerified(String id) { int countToBeVerified
	 * = 0; try { PurchaseOrderUserAccess userAccess =
	 * purchaseOrderUserAccessRepository.findByPsID(id); if
	 * (userAccess.getToBeVerified() != null) { String[] result =
	 * userAccess.getToBeVerified().split(","); countToBeVerified =
	 * result.length; } } catch (Exception e) { log.log(DEBUG_ERROR, DEBUG_DES,
	 * e); } return countToBeVerified; }
	 * 
	 * @Override public int countToBeAuthorized(String id) { int
	 * countToBeAuthorized = 0; try { PurchaseOrderUserAccess userAccess =
	 * purchaseOrderUserAccessRepository.findByPsID(id); if
	 * (userAccess.getToBeAuthorized() != null) { String[] result =
	 * userAccess.getToBeAuthorized().split(","); countToBeAuthorized =
	 * result.length; } } catch (Exception e) { log.log(DEBUG_ERROR, DEBUG_DES,
	 * e); } return countToBeAuthorized; }
	 * 
	 * @Override public int countToBeInvoiced(String id) { int countToBeInvoiced
	 * = 0; try { PurchaseOrderUserAccess userAccess =
	 * purchaseOrderUserAccessRepository.findByPsID(id); if
	 * (userAccess.getToBeAuthorized() != null) { String[] result =
	 * userAccess.getToBeAuthorized().split(","); countToBeInvoiced =
	 * result.length; } } catch (Exception e) { log.log(DEBUG_ERROR, DEBUG_DES,
	 * e); } return countToBeInvoiced; }
	 * 
	 * @Override public int countRedrafted(String id) { int countRedrafted = 0;
	 * try { PurchaseOrderUserAccess userAccess =
	 * purchaseOrderUserAccessRepository.findByPsID(id); if
	 * (userAccess.getRedraft() != null) { String[] result =
	 * userAccess.getDraft().split(","); countRedrafted = result.length; } }
	 * catch (Exception e) { log.log(DEBUG_ERROR, DEBUG_DES, e); } return
	 * countRedrafted; }
	 * 
	 * @Override public int countDrafted(String id) { int countDrafted = 0; try
	 * { PurchaseOrderUserAccess userAccess =
	 * purchaseOrderUserAccessRepository.findByPsID(id); if
	 * (userAccess.getDraft() != null) { String[] result =
	 * userAccess.getDraft().split(","); countDrafted = result.length; } } catch
	 * (Exception e) { log.log(DEBUG_ERROR, DEBUG_DES, e); } return
	 * countDrafted; }
	 */

	@Override
	public List<PurchaseOrderUserAccess> getBypsIDAndAction(String psID, String poAction) {
		List<PurchaseOrderUserAccess> purchaseOrderUserAccess = null;
		try {
			purchaseOrderUserAccess = purchaseOrderUserAccessRepository
					.findAllByPurchaseorderuseraccessIdPsIDAndPurchaseorderuseraccessIdPoAction(psID, poAction);
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return purchaseOrderUserAccess;
	}

	@Override
	public List<PurchaseOrderUserAccess> getBypsIDAndActionForRedraft(String psID) {
		try {
			return purchaseOrderUserAccessRepository.findAllByPsIDAndPoActionForRedraft(psID);
		} catch (Exception e) {
			LOGGER.error(e);
		}

		return null;
	}

	@Override
	public PurchaseOrderUserAccess getPurchaseOrderUserAccessByPK(PurchaseOrderUserAccessPk purchaseorderuseraccessId) {
		try {
			return purchaseOrderUserAccessRepository.findByPurchaseorderuseraccessId(purchaseorderuseraccessId);
		} catch (Exception e) {
			LOGGER.error(e);
		}

		return null;
	}

	@Override
	public List<PurchaseOrderUserAccess> getBypsIDAndActionForUpdate(String psID) {
		try {
			return purchaseOrderUserAccessRepository.findAllByPsIDAndPoActionForUpdate(psID);
		} catch (Exception e) {
			LOGGER.error(e);
		}

		return null;
	}

	@Override
	public PurchaseOrderUserAccess getByPoNOAndActionForUpdate(String poNo) {
		try {
			return purchaseOrderUserAccessRepository.findAllByPoNoAndPoActionForUpdate(poNo);
		} catch (Exception e) {
			LOGGER.error(e);
		}

		return null;
	}
}