/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The Class MenuGroup.
 */
@Entity
@Table(name = "menu_grp")
public class MenuGroup implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The menu group id. */
	@Id
	@Column(name = "menu_grp_id", unique = true, nullable = false)
	private Integer menuGroupId;

	/** The menu group enable. */
	@Column(name="menu_grp_enable", length = 100, nullable = false)
	private boolean menuGroupEnable;

	/** The menu group parent. */
	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST,
			CascadeType.MERGE })
	@JoinColumn(name = "menu_grp_parent")
	private MenuGroup menuGroupParent;

	/** The menu group children. */
	@OneToMany(mappedBy = "menuGroupParent")
	private List<MenuGroup> menuGroupChildren;
	
	/** The menu. */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="menu_id",referencedColumnName="menu_id")
	private Menu menu;

	/**
	 * Gets the menu.
	 *
	 * @return the menu
	 */
	public Menu getMenu() {
		return menu;
	}

	/**
	 * Sets the menu.
	 *
	 * @param menu the new menu
	 */
	public void setMenu(Menu menu) {
		this.menu = menu;
	}

	/**
	 * Gets the menu group id.
	 *
	 * @return the menu group id
	 */
	public Integer getMenuGroupId() {
		return menuGroupId;
	}

	/**
	 * Sets the menu group id.
	 *
	 * @param menuGroupId the new menu group id
	 */
	public void setMenuGroupId(Integer menuGroupId) {
		this.menuGroupId = menuGroupId;
	}
	
	/**
	 * Checks if is menu group enable.
	 *
	 * @return true, if is menu group enable
	 */
	public boolean isMenuGroupEnable() {
		return menuGroupEnable;
	}

	/**
	 * Sets the menu group enable.
	 *
	 * @param menuGroupEnable the new menu group enable
	 */
	public void setMenuGroupEnable(boolean menuGroupEnable) {
		this.menuGroupEnable = menuGroupEnable;
	}

	/**
	 * Gets the menu group parent.
	 *
	 * @return the menu group parent
	 */
	public MenuGroup getMenuGroupParent() {
		return menuGroupParent;
	}

	/**
	 * Sets the menu group parent.
	 *
	 * @param menuGroupParent the new menu group parent
	 */
	public void setMenuGroupParent(MenuGroup menuGroupParent) {
		this.menuGroupParent = menuGroupParent;
	}

	/**
	 * Gets the menu group children.
	 *
	 * @return the menu group children
	 */
	public List<MenuGroup> getMenuGroupChildren() {
		return menuGroupChildren;
	}

	/**
	 * Sets the menu group children.
	 *
	 * @param menuGroupChildren the new menu group children
	 */
	public void setMenuGroupChildren(List<MenuGroup> menuGroupChildren) {
		this.menuGroupChildren = menuGroupChildren;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MenuGroup [menuGroupId=" + menuGroupId + "]";
	}
}