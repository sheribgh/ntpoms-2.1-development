/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import my.com.parkson.ntpoms.common.entities.DataSource;

/**
 * The Interface DataSourceRepository.
 */
public interface DataSourceRepository extends JpaRepository<DataSource, Integer>, JpaSpecificationExecutor<DataSource> {

	/**
	 * Find by company.
	 *
	 * @param company the company
	 * @return the data source
	 */
	public DataSource findByCompany(String company);

	/**
	 * Find by company and year.
	 *
	 * @param company the company
	 * @param year the year
	 * @return the data source
	 */
	public DataSource findByCompanyAndYear(String company, String year);

	/**
	 * Find all by is active and year.
	 *
	 * @param isActive the is active
	 * @param year the year
	 * @return the list
	 */
	public List<DataSource> findAllByIsActiveAndYear(boolean isActive, String year);

	/**
	 * Find all after year.
	 *
	 * @param year the year
	 * @return the list
	 */
	@Query("select o from DataSource o where o.year > ?1")
	public List<DataSource> findAllAfterYear(String year);

	/* (non-Javadoc)
	 * @see org.springframework.data.jpa.repository.JpaRepository#findAll()
	 */
	public List<DataSource> findAll();

}