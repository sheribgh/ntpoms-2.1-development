/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.main.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The Class AccessLog.
 */
@Entity
@Table(name = "\"LD02_T_ACSLOG\"")
@Data
@NoArgsConstructor
public class AccessLogs implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ld02_t_acslog_sequence")
	@SequenceGenerator(name = "ld02_t_acslog_sequence", sequenceName = "ld02_t_acslog_sequence", allocationSize = 1)
	@Column(name = "\"ld02_logId\"")
	private int id;

	/** The level. */
	@Column(name = "\"ld02_logLevel\"")
	private String level;

	/** The message. */
	@Column(name = "\"ld02_logMessage\"")
	private String message;

	/** The class file name. */
	@Column(name = "\"ld02_classFileName\"")
	private String classFileName;

	/** The screen id. */
	@Column(name = "\"ld02_screenId\"")
	private String screenId;

	/** The line no. */
	@Column(name = "\"ld02_lineNo\"")
	private int lineNo;

	@Column(name = "\"ld02_company\"")
	private String company;

	/** The time. */
	@Column(name = "\"ld02_logTime\"")
	private Date time;

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Gets the level.
	 *
	 * @return the level
	 */
	public String getLevel() {
		return level;
	}

	/**
	 * Sets the level.
	 *
	 * @param level
	 *            the new level
	 */
	public void setLevel(String level) {
		this.level = level;
	}

	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Sets the message.
	 *
	 * @param message
	 *            the new message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * Gets the class file name.
	 *
	 * @return the class file name
	 */
	public String getClassFileName() {
		return classFileName;
	}

	/**
	 * Sets the class file name.
	 *
	 * @param classFileName
	 *            the new class file name
	 */
	public void setClassFileName(String classFileName) {
		this.classFileName = classFileName;
	}

	/**
	 * Gets the screen id.
	 *
	 * @return the screen id
	 */
	public String getScreenId() {
		return screenId;
	}

	/**
	 * Sets the screen id.
	 *
	 * @param screenId
	 *            the new screen id
	 */
	public void setScreenId(String screenId) {
		this.screenId = screenId;
	}

	/**
	 * Gets the line no.
	 *
	 * @return the line no
	 */
	public int getLineNo() {
		return lineNo;
	}

	/**
	 * Sets the line no.
	 *
	 * @param lineNo
	 *            the new line no
	 */
	public void setLineNo(int lineNo) {
		this.lineNo = lineNo;
	}

	/**
	 * Gets the time.
	 *
	 * @return the time
	 */
	public Date getTime() {
		return time;
	}

	/**
	 * @return the company
	 */
	public String getCompany() {
		return company;
	}

	/**
	 * @param company
	 *            the company to set
	 */
	public void setCompany(String company) {
		this.company = company;
	}

	/**
	 * Sets the time.
	 *
	 * @param time
	 *            the new time
	 */
	public void setTime(Date time) {
		this.time = time;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return id + " - " + message;
	}
}