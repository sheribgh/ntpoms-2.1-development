package my.com.parkson.ntpoms.main.services;

import java.util.List;

import org.springframework.data.repository.query.Param;

import my.com.parkson.ntpoms.main.entities.PurchaseOrder;
import my.com.parkson.ntpoms.main.entities.PurchaseOrderUserAccess;
import my.com.parkson.ntpoms.main.entities.PurchaseOrderUserAccessPk;

public interface PurchaseOrderUserAccessService {
	
public List<PurchaseOrderUserAccess> getBypsID(String psID);
	public void createPurchaseOrderUserAccess(PurchaseOrderUserAccess purchaseOrderUserAccess);	
	public PurchaseOrderUserAccess getPurchaseOrderUserAccessById(String id);	
	/*public List<PurchaseOrder> getAllToBeVerifiedPurchaseOrderById(String id);	
	public List<PurchaseOrder> getAllToBeAuthorizedPurchaseOrderById(String id);	
	public List<PurchaseOrder> getAllDraftPurchaseOrderById(String id);	
	public List<PurchaseOrder> getAllRedraftByPurchaseOrderById(String id);	
	public List<PurchaseOrder> getAllUnactionablePurchaseOrderById(String id);
	public List<PurchaseOrder> getAllToBeInvoicedPurchaseOrderById(String id);	
	public int countToBeVerified(String id);	
	public int countToBeAuthorized(String id);	
	public int countToBeInvoiced(String id);	
	public int countRedrafted(String id);	
	public int countDrafted(String id);*/
	public List<PurchaseOrderUserAccess> getBypsIDAndAction(String psID, String poAction);
	
	public List<PurchaseOrderUserAccess> getBypsIDAndActionForRedraft(String psID);
	public List<PurchaseOrderUserAccess> getBypsIDAndActionForUpdate(String psID);
	public PurchaseOrderUserAccess getByPoNOAndActionForUpdate(String poNo);
	public PurchaseOrderUserAccess getPurchaseOrderUserAccessByPK(PurchaseOrderUserAccessPk purchaseorderuseraccessId);	
}