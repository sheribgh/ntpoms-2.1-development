package my.com.parkson.ntpoms.main.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
//import javax.persistence.JoinColumn;
//import javax.persistence.JoinColumns;
//import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.NoArgsConstructor;
/*import my.com.parkson.ntpoms.common.entities.VendorCreditTerms.VCRTpk;*/

@Entity
@Table(name = "\"mf03_m_vendorcrterms\"")
@Data
/*@IdClass(VCRTpk.class)*/
@NoArgsConstructor
public class VendorCreditTerms implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EmbeddedId
    VendorCreditTermsId vendcrtId;
	
	public VendorCreditTermsId getVendcrtId() {
		return vendcrtId;
	}

	public void setVendcrtId(VendorCreditTermsId vendcrtId) {
		this.vendcrtId = vendcrtId;
	}


	
	
	

	/*@Id
	@Column(name = "mf01_vendorbranch_mf01_vendorcode")
	private String vendorCode ;

	@Id 
	@Column(name = "mf01_vendorbranch_mf01_vendorbranchcode")
	private String vendorBranchCode ;	

	@Id
	@Column(name = "mf02_creditterms_mf02_credittermscode")
	private String creditTermsCode;

	@Id
	@Column(name="\"mf11mcompmast_mf03_compcode\"")
	private String companyCode;*/


	@Column(name = "mf03_createdby")
	private String  createdby;

	@Column(name = "mf03_createdon")
	private Date createdon ;

	@Column(name = "mf03_lastmodifiedby")
	private String lastmodifiedby ;

	@Column(name = "\"mf03_lastmodifiedon\"")
	private Date lastModifiedOn ;

	@Column(name = "mf03_deactivatedby")
	private String deactivatedby ;

	@Column(name = "mf03_deactivatedon")
	private Date deactivatedon ;

	@Column(name = "mf03_reactivatedby")
	private String reactivatedby ;

	@Column(name = "mf03_reactivatedon")
	private Date reactivatedon ;

	@Column(name = "mf03_isactive")
	private boolean isActive;

	
	

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public Date getCreatedon() {
		return createdon;
	}

	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}

	public String getLastmodifiedby() {
		return lastmodifiedby;
	}

	public void setLastmodifiedby(String lastmodifiedby) {
		this.lastmodifiedby = lastmodifiedby;
	}



	public Date getLastModifiedOn() {
		return lastModifiedOn;
	}

	public void setLastModifiedOn(Date lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

	public String getDeactivatedby() {
		return deactivatedby;
	}

	public void setDeactivatedby(String deactivatedby) {
		this.deactivatedby = deactivatedby;
	}

	public Date getDeactivatedon() {
		return deactivatedon;
	}

	public void setDeactivatedon(Date deactivatedon) {
		this.deactivatedon = deactivatedon;
	}

	public String getReactivatedby() {
		return reactivatedby;
	}

	public void setReactivatedby(String reactivatedby) {
		this.reactivatedby = reactivatedby;
	}

	public Date getReactivatedon() {
		return reactivatedon;
	}

	public void setReactivatedon(Date reactivatedon) {
		this.reactivatedon = reactivatedon;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}



}