/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import my.com.parkson.ntpoms.common.entities.User;
import my.com.parkson.ntpoms.common.entities.UserRole;

/**
 * The Interface UsersService.
 */
public interface UsersService {

	/**
	 * Creates the users.
	 *
	 * @param users the users
	 */
	public void createUsers(User users);
	
	public boolean saveUser(User user);
	
	public void addUserRole(UserRole role);
	
	
	public long getCount();
	
	public long getCountActive();
	
	public UserRole getUsersByUser(String username);
	
	/**
	 * Gets the all pagable users.
	 *
	 * @param pageRequest the page request
	 * @return the all pagable users
	 */
	public Page<User> getAllPagableUsers(Pageable pageRequest);

	/**
	 * Gets the all pagable users.
	 *
	 * @param s the s
	 * @param pageRequest the page request
	 * @return the all pagable users
	 */
	public Page<User> getAllPagableUsers(Specification<User> s, Pageable pageRequest);

	/**
	 * Gets the users by username.
	 *
	 * @param username the username
	 * @return the users by username
	 */
	public User getUsersByUsername(String username);
	
	/**
	 * Gets the users by Email.
	 *
	 * @param username the Email
	 * @return the users by Email
	 */
	public User getUsersByUserEmail(String email);
	
	
	
	public List<User> getUsersByUsersEmail(String email);

	/**
	 * Gets the all users.
	 *
	 * @return the all users
	 */
	public List<User> getAllUsers();

	/**
	 * Gets the all users exclude superadmin.
	 *
	 * @return the all users exclude superadmin
	 */
	public List<User> getAllUsersExcludeSuperadmin();

	/**
	 * Gets the all active users.
	 *
	 * @return the all active users
	 */
	public List<User> getAllActiveUsers();

	/**
	 * Gets the all pageable company.
	 *
	 * @param pageRequest the page request
	 * @return the all pageable company
	 */
	public Page<User> getAllPageableUserOrdered(Pageable pageRequest);
	
	/**
	 * Gets the all pageable company.
	 *
	 * @param s the s
	 * @param pageRequest the page request
	 * @return the all pageable company
	 */
	public Page<User> getAllPageableUserOrdered(Specification<User> s, Pageable pageRequest);

}