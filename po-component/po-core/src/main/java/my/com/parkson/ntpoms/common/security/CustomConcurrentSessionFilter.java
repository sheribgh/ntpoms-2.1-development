/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.security.web.util.UrlUtils;
import org.springframework.util.Assert;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * The Class CustomConcurrentSessionFilter.
 */
public class CustomConcurrentSessionFilter extends GenericFilterBean {

    /** The create new session. */
    private boolean createNewSession = true;
    
    /** The session registry. */
    private SessionRegistry sessionRegistry;
    
    /** The expired url. */
    private String expiredUrl;
    
    /** The handlers. */
    private LogoutHandler[] handlers = new LogoutHandler[] { new SecurityContextLogoutHandler() };
    
    /** The redirect strategy. */
    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    /**
     * Instantiates a new custom concurrent session filter.
     *
     * @param sessionRegistry the session registry
     */
    public CustomConcurrentSessionFilter(SessionRegistry sessionRegistry) {
        Assert.notNull(sessionRegistry, "SessionRegistry required");
        this.sessionRegistry = sessionRegistry;
    }

    /**
     * Instantiates a new custom concurrent session filter.
     *
     * @param sessionRegistry the session registry
     * @param expiredUrl the expired url
     */
    public CustomConcurrentSessionFilter(SessionRegistry sessionRegistry, String expiredUrl) {
        Assert.notNull(sessionRegistry, "SessionRegistry required");
        Assert.isTrue(expiredUrl == null || UrlUtils.isValidRedirectUrl(expiredUrl), expiredUrl + " is not a valid redirect URL !");
        this.sessionRegistry = sessionRegistry;
        this.expiredUrl = expiredUrl;
    }

    /* (non-Javadoc)
     * @see org.springframework.web.filter.GenericFilterBean#afterPropertiesSet()
     */
    @Override
    public void afterPropertiesSet() {
        Assert.notNull(sessionRegistry, "SessionRegistry required");
        Assert.isTrue(expiredUrl == null || UrlUtils.isValidRedirectUrl(expiredUrl), expiredUrl + " is not a valid redirect URL !");
    }

    /* (non-Javadoc)
     * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
     */
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        HttpSession session = request.getSession(false);
        if (session != null) {
            SessionInformation info = sessionRegistry.getSessionInformation(session.getId());
            if (info != null) {
                if (info.isExpired()) {
                    doLogout(request, response);
                    String targetUrl = determineExpiredUrl(request, info);
                    if (targetUrl != null) {
                        logger.debug("Start new session (if required) and redirect to ['" + targetUrl + "']");
                        if (createNewSession) {
                            request.getSession();
                        }
                        redirectStrategy.sendRedirect(request, response, targetUrl);
                        return;
                    }
                    else {
                        response.getWriter().print("This session has been expired (possibly due to multiple concurrent logins being attempted as the same user).");
                        response.flushBuffer();
                    }
                    return;
                }
                else {
                    sessionRegistry.refreshLastRequest(info.getSessionId());
                }
            }
        }
        chain.doFilter(request, response);
    }

    /**
     * Determine expired url.
     *
     * @param request the request
     * @param info the info
     * @return the string
     */
    protected String determineExpiredUrl(HttpServletRequest request, SessionInformation info) {
        return expiredUrl;
    }

    /**
     * Do logout.
     *
     * @param request the request
     * @param response the response
     */
    private void doLogout(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        for (LogoutHandler handler : handlers) {
            handler.logout(request, response, auth);
        }
    }

    /**
     * Sets the logout handlers.
     *
     * @param handlers the new logout handlers
     */
    public void setLogoutHandlers(LogoutHandler[] handlers) {
        Assert.notNull(handlers);
        this.handlers = handlers;
    }

    /**
     * Sets the redirect strategy.
     *
     * @param redirectStrategy the new redirect strategy
     */
    public void setRedirectStrategy(RedirectStrategy redirectStrategy) {
        this.redirectStrategy = redirectStrategy;
    }

    /**
     * Sets the creates the new session.
     *
     * @param createNewSession the new creates the new session
     */
    public void setCreateNewSession(boolean createNewSession) {
        this.createNewSession = createNewSession;
    }
}