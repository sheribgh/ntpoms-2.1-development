package my.com.parkson.ntpoms.main.services.impl;

import static my.com.parkson.ntpoms.common.MessageCodes.FAILED_FETCH_RECORDS_EXCEPTION;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import groovy.util.logging.Slf4j;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.LogMessages;
import my.com.parkson.ntpoms.common.services.MessageService;
import my.com.parkson.ntpoms.main.entities.AccessLogsSetting;
import my.com.parkson.ntpoms.main.repositories.AccessLogsSettingRepository;
import my.com.parkson.ntpoms.main.services.AccessLogsSettingService;

@Slf4j
@Service
@Transactional
public class AccessLogsSettingServiceImpl implements AccessLogsSettingService {

	private static final Logger LOGGER = Logger.getLogger(AccessLogsSettingServiceImpl.class);

	@Autowired
	private AccessLogsSettingRepository accessLogSettingRepository;

	@Autowired
	private MessageService messages;

	@Override
	public int getLastAccessLogSetting() {
		int lastSetting = 1;
		try {
			List<AccessLogsSetting> accessLogSettingList = accessLogSettingRepository.findAll();
			if (!accessLogSettingList.isEmpty()) {
				lastSetting = (accessLogSettingList.get(accessLogSettingList.size() - 1)).getSetting();
			}
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return lastSetting;
	}

	@Override
	public void createAccessLogSetting(AccessLogsSetting accessLogSetting) {
		try {
			accessLogSettingRepository.save(accessLogSetting);
		} catch (Exception e) {
			LOGGER.error(e);
		}
	}

	@Override
	public AccessLogsSetting getLastStagingLog() {
		AccessLogsSetting lastSetting = null;
		try {
			List<AccessLogsSetting> stagingLogList = accessLogSettingRepository.findAll();
			if (stagingLogList != null && !stagingLogList.isEmpty()) {
				lastSetting = (stagingLogList.get(stagingLogList.size() - 1));
			}
		} catch (POException e) {
			LOGGER.error(e);
		}
		return lastSetting;
	}

	@Override
	public List<AccessLogsSetting> getAllStagingLog() {
		try {
			return accessLogSettingRepository.findAll();
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}
}