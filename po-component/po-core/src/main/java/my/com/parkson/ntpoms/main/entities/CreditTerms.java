package my.com.parkson.ntpoms.main.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import my.com.parkson.ntpoms.main.entities.CreditTerms.CreditTermsPk;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "\"mf02_m_creditterms\"")
@Data
@NoArgsConstructor
public class CreditTerms implements Serializable {

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	CreditTermsId crtId;

	@Column(name = "\"mf02_credittermsdescription\"")
	private String description;

	@Column(name = "mf02_createdby")
	private String createdby;

	@Column(name = "mf02_createdon")
	private Date createdon;

	@Column(name = "mf02_lastmodifiedby")
	private String lastmodifiedby;

	@Column(name = "\"mf02_lastmodifiedon\"")
	private Date lastModifiedOn;

	@Column(name = "mf02_deactivatedby")
	private String deactivatedby;

	@Column(name = "mf02_deactivatedon")
	private Date deactivatedon;

	@Column(name = "mf02_reactivatedby")
	private String reactivatedby;

	@Column(name = "mf02_reactivatedon")
	private Date reactivatedon;

	@Column(name = "\"mf02_isactive\"")
	private boolean isActive;

	


	public CreditTermsId getCrtId() {
		return crtId;
	}

	public void setCrtId(CreditTermsId crtId) {
		this.crtId = crtId;
	}

	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public Date getCreatedon() {
		return createdon;
	}

	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}

	public String getLastmodifiedby() {
		return lastmodifiedby;
	}

	public void setLastmodifiedby(String lastmodifiedby) {
		this.lastmodifiedby = lastmodifiedby;
	}

	public Date getLastModifiedOn() {
		return lastModifiedOn;
	}

	public void setLastModifiedOn(Date lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

	public String getDeactivatedby() {
		return deactivatedby;
	}

	public void setDeactivatedby(String deactivatedby) {
		this.deactivatedby = deactivatedby;
	}

	public Date getDeactivatedon() {
		return deactivatedon;
	}

	public void setDeactivatedon(Date deactivatedon) {
		this.deactivatedon = deactivatedon;
	}

	public String getReactivatedby() {
		return reactivatedby;
	}

	public void setReactivatedby(String reactivatedby) {
		this.reactivatedby = reactivatedby;
	}

	public Date getReactivatedon() {
		return reactivatedon;
	}

	public void setReactivatedon(Date reactivatedon) {
		this.reactivatedon = reactivatedon;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(crtId.getCode() + "-" + description);
		return sb.toString();
	}

	public static class CreditTermsPk implements Serializable {

		private static final long serialVersionUID = 1L;

		protected String code;

		protected String companycode;

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public String getCompanyCode() {
			return companycode;
		}

		public void setCompanyCode(String companyCode) {
			this.companycode = companyCode;
		}

		public CreditTermsPk() {

		}

		public CreditTermsPk(String code, String companyCode) {
			this.code = code;
			this.companycode = companyCode;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((code == null) ? 0 : code.hashCode());
			result = prime * result + ((companycode == null) ? 0 : companycode.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			CreditTermsPk other = (CreditTermsPk) obj;

			if (code == null) {
				if (other.code != null)
					return false;
			} else if (!code.equals(other.code))
				return false;
			if (companycode == null) {
				if (other.companycode != null)
					return false;
			} else if (!companycode.equals(other.companycode))
				return false;

			return true;
		}
	}
}
