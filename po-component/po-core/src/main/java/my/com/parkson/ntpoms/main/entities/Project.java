package my.com.parkson.ntpoms.main.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "\"po03_t_project\"")
public class Project implements Serializable {


	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "\"po03_projectCode\"")
	private String code;

	@Column(name = "\"po03_projectName\"")
	private String name;

	@Column(name = "\"po03_projectDescription\"")
	private String description;

	@Column(name = "\"po03_projectHead\"")
	private String head;

	@Column(name = "\"po03_projectTeamMember\"")
	private String teamMember;

	@Column(name = "\"mf13MPCMast_mf13_pcCode\"")
	private String pcCode;

	@Column(name = "\"po03_completionDate\"")
	private Date completionDate;

	@Column(name = "\"po03_createdBy\"")
	private String createdBy;

	@Column(name = "\"po03_createdOn\"")
	private Date createdOn;

	@Column(name = "\"po03_lastModifiedBy\"")
	private String lastModifiedBy;

	@Column(name = "\"po03_lastModifiedOn\"")
	private Date lastModifiedOn;

	@Column(name = "\"po03_deactivatedBy\"")
	private String deactivatedBy;

	@Column(name = "\"po03_deactivatedOn\"")
	private Date deactivatedOn;

	@Column(name = "\"po03_reactivatedBy\"")
	private String reactivatedBy;

	@Column(name = "\"po03_reactivatedOn\"")
	private Date reactivatedOn;

	@Column(name = "\"po03_isActive\"")
	private boolean isActive;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getHead() {
		return head;
	}

	public void setHead(String head) {
		this.head = head;
	}

	public String getTeamMember() {
		return teamMember;
	}

	public void setTeamMember(String teamMember) {
		this.teamMember = teamMember;
	}

	public String getPcCode() {
		return pcCode;
	}

	public void setPcCode(String pcCode) {
		this.pcCode = pcCode;
	}

	public Date getCompletionDate() {
		return completionDate;
	}

	public void setCompletionDate(Date completionDate) {
		this.completionDate = completionDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public Date getLastModifiedOn() {
		return lastModifiedOn;
	}

	public void setLastModifiedOn(Date lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

	public String getDeactivatedBy() {
		return deactivatedBy;
	}

	public void setDeactivatedBy(String deactivatedBy) {
		this.deactivatedBy = deactivatedBy;
	}

	public Date getDeactivatedOn() {
		return deactivatedOn;
	}

	public void setDeactivatedOn(Date deactivatedOn) {
		this.deactivatedOn = deactivatedOn;
	}

	public String getReactivatedBy() {
		return reactivatedBy;
	}

	public void setReactivatedBy(String reactivatedBy) {
		this.reactivatedBy = reactivatedBy;
	}

	public Date getReactivatedOn() {
		return reactivatedOn;
	}

	public void setReactivatedOn(Date reactivatedOn) {
		this.reactivatedOn = reactivatedOn;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public String toString () {		
		return code + " - " + name;
	}
}
