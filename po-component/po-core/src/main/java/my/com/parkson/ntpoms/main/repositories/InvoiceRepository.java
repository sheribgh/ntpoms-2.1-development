package my.com.parkson.ntpoms.main.repositories;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import my.com.parkson.ntpoms.main.entities.Invoice;

public interface InvoiceRepository extends JpaRepository<Invoice, Integer>, JpaSpecificationExecutor<Invoice> {

	@Query("select o from Invoice o where o.purchaseOrder = ?1")
	public List<Invoice> findAllByPurchaseOrderNo(String poNo);

	public Invoice findByInvoiceNo(String invoiceNo);

	public List<Invoice> findAll();
	

	public List<Invoice> findAllByPurchaseOrder(String poNo);

	public Invoice findByPoInvoiceNo(String poInvoiceNo);
}