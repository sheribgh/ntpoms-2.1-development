/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import my.com.parkson.ntpoms.common.model.EmailCriteria;

import javax.mail.MessagingException;
import javax.mail.Session;

/**
 * The Interface EmailService.
 */
public interface EmailService {

	/**
	 * Send email.
	 *
	 * @param context the context
	 * @param EmailCriteria the email criteria
	 */
	public void sendEmail(Session session, Map<String, String> map,Map<String,List> maplist, EmailCriteria emailCriteria) throws MessagingException, IOException;
	
}