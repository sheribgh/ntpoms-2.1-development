/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.entities.OrderSubCategory;
import my.com.parkson.ntpoms.common.repositories.OrderSubCategoryRepository;
import my.com.parkson.ntpoms.common.services.MessageService;
import my.com.parkson.ntpoms.common.services.OrderSubCategoryService;

/**
 * The Class OrderSubCategoryServiceImpl.
 */
@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class OrderSubCategoryServiceImpl implements OrderSubCategoryService {

	private static final Logger LOGGER = Logger.getLogger(OrderSubCategoryServiceImpl.class);

	/** The messages. */
	@Autowired
	private MessageService messages;

	/** The order sub category repository. */
	@Autowired
	private OrderSubCategoryRepository orderSubCategoryRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.OrderSubCategoryService#
	 * getAllOrderSubCategory()
	 */
	@Override
	public List<OrderSubCategory> getAllOrderSubCategory() {
		List<OrderSubCategory> orderSubCategoryList = new ArrayList<>();
		try {
			orderSubCategoryList = orderSubCategoryRepository.findAllByOrderByLastModifiedOnDesc();
		} catch (POException e) {
			LOGGER.error(e);
		}
		return orderSubCategoryList;
	}

	@Override
	public long getCount() {
		return orderSubCategoryRepository.count();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.OrderSubCategoryService#
	 * getAllActiveOrderSubCategory()
	 */
	@Override
	public List<OrderSubCategory> getAllActiveOrderSubCategory() {
		List<OrderSubCategory> orderSubCategoryList = new ArrayList<>();
		try {
			orderSubCategoryList = orderSubCategoryRepository.findAllByIsActiveOrderByLastModifiedOnDesc(true);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return orderSubCategoryList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.OrderSubCategoryService#
	 * getOrderSubCategoryByCode(java.lang.String)
	 */
	@Override
	public OrderSubCategory getOrderSubCategoryByCode(String code) {
		try {
			return orderSubCategoryRepository.findByCode(code);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.OrderSubCategoryService#
	 * createOrderSubCategory(my.com.parkson.ntpoms.common.entities.
	 * OrderSubCategory)
	 */
	@Override
	public void createOrderSubCategory(OrderSubCategory orderSubCategory) {
		try {
			orderSubCategoryRepository.save(orderSubCategory);
		} catch (NonUniqueResultException e) {
			LOGGER.error(e);
		} catch (PersistenceException e) {
			LOGGER.error(e);
		} catch (POException e) {
			LOGGER.error(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.OrderSubCategoryService#
	 * getAllOrderSubCategoryByOrderCategoryCode(java.lang.String)
	 */
	@Override
	public List<OrderSubCategory> getAllOrderSubCategoryByOrderCategoryCode(String orderCategoryCode) {
		List<OrderSubCategory> orderSubCategoryList = new ArrayList<>();
		try {
			orderSubCategoryList = orderSubCategoryRepository.findAllByOrderCategoryCode(orderCategoryCode);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return orderSubCategoryList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.OrderSubCategoryService#
	 * getAllActiveOrderSubCategoryByOrderCategoryCode(java.lang.String)
	 */
	@Override
	public List<OrderSubCategory> getAllActiveOrderSubCategoryByOrderCategoryCode(String orderCategoryCode) {
		List<OrderSubCategory> orderSubCategoryList = new ArrayList<>();
		try {
			orderSubCategoryList = orderSubCategoryRepository.findAllActiveByOrderCategoryCode(orderCategoryCode);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return orderSubCategoryList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.OrderSubCategoryService#
	 * getAllPageableOrderSubCategory(org.springframework.data.domain.Pageable)
	 */
	@Override
	public Page<OrderSubCategory> getAllPageableOrderSubCategory(Pageable pageRequest) {
		return orderSubCategoryRepository.findAll(pageRequest);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.OrderSubCategoryService#
	 * getAllPageableOrderSubCategory(org.springframework.data.jpa.domain.
	 * Specification, org.springframework.data.domain.Pageable)
	 */
	@Override
	public Page<OrderSubCategory> getAllPageableOrderSubCategory(Specification<OrderSubCategory> s,
			Pageable pageRequest) {
		return orderSubCategoryRepository.findAll(s, pageRequest);
	}

	@Override
	public Page<OrderSubCategory> getAllPageableOrderSubCategoryOrdered(Pageable pageRequest) {
		return orderSubCategoryRepository.findAllByOrderByLastModifiedOnDesc(pageRequest);
	}

	@Override
	public Page<OrderSubCategory> getAllPageableOrderSubCategoryOrdered(Specification<OrderSubCategory> s,
			Pageable pageRequest) {
		return orderSubCategoryRepository.findAllByOrderByLastModifiedOnDesc(s, pageRequest);
	}
}