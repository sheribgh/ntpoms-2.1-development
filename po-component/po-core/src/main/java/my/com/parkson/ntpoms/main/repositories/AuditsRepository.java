/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
/* Name : audit_Service                                      */
/* Usage: Code for the audit service                        */
/* Description: This code creates an interface for audit that will */
/*              used for connecting to data. This code*/
/*              interacts with the table <audit>.        */
/*************************************************************/

package my.com.parkson.ntpoms.main.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import my.com.parkson.ntpoms.main.entities.Audits;


/**
 * The Interface AuditRepository.
 */
public interface AuditsRepository extends JpaRepository<Audits, Integer>, JpaSpecificationExecutor<Audits> {

	/**
	 * Find all by order by created on desc.
	 *
	 * @return the list
	 */
	public List<Audits> findAllByOrderByCreatedOnDesc();

	/**
	 * Find by search code.
	 *
	 * @param searchval the searchval
	 * @param pageRequest the page request
	 * @return the page
	 */
	@Query("SELECT o FROM Audits o where o.username LIKE %:searchval% OR o.message LIKE %:searchval% OR o.operation LIKE %:searchval%")
	public Page<Audits> findBySearchCode(@Param("searchval") String searchval, Pageable pageRequest);
}