package my.com.parkson.ntpoms.main.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import my.com.parkson.ntpoms.main.entities.PurchaseOrderUserAccess;
import my.com.parkson.ntpoms.main.entities.PurchaseOrderUserAccessPk;

public interface PurchaseOrderUserAccessRepository extends JpaRepository<PurchaseOrderUserAccess, Integer> , JpaSpecificationExecutor<PurchaseOrderUserAccess> {
	
    public PurchaseOrderUserAccess findByPurchaseorderuseraccessIdPsID(String psid);

	public List<PurchaseOrderUserAccess> findAllByPurchaseorderuseraccessIdPsIDAndPurchaseorderuseraccessIdPoAction(String psID, String poAction);
	
	@Query("SELECT o FROM PurchaseOrderUserAccess o where o.purchaseorderuseraccessId.psID = :psID AND (o.purchaseorderuseraccessId.poAction = 'D' OR o.purchaseorderuseraccessId.poAction = 'V')")
	public List<PurchaseOrderUserAccess> findAllByPsIDAndPoActionForRedraft(@Param("psID") String psID);
	
	@Query("SELECT o FROM PurchaseOrderUserAccess o where o.purchaseorderuseraccessId.psID = :psID AND (o.purchaseorderuseraccessId.poAction = 'A' OR o.purchaseorderuseraccessId.poAction = 'V')")
	public List<PurchaseOrderUserAccess> findAllByPsIDAndPoActionForUpdate(@Param("psID") String psID);
	
	@Query("SELECT o FROM PurchaseOrderUserAccess o where o.purchaseorderuseraccessId.pono = :pono AND o.purchaseorderuseraccessId.poAction = 'A'")
	public PurchaseOrderUserAccess findAllByPoNoAndPoActionForUpdate(@Param("pono") String pono);
	
	public List<PurchaseOrderUserAccess> findAllByPurchaseorderuseraccessIdPsID(String psID);
	public PurchaseOrderUserAccess findByPurchaseorderuseraccessId(PurchaseOrderUserAccessPk purchaseorderuseraccessId);
}