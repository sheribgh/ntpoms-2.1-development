/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OrderBy;

/**
 * The Class MenuGroupRole.
 */
@Entity
@Table(name = "menu_grp_role")
public class MenuGroupRole implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	@Id
	@Column(name="id")
	private Integer id;
	
	/** The role group. */
	@ManyToOne()
	@JoinColumn(name="role_id")
	public Role roleGroup;
	
	/** The menu group. */
	@ManyToOne()
	@JoinColumn(name="menu_grp_id")
	@OrderBy(clause = "menu_sequence desc")
	public MenuGroup menuGroup;

	/**
	 * Gets the role group.
	 *
	 * @return the role group
	 */
	public Role getRoleGroup() {
		return roleGroup;
	}

	/**
	 * Sets the role group.
	 *
	 * @param roleGroup the new role group
	 */
	public void setRoleGroup(Role roleGroup) {
		this.roleGroup = roleGroup;
	}

	/**
	 * Gets the menu group.
	 *
	 * @return the menu group
	 */
	public MenuGroup getMenuGroup() {
		return menuGroup;
	}

	/**
	 * Sets the menu group.
	 *
	 * @param menuGroup the new menu group
	 */
	public void setMenuGroup(MenuGroup menuGroup) {
		this.menuGroup = menuGroup;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MenuGroupRole [id=" + id + "]";
	}
}