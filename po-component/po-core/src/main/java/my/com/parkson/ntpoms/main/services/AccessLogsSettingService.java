package my.com.parkson.ntpoms.main.services;

import java.util.List;

import my.com.parkson.ntpoms.main.entities.AccessLogsSetting;

public interface AccessLogsSettingService {

	public int getLastAccessLogSetting();

	public void createAccessLogSetting(AccessLogsSetting accessLogSetting);

	public AccessLogsSetting getLastStagingLog();

	public List<AccessLogsSetting> getAllStagingLog();
}