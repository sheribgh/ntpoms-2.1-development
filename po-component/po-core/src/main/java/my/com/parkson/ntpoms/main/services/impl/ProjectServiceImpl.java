package my.com.parkson.ntpoms.main.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import groovy.util.logging.Slf4j;

import my.com.parkson.ntpoms.common.services.AccessLogService;
import my.com.parkson.ntpoms.main.entities.Project;
import my.com.parkson.ntpoms.main.repositories.ProjectRepository;
import my.com.parkson.ntpoms.main.services.ProjectService;

@Slf4j
@Service
@Transactional
public class ProjectServiceImpl implements ProjectService {

	private static final Logger LOGGER = Logger.getLogger(ProjectServiceImpl.class);

	@Autowired
	private ProjectRepository projectRepository;

	@Override
	public void createProject(Project project) {
		try {
			projectRepository.save(project);
		} catch (Exception e) {
			LOGGER.error(e);
		}
	}

	@Override
	public Project getProjectByCode(String code) {
		try {
			return projectRepository.findByCode(code);
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return null;
	}

	@Override
	public Project getLastCreatedProject() {
		try {
			return null;
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return null;
	}

	@Override
	public List<Project> getAllProject() {
		List<Project> projectList = new ArrayList<>();
		try {
			projectList = projectRepository.findAll();
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return projectList;
	}

	@Override
	public List<Project> getAllActiveProject() {
		List<Project> projectList = new ArrayList<>();
		try {
			projectList = projectRepository.findAllActive(true, new Date());
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return projectList;
	}

	@Override
	public List<Project> getActiveProjectByProfitCenterCode(String profitCenterCode) {
		List<Project> projectList = new ArrayList<>();
		try {
			projectList = projectRepository.findAllActiveByProfitCenterCode(true, new Date(), profitCenterCode);
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return projectList;
	}

	@Override
	public Page<Project> getPagebleAll(Pageable req) {
		return projectRepository.findAll(req);
	}

	@Override
	public Page<Project> getAllPageableProject(Pageable pageRequest) {
		try {
			return projectRepository.findAll(pageRequest);
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return null;
	}

	@Override
	public Page<Project> getAllPageableProject(Specification<Project> s, Pageable pageRequest) {

		try {
			return projectRepository.findAll(s, pageRequest);
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return null;
	}

	@Override
	public Page<Project> getAllPageableProjectOrdered(Pageable pageRequest) {
		try {
			return projectRepository.findAllByOrderByLastModifiedOnDesc(pageRequest);
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return null;
	}

	@Override
	public Page<Project> getAllPageableProjectOrdered(Specification<Project> s, Pageable pageRequest) {
		try {
			return projectRepository.findAllByOrderByLastModifiedOnDesc(s, pageRequest);
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return null;
	}

	@Override
	public long getCount() {
		return projectRepository.count();
	}

	@Override
	public long getCountActive() {
		List<Project> projectList = new ArrayList<>();
		try {
			projectList = projectRepository.findAllByIsActiveOrderByLastModifiedOnDesc(true);
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return projectList.size();
	}
}