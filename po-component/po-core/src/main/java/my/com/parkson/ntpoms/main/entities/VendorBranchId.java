package my.com.parkson.ntpoms.main.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Data;

@Data
@Embeddable
public class VendorBranchId implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Column(name = "\"mf01_vendorcode\"")
	private String code;

	@Column(name = "\"mf01_vendorbranchcode\"")
	private String branchCode;

	@Column(name="\"mf11mcompmast_mf01_compcode\"")
	private String companyCode;
	

	public VendorBranchId() {
		
	}
	
	public VendorBranchId(String code , String branchCode , String companyCode) {
		
		this.code = code;
		this.branchCode = branchCode;
		this.companyCode = companyCode;
		
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}	

}