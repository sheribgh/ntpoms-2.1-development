/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.entities;

import java.io.Serializable;

/**
 * The Class ProfitCenterParseToString.
 */
public class ProfitCenterParseToString implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4622808600663329443L;
	
	/** The emp string. */
	private String empString;

	/**
	 * Gets the emp string.
	 *
	 * @return the emp string
	 */
	public String getEmpString() {
		return empString;
	}

	/**
	 * Sets the emp string.
	 *
	 * @param empString the new emp string
	 */
	public void setEmpString(String empString) {
		this.empString = empString;
	}
	

}
