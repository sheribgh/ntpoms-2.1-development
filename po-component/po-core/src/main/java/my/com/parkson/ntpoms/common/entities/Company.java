/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The Class Company.
 */
@Entity
@Table(name = "\"MF11_M_COMPMAST\"")
@Data
@NoArgsConstructor
public class Company implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The code. */
	@Id
	@Column(name = "\"mf11_compCode\"")
	private String code;
	
	/** The code HRIS. */
	@Column(name = "\"mf11_compCodeHRIS\"")
	private String codeHRIS;
	
	/** The abbr name. */
	@Column(name = "\"mf11_compAbbrName\"")
	private String abbrName;
	
	/** The reg no. */
	@Column(name = "\"mf11_compRegNo\"")
	private String regNo;
	
	/** The name. */
	@Column(name = "\"mf11_compName\"")
	private String name;
	
	/** The logo. */
	@Column(name = "\"mf11_compLogo\"")
	private String logo;
	
	/** The is active. */
	@Column(name = "\"mf11_isActive\"")
	private boolean isActive;
		
	/** The created on. */
	@Column(name = "\"mf11_createdOn\"")
	private Date createdOn;
	
	/** The created by. */
	@Column(name = "\"mf11_createdBy\"")
	private String createdBy;
	
	/** The last modified on. */
	@Column(name = "\"mf11_lastModifiedOn\"")
	private Date lastModifiedOn;
	
	/** The last modified by. */
	@Column(name = "\"mf11_lastModifiedBy\"")
	private String lastModifiedBy;
	
	/** The letter heading. */
	@Column(name = "\"mf11_companyLetterHeading\"")
	private String letterHeading;
	
	/** The po terms and conditions portrait. */
	@Column(name = "\"mf11_poTAndCPortrait\"")
	private String poTermsAndConditionsPortrait;
	
	/** The po terms and conditions landscape. */
	@Column(name = "\"mf11_poTAndCLandscape\"")
	private String poTermsAndConditionsLandscape;
	
	/** The deactivated by. */
	@Column(name = "\"mf11_deactivatedBy\"")
	private String deactivatedBy;
	
	/** The deactivated on. */
	@Column(name = "\"mf11_deactivatedOn\"")
	private Date deactivatedOn;
	
	/** The reactivated by. */
	@Column(name = "\"mf11_reactivatedBy\"")
	private String reactivatedBy;
	
	/** The reactivated on. */
	@Column(name = "\"mf11_reactivatedOn\"")
	private Date reactivatedOn;

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * Gets the code HRIS.
	 *
	 * @return the code HRIS
	 */
	public String getCodeHRIS() {
		return codeHRIS;
	}

	/**
	 * Sets the code HRIS.
	 *
	 * @param codeHRIS the new code HRIS
	 */
	public void setCodeHRIS(String codeHRIS) {
		this.codeHRIS = codeHRIS;
	}

	/**
	 * Gets the abbr name.
	 *
	 * @return the abbr name
	 */
	public String getAbbrName() {
		return abbrName;
	}

	/**
	 * Sets the abbr name.
	 *
	 * @param abbrName the new abbr name
	 */
	public void setAbbrName(String abbrName) {
		this.abbrName = abbrName;
	}

	/**
	 * Gets the reg no.
	 *
	 * @return the reg no
	 */
	public String getRegNo() {
		return regNo;
	}

	/**
	 * Sets the reg no.
	 *
	 * @param regNo the new reg no
	 */
	public void setRegNo(String regNo) {
		this.regNo = regNo;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the logo.
	 *
	 * @return the logo
	 */
	public String getLogo() {
		return logo;
	}

	/**
	 * Sets the logo.
	 *
	 * @param logo the new logo
	 */
	public void setLogo(String logo) {
		this.logo = logo;
	}

	/**
	 * Gets the letter heading.
	 *
	 * @return the letter heading
	 */
	public String getLetterHeading() {
		return letterHeading;
	}

	/**
	 * Sets the letter heading.
	 *
	 * @param letterHeading the new letter heading
	 */
	public void setLetterHeading(String letterHeading) {
		this.letterHeading = letterHeading;
	}

	/**
	 * Gets the po terms and conditions portrait.
	 *
	 * @return the po terms and conditions portrait
	 */
	public String getPoTermsAndConditionsPortrait() {
		return poTermsAndConditionsPortrait;
	}

	/**
	 * Sets the po terms and conditions portrait.
	 *
	 * @param poTermsAndConditionsPortrait the new po terms and conditions portrait
	 */
	public void setPoTermsAndConditionsPortrait(String poTermsAndConditionsPortrait) {
		this.poTermsAndConditionsPortrait = poTermsAndConditionsPortrait;
	}

	/**
	 * Gets the po terms and conditions landscape.
	 *
	 * @return the po terms and conditions landscape
	 */
	public String getPoTermsAndConditionsLandscape() {
		return poTermsAndConditionsLandscape;
	}

	/**
	 * Sets the po terms and conditions landscape.
	 *
	 * @param poTermsAndConditionsLandscape the new po terms and conditions landscape
	 */
	public void setPoTermsAndConditionsLandscape(String poTermsAndConditionsLandscape) {
		this.poTermsAndConditionsLandscape = poTermsAndConditionsLandscape;
	}

	/**
	 * Checks if is active.
	 *
	 * @return true, if is active
	 */
	public boolean isActive() {
		return isActive;
	}

	/**
	 * Sets the active.
	 *
	 * @param isActive the new active
	 */
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	/**
	 * Gets the created on.
	 *
	 * @return the created on
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * Sets the created on.
	 *
	 * @param createdOn the new created on
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * Gets the created by.
	 *
	 * @return the created by
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * Sets the created by.
	 *
	 * @param createdBy the new created by
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Gets the last modified on.
	 *
	 * @return the last modified on
	 */
	public Date getLastModifiedOn() {
		return lastModifiedOn;
	}

	/**
	 * Sets the last modified on.
	 *
	 * @param lastModifiedOn the new last modified on
	 */
	public void setLastModifiedOn(Date lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

	/**
	 * Gets the last modified by.
	 *
	 * @return the last modified by
	 */
	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	/**
	 * Sets the last modified by.
	 *
	 * @param lastModifiedBy the new last modified by
	 */
	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	/**
	 * Gets the deactivated by.
	 *
	 * @return the deactivated by
	 */
	public String getDeactivatedBy() {
		return deactivatedBy;
	}

	/**
	 * Sets the deactivated by.
	 *
	 * @param deactivatedBy the new deactivated by
	 */
	public void setDeactivatedBy(String deactivatedBy) {
		this.deactivatedBy = deactivatedBy;
	}

	/**
	 * Gets the deactivated on.
	 *
	 * @return the deactivated on
	 */
	public Date getDeactivatedOn() {
		return deactivatedOn;
	}

	/**
	 * Sets the deactivated on.
	 *
	 * @param deactivatedOn the new deactivated on
	 */
	public void setDeactivatedOn(Date deactivatedOn) {
		this.deactivatedOn = deactivatedOn;
	}

	/**
	 * Gets the reactivated by.
	 *
	 * @return the reactivated by
	 */
	public String getReactivatedBy() {
		return reactivatedBy;
	}

	/**
	 * Sets the reactivated by.
	 *
	 * @param reactivatedBy the new reactivated by
	 */
	public void setReactivatedBy(String reactivatedBy) {
		this.reactivatedBy = reactivatedBy;
	}

	/**
	 * Gets the reactivated on.
	 *
	 * @return the reactivated on
	 */
	public Date getReactivatedOn() {
		return reactivatedOn;
	}

	/**
	 * Sets the reactivated on.
	 *
	 * @param reactivatedOn the new reactivated on
	 */
	public void setReactivatedOn(Date reactivatedOn) {
		this.reactivatedOn = reactivatedOn;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return code;
	}
}