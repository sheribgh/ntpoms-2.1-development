package my.com.parkson.ntpoms.main.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.main.entities.PurchaseOrderArchiveDetails;
import my.com.parkson.ntpoms.main.repositories.PurchaseOrderArchiveRepository;
import my.com.parkson.ntpoms.main.services.PurchaseOrderArchiveService;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class PurchaseOrderArchiveServiceImpl implements PurchaseOrderArchiveService {

	@Autowired
	private PurchaseOrderArchiveRepository purchaseOrderArchiveRepository;
	
	
	@Override
	public Page<PurchaseOrderArchiveDetails> getAllPageablePurchaseOrder(Specification<PurchaseOrderArchiveDetails> s,
			Pageable pageRequest) {
		return purchaseOrderArchiveRepository.findAll(s,pageRequest);
	}


	@Override
	public Page<PurchaseOrderArchiveDetails> getAllPageablePurchaseOrder(Pageable pageRequest) {
		return purchaseOrderArchiveRepository.findAll(pageRequest);
	}

}
