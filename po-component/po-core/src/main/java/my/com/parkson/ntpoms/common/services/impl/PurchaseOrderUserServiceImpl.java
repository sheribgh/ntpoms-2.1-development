/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.entities.PurchaseOrderUser;
import my.com.parkson.ntpoms.common.repositories.PurchaseOrderUserRepository;
import my.com.parkson.ntpoms.common.services.PurchaseOrderUserService;

/**
 * The Class PurchaseOrderUserServiceImpl.
 */
@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class PurchaseOrderUserServiceImpl implements PurchaseOrderUserService {

	private static final Logger LOGGER = Logger.getLogger(PurchaseOrderUserServiceImpl.class);

	/** The purchase order user repository. */
	@Autowired
	private PurchaseOrderUserRepository purchaseOrderUserRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.PurchaseOrderUserService#
	 * createPurchaseOrderUser(my.com.parkson.ntpoms.common.entities.
	 * PurchaseOrderUser)
	 */
	@Override
	public void createPurchaseOrderUser(PurchaseOrderUser purchaseOrderUser) {
		try {
			purchaseOrderUserRepository.save(purchaseOrderUser);
		} catch (POException e) {
			LOGGER.error(e);
		}
	}

	@Override
	public long getCount() {
		return purchaseOrderUserRepository.count();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.PurchaseOrderUserService#
	 * getPurchaseOrderUserById(java.lang.String)
	 */
	@Override
	public PurchaseOrderUser getPurchaseOrderUserById(String id) {
		try {
			return purchaseOrderUserRepository.findById(id);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.PurchaseOrderUserService#
	 * getAllPurchaseOrderUser()
	 */
	@Override
	public List<PurchaseOrderUser> getAllPurchaseOrderUser() {
		List<PurchaseOrderUser> purchaseOrderUserList = new ArrayList<>();
		try {
			purchaseOrderUserList = purchaseOrderUserRepository.findAllByOrderByLastModifiedOnDesc();
		} catch (POException e) {
			LOGGER.error(e);
		}
		return purchaseOrderUserList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.PurchaseOrderUserService#
	 * getAllActivePurchaseOrderUser()
	 */
	@Override
	public List<PurchaseOrderUser> getAllActivePurchaseOrderUser() {
		List<PurchaseOrderUser> purchaseOrderUserList = new ArrayList<>();
		try {
			purchaseOrderUserList = purchaseOrderUserRepository.findAllByActiveOrderByLastModifiedOnDesc(true);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return purchaseOrderUserList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.PurchaseOrderUserService#
	 * getAllPurchaseOrderUserIds()
	 */
	@Override
	public List<String> getAllPurchaseOrderUserIds() {
		List<String> purchaseOrderUserIds = new ArrayList<>();
		try {
			purchaseOrderUserIds = purchaseOrderUserRepository.getAllIds();
		} catch (POException e) {
			LOGGER.error(e);
		}
		return purchaseOrderUserIds;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.PurchaseOrderUserService#
	 * getAllPageablePurchaseOrderUser(org.springframework.data.domain.Pageable)
	 */
	@Override
	public Page<PurchaseOrderUser> getAllPageablePurchaseOrderUser(Pageable pageRequest) {
		return purchaseOrderUserRepository.findAll(pageRequest);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.PurchaseOrderUserService#
	 * getAllPageablePurchaseOrderUser(org.springframework.data.jpa.domain.
	 * Specification, org.springframework.data.domain.Pageable)
	 */
	@Override
	public Page<PurchaseOrderUser> getAllPageablePurchaseOrderUser(Specification<PurchaseOrderUser> s,
			Pageable pageRequest) {
		return purchaseOrderUserRepository.findAll(s, pageRequest);
	}

	@Override
	public Page<PurchaseOrderUser> getAllPageablePurchaseOrderUserOrdered(Pageable pageRequest) {
		return purchaseOrderUserRepository.findAllByOrderByLastModifiedOnDesc(pageRequest);
	}

	@Override
	public Page<PurchaseOrderUser> getAllPageablePurchaseOrderUserOrdered(Specification<PurchaseOrderUser> s,
			Pageable pageRequest) {
		return purchaseOrderUserRepository.findAllByOrderByLastModifiedOnDesc(s, pageRequest);
	}

	@Override
	public long getCountActive() {
		List<PurchaseOrderUser> purchaseOrderUserList = new ArrayList<>();
		purchaseOrderUserList = purchaseOrderUserRepository.findAllByActiveOrderByLastModifiedOnDesc(true);
		return purchaseOrderUserList.size();
	}
}