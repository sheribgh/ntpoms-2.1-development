/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
/* Name			:	MF20_M_ADMINMAST_ServiceImpl											*/
/* Usage		:	Creating service layer for Admin									*/
/* Description	:	This code creates a connection between Admin Screen				*/
/* 					and Admin table in database. This code interacts with				*/
/* 					the table MF20_M_ADMINMAST											*/
/****************************************************************************************/

package my.com.parkson.ntpoms.common.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.entities.User;
import my.com.parkson.ntpoms.common.entities.UserRole;
import my.com.parkson.ntpoms.common.repositories.UserRoleRepository;
import my.com.parkson.ntpoms.common.repositories.UsersRepository;
import my.com.parkson.ntpoms.common.services.MessageService;
import my.com.parkson.ntpoms.common.services.UsersService;

/**
 * The Class UserServiceImpl.
 */
@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class UserServiceImpl implements UsersService {

	private static final Logger LOGGER = Logger.getLogger(UserServiceImpl.class);

	/** The messages. */
	@Autowired
	private MessageService messages;

	/** The users repository. */
	@Autowired
	private UsersRepository usersRepository;

	@Autowired
	private UserRoleRepository userRoleRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.com.parkson.ntpoms.common.services.UsersService#createUsers(my.com.
	 * parkson.ntpoms.common.entities.User)
	 */
	@Override
	public void createUsers(User users) {
		try {
			usersRepository.save(users);
		} catch (POException e) {
			LOGGER.error(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.com.parkson.ntpoms.common.services.UsersService#createUsers(my.com.
	 * parkson.ntpoms.common.entities.User)
	 */
	@Override
	public boolean saveUser(User user) {
		try {
			usersRepository.save(user);
			return true;
		} catch (POException e) {
			LOGGER.error(e);
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.com.parkson.ntpoms.common.services.UsersService#getUsersByUsername(
	 * java.lang.String)
	 */
	@Override
	public User getUsersByUsername(String username) {
		try {
			return usersRepository.findByUsername(username);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.UsersService#
	 * getAllUsersExcludeSuperadmin()
	 */
	@Override
	public List<User> getAllUsersExcludeSuperadmin() {
		List<User> usersList = new ArrayList<>();
		try {
			usersList = usersRepository.findAllExceptSuperadmin();
		} catch (POException e) {
			LOGGER.error(e);
		}
		return usersList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.UsersService#getAllUsers()
	 */
	@Override
	public List<User> getAllUsers() {
		List<User> usersList = new ArrayList<>();
		try {
			usersList = usersRepository.findAll();
		} catch (POException e) {
			LOGGER.error(e);
		}
		return usersList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.com.parkson.ntpoms.common.services.UsersService#getAllActiveUsers()
	 */
	@Override
	public List<User> getAllActiveUsers() {
		List<User> usersList = new ArrayList<>();
		try {
			usersList = usersRepository.finaAllByIsActiveOrderByLastModifiedOnDesc(true);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return usersList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.com.parkson.ntpoms.common.services.UsersService#getAllPagableUsers(org
	 * .springframework.data.domain.Pageable)
	 */
	@Override
	public Page<User> getAllPagableUsers(Pageable pageRequest) {
		return usersRepository.findAll(pageRequest);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.com.parkson.ntpoms.common.services.UsersService#getAllPagableUsers(org
	 * .springframework.data.jpa.domain.Specification,
	 * org.springframework.data.domain.Pageable)
	 */
	@Override
	public Page<User> getAllPagableUsers(Specification<User> s, Pageable pageRequest) {
		return usersRepository.findAll(s, pageRequest);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.CompanyService#
	 * getAllPageableCompany(org.springframework.data.jpa.domain.Specification,
	 * org.springframework.data.domain.Pageable)
	 */
	@Override
	public Page<User> getAllPageableUserOrdered(Pageable pageRequest) {
		return usersRepository.findAllByOrderByLastModifiedOnDesc(pageRequest);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.CompanyService#
	 * getAllPageableCompany(org.springframework.data.jpa.domain.Specification,
	 * org.springframework.data.domain.Pageable)
	 */
	@Override
	public Page<User> getAllPageableUserOrdered(Specification<User> s, Pageable pageRequest) {
		return usersRepository.findAllByOrderByLastModifiedOnDesc(s, pageRequest);
	}

	@Override
	public User getUsersByUserEmail(String email) {
		try {
			return usersRepository.findByEmail(email);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	@Override
	public List<User> getUsersByUsersEmail(String email) {

		List<User> list = usersRepository.findUserByEmail(email);
		try {
			return list;
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	@Override
	public void addUserRole(UserRole role) {
		try {
			userRoleRepository.save(role);
		} catch (POException e) {
			LOGGER.error(e);
		}
	}

	@Override
	public UserRole getUsersByUser(String username) {

		UserRole userRole = null;
		try {
			userRole = userRoleRepository.findByUser(username);
		} catch (POException e) {
			LOGGER.error(e);
		}

		return userRole;
	}

	@Override
	public long getCount() {
		return usersRepository.count();
	}

	@Override
	public long getCountActive() {
		List<User> usersList = usersRepository.finaAllByIsActiveOrderByLastModifiedOnDesc(true);
		return usersList.size();
	}

}