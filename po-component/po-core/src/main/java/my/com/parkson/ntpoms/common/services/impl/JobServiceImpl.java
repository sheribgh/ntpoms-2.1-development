/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.entities.Job;
import my.com.parkson.ntpoms.common.repositories.JobRepository;
import my.com.parkson.ntpoms.common.services.JobService;
import my.com.parkson.ntpoms.common.services.MessageService;

/**
 * The Class JobServiceImpl.
 */
@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class JobServiceImpl implements JobService {

	private static final Logger LOGGER = Logger.getLogger(JobServiceImpl.class);

	/** The messages. */
	@Autowired
	private MessageService messages;

	/** The job repository. */
	@Autowired
	private JobRepository jobRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.JobService#getAllJob()
	 */
	@Override
	public List<Job> getAllJob() {
		List<Job> jobList = new ArrayList<>();
		try {
			jobList = jobRepository.findAllByOrderByLastModifiedOnDesc();
		} catch (POException e) {
			LOGGER.error(e);
		}
		return jobList;
	}

	@Override
	public long getCount() {
		return jobRepository.count();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.JobService#getAllActiveJob()
	 */
	@Override
	public List<Job> getAllActiveJob() {
		try {
			return jobRepository.findAllByIsActiveOrderByLastModifiedOnDesc(true);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.com.parkson.ntpoms.common.services.JobService#getJobByCode(java.lang.
	 * String)
	 */
	@Override
	public Job getJobByCode(String code) {
		try {
			return jobRepository.findByCode(code);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.com.parkson.ntpoms.common.services.JobService#createJob(my.com.parkson
	 * .ntpoms.common.entities.Job)
	 */
	@Override
	public void createJob(Job job) {
		try {
			jobRepository.save(job);
		} catch (NonUniqueResultException e) {
			LOGGER.error(e);
		} catch (PersistenceException e) {
			LOGGER.error(e);
		} //
		catch (POException e) {
			LOGGER.error(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.com.parkson.ntpoms.common.services.JobService#getAllPagableJob(org.
	 * springframework.data.domain.Pageable)
	 */
	@Override
	public Page<Job> getAllPagableJob(Pageable pageRequest) {
		return jobRepository.findAll(pageRequest);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.com.parkson.ntpoms.common.services.JobService#getAllPagableJob(org.
	 * springframework.data.jpa.domain.Specification,
	 * org.springframework.data.domain.Pageable)
	 */
	@Override
	public Page<Job> getAllPagableJob(Specification<Job> s, Pageable pageRequest) {
		return jobRepository.findAll(s, pageRequest);
	}

	@Override
	public Page<Job> getAllPagableJobOrdered(Pageable pageRequest) {
		return jobRepository.findAllByOrderByLastModifiedOnDesc(pageRequest);
	}

	@Override
	public Page<Job> getAllPagableJobOrdered(Specification<Job> s, Pageable pageRequest) {
		return jobRepository.findAllByOrderByLastModifiedOnDesc(s, pageRequest);
	}
}