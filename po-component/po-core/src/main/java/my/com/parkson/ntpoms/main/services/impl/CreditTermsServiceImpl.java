package my.com.parkson.ntpoms.main.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.main.entities.CreditTerms;
import my.com.parkson.ntpoms.main.entities.CreditTermsId;
import my.com.parkson.ntpoms.main.repositories.CreditTermsRepository;
import my.com.parkson.ntpoms.main.services.CreditTermsService;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class CreditTermsServiceImpl implements CreditTermsService {

	private static final Logger LOGGER = Logger.getLogger(CreditTermsServiceImpl.class);

	@Autowired
	private CreditTermsRepository creditTermsRepository;

	@Override
	public void createCreditTerms(CreditTerms creditTerms) {
		try {
			creditTermsRepository.save(creditTerms);
		} catch (Exception e) {
			LOGGER.error(e);
		}
	}

	@Override
	public CreditTerms getCreditTermsByCode(String code) {
		try {
			return creditTermsRepository.findByCrtIdCode(code);
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return null;
	}

	@Override
	public List<CreditTerms> getAllCreditTerms() {
		try {
			/*
			 * return
			 * creditTermsRepository.findAllByOrderByLastmodifiedonDesc();
			 */
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return null;
	}

	@Override
	public List<CreditTerms> getAllActiveCreditTerms() {
		List<CreditTerms> crtList = new ArrayList<>();
		try {
			crtList = creditTermsRepository.findAllByIsActiveOrderByLastModifiedOnDesc(true);
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return crtList;
	}

	@Override
	public Page<CreditTerms> getPagebleAll(Pageable req) {
		return creditTermsRepository.findAll(req);
	}

	@Override
	public Page<CreditTerms> getAllPagableCrt(Specification<CreditTerms> s, Pageable pageRequest) {
		return creditTermsRepository.findAll(s, pageRequest);
	}

	@Override
	public Page<CreditTerms> getAllPageableCrtOrdered(Pageable pageRequest) {
		return creditTermsRepository.findAllByOrderByLastModifiedOnDesc(pageRequest);
	}

	@Override
	public long getCount() {
		return creditTermsRepository.count();
	}

	@Override
	public long getCountActive() {
		List<CreditTerms> crtList = new ArrayList<>();
		try {
			crtList = creditTermsRepository.findAllByIsActiveOrderByLastModifiedOnDesc(true);
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return crtList.size();
	}

	@Override
	public CreditTerms getCreditTermsbyCrtId(CreditTermsId pkey) {
		CreditTerms CreditTerms = null;
		try {
			CreditTerms = creditTermsRepository.findByCrtId(pkey);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e);
		}
		return CreditTerms;
	}

}