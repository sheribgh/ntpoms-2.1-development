/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.entities.OrderCategory;
import my.com.parkson.ntpoms.common.repositories.OrderCategoryRepository;
import my.com.parkson.ntpoms.common.services.MessageService;
import my.com.parkson.ntpoms.common.services.OrderCategoryService;

/**
 * The Class OrderCategoryServiceImpl.
 */
@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class OrderCategoryServiceImpl implements OrderCategoryService {

	private static final Logger LOGGER = Logger.getLogger(OrderCategoryServiceImpl.class);

	/** The order category repository. */
	@Autowired
	private OrderCategoryRepository orderCategoryRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.OrderCategoryService#
	 * getAllOrderCategory()
	 */
	@Override
	public List<OrderCategory> getAllOrderCategory() {
		List<OrderCategory> orderCategoryList = new ArrayList<>();
		try {
			orderCategoryList = orderCategoryRepository.findAllByOrderByLastModifiedOnDesc();
		} catch (POException e) {
			LOGGER.error(e);
		}
		return orderCategoryList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.OrderCategoryService#
	 * getAllActiveOrderCategory()
	 */
	@Override
	public List<OrderCategory> getAllActiveOrderCategory() {
		List<OrderCategory> orderCategoryList = new ArrayList<>();
		try {
			orderCategoryList = orderCategoryRepository.findAllByIsActiveOrderByLastModifiedOnDesc(true);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return orderCategoryList;
	}

	@Override
	public long getCount() {
		return orderCategoryRepository.count();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.OrderCategoryService#
	 * getOrderCategoryByCode(java.lang.String)
	 */
	@Override
	public OrderCategory getOrderCategoryByCode(String code) {
		try {
			return orderCategoryRepository.findByCode(code);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.OrderCategoryService#
	 * createOrderCategory(my.com.parkson.ntpoms.common.entities.OrderCategory)
	 */
	@Override
	public void createOrderCategory(OrderCategory orderCategory) {
		try {
			orderCategoryRepository.save(orderCategory);
		} catch (NonUniqueResultException e) {
			LOGGER.error(e);
		} catch (PersistenceException e) {
			LOGGER.error(e);
		} catch (POException e) {
			LOGGER.error(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.OrderCategoryService#
	 * getAllPageableOrderCategory(org.springframework.data.domain.Pageable)
	 */
	@Override
	public Page<OrderCategory> getAllPageableOrderCategory(Pageable pageRequest) {
		return orderCategoryRepository.findAll(pageRequest);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.OrderCategoryService#
	 * getAllPageableOrderCategory(org.springframework.data.jpa.domain.
	 * Specification, org.springframework.data.domain.Pageable)
	 */
	@Override
	public Page<OrderCategory> getAllPageableOrderCategory(Specification<OrderCategory> s, Pageable pageRequest) {
		return orderCategoryRepository.findAll(s, pageRequest);
	}

	@Override
	public Page<OrderCategory> getAllPageableOrderCategoryOrdered(Pageable pageRequest) {
		return orderCategoryRepository.findAllByOrderByLastModifiedOnDesc(pageRequest);
	}

	@Override
	public Page<OrderCategory> getAllPageableOrderCategoryOrdered(Specification<OrderCategory> s,
			Pageable pageRequest) {
		return orderCategoryRepository.findAllByOrderByLastModifiedOnDesc(s, pageRequest);
	}
}