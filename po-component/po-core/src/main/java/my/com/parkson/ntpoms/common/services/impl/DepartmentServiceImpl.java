/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */

package my.com.parkson.ntpoms.common.services.impl;

import java.util.List;

import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.entities.Department;
import my.com.parkson.ntpoms.common.repositories.DepartmentRepository;
import my.com.parkson.ntpoms.common.services.DepartmentService;
import my.com.parkson.ntpoms.common.services.MessageService;

/**
 * The Class DepartmentServiceImpl.
 */
@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class DepartmentServiceImpl implements DepartmentService {

	private static final Logger LOGGER = Logger.getLogger(DepartmentServiceImpl.class);

	/** The messages. */
	@Autowired
	private MessageService messages;

	/** The department repository. */
	@Autowired
	private DepartmentRepository departmentRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.com.parkson.ntpoms.common.services.DepartmentService#getAllDepartment(
	 * )
	 */
	@Override
	public List<Department> getAllDepartment() {
		try {
			return departmentRepository.findAllByOrderByLastModifiedOnDesc();
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	@Override
	public long getCount() {
		return departmentRepository.count();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.DepartmentService#
	 * getAllActiveDepartment()
	 */
	@Override
	public List<Department> getAllActiveDepartment() {
		try {
			return departmentRepository.findAllByIsActiveOrderByLastModifiedOnDesc(true);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.DepartmentService#
	 * getDepartmentByCode(java.lang.String)
	 */
	@Override
	public Department getDepartmentByCode(String code) {
		try {
			return departmentRepository.findByCode(code);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.com.parkson.ntpoms.common.services.DepartmentService#createDepartment(
	 * my.com.parkson.ntpoms.common.entities.Department)
	 */
	@Override
	public void createDepartment(Department department) {
		try {
			departmentRepository.save(department);
		} catch (NonUniqueResultException e) {
			LOGGER.error(e);
		} catch (PersistenceException e) {
			LOGGER.error(e);
		} catch (POException e) {
			LOGGER.error(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.DepartmentService#
	 * getAllPagableDepartment(org.springframework.data.domain.Pageable)
	 */
	@Override
	public Page<Department> getAllPagableDepartment(Pageable pageRequest) {
		return departmentRepository.findAll(pageRequest);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.DepartmentService#
	 * getAllPagableDepartment(org.springframework.data.jpa.domain.
	 * Specification, org.springframework.data.domain.Pageable)
	 */
	@Override
	public Page<Department> getAllPagableDepartment(Specification<Department> s, Pageable pageRequest) {
		return departmentRepository.findAll(s, pageRequest);
	}

	@Override
	public Page<Department> getAllPageableDepartmentOrdered(Pageable pageRequest) {
		return departmentRepository.findAllByOrderByLastModifiedOnDesc(pageRequest);
	}

	@Override
	public Page<Department> getAllPageableDepartmentOrdered(Specification<Department> s, Pageable pageRequest) {
		return departmentRepository.findAllByOrderByLastModifiedOnDesc(s, pageRequest);
	}
}