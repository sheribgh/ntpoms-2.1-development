/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.entities.DataSource;
import my.com.parkson.ntpoms.common.repositories.DataSourceRepository;
import my.com.parkson.ntpoms.common.services.DataSourceService;
import my.com.parkson.ntpoms.common.services.MessageService;

/**
 * The Class DataSourceServiceImpl.
 */
@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class DataSourceServiceImpl implements DataSourceService {

	private static final Logger LOGGER = Logger.getLogger(DataSourceServiceImpl.class);

	/** The messages. */
	@Autowired
	private MessageService messages;

	/** The data source repository. */
	@Autowired
	private DataSourceRepository dataSourceRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.com.parkson.ntpoms.common.services.DataSourceService#createDataSource(
	 * my.com.parkson.ntpoms.common.entities.DataSource)
	 */
	@Override
	public boolean createDataSource(DataSource dataSource) {
		try {
			dataSourceRepository.save(dataSource);
			return true;
		} catch (POException e) {
			LOGGER.error(e);
			return false;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.DataSourceService#
	 * getDataSourceByCompany(java.lang.String)
	 */
	@Override
	public DataSource getDataSourceByCompany(String company) {
		try {
			return dataSourceRepository.findByCompany(company);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.DataSourceService#
	 * getDataSourceByCompanyAndYear(java.lang.String, java.lang.String)
	 */
	@Override
	public DataSource getDataSourceByCompanyAndYear(String company, String year) {
		try {
			return dataSourceRepository.findByCompanyAndYear(company, year);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.DataSourceService#
	 * getActiveDataSource(java.lang.String)
	 */
	@Override
	public List<DataSource> getActiveDataSource(String currentYear) {
		List<DataSource> activeDataSourceList = new ArrayList<>();
		try {
			List<DataSource> dataSourceList = dataSourceRepository.findAllByIsActiveAndYear(true, currentYear);
			List<DataSource> newDataSourceList = dataSourceRepository.findAllAfterYear(currentYear);
			for (DataSource dataSource : dataSourceList) {
				if (!newDataSourceList.contains(dataSource)) {
					activeDataSourceList.add((DataSource) dataSource);
				}
			}
		} catch (POException e) {
			LOGGER.error(e);
		}
		return activeDataSourceList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.com.parkson.ntpoms.common.services.DataSourceService#getAllDataSource(
	 * )
	 */
	@Override
	public List<DataSource> getAllDataSource() {
		try {
			dataSourceRepository.findAll();
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.DataSourceService#
	 * getAllPageableDataSource(org.springframework.data.domain.Pageable)
	 */
	@Override
	public Page<DataSource> getAllPageableDataSource(Pageable pageRequest) {
		return dataSourceRepository.findAll(pageRequest);
	}
}