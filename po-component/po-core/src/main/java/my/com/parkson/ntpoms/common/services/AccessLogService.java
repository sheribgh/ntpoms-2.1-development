/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import my.com.parkson.ntpoms.common.entities.AccessLog;

/**
 * The Interface AccessLogService.
 */
public interface AccessLogService {

	/**
	 * Gets the all access log.
	 *
	 * @return the all access log
	 */
	public List<AccessLog> getAllAccessLog();
	
	
	public long getCount();

	/**
	 * Gets the all pageable access log.
	 *
	 * @param pageRequest the page request
	 * @return the all pageable access log
	 */
	public Page<AccessLog> getAllPageableAccessLog(Pageable pageRequest);

	/**
	 * Gets the all pageable access log.
	 *
	 * @param s the s
	 * @param pageRequest the page request
	 * @return the all pageable access log
	 */
	public Page<AccessLog> getAllPageableAccessLog(Specification<AccessLog> s, Pageable pageRequest);

	/**
	 * Creates the access log.
	 *
	 * @param accessLog the access log
	 */
	public void createAccessLog(AccessLog accessLog);

	/**
	 * Log.
	 *
	 * @param logLevel the log level
	 * @param logMessage the log message
	 * @param exception the exception
	 */
	public void log(String logLevel, String logMessage, Exception exception);
	
	
	public void log(String logLevel, String logMessage, Exception exception, int lineNo, String className);

	/**
	 * Log.
	 *
	 * @param database the database
	 * @param logLevel the log level
	 * @param logMessage the log message
	 * @param exception the exception
	 */
	public void log(String database, String logLevel, String logMessage, Exception exception);

	Page<AccessLog> getAllPageableLogOrdered(Pageable pageRequest);
	
}