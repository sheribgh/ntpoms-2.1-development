/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
/* Name			:	MF02_M_STRMAST_ServiceImpl											*/
/* Usage		:	Creating service layer for Store									*/
/* Description	:	This code creates a connection between Store Screen					*/
/* 					and Store table in database. This code interacts with				*/
/* 					the table MF02_M_STRMAST											*/
/****************************************************************************************/

package my.com.parkson.ntpoms.common.services.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.entities.Store;
import my.com.parkson.ntpoms.common.repositories.StoreRepository;
import my.com.parkson.ntpoms.common.services.MessageService;
import my.com.parkson.ntpoms.common.services.StoreService;

/**
 * The Class StoreServiceImpl.
 */
@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class StoreServiceImpl implements StoreService {

	private static final Logger LOGGER = Logger.getLogger(StoreServiceImpl.class);

	/** The messages. */
	@Autowired
	private MessageService messages;

	/** The store repository. */
	@Autowired
	private StoreRepository storeRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.StoreService#getAllStore()
	 */
	@Override
	public List<Store> getAllStore() {
		List<Store> storeList = new ArrayList<>();
		try {
			storeList = storeRepository.findAllByOrderByLastModifiedOnDesc();
		} catch (POException e) {
			LOGGER.error(e);
		}
		return storeList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.com.parkson.ntpoms.common.services.StoreService#getAllActiveStore()
	 */
	@Override
	public List<Store> getAllActiveStore() {
		List<Store> storeList = new ArrayList<>();
		try {
			storeList = storeRepository.findByIsActiveOrderByLastModifiedOnDesc(true);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return storeList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.com.parkson.ntpoms.common.services.StoreService#getStoreByCode(java.
	 * lang.String)
	 */
	@Override
	public Store getStoreByCode(String code) {
		try {
			return storeRepository.findByCode(code);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.com.parkson.ntpoms.common.services.StoreService#createStore(my.com.
	 * parkson.ntpoms.common.entities.Store)
	 */
	@Override
	public void createStore(Store store) {
		try {
			storeRepository.save(store);
		} catch (NonUniqueResultException e) {
			LOGGER.error(e);
		} catch (PersistenceException e) {
			LOGGER.error(e);
		} catch (POException e) {
			LOGGER.error(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.StoreService#
	 * getAllActiveStoreByCompanyCode(java.lang.String)
	 */
	@Override
	public List<Store> getAllActiveStoreByCompanyCode(String companyCode) {
		List<Store> storeList = new ArrayList<>();
		try {
			storeList = storeRepository.findAllActiveByCompanyCode(companyCode);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return storeList;
	}

	@Override
	public long getCount() {
		return storeRepository.count();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.com.parkson.ntpoms.common.services.StoreService#getAllPageableStore(
	 * org.springframework.data.domain.Pageable)
	 */
	@Override
	public Page<Store> getAllPageableStore(Pageable pageRequest) {
		return storeRepository.findAll(pageRequest);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.com.parkson.ntpoms.common.services.StoreService#getAllPageableStore(
	 * org.springframework.data.jpa.domain.Specification,
	 * org.springframework.data.domain.Pageable)
	 */
	@Override
	public Page<Store> getAllPageableStore(Specification<Store> s, Pageable pageRequest) {
		return storeRepository.findAll(s, pageRequest);
	}

	@Override
	public Page<Store> getAllPageableStoreOrdered(Pageable pageRequest) {
		return storeRepository.findAllByOrderByLastModifiedOnDesc(pageRequest);
	}

	@Override
	public Page<Store> getAllPageableStoreOrdered(Specification<Store> s, Pageable pageRequest) {
		return storeRepository.findAllByOrderByLastModifiedOnDesc(s, pageRequest);
	}
}