/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import my.com.parkson.ntpoms.common.entities.PurchaseOrderUser;

/**
 * The Interface PurchaseOrderUserService.
 */
public interface PurchaseOrderUserService {
	
	/**
	 * Creates the purchase order user.
	 *
	 * @param PO06_T_USER the po06 t user
	 */
	public void createPurchaseOrderUser(PurchaseOrderUser PO06_T_USER);	
	
	/**
	 * Gets the purchase order user by id.
	 *
	 * @param id the id
	 * @return the purchase order user by id
	 */
	public PurchaseOrderUser getPurchaseOrderUserById(String id);	
	
	
	
	
	public long getCount();
	
	
	public long getCountActive();
	
	/**
	 * Gets the all purchase order user.
	 *
	 * @return the all purchase order user
	 */
	public List<PurchaseOrderUser> getAllPurchaseOrderUser();
	
	/**
	 * Gets the all pageable purchase order user.
	 *
	 * @param pageRequest the page request
	 * @return the all pageable purchase order user
	 */
	public Page<PurchaseOrderUser> getAllPageablePurchaseOrderUser(Pageable pageRequest);
	
	/**
	 * Gets the all pageable purchase order user.
	 *
	 * @param s the s
	 * @param pageRequest the page request
	 * @return the all pageable purchase order user
	 */
	public Page<PurchaseOrderUser> getAllPageablePurchaseOrderUser(Specification<PurchaseOrderUser> s,Pageable pageRequest);
	
	
	/**
	 * Gets the all pageable purchase order user.
	 *
	 * @param pageRequest the page request
	 * @return the all pageable purchase order user
	 */
	public Page<PurchaseOrderUser> getAllPageablePurchaseOrderUserOrdered(Pageable pageRequest);
	
	/**
	 * Gets the all pageable purchase order user.
	 *
	 * @param s the s
	 * @param pageRequest the page request
	 * @return the all pageable purchase order user
	 */
	public Page<PurchaseOrderUser> getAllPageablePurchaseOrderUserOrdered(Specification<PurchaseOrderUser> s,Pageable pageRequest);
	
	
	/**
	 * Gets the all active purchase order user.
	 *
	 * @return the all active purchase order user
	 */
	public List<PurchaseOrderUser> getAllActivePurchaseOrderUser();	
	
	/**
	 * Gets the all purchase order user ids.
	 *
	 * @return the all purchase order user ids
	 */
	public List<String> getAllPurchaseOrderUserIds();
}