/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */

package my.com.parkson.ntpoms.common.services.impl;

import static my.com.parkson.ntpoms.common.MessageCodes.FAILED_PERSIST_EXCEPTION;
import static my.com.parkson.ntpoms.common.MessageCodes.FAILED_FETCH_RECORDS_EXCEPTION;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.LogMessages;
import my.com.parkson.ntpoms.common.entities.Audit;
import my.com.parkson.ntpoms.common.repositories.AuditRepository;
import my.com.parkson.ntpoms.common.services.AccessLogService;
import my.com.parkson.ntpoms.common.services.AuditService;
import my.com.parkson.ntpoms.common.services.MessageService;
import my.com.parkson.ntpoms.main.services.impl.PurchaseOrderServiceImpl;

/**
 * The Class AuditServiceImpl.
 */
@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class AuditServiceImpl implements AuditService {

	private static final Logger LOGGER = Logger.getLogger(AuditServiceImpl.class);

	/** The messages. */
	@Autowired
	private MessageService messages;

	/** The audit repository. */
	@Autowired
	private AuditRepository auditRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.com.parkson.ntpoms.common.services.AuditService#putAudit(java.util.
	 * Date, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public void putAudit(Date date, String message, String username, String operation) {
		Audit audit = new Audit();
		audit.setCreatedOn(date);
		audit.setMessage(message);
		audit.setOperation(operation);
		audit.setUsername(username);
		try {
			auditRepository.save(audit);
		} catch (POException e) {
			LOGGER.error(e);
		}
	}

	@Override
	public long getCount() {
		return auditRepository.count();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.AuditService#getAllAudit()
	 */
	@Override
	public List<Audit> getAllAudit() {
		try {
			return auditRepository.findAllByOrderByCreatedOnDesc();
		} catch (POException e) {
			LOGGER.error(e);
		}

		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.com.parkson.ntpoms.common.services.AuditService#getAllPagableAudit(org
	 * .springframework.data.domain.Pageable)
	 */
	@Override
	public Page<Audit> getAllPagableAudit(Pageable pageRequest) {
		return auditRepository.findAll(pageRequest);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.com.parkson.ntpoms.common.services.AuditService#getRecordsBySearchCode
	 * (java.lang.String, org.springframework.data.domain.Pageable)
	 */
	@Override
	public Page<Audit> getRecordsBySearchCode(@Param("searchval") String searchval, Pageable pageRequest) {
		try {
			return auditRepository.findBySearchCode(searchval, pageRequest);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.com.parkson.ntpoms.common.services.AuditService#getAllPagableAudit(org
	 * .springframework.data.jpa.domain.Specification,
	 * org.springframework.data.domain.Pageable)
	 */
	@Override
	public Page<Audit> getAllPagableAudit(Specification<Audit> s, Pageable pageRequest) {
		return auditRepository.findAll(s, pageRequest);
	}
}