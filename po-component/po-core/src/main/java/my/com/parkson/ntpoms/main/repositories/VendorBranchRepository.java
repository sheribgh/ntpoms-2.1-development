package my.com.parkson.ntpoms.main.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import my.com.parkson.ntpoms.main.entities.Project;
import my.com.parkson.ntpoms.main.entities.VendorBranch;
import my.com.parkson.ntpoms.main.entities.VendorBranchId;


public interface VendorBranchRepository extends JpaRepository<VendorBranch, Integer> , JpaSpecificationExecutor<VendorBranch>  {

	public List<VendorBranch> findAllByOrderByLastModifiedOnDesc();
	public Page<VendorBranch> findAllByOrderByLastModifiedOnDesc(Pageable pageRequest);
	public Page<VendorBranch> findAllByOrderByLastModifiedOnDesc(Specification<VendorBranch> s, Pageable pageRequest);
	public List<VendorBranch> findAllByIsActiveOrderByLastModifiedOnDesc(boolean isActive);
	public VendorBranch findByVendorBranchId(VendorBranchId vendorBranchId);
	
	public List<VendorBranch> findAllByIsActiveAndVendorBranchIdCompanyCode(boolean isActive,String companyCode);
	
	
	@Query("select v from VendorBranch v where v.vendorBranchId.code=:vedorcode")
	public VendorBranch getVendor(@Param("vedorcode") String vedorcode);
	
}