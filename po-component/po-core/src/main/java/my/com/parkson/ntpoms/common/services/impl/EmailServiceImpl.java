/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services.impl;

import static my.com.parkson.ntpoms.common.MessageCodes.FAILED_SENDMAIL_EXCEPTION;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.activation.DataHandler;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.tools.generic.DateTool;
import org.apache.velocity.tools.generic.NumberTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.model.EmailCriteria;
import my.com.parkson.ntpoms.common.services.EmailService;
import my.com.parkson.ntpoms.common.services.MessageService;

/**
 * The Class EmailServiceImpl.
 */
@Slf4j
@Service
@Transactional
public class EmailServiceImpl implements EmailService {

	private static final Logger LOGGER = Logger.getLogger(EmailServiceImpl.class);
	@Autowired
	private MessageService messages;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.com.parkson.ntpoms.common.services.EmailService#sendEmail(org.apache.
	 * velocity.VelocityContext,
	 * my.com.parkson.ntpoms.common.model.EmailCriteria)
	 */
	@Override
	public void sendEmail(Session session, Map<String, String> contextParam, Map<String, List> maplist,
			EmailCriteria emailCriteria) throws MessagingException, IOException {
		try {
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(emailCriteria.getEmailUser()));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(emailCriteria.getRecipient()));
			message.setSubject(emailCriteria.getSubject());
			int i = 0;
			if (emailCriteria.getCc() != null && !emailCriteria.getCc().isEmpty()) {
				InternetAddress[] address = new InternetAddress[emailCriteria.getCc().size()];
				for (String email : emailCriteria.getCc()) {
					address[i] = new InternetAddress(email);
					i++;
				}
				message.addRecipients(Message.RecipientType.CC, address);
			}
			VelocityEngine velocityEngine = new VelocityEngine();
			velocityEngine.setProperty(RuntimeConstants.RESOURCE_LOADER, "class,file");
			velocityEngine.setProperty(RuntimeConstants.RUNTIME_LOG_LOGSYSTEM_CLASS,
					"org.apache.velocity.runtime.log.Log4JLogChute");
			velocityEngine.setProperty("runtime.log.logsystem.log4j.logger", "VELLOGGER");
			velocityEngine.setProperty("class.resource.loader.class",
					"org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
			velocityEngine.setProperty("runtime.log.logsystem.class", "org.apache.velocity.runtime.log.NullLogSystem");
			velocityEngine.init();
			Template t = velocityEngine.getTemplate(emailCriteria.getTemplateName());
			VelocityContext context = new VelocityContext();
			for (Entry<String, String> entry : contextParam.entrySet()) {
				LOGGER.debug("Item : " + entry.getKey() + " Count : " + entry.getValue());
				context.put(entry.getKey(), entry.getValue());
			}

			context.put("date", new DateTool());
			context.put("numberTool", new NumberTool());

			if (maplist != null) {
				for (Entry<String, List> entry : maplist.entrySet()) {
					context.put(entry.getKey(), entry.getValue());
				}
			}
			StringWriter out = new StringWriter();
			t.merge(context, out);
			BodyPart body = new MimeBodyPart();
			body.setContent(out.toString(), "text/html");
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(body);
			if (emailCriteria.getTemplateName().contains("upload") && emailCriteria.getAttachment() != null) {
				for (String filename : emailCriteria.getAttachment()) {
					MimeBodyPart attachPart = new MimeBodyPart();
					FileInputStream fis = new FileInputStream(filename);
					ByteArrayDataSource ds = new ByteArrayDataSource(fis, "text/plain");
					attachPart.setDataHandler(new DataHandler(ds));
					attachPart.setFileName(new File(filename).getName());
					multipart.addBodyPart(attachPart);
				}
			}
			message.setContent(multipart);
			Transport.send(message);
		} catch (POException ex) {
			LOGGER.error(ex);
			throw new POException(messages.getMessage(FAILED_SENDMAIL_EXCEPTION), ex);
		}
	}
}