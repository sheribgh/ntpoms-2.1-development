package my.com.parkson.ntpoms.main.services;

import java.util.List;

import my.com.parkson.ntpoms.main.entities.ItemInformation;

public interface ItemInformationService {
	
	public List<ItemInformation> getAllItemInformation();	
	public ItemInformation getItemInformationByItemNo(String itemNo);	
	public void createItemInformation(ItemInformation itemInformation);
}