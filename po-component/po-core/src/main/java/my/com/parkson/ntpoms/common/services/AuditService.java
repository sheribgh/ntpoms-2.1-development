/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
/* Name : audit_Service                                      */
/* Usage: Code for the audit service                        */
/* Description: This code creates an interface for audit that will */
/*              used for connecting to data. This code*/
/*              interacts with the table <audit>.        */
/*************************************************************/

package my.com.parkson.ntpoms.common.services;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.query.Param;

import my.com.parkson.ntpoms.common.entities.Audit;

/**
 * The Interface AuditService.
 */
public interface AuditService {

	/**
	 * Put audit.
	 *
	 * @param createdon the createdon
	 * @param message the message
	 * @param username the username
	 * @param operation the operation
	 */
	public void putAudit(Date createdon, String message, String username, String operation);
	
	
	
	public long getCount();

	/**
	 * Gets the all audit.
	 *
	 * @return the all audit
	 */
	public List<Audit> getAllAudit();

	/**
	 * Gets the all pagable audit.
	 *
	 * @param pageRequest the page request
	 * @return the all pagable audit
	 */
	public Page<Audit> getAllPagableAudit(Pageable pageRequest);

	/**
	 * Gets the all pagable audit.
	 *
	 * @param s the s
	 * @param pageRequest the page request
	 * @return the all pagable audit
	 */
	public Page<Audit> getAllPagableAudit(Specification<Audit> s, Pageable pageRequest);

	/**
	 * Gets the records by search code.
	 *
	 * @param searchval the searchval
	 * @param pageRequest the page request
	 * @return the records by search code
	 */
	public Page<Audit> getRecordsBySearchCode(@Param("searchval") String searchval, Pageable pageRequest);
}