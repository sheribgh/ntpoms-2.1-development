/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.main.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import my.com.parkson.ntpoms.main.entities.AccessLogsSetting;

/**
 * The Interface AccessLogSettingRepository.
 */
public interface AccessLogsSettingRepository
		extends JpaRepository<AccessLogsSetting, Integer>, JpaSpecificationExecutor<AccessLogsSetting> {

	/**
	 * Find all by order by id asc.
	 *
	 * @return the list
	 */
	public List<AccessLogsSetting> findAllByOrderByIdAsc();
}