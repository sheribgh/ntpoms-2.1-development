/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The Class MasterData.
 */
@Entity
@Table(name = "\"MF06_R_MASTDATA\"")
@Data
@NoArgsConstructor
public class MasterData implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mf06_r_mastdata_sequence")
	@SequenceGenerator(name = "mf06_r_mastdata_sequence", sequenceName = "mf06_r_mastdata_sequence", 
			allocationSize = 1)
	@Column(name = "\"mf06_mastDataId\"")
	private int id;
	
	/** The code type. */
	@Column(name = "\"mf06_codeType\"")
	private String codeType;
	
	/** The code value. */
	@Column(name = "\"mf06_codeValue\"")
	private String codeValue;
	
	/** The code desc. */
	@Column(name = "\"mf06_codeDesc\"")
	private String codeDesc;
	
	/** The sys name. */
	@Column(name = "\"mf06_sysName\"")
	private String sysName ;
	
	/** The created by. */
	@Column(name = "\"mf06_createdBy\"")
	private String createdBy;
	
	/** The created on. */
	@Column(name = "\"mf06_createdOn\"")
	private Date   createdOn;
	
	/** The last modified by. */
	@Column(name = "\"mf06_lastModifiedBy\"")
	private String lastModifiedBy;
	
	/** The last modified on. */
	@Column(name = "\"mf06_lastModifiedOn\"")
	private Date lastModifiedOn;
	
	/** The deactivated by. */
	@Column(name = "\"mf06_deactivatedBy\"")
	private String deactivatedBy;
	
	/** The deactivated on. */
	@Column(name = "\"mf06_deactivatedOn\"")
	private Date deactivatedOn;
	
	/** The reactivated by. */
	@Column(name = "\"mf06_reactivatedBy\"")
	private String reactivatedBy;
	
	/** The reactivated on. */
	@Column(name = "\"mf06_reactivatedOn\"")
	private Date reactivatedOn;
	
	/** The is active. */
	@Column(name = "\"mf06_isActive\"")
	private boolean isActive;

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Gets the code type.
	 *
	 * @return the code type
	 */
	public String getCodeType() {
		return codeType;
	}

	/**
	 * Sets the code type.
	 *
	 * @param codeType the new code type
	 */
	public void setCodeType(String codeType) {
		this.codeType = codeType;
	}

	/**
	 * Gets the code value.
	 *
	 * @return the code value
	 */
	public String getCodeValue() {
		return codeValue;
	}

	/**
	 * Sets the code value.
	 *
	 * @param codeValue the new code value
	 */
	public void setCodeValue(String codeValue) {
		this.codeValue = codeValue;
	}

	/**
	 * Gets the code desc.
	 *
	 * @return the code desc
	 */
	public String getCodeDesc() {
		return codeDesc;
	}

	/**
	 * Sets the code desc.
	 *
	 * @param codeDesc the new code desc
	 */
	public void setCodeDesc(String codeDesc) {
		this.codeDesc = codeDesc;
	}

	/**
	 * Gets the sys name.
	 *
	 * @return the sys name
	 */
	public String getSysName() {
		return sysName;
	}

	/**
	 * Sets the sys name.
	 *
	 * @param sysName the new sys name
	 */
	public void setSysName(String sysName) {
		this.sysName = sysName;
	}

	/**
	 * Gets the created by.
	 *
	 * @return the created by
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * Sets the created by.
	 *
	 * @param createdBy the new created by
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Gets the created on.
	 *
	 * @return the created on
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * Sets the created on.
	 *
	 * @param createdOn the new created on
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * Gets the last modified by.
	 *
	 * @return the last modified by
	 */
	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	/**
	 * Sets the last modified by.
	 *
	 * @param lastModifiedBy the new last modified by
	 */
	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	/**
	 * Gets the last modified on.
	 *
	 * @return the last modified on
	 */
	public Date getLastModifiedOn() {
		return lastModifiedOn;
	}

	/**
	 * Sets the last modified on.
	 *
	 * @param lastModifiedOn the new last modified on
	 */
	public void setLastModifiedOn(Date lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

	/**
	 * Gets the deactivated by.
	 *
	 * @return the deactivated by
	 */
	public String getDeactivatedBy() {
		return deactivatedBy;
	}

	/**
	 * Sets the deactivated by.
	 *
	 * @param deactivatedBy the new deactivated by
	 */
	public void setDeactivatedBy(String deactivatedBy) {
		this.deactivatedBy = deactivatedBy;
	}

	/**
	 * Gets the deactivated on.
	 *
	 * @return the deactivated on
	 */
	public Date getDeactivatedOn() {
		return deactivatedOn;
	}

	/**
	 * Sets the deactivated on.
	 *
	 * @param deactivatedOn the new deactivated on
	 */
	public void setDeactivatedOn(Date deactivatedOn) {
		this.deactivatedOn = deactivatedOn;
	}

	/**
	 * Gets the reactivated by.
	 *
	 * @return the reactivated by
	 */
	public String getReactivatedBy() {
		return reactivatedBy;
	}

	/**
	 * Sets the reactivated by.
	 *
	 * @param reactivatedBy the new reactivated by
	 */
	public void setReactivatedBy(String reactivatedBy) {
		this.reactivatedBy = reactivatedBy;
	}

	/**
	 * Gets the reactivated on.
	 *
	 * @return the reactivated on
	 */
	public Date getReactivatedOn() {
		return reactivatedOn;
	}

	/**
	 * Sets the reactivated on.
	 *
	 * @param reactivatedOn the new reactivated on
	 */
	public void setReactivatedOn(Date reactivatedOn) {
		this.reactivatedOn = reactivatedOn;
	}

	/**
	 * Checks if is active.
	 *
	 * @return true, if is active
	 */
	public boolean isActive() {
		return isActive;
	}

	/**
	 * Sets the active.
	 *
	 * @param isActive the new active
	 */
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
}