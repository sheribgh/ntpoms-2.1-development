/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.main.services;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import my.com.parkson.ntpoms.main.entities.PurchaseOrder;

/**
 * The Interface PurchaseOrderService.
 */
public interface PurchaseOrderService {

	/**
	 * Creates the purchase order.
	 *
	 * @param purchaseOrder
	 *            the purchase order
	 */
	public void createPurchaseOrder(PurchaseOrder purchaseOrder);

	/**
	 * Gets the all purchase order.
	 *
	 * @return the all purchase order
	 */
	public List<PurchaseOrder> getAllPurchaseOrder();

	public Page<PurchaseOrder> getAllPageablePurchaseOrder(Pageable pageRequest);

	/**
	 * Gets the all pageable purchase order.
	 *
	 * @param pageRequest
	 *            the page request
	 * @return the all pageable purchase order
	 */
	public Page<PurchaseOrder> getAllPageablePurchaseOrder(Specification<PurchaseOrder> s, Pageable pageRequest);

	public PurchaseOrder getAllPurchaseOrderByPoNo(String poNo);

	/**
	 * Gets the purchase order by po no.
	 *
	 * @param poNo
	 *            the po no
	 * @return the purchase order by po no
	 */
	public PurchaseOrder getPurchaseOrderByPoNo(String poNo);

	/**
	 * Gets the purchase order by status.
	 *
	 * @param status
	 *            the status
	 * @return the purchase order by status
	 */
	public List<PurchaseOrder> getPurchaseOrderByStatus(String status);

	/**
	 * Gets the purchase order by pc code.
	 *
	 * @param pcCode
	 *            the pc code
	 * @return the purchase order by pc code
	 */
	public List<PurchaseOrder> getPurchaseOrderByPcCode(String pcCode);

	/**
	 * Gets the last purchase order by pc code.
	 *
	 * @param pcCode
	 *            the pc code
	 * @return the last purchase order by pc code
	 */
	public PurchaseOrder getLastPurchaseOrderByPcCode(String pcCode);

	/**
	 * Gets the all non draft purchase order.
	 *
	 * @return the all non draft purchase order
	 */
	public List<PurchaseOrder> getAllNonDraftPurchaseOrder();

	public List<PurchaseOrder> getPurchaseOrderByCompanyCode(String company);

	public Page<PurchaseOrder> getAllPageablePurchaseOrderwithpono(Specification<PurchaseOrder> s,
			Pageable pageRequest);

	public Page<PurchaseOrder> getAllPageablePurchaseOrderwithpono(Pageable pageRequest);

	public List<PurchaseOrder> getPurchaseOrderExpiryDetails(Date date, String status);
}