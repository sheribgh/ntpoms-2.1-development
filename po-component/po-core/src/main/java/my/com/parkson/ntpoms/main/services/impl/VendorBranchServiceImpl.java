package my.com.parkson.ntpoms.main.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import my.com.parkson.ntpoms.main.entities.VendorBranch;
import my.com.parkson.ntpoms.main.entities.VendorBranchId;
import my.com.parkson.ntpoms.main.repositories.VendorBranchRepository;
import my.com.parkson.ntpoms.main.services.VendorBranchService;

@Service
@Transactional
public class VendorBranchServiceImpl implements VendorBranchService {

	private static final Logger LOGGER = Logger.getLogger(VendorBranchServiceImpl.class);

	@Autowired
	private VendorBranchRepository vendorBranchRepository;

	@Override
	public List<VendorBranch> getAllVendorBranch() {
		List<VendorBranch> vendorBranchList = new ArrayList<>();
		try {
			vendorBranchList = vendorBranchRepository.findAllByOrderByLastModifiedOnDesc();
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return vendorBranchList;
	}

	@Override
	public List<VendorBranch> getAllActiveVendorBranch() {
		List<VendorBranch> vendorBranchList = new ArrayList<>();
		try {
			vendorBranchList = vendorBranchRepository.findAllByIsActiveOrderByLastModifiedOnDesc(true);
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return vendorBranchList;
	}

	/*
	 * @Override public VendorBranch getVendorBranchByCodeAndBranchCode(String
	 * code, String branchCode) { try { return
	 * vendorBranchRepository.findByCodeAndBranchCode(code, branchCode); } catch
	 * (Exception e) { log.log(DEBUG_ERROR, DEBUG_DES, e); } return null; }
	 * 
	 * @Override public VendorBranch getVendorBranchByCode(String code) { try {
	 * return vendorBranchRepository.findByCode(code); } catch (Exception e) {
	 * log.log(DEBUG_ERROR, DEBUG_DES, e); } return null; }
	 * 
	 * @Override public VendorBranch getVendorBranchByBranchCode(String
	 * branchCode) { try { return
	 * vendorBranchRepository.findByBranchCode(branchCode); } catch (Exception
	 * e) { log.log(DEBUG_ERROR, DEBUG_DES, e); } return null; }
	 */

	@Override
	public void createVendorBranch(VendorBranch vendorBranch) {
		try {
			vendorBranchRepository.save(vendorBranch);
		} catch (Exception e) {
			LOGGER.error(e);
		}
	}

	@Override
	public Page<VendorBranch> getPagebleAll(Pageable req) {
		try {
			return vendorBranchRepository.findAll(req);
		} catch (Exception e) {
			LOGGER.error(e);
		}

		return null;
	}

	@Override
	public Page<VendorBranch> getAllPageableVendorBranch(Pageable pageRequest) {
		try {
			return vendorBranchRepository.findAll(pageRequest);
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return null;
	}

	@Override
	public Page<VendorBranch> getAllPageableVendorBranch(Specification<VendorBranch> s, Pageable pageRequest) {
		try {
			return vendorBranchRepository.findAll(s, pageRequest);
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return null;
	}

	@Override
	public Page<VendorBranch> getAllPageableVendorBranchOrdered(Pageable pageRequest) {
		try {
			return vendorBranchRepository.findAllByOrderByLastModifiedOnDesc(pageRequest);
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return null;
	}

	@Override
	public Page<VendorBranch> getAllPageableVendorBranchOrdered(Specification<VendorBranch> s, Pageable pageRequest) {
		try {
			return vendorBranchRepository.findAllByOrderByLastModifiedOnDesc(s, pageRequest);
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return null;
	}

	@Override
	public VendorBranch getVendorBranchbyVendorId(VendorBranchId vendorBranchId) {
		VendorBranch vendorBranch = null;
		try {
			vendorBranch = vendorBranchRepository.findByVendorBranchId(vendorBranchId);
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return vendorBranch;
	}

	@Override
	public List<VendorBranch> getAllActiveVendorBranchBycompCode(String companyCode) {
		List<VendorBranch> vendorBranchList = new ArrayList<>();
		try {
			vendorBranchList = vendorBranchRepository.findAllByIsActiveAndVendorBranchIdCompanyCode(true, companyCode);
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return vendorBranchList;
	}

	@Override
	public long getCount() {
		// TODO Auto-generated method stub
		return vendorBranchRepository.count();
	}

	@Override
	public long getCountActive() {
		List<VendorBranch> vendorBranchList = new ArrayList<>();
		vendorBranchList = vendorBranchRepository.findAllByIsActiveOrderByLastModifiedOnDesc(true);
		return vendorBranchList.size();
	}
	@Override
	public VendorBranch getvendor(String code) {
		return vendorBranchRepository.getVendor(code);
	}

}