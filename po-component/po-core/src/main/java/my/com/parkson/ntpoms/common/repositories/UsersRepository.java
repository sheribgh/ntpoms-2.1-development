/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
//import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import my.com.parkson.ntpoms.common.entities.User;

/**
 * The Interface UsersRepository.
 */
public interface UsersRepository extends JpaRepository<User, Integer>, JpaSpecificationExecutor<User> {

	/**
	 * Find by username.
	 *
	 * @param username the username
	 * @return the user
	 */
	public User findByUsername(String username);
	
	/**
	 * Find by username.
	 *
	 * @param username the username
	 * @return the user
	 */	
	User findByUsernameAndIsActiveAndIsLocked(String username, boolean isActive, boolean isLocked);

	User findByUsernameAndIsActive(String username, boolean isActive);	
	
	public User findByEmail(String email);
	
	@Query("select o from User o where o.email =:at")
	public List<User> findUserByEmail(@Param("at") String email);

	/* (non-Javadoc)
	 * @see org.springframework.data.jpa.repository.JpaRepository#findAll()
	 */
	public List<User> findAll();

	/**
	 * Find all except superadmin.
	 *
	 * @return the list
	 */
	@Query("select o from User o where o.username != " + "superadmin" + " order by o.lastModifiedOn desc")
	public List<User> findAllExceptSuperadmin();

	/**
	 * Fina all by is active order by last modified on desc.
	 *
	 * @param isActive the is active
	 * @return the list
	 */
	@Query("select o from User o where o.isActive=:at")
	public List<User> finaAllByIsActiveOrderByLastModifiedOnDesc(@Param("at") boolean isActive);

	/**
	 * Find all by order by last modified on desc.
	 *
	 * @return the list
	 */
	public Page<User> findAllByOrderByLastModifiedOnDesc(Pageable pageRequest);

	/**
	 * Find all by order by last modified on desc.
	 *
	 * @return the list
	 */
	public Page<User> findAllByOrderByLastModifiedOnDesc(Specification<User> s, Pageable pageRequest);
	
}
