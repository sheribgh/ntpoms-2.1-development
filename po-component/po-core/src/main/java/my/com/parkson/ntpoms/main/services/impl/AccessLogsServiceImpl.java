/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.main.services.impl;

import static my.com.parkson.ntpoms.common.MessageCodes.FAILED_FETCH_RECORDS_EXCEPTION;
import static my.com.parkson.ntpoms.common.MessageCodes.FAILED_PERSIST_EXCEPTION;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.LogMessages;
import my.com.parkson.ntpoms.main.entities.AccessLogs;
import my.com.parkson.ntpoms.main.repositories.AccessLogsRepository;
import my.com.parkson.ntpoms.main.services.AccessLogsService;

/**
 * The Class AccessLogServiceImpl.
 */
@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class AccessLogsServiceImpl implements AccessLogsService {

	private static final Logger LOGGER = Logger.getLogger(AccessLogsServiceImpl.class);

	/** The access log repository. */
	@Autowired
	private AccessLogsRepository accessLogRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.com.parkson.ntpoms.common.services.AccessLogService#getAllAccessLog()
	 */
	@Override
	public List<AccessLogs> getAllAccessLog() {
		try {
			return accessLogRepository.findAll();
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	@Override
	public long getCount() {
		return accessLogRepository.count();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.com.parkson.ntpoms.common.services.AccessLogService#createAccessLog(my
	 * .com.parkson.ntpoms.common.entities.AccessLog)
	 */
	@Override
	public void createAccessLog(AccessLogs accessLog) {
		try {
			accessLogRepository.save(accessLog);
		} catch (POException e) {
			LOGGER.error(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.com.parkson.ntpoms.common.services.AccessLogService#log(java.lang.
	 * String, java.lang.String, java.lang.Exception)
	 */
	@Override
	public void log(String logLevel, String logMessage, Exception exception) {
		StackTraceElement caller = exception != null ? exception.getStackTrace()[1]
				: new Exception().getStackTrace()[1];
		String classFileName = caller.getFileName();
		int lineNo = caller.getLineNumber();
		Date logTime = new Date();
		String screenId = getScreenId(caller);
		AccessLogs newLog = new AccessLogs();
		newLog.setClassFileName(classFileName);
		newLog.setScreenId(screenId);
		newLog.setLineNo(lineNo);
		newLog.setLevel(logLevel.toUpperCase());
		newLog.setMessage(logMessage);
		newLog.setTime(logTime);
		createAccessLog(newLog);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.com.parkson.ntpoms.common.services.AccessLogService#log(java.lang.
	 * String, java.lang.String, java.lang.String, java.lang.Exception)
	 */
	@Override
	public void log(String logDbName, String logLevel, String logMessage, Exception exception) {
		StackTraceElement caller = new Exception().getStackTrace()[1];
		String classFileName = caller.getFileName();
		int lineNo = caller.getLineNumber();
		Date logTime = new Date();
		String screenId = getScreenId(caller);
		AccessLogs newLog = new AccessLogs();
		newLog.setClassFileName(classFileName);
		newLog.setScreenId(screenId);
		newLog.setLineNo(lineNo);
		newLog.setLevel(logLevel.toUpperCase());
		newLog.setMessage(logMessage);
		newLog.setTime(logTime);
		createAccessLog(newLog);
	}

	/**
	 * Gets the screen id.
	 *
	 * @param caller
	 *            the caller
	 * @return the screen id
	 */
	private String getScreenId(StackTraceElement caller) {
		String screenId = null;
		if (caller.getClassName().contains("auditTrail"))
			screenId = "PO-UT01L";
		else if (caller.getClassName().contains("Log"))
			screenId = "PO-UT02L";

		else if (caller.getClassName().contains("CreditTerms"))
			screenId = "PO-UT02L";

		else if (caller.getClassName().contains("VendorCreditTerms"))
			screenId = "PO-UT02L";

		else if (caller.getClassName().contains("VendorBranch"))
			screenId = "PO-UT02L";

		else if (caller.getClassName().contains("Project"))
			screenId = "PO-UT02L";

		else if (caller.getClassName().contains("LogSetting"))
			screenId = "PO-AD11L";
		else if (caller.getClassName().contains("Login"))
			screenId = "PO-LI01S";
		else if (caller.getClassName().contains("MainScreen"))
			screenId = "PO-HM01S";
		else if (caller.getClassName().contains("mail"))
			screenId = "Mail Service";
		else if (caller.getClassName().contains("velocity"))
			screenId = "Mail Service";
		else
			screenId = caller.getClassName();

		return screenId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.AccessLogService#
	 * getAllPageableAccessLog(org.springframework.data.domain.Pageable)
	 */
	@Override
	public Page<AccessLogs> getAllPageableAccessLog(Pageable pageRequest) {
		return accessLogRepository.findAll(pageRequest);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.AccessLogService#
	 * getAllPageableAccessLog(org.springframework.data.jpa.domain.
	 * Specification, org.springframework.data.domain.Pageable)
	 */
	@Override
	public Page<AccessLogs> getAllPageableAccessLog(Specification<AccessLogs> s, Pageable pageRequest) {
		return accessLogRepository.findAll(s, pageRequest);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.CompanyService#
	 * getAllPageableCompany(org.springframework.data.jpa.domain.Specification,
	 * org.springframework.data.domain.Pageable)
	 */
	@Override
	public Page<AccessLogs> getAllPageableLogOrdered(Pageable pageRequest) {

		return accessLogRepository.findAllByOrderByIdDesc(pageRequest);
	}

	@Override
	public void log(String logLevel, String logMessage, Exception exception, String company) {
		StackTraceElement caller = exception != null ? exception.getStackTrace()[0]
				: new Exception().getStackTrace()[1];
		String classFileName = caller.getFileName();
		int lineNo = caller.getLineNumber();
		Date logTime = new Date();
		String screenId = getScreenId(caller);
		AccessLogs newLog = new AccessLogs();
		newLog.setCompany(company);
		newLog.setClassFileName(classFileName);
		newLog.setScreenId(screenId);
		newLog.setLineNo(lineNo);
		newLog.setLevel(logLevel.toUpperCase());
		newLog.setMessage(exception.getMessage());
		newLog.setTime(logTime);
		createAccessLog(newLog);
	}

}