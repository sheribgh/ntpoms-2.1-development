/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import my.com.parkson.ntpoms.common.entities.SystemParameter;

/**
 * The Interface SystemParameterRepository.
 */
public interface SystemParameterRepository
		extends JpaRepository<SystemParameter, Integer>, JpaSpecificationExecutor<SystemParameter> {

	/**
	 * Find all by is active order by last modified on desc.
	 *
	 * @param isActive the is active
	 * @return the list
	 */
	public List<SystemParameter> findAllByIsActiveOrderByLastModifiedOnDesc(boolean isActive);

	/**
	 * Find by code.
	 *
	 * @param code the code
	 * @return the system parameter
	 */
	public SystemParameter findByCode(int code);

	/**
	 * Find by prop name.
	 *
	 * @param propName the prop name
	 * @return the system parameter
	 */
	public SystemParameter findByPropName(String propName);
	
	/**
	 * Find by prop name.
	 *
	 * @param propName the prop name
	 * @return the system parameter
	 */
	public SystemParameter findByPropValue(String propValue);
	
	/**
	 * Find by prop name.
	 *
	 * @param propName the prop name
	 * @return the system parameter
	 */
	@Query("select o.propValue from SystemParameter AS o where o.propName= :propName")
	public String findValueByPropName(@Param("propName") String propName);
	
	/**
	 * Find all by order by last modified on desc.
	 *
	 * @return the list
	 */
	public Page<SystemParameter> findAllByOrderByLastModifiedOnDesc(Pageable pageRequest);

	/**
	 * Find all by order by last modified on desc.
	 *
	 * @return the list
	 */
	public Page<SystemParameter> findAllByOrderByLastModifiedOnDesc(Specification<SystemParameter> s, Pageable pageRequest);

}