/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.utils;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.stat.Statistics;

import my.com.parkson.ntpoms.POException;

/**
 * The Class HibernateUtility.
 */
public class HibernateUtility {
	
	/** The Constant sessionFactory. */
	private static final SessionFactory sessionFactory;

	/** The service registry. */
	private static ServiceRegistry serviceRegistry;

	static {
		try {
			StandardServiceRegistryBuilder standardRegistry = 
			new StandardServiceRegistryBuilder();
			serviceRegistry = standardRegistry.configure("hibernate.cfg.xml").build();
			
			Metadata metaData = new MetadataSources(serviceRegistry).getMetadataBuilder().build();
			sessionFactory = metaData.getSessionFactoryBuilder().build();			
			Statistics stacs = sessionFactory.getStatistics();
			stacs.setStatisticsEnabled(true);
		} catch (POException ex) {
			throw new POException(ex);
		}
	}
	
	/**
	 * Gets the session factory.
	 *
	 * @return the session factory
	 */
	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	
   /**
    * Shutdown.
    */
   public static void shutdown() {
      if (serviceRegistry != null) {
         StandardServiceRegistryBuilder.destroy(serviceRegistry);
      }
   }

   /**
    * Gets the session.
    *
    * @return the session
    */
   public static Session getSession() {
	    Session currentSession = HibernateUtility.getSessionFactory().getCurrentSession();
	    if(!currentSession.getTransaction().isActive()) {
	        currentSession.beginTransaction();
	    }
	    
	    return currentSession;
   }
   
   /**
    * Instantiates a new hibernate utility.
    */
   private HibernateUtility() {}
}