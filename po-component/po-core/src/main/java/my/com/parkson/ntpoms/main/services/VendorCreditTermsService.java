package my.com.parkson.ntpoms.main.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import my.com.parkson.ntpoms.main.entities.VendorCreditTerms;
import my.com.parkson.ntpoms.main.entities.VendorCreditTermsId;


public interface VendorCreditTermsService {
	
	//public List<VendorCreditTerms> getAllVendorCreditTerms();	
	//public List<VendorCreditTerms> getAllActiveVendorCreditTerms();	
	public void createVendorCreditTerms(VendorCreditTerms vendorCreditTerms);
	public List<VendorCreditTerms> getVendorCreditTermsById(String creditTermsCode);
	//public List<String> getVendorCreditTermsByVendorCode(String vendorCode);
	//public VendorCreditTerms getVendorCreditTermsByVendorCodeAndCreditTermsCodeAndCompanyCodeAndVendorBranchCode(VendorCreditTermsId VendorCreditTermsId);
	public VendorCreditTerms getVendorCreditTermsByVendorCodeAndCreditTermsCodeAndCompanyCodeAndVendorBranchCode(String vendorCode, String creditTermsCode , String companyCode , String vendorBranchCode);
	
	public List<VendorCreditTerms> getVendorCreditTermsByVendorCodeAndCreditTermsCodeAndVendorBranchCode(String vendorCode, String creditTermsCode , String vendorBranchCode);
	public List<VendorCreditTerms> getVendorCreditTermsByVendorCodeAndVendorBranchCodeAndCompanyCode(String vendorCode, String vendorBranchCode, String companyCode);
	
	public VendorCreditTerms getVendorCreditTermsById(VendorCreditTermsId vendorCreditTermsId);
	public Page<VendorCreditTerms> getPagebleAll(Specification<VendorCreditTerms> s,Pageable req);
	public Page<VendorCreditTerms> getAllPageableVendCrtOrdered(Pageable pageRequest);
	public long getCount();
	
	public long getCountActive();
}