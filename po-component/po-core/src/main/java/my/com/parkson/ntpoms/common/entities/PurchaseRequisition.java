/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The Class PurchaseRequisition.
 */
@Entity
@Table(name = "\"MF15_M_PRMAST\"")
@Data
@NoArgsConstructor
public class PurchaseRequisition implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The type code. */
	@Id
	@Column(name = "\"mf15_prTypeCode\"")
	private String typeCode;
	
	/** The abbr name. */
	@Column(name = "\"mf15_prAbbrName\"")
	private String abbrName;
	
	/** The desc. */
	@Column(name = "\"mf15_prDesc\"")
	private String desc;
		
	/** The is active. */
	@Column(name = "\"mf15_isActive\"")
	private boolean isActive;
	
	/** The created by. */
	@Column(name = "\"mf15_createdBy\"")
	private String createdBy;
	
	/** The created on. */
	@Column(name = "\"mf15_createdOn\"")
	private Date createdOn;
	
	/** The last modified by. */
	@Column(name = "\"mf15_lastModifiedBy\"")
	private String lastModifiedBy;
	
	/** The last modified on. */
	@Column(name = "\"mf15_lastModifiedOn\"")
	private Date lastModifiedOn;
	
	/** The deactivated by. */
	@Column(name = "\"mf15_deactivatedBy\"")
	private String deactivatedBy;
	
	/** The deactivated on. */
	@Column(name = "\"mf15_deactivatedOn\"")
	private Date deactivatedOn;
	
	/** The reactivated by. */
	@Column(name = "\"mf15_reactivatedBy\"")
	private String reactivatedBy;
	
	/** The reactivated on. */
	@Column(name = "\"mf15_reactivatedOn\"")
	private Date reactivatedOn;
	
	/**
	 * Gets the type code.
	 *
	 * @return the type code
	 */
	public String getTypeCode() {
		return typeCode;
	}

	/**
	 * Sets the type code.
	 *
	 * @param typeCode the new type code
	 */
	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}

	/**
	 * Gets the abbr name.
	 *
	 * @return the abbr name
	 */
	public String getAbbrName() {
		return abbrName;
	}

	/**
	 * Sets the abbr name.
	 *
	 * @param abbrName the new abbr name
	 */
	public void setAbbrName(String abbrName) {
		this.abbrName = abbrName;
	}

	/**
	 * Gets the desc.
	 *
	 * @return the desc
	 */
	public String getDesc() {
		return desc;
	}

	/**
	 * Sets the desc.
	 *
	 * @param desc the new desc
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}

	/**
	 * Checks if is active.
	 *
	 * @return true, if is active
	 */
	public boolean isActive() {
		return isActive;
	}

	/**
	 * Sets the active.
	 *
	 * @param isActive the new active
	 */
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	/**
	 * Gets the created by.
	 *
	 * @return the created by
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * Sets the created by.
	 *
	 * @param createdBy the new created by
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Gets the created on.
	 *
	 * @return the created on
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * Sets the created on.
	 *
	 * @param createdOn the new created on
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * Gets the last modified by.
	 *
	 * @return the last modified by
	 */
	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	/**
	 * Sets the last modified by.
	 *
	 * @param lastModifiedBy the new last modified by
	 */
	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	/**
	 * Gets the last modified on.
	 *
	 * @return the last modified on
	 */
	public Date getLastModifiedOn() {
		return lastModifiedOn;
	}

	/**
	 * Sets the last modified on.
	 *
	 * @param lastModifiedOn the new last modified on
	 */
	public void setLastModifiedOn(Date lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

	/**
	 * Gets the deactivated by.
	 *
	 * @return the deactivated by
	 */
	public String getDeactivatedBy() {
		return deactivatedBy;
	}

	/**
	 * Sets the deactivated by.
	 *
	 * @param deactivatedBy the new deactivated by
	 */
	public void setDeactivatedBy(String deactivatedBy) {
		this.deactivatedBy = deactivatedBy;
	}

	/**
	 * Gets the deactivated on.
	 *
	 * @return the deactivated on
	 */
	public Date getDeactivatedOn() {
		return deactivatedOn;
	}

	/**
	 * Sets the deactivated on.
	 *
	 * @param deactivatedOn the new deactivated on
	 */
	public void setDeactivatedOn(Date deactivatedOn) {
		this.deactivatedOn = deactivatedOn;
	}

	/**
	 * Gets the reactivated by.
	 *
	 * @return the reactivated by
	 */
	public String getReactivatedBy() {
		return reactivatedBy;
	}

	/**
	 * Sets the reactivated by.
	 *
	 * @param reactivatedBy the new reactivated by
	 */
	public void setReactivatedBy(String reactivatedBy) {
		this.reactivatedBy = reactivatedBy;
	}

	/**
	 * Gets the reactivated on.
	 *
	 * @return the reactivated on
	 */
	public Date getReactivatedOn() {
		return reactivatedOn;
	}

	/**
	 * Sets the reactivated on.
	 *
	 * @param reactivatedOn the new reactivated on
	 */
	public void setReactivatedOn(Date reactivatedOn) {
		this.reactivatedOn = reactivatedOn;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return typeCode + " - " + abbrName;
	}
}