/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import my.com.parkson.ntpoms.common.entities.EmployeeMaster;
import my.com.parkson.ntpoms.common.entities.ProfitCenter;
import my.com.parkson.ntpoms.common.entities.PurchaseOrderUser;

/**
 * The Interface EmployeeMasterRepository.
 */
public interface EmployeeMasterRepository
		extends JpaRepository<EmployeeMaster, Integer>, JpaSpecificationExecutor<EmployeeMaster> {

	/* (non-Javadoc)
	 * @see org.springframework.data.jpa.repository.JpaRepository#findAll()
	 */
	public List<EmployeeMaster> findAll();

	/**
	 * Find all by is active.
	 *
	 * @param isActive the is active
	 * @return the list
	 */
	@Query("select o from EmployeeMaster o where o.isActive =:act")
	public List<EmployeeMaster> findAllByIsActive(@Param("act") boolean isActive);

	/**
	 * Find by ps id.
	 *
	 * @param psId the ps id
	 * @return the employee master
	 */
	public EmployeeMaster findByPsId(String psId);

	/**
	 * Find all active by profit center code.
	 *
	 * @param code the code
	 * @return the list
	 */
	@Query("select o from EmployeeMaster o where o.isActive = true AND o.profitCenter.code = ?1")
	public List<EmployeeMaster> findAllActiveByProfitCenterCode(String code);
	
	public List<EmployeeMaster> findAllByProfitCenterCodeAndIsActiveOrderByLastModifiedOnDesc(String code,boolean isActive);

	/**
	 * Find all active ps ids.
	 *
	 * @return the list
	 */
	@Query("select o.psId from EmployeeMaster o where o.isActive = true")
	public List<String> findAllActivePsIds();

	/**
	 * Find selected fields.
	 *
	 * @return the list
	 */
	/*public List<EmployeeMaster> findSelectedFields();*/
	
	/**
	 * Find all by order by last modified on desc.
	 *
	 * @return the list
	 */
	public Page<EmployeeMaster> findAllByOrderByLastModifiedOnDesc(Pageable pageRequest);

	/**
	 * Find all by order by last modified on desc.
	 *
	 * @return the list
	 */
	public Page<EmployeeMaster> findAllByOrderByLastModifiedOnDesc(Specification<EmployeeMaster> s, Pageable pageRequest);
	
	/**
	 * Find all active by company code.
	 *
	 * @param code the code
	 * @return the list
	 */
	@Query("select o from EmployeeMaster o where o.psId = ?1 ")
	public List<EmployeeMaster> findAllEmpNameByPsId(String psid);

}