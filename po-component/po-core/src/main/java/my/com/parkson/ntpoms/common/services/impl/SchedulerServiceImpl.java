/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services.impl;

import static my.com.parkson.ntpoms.common.MessageCodes.FAILED_CLOSE_CONNECTION_EXCEPTION;
import static my.com.parkson.ntpoms.common.MessageCodes.FAILED_CLOSE_STATEMENT_EXCEPTION;
import static my.com.parkson.ntpoms.common.MessageCodes.FAILED_FETCH_RECORDS_EXCEPTION;
import static my.com.parkson.ntpoms.common.MessageCodes.FAILED_NOTFOUND_EXCEPTION;
import static my.com.parkson.ntpoms.common.MessageCodes.FAILED_PERSIST_EXCEPTION;
import static my.com.parkson.ntpoms.common.MessageCodes.FAILED_SQLEXCEPTION;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.LogMessages;
import my.com.parkson.ntpoms.common.entities.Scheduler;
import my.com.parkson.ntpoms.common.repositories.SchedulerRepository;
import my.com.parkson.ntpoms.common.services.MessageService;
import my.com.parkson.ntpoms.common.services.SchedulerService;

/**
 * The Class SchedulerServiceImpl.
 */
@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class SchedulerServiceImpl implements SchedulerService {
	private static final Logger LOGGER = Logger.getLogger(SchedulerServiceImpl.class);

	/** The messages. */
	@Autowired
	private MessageService messages;

	/** The scheduler repository. */
	@Autowired
	private SchedulerRepository schedulerRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.com.parkson.ntpoms.common.services.SchedulerService#createScheduler(my
	 * .com.parkson.ntpoms.common.entities.Scheduler)
	 */
	@Override
	public void createScheduler(Scheduler scheduler) {
		try {
			schedulerRepository.save(scheduler);
		} catch (POException e) {
			LOGGER.error(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.com.parkson.ntpoms.common.services.SchedulerService#getSchedulerById(
	 * int)
	 */
	@Override
	public Scheduler getSchedulerById(int id) {
		try {
			return schedulerRepository.findById(id);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.com.parkson.ntpoms.common.services.SchedulerService#getAllScheduler()
	 */
	@Override
	public List<Scheduler> getAllScheduler() {
		List<Scheduler> schedulerList = new ArrayList<>();
		try {
			schedulerList = schedulerRepository.findAll();
		} catch (POException e) {
			LOGGER.error(e);
		}
		return schedulerList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.SchedulerService#
	 * getAllSchedulerInOrder()
	 */
	@Override
	public List<Scheduler> getAllSchedulerInOrder() {
		List<Scheduler> schedulerList = new ArrayList<>();
		try {
			schedulerList = schedulerRepository.findAllByOrderByIdDesc();
		} catch (POException e) {
			LOGGER.error(e);
		}
		return schedulerList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.com.parkson.ntpoms.common.services.SchedulerService#checkDatabase(java
	 * .lang.String, java.lang.String, java.lang.String)
	 */
	public boolean checkDatabase(String datasourceUrl, String username, String password) throws POException {
		String actualDatasourceUrl = "jdbc:postgresql://localhost:5432/ntpoms";
		String actualUsername = "postgres";
		String actualPassword = "password";

		if (datasourceUrl != null) {
			actualDatasourceUrl = datasourceUrl;
		}
		if (username != null) {
			actualUsername = username;
		}
		if (password != null) {
			actualPassword = password;
		}

		Connection connection = null;
		Statement statement = null;
		try {
			Class.forName("org.postgresql.Driver");
			connection = DriverManager.getConnection(actualDatasourceUrl, actualUsername, actualPassword);
			return true;
		} catch (POException e) {
			return false;
		} catch (ClassNotFoundException e) {
			LOGGER.error(e);
			return false;
		} catch (SQLException e) {
			LOGGER.error(e);
			return false;
		} finally {
			try {
				if (statement != null)
					statement.close();
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				LOGGER.error(e);
			} catch (POException e) {
				LOGGER.error(e);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.com.parkson.ntpoms.common.services.SchedulerService#checkTable(java.
	 * lang.String)
	 */
	@Override
	public boolean checkTable(String database) {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.SchedulerService#
	 * getAllPageableScheduler(org.springframework.data.domain.Pageable)
	 */
	@Override
	public Page<Scheduler> getAllPageableScheduler(Pageable pageRequest) {
		return schedulerRepository.findAll(pageRequest);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.SchedulerService#
	 * getAllPageableScheduler(org.springframework.data.jpa.domain.
	 * Specification, org.springframework.data.domain.Pageable)
	 */
	@Override
	public Page<Scheduler> getAllPageableScheduler(Specification<Scheduler> s, Pageable pageRequest) {
		return schedulerRepository.findAll(s, pageRequest);
	}

	@Override
	public Scheduler getSchedulerByJobname(String name) {
		try {
			return schedulerRepository.findByJobName(name);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	@Override
	public long getCount() {
		return schedulerRepository.count();
	}

	@Override
	public boolean deleteSched(Scheduler sched) {

		boolean isDeleted = true;
		try {
			schedulerRepository.delete(sched);
		} catch (Exception e) {
			System.out.println("ERRORRRR " + e.getMessage());
			isDeleted = false;
		}

		return isDeleted;

	}
}