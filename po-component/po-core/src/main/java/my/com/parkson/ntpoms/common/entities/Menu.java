/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The Class Menu.
 */
@Entity
@Table(name = "menu")
public class Menu implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The menu id. */
	@Id
	@Column(name="menu_id", length = 100)
	private Integer menuId;

	/** The menu code. */
	@Column(name="menu_code", length = 100)
	private String menuCode;

	/** The menu desc. */
	@Column(name="menu_desc", length = 200)
	private String menuDesc;

	/** The menu url. */
	@Column(name="menu_url", length = 100)
	private String menuUrl;

	/** The menu reference. */
	@Column(name="menu_reference", length = 100)
	private String menuReference;

	/**
	 * Gets the menu id.
	 *
	 * @return the menu id
	 */
	public Integer getMenuId() {
		return menuId;
	}

	/**
	 * Sets the menu id.
	 *
	 * @param menuId the new menu id
	 */
	public void setMenuId(Integer menuId) {
		this.menuId = menuId;
	}

	/**
	 * Gets the menu code.
	 *
	 * @return the menu code
	 */
	public String getMenuCode() {
		return menuCode;
	}

	/**
	 * Sets the menu code.
	 *
	 * @param menuCode the new menu code
	 */
	public void setMenuCode(String menuCode) {
		this.menuCode = menuCode;
	}

	/**
	 * Gets the menu desc.
	 *
	 * @return the menu desc
	 */
	public String getMenuDesc() {
		return menuDesc;
	}

	/**
	 * Sets the menu desc.
	 *
	 * @param menuDesc the new menu desc
	 */
	public void setMenuDesc(String menuDesc) {
		this.menuDesc = menuDesc;
	}

	/**
	 * Gets the menu url.
	 *
	 * @return the menu url
	 */
	public String getMenuUrl() {
		return menuUrl;
	}

	/**
	 * Sets the menu url.
	 *
	 * @param menuUrl the new menu url
	 */
	public void setMenuUrl(String menuUrl) {
		this.menuUrl = menuUrl;
	}

	/**
	 * Gets the menu reference.
	 *
	 * @return the menu reference
	 */
	public String getMenuReference() {
		return menuReference;
	}

	/**
	 * Sets the menu reference.
	 *
	 * @param menuReference the new menu reference
	 */
	public void setMenuReference(String menuReference) {
		this.menuReference = menuReference;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Menu [menuId=" + menuId + ", menuCode=" + menuCode + "]";
	}
}