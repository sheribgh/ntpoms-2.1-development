/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import my.com.parkson.ntpoms.common.entities.OrderSubCategory;

/**
 * The Interface OrderSubCategoryService.
 */
public interface OrderSubCategoryService {

	/**
	 * Gets the all order sub category.
	 *
	 * @return the all order sub category
	 */
	public List<OrderSubCategory> getAllOrderSubCategory();
	
	
	
	
	public long getCount();

	/**
	 * Gets the all pageable order sub category.
	 *
	 * @param pageRequest the page request
	 * @return the all pageable order sub category
	 */
	public Page<OrderSubCategory> getAllPageableOrderSubCategory(Pageable pageRequest);

	/**
	 * Gets the all pageable order sub category.
	 *
	 * @param s the s
	 * @param pageRequest the page request
	 * @return the all pageable order sub category
	 */
	public Page<OrderSubCategory> getAllPageableOrderSubCategory(Specification<OrderSubCategory> s,
			Pageable pageRequest);

	
	/**
	 * Gets the all pageable order sub category.
	 *
	 * @param pageRequest the page request
	 * @return the all pageable order sub category
	 */
	public Page<OrderSubCategory> getAllPageableOrderSubCategoryOrdered(Pageable pageRequest);

	/**
	 * Gets the all pageable order sub category.
	 *
	 * @param s the s
	 * @param pageRequest the page request
	 * @return the all pageable order sub category
	 */
	public Page<OrderSubCategory> getAllPageableOrderSubCategoryOrdered(Specification<OrderSubCategory> s,
			Pageable pageRequest);

	/**
	 * Gets the all active order sub category.
	 *
	 * @return the all active order sub category
	 */
	public List<OrderSubCategory> getAllActiveOrderSubCategory();

	/**
	 * Gets the order sub category by code.
	 *
	 * @param code the code
	 * @return the order sub category by code
	 */
	public OrderSubCategory getOrderSubCategoryByCode(String code);

	/**
	 * Creates the order sub category.
	 *
	 * @param orderSubCategory the order sub category
	 */
	public void createOrderSubCategory(OrderSubCategory orderSubCategory);

	/**
	 * Gets the all order sub category by order category code.
	 *
	 * @param orderCategoryCode the order category code
	 * @return the all order sub category by order category code
	 */
	public List<OrderSubCategory> getAllOrderSubCategoryByOrderCategoryCode(String orderCategoryCode);

	/**
	 * Gets the all active order sub category by order category code.
	 *
	 * @param orderCategoryCode the order category code
	 * @return the all active order sub category by order category code
	 */
	public List<OrderSubCategory> getAllActiveOrderSubCategoryByOrderCategoryCode(String orderCategoryCode);
}