/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import my.com.parkson.ntpoms.common.entities.Scheduler;

/**
 * The Interface SchedulerService.
 */
public interface SchedulerService {

	/**
	 * Creates the scheduler.
	 *
	 * @param scheduler the scheduler
	 */
	public void createScheduler(Scheduler scheduler);
	
	
	public long getCount();

	/**
	 * Gets the all pageable scheduler.
	 *
	 * @param pageRequest the page request
	 * @return the all pageable scheduler
	 */
	public Page<Scheduler> getAllPageableScheduler(Pageable pageRequest);

	/**
	 * Gets the all pageable scheduler.
	 *
	 * @param s the s
	 * @param pageRequest the page request
	 * @return the all pageable scheduler
	 */
	public Page<Scheduler> getAllPageableScheduler(Specification<Scheduler> s, Pageable pageRequest);

	/**
	 * Gets the scheduler by id.
	 *
	 * @param id the id
	 * @return the scheduler by id
	 */
	public Scheduler getSchedulerById(int id);

	/**
	 * Gets the all scheduler.
	 *
	 * @return the all scheduler
	 */
	public List<Scheduler> getAllScheduler();

	/**
	 * Gets the all scheduler in order.
	 *
	 * @return the all scheduler in order
	 */
	public List<Scheduler> getAllSchedulerInOrder();

	/**
	 * Check database.
	 *
	 * @param datasourceUrl the datasource url
	 * @param username the username
	 * @param password the password
	 * @return true, if successful
	 */
	public boolean checkDatabase(String datasourceUrl, String username, String password);

	/**
	 * Check table.
	 *
	 * @param database the database
	 * @return true, if successful
	 */
	public boolean checkTable(String database);
	
	public boolean deleteSched(Scheduler sched);
	
	public Scheduler getSchedulerByJobname(String name);
}