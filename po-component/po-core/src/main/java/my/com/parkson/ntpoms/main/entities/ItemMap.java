package my.com.parkson.ntpoms.main.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "po03_t_itemmap")
@Data
@NoArgsConstructor
public class ItemMap implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "po03_mapcode")
	private int code;
	
	@Column(name = "po01tmain_po03_pono")
	private String poNo;
	
	@Column(name = "po02titeminfo_po03_itemno")
	private String itemNo;
	
	@Column(name = "po02titeminfo_po03_particulars")
	private String particulars;
	
	@Column(name = "po02titeminfo_po03_packing")
	private String packing;
	
	@Column(name = "po03_itemorder")
	private int itemOrder;
	
	@Column(name = "mf16mgstmast_po03_taxcode")
	private String taxcode;
	
	@Column(name = "po03_taxrate")
	private double taxRate;
	
	@Column(name = "po03_variance")
	private double variance;

	@Column(name = "po03_unitpriceexclgst")
	private double unitPriceExclGST;

	@Column(name = "po03_unitpriceinclgst")
	private double unitPriceInclGST;
	
	@Column(name = "po03_discount")
	private double discount;
	
	@Column(name = "po03_totalexclgst")
	private double totalExclGST;

	@Column(name = "po03_totalinclgst")
	private double totalInclGST;
	
	@Column(name = "po03_totalgst")
	private double totalGST;
	
	@Column(name = "po03_quantity")
	private double quantity;

	@Column(name = "po03_totaldiscount")
	private double totalDiscount;

	@Column(name = "po03_unitgst")
	private double unitGST;

	@Column(name = "mf13mpcmast_po03_pccode")
	private String pcCode;
	
	@Column(name = "po03_olditem")
	private boolean oldlst;
	
	public double getVariance() {
		return variance;
	}

	public void setVariance(double variance) {
		this.variance = variance;
	}
	
	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getPoNo() {
		return poNo;
	}

	public void setPoNo(String poNo) {
		this.poNo = poNo;
	}

	public String getItemNo() {
		return itemNo;
	}

	public void setItemNo(String itemNo) {
		this.itemNo = itemNo;
	}

	public String getParticulars() {
		return particulars;
	}

	public void setParticulars(String particulars) {
		this.particulars = particulars;
	}

	public String getPacking() {
		return packing;
	}

	public void setPacking(String packing) {
		this.packing = packing;
	}

	public int getItemOrder() {
		return itemOrder;
	}

	public void setItemOrder(int itemOrder) {
		this.itemOrder = itemOrder;
	}

	public String getTaxcode() {
		return taxcode;
	}

	public void setTaxcode(String taxcode) {
		this.taxcode = taxcode;
	}

	public double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(double taxRate) {
		this.taxRate = taxRate;
	}

	public double getUnitPriceExclGST() {
		return unitPriceExclGST;
	}

	public void setUnitPriceExclGST(double unitPriceExclGST) {
		this.unitPriceExclGST = unitPriceExclGST;
	}

	public double getUnitPriceInclGST() {
		return unitPriceInclGST;
	}

	public void setUnitPriceInclGST(double unitPriceInclGST) {
		this.unitPriceInclGST = unitPriceInclGST;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public double getTotalExclGST() {
		return totalExclGST;
	}

	public void setTotalExclGST(double totalExclGST) {
		this.totalExclGST = totalExclGST;
	}

	public double getTotalInclGST() {
		return totalInclGST;
	}

	public void setTotalInclGST(double totalInclGST) {
		this.totalInclGST = totalInclGST;
	}

	public double getTotalGST() {
		return totalGST;
	}

	public void setTotalGST(double totalGST) {
		this.totalGST = totalGST;
	}

	public double getQuantity() {
		return quantity;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	public double getTotalDiscount() {
		return totalDiscount;
	}

	public void setTotalDiscount(double totalDiscount) {
		this.totalDiscount = totalDiscount;
	}

	public double getUnitGST() {
		return unitGST;
	}

	public void setUnitGST(double unitGST) {
		this.unitGST = unitGST;
	}

	public String getPcCode() {
		return pcCode;
	}

	public void setPcCode(String pcCode) {
		this.pcCode = pcCode;
	}

	public boolean isOldlst() {
		return oldlst;
	}

	public void setOldlst(boolean oldlst) {
		this.oldlst = oldlst;
	}

}