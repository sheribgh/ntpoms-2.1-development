/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.utils;

import static my.com.parkson.ntpoms.common.MessageCodes.FAILED_FETCH_RECORDS_EXCEPTION;
import static my.com.parkson.ntpoms.common.MessageCodes.FAILED_INTERRUPTED_EXCEPTION;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;

import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.LogMessages;
import my.com.parkson.ntpoms.common.services.AccessLogService;
import my.com.parkson.ntpoms.common.services.MessageService;
import my.com.parkson.ntpoms.main.entities.PurchaseOrder;
import my.com.parkson.ntpoms.main.entities.PurchaseOrderArchiveDetails;

/**
 * The Class HibernateSearch.
 */
public class HibernateSearch {

	/** The log. */
	@Autowired
	private AccessLogService log;

	/** The messages. */
	@Autowired
	private MessageService messages;

	/**
	 * Instantiates a new hibernate search.
	 *
	 * @param tenantID
	 *            the tenant ID
	 */
	public HibernateSearch() {		
	}

	/**
	 * Search main.
	 *
	 * @param searchWord
	 *            the search word
	 * @param searchNumeral
	 *            the search numeral
	 * @param maxResult
	 *            the max result
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<PurchaseOrder> SearchMain(Map<String, String> searchWord, Map<String, Double> searchNumeral, int maxResult) {		
		Session session = HibernateUtility.getSessionFactory().withOptions().openSession();
		FullTextSession fullTextSession = Search.getFullTextSession(session);
		Transaction tnx = fullTextSession.beginTransaction();		
		try {
			fullTextSession.createIndexer().startAndWait();
		} catch (POException | InterruptedException e) {
			log.log(LogMessages.ERROR.value(), messages.getMessage(FAILED_INTERRUPTED_EXCEPTION), e);
		}
		
		QueryBuilder qb = fullTextSession.getSearchFactory().buildQueryBuilder().forEntity(PurchaseOrder.class).get();
		BooleanQuery.Builder finalLuceneQuery = new BooleanQuery.Builder();
		
		if (searchWord.get("poNo") != null) {
			org.apache.lucene.search.Query querypono = qb.keyword().onField("poNo")
					.matching(searchWord.get("poNo")).createQuery();
			finalLuceneQuery.add(querypono, BooleanClause.Occur.MUST);
		}

		if (searchWord.get("status") != null) {
			org.apache.lucene.search.Query queryStatus = qb.simpleQueryString().onField("status")
					.matching(searchWord.get("status")).createQuery();
			finalLuceneQuery.add(queryStatus, BooleanClause.Occur.MUST);
		}

		if (searchWord.get("issuer") != null) {
			org.apache.lucene.search.Query queryIssuer = qb.simpleQueryString().onField("issuer")
					.matching(searchWord.get("issuer")).createQuery();
			finalLuceneQuery.add(queryIssuer, BooleanClause.Occur.MUST);
		}

		if (searchWord.get("verifier") != null) {
			org.apache.lucene.search.Query queryverifier = qb.simpleQueryString().onField("verifier")
					.matching(searchWord.get("verifier")).createQuery();
			finalLuceneQuery.add(queryverifier, BooleanClause.Occur.MUST);
		}

		if (searchWord.get("authorizer") != null) {
			org.apache.lucene.search.Query queryauthorizer = qb.simpleQueryString()
					.onField("authorizer").matching(searchWord.get("authorizer")).createQuery();
			finalLuceneQuery.add(queryauthorizer, BooleanClause.Occur.MUST);
		}

		if (searchWord.get("rejectedBy") != null) {
			org.apache.lucene.search.Query queryauthorizer = qb.simpleQueryString().onField("rejectedBy")
					.matching(searchWord.get("rejectedBy")).createQuery();
			finalLuceneQuery.add(queryauthorizer, BooleanClause.Occur.MUST);
		}

		if (searchWord.get("cancelledBy") != null) {
			org.apache.lucene.search.Query queryauthorizer = qb.simpleQueryString().onField("cancelledBy")
					.matching(searchWord.get("cancelledBy")).createQuery();
			finalLuceneQuery.add(queryauthorizer, BooleanClause.Occur.MUST);
		}

		if (searchWord.get("deletedBy") != null) {
			org.apache.lucene.search.Query queryauthorizer = qb.simpleQueryString().onField("deletedBy")
					.matching(searchWord.get("deletedBy")).createQuery();
			finalLuceneQuery.add(queryauthorizer, BooleanClause.Occur.MUST);
		}

		if (searchWord.get("closedBy") != null) {
			org.apache.lucene.search.Query queryauthorizer = qb.simpleQueryString().onField("closedBy")
					.matching(searchWord.get("closedBy")).createQuery();
			finalLuceneQuery.add(queryauthorizer, BooleanClause.Occur.MUST);
		}

		if (searchWord.get("orderCat") != null) {
			org.apache.lucene.search.Query queryordercat = qb.simpleQueryString()
					.onField("orderCatCode").matching(searchWord.get("orderCat")).createQuery();
			finalLuceneQuery.add(queryordercat, BooleanClause.Occur.MUST);
		}

		if (searchWord.get("orderSubCat") != null) {
			org.apache.lucene.search.Query queryordersub = qb.simpleQueryString()
					.onField("ordSubCatCode").matching(searchWord.get("orderSubCat")).createQuery();
			finalLuceneQuery.add(queryordersub, BooleanClause.Occur.MUST);
		}

		if (searchWord.get("pcCode") != null) {
			org.apache.lucene.search.Query querypccode = qb.simpleQueryString().onField("pcCode")
					.matching(searchWord.get("pcCode")).createQuery();
			finalLuceneQuery.add(querypccode, BooleanClause.Occur.MUST);
		}

		if (searchWord.get("vendorCode") != null) {
			org.apache.lucene.search.Query queryvendor = qb.simpleQueryString()
					.onField("vendorCode").matching(searchWord.get("vendorCode")).createQuery();
			finalLuceneQuery.add(queryvendor, BooleanClause.Occur.MUST);

		}

		if (searchWord.get("vendorName") != null) {
			org.apache.lucene.search.Query queryIssuer = qb.simpleQueryString().onField("vendorName")
					.matching(searchWord.get("vendorName")).createQuery();
			finalLuceneQuery.add(queryIssuer, BooleanClause.Occur.MUST);
		}

		if (searchNumeral.get("totalMax") != null) {
			org.apache.lucene.search.Query querytotal = qb.range().onField("totalInclGST")
					.above(searchNumeral.get("totalMax")).createQuery();
			finalLuceneQuery.add(querytotal, BooleanClause.Occur.MUST);
		}

		if (searchNumeral.get("totalMin") != null) {
			org.apache.lucene.search.Query querytotal2 = qb.range().onField("totalInclGST")
					.below(searchNumeral.get("totalMin")).createQuery();
			finalLuceneQuery.add(querytotal2, BooleanClause.Occur.MUST);
		}

		if (searchWord.get("issueDate") != null) {
			org.apache.lucene.search.Query querydate = qb.simpleQueryString().onField("issuedOn")
					.matching(searchWord.get("issueDate")).createQuery();
			finalLuceneQuery.add(querydate, BooleanClause.Occur.MUST);
		}

		Query<?> hibQuery = fullTextSession.createFullTextQuery(finalLuceneQuery.build(), PurchaseOrder.class);
		hibQuery.setFirstResult(0);
		hibQuery.setMaxResults(maxResult);
		List<PurchaseOrder> result = new ArrayList<>();
		try {
			result = (List<PurchaseOrder>) hibQuery.getResultList();
		} catch (POException e) {
			log.log(LogMessages.ERROR.value(), messages.getMessage(FAILED_FETCH_RECORDS_EXCEPTION), e);
		}		
		
		for(Object model : result) {
			System.out.println("Advance search result:::::::::::" + model.toString());
			System.out.println("Advance search size:::::::::::" + result.size());
		}
		
		tnx.commit();
		session.close();
		return result;
	}
	
 public List<?> searchArchiveDirectory(Map<String, String> SearchWord, Map<String, Double> SearchNumeral) {		
	Session session = HibernateUtility.getSessionFactory().withOptions().openSession();
	FullTextSession fullTextSession = Search.getFullTextSession(session);
	Transaction tnx = fullTextSession.beginTransaction();
		try {
			fullTextSession.createIndexer().startAndWait();
		} catch (POException | InterruptedException e) {
			log.log(LogMessages.ERROR.value(), messages.getMessage(FAILED_INTERRUPTED_EXCEPTION), e);
		}
		
		QueryBuilder qb = fullTextSession.getSearchFactory().buildQueryBuilder()
				.forEntity(PurchaseOrderArchiveDetails.class).get();
		
		BooleanQuery.Builder finalLuceneQuery = new BooleanQuery.Builder();
				
		if(SearchWord.get("poNo") != null){
		org.apache.lucene.search.Query querypono = qb.keyword().onField("poNo").matching(SearchWord.get("poNo"))
				.createQuery();
		finalLuceneQuery.add(querypono, BooleanClause.Occur.MUST);
		}

		if (SearchWord.get("companyCode") != null) {
			org.apache.lucene.search.Query queryStatus = qb.simpleQueryString().onField("compCode")
					.matching(SearchWord.get("companyCode")).createQuery();			
		finalLuceneQuery.add(queryStatus, BooleanClause.Occur.MUST);
		}
		
		if (SearchWord.get("pcCode") != null) {
			org.apache.lucene.search.Query queryStatus = qb.simpleQueryString().onField("pcCode")
					.matching(SearchWord.get("pcCode")).createQuery();			
		finalLuceneQuery.add(queryStatus, BooleanClause.Occur.MUST);
		}
		
		if (SearchWord.get("vendorCode") != null) {
			org.apache.lucene.search.Query queryStatus = qb.simpleQueryString().onField("vendorCode")
					.matching(SearchWord.get("vendorCode")).createQuery();			
		finalLuceneQuery.add(queryStatus, BooleanClause.Occur.MUST);
		}
		
		if (SearchWord.get("vendorName") != null) {
			org.apache.lucene.search.Query queryStatus = qb.simpleQueryString().onField("vendorName")
					.matching(SearchWord.get("vendorName")).createQuery();			
		finalLuceneQuery.add(queryStatus, BooleanClause.Occur.MUST);
		}
		
		if (SearchWord.get("issuer") != null) {
			org.apache.lucene.search.Query queryStatus = qb.simpleQueryString().onField("issuerPsid")
					.matching(SearchWord.get("issuer")).createQuery();			
		finalLuceneQuery.add(queryStatus, BooleanClause.Occur.MUST);
		}
		
		if (SearchWord.get("issuerName") != null) {
			org.apache.lucene.search.Query queryStatus = qb.simpleQueryString().onField("issuerName")
					.matching(SearchWord.get("issuerName")).createQuery();			
		finalLuceneQuery.add(queryStatus, BooleanClause.Occur.MUST);
		}
		
		if (SearchWord.get("archivedTable") != null) {
			org.apache.lucene.search.Query queryStatus = qb.simpleQueryString().onField("archivedTable")
					.matching(SearchWord.get("archivedTable")).createQuery();			
		finalLuceneQuery.add(queryStatus, BooleanClause.Occur.MUST);
		}
		
		if (SearchWord.get("archivedYear") != null) {
			org.apache.lucene.search.Query queryStatus = qb.simpleQueryString().onField("archivedYear")
					.matching(SearchWord.get("archivedYear")).createQuery();			
		finalLuceneQuery.add(queryStatus, BooleanClause.Occur.MUST);
		}
		
		if (SearchWord.get("totalAmt") != null) {
			org.apache.lucene.search.Query queryStatus = qb.simpleQueryString().onField("totalAmt")
					.matching(SearchWord.get("totalAmt")).createQuery();			
		finalLuceneQuery.add(queryStatus, BooleanClause.Occur.MUST);
		}
		
		if (SearchWord.get("createdOn") != null) {
			org.apache.lucene.search.Query queryStatus = qb.simpleQueryString().onField("createdOn")
					.matching(SearchWord.get("createdOn")).createQuery();			
		finalLuceneQuery.add(queryStatus, BooleanClause.Occur.MUST);
		}
				
		Query<?> hibQuery  = fullTextSession.createFullTextQuery(finalLuceneQuery.build(),
				PurchaseOrderArchiveDetails.class);		
		hibQuery.setFirstResult(0);
		List<?> resultArchive = new ArrayList<String>();
		try {
			resultArchive = hibQuery.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}

		for (Object model : resultArchive) {
			System.out.println("Archive Search::::::" + resultArchive.size());
			System.out.println("Archive PO = " + model.toString());
		}
		
		
		tnx.commit();
		session.close();
		return resultArchive;
	}
	
	
	
}