/*
 * ********************************************************************************
 * Copyright (c) 2018 - Brentin Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "eCommerce-core" is used for the core features and functionalities inside
 * eCommerce application.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : eCommerce-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Brentin Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * The Class ErrorController.
 */
@Controller
public class CustomErrorController implements ErrorController {
	private static final String VIEWPREFIX = "error/";

	private static final Logger LOGGER = Logger.getLogger(CustomErrorController.class);

	/**
	 * Access denied.
	 *
	 * @return the string
	 */
	@GetMapping("/403")
	public String accessDenied() {
		return VIEWPREFIX + "accessDenied";
	}

	/* @RequestMapping("/error") */
	@ResponseBody
	public ModelAndView handleError(HttpServletRequest request) {
		Integer status = (Integer) request.getAttribute("javax.servlet.error.status_code");
		Exception exception = (Exception) request.getAttribute("javax.servlet.error.exception");
		String message = (String) request.getAttribute("javax.servlet.error.message");
		String requestUri = (String) request.getAttribute("javax.servlet.error.request_uri");
		SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
		ModelAndView modelAndView = new ModelAndView("/error/error");
		modelAndView.addObject("timstamp", sdf.format(new Date()));
		modelAndView.addObject("status", status);
		modelAndView.addObject("uri", requestUri);
		modelAndView.addObject("message", message);
		try {
			modelAndView.addObject("exception", exception.getClass().getName());
		} catch (Exception e) {
			LOGGER.error(e);
		}
		modelAndView.addObject("path", getErrorPath());
		modelAndView.addObject("trace", exception != null ? exception.getStackTrace() : "N/A");
		return modelAndView;
	}

	@Override
	public String getErrorPath() {
		return "/error";
	}
}