/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import my.com.parkson.ntpoms.common.entities.Store;

/**
 * The Interface StoreRepository.
 */
public interface StoreRepository extends JpaRepository<Store, Integer>, JpaSpecificationExecutor<Store> {

	/**
	 * Find all by order by last modified on desc.
	 *
	 * @return the list
	 */
	public List<Store> findAllByOrderByLastModifiedOnDesc();

	/**
	 * Find by is active order by last modified on desc.
	 *
	 * @param isActive the is active
	 * @return the list
	 */
	public List<Store> findByIsActiveOrderByLastModifiedOnDesc(boolean isActive);
	
	/**
	 * Find all by order by last modified on desc.
	 *
	 * @return the list
	 */
	public Page<Store> findAllByOrderByLastModifiedOnDesc(Pageable pageRequest);

	/**
	 * Find all by order by last modified on desc.
	 *
	 * @return the list
	 */
	public Page<Store> findAllByOrderByLastModifiedOnDesc(Specification<Store> s, Pageable pageRequest);


	/**
	 * Find by code.
	 *
	 * @param code the code
	 * @return the store
	 */
	public Store findByCode(String code);

	/**
	 * Find all active by company code.
	 *
	 * @param code the code
	 * @return the list
	 */
	@Query("select o from Store o where o.isActive = true and o.compCode = ?1 order by o.lastModifiedOn desc")
	public List<Store> findAllActiveByCompanyCode(String code);
}