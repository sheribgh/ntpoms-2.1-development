package my.com.parkson.ntpoms.main.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import my.com.parkson.ntpoms.main.entities.Invoice;

public interface InvoiceService {
	
	public void createInvoice(Invoice invoice);
	public List<Invoice> getInvoiceByPurchaseOrderNo(String poNo);
	public Invoice getInvoiceByInvoiceNo(String invoiceNo);
	public List<Invoice> getAllInvoice();	
	public void removeInvoice(Invoice invoice);	
	public Invoice getInvoiceByPoInvoiceNo(String poInvoiceNo);
	
	public Page<Invoice> getAllPagableInvoice(Specification<Invoice> s, Pageable pageRequest);
	public List<Invoice> getInvoiceByPONo(String poNo);	
}