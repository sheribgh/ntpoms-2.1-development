/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */

package my.com.parkson.ntpoms.common.services.impl;

import static my.com.parkson.ntpoms.common.MessageCodes.FAILED_FETCH_RECORDS_EXCEPTION;
import static my.com.parkson.ntpoms.common.MessageCodes.FAILED_PERSIST_EXCEPTION;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.LogMessages;
import my.com.parkson.ntpoms.common.entities.ProfitCenter;
import my.com.parkson.ntpoms.common.repositories.ProfitCenterRepository;
import my.com.parkson.ntpoms.common.services.AccessLogService;
import my.com.parkson.ntpoms.common.services.MessageService;
import my.com.parkson.ntpoms.common.services.ProfitCenterService;

/**
 * The Class ProfitCenterServiceImpl.
 */
@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class ProfitCenterServiceImpl implements ProfitCenterService {

	private static final Logger LOGGER = Logger.getLogger(ProfitCenterServiceImpl.class);

	/** The messages. */
	@Autowired
	private MessageService messages;

	/** The profit center repository. */
	@Autowired
	private ProfitCenterRepository profitCenterRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.ProfitCenterService#
	 * getAllProfitCenter()
	 */
	@Override
	public List<ProfitCenter> getAllProfitCenter() {
		List<ProfitCenter> profitCenterList = new ArrayList<>();
		try {
			profitCenterList = profitCenterRepository.findAllByOrderByLastModifiedOnDesc();
		} catch (POException e) {
			LOGGER.error(e);
		}
		return profitCenterList;
	}

	@Override
	public long getCount() {
		return profitCenterRepository.count();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.ProfitCenterService#
	 * getAllActiveProfitCenter()
	 */
	@Override
	public List<ProfitCenter> getAllActiveProfitCenter() {
		List<ProfitCenter> profitCenterList = new ArrayList<>();
		try {
			profitCenterList = profitCenterRepository.findAllByIsActiveOrderByLastModifiedOnDesc(true);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return profitCenterList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.ProfitCenterService#
	 * getProfitCenterByCode(java.lang.String)
	 */
	@Override
	public ProfitCenter getProfitCenterByCode(String code) {
		try {
			return profitCenterRepository.findByCode(code);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.ProfitCenterService#
	 * createProfitCenter(my.com.parkson.ntpoms.common.entities.ProfitCenter)
	 */
	@Override
	public void createProfitCenter(ProfitCenter profitCenter) {
		try {
			profitCenterRepository.save(profitCenter);
		} catch (NonUniqueResultException e) {
			LOGGER.error(e);
		} catch (PersistenceException e) {
			LOGGER.error(e);
		} catch (POException e) {
			LOGGER.error(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.ProfitCenterService#
	 * getAllProfitCenterByCompanyCode(java.lang.String)
	 */
	@Override
	public List<ProfitCenter> getAllProfitCenterByCompanyCode(String companyCode, boolean isActive) {
		List<ProfitCenter> profitCenterList = new ArrayList<>();
		try {
			profitCenterList = profitCenterRepository
					.findAllByCompanyCodeAndIsactiveOrderByLastModifiedOnDesc(companyCode, true);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return profitCenterList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.ProfitCenterService#
	 * getAllPageableProfitCenter(org.springframework.data.domain.Pageable)
	 */
	@Override
	public Page<ProfitCenter> getAllPageableProfitCenter(Pageable pageRequest) {
		return profitCenterRepository.findAll(pageRequest);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.ProfitCenterService#
	 * getAllPageableProfitCenter(org.springframework.data.jpa.domain.
	 * Specification, org.springframework.data.domain.Pageable)
	 */
	@Override
	public Page<ProfitCenter> getAllPageableProfitCenter(Specification<ProfitCenter> s, Pageable pageRequest) {
		return profitCenterRepository.findAll(s, pageRequest);
	}

	@Override
	public Page<ProfitCenter> getAllPageableProfitCenterOrdered(Pageable pageRequest) {
		return profitCenterRepository.findAllByOrderByLastModifiedOnDesc(pageRequest);
	}

	@Override
	public Page<ProfitCenter> getAllPageableProfitCenterOrdered(Specification<ProfitCenter> s, Pageable pageRequest) {
		return profitCenterRepository.findAllByOrderByLastModifiedOnDesc(s, pageRequest);
	}

	@Override
	public List<String> getPCHeadsBasedOnUserLogin(String userid) {
		return profitCenterRepository.getPCHeadsBasedOnUserLogin(userid);
	}
}