package my.com.parkson.ntpoms.main.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "po05_t_invoice")
@Data
@NoArgsConstructor
public class Invoice implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "po05_poinvoiceno")
	private String poInvoiceNo;

	@Column(name = "po05_invoiceno")
	private String invoiceNo;

	@Column(name = "po05_date")
	private Date invoiceDate;

	@Column(name = "po05_recipient")
	private String recipient;

	@Column(name = "po05_amount")
	private double amount;

	
	@Column(name= "po01tmain_po05_pono")
	private String purchaseOrder;

	@Column(name = "po05_invoicenote")
	private String invoiceNote;

	public String getPoInvoiceNo() {
		return poInvoiceNo;
	}

	public void setPoInvoiceNo(String poInvoiceNo) {
		this.poInvoiceNo = poInvoiceNo;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	
	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getRecipient() {
		return recipient;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	
	public String getPurchaseOrder() {
		return purchaseOrder;
	}

	public void setPurchaseOrder(String purchaseOrder) {
		this.purchaseOrder = purchaseOrder;
	}

	public String getInvoiceNote() {
		return invoiceNote;
	}

	public void setInvoiceNote(String invoiceNote) {
		this.invoiceNote = invoiceNote;
	}
}