/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services.impl;

import static my.com.parkson.ntpoms.common.MessageCodes.FAILED_PERSIST_EXCEPTION;
import static my.com.parkson.ntpoms.common.MessageCodes.FAILED_FETCH_RECORDS_EXCEPTION;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.LogMessages;
import my.com.parkson.ntpoms.common.entities.AccessLog;
import my.com.parkson.ntpoms.common.entities.Company;
import my.com.parkson.ntpoms.common.repositories.AccessLogRepository;
import my.com.parkson.ntpoms.common.services.AccessLogService;

/**
 * The Class AccessLogServiceImpl.
 */
@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class AccessLogServiceImpl implements AccessLogService {
	
	
	

	/** The access log repository. */
	@Autowired
	private AccessLogRepository accessLogRepository;

	/* (non-Javadoc)
	 * @see my.com.parkson.ntpoms.common.services.AccessLogService#getAllAccessLog()
	 */
	@Override
	public List<AccessLog> getAllAccessLog() {
		try {
			return accessLogRepository.findAll();
		} catch (POException e) {
			log(LogMessages.ERROR.value(), FAILED_FETCH_RECORDS_EXCEPTION, e);
		}
		return null;
	}
	
	
	@Override
	public long getCount() {
		return accessLogRepository.count();
	}

	/* (non-Javadoc)
	 * @see my.com.parkson.ntpoms.common.services.AccessLogService#createAccessLog(my.com.parkson.ntpoms.common.entities.AccessLog)
	 */
	@Override
	public void createAccessLog(AccessLog accessLog) {
		try {
			accessLogRepository.save(accessLog);
		} catch (POException e) {
			log(LogMessages.ERROR.value(), FAILED_PERSIST_EXCEPTION, e);
		}
	}

	/* (non-Javadoc)
	 * @see my.com.parkson.ntpoms.common.services.AccessLogService#log(java.lang.String, java.lang.String, java.lang.Exception)
	 */
	@Override
	public void log(String logLevel, String logMessage, Exception exception) {
		StackTraceElement caller = exception != null ? exception.getStackTrace()[1]
				: new Exception().getStackTrace()[1];
		String classFileName = caller.getFileName();
		int lineNo = caller.getLineNumber();
		Date logTime = new Date();
		String screenId = getScreenId(caller);
		AccessLog newLog = new AccessLog();
		newLog.setClassFileName(classFileName);
		newLog.setScreenId(screenId);
		newLog.setLineNo(lineNo);
		newLog.setLevel(logLevel.toUpperCase());
		newLog.setMessage(logMessage);
		newLog.setTime(logTime);
		createAccessLog(newLog);
	}
	
	
	@Override
	public void log(String logLevel, String logMessage, Exception exception, int lineNo, String className) {
		StackTraceElement caller = exception != null ? exception.getStackTrace()[1]
				: new Exception().getStackTrace()[1];	
		Date logTime = new Date();
		String screenId = getScreenId(caller);
		AccessLog newLog = new AccessLog();
		newLog.setClassFileName(className);
		newLog.setScreenId(screenId);
		newLog.setLineNo(lineNo);
		newLog.setLevel(logLevel.toUpperCase());
		newLog.setMessage(logMessage);
		newLog.setTime(logTime);
		createAccessLog(newLog);
	}

	/* (non-Javadoc)
	 * @see my.com.parkson.ntpoms.common.services.AccessLogService#log(java.lang.String, java.lang.String, java.lang.String, java.lang.Exception)
	 */
	@Override
	public void log(String logDbName, String logLevel, String logMessage, Exception exception) {
		StackTraceElement caller = new Exception().getStackTrace()[1];
		String classFileName = caller.getFileName();
		int lineNo = caller.getLineNumber();
		Date logTime = new Date();
		String screenId = getScreenId(caller);
		AccessLog newLog = new AccessLog();
		newLog.setClassFileName(classFileName);
		newLog.setScreenId(screenId);
		newLog.setLineNo(lineNo);
		newLog.setLevel(logLevel.toUpperCase());
		newLog.setMessage(logMessage);
		newLog.setTime(logTime);
		createAccessLog(newLog);
	}

	/**
	 * Gets the screen id.
	 *
	 * @param caller the caller
	 * @return the screen id
	 */
	private String getScreenId(StackTraceElement caller) {
		String screenId = null;
		if (caller.getClassName().contains("auditTrail"))
			screenId = "PO-UT01L";
		else if (caller.getClassName().contains("Company"))
			screenId = "PO-AD11L";
		else if (caller.getClassName().contains("Department"))
			screenId = "PO-AD14L";
		else if (caller.getClassName().contains("Employee"))
			screenId = "PO-AD12L";
		else if (caller.getClassName().contains("GST"))
			screenId = "PO-AD16L";
		else if (caller.getClassName().contains("JobMaster"))
			screenId = "PO-AD19L";
		else if (caller.getClassName().contains("Log"))
			screenId = "PO-UT02L";
		else if (caller.getClassName().contains("LogSetting"))
			screenId = "PO-AD11L";
		else if (caller.getClassName().contains("MasterData"))
			screenId = "PO-AD06L";
		else if (caller.getClassName().contains("OrderCategory"))
			screenId = "PO-AD17L";
		else if (caller.getClassName().contains("OrderSubCat"))
			screenId = "PO-AD18L";
		else if (caller.getClassName().contains("ProfitCenter"))
			screenId = "PO-AD13L";
		else if (caller.getClassName().contains("PurchaseRequisition"))
			screenId = "PO-AD15L";
		else if (caller.getClassName().contains("Store"))
			screenId = "PO-AD02L";
		else if (caller.getClassName().contains("SystemParameters"))
			screenId = "PO-AD07L";
		else if (caller.getClassName().contains("Login"))
			screenId = "PO-LI01S";
		else if (caller.getClassName().contains("MainScreen"))
			screenId = "PO-HM01S";
		else if (caller.getClassName().contains("AdminView"))
			screenId = "PO-SA20L";
		else if (caller.getClassName().contains("mail"))
			screenId = "Mail Service";
		else if (caller.getClassName().contains("velocity"))
			screenId = "Mail Service";
		else
			screenId = caller.getClassName();

		return screenId;
	}

	/* (non-Javadoc)
	 * @see my.com.parkson.ntpoms.common.services.AccessLogService#getAllPageableAccessLog(org.springframework.data.domain.Pageable)
	 */
	@Override
	public Page<AccessLog> getAllPageableAccessLog(Pageable pageRequest) {
		return accessLogRepository.findAll(pageRequest);
	}

	/* (non-Javadoc)
	 * @see my.com.parkson.ntpoms.common.services.AccessLogService#getAllPageableAccessLog(org.springframework.data.jpa.domain.Specification, org.springframework.data.domain.Pageable)
	 */
	@Override
	public Page<AccessLog> getAllPageableAccessLog(Specification<AccessLog> s, Pageable pageRequest) {
		return accessLogRepository.findAll(s, pageRequest);
	}
	
	/* (non-Javadoc)
	 * @see my.com.parkson.ntpoms.common.services.CompanyService#getAllPageableCompany(org.springframework.data.jpa.domain.Specification, org.springframework.data.domain.Pageable)
	 */
	@Override
	public Page<AccessLog> getAllPageableLogOrdered(Pageable pageRequest) {
		
		return accessLogRepository.findAllByOrderByIdDesc(pageRequest);
	}
	
	
}