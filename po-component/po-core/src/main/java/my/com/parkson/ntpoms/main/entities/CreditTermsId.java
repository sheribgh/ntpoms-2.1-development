package my.com.parkson.ntpoms.main.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Data;

@Data
@Embeddable
public class CreditTermsId implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	@Column(name = "\"mf02_credittermscode\"")
	private String code;

	@Column(name = "\"mf11mcompmast_mf02_compcode\"")
	private String companycode;
	
	public CreditTermsId(){
		
	}
	
		public CreditTermsId(String code, String companyCode){
			
			this.code = code;
			this.companycode = companyCode;
		
	}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public String getCompanycode() {
			return companycode;
		}

		public void setCompanycode(String companycode) {
			this.companycode = companycode;
		}
		
		

}
