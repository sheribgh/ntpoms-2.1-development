package my.com.parkson.ntpoms.main.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import my.com.parkson.ntpoms.main.entities.PurchaseOrderArchiveDetails;

public interface PurchaseOrderArchiveService {

	public Page<PurchaseOrderArchiveDetails> getAllPageablePurchaseOrder(Specification<PurchaseOrderArchiveDetails> s,Pageable pageRequest);
	public Page<PurchaseOrderArchiveDetails> getAllPageablePurchaseOrder(Pageable pageRequest);
}
