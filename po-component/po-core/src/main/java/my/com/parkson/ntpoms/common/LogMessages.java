/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum LogMessages.
 */
public enum LogMessages {

	/** The error. */
	ERROR("ERROR"),
	/** The debug. */
	DEBUG("DEBUG"),
	/** The warning. */
	WARNING("WARNING"),
	/** The failure. */
	FAILURE("FAILURE"),
	/** The critical. */
	CRITICAL("CRITICAL"),

	/** The success. */
	SUCCESS("SUCCESS"),
	/** The service failure. */
	SERVICE_FAILURE("SERVICE_FAILURE");

	/** The message. */
	private String message;

	/**
	 * Instantiates a new log messages.
	 *
	 * @param message
	 *            the message
	 */
	private LogMessages(String message) {
		this.message = message;
	}

	/**
	 * To status.
	 *
	 * @param strValue
	 *            the str value
	 * @return the log messages
	 */
	public static LogMessages toStatus(String strValue) {
		LogMessages[] values = LogMessages.values();
		for (LogMessages c : values) {
			if (c.value().equals(strValue)) {
				return c;
			}
		}
		return null;
	}

	/**
	 * Value.
	 *
	 * @return the string
	 */
	public String value() {
		return message;
	}

	/**
	 * Options.
	 *
	 * @return the map
	 */
	public static Map<String, String> options() {
		Map<String, String> options = new HashMap<String, String>();
		for (LogMessages g : LogMessages.values()) {
			options.put(g.name(), g.value());
		}
		return options;
	}
}