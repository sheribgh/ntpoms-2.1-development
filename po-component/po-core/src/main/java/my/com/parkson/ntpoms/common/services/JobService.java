/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import my.com.parkson.ntpoms.common.entities.Job;

/**
 * The Interface JobService.
 */
public interface JobService {

	/**
	 * Gets the all job.
	 *
	 * @return the all job
	 */
	public List<Job> getAllJob();
	
	
	
	
	public long getCount();

	/**
	 * Gets the all pagable job.
	 *
	 * @param pageRequest the page request
	 * @return the all pagable job
	 */
	public Page<Job> getAllPagableJob(Pageable pageRequest);

	/**
	 * Gets the all pagable job.
	 *
	 * @param s the s
	 * @param pageRequest the page request
	 * @return the all pagable job
	 */
	public Page<Job> getAllPagableJob(Specification<Job> s, Pageable pageRequest);

	
	/**
	 * Gets the all pagable job.
	 *
	 * @param pageRequest the page request
	 * @return the all pagable job
	 */
	public Page<Job> getAllPagableJobOrdered(Pageable pageRequest);

	/**
	 * Gets the all pagable job.
	 *
	 * @param s the s
	 * @param pageRequest the page request
	 * @return the all pagable job
	 */
	public Page<Job> getAllPagableJobOrdered(Specification<Job> s, Pageable pageRequest);

	/**
	 * Gets the all active job.
	 *
	 * @return the all active job
	 */
	public List<Job> getAllActiveJob();

	/**
	 * Gets the job by code.
	 *
	 * @param code the code
	 * @return the job by code
	 */
	public Job getJobByCode(String code);

	/**
	 * Creates the job.
	 *
	 * @param job the job
	 */
	public void createJob(Job job);
}