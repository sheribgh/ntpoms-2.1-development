package my.com.parkson.ntpoms.main.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.DateBridge;
import org.hibernate.search.annotations.DocumentId;
import org.hibernate.search.annotations.EncodingType;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.NumericField;
import org.hibernate.search.annotations.Resolution;
import org.hibernate.search.annotations.Store;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "po01_t_main")
@Data
@NoArgsConstructor
public class PurchaseOrder implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "po01_pono")
	private String poNo;

	
	@Column(name = "po01_status")
	private String status;

	@Column(name = "po01_quoteno")
	private String quoteNo;

	
	@DateBridge(resolution = Resolution.DAY, encoding=EncodingType.STRING)
	@Column(name = "po01_quotedate")
	private Date quoteDate;

	
	@Column(name = "mf13mpcmast_po01_pccode")
	private String pcCode; 

	@Column(name = "po01_contactperson")
	private String contactPerson;

	@Column(name = "po01_contactpersonname")
	private String contactPersonName; 

	@Column(name = "po01_contactpersonphone")
	private String contactPersonPhone;

	@Column(name = "po01_contactpersonemail")
	private String contactPersonEmail;

	@Column(name = "po01_addressline1")
	private String addressLine1;

	@Column(name = "po01_addressline2")
	private String addressLine2;

	@Column(name = "po01_addressline3")
	private String addressLine3;

	@Column(name = "po01_postcode")
	private String postCode;

	@Column(name = "po01_postarea")
	private String postArea;

	@Column(name = "mf11mcompmast_po01_compcode")
	private String compCode;

	@Column(name = "po01_termsandconditionsportrait")
	private String termsAndConditionsPortrait;

	@Column(name = "po01_termsandconditionslandscape")
	private String termsAndConditionsLandscape;

	@Column(name = "po01_storename")
	private String storeName;

	@Column(name = "po01_departmentname")
	private String departmentName;

	
	@Column(name = "mf01mvendorbranch_po01_vendorcode")
	private String vendorcode; 

	
	@Column(name = "mf01mvendorbranch_po01_vendorbranchcode")
	private String vendorbranchcode; 

	
	@Column(name = "po01_vendorname")
	private String vendorName; 

	@Column(name = "po01_vendorpersonincharge")
	private String vendorPersonInCharge;

	@Column(name = "po01_vendorpersoninchargephone")
	private String vendorPersonInChargePhone;

	@Column(name = "po01_vendorpersoninchargeemail")
	private String vendorPersonInChargeEmail;

	@Column(name = "po01_vendoraddressline1")
	private String vendorAddressLine1;

	@Column(name = "po01_vendoraddressline2")
	private String vendorAddressLine2;

	@Column(name = "po01_vendoraddressline3")
	private String vendorAddressLine3;

	@Column(name = "po01_vendorpostcode")
	private String vendorPostCode;

	@Column(name = "po01_vendorpostarea")
	private String vendorPostArea;

	@Column(name = "mf02mcreditterms_po01_credittermscode")
	private String creditTermsCode;

	@Column(name = "po01_currency")
	private String currency;

	
	@Column(name = "mf17mordcatmast_po01_ordcatcode")
	private String orderCatCode; 

	@Column(name = "po01_ordercategoryauthorizationgrid")
	private String orderCategoryAuthorizationGrid;

	
	@Column(name = "mf18mordsubcmast_po01_ordsubccode")
	private String ordSubCatCode; 

	
	@Column(name = "mf15mprmast_po01_prtypecode")
	private String prTypeCode;

	@Column(name = "po01_paymentterms")
	private String paymentTerms;

	@Column(name = "po01_deliveryinstruction")
	private String deliveryInstruction;

	
	@NumericField
	@Column(name = "po01_totinclgst")
	private double totalInclGST; 

	@Column(name = "po01_totexclgst")
	private double totalExclGST;

	@Column(name = "po01_totgst")
	private double totalGST;
	
	@Column(name = "po01_overalldiscount_percentage")
	private double overalldiscountpercent;

	
	@Column(name = "mf12mempmast_po01_issuer")
	private String issuer;

	
	@DateBridge(resolution = Resolution.DAY, encoding=EncodingType.STRING)
	@Column(name = "po01_issuedon")
	private Date issuedOn; 

	
	@Column(name = "mf12mempmast_po01_verifier")
	private String verifier; 

	@Column(name = "po01_verifiedon")
	private Date verifiedOn;

	@Column(name = "po01_verifyremark")
	private String verifyRemark;

	
	@Column(name = "mf12mempmast_po01_authorizer")
	private String authorizer; 

	@Column(name = "po01_authorizedon")
	private Date authorizedOn;

	@Column(name = "po01_authorizeremark")
	private String authorizeRemark;

	@Column(name = "po01_lastprintedby")
	private String lastPrintedBy;

	@Column(name = "po01_lastprintedon")
	private Date lastPrintedOn;

	@Column(name = "po01_closeexception")
	private String closeException;

	@Column(name = "po01_redraftremark")
	private String redraftRemark;

	@Column(name = "po01_rejectremark")
	private String rejectRemark;

	@Column(name = "po01_deleteremark")
	private String deleteRemark;

	@Column(name = "po01_cancelremark")
	private String cancelRemark;

	@Column(name = "po01_firstapprover")
	private String firstApprover;

	@Column(name = "po01_firstapproverdesignation")
	private String firstApproverDesignation;

	@Column(name = "po01_secondapprover")
	private String secondApprover;

	@Column(name = "po01_secondapproverdesignation")
	private String secondApproverDesignation;

	@Column(name = "po01_thirdapprover")
	private String thirdApprover;

	@Column(name = "po01_thirdapproverdesignation")
	private String thirdApproverDesignation;

	@Column(name = "po01_expiredate")
	private Date expireDate;

	@Column(name = "po01_expirystatus")
	private boolean expiryStatus;

	@Column(name = "po01_expirydays")
	private int expiryDays;

	@Column(name = "po01_overalldiscount")
	private double overAllDiscount;

	@Column(name = "po03tproject_po01_projectcode")
	private String projectCode;

	@Column(name = "po01_issuerremark")
	private String issuerRemark;

	
	@Column(name = "po01_deletedby")
	private String deletedBy;

	@Column(name = "po01_deletedon")
	private Date deletedOn;

	
	@Column(name = "po01_rejectedby")
	private String rejectedBy;

	@Column(name = "po01_rejectedon")
	private Date rejectedOn;

	
	@Column(name = "po01_closedby")
	private String closedBy;

	@Column(name = "po01_closedon")
	private Date closedOn;

	
	@Column(name = "po01_cancelledby")
	private String cancelledBy;

	@Column(name = "po01_cancelledon")
	private Date cancelledOn;

	@Column(name = "po01_forwardingstatus")
	private String forwardingstatus;
	
	@Column(name = "po01_lastmodifiedon")
	private Date lastModifiedOn;
	
	

	public Date getLastModifiedOn() {
		return lastModifiedOn;
	}

	public void setLastModifiedOn(Date lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

	public String getPoNo() {
		return poNo;
	}

	public void setPoNo(String poNo) {
		this.poNo = poNo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getQuoteNo() {
		return quoteNo;
	}

	public void setQuoteNo(String quoteNo) {
		this.quoteNo = quoteNo;
	}

	public Date getQuoteDate() {
		return quoteDate;
	}

	public void setQuoteDate(Date quoteDate) {
		this.quoteDate = quoteDate;
	}

	public String getPcCode() {
		return pcCode;
	}

	public void setPcCode(String pcCode) {
		this.pcCode = pcCode;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getContactPersonName() {
		return contactPersonName;
	}

	public void setContactPersonName(String contactPersonName) {
		this.contactPersonName = contactPersonName;
	}

	public String getContactPersonPhone() {
		return contactPersonPhone;
	}

	public void setContactPersonPhone(String contactPersonPhone) {
		this.contactPersonPhone = contactPersonPhone;
	}

	public String getContactPersonEmail() {
		return contactPersonEmail;
	}

	public void setContactPersonEmail(String contactPersonEmail) {
		this.contactPersonEmail = contactPersonEmail;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getAddressLine3() {
		return addressLine3;
	}

	public void setAddressLine3(String addressLine3) {
		this.addressLine3 = addressLine3;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	public String getPostArea() {
		return postArea;
	}

	public void setPostArea(String postArea) {
		this.postArea = postArea;
	}

	public String getCompCode() {
		return compCode;
	}

	public void setCompCode(String compCode) {
		this.compCode = compCode;
	}

	public String getTermsAndConditionsPortrait() {
		return termsAndConditionsPortrait;
	}

	public void setTermsAndConditionsPortrait(String termsAndConditionsPortrait) {
		this.termsAndConditionsPortrait = termsAndConditionsPortrait;
	}

	public String getTermsAndConditionsLandscape() {
		return termsAndConditionsLandscape;
	}

	public void setTermsAndConditionsLandscape(String termsAndConditionsLandscape) {
		this.termsAndConditionsLandscape = termsAndConditionsLandscape;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public String getVendorcode() {
		return vendorcode;
	}

	public void setVendorcode(String vendorcode) {
		this.vendorcode = vendorcode;
	}

	public String getVendorbranchcode() {
		return vendorbranchcode;
	}

	public void setVendorbranchcode(String vendorbranchcode) {
		this.vendorbranchcode = vendorbranchcode;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public String getVendorPersonInCharge() {
		return vendorPersonInCharge;
	}

	public void setVendorPersonInCharge(String vendorPersonInCharge) {
		this.vendorPersonInCharge = vendorPersonInCharge;
	}

	public String getVendorPersonInChargePhone() {
		return vendorPersonInChargePhone;
	}

	public void setVendorPersonInChargePhone(String vendorPersonInChargePhone) {
		this.vendorPersonInChargePhone = vendorPersonInChargePhone;
	}

	public String getVendorPersonInChargeEmail() {
		return vendorPersonInChargeEmail;
	}

	public void setVendorPersonInChargeEmail(String vendorPersonInChargeEmail) {
		this.vendorPersonInChargeEmail = vendorPersonInChargeEmail;
	}

	public String getVendorAddressLine1() {
		return vendorAddressLine1;
	}

	public void setVendorAddressLine1(String vendorAddressLine1) {
		this.vendorAddressLine1 = vendorAddressLine1;
	}

	public String getVendorAddressLine2() {
		return vendorAddressLine2;
	}

	public void setVendorAddressLine2(String vendorAddressLine2) {
		this.vendorAddressLine2 = vendorAddressLine2;
	}

	public String getVendorAddressLine3() {
		return vendorAddressLine3;
	}

	public void setVendorAddressLine3(String vendorAddressLine3) {
		this.vendorAddressLine3 = vendorAddressLine3;
	}

	public String getVendorPostCode() {
		return vendorPostCode;
	}

	public void setVendorPostCode(String vendorPostCode) {
		this.vendorPostCode = vendorPostCode;
	}

	public String getVendorPostArea() {
		return vendorPostArea;
	}

	public void setVendorPostArea(String vendorPostArea) {
		this.vendorPostArea = vendorPostArea;
	}

	public String getCreditTermsCode() {
		return creditTermsCode;
	}

	public void setCreditTermsCode(String creditTermsCode) {
		this.creditTermsCode = creditTermsCode;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getOrderCatCode() {
		return orderCatCode;
	}

	public void setOrderCatCode(String orderCatCode) {
		this.orderCatCode = orderCatCode;
	}

	public String getOrderCategoryAuthorizationGrid() {
		return orderCategoryAuthorizationGrid;
	}

	public void setOrderCategoryAuthorizationGrid(String orderCategoryAuthorizationGrid) {
		this.orderCategoryAuthorizationGrid = orderCategoryAuthorizationGrid;
	}

	public String getOrdSubCatCode() {
		return ordSubCatCode;
	}

	public void setOrdSubCatCode(String ordSubCatCode) {
		this.ordSubCatCode = ordSubCatCode;
	}

	public String getPrTypeCode() {
		return prTypeCode;
	}

	public void setPrTypeCode(String prTypeCode) {
		this.prTypeCode = prTypeCode;
	}

	public String getPaymentTerms() {
		return paymentTerms;
	}

	public void setPaymentTerms(String paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	public String getDeliveryInstruction() {
		return deliveryInstruction;
	}

	public void setDeliveryInstruction(String deliveryInstruction) {
		this.deliveryInstruction = deliveryInstruction;
	}

	public double getTotalInclGST() {
		return totalInclGST;
	}

	public void setTotalInclGST(double totalInclGST) {
		this.totalInclGST = totalInclGST;
	}

	public double getTotalExclGST() {
		return totalExclGST;
	}

	public void setTotalExclGST(double totalExclGST) {
		this.totalExclGST = totalExclGST;
	}

	public double getTotalGST() {
		return totalGST;
	}

	public void setTotalGST(double totalGST) {
		this.totalGST = totalGST;
	}

	public String getIssuer() {
		return issuer;
	}

	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}

	public Date getIssuedOn() {
		return issuedOn;
	}

	public void setIssuedOn(Date issuedOn) {
		this.issuedOn = issuedOn;
	}

	public String getVerifier() {
		return verifier;
	}

	public void setVerifier(String verifier) {
		this.verifier = verifier;
	}

	public Date getVerifiedOn() {
		return verifiedOn;
	}

	public void setVerifiedOn(Date verifiedOn) {
		this.verifiedOn = verifiedOn;
	}

	public String getVerifyRemark() {
		return verifyRemark;
	}

	public void setVerifyRemark(String verifyRemark) {
		this.verifyRemark = verifyRemark;
	}

	public String getAuthorizer() {
		return authorizer;
	}

	public void setAuthorizer(String authorizer) {
		this.authorizer = authorizer;
	}

	public Date getAuthorizedOn() {
		return authorizedOn;
	}

	public void setAuthorizedOn(Date authorizedOn) {
		this.authorizedOn = authorizedOn;
	}

	public String getAuthorizeRemark() {
		return authorizeRemark;
	}

	public void setAuthorizeRemark(String authorizeRemark) {
		this.authorizeRemark = authorizeRemark;
	}

	public String getLastPrintedBy() {
		return lastPrintedBy;
	}

	public void setLastPrintedBy(String lastPrintedBy) {
		this.lastPrintedBy = lastPrintedBy;
	}

	public Date getLastPrintedOn() {
		return lastPrintedOn;
	}

	public void setLastPrintedOn(Date lastPrintedOn) {
		this.lastPrintedOn = lastPrintedOn;
	}

	public String getCloseException() {
		return closeException;
	}

	public void setCloseException(String closeException) {
		this.closeException = closeException;
	}

	public String getRedraftRemark() {
		return redraftRemark;
	}

	public void setRedraftRemark(String redraftRemark) {
		this.redraftRemark = redraftRemark;
	}

	public String getRejectRemark() {
		return rejectRemark;
	}

	public void setRejectRemark(String rejectRemark) {
		this.rejectRemark = rejectRemark;
	}

	public String getDeleteRemark() {
		return deleteRemark;
	}

	public void setDeleteRemark(String deleteRemark) {
		this.deleteRemark = deleteRemark;
	}

	public String getCancelRemark() {
		return cancelRemark;
	}

	public void setCancelRemark(String cancelRemark) {
		this.cancelRemark = cancelRemark;
	}

	public String getFirstApprover() {
		return firstApprover;
	}

	public void setFirstApprover(String firstApprover) {
		this.firstApprover = firstApprover;
	}

	public String getFirstApproverDesignation() {
		return firstApproverDesignation;
	}

	public void setFirstApproverDesignation(String firstApproverDesignation) {
		this.firstApproverDesignation = firstApproverDesignation;
	}

	public String getSecondApprover() {
		return secondApprover;
	}

	public void setSecondApprover(String secondApprover) {
		this.secondApprover = secondApprover;
	}

	public String getSecondApproverDesignation() {
		return secondApproverDesignation;
	}

	public void setSecondApproverDesignation(String secondApproverDesignation) {
		this.secondApproverDesignation = secondApproverDesignation;
	}

	public String getThirdApprover() {
		return thirdApprover;
	}

	public void setThirdApprover(String thirdApprover) {
		this.thirdApprover = thirdApprover;
	}

	public String getThirdApproverDesignation() {
		return thirdApproverDesignation;
	}

	public void setThirdApproverDesignation(String thirdApproverDesignation) {
		this.thirdApproverDesignation = thirdApproverDesignation;
	}

	public Date getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}

	public boolean isExpiryStatus() {
		return expiryStatus;
	}

	public void setExpiryStatus(boolean expiryStatus) {
		this.expiryStatus = expiryStatus;
	}

	public int getExpiryDays() {
		return expiryDays;
	}

	public void setExpiryDays(int expiryDays) {
		this.expiryDays = expiryDays;
	}

	public double getOverAllDiscount() {
		return overAllDiscount;
	}

	public void setOverAllDiscount(double overAllDiscount) {
		this.overAllDiscount = overAllDiscount;
	}

	public String getProjectCode() {
		return projectCode;
	}

	public void setProjectCode(String projectCode) {
		this.projectCode = projectCode;
	}

	public String getIssuerRemark() {
		return issuerRemark;
	}

	public void setIssuerRemark(String issuerRemark) {
		this.issuerRemark = issuerRemark;
	}

	public String getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(String deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public String getRejectedBy() {
		return rejectedBy;
	}

	public void setRejectedBy(String rejectedBy) {
		this.rejectedBy = rejectedBy;
	}

	public Date getRejectedOn() {
		return rejectedOn;
	}

	public void setRejectedOn(Date rejectedOn) {
		this.rejectedOn = rejectedOn;
	}

	public String getClosedBy() {
		return closedBy;
	}

	public void setClosedBy(String closedBy) {
		this.closedBy = closedBy;
	}

	public Date getClosedOn() {
		return closedOn;
	}

	public void setClosedOn(Date closedOn) {
		this.closedOn = closedOn;
	}

	public String getCancelledBy() {
		return cancelledBy;
	}

	public void setCancelledBy(String cancelledBy) {
		this.cancelledBy = cancelledBy;
	}

	public Date getCancelledOn() {
		return cancelledOn;
	}

	public void setCancelledOn(Date cancelledOn) {
		this.cancelledOn = cancelledOn;
	}

	public String getForwardingstatus() {
		return forwardingstatus;
	}

	public void setForwardingstatus(String forwardingstatus) {
		this.forwardingstatus = forwardingstatus;
	}

	public double getOveralldiscountpercent() {
		return overalldiscountpercent;
	}

	public void setOveralldiscountpercent(double overalldiscountpercent) {
		this.overalldiscountpercent = overalldiscountpercent;
	}	
	
}