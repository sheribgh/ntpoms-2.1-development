package my.com.parkson.ntpoms.main.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ItemsTotal implements Serializable {
	
	private static final long serialVersionUID = 1L;

	
	private Double totalexclgst;
	
	private Double totalinclgst;
	
	private Double totalgst;
	
	private Double totaldiscount;
	
	

	public Double getTotalexclgst() {
		return totalexclgst;
	}

	public void setTotalexclgst(Double totalexclgst) {
		this.totalexclgst = totalexclgst;
	}

	public Double getTotalinclgst() {
		return totalinclgst;
	}

	public void setTotalinclgst(Double totalinclgst) {
		this.totalinclgst = totalinclgst;
	}

	public Double getTotalgst() {
		return totalgst;
	}

	public void setTotalgst(Double totalgst) {
		this.totalgst = totalgst;
	}

	public Double getTotaldiscount() {
		return totaldiscount;
	}

	public void setTotaldiscount(Double totaldiscount) {
		this.totaldiscount = totaldiscount;
	}

}