/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import my.com.parkson.ntpoms.common.entities.ProfitCenter;

/**
 * The Interface ProfitCenterService.
 */
public interface ProfitCenterService {

	/**
	 * Gets the all profit center.
	 *
	 * @return the all profit center
	 */
	public List<ProfitCenter> getAllProfitCenter();
	
	
	public List<String> getPCHeadsBasedOnUserLogin(String userid);
	
	public long getCount();

	/**
	 * Gets the all pageable profit center.
	 *
	 * @param pageRequest the page request
	 * @return the all pageable profit center
	 */
	public Page<ProfitCenter> getAllPageableProfitCenter(Pageable pageRequest);

	/**
	 * Gets the all pageable profit center.
	 *
	 * @param s the s
	 * @param pageRequest the page request
	 * @return the all pageable profit center
	 */
	public Page<ProfitCenter> getAllPageableProfitCenter(Specification<ProfitCenter> s, Pageable pageRequest);
	
	/**
	 * Gets the all pageable profit center.
	 *
	 * @param pageRequest the page request
	 * @return the all pageable profit center
	 */
	public Page<ProfitCenter> getAllPageableProfitCenterOrdered(Pageable pageRequest);

	/**
	 * Gets the all pageable profit center.
	 *
	 * @param s the s
	 * @param pageRequest the page request
	 * @return the all pageable profit center
	 */
	public Page<ProfitCenter> getAllPageableProfitCenterOrdered(Specification<ProfitCenter> s, Pageable pageRequest);

	/**
	 * Gets the all active profit center.
	 *
	 * @return the all active profit center
	 */
	public List<ProfitCenter> getAllActiveProfitCenter();

	/**
	 * Gets the profit center by code.
	 *
	 * @param code the code
	 * @return the profit center by code
	 */
	public ProfitCenter getProfitCenterByCode(String code);

	/**
	 * Creates the profit center.
	 *
	 * @param profitCenter the profit center
	 */
	public void createProfitCenter(ProfitCenter profitCenter);

	/**
	 * Gets the all profit center by company code.
	 *
	 * @param code the code
	 * @return the all profit center by company code
	 */
	public List<ProfitCenter> getAllProfitCenterByCompanyCode(String companyCode,boolean isActive);
}