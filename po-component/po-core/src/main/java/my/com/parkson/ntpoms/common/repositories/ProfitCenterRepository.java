/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import my.com.parkson.ntpoms.common.entities.ProfitCenter;

/**
 * The Interface ProfitCenterRepository.
 */
public interface ProfitCenterRepository
		extends JpaRepository<ProfitCenter, Integer>, JpaSpecificationExecutor<ProfitCenter> {

	/**
	 * Find all by order by last modified on desc.
	 *
	 * @return the list
	 */
	List<ProfitCenter> findAllByOrderByLastModifiedOnDesc();

	/**
	 * Find all by is active order by last modified on desc.
	 *
	 * @param isActive
	 *            the is active
	 * @return the list
	 */
	@Query("select o from ProfitCenter o where o.isactive=:at order by o.lastModifiedOn desc")
	List<ProfitCenter> findAllByIsActiveOrderByLastModifiedOnDesc(@Param("at") boolean isActive);

	/**
	 * Find by code.
	 *
	 * @param code
	 *            the code
	 * @return the profit center
	 */
	public ProfitCenter findByCode(String code);

	/**
	 * Find all by company.
	 *
	 * @param code
	 *            the code
	 * @return the list
	 */
	@Query("select o from ProfitCenter o where o.company.code = ?1 order by o.lastModifiedOn desc")
	public List<ProfitCenter> findAllByCompany(String code);
	
	public List<ProfitCenter> findAllByCompanyCodeAndIsactiveOrderByLastModifiedOnDesc(String code,boolean isActive);

	/**
	 * Find all by order by last modified on desc.
	 *
	 * @return the list
	 */
	public Page<ProfitCenter> findAllByOrderByLastModifiedOnDesc(Pageable pageRequest);

	@Query("select o.code from ProfitCenter o where o.head LIKE :userid")
	public List<String> getPCHeadsBasedOnUserLogin(@Param("userid") String userid);

	/**
	 * Find all by order by last modified on desc.
	 *
	 * @return the list
	 */
	public Page<ProfitCenter> findAllByOrderByLastModifiedOnDesc(Specification<ProfitCenter> s, Pageable pageRequest);

}