/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.security;

import org.springframework.security.web.session.HttpSessionEventPublisher;

import javax.servlet.http.HttpSessionEvent;

/**
 * The Class CustomHttpSessionEventPublisher.
 */
public class CustomHttpSessionEventPublisher extends HttpSessionEventPublisher {

    /** The session timeout. */
    private Integer sessionTimeout;

    /* (non-Javadoc)
     * @see org.springframework.security.web.session.HttpSessionEventPublisher#sessionCreated(javax.servlet.http.HttpSessionEvent)
     */
    @Override
    public void sessionCreated(HttpSessionEvent event) {
        if(sessionTimeout != null){
            event.getSession().setMaxInactiveInterval(sessionTimeout);
        }
        super.sessionCreated(event);
    }

    /**
     * Sets the session timeout.
     *
     * @param sessionTimeout the new session timeout
     */
    public void setSessionTimeout(Integer sessionTimeout){
        this.sessionTimeout = sessionTimeout;
    }
}