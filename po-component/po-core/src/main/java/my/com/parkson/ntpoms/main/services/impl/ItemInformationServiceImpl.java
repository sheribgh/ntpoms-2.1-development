package my.com.parkson.ntpoms.main.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.main.entities.ItemInformation;
import my.com.parkson.ntpoms.main.repositories.ItemInformationRepository;
import my.com.parkson.ntpoms.main.services.AccessLogsService;
import my.com.parkson.ntpoms.main.services.ItemInformationService;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class ItemInformationServiceImpl implements ItemInformationService {
	
	private static final String DEBUG_ERROR = "DEBUG";	
	private static final String DEBUG_DES = "Error fetching from database.";

	@Autowired
	private AccessLogsService log;
	
	@Autowired
	private ItemInformationRepository itemInformationRepository;
	
	@Override
	public List<ItemInformation> getAllItemInformation() {
		List<ItemInformation> itemInformationList = new ArrayList<>();		
		try {
			itemInformationList = itemInformationRepository.findAll();
		} catch (Exception e) {
			log.log(DEBUG_ERROR, DEBUG_DES, e);
		}		
		return itemInformationList;
	}	
	
	@Override
	public ItemInformation getItemInformationByItemNo(String itemNo) {
		try {
			return itemInformationRepository.findByItemNo(itemNo);
		} catch (Exception e) {
			log.log(DEBUG_ERROR, DEBUG_DES, e);
		}
		return null;
	}
	
	@Override
	public void createItemInformation(ItemInformation itemInformation) {
		try {			
			itemInformationRepository.save(itemInformation);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			log.log(DEBUG_ERROR, DEBUG_DES, e);
		}
	}
}