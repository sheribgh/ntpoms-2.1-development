/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import my.com.parkson.ntpoms.common.entities.Store;

/**
 * The Interface StoreService.
 */
public interface StoreService {

	/**
	 * Gets the all store.
	 *
	 * @return the all store
	 */
	public List<Store> getAllStore();
	
	
	
	public long getCount();

	/**
	 * Gets the all pageable store.
	 *
	 * @param pageRequest the page request
	 * @return the all pageable store
	 */
	public Page<Store> getAllPageableStore(Pageable pageRequest);

	/**
	 * Gets the all pageable store.
	 *
	 * @param s the s
	 * @param pageRequest the page request
	 * @return the all pageable store
	 */
	public Page<Store> getAllPageableStore(Specification<Store> s, Pageable pageRequest);
	

	/**
	 * Gets the all pageable store.
	 *
	 * @param pageRequest the page request
	 * @return the all pageable store
	 */
	public Page<Store> getAllPageableStoreOrdered(Pageable pageRequest);

	/**
	 * Gets the all pageable store.
	 *
	 * @param s the s
	 * @param pageRequest the page request
	 * @return the all pageable store
	 */
	public Page<Store> getAllPageableStoreOrdered(Specification<Store> s, Pageable pageRequest);
	

	/**
	 * Gets the all active store.
	 *
	 * @return the all active store
	 */
	public List<Store> getAllActiveStore();

	/**
	 * Gets the store by code.
	 *
	 * @param mf02_storeCode the mf 02 store code
	 * @return the store by code
	 */
	public Store getStoreByCode(String mf02_storeCode);

	/**
	 * Creates the store.
	 *
	 * @param store the store
	 */
	public void createStore(Store store);

	/**
	 * Gets the all active store by company code.
	 *
	 * @param companyCode the company code
	 * @return the all active store by company code
	 */
	public List<Store> getAllActiveStoreByCompanyCode(String companyCode);
}