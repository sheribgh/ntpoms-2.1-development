/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services.impl;

import static my.com.parkson.ntpoms.common.MessageCodes.FAILED_FETCH_RECORDS_EXCEPTION;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.util.StringUtils;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.LogMessages;
import my.com.parkson.ntpoms.common.RegExHashSet;
import my.com.parkson.ntpoms.common.entities.MenuGroup;
import my.com.parkson.ntpoms.common.entities.Role;
import my.com.parkson.ntpoms.common.entities.User;
import my.com.parkson.ntpoms.common.model.Menu;
import my.com.parkson.ntpoms.common.model.UserMenu;
import my.com.parkson.ntpoms.common.repositories.RoleRepository;
import my.com.parkson.ntpoms.common.services.AccessLogService;
import my.com.parkson.ntpoms.common.services.MenuService;
import my.com.parkson.ntpoms.common.services.MessageService;

/**
 * The Class MenuServiceImpl.
 */
@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class MenuServiceImpl implements MenuService {

	private static final Logger LOGGER = Logger.getLogger(MenuServiceImpl.class);

	/** The messages. */
	@Autowired
	private MessageService messages;

	/** The role repository. */
	@Autowired
	private RoleRepository roleRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * my.com.parkson.ntpoms.common.services.MenuService#getUserMenu(my.com.
	 * parkson.ntpoms.common.entities.User)
	 */
	@Override
	public UserMenu getUserMenu(User user) {
		if (user != null && !user.getRoles().isEmpty()) {
			String userRole = user.getRoles().get(0).getName();
			if (!StringUtils.isEmptyOrWhitespace(userRole)) {
				return this.getUserMenuByRoleName(userRole);
			}
		}
		return null;
	}

	/**
	 * Gets the user menu by role name.
	 *
	 * @param roleName
	 *            the role name
	 * @return the user menu by role name
	 */
	private UserMenu getUserMenuByRoleName(String roleName) {
		RegExHashSet authorizedPath = new RegExHashSet();
		UserMenu userMenu = new UserMenu();
		try {
			Role role = roleRepository.findByName(roleName);
			if (role != null) {
				userMenu.setActive(true);
				userMenu.setApp("NTPOMS");
				userMenu.setRole(role.getName());
				if (!role.getMenuGroupRoleList().isEmpty()) {
					MenuGroup menuGroup = role.getMenuGroupRoleList().get(0).getMenuGroup();
					Menu menu = new Menu();
					menu.setMenuItem(role.getName());
					menu.setName(role.getName());
					menu.setMenuUrl("/#");
					populateMenuTree(menu, menuGroup, authorizedPath);
					userMenu.setMenu(menu);
					return userMenu;
				}
				return null;
			}
		} catch (POException e) {
			LOGGER.error(e);
			return null;
		}

		return userMenu;
	}

	/**
	 * Populate menu tree.
	 *
	 * @param menu
	 *            the menu
	 * @param menuGroup
	 *            the menu group
	 * @param authorizedPath
	 *            the authorized path
	 */
	private void populateMenuTree(Menu menu, MenuGroup menuGroup, RegExHashSet authorizedPath) {
		List<Menu> listMenu = new ArrayList<>();
		if (!menuGroup.getMenuGroupChildren().isEmpty()) {
			for (MenuGroup menuGroup1 : menuGroup.getMenuGroupChildren()) {
				if (menuGroup1.isMenuGroupEnable()) {
					Menu menu1 = new Menu();
					menu1.setMenuItem(menuGroup1.getMenu().getMenuCode());
					String menuUrl = menuGroup1.getMenu().getMenuUrl();
					menu1.setMenuUrl(menuUrl);
					if (!menuUrl.contains("#")) {
						authorizedPath.add(menuUrl);
					}
					listMenu.add(menu1);
					populateMenuTree(menu1, menuGroup1, authorizedPath);
				}
			}
		}
		menu.setListMenu(listMenu);
	}
}