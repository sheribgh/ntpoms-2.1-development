/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import my.com.parkson.ntpoms.common.entities.OrderSubCategory;

/**
 * The Interface OrderSubCategoryRepository.
 */
public interface OrderSubCategoryRepository
		extends JpaRepository<OrderSubCategory, Integer>, JpaSpecificationExecutor<OrderSubCategory> {

	/**
	 * Find all by order by last modified on desc.
	 *
	 * @return the list
	 */
	public List<OrderSubCategory> findAllByOrderByLastModifiedOnDesc();

	/**
	 * Find all by is active order by last modified on desc.
	 *
	 * @param isActive the is active
	 * @return the list
	 */
	public List<OrderSubCategory> findAllByIsActiveOrderByLastModifiedOnDesc(boolean isActive);

	/**
	 * Find by code.
	 *
	 * @param code the code
	 * @return the order sub category
	 */
	public OrderSubCategory findByCode(String code);

	/**
	 * Find all by order category code.
	 *
	 * @param code the code
	 * @return the list
	 */
	@Query("select o from OrderSubCategory o where o.orderCategory.code = ?1")
	public List<OrderSubCategory> findAllByOrderCategoryCode(String code);

	/**
	 * Find all active by order category code.
	 *
	 * @param code the code
	 * @return the list
	 */
	@Query("select o from OrderSubCategory o where o.isActive = true and o.orderCategory.code = ?1")
	public List<OrderSubCategory> findAllActiveByOrderCategoryCode(String code);
	
	/**
	 * Find all by order by last modified on desc.
	 *
	 * @return the list
	 */
	public Page<OrderSubCategory> findAllByOrderByLastModifiedOnDesc(Pageable pageRequest);

	/**
	 * Find all by order by last modified on desc.
	 *
	 * @return the list
	 */
	public Page<OrderSubCategory> findAllByOrderByLastModifiedOnDesc(Specification<OrderSubCategory> s, Pageable pageRequest);

}