package my.com.parkson.ntpoms.main.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import my.com.parkson.ntpoms.main.entities.CreditTerms;
import my.com.parkson.ntpoms.main.entities.Project;


public interface ProjectRepository extends JpaRepository<Project, Integer> , JpaSpecificationExecutor<Project>{
	
	public Project findByCode (String code);

	public Page<Project> findAllByOrderByLastModifiedOnDesc(Pageable pageRequest);
	
	public Page<Project> findAllByOrderByLastModifiedOnDesc(Specification<Project> s, Pageable pageRequest);

	public List<Project> findAllByIsActiveOrderByLastModifiedOnDesc(boolean isActive);
	
	@Override
	public List<Project> findAll();

	@Query("select o from Project o where o.isActive = ?1 and (o.completionDate = NULL OR o.completionDate >= ?1)")
	public List<Project> findAllActive(boolean isActive, Date date);

	@Query("select o from Project o where o.isActive = ?1 and o.completionDate >= ?2 and o.pcCode LIKE %?3%")
	public List<Project> findAllActiveByProfitCenterCode (boolean isActive, Date date, String pcCode);
}
