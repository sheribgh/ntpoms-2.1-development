/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.entities;

import static javax.persistence.CascadeType.MERGE;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The Class User.
 */
@Entity
@Table(name = "\"MF20_M_ADMINMAST\"")
@Data
@NoArgsConstructor
public class User implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The username. */
	@Id
	@Column(name = "\"mf20_username\"")
	private String username;
	
	/** The password. */
	@NotEmpty
	@Size(min = 4)
	@Column(name = "\"mf20_password\"", nullable = false)
	private String password;
	
	/** The recent password. */
	@Column(name = "\"mf20_recentPassword\"")
	private String recentPassword;
	
	/** The start date. */
	@Column(name = "\"mf20_startDate\"")
	private Date startDate;
	
	/** The end date. */
	@Column(name = "\"mf20_endDate\"")
	private Date endDate;
	
	/** The is active. */
	@Column(name = "\"mf20_isActive\"")
	private boolean isActive;
	
	/** The is locked. */
	@Column(name = "\"mf20_isLocked\"")
	private boolean isLocked;
	
	/** The blocked. */
	@Column(name = "\"mf20_blocked\"")
	private int blocked;
	
	/** The created on. */
	@Column(name = "\"mf20_createdOn\"")
	private Date createdOn;
	
	@Column(name = "\"mf20_createdBy\"")
	private String createdBy;

	/** The last modified on. */
	@Column(name = "\"mf20_lastModifiedOn\"")
	private Date lastModifiedOn;
	
	@Column(name = "\"mf20_lastModifiedBy\"")
	private String lastModifiedBy;
	
	/** The deactivated on. */
	@Column(name = "\"mf20_deactivatedOn\"")
	private Date deactivatedOn;

	/** The reactivated on. */
	@Column(name = "\"mf20_reactivatedOn\"")
	private Date reactivatedOn;
	

	/** The password reset token. */
	@Column(name = "password_reset_token")
	private String passwordResetToken;
	
	@Column(name = "mf20_email")
	private String email;

	@Column(name = "\"mf20_reactivatedBy\"")
	private String reactivatedBy;
	
	@Column(name = "\"mf20_deactivatedBy\"")
	private String deactivatedBy;
	
	@Column(name = "\"mf20_logincount\"")
	private int logincount;
	
	/** The roles. */
	@ManyToMany(cascade = MERGE)
	@JoinTable(name = "user_role", joinColumns = {
			@JoinColumn(name = "user_id", referencedColumnName = "\"mf20_username\"") }, inverseJoinColumns = {
					@JoinColumn(name = "role_id", referencedColumnName = "role_id") })
	private List<Role> roles;
	
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	/**
	 * Gets the username.
	 *
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Sets the username.
	 *
	 * @param username the new username
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the password.
	 *
	 * @param password the new password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Gets the recent password.
	 *
	 * @return the recent password
	 */
	public String getRecentPassword() {
		return recentPassword;
	}

	/**
	 * Sets the recent password.
	 *
	 * @param recentPassword the new recent password
	 */
	public void setRecentPassword(String recentPassword) {
		this.recentPassword = recentPassword;
	}

	/**
	 * Gets the start date.
	 *
	 * @return the start date
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * Sets the start date.
	 *
	 * @param startDate the new start date
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * Gets the end date.
	 *
	 * @return the end date
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * Sets the end date.
	 *
	 * @param endDate the new end date
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * Checks if is active.
	 *
	 * @return true, if is active
	 */
	public boolean isActive() {
		return isActive;
	}

	/**
	 * Sets the active.
	 *
	 * @param isActive the new active
	 */
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	/**
	 * Checks if is locked.
	 *
	 * @return true, if is locked
	 */
	public boolean isLocked() {
		return isLocked;
	}

	/**
	 * Sets the locked.
	 *
	 * @param isLocked the new locked
	 */
	public void setLocked(boolean isLocked) {
		this.isLocked = isLocked;
	}

	/**
	 * Gets the blocked.
	 *
	 * @return the blocked
	 */
	public int getBlocked() {
		return blocked;
	}

	/**
	 * Sets the blocked.
	 *
	 * @param blocked the new blocked
	 */
	public void setBlocked(int blocked) {
		this.blocked = blocked;
	}

	/**
	 * Gets the created on.
	 *
	 * @return the created on
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * Sets the created on.
	 *
	 * @param createdOn the new created on
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * Gets the last modified on.
	 *
	 * @return the last modified on
	 */
	public Date getLastModifiedOn() {
		return lastModifiedOn;
	}

	/**
	 * Sets the last modified on.
	 *
	 * @param lastModifiedOn the new last modified on
	 */
	public void setLastModifiedOn(Date lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

	/**
	 * Gets the deactivated on.
	 *
	 * @return the deactivated on
	 */
	public Date getDeactivatedOn() {
		return deactivatedOn;
	}

	/**
	 * Sets the deactivated on.
	 *
	 * @param deactivatedOn the new deactivated on
	 */
	public void setDeactivatedOn(Date deactivatedOn) {
		this.deactivatedOn = deactivatedOn;
	}

	/**
	 * Gets the reactivated on.
	 *
	 * @return the reactivated on
	 */
	public Date getReactivatedOn() {
		return reactivatedOn;
	}

	/**
	 * Sets the reactivated on.
	 *
	 * @param reactivatedOn the new reactivated on
	 */
	public void setReactivatedOn(Date reactivatedOn) {
		this.reactivatedOn = reactivatedOn;
	}

	/**
	 * Gets the password reset token.
	 *
	 * @return the password reset token
	 */
	public String getPasswordResetToken() {
		return passwordResetToken;
	}

	/**
	 * Sets the password reset token.
	 *
	 * @param passwordResetToken the new password reset token
	 */
	public void setPasswordResetToken(String passwordResetToken) {
		this.passwordResetToken = passwordResetToken;
	}
	
	/**
	 * Gets the roles.
	 *
	 * @return the roles
	 */
	public List<Role> getRoles() {
		return roles;
	}

	/**
	 * Sets the roles.
	 *
	 * @param roles the new roles
	 */
	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getReactivatedBy() {
		return reactivatedBy;
	}

	public void setReactivatedBy(String reactivatedBy) {
		this.reactivatedBy = reactivatedBy;
	}

	public String getDeactivatedBy() {
		return deactivatedBy;
	}

	public void setDeactivatedBy(String mf20_deactivatedBy) {
		this.deactivatedBy = mf20_deactivatedBy;
	}

	public int getLogincount() {
		return logincount;
	}

	public void setLogincount(int logincount) {
		this.logincount = logincount;
	}



}