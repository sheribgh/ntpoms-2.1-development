package my.com.parkson.ntpoms.common.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "\"Mf22_M_SCHEDULER\"")
@Data
@NoArgsConstructor
public class Scheduler implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mf22_m_scheduler_mf22_schedulerid_seq")
	@SequenceGenerator(name = "mf22_m_scheduler_mf22_schedulerid_seq", sequenceName = "mf22_m_scheduler_mf22_schedulerid_seq", allocationSize = 1)
	@Column(name = "mf22_schedulerid")
	private int id;

	@Column(name = "mf22_task")
	private String task;

	@Column(name = "mf22_status")
	private String status;

	@Column(name = "mf22_startdate")
	private Date startdate;

	@Column(name = "mf22_cronfrequency_amount")
	private String cronexpression;

	@Column(name = "mf22_cronfrequency_occurrence")
	private String crondetails;

	@Column(name = "mf22_jobname")
	private String jobname;

	@Column(name = "mf22_table")
	private String table;

	@Column(name = "mf22_database")
	private String database;

	@Column(name = "mf22_scheduletype")
	private String scheduleType;

	@Column(name = "mf22_enddate")
	private Date endDate;

	@Column(name = "mf22_noenddate")
	private boolean isActive;

	@Column(name = "mf22_upload")
	private String upload;

	@Column(name = "mf22_path")
	private String path;

	@Column(name = "mf22_createdby")
	private String createdBy;

	@Column(name = "mf22_createdon")
	private Date createdOn;

	@Column(name = "mf22_deactivatedby")
	private String deactivatedBy;

	@Column(name = "mf22_deactivatedon")
	private Date deactivatedOn;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTask() {
		return task;
	}

	public void setTask(String task) {
		this.task = task;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getStartdate() {
		return startdate;
	}

	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}

	/**
	 * @return the cronexpression
	 */
	public String getCronexpression() {
		return cronexpression;
	}

	/**
	 * @param cronexpression
	 *            the cronexpression to set
	 */
	public void setCronexpression(String cronexpression) {
		this.cronexpression = cronexpression;
	}

	/**
	 * @return the crondetails
	 */
	public String getCrondetails() {
		return crondetails;
	}

	/**
	 * @param crondetails
	 *            the crondetails to set
	 */
	public void setCrondetails(String crondetails) {
		this.crondetails = crondetails;
	}

	public String getTable() {
		return table;
	}

	public void setTable(String table) {
		this.table = table;
	}

	public String getDatabase() {
		return database;
	}

	public void setDatabase(String database) {
		this.database = database;
	}

	public String getScheduleType() {
		return scheduleType;
	}

	public void setScheduleType(String scheduleType) {
		this.scheduleType = scheduleType;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getUpload() {
		return upload;
	}

	public void setUpload(String upload) {
		this.upload = upload;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getDeactivatedBy() {
		return deactivatedBy;
	}

	public void setDeactivatedBy(String deactivatedBy) {
		this.deactivatedBy = deactivatedBy;
	}

	/**
	 * @return the jobname
	 */
	public String getJobname() {
		return jobname;
	}

	/**
	 * @param jobname
	 *            the jobname to set
	 */
	public void setJobname(String jobname) {
		this.jobname = jobname;
	}

	public Date getDeactivatedOn() {
		return deactivatedOn;
	}

	/**
	 * @return the isActive
	 */
	public boolean isActive() {
		return isActive;
	}

	/**
	 * @param isActive
	 *            the isActive to set
	 */
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public void setDeactivatedOn(Date deactivatedOn) {
		this.deactivatedOn = deactivatedOn;
	}
}