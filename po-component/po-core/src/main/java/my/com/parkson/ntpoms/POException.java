/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms;

/**
 * The Class POException.
 */
public class POException extends RuntimeException {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new PO exception.
	 */
	public POException() {
		super();
	}

	/**
	 * Instantiates a new PO exception.
	 *
	 * @param message the message
	 * @param cause the cause
	 * @param enableSuppression the enable suppression
	 * @param writableStackTrace the writable stack trace
	 */
	public POException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * Instantiates a new PO exception.
	 *
	 * @param message the message
	 * @param cause the cause
	 */
	public POException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Instantiates a new PO exception.
	 *
	 * @param message the message
	 */
	public POException(String message) {
		super(message);
	}

	/**
	 * Instantiates a new PO exception.
	 *
	 * @param cause the cause
	 */
	public POException(Throwable cause) {
		super(cause);
	}

}
