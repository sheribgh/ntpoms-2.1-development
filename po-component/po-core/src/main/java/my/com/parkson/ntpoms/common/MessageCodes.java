/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common;

/**
 * The Class MessageCodes.
 */
public final class MessageCodes {	
	
	/** The Constant SUCCESS_SAVE. */
	public static final String SUCCESS_SAVE = "success.save";
	
	/** The Constant SUCCESS_FETCH_RECORDS. */
	public static final String SUCCESS_FETCH_RECORDS = "success.fetchrecords";
	
	/** The Constant SUCCESS_FILE_CLOSE. */
	public static final String SUCCESS_FILE_CLOSE = "success.fileclose";

	/** The Constant FAILED_IOEXCEPTION. */
	public static final String FAILED_IOEXCEPTION = "exception.ioexception";
	
	/** The Constant FAILED_SQLEXCEPTION. */
	public static final String FAILED_SQLEXCEPTION = "exception.sqlexception";
	
	/** The Constant FAILED_NOTFOUND_EXCEPTION. */
	public static final String FAILED_NOTFOUND_EXCEPTION = "exception.notfound.algorithm";
	
	/** The Constant FAILED_FETCH_RECORDS_EXCEPTION. */
	public static final String FAILED_FETCH_RECORDS_EXCEPTION = "exception.fetchrecords.failed";
	
	/** The Constant FAILED_PERSIST_EXCEPTION. */
	public static final String FAILED_PERSIST_EXCEPTION = "exception.save.failed";
	
	/** The Constant FAILED_DBCONNECTION_EXCEPTION. */
	public static final String FAILED_DBCONNECTION_EXCEPTION = "exception.dbconnection.failed";
	
	/** The Constant FAILED_DBBACKUP_EXCEPTION. */
	public static final String FAILED_DBBACKUP_EXCEPTION = "exception.dbbackup.failed";
	
	/** The Constant FAILED_UPLOAD_EXCEPTION. */
	public static final String FAILED_UPLOAD_EXCEPTION = "exception.upload.failed";
	
	/** The Constant FAILED_DBDROP_EXCEPTION. */
	public static final String FAILED_DBDROP_EXCEPTION = "exception.dropdb.failed";
	
	/** The Constant FAILED_DBCREATE_EXCEPTION. */
	public static final String FAILED_DBCREATE_EXCEPTION = "exception.createdb.failed";
	
	/** The Constant FAILED_DBUPDATE_EXCEPTION. */
	public static final String FAILED_DBUPDATE_EXCEPTION = "exception.updatedb.failed";
	
	/** The Constant FAILE_DBRESTORE_EXCEPTION. */
	public static final String FAILE_DBRESTORE_EXCEPTION = "exception.restoredb.failed";
	
	/** The Constant FAILED_CLOSE_STATEMENT_EXCEPTION. */
	public static final String FAILED_CLOSE_STATEMENT_EXCEPTION = "exception.closestatement.failed";
	
	/** The Constant FAILED_CLOSE_CONNECTION_EXCEPTION. */
	public static final String FAILED_CLOSE_CONNECTION_EXCEPTION = "exception.closeconnection.failed";
	
	/** The Constant FAILED_READFILE_EXCEPTION. */
	public static final String FAILED_READFILE_EXCEPTION = "exception.readfile.failed";
	
	/** The Constant FAILED_CLOESFILE_EXCEPTION. */
	public static final String FAILED_CLOESFILE_EXCEPTION = "exception.closefile.failed";
	
	/** The Constant FAILED_QUARTZJOB_EXCEPTION. */
	public static final String FAILED_QUARTZJOB_EXCEPTION = "exception.quartzjob.failed";
	
	/** The Constant FAILED_SENDMAIL_EXCEPTION. */
	public static final String FAILED_SENDMAIL_EXCEPTION = "exception.sendmail.failed";
	
	/** The Constant FAILED_PARSETOINT_EXCEPTION. */
	public static final String FAILED_PARSETOINT_EXCEPTION = "exception.parsetoint.failed";
	
	/** The Constant FAILED_INTERRUPTED_EXCEPTION. */
	public static final String FAILED_INTERRUPTED_EXCEPTION = "exception.interrupted.failed";
	
	/** The Constant FAILED_SESSIONFACTORY_CREATION_EXCEPTION. */
	public static final String FAILED_SESSIONFACTORY_CREATION_EXCEPTION = "exception.factorycreation.failed";
	
	public static final String INVALID_FILE_ORDER_AUTH = "Invalid File Type";
	
	public static final String MISSING_LOGO_FILE = "Logo has removed from Directory ";

	/**
	 * Instantiates a new message codes.
	 */
	private MessageCodes() {
		super();
	}
}
