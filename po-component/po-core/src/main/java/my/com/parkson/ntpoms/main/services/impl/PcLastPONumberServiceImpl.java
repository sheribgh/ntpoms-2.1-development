package my.com.parkson.ntpoms.main.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.main.entities.ProfitCenterLastPONumber;
import my.com.parkson.ntpoms.main.repositories.PCLastPONumberRepository;
import my.com.parkson.ntpoms.main.services.PcLastPONumberService;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class PcLastPONumberServiceImpl implements PcLastPONumberService {
	
	@Autowired PCLastPONumberRepository pCLastPONumberRepository;

	@Override
	public Page<ProfitCenterLastPONumber> getAllPagablePc(Specification<ProfitCenterLastPONumber> s,
			Pageable pageRequest) {
		return pCLastPONumberRepository.findAll(s,pageRequest);
	}

	@Override
	public Page<ProfitCenterLastPONumber> getAllPagablePc(Pageable pageRequest) {
		return pCLastPONumberRepository.findAll(pageRequest);
	}

	@Override
	public void createProfitCenterLastPONumber(ProfitCenterLastPONumber profitCenterLastPONumber) {
		pCLastPONumberRepository.save(profitCenterLastPONumber);
	}

	@Override
	public ProfitCenterLastPONumber getPcLastPONumberByPcCodeAndYear(String pcCode, String year) {
		return pCLastPONumberRepository.findAllByPcCodeAndYear(pcCode, year);
	}
}