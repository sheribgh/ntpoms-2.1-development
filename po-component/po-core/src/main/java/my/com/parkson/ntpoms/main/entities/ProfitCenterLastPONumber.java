package my.com.parkson.ntpoms.main.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "\"mf04_m_profitcenterlastponumber\"")
@Data
@NoArgsConstructor
public class ProfitCenterLastPONumber implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "\"mf13mpcmast_mf13_pccode\"")
	private String pcCode;
	
	@Column(name = "\"mf04_runningno\"")
	private int runningNumber;
	
	@Column(name = "\"mf04_lastponumber\"")
	private String lastPoNumber;
	
	@Column(name = "\"mf04_year\"")
	private String year;

	public String getPcCode() {
		return pcCode;
	}

	public void setPcCode(String pcCode) {
		this.pcCode = pcCode;
	}

	public int getRunningNumber() {
		return runningNumber;
	}

	public void setRunningNumber(int runningNumber) {
		this.runningNumber = runningNumber;
	}

	public String getLastPoNumber() {
		return lastPoNumber;
	}

	public void setLastPoNumber(String lastPoNumber) {
		this.lastPoNumber = lastPoNumber;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}
	
	
	
}
