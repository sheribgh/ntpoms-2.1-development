/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import my.com.parkson.ntpoms.common.entities.Scheduler;

/**
 * The Interface SchedulerRepository.
 */
public interface SchedulerRepository extends JpaRepository<Scheduler, Integer>, JpaSpecificationExecutor<Scheduler> {

	/**
	 * Find by id.
	 *
	 * @param id the id
	 * @return the scheduler
	 */
	public Scheduler findById(int id);

	/* (non-Javadoc)
	 * @see org.springframework.data.jpa.repository.JpaRepository#findAll()
	 */
	public List<Scheduler> findAll();

	/**
	 * Find all by order by id desc.
	 *
	 * @return the list
	 */
	public List<Scheduler> findAllByOrderByIdDesc();

	@Query("SELECT t FROM Scheduler t WHERE t.jobname = :name")
	public Scheduler findByJobName(@Param("name") String name);
}