package my.com.parkson.ntpoms.main.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import my.com.parkson.ntpoms.main.entities.VendorBranch;
import my.com.parkson.ntpoms.main.entities.VendorBranchId;

public interface VendorBranchService {

	public List<VendorBranch> getAllVendorBranch();

	public List<VendorBranch> getAllActiveVendorBranch();


	public void createVendorBranch(VendorBranch vendorBranch);

	Page<VendorBranch> getPagebleAll(Pageable req);
	
	public Page<VendorBranch> getAllPageableVendorBranch(Pageable pageRequest);
	
	public Page<VendorBranch> getAllPageableVendorBranch(Specification<VendorBranch> s, Pageable pageRequest);
	
	public Page<VendorBranch> getAllPageableVendorBranchOrdered(Pageable pageRequest);
	
	public Page<VendorBranch> getAllPageableVendorBranchOrdered(Specification<VendorBranch> s, Pageable pageRequest);

	public VendorBranch getVendorBranchbyVendorId(VendorBranchId pkey);
	
	public List<VendorBranch> getAllActiveVendorBranchBycompCode(String companyCode);
	public long getCount();
	
	public long getCountActive();

	public VendorBranch getvendor(String code);
}