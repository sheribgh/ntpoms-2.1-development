/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.query.Param;

import my.com.parkson.ntpoms.common.entities.Company;
import my.com.parkson.ntpoms.common.entities.LoginList;

/**
 * The Interface CompanyService.
 */
public interface LogInService {

	/**
	 * Creates the company in LoginList.
	 *
	 * @param company the company
	 */
	public void createCompany(LoginList company);
	
	public List<LoginList> getYearByCompanyCode(String CompanyCode);
	
	
	public List<Company> getStatusByCompanyCode(String CompanyCode);

}