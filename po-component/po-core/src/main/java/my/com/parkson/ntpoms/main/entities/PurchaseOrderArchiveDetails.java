package my.com.parkson.ntpoms.main.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.DateBridge;
import org.hibernate.search.annotations.DocumentId;
import org.hibernate.search.annotations.EncodingType;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.NumericField;
import org.hibernate.search.annotations.Resolution;
import org.hibernate.search.annotations.Store;

import lombok.Data;

@Entity
@Indexed
@Data
@Table(name = "po07_t_poarchivedirectory", schema = "mfmaindb")
public class PurchaseOrderArchiveDetails implements Serializable {

	private static final long serialVersionUID = -2981981002795730547L;

	@Id
	@Column(name = "po07_ponumber")
	private String poNo;


	@Column(name = "\"po07_compcode\"")
	private String compCode;


	@Column(name = "\"po07_vendorcode\"")
	private String vendorCode;

	
	@Column(name = "\"po07_pccode\"")
	private String pcCode;

	
	@Column(name = "\"po07_vendorname\"")
	private String vendorName;

	
	@Column(name = "po07_issuerpsid")
	private String issuerPsid;

	
	@Column(name = "\"po07_issuername\"")
	private String issuerName;

	
	@Field(index=Index.YES, analyze=Analyze.YES, store=Store.NO)	
	@NumericField
	@Column(name = "\"po07_totalamt\"")
	private double totalAmt;

	
	@Column(name = "\"po07_archivedtable\"")
	private String archivedTable;

	
	@Column(name = "\"po07_users\"")
	private String users;

	
	/*@Field(index=Index.YES, analyze=Analyze.YES,store=Store.NO)
	@DateBridge(resolution = Resolution.DAY, encoding=EncodingType.STRING)*/
	@Column(name = "\"po07_archivedyear\"")
	private String archivedYear;

	@Field(index=Index.YES, analyze=Analyze.YES,store=Store.NO)
	@DateBridge(resolution = Resolution.DAY, encoding=EncodingType.STRING)
	@Column(name = "po07_createdon")
	private Date createdOn;

	public String getPoNo() {
		return poNo;
	}

	public void setPoNo(String poNo) {
		this.poNo = poNo;
	}

	public String getCompCode() {
		return compCode;
	}

	public void setCompCode(String compCode) {
		this.compCode = compCode;
	}

	public String getVendorCode() {
		return vendorCode;
	}

	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}

	public String getPcCode() {
		return pcCode;
	}

	public void setPcCode(String pcCode) {
		this.pcCode = pcCode;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public String getIssuerPsid() {
		return issuerPsid;
	}

	public void setIssuerPsid(String issuerPsid) {
		this.issuerPsid = issuerPsid;
	}

	public String getIssuerName() {
		return issuerName;
	}

	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}

	public double getTotalAmt() {
		return totalAmt;
	}

	public void setTotalAmt(double totalAmt) {
		this.totalAmt = totalAmt;
	}

	public String getArchivedtable() {
		return archivedTable;
	}

	public void setArchivedtable(String archivedtable) {
		this.archivedTable = archivedtable;
	}

	public String getUsers() {
		return users;
	}

	public void setUsers(String users) {
		this.users = users;
	}

	

	public String getArchivedYear() {
		return archivedYear;
	}

	public void setArchivedYear(String archivedYear) {
		this.archivedYear = archivedYear;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	
	
}