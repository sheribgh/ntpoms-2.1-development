package my.com.parkson.ntpoms.main.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import my.com.parkson.ntpoms.main.entities.VendorBranch;
import my.com.parkson.ntpoms.main.entities.VendorCreditTerms;
import my.com.parkson.ntpoms.main.entities.VendorCreditTermsId;

public interface VendorCreditTermsRepository
		extends JpaRepository<VendorCreditTerms, VendorCreditTermsId>, JpaSpecificationExecutor<VendorCreditTerms> {

	// @Query("select u from VendorCreditTerms u where u.creditTermsCode = ?")
	// public List<VendorCreditTerms> findByCreditTermsCode(String creditTermsCode);

	public List<VendorCreditTerms> findByVendcrtIdCreditTermsCode(String creditTermsCode);

	// @Query("select u from VendorCreditTerms u where u.vendorCode = ? and
	// u.creditTermsCode = ?")
	// public List<VendorCreditTerms> findByVendorCodeAndCreditTermsCode(String
	// vendorCode, String creditTermsCode);

	// @Query("select u from VendorCreditTerms u where u.vendorCode = ? and
	// u.vendorBranchCode = ?")
	// public List<VendorCreditTerms> findByVendorCodeAndVendorBranchCode(String
	// vendorCode, String vendorBranchCode);

	public List<VendorCreditTerms> findByVendcrtIdVendorCodeAndVendcrtIdCreditTermsCodeAndVendcrtIdVendorBranchCode(
			String vendorCode, String creditTermsCode, String vendorBranchCode);

	public VendorCreditTerms findByVendcrtId(VendorCreditTermsId vendcrtId);

	public List<VendorCreditTerms> findByVendcrtIdVendorCodeAndVendcrtIdVendorBranchCodeAndVendcrtIdCompanyCodeOrderByLastModifiedOnDesc(
			String vendorCode,String vendorBranchCode,String companyCode
			
			);
	
	// @Query("select u from VendorCreditTerms u where u.vendorCode = ? and
	// u.vendorBranchCode = ? and u.creditTermsCode = ?")
	// public List<VendorCreditTerms>
	// findByVendorCodeAndVendorBranchCodeAndCreditTermsCode(String
	// vendorCode,String vendorBranchCode,
	// String creditTermsCode );

	public Page<VendorCreditTerms> findAllByOrderByLastModifiedOnDesc(Pageable pageRequest);
	public List<VendorCreditTerms> findAllByIsActiveOrderByLastModifiedOnDesc(boolean isActive);

}