/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.main.services.impl;

import static my.com.parkson.ntpoms.common.MessageCodes.FAILED_FETCH_RECORDS_EXCEPTION;
import static my.com.parkson.ntpoms.common.MessageCodes.FAILED_PERSIST_EXCEPTION;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.LogMessages;
import my.com.parkson.ntpoms.common.services.MessageService;
import my.com.parkson.ntpoms.main.entities.PurchaseOrder;
import my.com.parkson.ntpoms.main.repositories.PurchaseOrderRepository;
import my.com.parkson.ntpoms.main.services.AccessLogsService;
import my.com.parkson.ntpoms.main.services.PurchaseOrderService;

/**
 * The Class PurchaseOrderServiceImpl.
 */
@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class PurchaseOrderServiceImpl implements PurchaseOrderService {

	private static final Logger LOGGER = Logger.getLogger(PurchaseOrderServiceImpl.class);

	/** The messages. */
	@Autowired
	private MessageService messages;

	/** The purchase order repository. */
	@Autowired
	private PurchaseOrderRepository purchaseOrderRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.PurchaseOrderService#
	 * createPurchaseOrder(my.com.parkson.ntpoms.common.entities.PurchaseOrder)
	 */
	@Override
	public void createPurchaseOrder(PurchaseOrder purchaseOrder) {
		try {
			purchaseOrderRepository.save(purchaseOrder);
		} catch (POException e) {
			LOGGER.error(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.PurchaseOrderService#
	 * getAllPurchaseOrder()
	 */
	@Override
	public List<PurchaseOrder> getAllPurchaseOrder() {
		List<PurchaseOrder> purchaseOrderList = new ArrayList<>();
		try {
			purchaseOrderList = purchaseOrderRepository.findAll();
		} catch (POException e) {
			LOGGER.error(e);
		}
		return purchaseOrderList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.PurchaseOrderService#
	 * getPurchaseOrderByPoNo(java.lang.String)
	 */
	@Override
	public PurchaseOrder getPurchaseOrderByPoNo(String poNo) {
		try {
			return purchaseOrderRepository.findByPoNo(poNo);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.PurchaseOrderService#
	 * getPurchaseOrderByStatus(java.lang.String)
	 */
	@Override
	public List<PurchaseOrder> getPurchaseOrderByStatus(String status) {
		List<PurchaseOrder> purchaseOrderList = new ArrayList<>();
		try {
			purchaseOrderList = purchaseOrderRepository.findByStatus(status);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return purchaseOrderList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.PurchaseOrderService#
	 * getPurchaseOrderByPcCode(java.lang.String)
	 */
	@Override
	public List<PurchaseOrder> getPurchaseOrderByPcCode(String pcCode) {
		List<PurchaseOrder> purchaseOrderList = new ArrayList<>();
		try {
			purchaseOrderList = purchaseOrderRepository.findByPcCode(pcCode);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return purchaseOrderList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.PurchaseOrderService#
	 * getAllNonDraftPurchaseOrder()
	 */
	@Override
	public List<PurchaseOrder> getAllNonDraftPurchaseOrder() {
		List<PurchaseOrder> purchaseOrderList = new ArrayList<>();
		try {
			purchaseOrderList = purchaseOrderRepository.findAllNonDraft();
		} catch (POException e) {
			LOGGER.error(e);
		}
		return purchaseOrderList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.PurchaseOrderService#
	 * getLastPurchaseOrderByPcCode(java.lang.String)
	 */
	@Override
	public PurchaseOrder getLastPurchaseOrderByPcCode(String pcCode) {
		try {
			List<PurchaseOrder> purchaseOrderList = purchaseOrderRepository.findByPcCodeOrderByPoNoDesc(pcCode);
			return purchaseOrderList.get(0);
		} catch (POException e) {
			LOGGER.error(e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see my.com.parkson.ntpoms.common.services.PurchaseOrderService#
	 * getAllPageablePurchaseOrder(org.springframework.data.domain.Pageable)
	 */
	@Override
	public Page<PurchaseOrder> getAllPageablePurchaseOrder(Pageable pageRequest) {
		return purchaseOrderRepository.findAll(pageRequest);
	}

	@Override
	public Page<PurchaseOrder> getAllPageablePurchaseOrder(Specification<PurchaseOrder> s, Pageable pageRequest) {
		return purchaseOrderRepository.findAll(s, pageRequest);
	}

	@Override
	public List<PurchaseOrder> getPurchaseOrderByCompanyCode(String company) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<PurchaseOrder> getAllPageablePurchaseOrderwithpono(Specification<PurchaseOrder> s,
			Pageable pageRequest) {
		// TODO Auto-generated method stub
		return purchaseOrderRepository.findByPoNo(s, pageRequest);
	}

	@Override
	public Page<PurchaseOrder> getAllPageablePurchaseOrderwithpono(Pageable pageRequest) {
		// TODO Auto-generated method stub
		return purchaseOrderRepository.findByPoNo(pageRequest);
	}

	@Override
	public PurchaseOrder getAllPurchaseOrderByPoNo(String poNo) {
		// TODO Auto-generated method stub
		return purchaseOrderRepository.findByPoNo(poNo);
	}

	@Override
	public List<PurchaseOrder> getPurchaseOrderExpiryDetails(Date date, String status) {
		return purchaseOrderRepository.getPurchaseOrderExpiryDetails(date, status);
	}

}