/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;


/* This is a modal class that fetches data from the Database and generates Getters and 
 * Setters for the same
 */

/**
 * The Class Employee.
 */
@Entity
@Table(name = "\"MF12_M_EMPMAST\"")
@Data
@NoArgsConstructor
public class Employee implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The ps id. */
	@Id
	@Column(name = "\"mf12_psId")
	private String psId;
	
	/** The alt id. */
	@Column(name = "\"mf12_altId")
	private String altId;
	
	/** The name. */
	@Column(name = "\"mf12_empName")
	private String name;
	
	/** The pos title. */
	@Column(name = "\"mf12_posTitle")
	private String posTitle;
	
	/** The email. */
	@Column(name = "\"email")
	private String email;
	
	/** The paygrp. */
	@Column(name = "\"mf12_payGrp")
	private String paygrp ;
	
	/** The is active. */
	@Column(name = "\"mf12_isActive\"")
	private boolean isActive;

	/** The company. */
	@ManyToOne
	@JoinColumn(name="\"mf11MCompmast_mf11_compCodeHRIS\"", referencedColumnName = "\"mf11_compCodeHRIS\"" ,nullable = false)
	private Company company;
	
	/** The profit center. */
	@ManyToOne
	@JoinColumn(name= "\"mf13MPcmast_mf13_pcCode\"" , nullable = false)
	private ProfitCenter profitCenter;

	/**
	 * Gets the ps id.
	 *
	 * @return the ps id
	 */
	public String getPsId() {
		return psId;
	}

	/**
	 * Sets the ps id.
	 *
	 * @param psId the new ps id
	 */
	public void setPsId(String psId) {
		this.psId = psId;
	}

	/**
	 * Gets the alt id.
	 *
	 * @return the alt id
	 */
	public String getAltId() {
		return altId;
	}

	/**
	 * Sets the alt id.
	 *
	 * @param altId the new alt id
	 */
	public void setAltId(String altId) {
		this.altId = altId;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the pos title.
	 *
	 * @return the pos title
	 */
	public String getPosTitle() {
		return posTitle;
	}

	/**
	 * Sets the pos title.
	 *
	 * @param posTitle the new pos title
	 */
	public void setPosTitle(String posTitle) {
		this.posTitle = posTitle;
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the email.
	 *
	 * @param email the new email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Gets the paygrp.
	 *
	 * @return the paygrp
	 */
	public String getPaygrp() {
		return paygrp;
	}

	/**
	 * Sets the paygrp.
	 *
	 * @param paygrp the new paygrp
	 */
	public void setPaygrp(String paygrp) {
		this.paygrp = paygrp;
	}	
	
	/**
	 * Checks if is active.
	 *
	 * @return true, if is active
	 */
	public boolean isActive() {
		return isActive;
	}

	/**
	 * Sets the active.
	 *
	 * @param isActive the new active
	 */
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	/**
	 * Gets the company.
	 *
	 * @return the company
	 */
	public Company getCompany() {
		return company;
	}

	/**
	 * Sets the company.
	 *
	 * @param company the new company
	 */
	public void setCompany(Company company) {
		this.company = company;
	}

	/**
	 * Gets the profit center.
	 *
	 * @return the profit center
	 */
	public ProfitCenter getProfitCenter() {
		return profitCenter;
	}

	/**
	 * Sets the profit center.
	 *
	 * @param profitCenter the new profit center
	 */
	public void setProfitCenter(ProfitCenter profitCenter) {
		this.profitCenter = profitCenter;
	}
}