/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.entities;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;


/* This is a modal class that fetches data from the Database and generates Getters and 
 * Setters for the same
 */

/**
 * The Class EmployeeMaster.
 */
@Entity
@Table(name = "\"MF12_M_EMPMAST\"")
@DiscriminatorValue(value="TRUE")
@Data
@NoArgsConstructor
public class EmployeeMaster implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The ps id. */
	@Id
	@Column(name = "\"mf12_psId\"")
	public String psId;
	
	/** The alt id. */
	@Column(name = "\"mf12_altId\"")
	public String altId;
	
	/** The name. */
	@Column(name = "\"mf12_empName\"")
	public String name;
	
	/** The pref name. */
	@Column(name = "\"mf12_prefName\"")
	public String prefName;
	
	/** The new ic no. */
	@Column(name = "\"mf12_newICNo\"")
	public String newIcNo;
	
	/** The old ic no. */
	@Column(name = "\"mf12_oldICNo\"")
	public String oldIcNo;
	
	/** The passport no. */
	@Column(name = "\"mf12_passportNo\"")
	public String passportNo ;
	
	/** The gender. */
	@Column(name = "\"mf12_gender\"")
	public String gender;

	/** The ethnic. */
	@Column(name = "\"mf12_ethnic\"")
	public String ethnic;

	/** The dob. */
	@Column(name = "\"mf12_dob\"")
	public Date dob;
	
	/** The foreign local. */
	@Column(name = "\"mf12_foreignLocal\"")
	public String foreignLocal ;
	
	/** The marital status. */
	@Column(name = "\"mf12_maritalStat\"")
	public String maritalStatus;
	
	/** The phone no. */
	@Column(name = "\"mf12_phoneNo\"")
	public String phoneNo;	

	/** The ext. */
	@Column(name = "\"mf12_ext\"")
	public String ext;	

	/** The email. */
	@Column(name = "\"mf12_email\"")
	public String email;
	
	/** The hr status. */
	@Column(name = "\"mf12_hrStatus\"")
	public String hrStatus ;
	
	/** The payroll status. */
	@Column(name = "\"mf12_payrollStatus\"")
	public String payrollStatus ;
	
	/** The pos no. */
	@Column(name = "\"mf12_posNo\"")
	public String posNo;
	
	/** The pos title. */
	@Column(name = "\"mf12_posTitle\"")
	public String posTitle;
	
	/** The code HRIS. */
	@Column(name = "\"mf12_deptCodeHRIS\"")
	public String codeHRIS;
	
	/** The desc HRIS. */
	@Column(name = "\"mf12_deptDescHRIS\"")
	public String descHRIS ;	

	/** The loc. */
	@Column(name = "\"mf12_loc\"")
	public String loc;	
	
	/** The loc desc. */
	@Column(name = "\"mf12_locDesc\"")
	public String locDesc;
	
	/** The salary plan. */
	@Column(name = "\"mf12_salaryPlan\"")
	public String salaryPlan;
	
	/** The salary grade. */
	@Column(name = "\"mf12_salaryGrade\"")
	public String salaryGrade ;
	
	/** The rpt to pos no. */
	@Column(name = "\"mf12_rptToPosNo\"")
	public String rptToPosNo;
	
	/** The rpt to emp id. */
	@Column(name = "\"mf12_rptToEmpId\"")
	public String rptToEmpId ;
	
	/** The payroll comp. */
	@Column(name = "\"mf12_payrollComp\"")
	public String payrollComp;
	
	/** The badge no. */
	@Column(name = "\"mf12_badgeNo\"")
	public String badgeNo;
	
	/** The hire date. */
	@Column(name = "\"mf12_hireDt\"")
	public Date hireDate;
	
	/** The confirm date. */
	@Column(name = "\"mf12_confirmDt\"")
	public Date confirmDate ;
	
	/** The last date work. */
	@Column(name = "\"mf12_lstDtWrk\"")
	public Date lastDateWork;
	
	/** The trf date. */
	@Column(name = "\"mf12_trfDt\"")
	public Date trfDate;
	
	/** The exp job end date. */
	@Column(name = "\"mf12_expJobEndDt\"")
	public Date expJobEndDate;
	
	/** The from comp code. */
	@Column(name = "\"mf12_frmCompCode\"")
	public String fromCompCode ;
	
	/** The to comp code. */
	@Column(name = "\"mf12_toCompCode\"")
	public String toCompCode ;
	
	/** The from loc. */
	@Column(name = "\"mf12_frmLoc\"")
	public String fromLoc ;
	
	/** The to loc. */
	@Column(name = "\"mf12_toLoc\"")
	public String toLoc ;
	
	/** The trf send flag. */
	@Column(name = "\"mf12_trfSndFlag\"")
	public String trfSendFlag; 
	
	/** The emp cat. */
	@Column(name = "\"mf12_empCat\"")
	public String empCat ;
	
	/** The last update time. */
	@Column(name = "\"mf12_lstUpdDtTm\"")
	public Timestamp lastUpdateTime ;
	
	/** The prc time. */
	@Column(name = "\"mf12_prcDtTm\"")
	public Timestamp prcTime ;
	
	/** The paygrp. */
	@Column(name = "\"mf12_payGrp\"")
	public String paygrp ;
	
	/** The to profit centre. */
	@Column(name = "\"mf12_toProfitCentre\"")
	public String toProfitCentre ;
	
	/** The to cost charge company. */
	@Column(name = "\"mf12_toCostChargeCompany\"")
	public String toCostChargeCompany ;
	
	/** The from profit centre. */
	@Column(name = "\"mf12_fromProfitCentre\"")
	public String fromProfitCentre ;
	
	/** The from cost charge company. */
	@Column(name = "\"mf12_fromCostChargeCompany\"")
	public String fromCostChargeCompany ;
	
	/** The is active. */
	@Column(name = "\"mf12_isActive\"")
	public boolean isActive;
	
	/** The created on. */
	@Column(name = "\"mf12_createdOn\"")
	public Timestamp createdOn;
	
	/** The created by. */
	@Column(name = "\"mf12_createdBy\"")
	public String createdBy;
	
	/** The last modified on. */
	@Column(name = "\"mf12_lastModifiedOn\"")
	public Timestamp lastModifiedOn ;
	
	/** The last modified by. */
	@Column(name = "\"mf12_lastModifiedBy\"")
	public String  lastModifiedBy;

	/** The company. */
	@ManyToOne
	@JoinColumn(name="\"mf11MCompmast_mf11_compCodeHRIS\"", referencedColumnName = "\"mf11_compCodeHRIS\"" ,nullable = false)
	public Company company;

	/** The profit center. */
	@ManyToOne
	@JoinColumn(name= "\"mf13MPcmast_mf13_pcCode\"" , nullable = false)
	public ProfitCenter profitCenter;
	
	/** The job. */
	@ManyToOne
	@JoinColumn(name= "\"mf19MJobmast_mf19_jobCode\"" , nullable = false)
	public Job job;
	
	/**
	 * Gets the ps id.
	 *
	 * @return the ps id
	 */
	public String getPsId() {
		return psId;
	}

	/**
	 * Sets the ps id.
	 *
	 * @param psId the new ps id
	 */
	public void setPsId(String psId) {
		this.psId = psId;
	}

	/**
	 * Gets the alt id.
	 *
	 * @return the alt id
	 */
	public String getAltId() {
		return altId;
	}

	/**
	 * Sets the alt id.
	 *
	 * @param altId the new alt id
	 */
	public void setAltId(String altId) {
		this.altId = altId;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the pref name.
	 *
	 * @return the pref name
	 */
	public String getPrefName() {
		return prefName;
	}

	/**
	 * Sets the pref name.
	 *
	 * @param prefName the new pref name
	 */
	public void setPrefName(String prefName) {
		this.prefName = prefName;
	}

	/**
	 * Gets the new ic no.
	 *
	 * @return the new ic no
	 */
	public String getNewIcNo() {
		return newIcNo;
	}

	/**
	 * Sets the new ic no.
	 *
	 * @param newIcNo the new new ic no
	 */
	public void setNewIcNo(String newIcNo) {
		this.newIcNo = newIcNo;
	}

	/**
	 * Gets the old ic no.
	 *
	 * @return the old ic no
	 */
	public String getOldIcNo() {
		return oldIcNo;
	}

	/**
	 * Sets the old ic no.
	 *
	 * @param oldIcNo the new old ic no
	 */
	public void setOldIcNo(String oldIcNo) {
		this.oldIcNo = oldIcNo;
	}

	/**
	 * Gets the passport no.
	 *
	 * @return the passport no
	 */
	public String getPassportNo() {
		return passportNo;
	}

	/**
	 * Sets the passport no.
	 *
	 * @param passportNo the new passport no
	 */
	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}

	/**
	 * Gets the gender.
	 *
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * Sets the gender.
	 *
	 * @param gender the new gender
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * Gets the ethnic.
	 *
	 * @return the ethnic
	 */
	public String getEthnic() {
		return ethnic;
	}

	/**
	 * Sets the ethnic.
	 *
	 * @param ethnic the new ethnic
	 */
	public void setEthnic(String ethnic) {
		this.ethnic = ethnic;
	}

	/**
	 * Gets the dob.
	 *
	 * @return the dob
	 */
	public Date getDob() {
		return dob;
	}

	/**
	 * Sets the dob.
	 *
	 * @param dob the new dob
	 */
	public void setDob(Date dob) {
		this.dob = dob;
	}

	/**
	 * Gets the foreign local.
	 *
	 * @return the foreign local
	 */
	public String getForeignLocal() {
		return foreignLocal;
	}

	/**
	 * Sets the foreign local.
	 *
	 * @param foreignLocal the new foreign local
	 */
	public void setForeignLocal(String foreignLocal) {
		this.foreignLocal = foreignLocal;
	}

	/**
	 * Gets the marital status.
	 *
	 * @return the marital status
	 */
	public String getMaritalStatus() {
		return maritalStatus;
	}

	/**
	 * Sets the marital status.
	 *
	 * @param maritalStatus the new marital status
	 */
	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	/**
	 * Gets the phone no.
	 *
	 * @return the phone no
	 */
	public String getPhoneNo() {
		return phoneNo;
	}

	/**
	 * Sets the phone no.
	 *
	 * @param phoneNo the new phone no
	 */
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	/**
	 * Gets the ext.
	 *
	 * @return the ext
	 */
	public String getExt() {
		return ext;
	}

	/**
	 * Sets the ext.
	 *
	 * @param ext the new ext
	 */
	public void setExt(String ext) {
		this.ext = ext;
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the email.
	 *
	 * @param email the new email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Gets the hr status.
	 *
	 * @return the hr status
	 */
	public String getHrStatus() {
		return hrStatus;
	}

	/**
	 * Sets the hr status.
	 *
	 * @param hrStatus the new hr status
	 */
	public void setHrStatus(String hrStatus) {
		this.hrStatus = hrStatus;
	}

	/**
	 * Gets the payroll status.
	 *
	 * @return the payroll status
	 */
	public String getPayrollStatus() {
		return payrollStatus;
	}

	/**
	 * Sets the payroll status.
	 *
	 * @param payrollStatus the new payroll status
	 */
	public void setPayrollStatus(String payrollStatus) {
		this.payrollStatus = payrollStatus;
	}

	/**
	 * Gets the pos no.
	 *
	 * @return the pos no
	 */
	public String getPosNo() {
		return posNo;
	}

	/**
	 * Sets the pos no.
	 *
	 * @param posNo the new pos no
	 */
	public void setPosNo(String posNo) {
		this.posNo = posNo;
	}

	/**
	 * Gets the pos title.
	 *
	 * @return the pos title
	 */
	public String getPosTitle() {
		return posTitle;
	}

	/**
	 * Sets the pos title.
	 *
	 * @param posTitle the new pos title
	 */
	public void setPosTitle(String posTitle) {
		this.posTitle = posTitle;
	}

	/**
	 * Gets the code HRIS.
	 *
	 * @return the code HRIS
	 */
	public String getCodeHRIS() {
		return codeHRIS;
	}

	/**
	 * Sets the code HRIS.
	 *
	 * @param codeHRIS the new code HRIS
	 */
	public void setCodeHRIS(String codeHRIS) {
		this.codeHRIS = codeHRIS;
	}

	/**
	 * Gets the desc HRIS.
	 *
	 * @return the desc HRIS
	 */
	public String getDescHRIS() {
		return descHRIS;
	}

	/**
	 * Sets the desc HRIS.
	 *
	 * @param descHRIS the new desc HRIS
	 */
	public void setDescHRIS(String descHRIS) {
		this.descHRIS = descHRIS;
	}

	/**
	 * Gets the loc.
	 *
	 * @return the loc
	 */
	public String getLoc() {
		return loc;
	}

	/**
	 * Sets the loc.
	 *
	 * @param loc the new loc
	 */
	public void setLoc(String loc) {
		this.loc = loc;
	}

	/**
	 * Gets the loc desc.
	 *
	 * @return the loc desc
	 */
	public String getLocDesc() {
		return locDesc;
	}

	/**
	 * Sets the loc desc.
	 *
	 * @param locDesc the new loc desc
	 */
	public void setLocDesc(String locDesc) {
		this.locDesc = locDesc;
	}

	/**
	 * Gets the salary plan.
	 *
	 * @return the salary plan
	 */
	public String getSalaryPlan() {
		return salaryPlan;
	}

	/**
	 * Sets the salary plan.
	 *
	 * @param salaryPlan the new salary plan
	 */
	public void setSalaryPlan(String salaryPlan) {
		this.salaryPlan = salaryPlan;
	}

	/**
	 * Gets the salary grade.
	 *
	 * @return the salary grade
	 */
	public String getSalaryGrade() {
		return salaryGrade;
	}

	/**
	 * Sets the salary grade.
	 *
	 * @param salaryGrade the new salary grade
	 */
	public void setSalaryGrade(String salaryGrade) {
		this.salaryGrade = salaryGrade;
	}

	/**
	 * Gets the rpt to pos no.
	 *
	 * @return the rpt to pos no
	 */
	public String getRptToPosNo() {
		return rptToPosNo;
	}

	/**
	 * Sets the rpt to pos no.
	 *
	 * @param rptToPosNo the new rpt to pos no
	 */
	public void setRptToPosNo(String rptToPosNo) {
		this.rptToPosNo = rptToPosNo;
	}

	/**
	 * Gets the rpt to emp id.
	 *
	 * @return the rpt to emp id
	 */
	public String getRptToEmpId() {
		return rptToEmpId;
	}

	/**
	 * Sets the rpt to emp id.
	 *
	 * @param rptToEmpId the new rpt to emp id
	 */
	public void setRptToEmpId(String rptToEmpId) {
		this.rptToEmpId = rptToEmpId;
	}

	/**
	 * Gets the payroll comp.
	 *
	 * @return the payroll comp
	 */
	public String getPayrollComp() {
		return payrollComp;
	}

	/**
	 * Sets the payroll comp.
	 *
	 * @param payrollComp the new payroll comp
	 */
	public void setPayrollComp(String payrollComp) {
		this.payrollComp = payrollComp;
	}

	/**
	 * Gets the badge no.
	 *
	 * @return the badge no
	 */
	public String getBadgeNo() {
		return badgeNo;
	}

	/**
	 * Sets the badge no.
	 *
	 * @param badgeNo the new badge no
	 */
	public void setBadgeNo(String badgeNo) {
		this.badgeNo = badgeNo;
	}

	/**
	 * Gets the hire date.
	 *
	 * @return the hire date
	 */
	public Date getHireDate() {
		return hireDate;
	}

	/**
	 * Sets the hire date.
	 *
	 * @param hireDate the new hire date
	 */
	public void setHireDate(Date hireDate) {
		this.hireDate = hireDate;
	}

	/**
	 * Gets the confirm date.
	 *
	 * @return the confirm date
	 */
	public Date getConfirmDate() {
		return confirmDate;
	}

	/**
	 * Sets the confirm date.
	 *
	 * @param confirmDate the new confirm date
	 */
	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	/**
	 * Gets the last date work.
	 *
	 * @return the last date work
	 */
	public Date getLastDateWork() {
		return lastDateWork;
	}

	/**
	 * Sets the last date work.
	 *
	 * @param lastDateWork the new last date work
	 */
	public void setLastDateWork(Date lastDateWork) {
		this.lastDateWork = lastDateWork;
	}

	/**
	 * Gets the trf date.
	 *
	 * @return the trf date
	 */
	public Date getTrfDate() {
		return trfDate;
	}

	/**
	 * Sets the trf date.
	 *
	 * @param trfDate the new trf date
	 */
	public void setTrfDate(Date trfDate) {
		this.trfDate = trfDate;
	}

	/**
	 * Gets the exp job end date.
	 *
	 * @return the exp job end date
	 */
	public Date getExpJobEndDate() {
		return expJobEndDate;
	}

	/**
	 * Sets the exp job end date.
	 *
	 * @param expJobEndDate the new exp job end date
	 */
	public void setExpJobEndDate(Date expJobEndDate) {
		this.expJobEndDate = expJobEndDate;
	}

	/**
	 * Gets the from comp code.
	 *
	 * @return the from comp code
	 */
	public String getFromCompCode() {
		return fromCompCode;
	}

	/**
	 * Sets the from comp code.
	 *
	 * @param fromCompCode the new from comp code
	 */
	public void setFromCompCode(String fromCompCode) {
		this.fromCompCode = fromCompCode;
	}

	/**
	 * Gets the to comp code.
	 *
	 * @return the to comp code
	 */
	public String getToCompCode() {
		return toCompCode;
	}

	/**
	 * Sets the to comp code.
	 *
	 * @param toCompCode the new to comp code
	 */
	public void setToCompCode(String toCompCode) {
		this.toCompCode = toCompCode;
	}

	/**
	 * Gets the from loc.
	 *
	 * @return the from loc
	 */
	public String getFromLoc() {
		return fromLoc;
	}

	/**
	 * Sets the from loc.
	 *
	 * @param fromLoc the new from loc
	 */
	public void setFromLoc(String fromLoc) {
		this.fromLoc = fromLoc;
	}

	/**
	 * Gets the to loc.
	 *
	 * @return the to loc
	 */
	public String getToLoc() {
		return toLoc;
	}

	/**
	 * Sets the to loc.
	 *
	 * @param toLoc the new to loc
	 */
	public void setToLoc(String toLoc) {
		this.toLoc = toLoc;
	}

	/**
	 * Gets the trf send flag.
	 *
	 * @return the trf send flag
	 */
	public String getTrfSendFlag() {
		return trfSendFlag;
	}

	/**
	 * Sets the trf send flag.
	 *
	 * @param trfSendFlag the new trf send flag
	 */
	public void setTrfSendFlag(String trfSendFlag) {
		this.trfSendFlag = trfSendFlag;
	}

	/**
	 * Gets the emp cat.
	 *
	 * @return the emp cat
	 */
	public String getEmpCat() {
		return empCat;
	}

	/**
	 * Sets the emp cat.
	 *
	 * @param empCat the new emp cat
	 */
	public void setEmpCat(String empCat) {
		this.empCat = empCat;
	}

	/**
	 * Gets the last update time.
	 *
	 * @return the last update time
	 */
	public Timestamp getLastUpdateTime() {
		return lastUpdateTime;
	}

	/**
	 * Sets the last update time.
	 *
	 * @param lastUpdateTime the new last update time
	 */
	public void setLastUpdateTime(Timestamp lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}

	/**
	 * Gets the prc time.
	 *
	 * @return the prc time
	 */
	public Timestamp getPrcTime() {
		return prcTime;
	}

	/**
	 * Sets the prc time.
	 *
	 * @param prcTime the new prc time
	 */
	public void setPrcTime(Timestamp prcTime) {
		this.prcTime = prcTime;
	}

	/**
	 * Gets the paygrp.
	 *
	 * @return the paygrp
	 */
	public String getPaygrp() {
		return paygrp;
	}

	/**
	 * Sets the paygrp.
	 *
	 * @param paygrp the new paygrp
	 */
	public void setPaygrp(String paygrp) {
		this.paygrp = paygrp;
	}

	/**
	 * Gets the to profit centre.
	 *
	 * @return the to profit centre
	 */
	public String getToProfitCentre() {
		return toProfitCentre;
	}

	/**
	 * Sets the to profit centre.
	 *
	 * @param toProfitCentre the new to profit centre
	 */
	public void setToProfitCentre(String toProfitCentre) {
		this.toProfitCentre = toProfitCentre;
	}

	/**
	 * Gets the to cost charge company.
	 *
	 * @return the to cost charge company
	 */
	public String getToCostChargeCompany() {
		return toCostChargeCompany;
	}

	/**
	 * Sets the to cost charge company.
	 *
	 * @param toCostChargeCompany the new to cost charge company
	 */
	public void setToCostChargeCompany(String toCostChargeCompany) {
		this.toCostChargeCompany = toCostChargeCompany;
	}

	/**
	 * Gets the from profit centre.
	 *
	 * @return the from profit centre
	 */
	public String getFromProfitCentre() {
		return fromProfitCentre;
	}

	/**
	 * Sets the from profit centre.
	 *
	 * @param fromProfitCentre the new from profit centre
	 */
	public void setFromProfitCentre(String fromProfitCentre) {
		this.fromProfitCentre = fromProfitCentre;
	}

	/**
	 * Gets the from cost charge company.
	 *
	 * @return the from cost charge company
	 */
	public String getFromCostChargeCompany() {
		return fromCostChargeCompany;
	}

	/**
	 * Sets the from cost charge company.
	 *
	 * @param fromCostChargeCompany the new from cost charge company
	 */
	public void setFromCostChargeCompany(String fromCostChargeCompany) {
		this.fromCostChargeCompany = fromCostChargeCompany;
	}

	/**
	 * Checks if is active.
	 *
	 * @return true, if is active
	 */
	public boolean isActive() {
		return isActive;
	}

	/**
	 * Sets the active.
	 *
	 * @param isActive the new active
	 */
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	/**
	 * Gets the created on.
	 *
	 * @return the created on
	 */
	public Timestamp getCreatedOn() {
		return createdOn;
	}

	/**
	 * Sets the created on.
	 *
	 * @param createdOn the new created on
	 */
	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * Gets the created by.
	 *
	 * @return the created by
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * Sets the created by.
	 *
	 * @param createdBy the new created by
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Gets the last modified on.
	 *
	 * @return the last modified on
	 */
	public Timestamp getLastModifiedOn() {
		return lastModifiedOn;
	}

	/**
	 * Sets the last modified on.
	 *
	 * @param lastModifiedOn the new last modified on
	 */
	public void setLastModifiedOn(Timestamp lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

	/**
	 * Gets the last modified by.
	 *
	 * @return the last modified by
	 */
	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	/**
	 * Sets the last modified by.
	 *
	 * @param lastModifiedBy the new last modified by
	 */
	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	/**
	 * Gets the company.
	 *
	 * @return the company
	 */
	public Company getCompany() {
		return company;
	}

	/**
	 * Sets the company.
	 *
	 * @param company the new company
	 */
	public void setCompany(Company company) {
		this.company = company;
	}

	/**
	 * Gets the profit center.
	 *
	 * @return the profit center
	 */
	public ProfitCenter getProfitCenter() {
		return profitCenter;
	}

	/**
	 * Sets the profit center.
	 *
	 * @param profitCenter the new profit center
	 */
	public void setProfitCenter(ProfitCenter profitCenter) {
		this.profitCenter = profitCenter;
	}

	/**
	 * Gets the job.
	 *
	 * @return the job
	 */
	public Job getJob() {
		return job;
	}

	/**
	 * Sets the job.
	 *
	 * @param job the new job
	 */
	public void setJob(Job job) {
		this.job = job;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		return this.psId + "-" + this.prefName;
	}
	
	
	public String isActiveString() {
						
		String status = String.valueOf(isActive()).toUpperCase();		
		return status ;
	}
	
	/**
	 * Profit center print.
	 *
	 * @return the string
	 */
	public String profitCenterPrint() {
		return this.psId + " - " + this.name + "(" + this.posTitle + ")";
	}
}