package my.com.parkson.ntpoms.main.services.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import groovy.util.logging.Slf4j;
import lombok.RequiredArgsConstructor;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.services.MessageService;
import my.com.parkson.ntpoms.main.entities.VendorBranch;
import my.com.parkson.ntpoms.main.entities.VendorCreditTerms;
import my.com.parkson.ntpoms.main.entities.VendorCreditTermsId;
import my.com.parkson.ntpoms.main.repositories.VendorCreditTermsRepository;
import my.com.parkson.ntpoms.main.services.VendorCreditTermsService;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class VendorCreditTermsServiceImpl implements VendorCreditTermsService {

	private static final Logger LOGGER = Logger.getLogger(VendorCreditTermsServiceImpl.class);
	@Autowired
	private VendorCreditTermsRepository vendorCreditTermsRepository;

	/** The messages. */
	@Autowired
	private MessageService messages;

	/*
	 * @Override public List<VendorCreditTerms> getAllVendorCreditTerms() {
	 * List<VendorCreditTerms> vendorCreditTermsList = new ArrayList<>(); try {
	 * vendorCreditTermsList =
	 * vendorCreditTermsRepository.findAllByOrderByLastmodifiedonDesc(); } catch
	 * (Exception e) { log.log(DEBUG_ERROR, DEBUG_DES, e); } return
	 * vendorCreditTermsList; }
	 * 
	 * @Override public List<VendorCreditTerms> getAllActiveVendorCreditTerms()
	 * { List<VendorCreditTerms> vendorCreditTermsList = new ArrayList<>(); try
	 * { vendorCreditTermsList =
	 * vendorCreditTermsRepository.findAllByIsActiveOrderByLastmodifiedonDesc(
	 * true); } catch (Exception e) { log.log(DEBUG_ERROR, DEBUG_DES, e); }
	 * return vendorCreditTermsList; }
	 */

	@Override
	public void createVendorCreditTerms(VendorCreditTerms vendorCreditTerms) {
		try {
			vendorCreditTermsRepository.save(vendorCreditTerms);
		} catch (NonUniqueResultException e) {
			LOGGER.error(e);
		} catch (PersistenceException e) {
			LOGGER.error(e);
		} catch (POException e) {
			LOGGER.error(e);
		}
	}

	@Override
	public List<VendorCreditTerms> getVendorCreditTermsById(String creditTermsCode) {
		List<VendorCreditTerms> vendorList = new ArrayList<>();
		try {
			vendorList = vendorCreditTermsRepository.findByVendcrtIdCreditTermsCode(creditTermsCode);

		} catch (Exception e) {
			LOGGER.error(e);
		}
		return vendorList;
	}

	/*
	 * @Override public List<String> getVendorCreditTermsByVendorCode(String
	 * vendorCode) { List<String> vendorCreditTermsList = new ArrayList<>(); try
	 * { vendorCreditTermsList =
	 * vendorCreditTermsRepository.findByVendorCodeOrderByLastmodifiedonDesc(
	 * vendorCode); } catch (Exception e) { log.log(DEBUG_ERROR, DEBUG_DES, e);
	 * } return vendorCreditTermsList; }
	 */

	@Override
	public VendorCreditTerms getVendorCreditTermsByVendorCodeAndCreditTermsCodeAndCompanyCodeAndVendorBranchCode(
			String vendorCode, String creditTermsCode, String companyCode, String vendorBranchCode) {
		try {
			VendorCreditTermsId priKey = new VendorCreditTermsId(vendorCode, vendorBranchCode, creditTermsCode,
					companyCode);
			// return
			// vendorCreditTermsRepository.findByVendorCodeAndCreditTermsCodeAndCompanyCodeAndVendorBranchCode(priKey);
			return vendorCreditTermsRepository.findByVendcrtId(priKey);
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return null;
	}

	@Override
	public Page<VendorCreditTerms> getPagebleAll(Specification<VendorCreditTerms> s, Pageable req) {
		return vendorCreditTermsRepository.findAll(s, req);
	}

	@Override
	public Page<VendorCreditTerms> getAllPageableVendCrtOrdered(Pageable pageRequest) {

		return vendorCreditTermsRepository.findAllByOrderByLastModifiedOnDesc(pageRequest);
	}

	@Override
	public List<VendorCreditTerms> getVendorCreditTermsByVendorCodeAndCreditTermsCodeAndVendorBranchCode(
			String vendorCode, String creditTermsCode, String vendorBranchCode) {
		List<VendorCreditTerms> vendorList = new ArrayList<>();

		try {

			vendorList = vendorCreditTermsRepository
					.findByVendcrtIdVendorCodeAndVendcrtIdCreditTermsCodeAndVendcrtIdVendorBranchCode(vendorCode,
							creditTermsCode, vendorBranchCode);
			System.out.println("in service " + vendorList);

		} catch (Exception e) {
			LOGGER.error(e);
		}
		return vendorList;
	}

	@Override
	public VendorCreditTerms getVendorCreditTermsById(VendorCreditTermsId vendorCreditTermsId) {

		VendorCreditTerms vendorcrtList = null;
		try {
			vendorcrtList = vendorCreditTermsRepository.findByVendcrtId(vendorCreditTermsId);
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return vendorcrtList;
	}

	@Override
	public List<VendorCreditTerms> getVendorCreditTermsByVendorCodeAndVendorBranchCodeAndCompanyCode(String vendorCode,
			String vendorBranchCode, String companyCode) {
		List<VendorCreditTerms> vendorList = new ArrayList<>();
		try {

			vendorList = vendorCreditTermsRepository
					.findByVendcrtIdVendorCodeAndVendcrtIdVendorBranchCodeAndVendcrtIdCompanyCodeOrderByLastModifiedOnDesc(
							vendorCode, vendorBranchCode, companyCode);

		} catch (Exception e) {
			LOGGER.error(e);
		}
		return vendorList;
	}

	@Override
	public long getCount() {
		return vendorCreditTermsRepository.count();
	}

	@Override
	public long getCountActive() {
		List<VendorCreditTerms> vendorList = new ArrayList<>();
		vendorList = vendorCreditTermsRepository.findAllByIsActiveOrderByLastModifiedOnDesc(true);
		return vendorList.size();
	}

}