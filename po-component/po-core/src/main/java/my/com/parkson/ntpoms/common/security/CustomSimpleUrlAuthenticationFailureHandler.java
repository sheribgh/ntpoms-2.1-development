package my.com.parkson.ntpoms.common.security;

import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.common.entities.EmployeeMaster;
import my.com.parkson.ntpoms.common.entities.PurchaseOrderUser;
import my.com.parkson.ntpoms.common.entities.User;
import my.com.parkson.ntpoms.common.entities.UserRole;
import my.com.parkson.ntpoms.common.services.PurchaseOrderUserService;
import my.com.parkson.ntpoms.common.services.UsersService;
import my.com.parkson.ntpoms.main.services.PurchaseOrderService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.authentication.session.SessionAuthenticationException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The Class CustomSimpleUrlAuthenticationFailureHandler.
 */
@Slf4j
public class CustomSimpleUrlAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {

	@Autowired
	private PurchaseOrderUserService pouserser;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.security.web.authentication.
	 * SimpleUrlAuthenticationFailureHandler#onAuthenticationFailure(javax.servlet.
	 * http.HttpServletRequest, javax.servlet.http.HttpServletResponse,
	 * org.springframework.security.core.AuthenticationException)
	 */
	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException exception) throws IOException, ServletException {
		setDefaultFailureUrl("/login?error");

		PurchaseOrderUser pouser = new PurchaseOrderUser();
		String userid = request.getParameter("username");
		pouser = pouserser.getPurchaseOrderUserById(userid);

		if (pouser != null) {
			if (!pouser.isActive()) {
				setDefaultFailureUrl("/login?error=4");
			} else {
				if (exception.getClass().isAssignableFrom(BadCredentialsException.class)) {
					setDefaultFailureUrl("/login?error=1");
				} else if (exception.getClass().isAssignableFrom(DisabledException.class)) {
					setDefaultFailureUrl("/login?error=2");
				} else if (exception.getClass().isAssignableFrom(SessionAuthenticationException.class)) {
					setDefaultFailureUrl("/login?error=3");
				}

			}
		}
		 else {
				if (exception.getClass().isAssignableFrom(BadCredentialsException.class)) {
					setDefaultFailureUrl("/login?error=1");
				} else if (exception.getClass().isAssignableFrom(DisabledException.class)) {
					setDefaultFailureUrl("/login?error=2");
				} else if (exception.getClass().isAssignableFrom(SessionAuthenticationException.class)) {
					setDefaultFailureUrl("/login?error=3");
				}

			}
		super.onAuthenticationFailure(request, response, exception);
	}
}