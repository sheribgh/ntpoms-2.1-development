/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import my.com.parkson.ntpoms.common.entities.GoodsServicesTax;

/**
 * The Interface GoodsServicesTaxService.
 */
public interface GoodsServicesTaxService {

	/**
	 * Creates the goods services tax.
	 *
	 * @param goodsServicesTax the goods services tax
	 */
	public void createGoodsServicesTax(GoodsServicesTax goodsServicesTax);
	
	
	
	
	
	public long getCount();

	/**
	 * Gets the all pagable goods services tax.
	 *
	 * @param pageRequest the page request
	 * @return the all pagable goods services tax
	 */
	public Page<GoodsServicesTax> getAllPagableGoodsServicesTax(Pageable pageRequest);

	/**
	 * Gets the all pagable goods services tax.
	 *
	 * @param s the s
	 * @param pageRequest the page request
	 * @return the all pagable goods services tax
	 */
	public Page<GoodsServicesTax> getAllPagableGoodsServicesTax(Specification<GoodsServicesTax> s,
			Pageable pageRequest);
	
	
	/**
	 * Gets the all pagable goods services tax.
	 *
	 * @param pageRequest the page request
	 * @return the all pagable goods services tax
	 */
	public Page<GoodsServicesTax> getAllPagableGoodsServicesTaxOrdered(Pageable pageRequest);

	/**
	 * Gets the all pagable goods services tax.
	 *
	 * @param s the s
	 * @param pageRequest the page request
	 * @return the all pagable goods services tax
	 */
	public Page<GoodsServicesTax> getAllPagableGoodsServicesTaxOrdered(Specification<GoodsServicesTax> s,
			Pageable pageRequest);

	/**
	 * Gets the goods services tax by code.
	 *
	 * @param code the code
	 * @return the goods services tax by code
	 */
	public GoodsServicesTax getGoodsServicesTaxByCode(String code);

	/**
	 * Gets the all goods services tax.
	 *
	 * @return the all goods services tax
	 */
	public List<GoodsServicesTax> getAllGoodsServicesTax();

	/**
	 * Gets the all active goods services tax.
	 *
	 * @return the all active goods services tax
	 */
	public List<GoodsServicesTax> getAllActiveGoodsServicesTax();
}