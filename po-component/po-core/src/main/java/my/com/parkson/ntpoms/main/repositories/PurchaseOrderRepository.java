/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.main.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import my.com.parkson.ntpoms.main.entities.PurchaseOrder;

/**
 * The Interface PurchaseOrderRepository.
 */
public interface PurchaseOrderRepository
		extends JpaRepository<PurchaseOrder, String>, JpaSpecificationExecutor<PurchaseOrder> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.data.jpa.repository.JpaRepository#findAll()
	 */
	public List<PurchaseOrder> findAll();

	/**
	 * Find by po no.
	 *
	 * @param poNo
	 *            the po no
	 * @return the purchase order
	 */
	public PurchaseOrder findByPoNo(String poNo);

	/**
	 * Find by status.
	 *
	 * @param status
	 *            the status
	 * @return the list
	 */
	public List<PurchaseOrder> findByStatus(String status);

	/**
	 * Find by pc code.
	 *
	 * @param pcCode
	 *            the pc code
	 * @return the list
	 */
	public List<PurchaseOrder> findByPcCode(String pcCode);

	public List<PurchaseOrder> findByPcCodeOrderByPoNoDesc(String pcCode);

	/**
	 * Find all non draft.
	 *
	 * @return the list
	 */
	@Query("select o from PurchaseOrder o where o.pcCode<> 'Draft'")
	public List<PurchaseOrder> findAllNonDraft();

	@Query(value = "select o from PurchaseOrder o where o.poNo like :poNo", countQuery = "select count(*) from PurchaseOrder")

	public List<PurchaseOrder> findByPoNoSingleParam(@Param("poNo") String pono, Pageable req);

	public Page<PurchaseOrder> findByPoNo(Specification<PurchaseOrder> s, Pageable pageRequest);

	public Page<PurchaseOrder> findByPoNo(Pageable pageRequest);

	@Query(value = "select p from PurchaseOrder p where p.status=:status and p.lastModifiedOn>=:date")
	public List<PurchaseOrder> getPurchaseOrderExpiryDetails(@Param("date") Date date, @Param("status") String status);

}