package my.com.parkson.ntpoms.main.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import my.com.parkson.ntpoms.main.entities.CreditTerms;
import my.com.parkson.ntpoms.main.entities.CreditTermsId;
import my.com.parkson.ntpoms.main.entities.VendorBranch;
import my.com.parkson.ntpoms.main.entities.VendorBranchId;

public interface CreditTermsService {
	
	public void createCreditTerms(CreditTerms creditTerms);	
	public CreditTerms getCreditTermsByCode(String code);	
	public List<CreditTerms> getAllCreditTerms();	
	public List<CreditTerms> getAllActiveCreditTerms();
	Page<CreditTerms> getPagebleAll(Pageable req);
	
	public Page<CreditTerms> getAllPagableCrt(Specification<CreditTerms> s,Pageable pageRequest);
	public Page<CreditTerms> getAllPageableCrtOrdered(Pageable pageRequest);
	public long getCount();
	
	
	public long getCountActive();
	
	public CreditTerms getCreditTermsbyCrtId(CreditTermsId pkey);
}