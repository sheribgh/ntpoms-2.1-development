/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services.impl;

import static my.com.parkson.ntpoms.common.MessageCodes.FAILED_PERSIST_EXCEPTION;
import static my.com.parkson.ntpoms.common.MessageCodes.FAILED_FETCH_RECORDS_EXCEPTION;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.LogMessages;
import my.com.parkson.ntpoms.common.entities.AccessLogSetting;
import my.com.parkson.ntpoms.common.repositories.AccessLogSettingRepository;
import my.com.parkson.ntpoms.common.services.AccessLogService;
import my.com.parkson.ntpoms.common.services.MessageService;
import my.com.parkson.ntpoms.common.services.AccessLogSettingService;

import java.util.List;

/**
 * The Class AccessLogSettingServiceImpl.
 */
@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class AccessLogSettingServiceImpl implements AccessLogSettingService {

	/** The log. */
	@Autowired
	private AccessLogService log;
	
	/** The messages. */
	@Autowired
    private MessageService messages;

	/** The staging log repository. */
	@Autowired
	private AccessLogSettingRepository stagingLogRepository;

	/* (non-Javadoc)
	 * @see my.com.parkson.ntpoms.common.services.AccessLogSettingService#getLastStagingLog()
	 */
	@Override
	public AccessLogSetting getLastStagingLog() {
		AccessLogSetting lastSetting = null;
		try {
			List<AccessLogSetting> stagingLogList = stagingLogRepository.findAll();
			if (stagingLogList != null && !stagingLogList.isEmpty()) {
				lastSetting = (stagingLogList.get(stagingLogList.size() - 1));
			}
		} catch (POException e) {
			log.log(LogMessages.ERROR.value(), messages.getMessage(FAILED_FETCH_RECORDS_EXCEPTION), e);
		}
		return lastSetting;
	}

	/* (non-Javadoc)
	 * @see my.com.parkson.ntpoms.common.services.AccessLogSettingService#createStagingLog(my.com.parkson.ntpoms.common.entities.AccessLogSetting)
	 */
	@Override
	public void createStagingLog(AccessLogSetting stagingLog) {
		try {
			stagingLogRepository.save(stagingLog);
		} catch (POException e) {
			log.log(LogMessages.ERROR.value(), messages.getMessage(FAILED_PERSIST_EXCEPTION), e);
		}
	}

	/* (non-Javadoc)
	 * @see my.com.parkson.ntpoms.common.services.AccessLogSettingService#getAllStagingLog()
	 */
	@Override
	public List<AccessLogSetting> getAllStagingLog() {
		try {
			return stagingLogRepository.findAll();
		} catch (POException e) {
			log.log(LogMessages.ERROR.value(), messages.getMessage(FAILED_FETCH_RECORDS_EXCEPTION), e);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see my.com.parkson.ntpoms.common.services.AccessLogSettingService#getAllPageableStagingLog(org.springframework.data.domain.Pageable)
	 */
	@Override
	public Page<AccessLogSetting> getAllPageableStagingLog(Pageable pageRequest) {
		return stagingLogRepository.findAll(pageRequest);
	}
}