package my.com.parkson.ntpoms.main.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import my.com.parkson.ntpoms.main.entities.CreditTerms;
import my.com.parkson.ntpoms.main.entities.CreditTermsId;
import my.com.parkson.ntpoms.main.entities.VendorBranch;
import my.com.parkson.ntpoms.main.entities.VendorBranchId;




public interface CreditTermsRepository extends JpaRepository<CreditTerms, Integer>, JpaSpecificationExecutor<CreditTerms> {

	public CreditTerms findByCrtIdCode(String code);	
	/*public List<CreditTerms> findAllByOrderByLastmodifiedonDesc();*/
	public List<CreditTerms> findAllByIsActiveOrderByLastModifiedOnDesc(boolean isActive);
	public Page<CreditTerms> findAllByOrderByLastModifiedOnDesc(Pageable pageRequest);
	
	public CreditTerms findByCrtId(CreditTermsId CreditTermsId);
}
