package my.com.parkson.ntpoms.main.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.main.entities.Invoice;
import my.com.parkson.ntpoms.main.repositories.InvoiceRepository;
import my.com.parkson.ntpoms.main.services.AccessLogsService;
import my.com.parkson.ntpoms.main.services.InvoiceService;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class InvoiceServiceImpl implements InvoiceService {

	private static final Logger LOGGER = Logger.getLogger(InvoiceServiceImpl.class);

	@Autowired
	private InvoiceRepository invoiceRepository;

	@Override
	public void removeInvoice(Invoice invoice) {
		try {
			invoiceRepository.delete(invoice);
		} catch (Exception e) {
			LOGGER.error(e);
		}
	}

	@Override
	public void createInvoice(Invoice invoice) {
		try {
			invoiceRepository.save(invoice);
		} catch (Exception e) {
			LOGGER.error(e);
		}
	}

	@Override
	public List<Invoice> getInvoiceByPurchaseOrderNo(String poNo) {
		List<Invoice> invoiceList = new ArrayList<>();
		try {
			invoiceList = invoiceRepository.findAllByPurchaseOrderNo(poNo);
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return invoiceList;
	}

	@Override
	public Invoice getInvoiceByInvoiceNo(String invoiceNo) {
		try {
			return invoiceRepository.findByInvoiceNo(invoiceNo);
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return null;
	}

	@Override
	public List<Invoice> getAllInvoice() {
		List<Invoice> invoiceList = new ArrayList<>();
		try {
			invoiceList = invoiceRepository.findAll();
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return invoiceList;
	}

	@Override
	public Invoice getInvoiceByPoInvoiceNo(String poInvoiceNo) {
		try {
			return invoiceRepository.findByPoInvoiceNo(poInvoiceNo);
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return null;
	}

	@Override
	public Page<Invoice> getAllPagableInvoice(Specification<Invoice> s, Pageable pageRequest) {
		return invoiceRepository.findAll(s, pageRequest);
	}

	@Override
	public List<Invoice> getInvoiceByPONo(String poNo) {
		List<Invoice> invoiceList = new ArrayList<>();
		try {
			invoiceList = invoiceRepository.findAllByPurchaseOrder(poNo);
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return invoiceList;
	}
}