package my.com.parkson.ntpoms.main.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "PO02_T_ITEMINFO")
@Data
@NoArgsConstructor
public class ItemInformation implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "po02_itemNo")
	private String itemNo;

	@Column(name = "po02_particulars")
	private String particulars;

	@Column(name = "po02_packing")
	private String packing;

	@Column(name = "mf01mvendorbranch_po01_vendorcode")
	private String vendorCode;
	
	@Column(name = "mf01mvendorbranch_po01_vendorbranchcode")
	private String vendorBranchCode;

	public String getItemNo() {
		return itemNo;
	}


	public void setItemNo(String itemNo) {
		this.itemNo = itemNo;
	}


	public String getParticulars() {
		return particulars;
	}


	public void setParticulars(String particulars) {
		this.particulars = particulars;
	}


	public String getPacking() {
		return packing;
	}


	public void setPacking(String packing) {
		this.packing = packing;
	}


	public String getVendorCode() {
		return vendorCode;
	}


	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}


	public String getVendorBranchCode() {
		return vendorBranchCode;
	}


	public void setVendorBranchCode(String vendorBranchCode) {
		this.vendorBranchCode = vendorBranchCode;
	}

	
	@Override
	public String toString() {		
		if (particulars == null || particulars.isEmpty()) {
			return itemNo;
		}		
		return itemNo + " - " + particulars;
	}
}