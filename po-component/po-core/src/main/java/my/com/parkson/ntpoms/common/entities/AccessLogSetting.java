/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The Class AccessLogSetting.
 */
@Entity
@Table(name="\"LD03_T_STGLOG\"")
@Data
@NoArgsConstructor
public class AccessLogSetting implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ld03_t_stglog_sequence")
	@SequenceGenerator(name = "ld03_t_stglog_sequence", sequenceName = "ld03_t_stglog_sequence", 
			allocationSize = 1)
	@Column(name = "\"ld03_logSettingId\"")
	private int id;

	/** The setting. */
	@Column(name = "\"ld03_logSetting\"")
	private int setting;

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Gets the setting.
	 *
	 * @return the setting
	 */
	public int getSetting() {
		return setting;
	}

	/**
	 * Sets the setting.
	 *
	 * @param setting the new setting
	 */
	public void setSetting(int setting) {
		this.setting = setting;
	}
}