package my.com.parkson.ntpoms.main.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="\"LD03_T_STGLOG\"")
public class AccessLogsSetting implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ld03_t_stglog_sequence")
	@SequenceGenerator(name = "ld03_t_stglog_sequence", sequenceName = "ld03_t_stglog_sequence", 
	allocationSize = 1)
	@Column(name = "\"ld03_logSettingId\"")
	private int id;

	@Column(name = "\"ld03_logSetting\"")
	private int setting;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getSetting() {
		return setting;
	}

	public void setSetting(int setting) {
		this.setting = setting;
	}
}