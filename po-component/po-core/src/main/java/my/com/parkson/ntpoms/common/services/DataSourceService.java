/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import my.com.parkson.ntpoms.common.entities.DataSource;

/**
 * The Interface DataSourceService.
 */
public interface DataSourceService {
	
	/**
	 * Creates the data source.
	 *
	 * @param dataSource the data source
	 * @return true, if successful
	 */
	public boolean createDataSource(DataSource dataSource);
	
	/**
	 * Gets the data source by company.
	 *
	 * @param companyCode the company code
	 * @return the data source by company
	 */
	public DataSource getDataSourceByCompany(String companyCode);	
	
	/**
	 * Gets the data source by company and year.
	 *
	 * @param companyCode the company code
	 * @param companyYear the company year
	 * @return the data source by company and year
	 */
	public DataSource getDataSourceByCompanyAndYear(String companyCode, String companyYear);	
	
	/**
	 * Gets the active data source.
	 *
	 * @param currentYear the current year
	 * @return the active data source
	 */
	public List<DataSource> getActiveDataSource(String currentYear);	
	
	/**
	 * Gets the all data source.
	 *
	 * @return the all data source
	 */
	public List<DataSource> getAllDataSource();
	
	/**
	 * Gets the all pageable data source.
	 *
	 * @param pageRequest the page request
	 * @return the all pageable data source
	 */
	public Page<DataSource> getAllPageableDataSource(Pageable pageRequest);
}