package my.com.parkson.ntpoms.main.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="\"po04_t_useraccess\"")
@Data
@NoArgsConstructor
public class PurchaseOrderUserAccess implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	PurchaseOrderUserAccessPk purchaseorderuseraccessId;

	public PurchaseOrderUserAccessPk getPurchaseorderuseraccessId() {
		return purchaseorderuseraccessId;
	}

	public void setPurchaseorderuseraccessId(PurchaseOrderUserAccessPk purchaseorderuseraccessId) {
		this.purchaseorderuseraccessId = purchaseorderuseraccessId;
	}
	
	

	/*
	@Column(name = "po04_myPOs")
	private String myPOS;

	@Column(name = "po04_toBeVerified")
	private String toBeVerified;

	@Column(name = "po04_toBeAuthorized")
	private String toBeAuthorized;

	@Column(name = "po04_draft")
	private String draft;

	@Column(name = "po04_redraft")
	private String redraft;

	@Column(name = "po04_toBeInvoiced")
	private String toBeInvoiced;

	@Column(name = "po04_viewRights")
	private String viewRights;
	*/

	

/*	
	public String getMyPOS() {
		return myPOS;
	}

	public void setMyPOS(String myPOS) {
		this.myPOS = myPOS;
	}

	public String getToBeVerified() {
		return toBeVerified;
	}

	public void setToBeVerified(String toBeVerified) {
		this.toBeVerified = toBeVerified;
	}

	public String getToBeAuthorized() {
		return toBeAuthorized;
	}

	public void setToBeAuthorized(String toBeAuthorized) {
		this.toBeAuthorized = toBeAuthorized;
	}

	public String getDraft() {
		return draft;
	}

	public void setDraft(String draft) {
		this.draft = draft;
	}

	public String getRedraft() {
		return redraft;
	}

	public void setRedraft(String redraft) {
		this.redraft = redraft;
	}

	public String getToBeInvoiced() {
		return toBeInvoiced;
	}

	public void setToBeInvoiced(String toBeInvoiced) {
		this.toBeInvoiced = toBeInvoiced;
	}

	public String getViewRights() {
		return viewRights;
	}

	public void setViewRights(String viewRights) {
		this.viewRights = viewRights;
	}
	*/	
}
