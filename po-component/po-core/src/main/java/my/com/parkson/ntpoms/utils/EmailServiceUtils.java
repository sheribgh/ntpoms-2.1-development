package my.com.parkson.ntpoms.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.LogMessages;
import my.com.parkson.ntpoms.common.services.AccessLogService;

public class EmailServiceUtils {

	private static final Logger LOGGER = Logger.getLogger(EmailServiceUtils.class);

	public static Properties loadEmailPropertyFile() throws IOException {
		Properties props = System.getProperties();
		InputStream inputStream = null;
		try {
			props = System.getProperties();
			inputStream = EmailServiceUtils.class.getResourceAsStream("/mail-settings.properties");
			props.load(inputStream);
			inputStream.close();
		} catch (POException ex) {
			LOGGER.error(ex);
			throw new POException("Unable to load mail-settings properties file.", ex);
		} finally {
			try {
				if (inputStream != null) {
					inputStream.close();
				}
			} catch (IOException e) {
				LOGGER.error(e);
			}
		}
		return props;
	}

	public static String getContentType(String filename) {
		String extension = "";
		String contentType = "text/plain";
		if (filename.lastIndexOf('.') != -1 && filename.lastIndexOf('.') != 0) {
			extension = filename.substring(filename.lastIndexOf('.') + 1);
		}
		if (StringUtils.isNotEmpty(extension)) {
			if ("csv".equalsIgnoreCase(extension)) {
				contentType = "text/csv";
			} else if ("html".equalsIgnoreCase(extension)) {
				contentType = "text/html";
			} else if ("png".equalsIgnoreCase(extension)) {
				contentType = "image/png";
			} else if ("jpeg".equalsIgnoreCase(extension)) {
				contentType = "image/jpeg";
			} else if ("gif".equalsIgnoreCase(extension)) {
				contentType = "image/gif";
			} else if ("svg".equalsIgnoreCase(extension)) {
				contentType = "image/svg+xml";
			} else if ("pdf".equalsIgnoreCase(extension)) {
				contentType = "application/pdf";
			} else if ("xml".equalsIgnoreCase(extension)) {
				contentType = "application/xml";
			} else if ("xls".equalsIgnoreCase(extension) || ("xlsx".equalsIgnoreCase(extension))) {
				contentType = "application/vnd.ms-excel";
			} else if ("doc".equalsIgnoreCase(extension) || ("docx".equalsIgnoreCase(extension))) {
				contentType = "application/msword";
			} else if ("gzip".equalsIgnoreCase(extension)) {
				contentType = "application/x-gzip";
			} else if ("zip".equalsIgnoreCase(extension)) {
				contentType = "application/zip";
			}
		}
		return contentType;
	}
}