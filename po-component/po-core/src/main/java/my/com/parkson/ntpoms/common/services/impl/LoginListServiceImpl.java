/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
/* Name			:	MF11_M_COMPMAST_ServiceImpl											*/
/* Usage		:	Creating service layer for Company									*/
/* Description	:	This code creates a connection between Company Screen				*/
/* 					and Company table in database. This code interacts with				*/
/* 					the table MF11_M_COMPMAST											*/
/****************************************************************************************/

package my.com.parkson.ntpoms.common.services.impl;

import java.util.List;

import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.entities.Company;
import my.com.parkson.ntpoms.common.entities.LoginList;
import my.com.parkson.ntpoms.common.repositories.LoginListRepository;
import my.com.parkson.ntpoms.common.services.LogInService;
import my.com.parkson.ntpoms.common.services.MessageService;

/**
 * The Class CompanyServiceImpl.
 */
@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class LoginListServiceImpl implements LogInService {

	private static final Logger LOGGER = Logger.getLogger(LoginListServiceImpl.class);

	/** The messages. */
	@Autowired
	private MessageService messages;

	/** The company repository. */
	@Autowired
	private LoginListRepository companyRepository;

	@Override
	public void createCompany(LoginList company) {
		try {
			companyRepository.save(company);
		} catch (NonUniqueResultException e) {
			LOGGER.error(e);
		} catch (PersistenceException e) {
			LOGGER.error(e);
		} //
		catch (POException e) {
			LOGGER.error(e);
		}
	}

	@Override
	public List<LoginList> getYearByCompanyCode(String CompanyCode) {
		return companyRepository.findAllByCompCode(CompanyCode);
	}

	@Override
	public List<Company> getStatusByCompanyCode(String CompanyCode) {
		return companyRepository.getStatusByCompanyCode(CompanyCode);
	}

}