/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
/* Name : MF14_M_DEPTMAST_Service                                      */
/* Usage: Code for the department service                        */
/* Description: This code creates an interface for department that will */
/*              used for connecting to data. This code*/
/*              interacts with the table <mf14_m_deptmast>.        */
/*************************************************************/

package my.com.parkson.ntpoms.common.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import my.com.parkson.ntpoms.common.entities.Department;

/**
 * The Interface DepartmentRepository.
 */
public interface DepartmentRepository extends JpaRepository<Department, Integer>, JpaSpecificationExecutor<Department> {
	
	/**
	 * Find all by order by last modified on desc.
	 *
	 * @return the list
	 */
	public List<Department> findAllByOrderByLastModifiedOnDesc();

	/**
	 * Find all by is active order by last modified on desc.
	 *
	 * @param isActive the is active
	 * @return the list
	 */
	public List<Department> findAllByIsActiveOrderByLastModifiedOnDesc(boolean isActive);

	/**
	 * Find by code.
	 *
	 * @param code the code
	 * @return the department
	 */
	public Department findByCode(String code);
	/**
	 * Find all by order by last modified on desc.
	 *
	 * @return the list
	 */
	public Page<Department> findAllByOrderByLastModifiedOnDesc(Pageable pageRequest);

	/**
	 * Find all by order by last modified on desc.
	 *
	 * @return the list
	 */
	public Page<Department> findAllByOrderByLastModifiedOnDesc(Specification<Department> s, Pageable pageRequest);

}