/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.common.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import my.com.parkson.ntpoms.common.entities.Permission;
import my.com.parkson.ntpoms.common.entities.Role;
import my.com.parkson.ntpoms.common.entities.User;

/**
 * The Interface SecurityService.
 */
public interface SecurityService {

	/**
	 * Find user by username.
	 *
	 * @param username the username
	 * @return the user
	 */
	User findUserByUsername(String username);

	/**
	 * Gets the all permissions.
	 *
	 * @return the all permissions
	 */
	List<Permission> getAllPermissions();

	/**
	 * Gets the all roles.
	 *
	 * @return the all roles
	 */
	List<Role> getAllRoles();

	/**
	 * Gets the all pageable permissions.
	 *
	 * @param pageRequest the page request
	 * @return the all pageable permissions
	 */
	Page<Permission> getAllPageablePermissions(Pageable pageRequest);

	/**
	 * Gets the all pageable roles.
	 *
	 * @param pageRequest the page request
	 * @return the all pageable roles
	 */
	Page<Role> getAllPageableRoles(Pageable pageRequest);

	/**
	 * Creates the role.
	 *
	 * @param role the role
	 * @return the role
	 */
	Role createRole(Role role);

	/**
	 * Gets the role by id.
	 *
	 * @param id the id
	 * @return the role by id
	 */
	Role getRoleById(Integer id);

	/**
	 * Update role.
	 *
	 * @param role the role
	 * @return the role
	 */
	Role updateRole(Role role);

	/**
	 * Reset password.
	 *
	 * @param email the email
	 * @return the string
	 */
	String resetPassword(String email);

	/**
	 * Verify password reset token.
	 *
	 * @param email the email
	 * @param token the token
	 * @return true, if successful
	 */
	boolean verifyPasswordResetToken(String email, String token);
	
	boolean verifyCurrentPassword(String username, String currentPassword);

	/**
	 * Update password.
	 *
	 * @param email the email
	 * @param token the token
	 * @param encodedPwd the encoded pwd
	 */
	void updatePassword(String email, String token, String encodedPwd);

	/**
	 * Gets the all users.
	 *
	 * @return the all users
	 */
	List<User> getAllUsers();

	/**
	 * Gets the all pageable user.
	 *
	 * @param pageRequest the page request
	 * @return the all pageable user
	 */
	Page<User> getAllPageableUser(Pageable pageRequest);

	/**
	 * Creates the user.
	 *
	 * @param user the user
	 * @return the user
	 */
	User createUser(User user);

	/**
	 * Gets the user by username.
	 *
	 * @param username the username
	 * @return the user by username
	 */
	User getUserByUsername(String username);

	/**
	 * Update user.
	 *
	 * @param user the user
	 * @return the user
	 */
	User updateUser(User user);

	/**
	 * Gets the role by name.
	 *
	 * @param roleName the role name
	 * @return the role by name
	 */
	Role getRoleByName(String roleName);
	
	void updateuserlock(User obj);
}
