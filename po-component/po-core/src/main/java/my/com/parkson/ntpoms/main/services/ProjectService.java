package my.com.parkson.ntpoms.main.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import my.com.parkson.ntpoms.main.entities.Project;


public interface ProjectService {
	
	public void createProject(Project project);	
	public Project getProjectByCode(String code);	
	public Project getLastCreatedProject();	
	public List<Project> getAllProject();	
	public List<Project> getAllActiveProject();	
	public List<Project> getActiveProjectByProfitCenterCode(String profitCenterCode);
	Page<Project> getPagebleAll(Pageable req);
	
	public Page<Project> getAllPageableProject(Pageable pageRequest);
	
	public Page<Project> getAllPageableProject(Specification<Project> s, Pageable pageRequest);
	
	public Page<Project> getAllPageableProjectOrdered(Pageable pageRequest);
	
	public Page<Project> getAllPageableProjectOrdered(Specification<Project> s, Pageable pageRequest);
public long getCount();
	
	
	public long getCountActive();
}