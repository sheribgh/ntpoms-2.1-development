SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;


CREATE SCHEMA pocomptrans;

ALTER SCHEMA pocomptrans OWNER TO postgres;

SET search_path = pocomptrans, pg_catalog;




-- **************************************
-- *         CREATING TABLES			*
---*------------------------------------*
-- *	po01_t_main						*
-- *	po02_t_iteminfo					*
-- *	po03_t_itemmap					*
-- *	po04_t_usracs					*
-- *	po05_t_invoice					*
-- **************************************


CREATE FUNCTION "ExportCarryForwardPo"(company text, path text) RETURNS integer
    LANGUAGE plpgsql
    AS $$BEGIN
	EXECUTE 'CREATE TEMP TABLE IF NOT EXISTS po AS
	SELECT * FROM pocomptrans.po01_t_main WHERE po01_status IN (''Draft'',''Issued'',''Verified'',''Authorized'',''Invoiced'');

	COPY (select * from po) TO ''' || concat(path, '/', company, '_polist.csv') || ''' (format CSV);

	CREATE TEMP TABLE IF NOT EXISTS itemmap AS
	SELECT * FROM pocomptrans.po03_t_itemmap WHERE po01tmain_po03_pono IN (SELECT po01_pono FROM po);

	COPY (SELECT * FROM itemmap) TO ''' || concat(path, '/', company, '_itemmaplist.csv') || ''' (format CSV);


	COPY (select * from pocomptrans.po02_t_iteminfo where po02_itemno in (select po02titeminfo_po03_itemno from itemmap)) TO ''' || concat(path, '/', company, '_iteminfolist.csv') || ''' (format CSV);


	COPY (select * from pocomptrans.po05_t_invoice where po01tmain_po05_pono in (select po01_pono from po)) TO ''' || concat(path, '/', company, '_invoicelist.csv') || ''' (format CSV);


	COPY (select * from pocomptrans.po04_t_usracs) TO ''' || concat(path, '/', company, '_useraccesslist.csv') || ''' (format CSV);

	DROP TABLE IF EXISTS po;
	DROP TABLE IF EXISTS itemmap;';
	RETURN 0;
END$$;


ALTER FUNCTION pocomptrans."ExportCarryForwardPo"(company text, path text) OWNER TO postgres;



CREATE FUNCTION "ImportCarryForwardPo"(company text, path text) RETURNS integer
    LANGUAGE plpgsql 
    AS $$BEGIN
	EXECUTE 'COPY pocomptrans.po04_t_usracs FROM ''' || concat(path, '/', company, '_useraccesslist.csv') || ''' WITH (FORMAT csv);
		COPY pocomptrans.po05_t_invoice FROM ''' || concat(path, '/', company, '_invoicelist.csv') || ''' WITH (FORMAT csv);
		COPY pocomptrans.po02_t_iteminfo FROM ''' || concat(path, '/', company, '_iteminfolist.csv') || ''' WITH (FORMAT csv);
		COPY pocomptrans.po01_t_main FROM ''' || concat(path, '/', company, '_polist.csv') || ''' WITH (FORMAT csv);
		COPY pocomptrans.po03_t_itemmap FROM ''' || concat(path, '/', company, '_itemmaplist.csv') || ''' WITH (FORMAT csv);
		UPDATE pocomptrans.po01_t_main set po01_forwardingstatus = ''BF'';';
	RETURN 0;
END$$;


ALTER FUNCTION pocomptrans."ImportCarryForwardPo"(company text, path text) OWNER TO postgres;




CREATE TABLE pocomptrans.po01_t_main
(
  po01_pono character varying(30) NOT NULL, -- 
  po01_status character varying(30),
  po01_quoteno character varying(20),
  po01_quotedate date,
  mf13mpcmast_po01_pccode character varying(15) NOT NULL,
  po01_contactperson character varying(12),
  po01_contactpersonname character varying(70),
  po01_contactpersonphone character varying(15),
  po01_contactpersonemail character varying(70),
  po01_addressline1 character varying(70),
  po01_addressline2 character varying(70),
  po01_addressline3 character varying(70),
  po01_postcode character varying(70),
  po01_postarea character varying(70),
  mf11mcompmast_po01_compcode character varying(3),
  po01_termsandconditionsportrait character varying(255),
  po01_termsandconditionslandscape character varying(255),
  po01_storename character varying(30),
  po01_departmentname character varying(30),
  mf01mvendorbranch_po01_vendorcode character varying(6),
  po01_vendorname character varying(70),
  po01_vendorpersonincharge character varying(70),
  po01_vendorpersoninchargephone character varying(24),
  po01_vendorpersoninchargeemail character varying(70),
  po01_vendoraddressline1 character varying(70),
  po01_vendoraddressline2 character varying(70),
  po01_vendoraddressline3 character varying(70),
  po01_vendorpostcode character varying(10),
  po01_vendorpostarea character varying(70),
  mf02mcreditterms_po01_credittermscode character varying(10),
  po01_currency character varying(3),
  mf17mordcatmast_po01_ordcatcode character varying(70),
  po01_ordercategoryauthorizationgrid character varying(255),
  mf18mordsubcmast_po01_ordsubccode character varying(15),
  mf15mprmast_po01_prtypecode character varying(10),
  mf16mgstmast_po01_taxcode character varying(20),
  po01_paymentterms text,
  po01_deliveryinstruction text,
  po01_totinclgst numeric,
  po01_totexclgst numeric,
  po01_totgst numeric,
  mf12mempmast_po01_issuer character varying(11),
  po01_issuedon timestamp without time zone,
  mf12mempmast_po01_verifier character varying(11),
  po01_verifiedon timestamp without time zone,
  po01_verifyremark text,
  mf12mempmast_po01_authorizer character varying(11),
  po01_authorizedon timestamp without time zone,
  po01_authorizeremark text,
  po01_lastprintedby character varying(11),
  po01_lastprintedon timestamp without time zone,
  po01_redraftremark text,
  po01_rejectremark text,
  po01_closeexception text,
  po01_firstapprover character varying(50),
  po01_secondapprover character varying(50),
  po01_thirdapprover character varying(50),
  po01_expirystatus character varying(10),
  po01_expiredate timestamp without time zone,
  po01_cancelremark text,
  po01_deleteremark text,
  po01_expirydays integer,
  po01_overalldiscount numeric,
  po01_firstapproverdesignation character varying(70),
  po01_secondapproverdesignation character varying(70),
  po01_thirdapproverdesignation character varying(70),
  po03tproject_po01_projectcode character varying(15),
  po01_issuerremark text,
  po01_deletedby character varying(70),
  po01_deletedon timestamp without time zone,
  po01_cancelledby character varying(70),
  po01_cancelledon timestamp without time zone,
  po01_rejectedby character varying(70),
  po01_rejectedon timestamp without time zone,
  po01_closedby character varying(70),
  po01_closedon timestamp without time zone,
  po01_forwardingstatus character varying(10)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE pocomptrans.po01_t_main
  OWNER TO postgres;
COMMENT ON COLUMN pocomptrans.po01_t_main.po01_pono IS '
';










CREATE TABLE po02_t_iteminfo (
    po02_itemno character varying(10) NOT NULL,
    po02_particulars text,
    po02_packing character varying(20)
);


ALTER TABLE po02_t_iteminfo OWNER TO postgres;








CREATE TABLE po03_t_itemmap (
    po03_unitpriceexclgst numeric(12,2),
    po03_unitpriceinclgst numeric(12,2),
    po03_discount numeric(4,2),
    po03_totalexclgst numeric(30,2),
    po03_totalinclgst numeric(30,2),
    po03_totalgst numeric(30,2),
    po03_quantity integer,
    po03_mapcode character varying(35) NOT NULL,
    po01tmain_po03_pono character varying(20),
    po02titeminfo_po03_itemno character varying(15),
    po03_itemorder integer,
    mf16mgstmast_po03_taxcode character varying(20),
    po03_taxrate numeric(10,2),
    po03_totaldiscount numeric(30,2),
    po03_unitgst numeric(10,2),
    mf13mpcmast_po03_pccode character varying(15)
);


ALTER TABLE po03_t_itemmap OWNER TO postgres;








CREATE TABLE po04_t_usracs (
    po04_psid character varying(11) NOT NULL,
    po04_mypos character varying(4096),
    po04_tobeverified character varying(4096),
    po04_tobeauthorized character varying(4096),
    po04_viewrights character varying(4096),
    po04_draft character varying(4096),
    po04_redraft character varying(4096),
    po04_tobeinvoiced character varying(4096)
);


ALTER TABLE po04_t_usracs OWNER TO postgres;

CREATE SEQUENCE po04_t_usracs_po04_acscode_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE po04_t_usracs_po04_acscode_seq OWNER TO postgres;








CREATE TABLE po05_t_invoice (
    po05_invoiceno character varying(20) NOT NULL,
    po05_recipient character varying(20),
    po01tmain_po05_pono character varying(30),
    po05_amount numeric(10,2),
    po05_date date,
    po05_poinvoiceno character varying(50) NOT NULL,
    po01_invoicenote character varying(3)
);


ALTER TABLE po05_t_invoice OWNER TO postgres;






-- **************************************
-- *       CREATING PK CONSTRAINTS		*
-- **************************************

ALTER TABLE ONLY po01_t_main
    ADD CONSTRAINT po01_t_main_pkey PRIMARY KEY (po01_pono);


ALTER TABLE ONLY po02_t_iteminfo
    ADD CONSTRAINT po02_t_iteminfo_pkey PRIMARY KEY (po02_itemno);


ALTER TABLE ONLY po03_t_itemmap
    ADD CONSTRAINT po03_t_itemmap_pkey PRIMARY KEY (po03_mapcode);


ALTER TABLE ONLY po04_t_usracs
    ADD CONSTRAINT po04_t_usracs_pkey PRIMARY KEY (po04_psid);


ALTER TABLE ONLY po05_t_invoice
    ADD CONSTRAINT po05_invoice_pkey PRIMARY KEY (po05_poinvoiceno);
