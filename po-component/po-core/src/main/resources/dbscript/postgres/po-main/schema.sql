CREATE TABLE mfmaindb."LD02_T_ACSLOG"
(
  "ld02_logId" integer NOT NULL DEFAULT nextval('mfmaindb.ld02_t_acslog_sequence'::regclass),
  "ld02_classFileName" character varying(255),
  "ld02_lineNo" integer NOT NULL,
  "ld02_logLevel" character varying(255),
  "ld02_logMessage" character varying(511),
  "ld02_logTime" timestamp without time zone,
  "ld02_screenId" character varying(100),
  CONSTRAINT "LD02_T_ACSLOG_pkey" PRIMARY KEY ("ld02_logId")
)

CREATE TABLE mfmaindb."LD03_T_STGLOG"
(
  "ld03_logSetting" integer,
  "ld03_logSettingId" integer NOT NULL DEFAULT nextval('mfmaindb."LD03_T_STGLOG"'::regclass),
  CONSTRAINT "LD03_T_STGLOG_pkey" PRIMARY KEY ("ld03_logSettingId")
)


CREATE TABLE mfmaindb."LD04_T_AUDIT"
(
  "ld04_auditId" integer NOT NULL DEFAULT nextval('mfmaindb.audit_auditid_seq'::regclass),
  ld04_username text,
  ld04_message text,
  ld04_operation text,
  "ld04_createdOn" timestamp without time zone,
  CONSTRAINT audit_pkey PRIMARY KEY ("ld04_auditId")
)


CREATE TABLE mfmaindb.mf01_m_vendorbranch
(
  mf01_vendorcode character varying(255) NOT NULL,
  mf01_vendorbranchcode character varying(255) NOT NULL,
  mf01_branchdescription character varying(255),
  mf01_createdby character varying(255),
  mf01_createdon timestamp without time zone,
  mf01_deactivatedby character varying(255),
  mf01_deactivatedon timestamp without time zone,
  mf01_isactive boolean NOT NULL,
  mf01_lastcontactpersoncontactnumber character varying(255),
  mf01_lastcontactpersonemailaddress character varying(255),
  mf01_lastcontactpersonname character varying(255),
  mf01_lastmodifiedby character varying(255),
  mf01_lastmodifiedon timestamp without time zone,
  mf01_lastpocurrency character varying(255),
  mf01_postcode character varying(255),
  mf01_postcodearea character varying(255),
  mf01_reactivatedby character varying(255),
  mf01_reactivatedon timestamp without time zone,
  mf01_vendoraddressline1 character varying(255),
  mf01_vendoraddressline2 character varying(255),
  mf01_vendoraddressline3 character varying(255),
  mf01_vendorname character varying(255),
  mf02_creditterms_mf02_lastcredittermscode character varying(10),
  mf11mcompmast_mf01_compcode character varying(255) NOT NULL,
  CONSTRAINT mf01_m_vendorbranch_pkey PRIMARY KEY(mf01_vendorcode, mf01_vendorbranchcode, mf11mcompmast_mf01_compcode)
  )


CREATE TABLE mfmaindb.mf02_m_creditterms
(
  mf02_credittermscode character varying(255) NOT NULL,
  mf02_createdby character varying(255),
  mf02_createdon timestamp without time zone,
  mf02_credittermsdescription character varying(255),
  mf02_deactivatedby character varying(255),
  mf02_deactivatedon timestamp without time zone,
  "mf02_isActive" boolean NOT NULL,
  mf02_lastmodifiedby character varying(255),
  "mf02_lastModifiedOn" timestamp without time zone,
  mf02_reactivatedby character varying(255),
  mf02_reactivatedon timestamp without time zone,
  mf11_compcode character varying(255),
  mf11mcompmast_mf02_compcode character varying(255) NOT NULL,
  CONSTRAINT mf02_m_creditterms_pkey PRIMARY KEY (mf02_credittermscode, mf11mcompmast_mf02_compcode)
)

CREATE TABLE mfmaindb.mf03_m_vendorcrterms
(
  mf02_creditterms_mf02_credittermscode character varying(255) NOT NULL,
  mf01_vendorbranch_mf01_vendorcode character varying(255) NOT NULL,
  mf01_vendorbranch_mf01_vendorbranchcode character varying(255) NOT NULL,
  mf03_createdby character varying(255),
  mf03_createdon timestamp without time zone,
  mf03_deactivatedby character varying(255),
  mf03_deactivatedon timestamp without time zone,
  mf03_isactive boolean,
  mf03_lastmodifiedby character varying(255),
  "mf03_lastModifiedOn" timestamp without time zone,
  mf03_reactivatedby character varying(255),
  mf03_reactivatedon timestamp without time zone,
  mf11_compcode character varying(255),
  mf11mcompmast_mf03_compcode character varying(255) NOT NULL,
  CONSTRAINT mf03_m_vendorcrterms_pkey PRIMARY KEY (mf01_vendorbranch_mf01_vendorcode, mf02_creditterms_mf02_credittermscode, mf01_vendorbranch_mf01_vendorbranchcode, mf11mcompmast_mf03_compcode)
)


CREATE TABLE mfmaindb.mf04_m_profitcenterlastponumber
(
  mf04_lastponumber character varying(20) NOT NULL,
  mf13mpcmast_mf13_pccode character varying(15) NOT NULL,
  mf04_createdby character varying(255),
  mf04_createdon timestamp without time zone,
  mf04_modifiedby character varying(255),
  mf04_modifiedon timestamp without time zone,
  mf04_deactivatedby character varying(255),
  mf04_deactivatedon timestamp without time zone,
  mf04_reactivatedby character varying(255),
  mf04_reactivatedon timestamp without time zone,
  mf04_isactive boolean,
  mf04_year character varying NOT NULL,
  mf04_jsonfield json,
  mf04_runningno integer,
  CONSTRAINT mf04_m_profitcenterlastponumber_pkey PRIMARY KEY (mf13mpcmast_mf13_pccode, mf04_year)
)


CREATE TABLE mfmaindb.po01_t_main
(
  po01_pono character varying(20) NOT NULL, -- 
  mf11mcompmast_po01_compcode character varying(3),
  mf13mpcmast_po01_pccode character varying(15) NOT NULL,
  mf01mvendorbranch_po01_vendorcode character varying(6),
  mf01mvendorbranch_po01_vendorbranchcode character(2),
  mf02mcreditterms_po01_credittermscode character varying(10),
  mf17mordcatmast_po01_ordcatcode character varying(70),
  mf18mordsubcmast_po01_ordsubccode character varying(15),
  mf15mprmast_po01_prtypecode character varying(10),
  mf16mgstmast_po01_taxcode character varying(20),
  mf12mempmast_po01_issuer character varying(11),
  mf12mempmast_po01_verifier character varying(11),
  mf12mempmast_po01_authorizer character varying(11),
  po03tproject_po01_projectcode character varying(15),
  po01_status character varying(30),
  po01_quoteno character varying(20),
  po01_quotedate date,
  po01_contactperson character varying(12),
  po01_contactpersonname character varying(70),
  po01_contactpersonphone character varying(15),
  po01_contactpersonemail character varying(70),
  po01_addressline1 character varying(70),
  po01_addressline2 character varying(70),
  po01_addressline3 character varying(70),
  po01_postcode character varying(70),
  po01_postarea character varying(70),
  po01_termsandconditionsportrait character varying(255),
  po01_termsandconditionslandscape character varying(255),
  po01_storename character varying(30),
  po01_departmentname character varying(30),
  po01_vendorname character varying(70),
  po01_vendorpersonincharge character varying(70),
  po01_vendorpersoninchargephone character varying(24),
  po01_vendorpersoninchargeemail character varying(70),
  po01_vendoraddressline1 character varying(70),
  po01_vendoraddressline2 character varying(70),
  po01_vendoraddressline3 character varying(70),
  po01_vendorpostcode character varying(10),
  po01_vendorpostarea character varying(70),
  po01_currency character varying(3),
  po01_ordercategoryauthorizationgrid character varying(255),
  po01_paymentterms text,
  po01_deliveryinstruction text,
  po01_totinclgst numeric,
  po01_totexclgst numeric,
  po01_totgst numeric,
  po01_overalldiscount numeric,
  po01_firstapprover character varying(50),
  po01_secondapprover character varying(50),
  po01_thirdapprover character varying(50),
  po01_firstapproverdesignation character varying(70),
  po01_secondapproverdesignation character varying(70),
  po01_thirdapproverdesignation character varying(70),
  po01_expirystatus character varying(10),
  po01_expiredate timestamp without time zone,
  po01_expirydays integer,
  po01_issuerremark text,
  po01_verifyremark text,
  po01_authorizeremark text,
  po01_redraftremark text,
  po01_rejectremark text,
  po01_closeexception text,
  po01_cancelremark text,
  po01_deleteremark text,
  po01_deletedby character varying(70),
  po01_cancelledby character varying(70),
  po01_rejectedby character varying(70),
  po01_closedby character varying(70),
  po01_lastprintedby character varying(50),
  po01_issuedon timestamp without time zone,
  po01_verifiedon timestamp without time zone,
  po01_authorizedon timestamp without time zone,
  po01_deletedon timestamp without time zone,
  po01_cancelledon timestamp without time zone,
  po01_rejectedon timestamp without time zone,
  po01_closedon timestamp without time zone,
  po01_lastprintedon timestamp without time zone,
  po01_lastmodifiedon timestamp without time zone,
  po01_forwardingstatus character varying(20),
  CONSTRAINT po01_t_main_pkey PRIMARY KEY (po01_pono)
)

CREATE TABLE mfmaindb.po03_t_project
(
  "po03_projectCode" character varying(15) NOT NULL,
  "po03_projectName" character varying(70) NOT NULL,
  "po03_projectDescription" character varying(255),
  "po03_projectHead" text,
  "po03_projectTeamMember" text,
  "mf13MPCMast_mf13_pcCode" text,
  "po03_completionDate" date,
  "po03_createdBy" character varying(255) NOT NULL,
  "po03_lastModifiedBy" character varying(255) NOT NULL,
  "po03_deactivatedBy" character varying(255),
  "po03_reactivatedBy" character varying(255),
  "po03_isActive" boolean,
  "po03_jsonField" json,
  "po03_createdOn" timestamp without time zone,
  "po03_lastModifiedOn" timestamp without time zone,
  "po03_deactivatedOn" timestamp without time zone,
  "po03_reactivatedOn" timestamp without time zone,
  CONSTRAINT "PO03_T_PROJECT_pkey" PRIMARY KEY ("po03_projectCode")
)


CREATE TABLE mfmaindb.po04_t_useraccess
(
  po04_pono character varying(30) NOT NULL,
  po04_psid character varying(15) NOT NULL,
  po04_action character varying(10) NOT NULL,
  CONSTRAINT po04_t_useraccess_pkey PRIMARY KEY (po04_pono, po04_psid, po04_action)
)

CREATE TABLE mfmaindb.po07_t_poarchivedirectory
(
  po07_compcode character varying(3),
  po07_ponumber character varying(30) NOT NULL,
  po07_pccode character varying(15),
  po07_vendorcode character varying(6) NOT NULL,
  po07_vendorname character varying(70) NOT NULL,
  po07_issuerpsid character varying(255) NOT NULL,
  po07_issuername character varying(255) NOT NULL,
  po07_totalamt numeric(30,2),
  po07_archivedtable character varying(30),
  po07_archivedyear character varying(4) NOT NULL,
  po07_createdon timestamp without time zone NOT NULL,
  po07_users character varying(255) NOT NULL,
  CONSTRAINT po07_t_poarchivedirectory_pkey PRIMARY KEY (po07_ponumber)
)