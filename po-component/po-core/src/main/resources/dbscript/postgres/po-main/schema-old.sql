SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;


CREATE SCHEMA pomain;

ALTER SCHEMA pomain OWNER TO postgres;

SET search_path = pomain, pg_catalog;

SET default_tablespace = '';
SET default_with_oids = false;


-- **************************************
-- *         CREATING TABLES			*
-- *------------------------------------*
-- *	mf01_m_vendorbranch				*
-- *	mf02_m_creditterms				*
-- *	mf03_m_vendorcrterms			*
-- *	mf04_m_profitcenterlastponumber	*
-- **************************************


CREATE TABLE mf01_m_vendorbranch (
    mf01_vendorcode character varying(6) NOT NULL,
    mf01_vendorbranchcode character varying(2) NOT NULL,
    mf01_vendorname character varying(70) NOT NULL,
    mf01_branchdescription character varying(70) NOT NULL,
    mf01_vendoraddressline1 character varying(70),
    mf01_vendoraddressline2 character varying(70),
    mf01_vendoraddressline3 character varying(70),
    mf01_postcode character varying(10),
    mf01_postcodearea character varying(70),
    mf01_lastcontactpersonname character varying(70),
    mf01_lastcontactpersoncontactnumber character varying(15),
    mf01_lastcontactpersonemailaddress character varying(70),
    mf01_lastpocurrency character(3),
    mf01_createdby character varying(255) NOT NULL,
    mf01_createdon timestamp without time zone NOT NULL,
    mf01_lastmodifiedby character varying(255) NOT NULL,
    mf01_lastmodifiedon timestamp without time zone NOT NULL,
    mf01_deactivatedby character varying(255),
    mf01_deactivatedon timestamp without time zone,
    mf01_reactivatedby character varying(255),
    mf01_reactivatedon timestamp without time zone,
    mf01_isactive boolean,
    mf01_jsonfield json,
    mf02_creditterms_mf02_lastcredittermscode character varying(10)
);

ALTER TABLE mf01_m_vendorbranch OWNER TO postgres;







CREATE TABLE mf02_m_creditterms (
    mf02_credittermscode character varying(2) NOT NULL,
    mf02_credittermsdescription character varying(100),
    mf02_createdby character varying(255),
    mf02_createdon timestamp without time zone,
    mf02_lastmodifiedby character varying(255),
    mf02_lastmodifiedon timestamp without time zone,
    mf02_deactivatedby character varying(255),
    mf02_deactivatedon timestamp without time zone,
    mf02_reactivatedby character varying(255),
    mf02_reactivatedon timestamp without time zone,
    mf02_isactive boolean,
    mf02_jsonfield json
);

ALTER TABLE mf02_m_creditterms OWNER TO postgres;








CREATE TABLE mf03_m_vendorcrterms (
    mf01_vendorbranch_mf01_vendorcode character varying,
    mf02_creditterms_mf02_credittermscode character varying,
    mf03_createdby character varying(255) NOT NULL,
    mf03_createdon timestamp without time zone NOT NULL,
    mf03_lastmodifiedby character varying(255) NOT NULL,
    mf03_lastmodifiedon timestamp without time zone NOT NULL,
    mf03_deactivatedby character varying,
    mf03_deactivatedon timestamp without time zone,
    mf03_reactivatedby character varying(255),
    mf03_reactivatedon timestamp without time zone,
    mf03_isactive boolean NOT NULL,
    mf03_json json,
    mf01_vendorbranch_mf01_vendorbranchcode character varying
);

ALTER TABLE mf03_m_vendorcrterms OWNER TO postgres;







CREATE TABLE pomain.mf04_m_profitcenterlastponumber
(
  mf04_lastponumber character varying(30) NOT NULL,
  mf13mpcmast_mf13_pccode character varying(15) NOT NULL,
  mf04_createdby character varying(255),
  mf04_createdon timestamp without time zone,
  mf04_modifiedby character varying(255),
  mf04_modifiedon timestamp without time zone,
  mf04_deactivatedby character varying(255),
  mf04_deactivatedon timestamp without time zone,
  mf04_reactivatedby character varying(255),
  mf04_reactivatedon timestamp without time zone,
  mf04_isactive boolean,
  mf04_year character varying NOT NULL,
  mf04_jsonfield json
);


ALTER TABLE pomain.mf04_m_profitcenterlastponumber
  OWNER TO postgres;


  
  
  
  
  
  
-- **************************************
-- *       CREATING PK CONSTRAINTS 		*
-- **************************************


ALTER TABLE ONLY mf02_m_creditterms
    ADD CONSTRAINT mf02_credittermscode_pkey PRIMARY KEY (mf02_credittermscode);


ALTER TABLE ONLY mf01_m_vendorbranch
    ADD CONSTRAINT vendorcodebranchcode_pkey PRIMARY KEY (mf01_vendorcode, mf01_vendorbranchcode);
	

ALTER TABLE ONLY mf04_m_profitcenterlastponumber
    ADD CONSTRAINT mf04_m_profitcenterlastponumber_pkey PRIMARY KEY (mf13mpcmast_mf13_pccode, mf04_year);


	
	
	
-- **************************************
-- *    CREATING UNIQUE CONSTRAINTS 	*
-- **************************************
	
	
ALTER TABLE ONLY mf01_m_vendorbranch
    ADD CONSTRAINT mf01_vendorbranchcode_uniq UNIQUE (mf01_vendorbranchcode);


ALTER TABLE ONLY mf01_m_vendorbranch
    ADD CONSTRAINT mf01_vendorcode_uniq UNIQUE (mf01_vendorcode);	
	
	
	
	
	
	
	
-- **************************************
-- *    	CREATING FK CONSTRAINTS 	*
-- **************************************
	

ALTER TABLE ONLY mf03_m_vendorcrterms
    ADD CONSTRAINT mf01_vendorbranch_mf01_vendorbranchcode_fkey FOREIGN KEY (mf01_vendorbranch_mf01_vendorbranchcode) REFERENCES mf01_m_vendorbranch(mf01_vendorbranchcode);


ALTER TABLE ONLY mf03_m_vendorcrterms
    ADD CONSTRAINT mf01_vendorbranch_mf01_vendorcode_fkey FOREIGN KEY (mf01_vendorbranch_mf01_vendorcode) REFERENCES mf01_m_vendorbranch(mf01_vendorcode);


ALTER TABLE ONLY mf03_m_vendorcrterms
    ADD CONSTRAINT mf02_creditterms_mf02_credittermscode_for FOREIGN KEY (mf02_creditterms_mf02_credittermscode) REFERENCES mf02_m_creditterms(mf02_credittermscode);


ALTER TABLE ONLY mf01_m_vendorbranch
    ADD CONSTRAINT mf02_creditterms_mf02_lastcredittermscode_foreign FOREIGN KEY (mf02_creditterms_mf02_lastcredittermscode) REFERENCES mf02_m_creditterms(mf02_credittermscode);







-- **************************************
-- *         CREATING INDEXES			*
-- **************************************


CREATE INDEX fki_mf01_vendorbranch_mf01_vendorbranchcode_fkey ON mf03_m_vendorcrterms USING btree (mf01_vendorbranch_mf01_vendorbranchcode);


CREATE INDEX fki_mf01_vendorbranch_mf01_vendorcode_fkey ON mf03_m_vendorcrterms USING btree (mf01_vendorbranch_mf01_vendorcode);


CREATE INDEX fki_mf02_creditterms_mf02_lastcredittermscode_foreign ON mf01_m_vendorbranch USING btree (mf02_creditterms_mf02_lastcredittermscode);
