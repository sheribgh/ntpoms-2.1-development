-- **************************************
-- *         Populating TABLES			*
-- *------------------------------------*
-- *	LD02_T_ACSLOG					*
-- *	LD03_T_STGLOG					*
-- *	LD04_T_AUDIT					*
-- *	MF02_M_STRMAST					*
-- *	MF06_R_MASTDATA					*
-- *	MF07_R_SYSPARAM					*
-- *	MF11_M_COMPMAST					*
-- *	MF12_M_EMPMAST					*
-- *	MF13_M_PCMAST					*
-- *	MF14_M_DEPTMAST					*
-- *	MF15_M_PRMAST					*
-- *	MF16_M_GSTMAST					*
-- *	MF17_M_ORDCATMAST				*
-- *	MF18_M_ORDSUBCMAST				*
-- *	MF19_M_JOBMAST					*
-- *	MF20_M_ADMINMAST				*
-- *	Mf22_M_SCHEDULER				*
-- *	PO06_T_USER						*
-- *	PO01_LOGINLIST					*
-- *	PO01_T_MAIN						*
-- *	menu							*
-- *	menu_grp						*
-- *	menu_grp_role					*
-- *	permissions						*
-- *	role_permission					*
-- *	roles							*
-- *	user_role						*
-- *	users							*
-- **************************************
  
set schema 'mfmaindb';

--*********************
--   LD02_T_ACSLOG
--*********************


--*********************
--   LD03_T_STGLOG
--*********************
INSERT INTO "LD03_T_STGLOG" VALUES (1, 2);

--*********************
--   LD04_T_AUDIT
--*********************


--*********************
--   MF02_M_STRMAST
--*********************
INSERT INTO "MF02_M_STRMAST" VALUES ('0123', 'MS1', 'My Store', '001', 'superadmin', '2018-05-08 12:22:36.834', 'superadmin', '2018-05-08 12:22:36.834', NULL, NULL, NULL, NULL, true, NULL);
INSERT INTO "MF02_M_STRMAST" VALUES ('0124', 'NS1', 'New Store', '001', 'superadmin', '2018-05-08 12:52:26.073', 'superadmin', '2018-05-08 12:52:26.073', NULL, NULL, NULL, NULL, true, NULL);

--*********************
--   MF06_R_MASTDATA
--*********************
INSERT INTO "MF06_R_MASTDATA" VALUES ('Master Data 1 Desc', 'MST1', 'Master Value 1', 'superadmin', '2018-05-08 13:14:47.294', true, 'superadmin', '2018-05-08 13:15:20.04', 'MSS1', NULL, 'superadmin', 'superadmin', '2018-05-08 13:15:20.04+08', '2018-05-08 13:15:12.252+08', 1);

--*********************
--   MF07_R_SYSPARAM
--*********************
INSERT INTO "MF07_R_SYSPARAM" VALUES ('superadmin', '2017-09-25 18:32:04.697', 'SIROI', true, 'superadmin', '2017-09-25 18:32:04.697', 'MF_LDAPFULLURL', 'ldap://localhost:389/dc=parksondev,dc=com,dc=my', 'NTPO', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO "MF07_R_SYSPARAM" VALUES ('superadmin', '2017-09-25 18:34:36.318', 'SIROI', true, 'superadmin', '2017-09-25 18:34:36.318', 'MF_LDAPURL', 'ldap://localhost:389', '', NULL, NULL, NULL, NULL, NULL, 2);
INSERT INTO "MF07_R_SYSPARAM" VALUES ('superadmin', '2017-10-23 11:14:30.135', 'SIROI', true, 'superadmin', '2017-10-23 11:14:30.135', 'MF_LDAPBASE', 'dc=parksondev,dc=com,dc=my', 'NTPO', NULL, NULL, NULL, NULL, NULL, 3);
INSERT INTO "MF07_R_SYSPARAM" VALUES ('superadmin', '2017-10-23 11:14:30.135', 'SIROI', true, 'superadmin', '2017-10-23 11:14:30.135', 'MF_LDAPMGRDN', 'cn=admin,dc=parksondev,dc=com,dc=my', '', NULL, NULL, NULL, NULL, NULL, 4);
INSERT INTO "MF07_R_SYSPARAM" VALUES ('superadmin', '2017-10-23 11:14:30.135', 'SIROI', true, 'superadmin', '2017-10-23 11:14:30.135', 'MF_LDAPMGRPASS', 'siroi12admin', '', NULL, NULL, NULL, NULL, NULL, 5);
INSERT INTO "MF07_R_SYSPARAM" VALUES ('superadmin', '2017-10-30 15:33:20.596', 'SIROI', true, 'superadmin', '2017-10-30 15:33:20.596', 'BACKUP_FOLDER_PATH', 'D:\\', '', NULL, NULL, NULL, NULL, NULL, 6);
INSERT INTO "MF07_R_SYSPARAM" VALUES ('superadmin', '2017-10-30 15:33:20.596', 'SIROI', true, 'superadmin', '2017-10-30 15:33:20.596', 'PASSWORD_LIFETIME', '30', '', NULL, NULL, NULL, NULL, NULL, 7);
INSERT INTO "MF07_R_SYSPARAM" VALUES ('superadmin', '2017-10-30 15:33:20.596', 'SIROI', true, 'superadmin', '2017-10-30 15:33:20.596', 'PASSWORD_NOTIF', '2', '', NULL, NULL, NULL, NULL, NULL, 8);
INSERT INTO "MF07_R_SYSPARAM" VALUES ('superadmin', '2017-10-30 15:33:20.596', 'SIROI', true, 'superadmin', '2017-10-30 15:33:20.596', 'ACCT_NOTIF', '2', '', NULL, NULL, NULL, NULL, NULL, 9);
INSERT INTO "MF07_R_SYSPARAM" VALUES ('superadmin', '2017-10-30 15:33:20.596', 'SIROI', true, 'superadmin', '2017-10-30 15:33:20.596', 'M_DEFAULT_LOGO', 'img/logo.jpg', '', NULL, NULL, NULL, NULL, NULL, 10);
INSERT INTO "MF07_R_SYSPARAM" VALUES ('superadmin', '2017-10-30 15:33:20.596', 'SIROI', true, 'superadmin', '2017-10-30 15:33:20.596', 'MFPDF', 'D:\\', '', NULL, NULL, NULL, NULL, NULL, 11);
INSERT INTO "MF07_R_SYSPARAM" VALUES ('superadmin', '2017-10-30 15:33:20.596', 'SIROI', true, 'superadmin', '2018-05-01 23:59:31.186', 'ACCT_LIFETIME', '30', 'new', NULL, NULL, NULL, NULL, NULL, 12);
INSERT INTO "MF07_R_SYSPARAM" VALUES ('superadmin', '2017-09-25 18:34:36.318', 'SIROI', true, 'superadmin', '2017-09-25 18:34:36.318', 'LETTERHEAD_FOLDER_PATH', 'C:\\Users\\User\\workspace-siroi\\poAdmin\\po-ui\\po-admin-ui\\uploaded files\\company letter heading\\', '', NULL, NULL, NULL, NULL, NULL, 13);
INSERT INTO "MF07_R_SYSPARAM" VALUES ('superadmin', '2017-09-25 18:34:36.318', 'SIROI', true, 'superadmin', '2017-09-25 18:34:36.318', 'TERMS_FOLDER_PATH', 'C:\\Users\\User\\workspace-siroi\\poAdmin\\po-ui\\po-admin-ui\\uploaded files\\terms and conditions\\', '', NULL, NULL, NULL, NULL, NULL, 14);
INSERT INTO "MF07_R_SYSPARAM" VALUES ('superadmin', '2017-10-23 11:14:30.135', 'SIROI', true, 'superadmin', '2017-10-23 11:14:30.135', 'AUTHGRID_FOLDER_PATH', 'C:\\Users\\User\\workspace-siroi\\poAdmin\\po-ui\\po-admin-ui\\uploaded files\\authorization grid\\', 'NTPO', NULL, NULL, NULL, NULL, NULL, 15);
INSERT INTO "MF07_R_SYSPARAM" VALUES ('superadmin', '2017-09-25 18:32:04.697', 'SIROI', true, 'superadmin', '2018-05-08 12:33:49.222', 'LOGO_FOLDER_PATH', 'C:\\Users\\User\\workspace-siroi\\poAdmin\\po-ui\\po-admin-ui\\uploaded files\\company logo\\', 'NTPO', NULL, NULL, NULL, NULL, NULL, 16);
INSERT INTO "MF07_R_SYSPARAM" VALUES ('superadmin', '2017-10-30 15:33:20.596', 'SIROI', true, 'superadmin', '2018-05-08 12:41:28.839', 'UPLOAD_FOLDER_PATH', 'C:\\Users\\User\\workspace-siroi\\poAdmin\\po-ui\\po-admin-ui\\uploaded files\\uploadFolderPath', '', NULL, NULL, NULL, NULL, NULL, 17);
INSERT INTO "MF07_R_SYSPARAM" VALUES ('superadmin', '2017-10-30 15:33:20.596', 'SIROI', true, 'superadmin', '2018-05-08 12:42:03.88', 'SUCCESS_FOLDER_PATH', 'C:\\Users\\User\\workspace-siroi\\poAdmin\\po-ui\\po-admin-ui\\uploaded files\\Success Folder', '', NULL, NULL, NULL, NULL, NULL, 18);
INSERT INTO "MF07_R_SYSPARAM" VALUES ('superadmin', '2017-10-23 11:14:30.135', 'SIROI', true, 'superadmin', '2018-05-08 12:42:33.46', 'FAILED_FOLDER_PATH', 'C:\\Users\\User\\workspace-siroi\\poAdmin\\po-ui\\po-admin-ui\\uploaded files\\Failed Folder', 'NTPO', NULL, NULL, NULL, NULL, NULL, 19);
INSERT INTO "MF07_R_SYSPARAM" VALUES ('superadmin', '2017-10-23 11:14:30.135', 'SIROI', true, 'superadmin', '2018-05-08 12:43:14.406', 'LOG_FOLDER_PATH', 'C:\\Users\\User\\workspace-siroi\\poAdmin\\po-ui\\po-admin-ui\\uploaded files\\Log Folder', 'NTPO', NULL, NULL, NULL, NULL, NULL, 20);
INSERT INTO "MF07_R_SYSPARAM" VALUES ('superadmin', '2017-10-23 11:14:30.135', 'SIROI', true, 'superadmin', '2018-05-16 12:43:14.406', 'DATE_FORMAT', 'dd-MM-yyyy', 'D_FM', NULL, NULL, NULL, NULL, NULL, 21);
INSERT INTO "MF07_R_SYSPARAM" VALUES ('superadmin', '2017-10-23 11:14:30.135', 'SIROI', true, 'superadmin', '2018-05-16 12:43:14.406', 'DATE_TIME_FORMAT', 'dd-MM-yyyy HH:mm:ss', 'DT_FM', NULL, NULL, NULL, NULL, NULL, 22);

--*********************
--   MF11_M_COMPMAST
--*********************
INSERT INTO "MF11_M_COMPMAST" VALUES ('001', 'Company One', '111111', 'superadmin', '2018-05-08 12:10:39.496', true, 'superadmin', '2018-05-08 12:10:39.496', 'HR1', 'CNO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "MF11_M_COMPMAST" VALUES ('002', 'Company Two', '22222', 'superadmin', '2018-05-08 12:45:30.756', true, 'superadmin', '2018-05-08 12:45:30.756', 'HR2', 'CNT', '002_LOGO.jpeg', '002_LETTERHEAD.jpeg', NULL, NULL, NULL, NULL, NULL, '002_TERMSP.pdf', '002_TERMSL.pdf');
INSERT INTO "MF11_M_COMPMAST" VALUES ('003', 'Company Three', '33333', 'superadmin', '2018-05-08 12:51:41.917', true, 'superadmin', '2018-05-08 12:51:41.917', 'HR3', 'CNR', '003_LOGO.jpeg', '003_LETTERHEAD.jpeg', NULL, NULL, NULL, NULL, NULL, '003_TERMSP.pdf', NULL);


--*********************
--   MF12_M_EMPMAST
--*********************
INSERT INTO "MF12_M_EMPMAST" VALUES ('0002049', ' 524636', 'P052463', '2015-10-02', 'admin', '2017-07-04 15:16:07.014', 'PCS0000234', 'P0231-040 ', '1959-01-11', 'f@g.com', 'R', 'John Clarcks', 'CHINESE', NULL, 'L', NULL, NULL, 'F', '2015-04-02', 'A', true, 'admin', '2017-07-04 15:16:07.014', 'P0231', 'Parkson Mahkota Parade', '2019-07-03', '2017-07-03 00:00:00', NULL, '590111045096', NULL, NULL, 'PSNONEXC', 'PCS', 'A', '4563563456', '00001362', 'Senior Cashier', '2017-07-03 00:00:00', NULL, '0002263', '00001360', 'E01', 'BE01', NULL, NULL, '2017-07-03', NULL, 'null', NULL, '001', '0123001', 'ACCNT', NULL, NULL, NULL, NULL);
INSERT INTO "MF12_M_EMPMAST" VALUES ('0002039', ' 524636', 'P052463', '2015-10-02', 'admin', '2017-07-04 15:16:07.014', 'PCS0000234', 'P0231-040 ', '1959-01-11', 'a@f.com', 'R', 'James Parker', 'CHINESE', NULL, 'L', NULL, NULL, 'F', '2015-04-02', 'A', true, 'admin', '2017-07-04 15:16:07.014', 'P0231', 'Parkson Mahkota Parade', '2019-07-03', '2017-07-03 00:00:00', NULL, '590111045096', NULL, NULL, 'PSNONEXC', 'PCS', 'A', '574674687478', '00001362', 'Senior Cashier', '2017-07-03 00:00:00', NULL, '0002263', '00001360', 'E01', 'BE01', NULL, NULL, '2017-07-03', NULL, 'null', NULL, '002', '0123001', 'CLRK', NULL, NULL, NULL, NULL);
INSERT INTO "MF12_M_EMPMAST" VALUES ('0002040', ' 524636', 'P052463', '2015-10-02', 'admin', '2017-07-04 15:16:07.014', 'PCS0000234', 'P0231-040 ', '1959-01-11', 'a@gmail.com', 'R', 'Mary Neo', 'CHINESE', NULL, 'L', NULL, NULL, 'F', '2015-04-02', 'A', true, 'admin', '2017-07-04 15:16:07.014', 'P0231', 'Parkson Mahkota Parade', '2019-07-03', '2017-07-03 00:00:00', NULL, '590111045096', NULL, NULL, 'PSNONEXC', 'PCS', 'A', '111111111111111', '00001362', 'Senior Cashier', '2017-07-03 00:00:00', NULL, '0002263', '00001360', 'E01', 'BE01', NULL, NULL, '2017-07-03', NULL, 'null', NULL, '001', '0123001', 'NTADM', NULL, NULL, NULL, NULL);
INSERT INTO "MF12_M_EMPMAST" VALUES ('0002041', ' 524636', 'P052463', '2015-10-02', 'admin', '2017-07-04 15:16:07.014', 'PCS0000234', 'P0231-040 ', '1959-01-11', 'a@h.com', 'R', 'Emily Geak', 'CHINESE', NULL, 'L', NULL, NULL, 'F', '2015-04-02', 'A', true, 'admin', '2017-07-04 15:16:07.014', 'P0231', 'Parkson Mahkota Parade', '2019-07-03', '2017-07-03 00:00:00', NULL, '590111045096', NULL, NULL, 'PSNONEXC', 'PCS', 'A', '78787878', '00001362', 'Senior Cashier', '2017-07-03 00:00:00', NULL, '0002263', '00001360', 'E01', 'BE01', NULL, NULL, '2017-07-03', NULL, 'null', NULL, '002', '0123001', 'ACCNT', NULL, NULL, NULL, NULL);


--*********************
--   MF13_M_PCMAST
--*********************
INSERT INTO "MF13_M_PCMAST" VALUES ('superadmin', '2018-05-08 12:25:14.224', 'superadmin', '2018-05-08 12:25:14.224', '0123001', 'My First Profit Center', true, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '001', '0123', '001', 'MFPC', 'S', NULL, NULL);
INSERT INTO "MF13_M_PCMAST" VALUES ('superadmin', '2018-05-08 12:54:40.378', 'superadmin', '2018-05-08 12:54:40.378', '0123002', 'Profit Center Two', true, 'Address 1', 'nothing', '', '57100', 'KL', '0002040-Mary Neo(Senior Cashier)', '0002041-Emily Geak(Senior Cashier),0002049-John Clarcks(Senior Cashier)', NULL, NULL, NULL, NULL, NULL, '001', '0123', '002', 'PC2', 'H', NULL, NULL);

--*********************
--   MF14_M_DEPTMAST
--*********************
INSERT INTO "MF14_M_DEPTMAST" VALUES ('001', 'First Department', 'superadmin', '2018-05-08 12:23:10.102', 'superadmin', '2018-05-08 12:23:10.102', NULL, NULL, NULL, NULL, true, NULL, '01');
INSERT INTO "MF14_M_DEPTMAST" VALUES ('002', 'Department Two', 'superadmin', '2018-05-08 12:53:01.423', 'superadmin', '2018-05-08 12:53:01.423', NULL, NULL, NULL, NULL, true, NULL, '02');

--*********************
--   MF15_M_PRMAST
--*********************
INSERT INTO "MF15_M_PRMAST" VALUES ('CAPEX ABBR', 'CAPEX Description Text', 'superadmin', '2018-05-08 12:58:17.178', 'superadmin', '2018-05-08 12:58:42.845', 'superadmin', '2018-05-08 12:58:34.341', 'superadmin', '2018-05-08 12:58:42.845', 'CAPEX', true, NULL);


--*********************
--   MF16_M_GSTMAST
--*********************
INSERT INTO "MF16_M_GSTMAST" VALUES ('GST', 'Government Tax', '2016-01-01', '2020-12-30', 'superadmin', '2018-05-08 12:56:19.899', 'superadmin', '2018-05-08 12:57:40.425', 'superadmin', '2018-05-08 12:57:25.914', 'superadmin', '2018-05-08 12:57:40.425', 'GST Abbreviation Text', true, NULL, 6.0000, 1.2500);


--*********************
--   MF17_M_ORDCATMAST
--*********************
INSERT INTO "MF17_M_ORDCATMAST" VALUES ('Order Category One', 'First Order Category', 'Order Category One_AUTHGRID.png', 'superadmin', '2018-05-08 12:59:48.904', 'superadmin', '2018-05-08 13:25:20.597', 'superadmin', '2018-05-08 13:01:27.07', 'superadmin', '2018-05-08 13:03:27.311', NULL, '', 'CAPEX', true);


--*********************
--   MF18_M_ORDSUBCMAST
--*********************
INSERT INTO "MF18_M_ORDSUBCMAST" VALUES ('OSC1', 'Order Subcategory One', 'Order Category One', 'superadmin', '2018-05-08 13:04:00.934', '2018-05-08 13:04:17.33', 'superadmin', '2018-05-08 13:04:10.471', 'superadmin', '2018-05-08 13:04:17.33', NULL, 'superadmin', 'OSC1 Abbr', true);


--*********************
--   MF19_M_JOBMAST
--*********************
INSERT INTO "MF19_M_JOBMAST" VALUES ('NTADM', 'Network Administrator!@3', 'NetADMIN', 'superadmin', '2017-10-05 11:58:00.736', 'superadmin', '2017-10-05 11:58:00.736', NULL, NULL, NULL, NULL, NULL, true);
INSERT INTO "MF19_M_JOBMAST" VALUES ('ACCNT', 'Accountant', 'Accountant', 'superadmin', '2017-10-16 15:32:18.662', 'superadmin', '2017-10-16 15:32:18.662', NULL, NULL, NULL, NULL, NULL, true);
INSERT INTO "MF19_M_JOBMAST" VALUES ('CLRK', 'Clerk', 'Clerk', 'superadmin', '2017-10-23 11:10:09.408', 'DR', '2018-05-01 23:53:35.141', 'superadmin', '2017-10-23 11:10:19.793', NULL, NULL, NULL, false);

--*********************
--   MF20_M_ADMINMAST
--*********************
INSERT INTO "MF20_M_ADMINMAST" VALUES ('A_MORI1', '$2a$10$Qv1V69wBvImseKIh.0D8jOYcMqCT7CDy2FK7IrPnxYVRtgQ1h.GeS', '2018-05-01', '2019-05-31', true, 'superadmin', 'superadmin', NULL, NULL,'2018-05-08 12:07:49.311', '2018-05-08 12:08:15.56', NULL, NULL, 0, NULL, '$2a$10$4tXNpkXUZ.TZGgeqG/BauONn/DT8hJImDKkArZb5Xh/27I.TffWQi', false, NULL, 'morteza.siroi@gmail.com');
INSERT INTO "MF20_M_ADMINMAST" VALUES ('A_UBAID', '$2a$10$PXBgM9tGz061wLF3/TmMjOi09rXe.uRk8vmpE5FO4Pl7LIP0OYm4S', '2018-05-08', '2018-09-10', true, 'superadmin', 'superadmin', NULL, NULL,'2018-05-08 12:09:10.807', '2018-05-08 12:09:10.807', NULL, NULL, 0, NULL, '$2a$10$SdQNJHO.DxBcHeV5fJUaN.QO1rflJYhKpCy7GOZpRimhDHPwAgsoy', false, NULL, 'ubaid@gmail.com');
INSERT INTO "MF20_M_ADMINMAST" VALUES ('superadmin', '$2a$10$8jkMy4NxVn1XNC8LugrHPe1QxWgTmmbcEpZBCMgTTyqXahZEb64.y', NULL, NULL, true, 'superadmin', 'superadmin', NULL, NULL,'2017-09-12 15:19:02.651', '2017-09-12 15:19:02.651', NULL, NULL, 0, NULL, NULL, false, NULL, NULL);

--*********************
--   Mf22_M_SCHEDULER
--*********************


--*********************
--   PO06_T_USER
--*********************


--*********************
--   PO01_LOGINLIST
--*********************


--*********************
--   PO01_T_MAIN
--*********************


--*********************
--   menu
--*********************
INSERT INTO menu (menu_id, menu_code, menu_desc, menu_url, menu_reference) VALUES (1, 'user-menu', 'User Menu', '/#', 'user-menu');
INSERT INTO menu (menu_id, menu_code, menu_desc, menu_url, menu_reference) VALUES (2, 'user-management', 'User Management', '/#', 'user-management');
INSERT INTO menu (menu_id, menu_code, menu_desc, menu_url, menu_reference) VALUES (3, 'adminuser-management', 'Admin User Management', '/users/adminuser', 'admin-user-management');
INSERT INTO menu (menu_id, menu_code, menu_desc, menu_url, menu_reference) VALUES (4, 'pouser-management', 'PO User Management', '/users/pouser', 'pouser-management');
INSERT INTO menu (menu_id, menu_code, menu_desc, menu_url, menu_reference) VALUES (5, 'audit-trail', 'Audit Trail', '/#', 'audit-trail');
INSERT INTO menu (menu_id, menu_code, menu_desc, menu_url, menu_reference) VALUES (6, 'audit-log', 'Audit Log', '/audit/logs', 'audit-log');
INSERT INTO menu (menu_id, menu_code, menu_desc, menu_url, menu_reference) VALUES (7, 'audit-traillist', 'Audit Trail List', '/audit/audittraillist', 'audit-traillist');
INSERT INTO menu (menu_id, menu_code, menu_desc, menu_url, menu_reference) VALUES (8, 'master-management', 'Master File Management', '/#', 'master-management');
INSERT INTO menu (menu_id, menu_code, menu_desc, menu_url, menu_reference) VALUES (9, 'company', 'Company', '/master/company', 'company');
INSERT INTO menu (menu_id, menu_code, menu_desc, menu_url, menu_reference) VALUES (10, 'store', 'Store', '/master/store', 'store');
INSERT INTO menu (menu_id, menu_code, menu_desc, menu_url, menu_reference) VALUES (11, 'department', 'Department', '/master/department', 'department');
INSERT INTO menu (menu_id, menu_code, menu_desc, menu_url, menu_reference) VALUES (12, 'profitcenter', 'Profit Center', '/master/profitcenter', 'profitcenter');
INSERT INTO menu (menu_id, menu_code, menu_desc, menu_url, menu_reference) VALUES (13, 'gst', 'Goods & Services Tax (GST)', '/master/gst', 'gst');
INSERT INTO menu (menu_id, menu_code, menu_desc, menu_url, menu_reference) VALUES (14, 'purchase-requisition', 'Purchase Requisition', '/master/purchase', 'purchase-requisition');
INSERT INTO menu (menu_id, menu_code, menu_desc, menu_url, menu_reference) VALUES (15, 'order-category', 'Order Category', '/master/ordercategory', 'order-category');
INSERT INTO menu (menu_id, menu_code, menu_desc, menu_url, menu_reference) VALUES (16, 'order-subcategory', 'Order Sub Category', '/master/ordersubcategory', 'order-subcategory');
INSERT INTO menu (menu_id, menu_code, menu_desc, menu_url, menu_reference) VALUES (17, 'scheduler', 'Scheduler', '/master/scheduler', 'scheduler');
INSERT INTO menu (menu_id, menu_code, menu_desc, menu_url, menu_reference) VALUES (18, 'employee-management', 'Employee Management', '/#', 'employee-management');
INSERT INTO menu (menu_id, menu_code, menu_desc, menu_url, menu_reference) VALUES (19, 'employee', 'Employee', '/employee/emplist', 'employee');
INSERT INTO menu (menu_id, menu_code, menu_desc, menu_url, menu_reference) VALUES (20, 'job-master', 'Job Master', '/employee/jobmaster', 'job-master');
INSERT INTO menu (menu_id, menu_code, menu_desc, menu_url, menu_reference) VALUES (21, 'system-configuration', 'System Configuration', '/#', 'system-configuration');
INSERT INTO menu (menu_id, menu_code, menu_desc, menu_url, menu_reference) VALUES (22, 'masterdata', 'Master Data', '/system/masterdata', 'masterdata');
INSERT INTO menu (menu_id, menu_code, menu_desc, menu_url, menu_reference) VALUES (23, 'system-parameter', 'System Parameter', '/system/parameter', 'system-parameter');


--*********************
--   menu_grp
--*********************
INSERT INTO menu_grp (menu_grp_id, menu_grp_parent, menu_code, menu_desc, menu_url, menu_reference, menu_grp_root, menu_id, menu_grp_enable) VALUES (1, 0, 'user-menu', 'User menu', '/#', 'user-menu', 'user-menu', 1, true);
INSERT INTO menu_grp (menu_grp_id, menu_grp_parent, menu_code, menu_desc, menu_url, menu_reference, menu_grp_root, menu_id, menu_grp_enable) VALUES (2, 1, 'user-management', 'User Management', '/#', 'user-management', 'user-menu', 2, true);
INSERT INTO menu_grp (menu_grp_id, menu_grp_parent, menu_code, menu_desc, menu_url, menu_reference, menu_grp_root, menu_id, menu_grp_enable) VALUES (3, 2, 'adminuser-management', 'Admin User Management', '/users/adminuser', 'admin-user-management', 'user-menu', 3, true);
INSERT INTO menu_grp (menu_grp_id, menu_grp_parent, menu_code, menu_desc, menu_url, menu_reference, menu_grp_root, menu_id, menu_grp_enable) VALUES (4, 2, 'pouser-management', 'PO User Management', '/users/pouser', 'pouser-management', 'user-menu', 4, true);
INSERT INTO menu_grp (menu_grp_id, menu_grp_parent, menu_code, menu_desc, menu_url, menu_reference, menu_grp_root, menu_id, menu_grp_enable) VALUES (5, 1, 'audit-trail', 'Audit Trail', '/#', 'audit-trail', 'user-menu', 5, true);
INSERT INTO menu_grp (menu_grp_id, menu_grp_parent, menu_code, menu_desc, menu_url, menu_reference, menu_grp_root, menu_id, menu_grp_enable) VALUES (6, 5, 'audit-log', 'Audit Log', '/audit/logs', 'audit-log', 'user-menu', 6, true);
INSERT INTO menu_grp (menu_grp_id, menu_grp_parent, menu_code, menu_desc, menu_url, menu_reference, menu_grp_root, menu_id, menu_grp_enable) VALUES (7, 5, 'audit-traillist', 'Audit Trail List', '/audit/audittraillist', 'audit-traillist', 'user-menu', 7, true);
INSERT INTO menu_grp (menu_grp_id, menu_grp_parent, menu_code, menu_desc, menu_url, menu_reference, menu_grp_root, menu_id, menu_grp_enable) VALUES (8, 1, 'master-management', 'Master File Management', '/#', 'master-management', 'user-menu', 8, true);
INSERT INTO menu_grp (menu_grp_id, menu_grp_parent, menu_code, menu_desc, menu_url, menu_reference, menu_grp_root, menu_id, menu_grp_enable) VALUES (9, 8, 'company', 'Company', '/master/company', 'company', 'user-menu', 9, true);
INSERT INTO menu_grp (menu_grp_id, menu_grp_parent, menu_code, menu_desc, menu_url, menu_reference, menu_grp_root, menu_id, menu_grp_enable) VALUES (10, 8, 'store', 'Store', '/master/store', 'store', 'user-menu', 10, true);
INSERT INTO menu_grp (menu_grp_id, menu_grp_parent, menu_code, menu_desc, menu_url, menu_reference, menu_grp_root, menu_id, menu_grp_enable) VALUES (11, 8, 'department', 'Department', '/master/department', 'department', 'user-menu', 11, true);
INSERT INTO menu_grp (menu_grp_id, menu_grp_parent, menu_code, menu_desc, menu_url, menu_reference, menu_grp_root, menu_id, menu_grp_enable) VALUES (12, 8, 'profitcenter', 'Profit Center', '/master/profitcenter', 'profitcenter', 'user-menu', 12, true);
INSERT INTO menu_grp (menu_grp_id, menu_grp_parent, menu_code, menu_desc, menu_url, menu_reference, menu_grp_root, menu_id, menu_grp_enable) VALUES (13, 8, 'gst', 'Goods & Services Tax (GST)', '/master/gst', 'gst', 'user-menu', 13, true);
INSERT INTO menu_grp (menu_grp_id, menu_grp_parent, menu_code, menu_desc, menu_url, menu_reference, menu_grp_root, menu_id, menu_grp_enable) VALUES (14, 8, 'purchase-requisition', 'Purchase Requisition', '/master/purchase', 'purchase-requisition', 'user-menu', 14, true);
INSERT INTO menu_grp (menu_grp_id, menu_grp_parent, menu_code, menu_desc, menu_url, menu_reference, menu_grp_root, menu_id, menu_grp_enable) VALUES (15, 8, 'order-category', 'Order Category', '/master/ordercategory', 'order-category', 'user-menu', 15, true);
INSERT INTO menu_grp (menu_grp_id, menu_grp_parent, menu_code, menu_desc, menu_url, menu_reference, menu_grp_root, menu_id, menu_grp_enable) VALUES (16, 8, 'order-subcategory', 'Order Sub Category', '/master/ordersubcategory', 'order-subcategory', 'user-menu', 16, true);
INSERT INTO menu_grp (menu_grp_id, menu_grp_parent, menu_code, menu_desc, menu_url, menu_reference, menu_grp_root, menu_id, menu_grp_enable) VALUES (17, 8, 'scheduler', 'Scheduler', '/master/scheduler', 'scheduler', 'user-menu', 17, true);
INSERT INTO menu_grp (menu_grp_id, menu_grp_parent, menu_code, menu_desc, menu_url, menu_reference, menu_grp_root, menu_id, menu_grp_enable) VALUES (18, 1, 'employee-management', 'Employee Management', '/#', 'employee-management', 'user-menu', 18, true);
INSERT INTO menu_grp (menu_grp_id, menu_grp_parent, menu_code, menu_desc, menu_url, menu_reference, menu_grp_root, menu_id, menu_grp_enable) VALUES (19, 18, 'employee', 'Employee', '/employee/emplist', 'employee', 'user-menu', 19, true);
INSERT INTO menu_grp (menu_grp_id, menu_grp_parent, menu_code, menu_desc, menu_url, menu_reference, menu_grp_root, menu_id, menu_grp_enable) VALUES (20, 18, 'job-master', 'Job Master', '/employee/jobmaster', 'job-master', 'user-menu', 20, true);
INSERT INTO menu_grp (menu_grp_id, menu_grp_parent, menu_code, menu_desc, menu_url, menu_reference, menu_grp_root, menu_id, menu_grp_enable) VALUES (21, 1, 'system-configuration', 'System Configuration', '/#', 'system-configuration', 'user-menu', 21, true);
INSERT INTO menu_grp (menu_grp_id, menu_grp_parent, menu_code, menu_desc, menu_url, menu_reference, menu_grp_root, menu_id, menu_grp_enable) VALUES (22, 21, 'masterdata', 'Master Data', '/system/masterdata', 'masterdata', 'user-menu', 22, true);
INSERT INTO menu_grp (menu_grp_id, menu_grp_parent, menu_code, menu_desc, menu_url, menu_reference, menu_grp_root, menu_id, menu_grp_enable) VALUES (23, 21, 'system-parameter', 'System Parameter', '/system/parameter', 'system-parameter', 'user-menu', 23, true);


--*********************
--   menu_grp_role
--*********************
INSERT INTO menu_grp_role (id, menu_grp_id, role_id) VALUES (1, 1, 1);


--*********************
--   permissions
--*********************
INSERT INTO permissions (id, description, name) VALUES (1, NULL, 'MANAGE_AUDIT');
INSERT INTO permissions (id, description, name) VALUES (2, NULL, 'MANAGE_COMPANY');
INSERT INTO permissions (id, description, name) VALUES (3, NULL, 'MANAGE_DEPARTMENT');
INSERT INTO permissions (id, description, name) VALUES (4, NULL, 'MANAGE_EMPLOYEE_MANAGEMENT');
INSERT INTO permissions (id, description, name) VALUES (5, NULL, 'MANAGE_GST');
INSERT INTO permissions (id, description, name) VALUES (6, NULL, 'MANAGE_ORDER');
INSERT INTO permissions (id, description, name) VALUES (7, NULL, 'MANAGE_ROLE');
INSERT INTO permissions (id, description, name) VALUES (8, NULL, 'MANAGE_PERMISSION');
INSERT INTO permissions (id, description, name) VALUES (9, NULL, 'MANAGE_SETTINGS');
INSERT INTO permissions (id, description, name) VALUES (10, NULL, 'MANAGE_PROFIT_CENTER');
INSERT INTO permissions (id, description, name) VALUES (11, NULL, 'MANAGE_PURCHASE');
INSERT INTO permissions (id, description, name) VALUES (12, NULL, 'MANAGE_SCHEDULER');
INSERT INTO permissions (id, description, name) VALUES (13, NULL, 'MANAGE_STORE');
INSERT INTO permissions (id, description, name) VALUES (14, NULL, 'MANAGE_SYSTEM_PARAMETER');
INSERT INTO permissions (id, description, name) VALUES (15, NULL, 'MANAGE_USER');


--*********************
--   role_permission
--*********************
INSERT INTO role_permission (role_id, perm_id) VALUES (1, 1);
INSERT INTO role_permission (role_id, perm_id) VALUES (1, 2);
INSERT INTO role_permission (role_id, perm_id) VALUES (1, 3);
INSERT INTO role_permission (role_id, perm_id) VALUES (1, 4);
INSERT INTO role_permission (role_id, perm_id) VALUES (1, 5);
INSERT INTO role_permission (role_id, perm_id) VALUES (1, 6);
INSERT INTO role_permission (role_id, perm_id) VALUES (1, 7);
INSERT INTO role_permission (role_id, perm_id) VALUES (1, 8);
INSERT INTO role_permission (role_id, perm_id) VALUES (1, 9);
INSERT INTO role_permission (role_id, perm_id) VALUES (1, 10);
INSERT INTO role_permission (role_id, perm_id) VALUES (1, 11);
INSERT INTO role_permission (role_id, perm_id) VALUES (1, 12);
INSERT INTO role_permission (role_id, perm_id) VALUES (1, 13);
INSERT INTO role_permission (role_id, perm_id) VALUES (1, 14);
INSERT INTO role_permission (role_id, perm_id) VALUES (1, 15);
INSERT INTO role_permission (role_id, perm_id) VALUES (2, 1);
INSERT INTO role_permission (role_id, perm_id) VALUES (2, 2);
INSERT INTO role_permission (role_id, perm_id) VALUES (2, 3);
INSERT INTO role_permission (role_id, perm_id) VALUES (2, 4);
INSERT INTO role_permission (role_id, perm_id) VALUES (2, 5);
INSERT INTO role_permission (role_id, perm_id) VALUES (2, 6);
INSERT INTO role_permission (role_id, perm_id) VALUES (2, 9);
INSERT INTO role_permission (role_id, perm_id) VALUES (2, 10);
INSERT INTO role_permission (role_id, perm_id) VALUES (2, 11);
INSERT INTO role_permission (role_id, perm_id) VALUES (2, 12);
INSERT INTO role_permission (role_id, perm_id) VALUES (2, 13);
INSERT INTO role_permission (role_id, perm_id) VALUES (2, 14);
INSERT INTO role_permission (role_id, perm_id) VALUES (2, 15);
INSERT INTO role_permission (role_id, perm_id) VALUES (3, 1);
INSERT INTO role_permission (role_id, perm_id) VALUES (3, 2);
INSERT INTO role_permission (role_id, perm_id) VALUES (3, 3);
INSERT INTO role_permission (role_id, perm_id) VALUES (3, 4);
INSERT INTO role_permission (role_id, perm_id) VALUES (3, 5);
INSERT INTO role_permission (role_id, perm_id) VALUES (3, 6);
INSERT INTO role_permission (role_id, perm_id) VALUES (3, 11);
INSERT INTO role_permission (role_id, perm_id) VALUES (3, 12);


--*********************
--   roles
--*********************
INSERT INTO roles (role_id, role_description, role_name) VALUES (1, NULL, 'ROLE_SUPER_ADMIN');
INSERT INTO roles (role_id, role_description, role_name) VALUES (2, NULL, 'ROLE_ADMIN');
INSERT INTO roles (role_id, role_description, role_name) VALUES (3, NULL, 'ROLE_CMS_ADMIN');
INSERT INTO roles (role_id, role_description, role_name) VALUES (4, NULL, 'ROLE_USER');


--*********************
--   user_role
--*********************
INSERT INTO user_role (user_id, role_id) VALUES ('superadmin', 1);
INSERT INTO user_role (user_id, role_id) VALUES ('superadmin', 2);
INSERT INTO user_role (user_id, role_id) VALUES ('superadmin', 3);
INSERT INTO user_role (user_id, role_id) VALUES ('superadmin', 4);


--*********************
--   users
--*********************
