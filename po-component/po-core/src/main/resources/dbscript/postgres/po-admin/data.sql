-- **************************************
-- *    Populating TABLES				*
-- *------------------------------------*
-- *	LD02_T_ACSLOG					*
-- *	LD03_T_STGLOG					*
-- *	LD04_T_AUDIT					*
-- *	MF02_M_STRMAST					*
-- *	MF06_R_MASTDATA					*
-- *	MF07_R_SYSPARAM					*
-- *	MF11_M_COMPMAST					*
-- *	MF12_M_EMPMAST					*
-- *	MF13_M_PCMAST					*
-- *	MF14_M_DEPTMAST					*
-- *	MF15_M_PRMAST					*
-- *	MF16_M_GSTMAST					*
-- *	MF17_M_ORDCATMAST				*
-- *	MF18_M_ORDSUBCMAST				*
-- *	MF19_M_JOBMAST					*
-- *	MF20_M_ADMINMAST				*
-- *	mf04_m_profitcenterlastponumber	*
-- *	mf22_m_scheduler				*
-- *	po06_t_user						*
-- *	po_dblist2						*
-- **************************************


INSERT INTO "LD03_T_STGLOG" VALUES (1, 1);


--
-- TOC entry 2414 (class 0 OID 16403)
-- Dependencies: 203
-- Data for Name: LD04_T_AUDIT; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO "LD04_T_AUDIT" VALUES (195, 'superadmin', 'User superadmin with IP address 0:0:0:0:0:0:0:1 is signed in', 'Log In', '2018-02-11 17:35:24.398');
INSERT INTO "LD04_T_AUDIT" VALUES (196, '', '', '', '2018-02-11 18:13:25.796');
INSERT INTO "LD04_T_AUDIT" VALUES (197, '', '', '', '2018-02-11 18:13:25.833');
INSERT INTO "LD04_T_AUDIT" VALUES (198, '', '', '', '2018-02-11 18:13:25.845');
INSERT INTO "LD04_T_AUDIT" VALUES (199, '', '', '', '2018-02-11 18:13:28.872');
INSERT INTO "LD04_T_AUDIT" VALUES (200, '', '', '', '2018-02-11 18:13:28.874');
INSERT INTO "LD04_T_AUDIT" VALUES (201, '', '', '', '2018-02-11 18:13:28.876');
INSERT INTO "LD04_T_AUDIT" VALUES (202, 'superadmin', 'User superadmin with IP address 0:0:0:0:0:0:0:1 is signed in', 'Log In', '2018-02-16 21:17:53.457');


--
-- TOC entry 2415 (class 0 OID 16410)
-- Dependencies: 204
-- Data for Name: MF02_M_STRMAST; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO "MF02_M_STRMAST" VALUES ('0123', 'Park KLCC', 'Parkson KLCC', '080', 'superadmin', '2017-09-22 14:31:26.162', 'superadmin', '2017-09-22 14:31:26.162', NULL, NULL, NULL, NULL, true, NULL);
INSERT INTO "MF02_M_STRMAST" VALUES ('0125', 'WCKLCC', 'West Coast KLCC', '080', 'superadmin', '2017-09-23 14:27:14.404', 'superadmin', '2017-09-23 14:27:14.404', NULL, NULL, NULL, NULL, true, NULL);
INSERT INTO "MF02_M_STRMAST" VALUES ('0126', 'ACKLCC', 'Angela Corp KLCC', '080', 'A_ROMI1', '2017-09-23 20:04:55.389', 'A_ROMI1', '2017-09-23 20:04:55.389', NULL, NULL, NULL, NULL, true, NULL);
INSERT INTO "MF02_M_STRMAST" VALUES ('1111', 'StoreAbbrName#2', 'StoreName#2', '080', 'superadmin', '2017-11-07 09:56:39.179', 'superadmin', '2018-04-29 21:12:47.079', 'superadmin', '2017-11-07 09:56:55.249', 'superadmin', '2017-11-07 10:20:02.506', true, NULL);


--
-- TOC entry 2416 (class 0 OID 16416)
-- Dependencies: 205
-- Data for Name: MF06_R_MASTDATA; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO "MF06_R_MASTDATA" VALUES ('ssdsd', 'sdsdsd', 'sdsd', 'superadmin', '2017-10-16 15:37:15.012', false, 'superadmin', '2017-10-16 15:38:24.015', 'SNIKERS', NULL, 'superadmin', NULL, NULL, '2017-10-16 15:37:47.969+08', 6);
INSERT INTO "MF06_R_MASTDATA" VALUES ('INNA@', 'INNA@', 'INN@', 'superadmin', '2017-10-23 11:11:23.628', true, 'superadmin', '2017-10-23 11:13:16.582', '', NULL, NULL, NULL, NULL, NULL, 8);
INSERT INTO "MF06_R_MASTDATA" VALUES ('INNA', 'INNA', 'INNA', 'superadmin', '2017-10-23 11:13:30.175', false, 'superadmin', '2017-11-07 11:04:26.34', 'INNA', NULL, 'superadmin', 'superadmin', '2017-10-31 11:26:51.58+08', '2017-11-07 11:04:26.34+08', 9);


--
-- TOC entry 2419 (class 0 OID 16427)
-- Dependencies: 208
-- Data for Name: MF07_R_SYSPARAM; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO "MF07_R_SYSPARAM" VALUES ('superadmin', '2017-09-25 18:32:04.697', '', true, 'user123', '2017-09-25 18:32:04.697', 'MF_LDAPFULLURL', 'ldap://localhost:389/dc=parksondev,dc=com,dc=my', 'NTPO', NULL, NULL, NULL, NULL, NULL, 13);
INSERT INTO "MF07_R_SYSPARAM" VALUES ('superadmin', '2017-09-25 18:34:36.318', '', true, 'user123', '2017-09-25 18:34:36.318', 'MF_LDAPURL', 'ldap://localhost:389', '', NULL, NULL, NULL, NULL, NULL, 12);
INSERT INTO "MF07_R_SYSPARAM" VALUES ('superadmin', '2017-10-23 11:14:30.135', 'INNA', true, 'user123', '2017-10-23 11:14:30.135', 'MF_LDAPBASE', 'dc=parksondev,dc=com,dc=my', 'NTPO', NULL, NULL, NULL, NULL, NULL, 15);
INSERT INTO "MF07_R_SYSPARAM" VALUES ('superadmin', '2017-10-23 11:14:30.135', 'INNA', true, 'user123', '2017-10-23 11:14:30.135', 'MF_LDAPMGRDN', 'cn=admin,dc=parksondev,dc=com,dc=my', '', NULL, NULL, NULL, NULL, NULL, 16);
INSERT INTO "MF07_R_SYSPARAM" VALUES ('superadmin', '2017-10-23 11:14:30.135', 'INNA', true, 'user123', '2017-10-23 11:14:30.135', 'MF_LDAPMGRPASS', 'siroi12admin', '', NULL, NULL, NULL, NULL, NULL, 17);
INSERT INTO "MF07_R_SYSPARAM" VALUES ('superadmin', '2017-10-23 11:14:30.135', 'INNA', true, 'user123', '2017-10-23 11:14:30.135', 'FAILED_FOLDER_PATH', 'D:\\Failed Folder', 'NTPO', NULL, NULL, NULL, NULL, NULL, 24);
INSERT INTO "MF07_R_SYSPARAM" VALUES ('superadmin', '2017-10-30 15:33:20.596', 'YJ', true, 'user123', '2017-10-30 15:33:20.596', 'SUCCESS_FOLDER_PATH', 'D:\\Success Folder', '', NULL, NULL, NULL, NULL, NULL, 25);
INSERT INTO "MF07_R_SYSPARAM" VALUES ('superadmin', '2017-10-23 11:14:30.135', 'INNA', true, 'user123', '2017-10-23 11:14:30.135', 'LOG_FOLDER_PATH', 'D:\\Log Folder', 'NTPO', NULL, NULL, NULL, NULL, NULL, 26);
INSERT INTO "MF07_R_SYSPARAM" VALUES ('superadmin', '2017-10-30 15:33:20.596', 'YJ', true, 'user123', '2017-10-30 15:33:20.596', 'UPLOAD_FOLDER_PATH', 'D:\\My Folder', '', NULL, NULL, NULL, NULL, NULL, 27);
INSERT INTO "MF07_R_SYSPARAM" VALUES ('superadmin', '2017-10-30 15:33:20.596', 'YJ', true, 'user123', '2017-10-30 15:33:20.596', 'BACKUP_FOLDER_PATH', 'D:\\', '', NULL, NULL, NULL, NULL, NULL, 28);
INSERT INTO "MF07_R_SYSPARAM" VALUES ('superadmin', '2017-10-30 15:33:20.596', 'YJ', true, 'user123', '2017-10-30 15:33:20.596', 'PASSWORD_LIFETIME', '30', '', NULL, NULL, NULL, NULL, NULL, 29);
INSERT INTO "MF07_R_SYSPARAM" VALUES ('superadmin', '2017-10-30 15:33:20.596', 'YJ', true, 'user123', '2017-10-30 15:33:20.596', 'PASSWORD_NOTIF', '2', '', NULL, NULL, NULL, NULL, NULL, 30);
INSERT INTO "MF07_R_SYSPARAM" VALUES ('superadmin', '2017-10-30 15:33:20.596', 'YJ', true, 'user123', '2017-10-30 15:33:20.596', 'ACCT_LIFETIME', '30', '', NULL, NULL, NULL, NULL, NULL, 31);
INSERT INTO "MF07_R_SYSPARAM" VALUES ('superadmin', '2017-10-30 15:33:20.596', 'YJ', true, 'user123', '2017-10-30 15:33:20.596', 'ACCT_NOTIF', '2', '', NULL, NULL, NULL, NULL, NULL, 32);
INSERT INTO "MF07_R_SYSPARAM" VALUES ('superadmin', '2017-10-30 15:33:20.596', 'YJ', true, 'user123', '2017-10-30 15:33:20.596', 'M_DEFAULT_LOGO', 'img/logo.jpg', '', NULL, NULL, NULL, NULL, NULL, 35);


--
-- TOC entry 2422 (class 0 OID 16438)
-- Dependencies: 211
-- Data for Name: MF11_M_COMPMAST; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO "MF11_M_COMPMAST" VALUES ('081', 'Test', 'X-34466', 'admin', '2017-09-22 12:57:44.389', true, 'admin', '2017-10-03 15:37:54.336', 'ACS', 'Test', '/home/siroi/workspace/poAdmin/poAdmin-ui/uploaded files/company logo/siroi.jpg', '/home/siroi/workspace/poAdmin/poAdmin-ui/uploaded files/company letter heading/download.jpg', NULL, NULL, NULL, NULL, NULL, '/home/siroi/workspace/poAdmin/poAdmin-ui/uploaded files/terms and conditions/Policy.pdf', '/home/siroi/workspace/poAdmin/poAdmin-ui/uploaded files/terms and conditions/Policy.pdf');
INSERT INTO "MF11_M_COMPMAST" VALUES ('080', 'Siroiiiiiiiiii', 'X-34455', 'superadmin', '2018-04-29 22:45:02.648', true, 'superadmin', '2018-04-29 22:45:02.648', 'PCS', 'siroi solns', '/home/siroi/workspace/poAdmin/poAdmin-ui/uploaded files/company logo/siroi.jpg', '/home/siroi/workspace/poAdmin/poAdmin-ui/uploaded files/company letter heading/download.jpg', NULL, NULL, NULL, NULL, NULL, '/home/siroi/workspace/poAdmin/poAdmin-ui/uploaded files/terms and conditions/Policy.pdf', '/home/siroi/workspace/poAdmin/poAdmin-ui/uploaded files/terms and conditions/Policy.pdf');


--
-- TOC entry 2423 (class 0 OID 16444)
-- Dependencies: 212
-- Data for Name: MF12_M_EMPMAST; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO "MF12_M_EMPMAST" VALUES ('0002049', ' 524636', 'P052463', '2015-10-02', 'admin', '2017-07-04 15:16:07.014', 'PCS0000234', 'P0231-040 ', '1959-01-11', 'f@g.com', 'R', 'E Geak Neo', 'CHINSE', NULL, 'L', NULL, NULL, 'F', '2015-04-02', 'A', true, 'admin', '2017-07-04 15:16:07.014', 'P0231', 'Parkson Mahkota Parade', '2019-07-03', '2017-07-03 00:00:00', NULL, '590111045096', NULL, NULL, 'PSNONEXC', 'PCS', 'A', '4563563456', '00001362', 'Senior Cashier', '2017-07-03 00:00:00', NULL, '0002263', '00001360', 'E01', 'BE01', NULL, NULL, '2017-07-03', NULL, 'null', NULL, 'PCS', '0909233', 'ACCNT', NULL, NULL, NULL, NULL);
INSERT INTO "MF12_M_EMPMAST" VALUES ('0002039', ' 524636', 'P052463', '2015-10-02', 'admin', '2017-07-04 15:16:07.014', 'PCS0000234', 'P0231-040 ', '1959-01-11', 'a@f.com', 'R', 'E Geak Neo', 'CHINSE', NULL, 'L', NULL, NULL, 'F', '2015-04-02', 'A', true, 'admin', '2017-07-04 15:16:07.014', 'P0231', 'Parkson Mahkota Parade', '2019-07-03', '2017-07-03 00:00:00', NULL, '590111045096', NULL, NULL, 'PSNONEXC', 'PCS', 'A', '574674687478', '00001362', 'Senior Cashier', '2017-07-03 00:00:00', NULL, '0002263', '00001360', 'E01', 'BE01', NULL, NULL, '2017-07-03', NULL, 'null', NULL, 'PCS', '0000000', 'INNA2', NULL, NULL, NULL, NULL);
INSERT INTO "MF12_M_EMPMAST" VALUES ('0002040', ' 524636', 'P052463', '2015-10-02', 'admin', '2017-07-04 15:16:07.014', 'PCS0000234', 'P0231-040 ', '1959-01-11', 'a@gmail.com', 'R', 'E Geak Neo', 'CHINSE', NULL, 'L', NULL, NULL, 'F', '2015-04-02', 'A', true, 'admin', '2017-07-04 15:16:07.014', 'P0231', 'Parkson Mahkota Parade', '2019-07-03', '2017-07-03 00:00:00', NULL, '590111045096', NULL, NULL, 'PSNONEXC', 'PCS', 'A', '111111111111111', '00001362', 'Senior Cashier', '2017-07-03 00:00:00', NULL, '0002263', '00001360', 'E01', 'BE01', NULL, NULL, '2017-07-03', NULL, 'null', NULL, 'PCS', '1111333', 'INNA2', NULL, NULL, NULL, NULL);
INSERT INTO "MF12_M_EMPMAST" VALUES ('0002041', ' 524636', 'P052463', '2015-10-02', 'admin', '2017-07-04 15:16:07.014', 'PCS0000234', 'P0231-040 ', '1959-01-11', 'a@h.com', 'R', 'E Geak Neo', 'CHINSE', NULL, 'L', NULL, NULL, 'F', '2015-04-02', 'A', true, 'admin', '2017-07-04 15:16:07.014', 'P0231', 'Parkson Mahkota Parade', '2019-07-03', '2017-07-03 00:00:00', NULL, '590111045096', NULL, NULL, 'PSNONEXC', 'PCS', 'A', '78787878', '00001362', 'Senior Cashier', '2017-07-03 00:00:00', NULL, '0002263', '00001360', 'E01', 'BE01', NULL, NULL, '2017-07-03', NULL, 'null', NULL, 'PCS', '1111333', '111111', NULL, NULL, NULL, NULL);


--
-- TOC entry 2424 (class 0 OID 16450)
-- Dependencies: 213
-- Data for Name: MF13_M_PCMAST; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO "MF13_M_PCMAST" VALUES ('superadmin', '2017-11-07 10:18:32.697', 'superadmin', '2018-04-23 23:44:40.492', '0000000', 'ProfitCenterName#1', true, 'Profit Center #1 Address Line 1', 'Profit Center #1 Address Line 2', 'Profit Center #1 Address Line 3', 'PC1PosCode', 'Profit Center #1 Post Area', '0002039-E Geak Neo[Senior Cashier],0002040-E Geak Neo[Senior Cashier],0002041-E Geak Neo[Senior Cashier],]', '0002039-E Geak Neo[Senior Cashier],0002040-E Geak Neo[Senior Cashier],0002041-E Geak Neo[Senior Cashier],]', NULL, NULL, NULL, NULL, NULL, '080', '0125', '101', 'ProfCentAbrNa#1', 'W', '2017-11-07', '2017-11-28');
INSERT INTO "MF13_M_PCMAST" VALUES ('superadmin', '2017-10-24 09:49:56.137', 'superadmin', '2018-04-29 22:27:02.627', '0909233', 'Inna Profit Center 2', true, 'Endah2', 'Promenade2', 'C92', '570002', 'Sri Petaling2', ']', ']', 'superadmin', '2017-10-24 09:50:07.505', NULL, NULL, NULL, '080', '0126', '444', 'Inna Prof Cent2', 'S', '2017-10-24', '2017-10-31');
INSERT INTO "MF13_M_PCMAST" VALUES ('superadmin', '2017-11-07 10:24:11.082', 'superadmin', '2018-04-29 22:42:46.703', '1111333', 'ProfitCenterName#2', false, 'Profit Center #2 Address Line 1', 'Profit Center #2 Address Line 2', 'Profit Center #2 Address Line 3', 'PC2PosCode', 'Profit Center #2 Post Area', ']', ']', 'superadmin', '2017-11-07 10:24:39.278', NULL, NULL, NULL, '080', '0123', '101', 'PforCentAbrNa#2', 'S', '2017-11-07', '2017-11-14');


--
-- TOC entry 2425 (class 0 OID 16456)
-- Dependencies: 214
-- Data for Name: MF14_M_DEPTMAST; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO "MF14_M_DEPTMAST" VALUES ('003', 'IT Department', 'A_ROMI1', '2017-09-23 20:06:23.778', 'A_ROMI1', '2017-09-23 20:06:23.778', NULL, NULL, NULL, NULL, true, NULL, '01');
INSERT INTO "MF14_M_DEPTMAST" VALUES ('004', 'Procurement Department', 'A_ROMI1', '2017-09-23 20:07:24.346', 'A_ROMI1', '2017-09-23 20:07:24.346', NULL, NULL, NULL, NULL, true, NULL, '02');
INSERT INTO "MF14_M_DEPTMAST" VALUES ('444', 'Parkson Pavillion', 'superadmin', '2017-10-03 10:10:11.185', 'superadmin', '2017-10-03 10:10:11.185', NULL, NULL, NULL, NULL, true, NULL, 'Pav');
INSERT INTO "MF14_M_DEPTMAST" VALUES ('101', 'Parkson Malaysia2', 'superadmin', '2017-10-04 15:22:53.285', 'superadmin', '2017-10-04 15:53:05.131', NULL, NULL, NULL, NULL, true, NULL, 'PN1*&2');
INSERT INTO "MF14_M_DEPTMAST" VALUES ('014', 'sdafsdaaaaaaaaaaaa', 'superadmin', '2017-09-23 17:44:44.325', 'superadmin', '2018-04-29 22:42:05.064', NULL, NULL, NULL, NULL, true, NULL, '345');


--
-- TOC entry 2426 (class 0 OID 16462)
-- Dependencies: 215
-- Data for Name: MF15_M_PRMAST; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO "MF15_M_PRMAST" VALUES ('CAPEX', 'Captial Expenditures', 'superadmin', '2017-09-22 19:57:41.735', 'superadmin', '2017-09-22 19:57:41.735', NULL, NULL, NULL, NULL, 'CAPEX', true, NULL);
INSERT INTO "MF15_M_PRMAST" VALUES ('OX', 'Operational-Expenses', 'A_ROMI1', '2017-09-23 20:38:18.748', 'A_ROMI1', '2017-09-23 20:38:18.748', NULL, NULL, NULL, NULL, 'OPEX', true, NULL);
INSERT INTO "MF15_M_PRMAST" VALUES ('PurchaseReqAbbrName#1', 'Purchase Requisition Description #1', 'superadmin', '2017-11-07 10:34:02.679', 'superadmin', '2017-11-07 10:34:02.679', NULL, NULL, NULL, NULL, 'PURREQTY#1', true, NULL);
INSERT INTO "MF15_M_PRMAST" VALUES ('PurchaseReqAbbrName#2', 'Purchase Requisition Description2', 'superadmin', '2017-11-07 10:36:14.952', 'superadmin', '2017-11-07 10:36:29.296', 'superadmin', '2017-11-07 10:36:29.296', NULL, NULL, 'PURREQTY#2', false, NULL);


--
-- TOC entry 2427 (class 0 OID 16468)
-- Dependencies: 216
-- Data for Name: MF16_M_GSTMAST; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO "MF16_M_GSTMAST" VALUES ('GST11111', 'TextTextTextText', '2017-10-11', '2017-10-31', 'superadmin', '2017-10-04 16:31:21.974', 'superadmin', '2017-10-04 16:31:21.974', NULL, NULL, NULL, NULL, 'Tax Description', true, NULL, 10.0000, 0.0000);
INSERT INTO "MF16_M_GSTMAST" VALUES ('TXCODE1', 'Tax Code Description #1', '2017-11-07', '2017-11-14', 'superadmin', '2017-11-07 10:28:04.836', 'superadmin', '2017-11-07 10:28:04.836', NULL, NULL, NULL, NULL, 'Tax Abbr Description #1', true, NULL, 0.0000, 0.0000);
INSERT INTO "MF16_M_GSTMAST" VALUES ('TXCODE2', 'Tax Code Description #3', '2017-11-14', '2017-11-21', 'superadmin', '2017-11-07 10:31:07.95', 'superadmin', '2018-04-29 21:07:49.825', 'superadmin', '2017-11-07 10:31:42.115', NULL, NULL, 'Tax Code Abbr Description #3', false, NULL, 1.0000, 1.0000);
INSERT INTO "MF16_M_GSTMAST" VALUES ('DISCOUNTFLOWERS00001', 'When purchase 10 flowers discount is 10%', '2017-10-17', '2017-10-31', 'superadmin', '2017-10-16 14:38:46.075', 'superadmin', '2018-04-29 22:43:36.561', 'superadmin', '2017-10-16 14:39:36.15', NULL, NULL, 'FLOWERS000011111111111', true, NULL, 1.0000, 1.0000);


--
-- TOC entry 2428 (class 0 OID 16474)
-- Dependencies: 217
-- Data for Name: MF17_M_ORDCATMAST; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO "MF17_M_ORDCATMAST" VALUES ('REQ-NEW STORE-IT', 'This is for New Store especially for IT', 'uploaded files/authorization grid/Sample Authorization Grid.pdf', 'A_ROMI1', '2017-09-23 20:43:28.342', 'A_ROMI1', '2017-09-23 20:43:28.342', NULL, NULL, NULL, NULL, NULL, 'R-NS-IT', 'CAPEX', true);
INSERT INTO "MF17_M_ORDCATMAST" VALUES ('REQUISITION_OF_PERFUME#2', 'Need to buy perfume for .................', 'uploaded files/authorization grid/geek-inside.sh-600x600.png', 'superadmin', '2017-10-16 15:02:55.684', 'superadmin', '2017-10-16 15:02:55.684', NULL, NULL, NULL, NULL, NULL, 'PERFUME', 'OPEX', true);
INSERT INTO "MF17_M_ORDCATMAST" VALUES ('ORDERCATCODE#1', 'Order Category Description #1', 'uploaded files/authorization grid/scan_002.png', 'superadmin', '2017-11-07 10:46:00.975', 'superadmin', '2017-11-07 10:46:00.975', NULL, NULL, NULL, NULL, NULL, 'OrderCatAbbrDesc#1', 'PURREQTY#1', true);
INSERT INTO "MF17_M_ORDCATMAST" VALUES ('ORDERCATCODE#2', 'order category description #2', 'uploaded files/authorization grid/Intuglo1.png', 'superadmin', '2017-11-07 10:48:21.515', 'superadmin', '2017-11-07 10:49:19.967', 'superadmin', '2017-11-07 10:49:19.967', NULL, NULL, NULL, 'OrderCatAbbrDesc#2', 'PURREQTY#2', false);


--
-- TOC entry 2429 (class 0 OID 16480)
-- Dependencies: 218
-- Data for Name: MF18_M_ORDSUBCMAST; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO "MF18_M_ORDSUBCMAST" VALUES ('Care supplies1', 'Cosmetics, skin care etc. *@&2', 'REQ-NEW STORE-IT', 'superadmin', '2017-10-05 10:40:47.003', '2017-10-05 10:59:22.155', 'superadmin', '2017-10-05 10:59:22.155', NULL, NULL, NULL, 'superadmin', 'Health Care1@', false);
INSERT INTO "MF18_M_ORDSUBCMAST" VALUES ('InnaSubCode2', 'Inna Sub Category Desc2', 'REQUISITION_OF_PERFUME#2', 'superadmin', '2017-10-24 11:14:03.95', '2017-10-24 11:14:30.664', 'superadmin', '2017-10-24 11:14:30.664', NULL, NULL, NULL, 'superadmin', 'Inna Sub Category Abbr2', false);
INSERT INTO "MF18_M_ORDSUBCMAST" VALUES ('OrdSubCatCode#1', 'Order Sub Category Desc #1', 'ORDERCATCODE#1', 'superadmin', '2017-11-07 10:52:21.963', '2017-11-07 10:52:21.963', NULL, NULL, NULL, NULL, NULL, 'superadmin', 'Order Sub Cat Abbreviation #1', true);
INSERT INTO "MF18_M_ORDSUBCMAST" VALUES ('OrdSubCatCode#2', 'Order Sub Category Description #2', 'ORDERCATCODE#2', 'superadmin', '2017-11-07 10:53:20.807', '2017-11-07 10:53:20.807', NULL, NULL, NULL, NULL, NULL, 'superadmin', 'Order Sub Category Abbr #2', true);


--
-- TOC entry 2430 (class 0 OID 16486)
-- Dependencies: 219
-- Data for Name: MF19_M_JOBMAST; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO "MF19_M_JOBMAST" VALUES ('Test77', 'Network Administrator!@3', 'NetADMIN', 'superadmin', '2017-10-05 11:58:00.736', 'superadmin', '2017-10-05 11:58:00.736', NULL, NULL, NULL, NULL, NULL, true);
INSERT INTO "MF19_M_JOBMAST" VALUES ('ACCNT', 'Accounter', 'Accounter', 'superadmin', '2017-10-16 15:32:18.662', 'superadmin', '2017-10-16 15:32:18.662', NULL, NULL, NULL, NULL, NULL, true);
INSERT INTO "MF19_M_JOBMAST" VALUES ('INNA2', 'JobName2', 'Job Abbr 2', 'superadmin', '2017-10-23 11:10:09.408', 'superadmin', '2017-10-24 11:30:08.037', 'superadmin', '2017-10-23 11:10:19.793', NULL, NULL, NULL, false);
INSERT INTO "MF19_M_JOBMAST" VALUES ('111111', 'JobName#2', 'JobAbbr#2', 'superadmin', '2017-11-07 11:03:23.908', 'superadmin', '2017-11-07 11:03:23.908', NULL, NULL, NULL, NULL, NULL, true);


--
-- TOC entry 2431 (class 0 OID 16492)
-- Dependencies: 220
-- Data for Name: MF20_M_ADMINMAST; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO "MF20_M_ADMINMAST" VALUES ('A_INNAA', '827ccb0eea8a706c4c34a16891f84e7b', '2017-10-16', '2017-12-07', false, '2017-10-16 11:32:38.089', '2017-10-16 11:35:23.525', '2017-10-16 11:35:23.525', NULL, 0, NULL, '[827ccb0eea8a706c4c34a16891f84e7b]', true, NULL);
INSERT INTO "MF20_M_ADMINMAST" VALUES ('superadmin', '$2a$08$vrmtYm/9JQAIvU77MxouFeeVP7v.15Z7hE0ML8m9CxCmSTVF1Gtky', NULL, NULL, true, '2017-09-12 15:19:02.651', '2017-09-12 15:19:02.651', NULL, NULL, 0, NULL, NULL, false, NULL);
INSERT INTO "MF20_M_ADMINMAST" VALUES ('A_BEH12', 'c20ad4d76fe97759aa27a0c99bff6710', '2017-09-22', '2017-09-22', true, '2017-09-22 12:55:52.807', '2018-04-29 21:05:35.29', NULL, '2018-04-29 21:05:35.29', 0, NULL, '[c20ad4d76fe97759aa27a0c99bff6710]', true, NULL);
INSERT INTO "MF20_M_ADMINMAST" VALUES ('A_ROMI1', '3d9d8555cc65310173c1129790169c1d', '2017-09-23', '2017-09-23', true, '2017-09-23 18:36:51.698', '2018-04-29 21:05:45.888', NULL, '2018-04-29 21:05:45.888', 0, NULL, '[3d9d8555cc65310173c1129790169c1d]', true, NULL);
INSERT INTO "MF20_M_ADMINMAST" VALUES ('A_MORI2', 'bb8e84bcd669d6b5a5ae44993566e8c5', '2017-10-05', '2017-10-05', true, '2017-10-05 15:31:11.453', '2018-04-29 22:37:21.646', NULL, '2018-04-29 22:37:21.646', 1, NULL, '[bb8e84bcd669d6b5a5ae44993566e8c5]', true, NULL);


--
-- TOC entry 2447 (class 0 OID 33805)
-- Dependencies: 245
-- Data for Name: Mf22_M_SCHEDULER; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--



--
-- TOC entry 2432 (class 0 OID 16511)
-- Dependencies: 221
-- Data for Name: PO03_T_PROJECT; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--



--
-- TOC entry 2433 (class 0 OID 16519)
-- Dependencies: 222
-- Data for Name: PO06_T_USER; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO "PO06_T_USER" VALUES ('0002050', 'E Geak Neo', NULL, NULL, 0, true, 'superadmin', '2017-10-30 12:10:17.398', 'superadmin', '2017-10-30 12:11:00.18', NULL, NULL, 'superadmin', '2017-10-30 12:11:00.18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "PO06_T_USER" VALUES ('0002041', 'E Geak Neo', NULL, NULL, 0, true, 'superadmin', '2017-10-30 12:11:38.761', 'superadmin', '2017-10-30 12:11:46.03', NULL, NULL, 'superadmin', '2017-10-30 12:11:46.03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "PO06_T_USER" VALUES ('0002042', 'E Geak Neo', NULL, NULL, 0, true, 'superadmin', '2017-10-30 15:23:15.178', 'superadmin', '2017-11-06 09:46:53.092', NULL, NULL, 'superadmin', '2017-11-06 09:46:53.092', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "PO06_T_USER" VALUES ('0002039', 'E Geak Neo', NULL, NULL, 0, false, NULL, NULL, 'superadmin', '2018-04-29 22:37:36.531', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "PO06_T_USER" VALUES ('0002049', 'aaaa', NULL, NULL, 0, true, 'superadmin', '2018-04-29 22:38:00.212', 'superadmin', '2018-04-29 22:38:00.212', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);


--
-- TOC entry 2444 (class 0 OID 33771)
-- Dependencies: 242
-- Data for Name: menu; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO "menu" VALUES (1, 'user-menu', 'User Menu', '/#', 'user-menu');
INSERT INTO "menu" VALUES (2, 'user-management', 'User Management', '/#', 'user-management');
INSERT INTO "menu" VALUES (3, 'adminuser-management', 'Admin User Management', '/users/adminuser', 'admin-user-management');
INSERT INTO "menu" VALUES (4, 'pouser-management', 'PO User Management', '/users/pouser', 'pouser-management');
INSERT INTO "menu" VALUES (5, 'audit-trail', 'Audit Trail', '/#', 'audit-trail');
INSERT INTO "menu" VALUES (6, 'audit-log', 'Audit Log', '/audit/logs', 'audit-log');
INSERT INTO "menu" VALUES (7, 'audit-traillist', 'Audit Trail List', '/audit/audittraillist', 'audit-traillist');
INSERT INTO "menu" VALUES (8, 'master-management', 'Master File Management', '/#', 'master-management');
INSERT INTO "menu" VALUES (9, 'company', 'Company', '/master/company', 'company');
INSERT INTO "menu" VALUES (10, 'store', 'Store', '/master/store', 'store');
INSERT INTO "menu" VALUES (11, 'department', 'Department', '/master/department', 'department');
INSERT INTO "menu" VALUES (12, 'profitcenter', 'Profit Center', '/master/profitcenter', 'profitcenter');
INSERT INTO "menu" VALUES (13, 'gst', 'Goods & Services Tax (GST)', '/master/gst', 'gst');
INSERT INTO "menu" VALUES (14, 'purchase-requisition', 'Purchase Requisition', '/master/purchase', 'purchase-requisition');
INSERT INTO "menu" VALUES (15, 'order-category', 'Order Category', '/master/ordercategory', 'order-category');
INSERT INTO "menu" VALUES (16, 'order-subcategory', 'Order Sub Category', '/master/ordersubcategory', 'order-subcategory');
INSERT INTO "menu" VALUES (17, 'scheduler', 'Scheduler', '/master/scheduler', 'scheduler');
INSERT INTO "menu" VALUES (18, 'employee-management', 'Employee Management', '/#', 'employee-management');
INSERT INTO "menu" VALUES (19, 'employee', 'Employee', '/employee/emplist', 'employee');
INSERT INTO "menu" VALUES (20, 'job-master', 'Job Master', '/employee/jobmaster', 'job-master');
INSERT INTO "menu" VALUES (21, 'system-configuration', 'System Configuration', '/#', 'system-configuration');
INSERT INTO "menu" VALUES (22, 'masterdata', 'Master Data', '/system/masterdata', 'masterdata');
INSERT INTO "menu" VALUES (23, 'system-parameter', 'System Parameter', '/system/parameter', 'system-parameter');


--
-- TOC entry 2445 (class 0 OID 33780)
-- Dependencies: 243
-- Data for Name: menu_grp; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO "menu_grp" VALUES (1, 0, 'user-menu', 'User menu', '/#', 'user-menu', 'user-menu', 1, true);
INSERT INTO "menu_grp" VALUES (2, 1, 'user-management', 'User Management', '/#', 'user-management', 'user-menu', 2, true);
INSERT INTO "menu_grp" VALUES (3, 2, 'adminuser-management', 'Admin User Management', '/users/adminuser', 'admin-user-management', 'user-menu', 3, true);
INSERT INTO "menu_grp" VALUES (4, 2, 'pouser-management', 'PO User Management', '/users/pouser', 'pouser-management', 'user-menu', 4, true);
INSERT INTO "menu_grp" VALUES (5, 1, 'audit-trail', 'Audit Trail', '/#', 'audit-trail', 'user-menu', 5, true);
INSERT INTO "menu_grp" VALUES (6, 5, 'audit-log', 'Audit Log', '/audit/logs', 'audit-log', 'user-menu', 6, true);
INSERT INTO "menu_grp" VALUES (7, 5, 'audit-traillist', 'Audit Trail List', '/audit/audittraillist', 'audit-traillist', 'user-menu', 7, true);
INSERT INTO "menu_grp" VALUES (8, 1, 'master-management', 'Master File Management', '/#', 'master-management', 'user-menu', 8, true);
INSERT INTO "menu_grp" VALUES (9, 8, 'company', 'Company', '/master/company', 'company', 'user-menu', 9, true);
INSERT INTO "menu_grp" VALUES (10, 8, 'store', 'Store', '/master/store', 'store', 'user-menu', 10, true);
INSERT INTO "menu_grp" VALUES (11, 8, 'department', 'Department', '/master/department', 'department', 'user-menu', 11, true);
INSERT INTO "menu_grp" VALUES (12, 8, 'profitcenter', 'Profit Center', '/master/profitcenter', 'profitcenter', 'user-menu', 12, true);
INSERT INTO "menu_grp" VALUES (13, 8, 'gst', 'Goods & Services Tax (GST)', '/master/gst', 'gst', 'user-menu', 13, true);
INSERT INTO "menu_grp" VALUES (14, 8, 'purchase-requisition', 'Purchase Requisition', '/master/purchase', 'purchase-requisition', 'user-menu', 14, true);
INSERT INTO "menu_grp" VALUES (15, 8, 'order-category', 'Order Category', '/master/ordercategory', 'order-category', 'user-menu', 15, true);
INSERT INTO "menu_grp" VALUES (16, 8, 'order-subcategory', 'Order Sub Category', '/master/ordersubcategory', 'order-subcategory', 'user-menu', 16, true);
INSERT INTO "menu_grp" VALUES (17, 8, 'scheduler', 'Scheduler', '/master/scheduler', 'scheduler', 'user-menu', 17, true);
INSERT INTO "menu_grp" VALUES (18, 1, 'employee-management', 'Employee Management', '/#', 'employee-management', 'user-menu', 18, true);
INSERT INTO "menu_grp" VALUES (19, 18, 'employee', 'Employee', '/employee/emplist', 'employee', 'user-menu', 19, true);
INSERT INTO "menu_grp" VALUES (20, 18, 'job-master', 'Job Master', '/employee/jobmaster', 'job-master', 'user-menu', 20, true);
INSERT INTO "menu_grp" VALUES (21, 1, 'system-configuration', 'System Configuration', '/#', 'system-configuration', 'user-menu', 21, true);
INSERT INTO "menu_grp" VALUES (22, 21, 'masterdata', 'Master Data', '/system/masterdata', 'masterdata', 'user-menu', 22, true);
INSERT INTO "menu_grp" VALUES (23, 21, 'system-parameter', 'System Parameter', '/system/parameter', 'system-parameter', 'user-menu', 23, true);


--
-- TOC entry 2446 (class 0 OID 33789)
-- Dependencies: 244
-- Data for Name: menu_grp_role; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO "menu_grp_role" VALUES (1, 1, 1);


--
-- TOC entry 2435 (class 0 OID 32938)
-- Dependencies: 225
-- Data for Name: permissions; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO "permissions" VALUES (1, NULL, 'MANAGE_AUDIT');
INSERT INTO "permissions" VALUES (2, NULL, 'MANAGE_COMPANY');
INSERT INTO "permissions" VALUES (3, NULL, 'MANAGE_DEPARTMENT');
INSERT INTO "permissions" VALUES (4, NULL, 'MANAGE_EMPLOYEE_MANAGEMENT');
INSERT INTO "permissions" VALUES (5, NULL, 'MANAGE_GST');
INSERT INTO "permissions" VALUES (6, NULL, 'MANAGE_ORDER');
INSERT INTO "permissions" VALUES (7, NULL, 'MANAGE_ROLE');
INSERT INTO "permissions" VALUES (8, NULL, 'MANAGE_PERMISSION');
INSERT INTO "permissions" VALUES (9, NULL, 'MANAGE_SETTINGS');
INSERT INTO "permissions" VALUES (10, NULL, 'MANAGE_PROFIT_CENTER');
INSERT INTO "permissions" VALUES (11, NULL, 'MANAGE_PURCHASE');
INSERT INTO "permissions" VALUES (12, NULL, 'MANAGE_SCHEDULER');
INSERT INTO "permissions" VALUES (13, NULL, 'MANAGE_STORE');
INSERT INTO "permissions" VALUES (14, NULL, 'MANAGE_SYSTEM_PARAMETER');
INSERT INTO "permissions" VALUES (15, NULL, 'MANAGE_USER');


--
-- TOC entry 2436 (class 0 OID 32986)
-- Dependencies: 226
-- Data for Name: role_permission; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO "role_permission" VALUES (1, 1);
INSERT INTO "role_permission" VALUES (1, 2);
INSERT INTO "role_permission" VALUES (1, 3);
INSERT INTO "role_permission" VALUES (1, 4);
INSERT INTO "role_permission" VALUES (1, 5);
INSERT INTO "role_permission" VALUES (1, 6);
INSERT INTO "role_permission" VALUES (1, 7);
INSERT INTO "role_permission" VALUES (1, 8);
INSERT INTO "role_permission" VALUES (1, 9);
INSERT INTO "role_permission" VALUES (1, 10);
INSERT INTO "role_permission" VALUES (1, 11);
INSERT INTO "role_permission" VALUES (1, 12);
INSERT INTO "role_permission" VALUES (1, 13);
INSERT INTO "role_permission" VALUES (1, 14);
INSERT INTO "role_permission" VALUES (1, 15);
INSERT INTO "role_permission" VALUES (2, 1);
INSERT INTO "role_permission" VALUES (2, 2);
INSERT INTO "role_permission" VALUES (2, 3);
INSERT INTO "role_permission" VALUES (2, 4);
INSERT INTO "role_permission" VALUES (2, 5);
INSERT INTO "role_permission" VALUES (2, 6);
INSERT INTO "role_permission" VALUES (2, 9);
INSERT INTO "role_permission" VALUES (2, 10);
INSERT INTO "role_permission" VALUES (2, 11);
INSERT INTO "role_permission" VALUES (2, 12);
INSERT INTO "role_permission" VALUES (2, 13);
INSERT INTO "role_permission" VALUES (2, 14);
INSERT INTO "role_permission" VALUES (3, 1);
INSERT INTO "role_permission" VALUES (3, 2);
INSERT INTO "role_permission" VALUES (3, 3);
INSERT INTO "role_permission" VALUES (3, 4);
INSERT INTO "role_permission" VALUES (3, 5);
INSERT INTO "role_permission" VALUES (3, 6);
INSERT INTO "role_permission" VALUES (3, 11);
INSERT INTO "role_permission" VALUES (3, 12);


--
-- TOC entry 2437 (class 0 OID 32989)
-- Dependencies: 227
-- Data for Name: roles; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO "roles" VALUES (1, NULL, 'ROLE_SUPER_ADMIN');
INSERT INTO "roles" VALUES (2, NULL, 'ROLE_ADMIN');
INSERT INTO "roles" VALUES (3, NULL, 'ROLE_CMS_ADMIN');
INSERT INTO "roles" VALUES (4, NULL, 'ROLE_USER');


--
-- TOC entry 2448 (class 0 OID 33822)
-- Dependencies: 247
-- Data for Name: user_role; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO "user_role" VALUES ('superadmin', 1);
INSERT INTO "user_role" VALUES ('superadmin', 2);
INSERT INTO "user_role" VALUES ('superadmin', 3);
INSERT INTO "user_role" VALUES ('superadmin', 4);


--
-- TOC entry 2438 (class 0 OID 33000)
-- Dependencies: 228
-- Data for Name: users; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO "users" VALUES (1, 'superadmin@gmail.com', 'SuperAdmin', '$2a$08$vrmtYm/9JQAIvU77MxouFeeVP7v.15Z7hE0ML8m9CxCmSTVF1Gtky', NULL);
INSERT INTO "users" VALUES (2, 'admin@gmail.com', 'Admin', '$2a$10$K3sjfsi0AfBX1zykQivikupRFsJC9JB0Coh0UkHKlnC88FFet8VTu', NULL);
INSERT INTO "users" VALUES (3, 'cmsadmin@gmail.com', 'CMSAdmin', '$2a$10$FGLfNDzhY6hBvf5P8tbSo.HrxWSwn/MJFTb0FmnsEe2YKb5biOYeu', NULL);
INSERT INTO "users" VALUES (4, 'user@gmail.com', 'DemoUser', '$2a$10$cx3jHJEuim84lmk82cvewuK3XFP8Q2M1ZZVsPJ9JhrWiYTvtgWG/G', NULL);
INSERT INTO "users" VALUES (5, 'ranjanlaisi@gmail.com', 'Ranjan', '$2a$10$K3sjfsi0AfBX1zykQivikupRFsJC9JB0Coh0UkHKlnC88FFet8VTu', NULL);
INSERT INTO "users" VALUES (6, 'sagar.subhash@gmail.com', 'Subhash', '$2a$08$vrmtYm/9JQAIvU77MxouFeeVP7v.15Z7hE0ML8m9CxCmSTVF1Gtky', NULL);

