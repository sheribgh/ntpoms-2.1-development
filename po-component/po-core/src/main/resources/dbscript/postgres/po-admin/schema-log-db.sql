
SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;


CREATE SCHEMA mfmaindb;

ALTER SCHEMA mfmaindb OWNER TO postgres;

SET search_path = mfmaindb, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;


-- **************************************
-- *         CREATING TABLES			*
-- *------------------------------------*
-- *	LD02_T_ACSLOG					*
-- *	LD03_T_STGLOG					*
-- **************************************
  

-- Remove Database Objects

drop trigger if exists insert_acslog_trigger on mfmaindb.ld02_t_acslog; 
drop table if exists mfmaindb."ld02_t_acslog" cascade;
drop function if exists mfmaindb.acslog_insert();
	
drop sequence if exists mfmaindb.ld02_t_acslog_sequence cascade;
drop table if exists mfmaindb."ld03_t_stglog" cascade;
drop sequence if exists mfmaindb.ld03_t_stglog_sequence cascade;











-- Creating Setting Log

CREATE SEQUENCE mfmaindb.ld03_t_stglog_sequence
	INCREMENT 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 87812
	CACHE 1;
	
ALTER TABLE mfmaindb.ld03_t_stglog_sequence OWNER TO postgres;


-- create stglog table
CREATE TABLE mfmaindb."ld03_t_stglog"
(
  "ld03_logsetting" integer,
  "ld03_logsettingid" integer NOT NULL DEFAULT nextval('mfmaindb."ld03_t_stglog"'::regclass),
  CONSTRAINT "ld03_t_stglog_pkey" PRIMARY KEY ("ld03_logsettingid")
)
WITH (
  OIDS=FALSE
);
ALTER TABLE mfmaindb."ld03_t_stglog"
  OWNER TO postgres;
  


  
  
  
  
-- Creating Access Log

CREATE SEQUENCE mfmaindb.ld02_t_acslog_sequence
	INCREMENT 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 87812
	CACHE 1;
	
ALTER TABLE mfmaindb.ld02_t_acslog_sequence OWNER TO postgres;


-- create ld02_t_acslog table (master)
CREATE TABLE mfmaindb."ld02_t_acslog"
(
  "ld02_logid" integer NOT NULL DEFAULT nextval('mfmaindb.ld02_t_acslog_sequence'::regclass),
  "ld02_dbname" character varying(100),
  "ld02_classfilename" character varying(255),
  "ld02_lineno" integer NOT NULL,
  "ld02_loglevel" character varying(255),
  "ld02_logmessage" character varying(511),
  "ld02_logtime" timestamp without time zone,
  "ld02_screenid" character varying(100),
  CONSTRAINT "ld02_t_acslog_pkey" PRIMARY KEY ("ld02_logid")
)
WITH (
  OIDS=FALSE
);
ALTER TABLE mfmaindb."ld02_t_acslog"
  OWNER TO postgres;
  
  
  
  
  
  
  
  
-- Creating Log Insertion Function

create or replace function mfmaindb.acslog_insert() 
returns trigger as $$
declare
	table_name varchar;
	date_timestamp timestamp;
	logtime_month int;
	logtime_year int;
begin
	SET datestyle = 'DMY';
	date_timestamp := CAST(new."ld02_logtime" as timestamp without time zone);
	logtime_month := extract(month from date_timestamp);
	logtime_year := extract(year from date_timestamp);
	table_name := format('ld02_t_acslog_%s_%s', logtime_year, logtime_month);
	perform 1 from pg_class where relname = table_name limit 1;
	if not found
	then
		execute format('create table mfmaindb."%s" (check (extract(year from ld02_logtime) = %s and extract(month from ld02_logtime) = %s)) inherits (mfmaindb."ld02_t_acslog")'
				, table_name,  logtime_year, logtime_month);
		execute format('create index idx_%s_logtime on mfmaindb.%s (ld02_logtime);', table_name, table_name);
	end if;
	execute 'insert into mfmaindb.' || table_name || ' values ( ($1).* )' using new;
	return null;
end;
$$ 
language plpgsql;






-- Creating Log Access Trigger

create trigger insert_acslog_trigger before insert on mfmaindb."ld02_t_acslog" for each row execute procedure mfmaindb.acslog_insert();