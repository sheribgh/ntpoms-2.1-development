-- **************************************
-- *         CREATING TABLES			*
-- *------------------------------------*
-- *	LD02_T_ACSLOG					*
-- *	LD03_T_STGLOG					*
-- *	LD04_T_AUDIT					*
-- *	MF02_M_STRMAST					*
-- *	MF06_R_MASTDATA					*
-- *	MF07_R_SYSPARAM					*
-- *	MF11_M_COMPMAST					*
-- *	MF12_M_EMPMAST					*
-- *	MF13_M_PCMAST					*
-- *	MF14_M_DEPTMAST					*
-- *	MF15_M_PRMAST					*
-- *	MF16_M_GSTMAST					*
-- *	MF17_M_ORDCATMAST				*
-- *	MF18_M_ORDSUBCMAST				*
-- *	MF19_M_JOBMAST					*
-- *	MF20_M_ADMINMAST				*
-- *	mf04_m_profitcenterlastponumber			*
-- *	mf22_m_scheduler				*
-- *	PO03_T_PROJECT					*
-- *	po06_t_user					*
-- *	po_dblist2					*
-- **************************************
  
   
--
-- PostgreSQL database dump
--

-- Dumped from database version 10.1
-- Dumped by pg_dump version 10.1

-- Started on 2018-05-01 01:52:04

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 8 (class 2615 OID 16385)
-- Name: mfmaindb; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA "mfmaindb";


ALTER SCHEMA "mfmaindb" OWNER TO "postgres";

SET search_path = "mfmaindb", pg_catalog;

--
-- TOC entry 198 (class 1259 OID 16386)
-- Name: ld02_t_acslog_sequence; Type: SEQUENCE; Schema: mfmaindb; Owner: postgres
--

CREATE SEQUENCE "ld02_t_acslog_sequence"
    START WITH 51065
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "ld02_t_acslog_sequence" OWNER TO "postgres";

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 199 (class 1259 OID 16388)
-- Name: LD02_T_ACSLOG; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE "LD02_T_ACSLOG" (
    "ld02_logId" integer DEFAULT "nextval"('"ld02_t_acslog_sequence"'::"regclass") NOT NULL,
    "ld02_classFileName" character varying(255),
    "ld02_lineNo" integer NOT NULL,
    "ld02_logLevel" character varying(255),
    "ld02_logMessage" character varying(511),
    "ld02_logTime" timestamp without time zone,
    "ld02_screenId" character varying(100)
);


ALTER TABLE "LD02_T_ACSLOG" OWNER TO "postgres";

--
-- TOC entry 200 (class 1259 OID 16395)
-- Name: LD03_T_STGLOG; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE "LD03_T_STGLOG" (
    "ld03_logSetting" integer,
    "ld03_logSettingId" integer DEFAULT "nextval"('"LD03_T_STGLOG"'::"regclass") NOT NULL
);


ALTER TABLE "LD03_T_STGLOG" OWNER TO "postgres";

--
-- TOC entry 202 (class 1259 OID 16401)
-- Name: audit_auditid_seq; Type: SEQUENCE; Schema: mfmaindb; Owner: postgres
--

CREATE SEQUENCE "audit_auditid_seq"
    START WITH 195
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "audit_auditid_seq" OWNER TO "postgres";

--
-- TOC entry 203 (class 1259 OID 16403)
-- Name: LD04_T_AUDIT; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE "LD04_T_AUDIT" (
    "ld04_auditId" integer DEFAULT "nextval"('"audit_auditid_seq"'::"regclass") NOT NULL,
    "ld04_username" "text",
    "ld04_message" "text",
    "ld04_operation" "text",
    "ld04_createdOn" timestamp without time zone
);


ALTER TABLE "LD04_T_AUDIT" OWNER TO "postgres";

--
-- TOC entry 204 (class 1259 OID 16410)
-- Name: MF02_M_STRMAST; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE "MF02_M_STRMAST" (
    "mf02_storeCode" character varying(4) NOT NULL,
    "mf02_storeAbbrName" character varying(15),
    "mf02_storeName" character varying(30),
    "mf11MCompmast_mf11_compCode" character varying(3) NOT NULL,
    "mf02_createdBy" character varying(255) NOT NULL,
    "mf02_createdOn" timestamp without time zone NOT NULL,
    "mf02_lastModifiedBy" character varying(255) NOT NULL,
    "mf02_lastModifiedOn" timestamp without time zone NOT NULL,
    "mf02_deactivatedBy" character varying(255),
    "mf02_deactivatedOn" timestamp without time zone,
    "mf02_reactivatedBy" character varying(255),
    "mf02_reactivatedOn" timestamp without time zone,
    "mf02_isActive" boolean NOT NULL,
    "mf02_jsonField" "json"
);


ALTER TABLE "MF02_M_STRMAST" OWNER TO "postgres";

--
-- TOC entry 205 (class 1259 OID 16416)
-- Name: MF06_R_MASTDATA; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE "MF06_R_MASTDATA" (
    "mf06_codeDesc" character varying(255) NOT NULL,
    "mf06_codeType" character varying(45) NOT NULL,
    "mf06_codeValue" character varying(255) NOT NULL,
    "mf06_createdBy" character varying(255) NOT NULL,
    "mf06_createdOn" timestamp without time zone NOT NULL,
    "mf06_isActive" boolean NOT NULL,
    "mf06_lastModifiedBy" character varying(255) NOT NULL,
    "mf06_lastModifiedOn" timestamp without time zone NOT NULL,
    "mf06_sysName" character varying(10),
    "mf06_jsonField" "json",
    "mf06_deactivatedBy" character varying(255),
    "mf06_reactivatedBy" character varying(255),
    "mf06_reactivatedOn" timestamp with time zone,
    "mf06_deactivatedOn" timestamp with time zone,
    "mf06_mastDataId" bigint NOT NULL
);


ALTER TABLE "MF06_R_MASTDATA" OWNER TO "postgres";

--
-- TOC entry 206 (class 1259 OID 16422)
-- Name: MF06_R_MASTDATA_mf06_mastDataId_seq; Type: SEQUENCE; Schema: mfmaindb; Owner: postgres
--

CREATE SEQUENCE "MF06_R_MASTDATA_mf06_mastDataId_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "MF06_R_MASTDATA_mf06_mastDataId_seq" OWNER TO "postgres";

--
-- TOC entry 2453 (class 0 OID 0)
-- Dependencies: 206
-- Name: MF06_R_MASTDATA_mf06_mastDataId_seq; Type: SEQUENCE OWNED BY; Schema: mfmaindb; Owner: postgres
--

ALTER SEQUENCE "MF06_R_MASTDATA_mf06_mastDataId_seq" OWNED BY "MF06_R_MASTDATA"."mf06_mastDataId";


--
-- TOC entry 208 (class 1259 OID 16427)
-- Name: MF07_R_SYSPARAM; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE "MF07_R_SYSPARAM" (
    "mf07_createdBy" character varying(255) NOT NULL,
    "mf07_createdOn" timestamp without time zone NOT NULL,
    "mf07_desc" character varying(255),
    "mf07_isActive" boolean NOT NULL,
    "mf07_lastModifiedBy" character varying(255) NOT NULL,
    "mf07_lastModifiedOn" timestamp without time zone NOT NULL,
    "mf07_propName" character varying(30) NOT NULL,
    "mf07_propValue" character varying(255) NOT NULL,
    "mf07_sysName" character varying(10),
    "mf07_jsonField" "json",
    "mf07_deactivatedBy" character varying(255),
    "mf07_reactivatedBy" character varying(255),
    "mf07_reactivatedOn" timestamp with time zone,
    "mf07_deactivatedOn" timestamp with time zone,
    "mf07_sysParamCode" bigint NOT NULL
);


ALTER TABLE "MF07_R_SYSPARAM" OWNER TO "postgres";

--
-- TOC entry 209 (class 1259 OID 16433)
-- Name: MF07_R_SYSPARAM_mf07_sysParamId_seq; Type: SEQUENCE; Schema: mfmaindb; Owner: postgres
--

CREATE SEQUENCE "MF07_R_SYSPARAM_mf07_sysParamId_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "MF07_R_SYSPARAM_mf07_sysParamId_seq" OWNER TO "postgres";

--
-- TOC entry 2454 (class 0 OID 0)
-- Dependencies: 209
-- Name: MF07_R_SYSPARAM_mf07_sysParamId_seq; Type: SEQUENCE OWNED BY; Schema: mfmaindb; Owner: postgres
--

ALTER SEQUENCE "MF07_R_SYSPARAM_mf07_sysParamId_seq" OWNED BY "MF07_R_SYSPARAM"."mf07_sysParamCode";


--
-- TOC entry 211 (class 1259 OID 16438)
-- Name: MF11_M_COMPMAST; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE "MF11_M_COMPMAST" (
    "mf11_compCode" character varying(3) NOT NULL,
    "mf11_compName" character varying(70) NOT NULL,
    "mf11_compRegNo" character varying(20),
    "mf11_createdBy" character varying(255) NOT NULL,
    "mf11_createdOn" timestamp without time zone NOT NULL,
    "mf11_isActive" boolean NOT NULL,
    "mf11_lastModifiedBy" character varying(255) NOT NULL,
    "mf11_lastModifiedOn" timestamp without time zone NOT NULL,
    "mf11_compCodeHRIS" character varying(3) NOT NULL,
    "mf11_compAbbrName" character varying(15),
    "mf11_compLogo" character varying(255),
    "mf11_companyLetterHeading" character varying(255),
    "mf11_deactivatedBy" character varying(255),
    "mf11_deactivatedOn" timestamp without time zone,
    "mf11_reactivatedBy" character varying(255),
    "mf11_reactivatedOn" timestamp without time zone,
    "mf11_jsonField" "json",
    "mf11_poTAndCPortrait" character varying(255),
    "mf11_poTAndCLandscape" character varying(255)
);


ALTER TABLE "MF11_M_COMPMAST" OWNER TO "postgres";

--
-- TOC entry 212 (class 1259 OID 16444)
-- Name: MF12_M_EMPMAST; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE "MF12_M_EMPMAST" (
    "mf12_psId" character varying(11) NOT NULL,
    "mf12_altId" character varying(11),
    "mf12_badgeNo" character varying(20),
    "mf12_confirmDt" "date",
    "mf12_createdBy" character varying(255) NOT NULL,
    "mf12_createdOn" timestamp without time zone NOT NULL,
    "mf12_deptDescHRIS" character varying(30),
    "mf12_deptCodeHRIS" character varying(10),
    "mf12_dob" "date",
    "mf12_email" character varying(70),
    "mf12_empCat" character(1),
    "mf12_empName" character varying(50),
    "mf12_ethnic" character varying(8),
    "mf12_ext" character varying(6),
    "mf12_foreignLocal" character(1),
    "mf12_frmCompCode" character varying(3),
    "mf12_frmLoc" character varying(10),
    "mf12_gender" character(1),
    "mf12_hireDt" "date",
    "mf12_hrStatus" character(1),
    "mf12_isActive" boolean NOT NULL,
    "mf12_lastModifiedBy" character varying(255) NOT NULL,
    "mf12_lastModifiedOn" timestamp without time zone NOT NULL,
    "mf12_loc" character varying(10),
    "mf12_locDesc" character varying(30),
    "mf12_lstDtWrk" "date",
    "mf12_lstUpdDtTm" timestamp without time zone,
    "mf12_maritalStat" character(1),
    "mf12_newICNo" character varying(20),
    "mf12_oldICNo" character varying(20),
    "mf12_passportNo" character varying(15),
    "mf12_payGrp" character varying(10),
    "mf12_payrollComp" character varying(3),
    "mf12_payrollStatus" character(1),
    "mf12_phoneNo" character varying(24),
    "mf12_posNo" character varying(8),
    "mf12_posTitle" character varying(30),
    "mf12_prcDtTm" timestamp without time zone,
    "mf12_prefName" character varying(50),
    "mf12_rptToEmpId" character varying(11),
    "mf12_rptToPosNo" character varying(8),
    "mf12_salaryGrade" character varying(3),
    "mf12_salaryPlan" character varying(4),
    "mf12_toCompCode" character varying(3),
    "mf12_toLoc" character varying(10),
    "mf12_trfDt" "date",
    "mf12_trfSndFlag" character(1),
    "mf12_jsonField" "json",
    "mf12_expJobEndDt" timestamp without time zone,
    "mf11MCompmast_mf11_compCodeHRIS" character varying(3),
    "mf13MPcmast_mf13_pcCode" character varying(7),
    "mf19MJobmast_mf19_jobCode" character varying(6),
    "mf12_fromCostChargeCompany" character varying(3),
    "mf12_fromProfitCentre" character varying(15),
    "mf12_toCostChargeCompany" character varying(3),
    "mf12_toProfitCentre" character varying(15)
);


ALTER TABLE "MF12_M_EMPMAST" OWNER TO "postgres";

--
-- TOC entry 213 (class 1259 OID 16450)
-- Name: MF13_M_PCMAST; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE "MF13_M_PCMAST" (
    "mf13_createdBy" character varying(255) NOT NULL,
    "mf13_createdOn" timestamp without time zone NOT NULL,
    "mf13_lastModifiedBy" character varying(255) NOT NULL,
    "mf13_lastModifiedOn" timestamp without time zone NOT NULL,
    "mf13_pcCode" character varying(7) NOT NULL,
    "mf13_pcName" character varying(30),
    "mf13_isActive" boolean NOT NULL,
    "mf13_addressLine1" character varying(70),
    "mf13_addressLine2" character varying(70),
    "mf13_addressLine3" character varying(70),
    "mf13_postCode" character varying(10),
    "mf13_postArea" character varying(70),
    "mf13_pcHead" "text",
    "mf13_pcViewRights" "text",
    "mf13_deactivatedBy" character varying(255),
    "mf13_deactivatedOn" timestamp without time zone,
    "mf13_reactivatedBy" character varying(255),
    "mf13_reactivatedOn" timestamp without time zone,
    "mf13_jsonField" "json",
    "mf11MCompmast_mf11_compCode" character varying(3),
    "mf02MStrmast_mf02_storeCode" character varying(4),
    "mf14MDeptmast_mf14_deptCode" character varying(3),
    "mf13_pcAbbrName" character varying(15),
    "mf13_storeType" character varying(1),
    "mf13_startDate" "date",
    "mf13_endDate" "date"
);


ALTER TABLE "MF13_M_PCMAST" OWNER TO "postgres";

--
-- TOC entry 214 (class 1259 OID 16456)
-- Name: MF14_M_DEPTMAST; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE "MF14_M_DEPTMAST" (
    "mf14_deptCode" character varying(3) NOT NULL,
    "mf14_deptName" character varying(30),
    "mf14_createdBy" character varying(255) NOT NULL,
    "mf14_createdOn" timestamp without time zone NOT NULL,
    "mf14_lastModifiedBy" character varying(255) NOT NULL,
    "mf14_lastModifiedOn" timestamp without time zone NOT NULL,
    "mf14_deactivatedBy" character varying(255),
    "mf14_deactivatedOn" timestamp without time zone,
    "mf14_reactivatedBy" character varying(255),
    "mf14_reactivatedOn" timestamp without time zone,
    "mf14_isActive" boolean NOT NULL,
    "mf14_jsonField" "json",
    "mf14_deptAbbrName" character varying(15)
);


ALTER TABLE "MF14_M_DEPTMAST" OWNER TO "postgres";

--
-- TOC entry 215 (class 1259 OID 16462)
-- Name: MF15_M_PRMAST; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE "MF15_M_PRMAST" (
    "mf15_prAbbrName" character varying(30) NOT NULL,
    "mf15_prDesc" character varying(255),
    "mf15_createdBy" character varying(255) NOT NULL,
    "mf15_createdOn" timestamp without time zone NOT NULL,
    "mf15_lastModifiedBy" character varying(255) NOT NULL,
    "mf15_lastModifiedOn" timestamp without time zone NOT NULL,
    "mf15_deactivatedBy" character varying(255),
    "mf15_deactivatedOn" timestamp without time zone,
    "mf15_reactivatedBy" character varying(255),
    "mf15_reactivatedOn" timestamp without time zone,
    "mf15_prTypeCode" character varying(10) NOT NULL,
    "mf15_isActive" boolean NOT NULL,
    "mf15_jsonField" "json"
);


ALTER TABLE "MF15_M_PRMAST" OWNER TO "postgres";

--
-- TOC entry 216 (class 1259 OID 16468)
-- Name: MF16_M_GSTMAST; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE "MF16_M_GSTMAST" (
    "mf16_taxCode" character varying(20) NOT NULL,
    "mf16_taxDesc" character varying(255) NOT NULL,
    "mf16_effectiveStartingDate" "date",
    "mf16_effectiveEndingDate" "date",
    "mf16_createdBy" character varying(255) NOT NULL,
    "mf16_createdOn" timestamp without time zone NOT NULL,
    "mf16_lastModifiedBy" character varying(255) NOT NULL,
    "mf16_lastModifiedOn" timestamp without time zone NOT NULL,
    "mf16_deactivatedBy" character varying(255),
    "mf16_deactivatedOn" timestamp without time zone,
    "mf16_reactivatedBy" character varying(255),
    "mf16_reactivatedOn" timestamp without time zone,
    "mf16_taxAbbrDesc" character varying(30),
    "mf16_isActive" boolean NOT NULL,
    "mf16_jsonField" "json",
    "mf16_taxRate" numeric(10,4),
    "mf16_allowedVariance" numeric(10,4)
);


ALTER TABLE "MF16_M_GSTMAST" OWNER TO "postgres";

--
-- TOC entry 217 (class 1259 OID 16474)
-- Name: MF17_M_ORDCATMAST; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE "MF17_M_ORDCATMAST" (
    "mf17_ordCatCode" character varying(70) NOT NULL,
    "mf17_ordCatDesc" character varying(255),
    "mf17_ordCatAuthorizationGrid" character varying(255) NOT NULL,
    "mf17_createdBy" character varying(255) NOT NULL,
    "mf17_createdOn" timestamp without time zone NOT NULL,
    "mf17_lastModifiedBy" character varying(255) NOT NULL,
    "mf17_lastModifiedOn" timestamp without time zone NOT NULL,
    "mf17_deactivatedBy" character varying(255),
    "mf17_deactivatedOn" timestamp without time zone,
    "mf17_reactivatedBy" character varying(255),
    "mf17_reactivatedOn" timestamp without time zone,
    "mf17_jsonField" "json",
    "mf17_ordCatAbbrDesc" character varying(30),
    "mf15MPrmast_mf15_prTypeCode" character varying(10),
    "mf17_isActive" boolean NOT NULL
);


ALTER TABLE "MF17_M_ORDCATMAST" OWNER TO "postgres";

--
-- TOC entry 218 (class 1259 OID 16480)
-- Name: MF18_M_ORDSUBCMAST; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE "MF18_M_ORDSUBCMAST" (
    "mf18_ordSubCCode" character varying(15) NOT NULL,
    "mf18_ordSubCDesc" character varying(255),
    "mf17MOrdcatmast_mf17_ordCatCode" character varying(70) NOT NULL,
    "mf18_createdBy" character varying(255) NOT NULL,
    "mf18_createdOn" timestamp without time zone NOT NULL,
    "mf18_lastModifiedOn" timestamp without time zone NOT NULL,
    "mf18_deactivatedBy" character varying(255),
    "mf18_deactivatedOn" timestamp without time zone,
    "mf18_reactivatedBy" character varying(255),
    "mf18_reactivatedOn" timestamp without time zone,
    "mf18_jsonField" "json",
    "mf18_lastModifiedBy" character varying(255) NOT NULL,
    "mf18_ordSubCAbbrDesc" character varying(30),
    "mf18_isActive" boolean NOT NULL
);


ALTER TABLE "MF18_M_ORDSUBCMAST" OWNER TO "postgres";

--
-- TOC entry 219 (class 1259 OID 16486)
-- Name: MF19_M_JOBMAST; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE "MF19_M_JOBMAST" (
    "mf19_jobCode" character varying(6) NOT NULL,
    "mf19_jobName" character varying(30),
    "mf19_jobAbbrName" character varying(15),
    "mf19_createdBy" character varying(255) NOT NULL,
    "mf19_createdOn" timestamp without time zone NOT NULL,
    "mf19_lastModifiedBy" character varying(255) NOT NULL,
    "mf19_lastModifiedOn" timestamp without time zone NOT NULL,
    "mf19_deactivatedBy" character varying(70),
    "mf19_deactivatedOn" timestamp without time zone,
    "mf19_reactivatedBy" character varying(70),
    "mf19_reactivatedOn" timestamp without time zone,
    "mf19_jsonField" "json",
    "mf19_isActive" boolean
);


ALTER TABLE "MF19_M_JOBMAST" OWNER TO "postgres";

--
-- TOC entry 220 (class 1259 OID 16492)
-- Name: MF20_M_ADMINMAST; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE "MF20_M_ADMINMAST" (
    "mf20_username" character varying(10) NOT NULL,
    "mf20_password" character varying(255),
    "mf20_startDate" "date",
    "mf20_endDate" "date",
    "mf20_isActive" boolean,
    "mf20_createdOn" timestamp without time zone,
    "mf20_lastModifiedOn" timestamp without time zone,
    "mf20_deactivatedOn" timestamp without time zone,
    "mf20_reactivatedOn" timestamp without time zone,
    "mf20_blocked" smallint,
    "mf20_jsonField" "json",
    "mf20_recentPassword" character varying(255),
    "mf20_isLocked" boolean,
    "password_reset_token" character varying(100)
);


ALTER TABLE "MF20_M_ADMINMAST" OWNER TO "postgres";

--
-- TOC entry 230 (class 1259 OID 33018)
-- Name: mf22_m_scheduler_mf22_schedulerid_seq; Type: SEQUENCE; Schema: mfmaindb; Owner: postgres
--

CREATE SEQUENCE "mf22_m_scheduler_mf22_schedulerid_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "mf22_m_scheduler_mf22_schedulerid_seq" OWNER TO "postgres";

--
-- TOC entry 245 (class 1259 OID 33805)
-- Name: Mf22_M_SCHEDULER; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE "Mf22_M_SCHEDULER" (
    "mf22_schedulerid" integer DEFAULT "nextval"('"mf22_m_scheduler_mf22_schedulerid_seq"'::"regclass") NOT NULL,
    "mf22_task" character varying(255),
    "mf22_cronfrequency_occurrence" character varying(50),
    "mf22_startdate" timestamp(6) with time zone,
    "mf22_noenddate" boolean,
    "mf22_cronfrequency_amount" character varying(50),
    "mf22_status" character varying(20),
    "mf22_database" character varying(30),
    "mf22_table" character varying(30),
    "mf22_scheduletype" character varying(30),
    "mf22_enddate" "date",
    "mf22_upload" character varying(255),
    "mf22_path" character varying(50),
    "mf22_createdon" time without time zone,
    "mf22_createdby" character varying,
    "mf22_deactivatedby" character varying,
    "mf22_deactivatedon" timestamp without time zone,
    "mf22_jobname" character varying(50)
);


ALTER TABLE "Mf22_M_SCHEDULER" OWNER TO "postgres";

--
-- TOC entry 221 (class 1259 OID 16511)
-- Name: PO03_T_PROJECT; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE "PO03_T_PROJECT" (
    "po03_projectCode" character varying(15) NOT NULL,
    "po03_projectName" character varying(70) NOT NULL,
    "po03_projectDescription" character varying(255),
    "po03_projectHead" "text",
    "po03_projectTeamMember" "text",
    "mf13MPCMast_mf13_pcCode" "text",
    "po03_completionDate" "date",
    "po03_createdBy" character varying(255) NOT NULL,
    "po03_lastModifiedBy" character varying(255) NOT NULL,
    "po03_deactivatedBy" character varying(255),
    "po03_reactivatedBy" character varying(255),
    "po03_isActive" boolean,
    "po03_jsonField" "json",
    "po03_createdOn" timestamp without time zone,
    "po03_lastModifiedOn" timestamp without time zone,
    "po03_deactivatedOn" timestamp without time zone,
    "po03_reactivatedOn" timestamp without time zone
);


ALTER TABLE "PO03_T_PROJECT" OWNER TO "postgres";

--
-- TOC entry 222 (class 1259 OID 16519)
-- Name: PO06_T_USER; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE "PO06_T_USER" (
    "po06_psid" character varying(11) NOT NULL,
    "po06_empname" character varying(50),
    "po06_firsttimelogin" timestamp without time zone,
    "po06_lastlogin" timestamp without time zone,
    "po06_numberoflogin" integer,
    "po06_isactive" boolean,
    "po06_createdby" character varying(50),
    "po06_createdon" timestamp without time zone,
    "po06_lastmodifiedby" character varying(50),
    "po06_lastmodifiedon" timestamp without time zone,
    "po06_deactivatedby" character varying(50),
    "po06_deactivatedon" timestamp without time zone,
    "po06_reactivatedby" character varying(50),
    "po06_reactivatedon" timestamp without time zone,
    "po06_json" "json",
    "po06_created_by" character varying(255),
    "po06_created_on" timestamp without time zone,
    "po06_deactivated_by" character varying(255),
    "po06_deactivated_on" timestamp without time zone,
    "po06_emp_name" character varying(255),
    "po06_first_time_login" timestamp without time zone,
    "po06_is_active" boolean,
    "po06_last_login" timestamp without time zone,
    "po06_last_modified_by" character varying(255),
    "po06_last_modified_on" timestamp without time zone,
    "po06_number_of_login" integer,
    "po06_reactivated_by" character varying(255),
    "po06_reactivated_on" timestamp without time zone
);


ALTER TABLE "PO06_T_USER" OWNER TO "postgres";

--
-- TOC entry 229 (class 1259 OID 33016)
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: mfmaindb; Owner: postgres
--

CREATE SEQUENCE "hibernate_sequence"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "hibernate_sequence" OWNER TO "postgres";

--
-- TOC entry 201 (class 1259 OID 16399)
-- Name: ld03_t_stglog_sequence; Type: SEQUENCE; Schema: mfmaindb; Owner: postgres
--

CREATE SEQUENCE "ld03_t_stglog_sequence"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "ld03_t_stglog_sequence" OWNER TO "postgres";

--
-- TOC entry 239 (class 1259 OID 33765)
-- Name: menu_seq; Type: SEQUENCE; Schema: mfmaindb; Owner: postgres
--

CREATE SEQUENCE "menu_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999999999999999
    CACHE 1;


ALTER TABLE "menu_seq" OWNER TO "postgres";

--
-- TOC entry 242 (class 1259 OID 33771)
-- Name: menu; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE "menu" (
    "menu_id" bigint DEFAULT "nextval"('"menu_seq"'::"regclass") NOT NULL,
    "menu_code" character varying(100),
    "menu_desc" character varying(200),
    "menu_url" character varying(100),
    "menu_reference" character varying(100)
);


ALTER TABLE "menu" OWNER TO "postgres";

--
-- TOC entry 240 (class 1259 OID 33767)
-- Name: menu_grp_seq; Type: SEQUENCE; Schema: mfmaindb; Owner: postgres
--

CREATE SEQUENCE "menu_grp_seq"
    START WITH 107
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE "menu_grp_seq" OWNER TO "postgres";

--
-- TOC entry 243 (class 1259 OID 33780)
-- Name: menu_grp; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE "menu_grp" (
    "menu_grp_id" bigint DEFAULT "nextval"('"menu_grp_seq"'::"regclass") NOT NULL,
    "menu_grp_parent" bigint NOT NULL,
    "menu_code" character varying(100),
    "menu_desc" character varying(200),
    "menu_url" character varying(100),
    "menu_reference" character varying(100),
    "menu_grp_root" character varying(100),
    "menu_id" bigint,
    "menu_grp_enable" boolean
);


ALTER TABLE "menu_grp" OWNER TO "postgres";

--
-- TOC entry 241 (class 1259 OID 33769)
-- Name: menu_grp_role_id_seq; Type: SEQUENCE; Schema: mfmaindb; Owner: postgres
--

CREATE SEQUENCE "menu_grp_role_id_seq"
    START WITH 11
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "menu_grp_role_id_seq" OWNER TO "postgres";

--
-- TOC entry 244 (class 1259 OID 33789)
-- Name: menu_grp_role; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE "menu_grp_role" (
    "id" bigint DEFAULT "nextval"('"menu_grp_role_id_seq"'::"regclass") NOT NULL,
    "menu_grp_id" bigint,
    "role_id" bigint
);


ALTER TABLE "menu_grp_role" OWNER TO "postgres";

--
-- TOC entry 207 (class 1259 OID 16424)
-- Name: mf06_r_mastdata_sequence; Type: SEQUENCE; Schema: mfmaindb; Owner: postgres
--

CREATE SEQUENCE "mf06_r_mastdata_sequence"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "mf06_r_mastdata_sequence" OWNER TO "postgres";

--
-- TOC entry 210 (class 1259 OID 16435)
-- Name: mf07_r_sysparam_sequence; Type: SEQUENCE; Schema: mfmaindb; Owner: postgres
--

CREATE SEQUENCE "mf07_r_sysparam_sequence"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "mf07_r_sysparam_sequence" OWNER TO "postgres";

--
-- TOC entry 225 (class 1259 OID 32938)
-- Name: permissions; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE "permissions" (
    "id" integer NOT NULL,
    "description" character varying(1024),
    "name" character varying(255) NOT NULL
);


ALTER TABLE "permissions" OWNER TO "postgres";

--
-- TOC entry 223 (class 1259 OID 16525)
-- Name: podblist__seq; Type: SEQUENCE; Schema: mfmaindb; Owner: postgres
--

CREATE SEQUENCE "podblist__seq"
    START WITH 27
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "podblist__seq" OWNER TO "postgres";

--
-- TOC entry 226 (class 1259 OID 32986)
-- Name: role_permission; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE "role_permission" (
    "role_id" integer NOT NULL,
    "perm_id" integer NOT NULL
);


ALTER TABLE "role_permission" OWNER TO "postgres";

--
-- TOC entry 227 (class 1259 OID 32989)
-- Name: roles; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE "roles" (
    "role_id" integer NOT NULL,
    "role_description" character varying(1024),
    "role_name" character varying(255) NOT NULL
);


ALTER TABLE "roles" OWNER TO "postgres";

--
-- TOC entry 247 (class 1259 OID 33822)
-- Name: user_role; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE "user_role" (
    "user_id" character varying(100) NOT NULL,
    "role_id" integer NOT NULL
);


ALTER TABLE "user_role" OWNER TO "postgres";

--
-- TOC entry 228 (class 1259 OID 33000)
-- Name: users; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE "users" (
    "id" integer NOT NULL,
    "email" character varying(255) NOT NULL,
    "name" character varying(255) NOT NULL,
    "password" character varying(255) NOT NULL,
    "password_reset_token" character varying(255)
);


ALTER TABLE "users" OWNER TO "postgres";

--
-- TOC entry 2207 (class 2604 OID 16426)
-- Name: MF06_R_MASTDATA mf06_mastDataId; Type: DEFAULT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY "MF06_R_MASTDATA" ALTER COLUMN "mf06_mastDataId" SET DEFAULT "nextval"('"MF06_R_MASTDATA_mf06_mastDataId_seq"'::"regclass");


--
-- TOC entry 2208 (class 2604 OID 16437)
-- Name: MF07_R_SYSPARAM mf07_sysParamCode; Type: DEFAULT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY "MF07_R_SYSPARAM" ALTER COLUMN "mf07_sysParamCode" SET DEFAULT "nextval"('"MF07_R_SYSPARAM_mf07_sysParamId_seq"'::"regclass");


--
-- TOC entry 2410 (class 0 OID 16388)
-- Dependencies: 199
-- Data for Name: LD02_T_ACSLOG; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--



--
-- TOC entry 2411 (class 0 OID 16395)
-- Dependencies: 200
-- Data for Name: LD03_T_STGLOG; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

--
-- TOC entry 2455 (class 0 OID 0)
-- Dependencies: 206
-- Name: MF06_R_MASTDATA_mf06_mastDataId_seq; Type: SEQUENCE SET; Schema: mfmaindb; Owner: postgres
--

SELECT pg_catalog.setval('"MF06_R_MASTDATA_mf06_mastDataId_seq"', 1, false);


--
-- TOC entry 2456 (class 0 OID 0)
-- Dependencies: 209
-- Name: MF07_R_SYSPARAM_mf07_sysParamId_seq; Type: SEQUENCE SET; Schema: mfmaindb; Owner: postgres
--

SELECT pg_catalog.setval('"MF07_R_SYSPARAM_mf07_sysParamId_seq"', 1, false);


--
-- TOC entry 2457 (class 0 OID 0)
-- Dependencies: 202
-- Name: audit_auditid_seq; Type: SEQUENCE SET; Schema: mfmaindb; Owner: postgres
--

SELECT pg_catalog.setval('"audit_auditid_seq"', 202, true);


--
-- TOC entry 2458 (class 0 OID 0)
-- Dependencies: 229
-- Name: hibernate_sequence; Type: SEQUENCE SET; Schema: mfmaindb; Owner: postgres
--

SELECT pg_catalog.setval('"hibernate_sequence"', 1, false);


--
-- TOC entry 2459 (class 0 OID 0)
-- Dependencies: 198
-- Name: ld02_t_acslog_sequence; Type: SEQUENCE SET; Schema: mfmaindb; Owner: postgres
--

SELECT pg_catalog.setval('"ld02_t_acslog_sequence"', 51065, false);


--
-- TOC entry 2460 (class 0 OID 0)
-- Dependencies: 201
-- Name: ld03_t_stglog_sequence; Type: SEQUENCE SET; Schema: mfmaindb; Owner: postgres
--

SELECT pg_catalog.setval('"ld03_t_stglog_sequence"', 1, true);


--
-- TOC entry 2461 (class 0 OID 0)
-- Dependencies: 241
-- Name: menu_grp_role_id_seq; Type: SEQUENCE SET; Schema: mfmaindb; Owner: postgres
--

SELECT pg_catalog.setval('"menu_grp_role_id_seq"', 11, false);


--
-- TOC entry 2462 (class 0 OID 0)
-- Dependencies: 240
-- Name: menu_grp_seq; Type: SEQUENCE SET; Schema: mfmaindb; Owner: postgres
--

SELECT pg_catalog.setval('"menu_grp_seq"', 107, false);


--
-- TOC entry 2463 (class 0 OID 0)
-- Dependencies: 239
-- Name: menu_seq; Type: SEQUENCE SET; Schema: mfmaindb; Owner: postgres
--

SELECT pg_catalog.setval('"menu_seq"', 1, false);


--
-- TOC entry 2464 (class 0 OID 0)
-- Dependencies: 207
-- Name: mf06_r_mastdata_sequence; Type: SEQUENCE SET; Schema: mfmaindb; Owner: postgres
--

SELECT pg_catalog.setval('"mf06_r_mastdata_sequence"', 6, true);


--
-- TOC entry 2465 (class 0 OID 0)
-- Dependencies: 210
-- Name: mf07_r_sysparam_sequence; Type: SEQUENCE SET; Schema: mfmaindb; Owner: postgres
--

SELECT pg_catalog.setval('"mf07_r_sysparam_sequence"', 6, true);


--
-- TOC entry 2466 (class 0 OID 0)
-- Dependencies: 230
-- Name: mf22_m_scheduler_mf22_schedulerid_seq; Type: SEQUENCE SET; Schema: mfmaindb; Owner: postgres
--

SELECT pg_catalog.setval('"mf22_m_scheduler_mf22_schedulerid_seq"', 1, false);


--
-- TOC entry 2467 (class 0 OID 0)
-- Dependencies: 223
-- Name: podblist__seq; Type: SEQUENCE SET; Schema: mfmaindb; Owner: postgres
--

SELECT pg_catalog.setval('"podblist__seq"', 30, true);


-- Completed on 2018-05-01 01:52:04

--
-- PostgreSQL database dump complete
--