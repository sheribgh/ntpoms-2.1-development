SET schema 'mfmaindb';


CREATE DATABASE "poAdminDB" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_United States.1252' LC_CTYPE = 'English_United States.1252';


ALTER DATABASE "poAdminDB" OWNER TO postgres;


CREATE SEQUENCE ld02_t_acslog_sequence
    START WITH 51065
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ld02_t_acslog_sequence OWNER TO postgres;

CREATE SEQUENCE ld03_t_stglog_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ld03_t_stglog_sequence OWNER TO postgres;

--
-- TOC entry 227 (class 1259 OID 96276)
-- Name: menu_seq; Type: SEQUENCE; Schema: mfmaindb; Owner: postgres
--

CREATE SEQUENCE menu_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999999999999999
    CACHE 1;


ALTER TABLE menu_seq OWNER TO postgres;

CREATE SEQUENCE mf06_r_mastdata_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE mf06_r_mastdata_sequence OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 87565)
-- Name: mf07_r_sysparam_sequence; Type: SEQUENCE; Schema: mfmaindb; Owner: postgres
--

CREATE SEQUENCE mf07_r_sysparam_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE mf07_r_sysparam_sequence OWNER TO postgres;

SET SCHEMA 'mfmaindb';

CREATE TABLE "LD02_T_ACSLOG" (
    "ld02_logId" integer DEFAULT nextval('ld02_t_acslog_sequence'::regclass) NOT NULL,
    "ld02_classFileName" character varying(255),
    "ld02_lineNo" integer NOT NULL,
    "ld02_logLevel" character varying(255),
    "ld02_logMessage" character varying(511),
    "ld02_logTime" timestamp without time zone,
    "ld02_screenId" character varying(100)
);


ALTER TABLE "LD02_T_ACSLOG" OWNER TO postgres;

--
-- TOC entry 185 (class 1259 OID 87466)
-- Name: LD03_T_STGLOG; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE "LD03_T_STGLOG" (
    "ld03_logSetting" integer,
    "ld03_logSettingId" integer DEFAULT nextval('"LD03_T_STGLOG"'::regclass) NOT NULL
);


ALTER TABLE "LD03_T_STGLOG" OWNER TO postgres;

--
-- TOC entry 186 (class 1259 OID 87470)
-- Name: audit_auditid_seq; Type: SEQUENCE; Schema: mfmaindb; Owner: postgres
--

CREATE SEQUENCE audit_auditid_seq
    START WITH 195
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE audit_auditid_seq OWNER TO postgres;

--
-- TOC entry 187 (class 1259 OID 87472)
-- Name: LD04_T_AUDIT; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE "LD04_T_AUDIT" (
    "ld04_auditId" integer DEFAULT nextval('audit_auditid_seq'::regclass) NOT NULL,
    ld04_username text,
    ld04_message text,
    ld04_operation text,
    "ld04_createdOn" timestamp without time zone
);


ALTER TABLE "LD04_T_AUDIT" OWNER TO postgres;

--
-- TOC entry 188 (class 1259 OID 87479)
-- Name: MF02_M_STRMAST; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE "MF02_M_STRMAST" (
    "mf02_storeCode" character varying(4) NOT NULL,
    "mf02_storeAbbrName" character varying(15),
    "mf02_storeName" character varying(30),
    "mf11MCompmast_mf11_compCode" character varying(3) NOT NULL,
    "mf02_createdBy" character varying(255) NOT NULL,
    "mf02_createdOn" timestamp without time zone NOT NULL,
    "mf02_lastModifiedBy" character varying(255) NOT NULL,
    "mf02_lastModifiedOn" timestamp without time zone NOT NULL,
    "mf02_deactivatedBy" character varying(255),
    "mf02_deactivatedOn" timestamp without time zone,
    "mf02_reactivatedBy" character varying(255),
    "mf02_reactivatedOn" timestamp without time zone,
    "mf02_isActive" boolean NOT NULL,
    "mf02_jsonField" json
);


ALTER TABLE "MF02_M_STRMAST" OWNER TO postgres;

--
-- TOC entry 189 (class 1259 OID 87485)
-- Name: MF06_R_MASTDATA; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE "MF06_R_MASTDATA" (
    "mf06_codeDesc" character varying(255) NOT NULL,
    "mf06_codeType" character varying(45) NOT NULL,
    "mf06_codeValue" character varying(255) NOT NULL,
    "mf06_createdBy" character varying(255) NOT NULL,
    "mf06_createdOn" timestamp without time zone NOT NULL,
    "mf06_isActive" boolean NOT NULL,
    "mf06_lastModifiedBy" character varying(255) NOT NULL,
    "mf06_lastModifiedOn" timestamp without time zone NOT NULL,
    "mf06_sysName" character varying(10),
    "mf06_jsonField" json,
    "mf06_deactivatedBy" character varying(255),
    "mf06_reactivatedBy" character varying(255),
    "mf06_reactivatedOn" timestamp with time zone,
    "mf06_deactivatedOn" timestamp with time zone,
    "mf06_mastDataId" bigint NOT NULL
);


ALTER TABLE "MF06_R_MASTDATA" OWNER TO postgres;

--
-- TOC entry 190 (class 1259 OID 87491)
-- Name: MF06_R_MASTDATA_mf06_mastDataId_seq; Type: SEQUENCE; Schema: mfmaindb; Owner: postgres
--

CREATE SEQUENCE "MF06_R_MASTDATA_mf06_mastDataId_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "MF06_R_MASTDATA_mf06_mastDataId_seq" OWNER TO postgres;

--
-- TOC entry 2478 (class 0 OID 0)
-- Dependencies: 190
-- Name: MF06_R_MASTDATA_mf06_mastDataId_seq; Type: SEQUENCE OWNED BY; Schema: mfmaindb; Owner: postgres
--

ALTER SEQUENCE "MF06_R_MASTDATA_mf06_mastDataId_seq" OWNED BY "MF06_R_MASTDATA"."mf06_mastDataId";


--
-- TOC entry 191 (class 1259 OID 87493)
-- Name: MF07_R_SYSPARAM; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE "MF07_R_SYSPARAM" (
    "mf07_createdBy" character varying(255) NOT NULL,
    "mf07_createdOn" timestamp without time zone NOT NULL,
    mf07_desc character varying(255),
    "mf07_isActive" boolean NOT NULL,
    "mf07_lastModifiedBy" character varying(255) NOT NULL,
    "mf07_lastModifiedOn" timestamp without time zone NOT NULL,
    "mf07_propName" character varying(30) NOT NULL,
    "mf07_propValue" character varying(255) NOT NULL,
    "mf07_sysName" character varying(10),
    "mf07_jsonField" json,
    "mf07_deactivatedBy" character varying(255),
    "mf07_reactivatedBy" character varying(255),
    "mf07_reactivatedOn" timestamp with time zone,
    "mf07_deactivatedOn" timestamp with time zone,
    "mf07_sysParamCode" bigint NOT NULL
);


ALTER TABLE "MF07_R_SYSPARAM" OWNER TO postgres;

--
-- TOC entry 192 (class 1259 OID 87499)
-- Name: MF07_R_SYSPARAM_mf07_sysParamId_seq; Type: SEQUENCE; Schema: mfmaindb; Owner: postgres
--

CREATE SEQUENCE "MF07_R_SYSPARAM_mf07_sysParamId_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "MF07_R_SYSPARAM_mf07_sysParamId_seq" OWNER TO postgres;

--
-- TOC entry 2479 (class 0 OID 0)
-- Dependencies: 192
-- Name: MF07_R_SYSPARAM_mf07_sysParamId_seq; Type: SEQUENCE OWNED BY; Schema: mfmaindb; Owner: postgres
--

ALTER SEQUENCE "MF07_R_SYSPARAM_mf07_sysParamId_seq" OWNED BY "MF07_R_SYSPARAM"."mf07_sysParamCode";


--
-- TOC entry 193 (class 1259 OID 87501)
-- Name: MF11_M_COMPMAST; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE "MF11_M_COMPMAST" (
    "mf11_compCode" character varying(3) NOT NULL,
    "mf11_compName" character varying(70) NOT NULL,
    "mf11_compRegNo" character varying(20),
    "mf11_createdBy" character varying(255) NOT NULL,
    "mf11_createdOn" timestamp without time zone NOT NULL,
    "mf11_isActive" boolean NOT NULL,
    "mf11_lastModifiedBy" character varying(255) NOT NULL,
    "mf11_lastModifiedOn" timestamp without time zone NOT NULL,
    "mf11_compCodeHRIS" character varying(3) NOT NULL,
    "mf11_compAbbrName" character varying(15),
    "mf11_compLogo" character varying(255),
    "mf11_companyLetterHeading" character varying(255),
    "mf11_deactivatedBy" character varying(255),
    "mf11_deactivatedOn" timestamp without time zone,
    "mf11_reactivatedBy" character varying(255),
    "mf11_reactivatedOn" timestamp without time zone,
    "mf11_jsonField" json,
    "mf11_poTAndCPortrait" character varying(255),
    "mf11_poTAndCLandscape" character varying(255)
);


ALTER TABLE "MF11_M_COMPMAST" OWNER TO postgres;

--
-- TOC entry 194 (class 1259 OID 87507)
-- Name: MF12_M_EMPMAST; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE "MF12_M_EMPMAST" (
    "mf12_psId" character varying(11) NOT NULL,
    "mf12_altId" character varying(11),
    "mf12_badgeNo" character varying(20),
    "mf12_confirmDt" date,
    "mf12_createdBy" character varying(255) NOT NULL,
    "mf12_createdOn" timestamp without time zone NOT NULL,
    "mf12_deptDescHRIS" character varying(30),
    "mf12_deptCodeHRIS" character varying(10),
    mf12_dob date,
    mf12_email character varying(70),
    "mf12_empCat" character(1),
    "mf12_empName" character varying(50),
    mf12_ethnic character varying(8),
    mf12_ext character varying(6),
    "mf12_foreignLocal" character(1),
    "mf12_frmCompCode" character varying(3),
    "mf12_frmLoc" character varying(10),
    mf12_gender character(1),
    "mf12_hireDt" date,
    "mf12_hrStatus" character(1),
    "mf12_isActive" boolean NOT NULL,
    "mf12_lastModifiedBy" character varying(255) NOT NULL,
    "mf12_lastModifiedOn" timestamp without time zone NOT NULL,
    mf12_loc character varying(10),
    "mf12_locDesc" character varying(30),
    "mf12_lstDtWrk" date,
    "mf12_lstUpdDtTm" timestamp without time zone,
    "mf12_maritalStat" character(1),
    "mf12_newICNo" character varying(20),
    "mf12_oldICNo" character varying(20),
    "mf12_passportNo" character varying(15),
    "mf12_payGrp" character varying(10),
    "mf12_payrollComp" character varying(3),
    "mf12_payrollStatus" character(1),
    "mf12_phoneNo" character varying(24),
    "mf12_posNo" character varying(8),
    "mf12_posTitle" character varying(30),
    "mf12_prcDtTm" timestamp without time zone,
    "mf12_prefName" character varying(50),
    "mf12_rptToEmpId" character varying(11),
    "mf12_rptToPosNo" character varying(8),
    "mf12_salaryGrade" character varying(3),
    "mf12_salaryPlan" character varying(4),
    "mf12_toCompCode" character varying(3),
    "mf12_toLoc" character varying(10),
    "mf12_trfDt" date,
    "mf12_trfSndFlag" character(1),
    "mf12_jsonField" json,
    "mf12_expJobEndDt" timestamp without time zone,
    "mf11MCompmast_mf11_compCodeHRIS" character varying(3),
    "mf13MPcmast_mf13_pcCode" character varying(7),
    "mf19MJobmast_mf19_jobCode" character varying(6),
    "mf12_fromCostChargeCompany" character varying(3),
    "mf12_fromProfitCentre" character varying(15),
    "mf12_toCostChargeCompany" character varying(3),
    "mf12_toProfitCentre" character varying(15),
    mf12_isactive boolean
);


ALTER TABLE "MF12_M_EMPMAST" OWNER TO postgres;

--
-- TOC entry 195 (class 1259 OID 87513)
-- Name: MF13_M_PCMAST; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE "MF13_M_PCMAST" (
    "mf13_createdBy" character varying(255) NOT NULL,
    "mf13_createdOn" timestamp without time zone NOT NULL,
    "mf13_lastModifiedBy" character varying(255) NOT NULL,
    "mf13_lastModifiedOn" timestamp without time zone NOT NULL,
    "mf13_pcCode" character varying(7) NOT NULL,
    "mf13_pcName" character varying(30),
    "mf13_isActive" boolean NOT NULL,
    "mf13_addressLine1" character varying(70),
    "mf13_addressLine2" character varying(70),
    "mf13_addressLine3" character varying(70),
    "mf13_postCode" character varying(10),
    "mf13_postArea" character varying(70),
    "mf13_pcHead" character varying(30),
    "mf13_pcViewRights" character varying(30),
    "mf13_deactivatedBy" character varying(255),
    "mf13_deactivatedOn" timestamp without time zone,
    "mf13_reactivatedBy" character varying(255),
    "mf13_reactivatedOn" timestamp without time zone,
    "mf13_jsonField" json,
    "mf11MCompmast_mf11_compCode" character varying(3),
    "mf02MStrmast_mf02_storeCode" character varying(4),
    "mf14MDeptmast_mf14_deptCode" character varying(3),
    "mf13_pcAbbrName" character varying(15),
    "mf13_storeType" character varying(1),
    "mf13_startDate" date,
    "mf13_endDate" date
);


ALTER TABLE "MF13_M_PCMAST" OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 87519)
-- Name: MF14_M_DEPTMAST; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE "MF14_M_DEPTMAST" (
    "mf14_deptCode" character varying(3) NOT NULL,
    "mf14_deptName" character varying(30),
    "mf14_createdBy" character varying(255) NOT NULL,
    "mf14_createdOn" timestamp without time zone NOT NULL,
    "mf14_lastModifiedBy" character varying(255) NOT NULL,
    "mf14_lastModifiedOn" timestamp without time zone NOT NULL,
    "mf14_deactivatedBy" character varying(255),
    "mf14_deactivatedOn" timestamp without time zone,
    "mf14_reactivatedBy" character varying(255),
    "mf14_reactivatedOn" timestamp without time zone,
    "mf14_isActive" boolean NOT NULL,
    "mf14_jsonField" json,
    "mf14_deptAbbrName" character varying(2)
);


ALTER TABLE "MF14_M_DEPTMAST" OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 87525)
-- Name: MF15_M_PRMAST; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE "MF15_M_PRMAST" (
    "mf15_prAbbrName" character varying(30) NOT NULL,
    "mf15_prDesc" character varying(255),
    "mf15_createdBy" character varying(255) NOT NULL,
    "mf15_createdOn" timestamp without time zone NOT NULL,
    "mf15_lastModifiedBy" character varying(255) NOT NULL,
    "mf15_lastModifiedOn" timestamp without time zone NOT NULL,
    "mf15_deactivatedBy" character varying(255),
    "mf15_deactivatedOn" timestamp without time zone,
    "mf15_reactivatedBy" character varying(255),
    "mf15_reactivatedOn" timestamp without time zone,
    "mf15_prTypeCode" character varying(10) NOT NULL,
    "mf15_isActive" boolean NOT NULL,
    "mf15_jsonField" json
);


ALTER TABLE "MF15_M_PRMAST" OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 87531)
-- Name: MF16_M_GSTMAST; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE "MF16_M_GSTMAST" (
    "mf16_taxCode" character varying(20) NOT NULL,
    "mf16_taxDesc" character varying(255) NOT NULL,
    "mf16_effectiveStartingDate" date,
    "mf16_effectiveEndingDate" date,
    "mf16_createdBy" character varying(255) NOT NULL,
    "mf16_createdOn" timestamp without time zone NOT NULL,
    "mf16_lastModifiedBy" character varying(255) NOT NULL,
    "mf16_lastModifiedOn" timestamp without time zone NOT NULL,
    "mf16_deactivatedBy" character varying(255),
    "mf16_deactivatedOn" timestamp without time zone,
    "mf16_reactivatedBy" character varying(255),
    "mf16_reactivatedOn" timestamp without time zone,
    "mf16_taxAbbrDesc" character varying(30),
    "mf16_isActive" boolean NOT NULL,
    "mf16_jsonField" json,
    "mf16_taxRate" numeric(10,4),
    "mf16_allowedVariance" numeric(10,4)
);


ALTER TABLE "MF16_M_GSTMAST" OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 87537)
-- Name: MF17_M_ORDCATMAST; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE "MF17_M_ORDCATMAST" (
    "mf17_ordCatCode" character varying(70) NOT NULL,
    "mf17_ordCatDesc" character varying(255),
    "mf17_ordCatAuthorizationGrid" character varying(255) NOT NULL,
    "mf17_createdBy" character varying(255) NOT NULL,
    "mf17_createdOn" timestamp without time zone NOT NULL,
    "mf17_lastModifiedBy" character varying(255) NOT NULL,
    "mf17_lastModifiedOn" timestamp without time zone NOT NULL,
    "mf17_deactivatedBy" character varying(255),
    "mf17_deactivatedOn" timestamp without time zone,
    "mf17_reactivatedBy" character varying(255),
    "mf17_reactivatedOn" timestamp without time zone,
    "mf17_jsonField" json,
    "mf17_ordCatAbbrDesc" character varying(30),
    "mf15MPrmast_mf15_prTypeCode" character varying(10),
    "mf17_isActive" boolean NOT NULL
);


ALTER TABLE "MF17_M_ORDCATMAST" OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 87543)
-- Name: MF18_M_ORDSUBCMAST; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE "MF18_M_ORDSUBCMAST" (
    "mf18_ordSubCCode" character varying(15) NOT NULL,
    "mf18_ordSubCDesc" character varying(255),
    "mf17MOrdcatmast_mf17_ordCatCode" character varying(70) NOT NULL,
    "mf18_createdBy" character varying(255) NOT NULL,
    "mf18_createdOn" timestamp without time zone NOT NULL,
    "mf18_lastModifiedOn" timestamp without time zone NOT NULL,
    "mf18_deactivatedBy" character varying(255),
    "mf18_deactivatedOn" timestamp without time zone,
    "mf18_reactivatedBy" character varying(255),
    "mf18_reactivatedOn" timestamp without time zone,
    "mf18_jsonField" json,
    "mf18_lastModifiedBy" character varying(255) NOT NULL,
    "mf18_ordSubCAbbrDesc" character varying(30),
    "mf18_isActive" boolean NOT NULL
);


ALTER TABLE "MF18_M_ORDSUBCMAST" OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 87549)
-- Name: MF19_M_JOBMAST; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE "MF19_M_JOBMAST" (
    "mf19_jobCode" character varying(6) NOT NULL,
    "mf19_jobName" character varying(30),
    "mf19_jobAbbrName" character varying(15),
    "mf19_createdBy" character varying(255) NOT NULL,
    "mf19_createdOn" timestamp without time zone NOT NULL,
    "mf19_lastModifiedBy" character varying(255) NOT NULL,
    "mf19_lastModifiedOn" timestamp without time zone NOT NULL,
    "mf19_deactivatedBy" character varying(70),
    "mf19_deactivatedOn" timestamp without time zone,
    "mf19_reactivatedBy" character varying(70),
    "mf19_reactivatedOn" timestamp without time zone,
    "mf19_jsonField" json,
    "mf19_isActive" boolean
);


ALTER TABLE "MF19_M_JOBMAST" OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 87555)
-- Name: MF20_M_ADMINMAST; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE "MF20_M_ADMINMAST" (
    mf20_username character varying(10) NOT NULL,
    mf20_password character varying(255),
    "mf20_startDate" date,
    "mf20_endDate" date,
    "mf20_isActive" boolean,
    "mf20_createdOn" timestamp without time zone,
    "mf20_lastModifiedOn" timestamp without time zone,
    "mf20_deactivatedOn" timestamp without time zone,
    "mf20_reactivatedOn" timestamp without time zone,
    mf20_blocked smallint,
    "mf20_jsonField" json,
    "mf20_recentPassword" character varying(255),
    "mf20_isLocked" boolean,
    password_reset_token character varying(20)
);


ALTER TABLE "MF20_M_ADMINMAST" OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 87567)
-- Name: Mf22_M_SCHEDULER; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE "Mf22_M_SCHEDULER" (
    mf22_schedulerid integer NOT NULL,
    mf22_task character varying(255),
    mf22_cronfrequency_occurrence character varying(50),
    mf22_startdate timestamp(6) with time zone,
    mf22_noenddate boolean,
    mf22_cronfrequency_amount character varying(50),
    mf22_status character varying(20),
    mf22_database character varying(30),
    mf22_table character varying(30),
    mf22_scheduletype character varying(30),
    mf22_enddate date,
    mf22_upload character varying(255),
    mf22_path character varying(50),
    mf22_createdon time without time zone,
    mf22_createdby character varying,
    mf22_deactivatedby character varying,
    mf22_deactivatedon timestamp without time zone,
    mf22_jobname character varying(50)
);


ALTER TABLE "Mf22_M_SCHEDULER" OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 87581)
-- Name: pologinlist__seq; Type: SEQUENCE; Schema: mfmaindb; Owner: postgres
--

CREATE SEQUENCE pologinlist__seq
    START WITH 27
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE pologinlist__seq OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 87583)
-- Name: PO01_LOGINLIST; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE "PO01_LOGINLIST" (
    po01_loginlist_seq integer DEFAULT nextval('pologinlist__seq'::regclass) NOT NULL,
    po01_year character varying(4),
    po01_companycode character varying(60),
    po01_companyname character varying(20)
);


ALTER TABLE "PO01_LOGINLIST" OWNER TO postgres;

--
-- TOC entry 225 (class 1259 OID 88190)
-- Name: PO01_T_MAIN; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE "PO01_T_MAIN" (
    "po01_poNo" character varying(255) NOT NULL,
    "po01_addressLine1" character varying(255),
    "po01_addressLine2" character varying(255),
    "po01_addressLine3" character varying(255),
    "po01_authorizeRemark" character varying(255),
    "po01_authorizedOn" timestamp without time zone,
    "mf12MEmpMast_po01_authorizer" character varying(255),
    "po01_cancelRemark" character varying(255),
    "po01_cancelledBy" character varying(255),
    "po01_cancelledOn" timestamp without time zone,
    "po01_closeException" character varying(255),
    "po01_closedBy" character varying(255),
    "po01_closedOn" timestamp without time zone,
    "mf11MCompMast_po01_compCode" character varying(255),
    "po01_contactPerson" character varying(255),
    "po01_contactPersonEmail" character varying(255),
    "po01_contactPersonName" character varying(255),
    "po01_contactPersonPhone" character varying(255),
    "mf02MCreditTerms_po01_creditTermsCode" character varying(255),
    po01_currency character varying(255),
    "po01_deleteRemark" character varying(255),
    "po01_deletedBy" character varying(255),
    "po01_deletedOn" timestamp without time zone,
    "po01_deliveryInstruction" character varying(255),
    "po01_departmentName" character varying(255),
    "po01_expireDate" timestamp without time zone,
    "po01_expiryDays" integer,
    "po01_expiryStatus" boolean,
    "po01_firstApprover" character varying(255),
    "po01_firstApproverDesignation" character varying(255),
    po01_forwardingstatus character varying(255),
    "po01_issuedOn" timestamp without time zone,
    "mf12MEmpMast_po01_issuer" character varying(255),
    "po01_issuerRemark" character varying(255),
    "po01_lastPrintedBy" character varying(255),
    "po01_lastPrintedOn" timestamp without time zone,
    "mf18MOrdSubCMast_po01_ordSubCCode" character varying(255),
    "mf17MOrdCatMast_po01_ordCatCode" character varying(255),
    "po01_orderCategoryAuthorizationGrid" character varying(255),
    "po01_overAllDiscount" double precision,
    "po01_paymentTerms" character varying(255),
    "mf13MPCMast_po01_pcCode" character varying(255),
    "po01_postArea" character varying(255),
    "po01_postCode" character varying(255),
    "mf15MPRMast_po01_prTypeCode" character varying(255),
    "po03TProject_po01_projectCode" character varying(255),
    "po01_quoteDate" timestamp without time zone,
    "po01_quoteNo" character varying(255),
    "po01_redraftRemark" character varying(255),
    "po01_rejectRemark" character varying(255),
    "po01_rejectedBy" character varying(255),
    "po01_rejectedOn" timestamp without time zone,
    "po01_secondApprover" character varying(255),
    "po01_secondApproverDesignation" character varying(255),
    po01_status character varying(255),
    "po01_storeName" character varying(255),
    "po01_termsAndConditionsLandscape" character varying(255),
    "po01_termsAndConditionsPortrait" character varying(255),
    "po01_thirdApprover" character varying(255),
    "po01_thirdApproverDesignation" character varying(255),
    "po01_totExclGST" double precision,
    "po01_totGST" double precision,
    "po01_totInclGST" double precision,
    "po01_vendorAddressLine1" character varying(255),
    "po01_vendorAddressLine2" character varying(255),
    "po01_vendorAddressLine3" character varying(255),
    "po01_vendorName" character varying(255),
    "po01_vendorPersonInCharge" character varying(255),
    "po01_vendorPersonInChargeEmail" character varying(255),
    "po01_vendorPersonInChargePhone" character varying(255),
    "po01_vendorPostArea" character varying(255),
    "po01_vendorPostCode" character varying(255),
    mf01mvendorbranch_po01_vendorbranchcode character varying(255),
    "mf01MVendorBranch_po01_vendorCode" character varying(255),
    "po01_verifiedOn" timestamp without time zone,
    "mf12MEmpMast_po01_verifier" character varying(255),
    "po01_verifyRemark" character varying(255)
);


ALTER TABLE "PO01_T_MAIN" OWNER TO postgres;

--
-- TOC entry 208 (class 1259 OID 87575)
-- Name: PO06_T_USER; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE "PO06_T_USER" (
    po06_psid character varying(11) NOT NULL,
    po06_empname character varying(50),
    po06_firsttimelogin timestamp without time zone,
    po06_lastlogin timestamp without time zone,
    po06_numberoflogin integer,
    po06_isactive boolean,
    po06_createdby character varying(50),
    po06_createdon timestamp without time zone,
    po06_lastmodifiedby character varying(50),
    po06_lastmodifiedon timestamp without time zone,
    po06_deactivatedby character varying(50),
    po06_deactivatedon timestamp without time zone,
    po06_reactivatedby character varying(50),
    po06_reactivatedon timestamp without time zone,
    po06_json json
);


ALTER TABLE "PO06_T_USER" OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 88063)
-- Name: audit; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE audit (
    auditid integer NOT NULL,
    createdon timestamp without time zone,
    message character varying(255),
    operation character varying(255),
    username character varying(255)
);


ALTER TABLE audit OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 88061)
-- Name: audit_auditid_seq1; Type: SEQUENCE; Schema: mfmaindb; Owner: postgres
--

CREATE SEQUENCE audit_auditid_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE audit_auditid_seq1 OWNER TO postgres;

--
-- TOC entry 2481 (class 0 OID 0)
-- Dependencies: 212
-- Name: audit_auditid_seq1; Type: SEQUENCE OWNED BY; Schema: mfmaindb; Owner: postgres
--

ALTER SEQUENCE audit_auditid_seq1 OWNED BY audit.auditid;


--
-- TOC entry 214 (class 1259 OID 88072)
-- Name: ld02_t_acslog; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE ld02_t_acslog (
    ld02_logid integer NOT NULL,
    ld02_classfilename character varying(255),
    ld02_dbname character varying(255),
    ld02_lineno integer,
    ld02_loglevel character varying(255),
    ld02_logmessage character varying(255),
    ld02_logtime timestamp without time zone,
    ld02_screenid character varying(255)
);


ALTER TABLE ld02_t_acslog OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 88080)
-- Name: ld03_t_stglog; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE ld03_t_stglog (
    ld03_logsettingid integer NOT NULL,
    ld03_logsetting integer
);


ALTER TABLE ld03_t_stglog OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 87561)
-- Name: ld03_t_stglog_sequence; Type: SEQUENCE; Schema: mfmaindb; Owner: postgres
--



--
-- TOC entry 228 (class 1259 OID 96278)
-- Name: menu; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE menu (
    menu_id bigint DEFAULT nextval('menu_seq'::regclass) NOT NULL,
    menu_code character varying(100),
    menu_desc character varying(200),
    menu_url character varying(100),
    menu_reference character varying(100)
);


ALTER TABLE menu OWNER TO postgres;

--
-- TOC entry 229 (class 1259 OID 96285)
-- Name: menu_grp_seq; Type: SEQUENCE; Schema: mfmaindb; Owner: postgres
--

CREATE SEQUENCE menu_grp_seq
    START WITH 107
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE menu_grp_seq OWNER TO postgres;

--
-- TOC entry 230 (class 1259 OID 96287)
-- Name: menu_grp; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE menu_grp (
    menu_grp_id bigint DEFAULT nextval('menu_grp_seq'::regclass) NOT NULL,
    menu_grp_parent bigint NOT NULL,
    menu_code character varying(100),
    menu_desc character varying(200),
    menu_url character varying(100),
    menu_reference character varying(100),
    menu_grp_root character varying(100),
    menu_id bigint,
    menu_grp_enable boolean
);


ALTER TABLE menu_grp OWNER TO postgres;

--
-- TOC entry 231 (class 1259 OID 96294)
-- Name: menu_grp_role_id_seq; Type: SEQUENCE; Schema: mfmaindb; Owner: postgres
--

CREATE SEQUENCE menu_grp_role_id_seq
    START WITH 11
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE menu_grp_role_id_seq OWNER TO postgres;

--
-- TOC entry 232 (class 1259 OID 96296)
-- Name: menu_grp_role; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE menu_grp_role (
    id bigint DEFAULT nextval('menu_grp_role_id_seq'::regclass) NOT NULL,
    menu_grp_id bigint,
    role_id bigint
);


ALTER TABLE menu_grp_role OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 88085)
-- Name: mf01_m_vendorbranch; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE mf01_m_vendorbranch (
    mf01_vendorcode character varying(255) NOT NULL,
    mf01_vendorbranchcode character varying(255) NOT NULL,
    mf01_branchdescription character varying(255),
    mf01_createdby character varying(255),
    mf01_createdon timestamp without time zone,
    mf01_deactivatedby character varying(255),
    mf01_deactivatedon timestamp without time zone,
    mf01_isactive boolean NOT NULL,
    mf01_lastcontactpersoncontactnumber character varying(255),
    mf01_lastcontactpersonemailaddress character varying(255),
    mf01_lastcontactpersonname character varying(255),
    mf01_lastmodifiedby character varying(255),
    mf01_lastmodifiedon timestamp without time zone,
    mf01_lastpocurrency character varying(255),
    mf01_postcode character varying(255),
    mf01_postcodearea character varying(255),
    mf01_reactivatedby character varying(255),
    mf01_reactivatedon timestamp without time zone,
    mf01_vendoraddressline1 character varying(255),
    mf01_vendoraddressline2 character varying(255),
    mf01_vendoraddressline3 character varying(255),
    mf01_vendorname character varying(255),
    mf02_creditterms_mf02_lastcredittermscode character varying(255),
    mf11_compcode character varying(255)
);


ALTER TABLE mf01_m_vendorbranch OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 88093)
-- Name: mf02_m_creditterms; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE mf02_m_creditterms (
    mf02_credittermscode character varying(255) NOT NULL,
    mf02_createdby character varying(255),
    mf02_createdon timestamp without time zone,
    mf02_credittermsdescription character varying(255),
    mf02_deactivatedby character varying(255),
    mf02_deactivatedon timestamp without time zone,
    mf02_isactive boolean NOT NULL,
    mf02_lastmodifiedby character varying(255),
    mf02_lastmodifiedon timestamp without time zone,
    mf02_reactivatedby character varying(255),
    mf02_reactivatedon timestamp without time zone,
    mf11_compcode character varying(255)
);


ALTER TABLE mf02_m_creditterms OWNER TO postgres;

--
-- TOC entry 218 (class 1259 OID 88101)
-- Name: mf03_m_vendorcrterms; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE mf03_m_vendorcrterms (
    mf02_creditterms_mf02_credittermscode character varying(255) NOT NULL,
    mf01_vendorbranch_mf01_vendorcode character varying(255) NOT NULL,
    mf01_vendorbranch_mf01_vendorbranchcode character varying(255) NOT NULL,
    mf03_createdby character varying(255),
    mf03_createdon timestamp without time zone,
    mf03_deactivatedby character varying(255),
    mf03_deactivatedon timestamp without time zone,
    mf03_isactive boolean,
    mf03_lastmodifiedby character varying(255),
    mf03_lastmodifiedon timestamp without time zone,
    mf03_reactivatedby character varying(255),
    mf03_reactivatedon timestamp without time zone,
    mf11_compcode character varying(255)
);


ALTER TABLE mf03_m_vendorcrterms OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 88109)
-- Name: mf04_m_profitcenterlastponumber; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE mf04_m_profitcenterlastponumber (
    mf13mpcmast_mf04_pccode character varying(255) NOT NULL,
    mf04_lastponumber character varying(255),
    mf04_runningno integer NOT NULL,
    mf04_year character varying(255)
);


ALTER TABLE mf04_m_profitcenterlastponumber OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 87563)
-- Name: mf06_r_mastdata_sequence; Type: SEQUENCE; Schema: mfmaindb; Owner: postgres
--


--
-- TOC entry 226 (class 1259 OID 96252)
-- Name: mf12_m_empmast; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE mf12_m_empmast (
    "mf12_psId" character varying(255) NOT NULL,
    "mf12_altId" character varying(255),
    "mf12_badgeNo" character varying(255),
    "mf12_confirmDt" date,
    "mf12_createdBy" character varying(255),
    "mf12_createdOn" timestamp without time zone,
    "mf12_deptCodeHRIS" character varying(255),
    "mf12_deptDescHRIS" character varying(255),
    mf12_dob date,
    mf12_email character varying(255),
    "mf12_empName" character varying(255),
    "mf12_empCat" character varying(255),
    mf12_ethnic character varying(255),
    "mf12_expJobEndDt" date,
    mf12_ext character varying(255),
    "mf12_foreignLocal" character varying(255),
    "mf12_frmCompCode" character varying(255),
    "mf12_frmLoc" character varying(255),
    "mf12_fromCostChargeCompany" character varying(255),
    "mf12_fromProfitCentre" character varying(255),
    mf12_gender character varying(255),
    "mf12_hireDt" date,
    "mf12_hrStatus" character varying(255),
    mf12_isactive boolean NOT NULL,
    "mf12_lastModifiedBy" character varying(255),
    "mf12_lastModifiedOn" timestamp without time zone,
    mf12_loc character varying(255),
    "mf12_locDesc" character varying(255),
    "mf12_lstDtWrk" date,
    "mf12_lstUpdDtTm" timestamp without time zone,
    "mf12_maritalStat" character varying(255),
    "mf12_newICNo" character varying(255),
    "mf12_oldICNo" character varying(255),
    "mf12_passportNo" character varying(255),
    "mf12_payGrp" character varying(255),
    "mf12_payrollComp" character varying(255),
    "mf12_payrollStatus" character varying(255),
    "mf12_phoneNo" character varying(255),
    "mf12_posNo" character varying(255),
    "mf12_posTitle" character varying(255),
    "mf12_prcDtTm" timestamp without time zone,
    "mf12_prefName" character varying(255),
    "mf12_rptToEmpId" character varying(255),
    "mf12_rptToPosNo" character varying(255),
    "mf12_salaryGrade" character varying(255),
    "mf12_salaryPlan" character varying(255),
    "mf12_toCompCode" character varying(255),
    "mf12_toCostChargeCompany" character varying(255),
    "mf12_toLoc" character varying(255),
    "mf12_toProfitCentre" character varying(255),
    "mf12_trfDt" date,
    "mf12_trfSndFlag" character varying(255),
    "mf11MCompmast_mf11_compCodeHRIS" character varying(255) NOT NULL,
    "mf13MPcmast_mf13_pcCode" character varying(255) NOT NULL,
    "mf19MJobmast_mf19_jobCode" character varying(255) NOT NULL
);


ALTER TABLE mf12_m_empmast OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 88117)
-- Name: mf22_m_scheduler; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE mf22_m_scheduler (
    mf22_schedulerid integer NOT NULL,
    mf22_createdby character varying(255),
    mf22_createdon timestamp without time zone,
    mf22_cronfrequency_amount character varying(255),
    mf22_cronfrequency_occurrence character varying(255),
    mf22_database character varying(255),
    mf22_deactivatedby character varying(255),
    mf22_deactivatedon timestamp without time zone,
    mf22_enddate timestamp without time zone,
    mf22_jobname character varying(255),
    mf22_noenddate boolean,
    mf22_path character varying(255),
    mf22_scheduletype character varying(255),
    mf22_startdate timestamp without time zone,
    mf22_status character varying(255),
    mf22_table character varying(255),
    mf22_task character varying(255),
    mf22_upload character varying(255)
);


ALTER TABLE mf22_m_scheduler OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 87573)
-- Name: mf22_m_scheduler_mf22_schedulerid_seq; Type: SEQUENCE; Schema: mfmaindb; Owner: postgres
--

CREATE SEQUENCE mf22_m_scheduler_mf22_schedulerid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE mf22_m_scheduler_mf22_schedulerid_seq OWNER TO postgres;

--
-- TOC entry 2482 (class 0 OID 0)
-- Dependencies: 207
-- Name: mf22_m_scheduler_mf22_schedulerid_seq; Type: SEQUENCE OWNED BY; Schema: mfmaindb; Owner: postgres
--

ALTER SEQUENCE mf22_m_scheduler_mf22_schedulerid_seq OWNED BY "Mf22_M_SCHEDULER".mf22_schedulerid;


--
-- TOC entry 233 (class 1259 OID 96300)
-- Name: permissions; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE permissions (
    id integer NOT NULL,
    description character varying(1024),
    name character varying(255) NOT NULL
);


ALTER TABLE permissions OWNER TO postgres;

--
-- TOC entry 221 (class 1259 OID 88125)
-- Name: po06_t_user; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE po06_t_user (
    po06_psid character varying(255) NOT NULL,
    po06_createdby character varying(255),
    po06_createdon timestamp without time zone,
    po06_deactivatedby character varying(255),
    po06_deactivatedon timestamp without time zone,
    po06_empname character varying(255),
    po06_firsttimelogin timestamp without time zone,
    po06_isactive boolean NOT NULL,
    po06_lastlogin timestamp without time zone,
    po06_lastmodifiedby character varying(255),
    po06_lastmodifiedon timestamp without time zone,
    po06_numberoflogin integer NOT NULL,
    po06_reactivatedby character varying(255),
    po06_reactivatedon timestamp without time zone
);


ALTER TABLE po06_t_user OWNER TO postgres;

--
-- TOC entry 222 (class 1259 OID 88133)
-- Name: po_dblist2; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE po_dblist2 (
    dblistseq integer NOT NULL,
    company character varying(255),
    company_abbr character varying(255),
    datname character varying(255),
    dbyear character varying(255),
    isactive boolean NOT NULL
);


ALTER TABLE po_dblist2 OWNER TO postgres;

--
-- TOC entry 223 (class 1259 OID 88143)
-- Name: podblist__seq; Type: SEQUENCE; Schema: mfmaindb; Owner: postgres
--

CREATE SEQUENCE podblist__seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE podblist__seq OWNER TO postgres;

--
-- TOC entry 234 (class 1259 OID 96306)
-- Name: role_permission; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE role_permission (
    role_id integer NOT NULL,
    perm_id integer NOT NULL
);


ALTER TABLE role_permission OWNER TO postgres;

--
-- TOC entry 235 (class 1259 OID 96309)
-- Name: roles; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE roles (
    role_id integer NOT NULL,
    role_description character varying(1024),
    role_name character varying(255) NOT NULL
);


ALTER TABLE roles OWNER TO postgres;

--
-- TOC entry 236 (class 1259 OID 96315)
-- Name: user_role; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE user_role (
    user_id character varying(100) NOT NULL,
    role_id integer NOT NULL
);


ALTER TABLE user_role OWNER TO postgres;

--
-- TOC entry 237 (class 1259 OID 96318)
-- Name: users; Type: TABLE; Schema: mfmaindb; Owner: postgres
--

CREATE TABLE users (
    id integer NOT NULL,
    email character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    password_reset_token character varying(255)
);


ALTER TABLE users OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 88001)
-- Name: vw01_loginview; Type: VIEW; Schema: mfmaindb; Owner: postgres
--

CREATE VIEW vw01_loginview AS
 SELECT c."mf11_compCode",
    l.po01_year,
    c."mf11_compAbbrName",
    c."mf11_compName",
    c."mf11_isActive"
   FROM ("PO01_LOGINLIST" l
     JOIN "MF11_M_COMPMAST" c ON (((l.po01_companycode)::text = (c."mf11_compCode")::text)));


ALTER TABLE vw01_loginview OWNER TO postgres;


--
-- TOC entry 224 (class 1259 OID 88146)
-- Name: po01_t_main; Type: TABLE; Schema: pocomptrans; Owner: postgres
--

CREATE TABLE po01_t_main (
    po01_pono character varying(255) NOT NULL,
    mf01mvendorbranch_po01_vendorcode character varying(255),
    mf02mcreditterms_po01_credittermscode character varying(255),
    mf11mcompmast_po01_compcode character varying(255),
    mf12mempmast_po01_authorizer character varying(255),
    mf12mempmast_po01_issuer character varying(255),
    mf12mempmast_po01_verifier character varying(255),
    mf13mpcmast_po01_pccode character varying(255),
    mf15mprmast_po01_prtypecode character varying(255),
    mf17mordcatmast_po01_ordcatcode character varying(255),
    mf18mordsubcmast_po01_ordsubccode character varying(255),
    po01_addressline1 character varying(255),
    po01_addressline2 character varying(255),
    po01_addressline3 character varying(255),
    po01_authorizeremark character varying(255),
    po01_authorizedon timestamp without time zone,
    po01_cancelremark character varying(255),
    po01_cancelledby character varying(255),
    po01_cancelledon timestamp without time zone,
    po01_closeexception character varying(255),
    po01_closedby character varying(255),
    po01_closedon timestamp without time zone,
    po01_contactperson character varying(255),
    po01_contactpersonemail character varying(255),
    po01_contactpersonname character varying(255),
    po01_contactpersonphone character varying(255),
    po01_currency character varying(255),
    po01_deleteremark character varying(255),
    po01_deletedby character varying(255),
    po01_deletedon timestamp without time zone,
    po01_deliveryinstruction character varying(255),
    po01_departmentname character varying(255),
    po01_expiredate timestamp without time zone,
    po01_expirydays integer NOT NULL,
    po01_expirystatus boolean NOT NULL,
    po01_firstapprover character varying(255),
    po01_firstapproverdesignation character varying(255),
    po01_forwardingstatus character varying(255),
    po01_issuedon timestamp without time zone,
    po01_issuerremark character varying(255),
    po01_lastprintedby character varying(255),
    po01_lastprintedon timestamp without time zone,
    po01_ordercategoryauthorizationgrid character varying(255),
    po01_overalldiscount double precision NOT NULL,
    po01_paymentterms character varying(255),
    po01_postarea character varying(255),
    po01_postcode character varying(255),
    po01_quotedate timestamp without time zone,
    po01_quoteno character varying(255),
    po01_redraftremark character varying(255),
    po01_rejectremark character varying(255),
    po01_rejectedby character varying(255),
    po01_rejectedon timestamp without time zone,
    po01_secondapprover character varying(255),
    po01_secondapproverdesignation character varying(255),
    po01_status character varying(255),
    po01_storename character varying(255),
    po01_termsandconditionslandscape character varying(255),
    po01_termsandconditionsportrait character varying(255),
    po01_thirdapprover character varying(255),
    po01_thirdapproverdesignation character varying(255),
    po01_totexclgst double precision NOT NULL,
    po01_totgst double precision NOT NULL,
    po01_totinclgst double precision NOT NULL,
    po01_vendoraddressline1 character varying(255),
    po01_vendoraddressline2 character varying(255),
    po01_vendoraddressline3 character varying(255),
    po01_vendorname character varying(255),
    po01_vendorpersonincharge character varying(255),
    po01_vendorpersoninchargeemail character varying(255),
    po01_vendorpersoninchargephone character varying(255),
    po01_vendorpostarea character varying(255),
    po01_vendorpostcode character varying(255),
    po01_verifiedon timestamp without time zone,
    po01_verifyremark character varying(255),
    po03tproject_po01_projectcode character varying(255)
);




--
-- TOC entry 2206 (class 2604 OID 87587)
-- Name: mf06_mastDataId; Type: DEFAULT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY "MF06_R_MASTDATA" ALTER COLUMN "mf06_mastDataId" SET DEFAULT nextval('"MF06_R_MASTDATA_mf06_mastDataId_seq"'::regclass);


--
-- TOC entry 2207 (class 2604 OID 87588)
-- Name: mf07_sysParamCode; Type: DEFAULT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY "MF07_R_SYSPARAM" ALTER COLUMN "mf07_sysParamCode" SET DEFAULT nextval('"MF07_R_SYSPARAM_mf07_sysParamId_seq"'::regclass);


--
-- TOC entry 2208 (class 2604 OID 87589)
-- Name: mf22_schedulerid; Type: DEFAULT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY "Mf22_M_SCHEDULER" ALTER COLUMN mf22_schedulerid SET DEFAULT nextval('mf22_m_scheduler_mf22_schedulerid_seq'::regclass);


--
-- TOC entry 2210 (class 2604 OID 88066)
-- Name: auditid; Type: DEFAULT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY audit ALTER COLUMN auditid SET DEFAULT nextval('audit_auditid_seq1'::regclass);


--
-- TOC entry 2417 (class 0 OID 87459)
-- Dependencies: 184
-- Data for Name: LD02_T_ACSLOG; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51065, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51066, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51067, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51068, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51069, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51070, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51071, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51072, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51073, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51074, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51075, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51076, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51077, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51078, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51079, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51080, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51081, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51082, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51083, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51084, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51085, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51086, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51087, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51088, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51089, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51090, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51091, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51092, 'MainScreenView.java', 90, 'DEBUG', 'Building menu content for superadmin user', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51093, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51094, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51095, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51096, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51097, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51098, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51099, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51100, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51101, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51102, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51103, 'AdminView.java', 150, 'DEBUG', 'Initializing screen components behaviour', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51104, 'AdminView.java', 169, 'DEBUG', 'ERROR: SYSPARAM: User validity Period is missing', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51105, 'AdminView.java', 226, 'DEBUG', 'Setting component caption', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51106, 'AdminView.java', 299, 'DEBUG', 'Setting fields value change listener', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51107, 'AdminView.java', 318, 'DEBUG', 'Setting fields validator', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51108, 'AdminView.java', 325, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51109, 'AdminView.java', 350, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51110, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51111, 'AdminView.java', 355, 'DEBUG', 'Getting all admin records', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51112, 'AdminView.java', 366, 'DEBUG', 'Binding data to paged container', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51113, 'AdminView.java', 380, 'DEBUG', 'Hiding unwanted attributes', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51114, 'AdminView.java', 387, 'DEBUG', 'Setting data source of grid', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51115, 'AdminView.java', 415, 'DEBUG', 'Setting initial state of grid', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51116, 'AdminView.java', 434, 'DEBUG', 'Adding "edit" column', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51117, 'MainScreenView.java', 194, 'DEBUG', 'Building header', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51118, 'MainScreenView.java', 201, 'DEBUG', 'Setting logo in header', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51119, 'MainScreenView.java', 234, 'DEBUG', 'Setting menu for screen setting', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51120, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51121, 'MainScreenView.java', 274, 'DEBUG', 'Setting menu for user profile setting', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51122, 'MainScreenView.java', 339, 'DEBUG', 'JavaScript function for closing popup when page scrolls', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51123, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51124, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51125, 'AdminView.java', 464, 'DEBUG', 'Getting all admin records', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51126, 'AdminView.java', 469, 'DEBUG', 'Binding data to paged container', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51127, 'AdminView.java', 890, 'DEBUG', 'Entering Company Screen', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51128, 'Logs.java', 59, 'DEBUG', 'LD04_T_AUDIT record is successfully saved into the database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51129, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51130, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51131, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51132, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51133, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51134, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51135, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51136, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51137, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51138, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51139, 'CreatePOuserView.java', 168, 'DEBUG', 'Initializing screen components behaviour', NULL, 'com.parkson.poAdmin.master.CreatePOuserView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51140, 'CreatePOuserView.java', 227, 'DEBUG', 'Setting component caption', NULL, 'com.parkson.poAdmin.master.CreatePOuserView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51141, 'CreatePOuserView.java', 233, 'DEBUG', 'Setting click listener for cancel button', NULL, 'com.parkson.poAdmin.master.CreatePOuserView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51142, 'CreatePOuserView.java', 257, 'DEBUG', 'Setting click listener for cancel button', NULL, 'com.parkson.poAdmin.master.CreatePOuserView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51143, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.org.hibernate.exception.SQLGrammarException: could not extract ResultSet', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51144, 'Logs.java', 71, 'DEBUG', 'PO06_T_USER records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51145, 'CreatePOuserView.java', 262, 'DEBUG', 'Getting all admin records', NULL, 'com.parkson.poAdmin.master.CreatePOuserView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51146, 'CreatePOuserView.java', 270, 'DEBUG', 'Binding data to paged container', NULL, 'com.parkson.poAdmin.master.CreatePOuserView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51147, 'CreatePOuserView.java', 284, 'DEBUG', 'Hiding unwanted attributes', NULL, 'com.parkson.poAdmin.master.CreatePOuserView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51148, 'CreatePOuserView.java', 291, 'DEBUG', 'Setting data source of grid', NULL, 'com.parkson.poAdmin.master.CreatePOuserView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51149, 'CreatePOuserView.java', 307, 'DEBUG', 'Setting initial state of grid', NULL, 'com.parkson.poAdmin.master.CreatePOuserView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51150, 'CreatePOuserView.java', 326, 'DEBUG', 'Adding "edit" column', NULL, 'com.parkson.poAdmin.master.CreatePOuserView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51151, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.could not extract ResultSet', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51152, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.PO06_T_USER', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51153, 'Logs.java', 71, 'DEBUG', 'MF12_M_EMPMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51154, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51155, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.org.hibernate.exception.SQLGrammarException: could not extract ResultSet', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51156, 'Logs.java', 71, 'DEBUG', 'PO06_T_USER records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51157, 'Logs.java', 71, 'DEBUG', 'MF12_M_EMPMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51158, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.could not extract ResultSet', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51159, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.PO06_T_USER', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51160, 'CreatePOuserView.java', 682, 'DEBUG', 'Entering Company Screen', NULL, 'com.parkson.poAdmin.master.CreatePOuserView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51161, 'LogView.java', 176, 'DEBUG', 'Getting all log records', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51162, 'Logs.java', 71, 'DEBUG', 'LD04_T_AUDIT records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51163, 'auditTrailView.java', 88, 'DEBUG', 'Closing file input stream', NULL, 'PO-UT01L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51164, 'Logs.java', 71, 'DEBUG', 'LD04_T_AUDIT records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51165, 'auditTrailView.java', 104, 'DEBUG', 'Getting all audit records', NULL, 'PO-UT01L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51166, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51167, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51168, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51169, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51170, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51171, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51172, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51173, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51174, 'CompanyView.java', 201, 'DEBUG', 'Initializing screen components behaviour', NULL, 'PO-AD11L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51175, 'CompanyView.java', 245, 'DEBUG', 'Closing file input stream', NULL, 'PO-AD11L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51176, 'CompanyView.java', 275, 'DEBUG', 'Setting component caption', NULL, 'PO-AD11L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51177, 'CompanyView.java', 284, 'DEBUG', 'Setting fields validator', NULL, 'PO-AD11L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51178, 'CompanyView.java', 648, 'DEBUG', 'Setting fields value change listener', NULL, 'PO-AD11L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51179, 'CompanyView.java', 664, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-AD11L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51180, 'CompanyView.java', 715, 'DEBUG', 'Setting click listener for save button', NULL, 'PO-AD11L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51181, 'Logs.java', 71, 'DEBUG', 'MF11_M_COMPMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51182, 'CompanyView.java', 1585, 'DEBUG', 'Entering Company Screen', NULL, 'PO-AD11L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51183, 'Logs.java', 71, 'DEBUG', 'MF11_M_COMPMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51184, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51185, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51186, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51187, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51188, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51189, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51190, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51191, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51192, 'JobMasterView.java', 98, 'DEBUG', 'Initializing screen components behaviour', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51193, 'JobMasterView.java', 132, 'DEBUG', 'Closing file input stream', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51194, 'JobMasterView.java', 154, 'DEBUG', 'Setting component caption', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51195, 'JobMasterView.java', 159, 'DEBUG', 'Setting fields validator', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51196, 'JobMasterView.java', 170, 'DEBUG', 'Setting fields value change listener', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51197, 'JobMasterView.java', 175, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51198, 'JobMasterView.java', 198, 'DEBUG', 'Setting click listener for save button', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51199, 'Logs.java', 71, 'DEBUG', 'MF19_M_JOBMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51200, 'JobMasterView.java', 203, 'DEBUG', 'Getting all job master records', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51201, 'JobMasterView.java', 211, 'DEBUG', 'Wrapping Job Master record to container that can be understood by Grid', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51202, 'JobMasterView.java', 226, 'DEBUG', 'Hiding unwanted attributes', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51203, 'JobMasterView.java', 233, 'DEBUG', 'Filling grid with data', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51204, 'JobMasterView.java', 265, 'DEBUG', 'Adding "edit" column', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51205, 'JobMasterView.java', 699, 'DEBUG', 'Entering Company Screen', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51206, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51207, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51208, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51209, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51210, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51211, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51212, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51213, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51214, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51215, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51216, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51217, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51218, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51219, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51220, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51221, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51222, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51223, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51224, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51225, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51226, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51227, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51228, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51229, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51230, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51231, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51232, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51233, 'MainScreenView.java', 90, 'DEBUG', 'Building menu content for superadmin user', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51234, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51235, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51236, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51237, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51238, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51239, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51240, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51241, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51242, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51243, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51244, 'AdminView.java', 150, 'DEBUG', 'Initializing screen components behaviour', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51245, 'AdminView.java', 169, 'DEBUG', 'ERROR: SYSPARAM: User validity Period is missing', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51246, 'AdminView.java', 226, 'DEBUG', 'Setting component caption', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51247, 'AdminView.java', 299, 'DEBUG', 'Setting fields value change listener', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51248, 'AdminView.java', 318, 'DEBUG', 'Setting fields validator', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51249, 'AdminView.java', 325, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51250, 'AdminView.java', 350, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51251, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51252, 'AdminView.java', 355, 'DEBUG', 'Getting all admin records', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51253, 'AdminView.java', 366, 'DEBUG', 'Binding data to paged container', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51254, 'AdminView.java', 380, 'DEBUG', 'Hiding unwanted attributes', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51255, 'AdminView.java', 387, 'DEBUG', 'Setting data source of grid', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51256, 'AdminView.java', 415, 'DEBUG', 'Setting initial state of grid', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51257, 'AdminView.java', 434, 'DEBUG', 'Adding "edit" column', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51258, 'MainScreenView.java', 194, 'DEBUG', 'Building header', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51259, 'MainScreenView.java', 201, 'DEBUG', 'Setting logo in header', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51260, 'MainScreenView.java', 234, 'DEBUG', 'Setting menu for screen setting', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51261, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51262, 'MainScreenView.java', 274, 'DEBUG', 'Setting menu for user profile setting', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51263, 'MainScreenView.java', 339, 'DEBUG', 'JavaScript function for closing popup when page scrolls', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51264, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51527, 'AdminView.java', 226, 'DEBUG', 'Setting component caption', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51265, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51266, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51267, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51268, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51269, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51270, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51271, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51272, 'JobMasterView.java', 98, 'DEBUG', 'Initializing screen components behaviour', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51273, 'JobMasterView.java', 132, 'DEBUG', 'Closing file input stream', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51274, 'JobMasterView.java', 154, 'DEBUG', 'Setting component caption', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51275, 'JobMasterView.java', 159, 'DEBUG', 'Setting fields validator', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51276, 'JobMasterView.java', 170, 'DEBUG', 'Setting fields value change listener', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51277, 'JobMasterView.java', 175, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51278, 'JobMasterView.java', 198, 'DEBUG', 'Setting click listener for save button', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51279, 'Logs.java', 71, 'DEBUG', 'MF19_M_JOBMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51280, 'JobMasterView.java', 203, 'DEBUG', 'Getting all job master records', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51281, 'JobMasterView.java', 211, 'DEBUG', 'Wrapping Job Master record to container that can be understood by Grid', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51282, 'JobMasterView.java', 226, 'DEBUG', 'Hiding unwanted attributes', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51283, 'JobMasterView.java', 233, 'DEBUG', 'Filling grid with data', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51284, 'JobMasterView.java', 265, 'DEBUG', 'Adding "edit" column', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51285, 'JobMasterView.java', 699, 'DEBUG', 'Entering Company Screen', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51286, 'Logs.java', 59, 'DEBUG', 'LD04_T_AUDIT record is successfully saved into the database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51287, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51288, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51289, 'AdminView.java', 464, 'DEBUG', 'Getting all admin records', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51290, 'AdminView.java', 469, 'DEBUG', 'Binding data to paged container', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51291, 'AdminView.java', 890, 'DEBUG', 'Entering Company Screen', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51292, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51293, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51294, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51295, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51296, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51297, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51298, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51299, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51300, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51301, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51302, 'CreatePOuserView.java', 168, 'DEBUG', 'Initializing screen components behaviour', NULL, 'com.parkson.poAdmin.master.CreatePOuserView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51303, 'CreatePOuserView.java', 227, 'DEBUG', 'Setting component caption', NULL, 'com.parkson.poAdmin.master.CreatePOuserView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51304, 'CreatePOuserView.java', 233, 'DEBUG', 'Setting click listener for cancel button', NULL, 'com.parkson.poAdmin.master.CreatePOuserView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51305, 'CreatePOuserView.java', 257, 'DEBUG', 'Setting click listener for cancel button', NULL, 'com.parkson.poAdmin.master.CreatePOuserView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51306, 'Logs.java', 71, 'DEBUG', 'PO06_T_USER records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51307, 'CreatePOuserView.java', 262, 'DEBUG', 'Getting all admin records', NULL, 'com.parkson.poAdmin.master.CreatePOuserView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51308, 'CreatePOuserView.java', 270, 'DEBUG', 'Binding data to paged container', NULL, 'com.parkson.poAdmin.master.CreatePOuserView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51309, 'CreatePOuserView.java', 284, 'DEBUG', 'Hiding unwanted attributes', NULL, 'com.parkson.poAdmin.master.CreatePOuserView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51310, 'CreatePOuserView.java', 291, 'DEBUG', 'Setting data source of grid', NULL, 'com.parkson.poAdmin.master.CreatePOuserView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51311, 'CreatePOuserView.java', 307, 'DEBUG', 'Setting initial state of grid', NULL, 'com.parkson.poAdmin.master.CreatePOuserView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51312, 'CreatePOuserView.java', 326, 'DEBUG', 'Adding "edit" column', NULL, 'com.parkson.poAdmin.master.CreatePOuserView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51313, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.PO06_T_USER', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51314, 'Logs.java', 71, 'DEBUG', 'MF12_M_EMPMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51315, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51316, 'Logs.java', 71, 'DEBUG', 'PO06_T_USER records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51317, 'Logs.java', 71, 'DEBUG', 'MF12_M_EMPMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51318, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.PO06_T_USER', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51319, 'CreatePOuserView.java', 682, 'DEBUG', 'Entering Company Screen', NULL, 'com.parkson.poAdmin.master.CreatePOuserView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51320, 'LogView.java', 176, 'DEBUG', 'Getting all log records', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51321, 'Logs.java', 71, 'DEBUG', 'LD04_T_AUDIT records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51322, 'auditTrailView.java', 88, 'DEBUG', 'Closing file input stream', NULL, 'PO-UT01L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51323, 'Logs.java', 71, 'DEBUG', 'LD04_T_AUDIT records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51324, 'auditTrailView.java', 104, 'DEBUG', 'Getting all audit records', NULL, 'PO-UT01L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51325, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51326, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51327, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51328, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51329, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51330, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51331, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51332, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51333, 'CompanyView.java', 201, 'DEBUG', 'Initializing screen components behaviour', NULL, 'PO-AD11L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51334, 'CompanyView.java', 245, 'DEBUG', 'Closing file input stream', NULL, 'PO-AD11L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51335, 'CompanyView.java', 275, 'DEBUG', 'Setting component caption', NULL, 'PO-AD11L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51336, 'CompanyView.java', 284, 'DEBUG', 'Setting fields validator', NULL, 'PO-AD11L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51337, 'CompanyView.java', 648, 'DEBUG', 'Setting fields value change listener', NULL, 'PO-AD11L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51338, 'CompanyView.java', 664, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-AD11L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51339, 'CompanyView.java', 715, 'DEBUG', 'Setting click listener for save button', NULL, 'PO-AD11L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51340, 'Logs.java', 71, 'DEBUG', 'MF11_M_COMPMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51341, 'CompanyView.java', 1585, 'DEBUG', 'Entering Company Screen', NULL, 'PO-AD11L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51342, 'Logs.java', 71, 'DEBUG', 'MF11_M_COMPMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51343, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51344, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51345, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51346, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51347, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51348, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51349, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51350, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51351, 'StoreView.java', 149, 'DEBUG', 'Initializing screen components behaviour', NULL, 'PO-AD02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51352, 'StoreView.java', 189, 'DEBUG', 'Closing file input stream', NULL, 'PO-AD02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51353, 'StoreView.java', 215, 'DEBUG', 'Setting component caption', NULL, 'PO-AD02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51354, 'StoreView.java', 221, 'DEBUG', 'Setting component styles', NULL, 'PO-AD02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51355, 'StoreView.java', 228, 'DEBUG', 'Setting fields validator', NULL, 'PO-AD02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51356, 'StoreView.java', 239, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-AD02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51357, 'StoreView.java', 251, 'DEBUG', 'Setting state of combobox', NULL, 'PO-AD02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51358, 'StoreView.java', 258, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-AD02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51359, 'StoreView.java', 284, 'DEBUG', 'Setting click listener for save button', NULL, 'PO-AD02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51360, 'Logs.java', 71, 'DEBUG', 'MF02_M_STRMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51361, 'StoreView.java', 292, 'DEBUG', 'Getting all store records', NULL, 'PO-AD02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51362, 'StoreView.java', 957, 'DEBUG', 'Entering Store Screen', NULL, 'PO-AD02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51363, 'Logs.java', 71, 'DEBUG', 'MF11_M_COMPMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51364, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51365, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51366, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51367, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51368, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51369, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51370, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51371, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51372, 'DepartmentView.java', 102, 'DEBUG', 'Initializing screen components behaviour', NULL, 'PO-AD14L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51373, 'DepartmentView.java', 137, 'DEBUG', 'Closing file input stream', NULL, 'PO-AD14L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51374, 'DepartmentView.java', 159, 'DEBUG', 'Setting component caption', NULL, 'PO-AD14L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51375, 'DepartmentView.java', 174, 'DEBUG', 'Setting fields validator', NULL, 'PO-AD14L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51376, 'DepartmentView.java', 182, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-AD14L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51377, 'DepartmentView.java', 211, 'DEBUG', 'Setting click listener for save button', NULL, 'PO-AD14L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51378, 'Logs.java', 71, 'DEBUG', 'MF14_M_DEPTMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51379, 'DepartmentView.java', 217, 'DEBUG', 'Getting all department records', NULL, 'PO-AD14L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51380, 'DepartmentView.java', 417, 'DEBUG', 'Clear form after click cancel button', NULL, 'PO-AD14L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51381, 'DepartmentView.java', 714, 'DEBUG', 'Entering department Screen', NULL, 'PO-AD14L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51382, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51383, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51384, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51385, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51386, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51387, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51388, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51389, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51390, 'ProfitCenterView.java', 177, 'DEBUG', 'Initializing screen components behaviour', NULL, 'PO-AD13L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51391, 'ProfitCenterView.java', 195, 'DEBUG', 'Closing file input stream', NULL, 'PO-AD13L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51392, 'Logs.java', 71, 'DEBUG', 'MF14_M_DEPTMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51393, 'Logs.java', 71, 'DEBUG', 'MF11_M_COMPMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51394, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.[Ljava.lang.Object; cannot be cast to com.parkson.poAdmin.backend.data.MF02_M_STRMAST', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51395, 'Logs.java', 71, 'DEBUG', 'MF02_M_STRMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51396, 'Logs.java', 71, 'DEBUG', 'MF13_M_PCMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51397, 'ProfitCenterView.java', 720, 'DEBUG', 'Setting fields value change listener', NULL, 'PO-AD13L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51398, 'Logs.java', 71, 'DEBUG', 'MF14_M_DEPTMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51399, 'Logs.java', 71, 'DEBUG', 'MF11_M_COMPMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51400, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.[Ljava.lang.Object; cannot be cast to com.parkson.poAdmin.backend.data.MF02_M_STRMAST', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51401, 'Logs.java', 71, 'DEBUG', 'MF02_M_STRMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51402, 'Logs.java', 71, 'DEBUG', 'MF11_M_COMPMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51403, 'Logs.java', 71, 'DEBUG', 'MF14_M_DEPTMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51404, 'Logs.java', 71, 'DEBUG', 'MF12_M_EMPMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51405, 'Logs.java', 71, 'DEBUG', 'MF12_M_EMPMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51406, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.[Ljava.lang.Object; cannot be cast to com.parkson.poAdmin.backend.data.MF02_M_STRMAST', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51407, 'Logs.java', 71, 'DEBUG', 'MF02_M_STRMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51408, 'ProfitCenterView.java', 1926, 'DEBUG', 'Entering profit center Screen', NULL, 'PO-AD13L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51409, 'ProfitCenterView.java', 319, 'DEBUG', 'DEBUG on rendering Company Code', NULL, 'PO-AD13L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51410, 'ProfitCenterView.java', 356, 'DEBUG', 'DEBUG on rendering Department Code', NULL, 'PO-AD13L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51411, 'ProfitCenterView.java', 337, 'DEBUG', 'DEBUG on rendering Store Code', NULL, 'PO-AD13L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51412, 'ProfitCenterView.java', 319, 'DEBUG', 'DEBUG on rendering Company Code', NULL, 'PO-AD13L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51413, 'ProfitCenterView.java', 356, 'DEBUG', 'DEBUG on rendering Department Code', NULL, 'PO-AD13L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51414, 'ProfitCenterView.java', 337, 'DEBUG', 'DEBUG on rendering Store Code', NULL, 'PO-AD13L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51415, 'ProfitCenterView.java', 319, 'DEBUG', 'DEBUG on rendering Company Code', NULL, 'PO-AD13L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51416, 'ProfitCenterView.java', 356, 'DEBUG', 'DEBUG on rendering Department Code', NULL, 'PO-AD13L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51417, 'ProfitCenterView.java', 337, 'DEBUG', 'DEBUG on rendering Store Code', NULL, 'PO-AD13L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51418, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51419, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51420, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51421, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51422, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51423, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51424, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51425, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51426, 'SystemParametersView.java', 103, 'DEBUG', 'Initializing screen components behaviour', NULL, 'PO-AD07L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51427, 'SystemParametersView.java', 139, 'DEBUG', 'Closing file input stream', NULL, 'PO-AD07L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51428, 'SystemParametersView.java', 161, 'DEBUG', 'Setting component caption', NULL, 'PO-AD07L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51429, 'SystemParametersView.java', 167, 'DEBUG', 'Setting fields validator', NULL, 'PO-AD07L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51430, 'SystemParametersView.java', 181, 'DEBUG', 'Setting fields value change listener', NULL, 'PO-AD07L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51431, 'SystemParametersView.java', 185, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-AD07L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51432, 'SystemParametersView.java', 208, 'DEBUG', 'Setting click listener for save button', NULL, 'PO-AD07L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51433, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51434, 'SystemParametersView.java', 218, 'DEBUG', 'Getting all System Parameters records', NULL, 'PO-AD07L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51435, 'SystemParametersView.java', 226, 'DEBUG', 'Wrapping System Parameters record to container that can be understood by Grid', NULL, 'PO-AD07L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51436, 'SystemParametersView.java', 244, 'DEBUG', 'Hiding unwanted attributes', NULL, 'PO-AD07L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51437, 'SystemParametersView.java', 251, 'DEBUG', 'Filling grid with data', NULL, 'PO-AD07L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51438, 'SystemParametersView.java', 282, 'DEBUG', 'Adding "edit" column', NULL, 'PO-AD07L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51439, 'SystemParametersView.java', 731, 'DEBUG', 'Inside System Parameters View', NULL, 'PO-AD07L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51440, 'JobMasterView.java', 98, 'DEBUG', 'Initializing screen components behaviour', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51441, 'JobMasterView.java', 132, 'DEBUG', 'Closing file input stream', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51442, 'JobMasterView.java', 154, 'DEBUG', 'Setting component caption', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51443, 'JobMasterView.java', 159, 'DEBUG', 'Setting fields validator', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51444, 'JobMasterView.java', 170, 'DEBUG', 'Setting fields value change listener', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51445, 'JobMasterView.java', 175, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51446, 'JobMasterView.java', 198, 'DEBUG', 'Setting click listener for save button', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51447, 'Logs.java', 71, 'DEBUG', 'MF19_M_JOBMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51448, 'JobMasterView.java', 203, 'DEBUG', 'Getting all job master records', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51449, 'JobMasterView.java', 211, 'DEBUG', 'Wrapping Job Master record to container that can be understood by Grid', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51450, 'JobMasterView.java', 226, 'DEBUG', 'Hiding unwanted attributes', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51451, 'JobMasterView.java', 233, 'DEBUG', 'Filling grid with data', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51452, 'JobMasterView.java', 265, 'DEBUG', 'Adding "edit" column', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51453, 'JobMasterView.java', 699, 'DEBUG', 'Entering Company Screen', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51454, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51455, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51456, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51457, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51458, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51459, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51460, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51461, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51462, 'EmployeeView.java', 88, 'DEBUG', 'Closing file input stream', NULL, 'PO-AD12L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52122, 'AdminView.java', 366, 'DEBUG', 'Binding data to paged container', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51463, 'Logs.java', 71, 'DEBUG', 'MF12_M_EMPMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51464, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51465, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51466, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51467, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51468, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51469, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51470, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51471, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51472, 'OrderSubCatView.java', 104, 'DEBUG', 'Initializing screen components behaviour', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51473, 'OrderSubCatView.java', 138, 'DEBUG', 'Closing file input stream', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51474, 'OrderSubCatView.java', 161, 'DEBUG', 'Setting component caption', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51475, 'OrderSubCatView.java', 167, 'DEBUG', 'Setting fields validator', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51476, 'OrderSubCatView.java', 184, 'DEBUG', 'Setting fields value change listener', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51477, 'OrderSubCatView.java', 195, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51478, 'OrderSubCatView.java', 217, 'DEBUG', 'Setting click listener for save button', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51479, 'Logs.java', 71, 'DEBUG', 'MF18_M_ORDSUBCMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51480, 'OrderSubCatView.java', 222, 'DEBUG', 'Getting all order sub category records', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51481, 'OrderSubCatView.java', 228, 'DEBUG', 'Wrapping order sub category records to container that can be understood by Grid', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51482, 'OrderSubCatView.java', 243, 'DEBUG', 'Hiding unwanted attributes', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51483, 'OrderSubCatView.java', 250, 'DEBUG', 'Filling grid with data', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51484, 'OrderSubCatView.java', 281, 'DEBUG', 'Adding "edit" column', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51485, 'OrderSubCatView.java', 731, 'DEBUG', 'Inside Order Sub Category View', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51486, 'Logs.java', 71, 'DEBUG', 'MF17_M_ORDCATMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51487, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51488, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51489, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51490, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51491, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51492, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51493, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51494, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51495, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51496, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51497, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51498, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51499, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51500, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51501, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51502, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51503, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51504, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51505, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51506, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51507, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51508, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51509, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51510, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51511, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51512, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51513, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51514, 'MainScreenView.java', 90, 'DEBUG', 'Building menu content for superadmin user', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51515, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51516, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51517, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51518, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51519, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51520, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51521, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51522, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51523, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51524, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51525, 'AdminView.java', 150, 'DEBUG', 'Initializing screen components behaviour', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51526, 'AdminView.java', 169, 'DEBUG', 'ERROR: SYSPARAM: User validity Period is missing', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51528, 'AdminView.java', 299, 'DEBUG', 'Setting fields value change listener', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51529, 'AdminView.java', 318, 'DEBUG', 'Setting fields validator', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51530, 'AdminView.java', 325, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51531, 'AdminView.java', 350, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51532, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51533, 'AdminView.java', 355, 'DEBUG', 'Getting all admin records', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51534, 'AdminView.java', 366, 'DEBUG', 'Binding data to paged container', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51535, 'AdminView.java', 380, 'DEBUG', 'Hiding unwanted attributes', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51536, 'AdminView.java', 387, 'DEBUG', 'Setting data source of grid', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51537, 'AdminView.java', 415, 'DEBUG', 'Setting initial state of grid', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51538, 'AdminView.java', 434, 'DEBUG', 'Adding "edit" column', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51539, 'MainScreenView.java', 194, 'DEBUG', 'Building header', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51540, 'MainScreenView.java', 201, 'DEBUG', 'Setting logo in header', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51541, 'MainScreenView.java', 234, 'DEBUG', 'Setting menu for screen setting', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51542, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51543, 'MainScreenView.java', 274, 'DEBUG', 'Setting menu for user profile setting', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51544, 'MainScreenView.java', 339, 'DEBUG', 'JavaScript function for closing popup when page scrolls', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51545, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51546, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51547, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51548, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51549, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51550, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51551, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51552, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51553, 'OrderSubCatView.java', 104, 'DEBUG', 'Initializing screen components behaviour', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51554, 'OrderSubCatView.java', 138, 'DEBUG', 'Closing file input stream', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51555, 'OrderSubCatView.java', 161, 'DEBUG', 'Setting component caption', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51556, 'OrderSubCatView.java', 167, 'DEBUG', 'Setting fields validator', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51557, 'OrderSubCatView.java', 184, 'DEBUG', 'Setting fields value change listener', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51558, 'OrderSubCatView.java', 195, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51559, 'OrderSubCatView.java', 217, 'DEBUG', 'Setting click listener for save button', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51560, 'Logs.java', 71, 'DEBUG', 'MF18_M_ORDSUBCMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51561, 'OrderSubCatView.java', 222, 'DEBUG', 'Getting all order sub category records', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51562, 'OrderSubCatView.java', 228, 'DEBUG', 'Wrapping order sub category records to container that can be understood by Grid', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51563, 'OrderSubCatView.java', 243, 'DEBUG', 'Hiding unwanted attributes', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51564, 'OrderSubCatView.java', 250, 'DEBUG', 'Filling grid with data', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51565, 'OrderSubCatView.java', 281, 'DEBUG', 'Adding "edit" column', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51566, 'OrderSubCatView.java', 731, 'DEBUG', 'Inside Order Sub Category View', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51567, 'Logs.java', 71, 'DEBUG', 'MF17_M_ORDCATMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51568, 'Logs.java', 59, 'DEBUG', 'LD04_T_AUDIT record is successfully saved into the database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51569, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51570, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51571, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51572, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51573, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51574, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51575, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51576, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51577, 'CompanyView.java', 200, 'DEBUG', 'Initializing screen components behaviour', NULL, 'PO-AD11L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51578, 'CompanyView.java', 244, 'DEBUG', 'Closing file input stream', NULL, 'PO-AD11L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51579, 'CompanyView.java', 274, 'DEBUG', 'Setting component caption', NULL, 'PO-AD11L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51580, 'CompanyView.java', 283, 'DEBUG', 'Setting fields validator', NULL, 'PO-AD11L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51581, 'CompanyView.java', 647, 'DEBUG', 'Setting fields value change listener', NULL, 'PO-AD11L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51582, 'CompanyView.java', 663, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-AD11L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51583, 'CompanyView.java', 714, 'DEBUG', 'Setting click listener for save button', NULL, 'PO-AD11L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51584, 'Logs.java', 71, 'DEBUG', 'MF11_M_COMPMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51585, 'CompanyView.java', 1584, 'DEBUG', 'Entering Company Screen', NULL, 'PO-AD11L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51586, 'Logs.java', 71, 'DEBUG', 'MF11_M_COMPMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51587, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51588, 'Logs.java', 71, 'DEBUG', 'MF11_M_COMPMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51589, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51590, 'Logs.java', 71, 'DEBUG', 'MF11_M_COMPMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51591, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51592, 'Logs.java', 71, 'DEBUG', 'MF11_M_COMPMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51593, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51594, 'Logs.java', 71, 'DEBUG', 'MF11_M_COMPMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51595, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51596, 'Logs.java', 71, 'DEBUG', 'MF11_M_COMPMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51597, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51598, 'Logs.java', 71, 'DEBUG', 'MF11_M_COMPMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51599, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51600, 'Logs.java', 71, 'DEBUG', 'MF11_M_COMPMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51601, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51602, 'Logs.java', 71, 'DEBUG', 'MF11_M_COMPMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51603, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51604, 'Logs.java', 71, 'DEBUG', 'MF11_M_COMPMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51605, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51606, 'Logs.java', 71, 'DEBUG', 'MF11_M_COMPMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51607, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51608, 'Logs.java', 71, 'DEBUG', 'MF11_M_COMPMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51609, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51610, 'Logs.java', 71, 'DEBUG', 'MF11_M_COMPMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51611, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51612, 'Logs.java', 71, 'DEBUG', 'MF11_M_COMPMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51613, 'CompanyView.java', 834, 'DEBUG', 'Setting values to be stored into database', NULL, 'PO-AD11L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51614, 'CompanyView.java', 890, 'DEBUG', 'Setting file related values to be stored into database', NULL, 'PO-AD11L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51615, 'CompanyView.java', 936, 'DEBUG', 'Setting audit data', NULL, 'PO-AD11L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51616, 'Logs.java', 59, 'DEBUG', 'MF11_M_COMPMAST record is successfully saved into the database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51617, 'CompanyView.java', 942, 'DEBUG', 'Saving record in database', NULL, 'PO-AD11L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51618, 'Logs.java', 71, 'DEBUG', 'MF11_M_COMPMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51619, 'CompanyView.java', 954, 'DEBUG', 'Set screen to initial state after saving', NULL, 'PO-AD11L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51620, 'Logs.java', 59, 'DEBUG', 'LD04_T_AUDIT record is successfully saved into the database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51621, 'Logs.java', 71, 'DEBUG', 'LD04_T_AUDIT records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51622, 'auditTrailView.java', 88, 'DEBUG', 'Closing file input stream', NULL, 'PO-UT01L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51623, 'Logs.java', 71, 'DEBUG', 'LD04_T_AUDIT records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51624, 'auditTrailView.java', 104, 'DEBUG', 'Getting all audit records', NULL, 'PO-UT01L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51625, 'LogView.java', 176, 'DEBUG', 'Getting all log records', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51626, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51627, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51628, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51629, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51630, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51631, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51632, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51633, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51634, 'StoreView.java', 149, 'DEBUG', 'Initializing screen components behaviour', NULL, 'PO-AD02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51635, 'StoreView.java', 189, 'DEBUG', 'Closing file input stream', NULL, 'PO-AD02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51636, 'StoreView.java', 215, 'DEBUG', 'Setting component caption', NULL, 'PO-AD02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51637, 'StoreView.java', 221, 'DEBUG', 'Setting component styles', NULL, 'PO-AD02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51638, 'StoreView.java', 228, 'DEBUG', 'Setting fields validator', NULL, 'PO-AD02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51639, 'StoreView.java', 239, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-AD02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51640, 'StoreView.java', 251, 'DEBUG', 'Setting state of combobox', NULL, 'PO-AD02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51641, 'StoreView.java', 258, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-AD02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51642, 'StoreView.java', 284, 'DEBUG', 'Setting click listener for save button', NULL, 'PO-AD02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51643, 'Logs.java', 71, 'DEBUG', 'MF02_M_STRMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51644, 'StoreView.java', 292, 'DEBUG', 'Getting all store records', NULL, 'PO-AD02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51645, 'StoreView.java', 957, 'DEBUG', 'Entering Store Screen', NULL, 'PO-AD02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51646, 'Logs.java', 71, 'DEBUG', 'MF11_M_COMPMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51647, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51648, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51649, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51650, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51651, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51652, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51653, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51654, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51655, 'JobMasterView.java', 98, 'DEBUG', 'Initializing screen components behaviour', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51656, 'JobMasterView.java', 132, 'DEBUG', 'Closing file input stream', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51657, 'JobMasterView.java', 154, 'DEBUG', 'Setting component caption', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51658, 'JobMasterView.java', 159, 'DEBUG', 'Setting fields validator', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51659, 'JobMasterView.java', 170, 'DEBUG', 'Setting fields value change listener', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51660, 'JobMasterView.java', 175, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51661, 'JobMasterView.java', 198, 'DEBUG', 'Setting click listener for save button', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51662, 'Logs.java', 71, 'DEBUG', 'MF19_M_JOBMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51663, 'JobMasterView.java', 203, 'DEBUG', 'Getting all job master records', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51664, 'JobMasterView.java', 211, 'DEBUG', 'Wrapping Job Master record to container that can be understood by Grid', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51665, 'JobMasterView.java', 226, 'DEBUG', 'Hiding unwanted attributes', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51666, 'JobMasterView.java', 233, 'DEBUG', 'Filling grid with data', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51667, 'JobMasterView.java', 265, 'DEBUG', 'Adding "edit" column', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51668, 'JobMasterView.java', 699, 'DEBUG', 'Entering Company Screen', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51669, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51670, 'Logs.java', 71, 'DEBUG', 'MF19_M_JOBMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51671, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51672, 'Logs.java', 71, 'DEBUG', 'MF19_M_JOBMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51673, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51674, 'Logs.java', 71, 'DEBUG', 'MF19_M_JOBMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51675, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51676, 'Logs.java', 71, 'DEBUG', 'MF19_M_JOBMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51677, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51678, 'Logs.java', 71, 'DEBUG', 'MF19_M_JOBMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51679, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51680, 'Logs.java', 71, 'DEBUG', 'MF19_M_JOBMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51681, 'Logs.java', 59, 'DEBUG', 'LD04_T_AUDIT record is successfully saved into the database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51682, 'JobMasterView.java', 499, 'DEBUG', 'Setting values to be stored into database', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51683, 'JobMasterView.java', 536, 'DEBUG', 'Setting audit data', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51684, 'Logs.java', 59, 'DEBUG', 'MF19_M_JOBMAST record is successfully saved into the database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51685, 'JobMasterView.java', 540, 'DEBUG', 'Saving record in database', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51686, 'Logs.java', 71, 'DEBUG', 'MF19_M_JOBMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51687, 'JobMasterView.java', 550, 'DEBUG', 'Set screen to initial state after saving', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51688, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51689, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51690, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51691, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51692, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51693, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51694, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51695, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51696, 'MasterDataView.java', 105, 'DEBUG', 'Initializing screen components behaviour', NULL, 'PO-AD06L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51697, 'MasterDataView.java', 142, 'DEBUG', 'Closing file input stream', NULL, 'PO-AD06L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51698, 'MasterDataView.java', 165, 'DEBUG', 'Setting component caption', NULL, 'PO-AD06L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51699, 'MasterDataView.java', 183, 'DEBUG', 'Setting fields value change listener', NULL, 'PO-AD06L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51700, 'MasterDataView.java', 187, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-AD06L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51701, 'MasterDataView.java', 212, 'DEBUG', 'Setting click listener for save button', NULL, 'PO-AD06L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51702, 'Logs.java', 71, 'DEBUG', 'MF06_R_MASTDATA records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51703, 'MasterDataView.java', 224, 'DEBUG', 'Getting all master data records', NULL, 'PO-AD06L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51704, 'MasterDataView.java', 232, 'DEBUG', 'Wrapping Job Master record to container that can be understood by Grid', NULL, 'PO-AD06L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51705, 'MasterDataView.java', 249, 'DEBUG', 'Hiding unwanted attributes', NULL, 'PO-AD06L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51706, 'MasterDataView.java', 256, 'DEBUG', 'Filling grid with data', NULL, 'PO-AD06L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51707, 'MasterDataView.java', 288, 'DEBUG', 'Adding "edit" column', NULL, 'PO-AD06L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51708, 'MasterDataView.java', 749, 'DEBUG', 'Entering Master Data Screen', NULL, 'PO-AD06L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51709, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51710, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51711, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51712, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51713, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51714, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51715, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51716, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51717, 'SchedulerView.java', 137, 'DEBUG', 'Initializing screen components behaviour', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51718, 'SchedulerView.java', 171, 'DEBUG', 'Closing file input stream', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51719, 'SchedulerView.java', 207, 'DEBUG', 'Closing file input stream', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51720, 'SchedulerView.java', 227, 'DEBUG', 'Setting component caption', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51721, 'SchedulerView.java', 647, 'DEBUG', 'Setting click listener for cancel button', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51722, 'SchedulerView.java', 650, 'DEBUG', 'Setting click listener for save button', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51723, 'Logs.java', 71, 'DEBUG', 'MF22_M_SCHEDULER records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51724, 'SchedulerView.java', 656, 'DEBUG', 'Getting all Order Category records', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51725, 'SchedulerView.java', 665, 'DEBUG', 'Wrapping Order Category record to container that can be understood by Grid', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51726, 'SchedulerView.java', 689, 'DEBUG', 'Hiding unwanted attributes', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51727, 'SchedulerView.java', 695, 'DEBUG', 'Filling grid with data', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51728, 'SchedulerView.java', 717, 'DEBUG', 'Adding "edit" column', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51729, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51730, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51731, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51732, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51733, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51734, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51735, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51736, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51737, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51738, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51739, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51740, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51741, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51742, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51743, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51744, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51745, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51746, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51747, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51748, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51749, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51750, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51751, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51752, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51753, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51754, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51755, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51756, 'MainScreenView.java', 90, 'DEBUG', 'Building menu content for superadmin user', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51757, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51758, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51759, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51760, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51761, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51762, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51763, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51764, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51765, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51766, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51767, 'AdminView.java', 150, 'DEBUG', 'Initializing screen components behaviour', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51768, 'AdminView.java', 169, 'DEBUG', 'ERROR: SYSPARAM: User validity Period is missing', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51769, 'AdminView.java', 226, 'DEBUG', 'Setting component caption', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51770, 'AdminView.java', 299, 'DEBUG', 'Setting fields value change listener', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51771, 'AdminView.java', 318, 'DEBUG', 'Setting fields validator', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51772, 'AdminView.java', 325, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51773, 'AdminView.java', 350, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51774, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51775, 'AdminView.java', 355, 'DEBUG', 'Getting all admin records', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51776, 'AdminView.java', 366, 'DEBUG', 'Binding data to paged container', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51777, 'AdminView.java', 380, 'DEBUG', 'Hiding unwanted attributes', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51778, 'AdminView.java', 387, 'DEBUG', 'Setting data source of grid', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51779, 'AdminView.java', 415, 'DEBUG', 'Setting initial state of grid', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51780, 'AdminView.java', 434, 'DEBUG', 'Adding "edit" column', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51781, 'MainScreenView.java', 194, 'DEBUG', 'Building header', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51782, 'MainScreenView.java', 201, 'DEBUG', 'Setting logo in header', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51783, 'MainScreenView.java', 234, 'DEBUG', 'Setting menu for screen setting', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51784, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51785, 'MainScreenView.java', 274, 'DEBUG', 'Setting menu for user profile setting', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51786, 'MainScreenView.java', 339, 'DEBUG', 'JavaScript function for closing popup when page scrolls', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51787, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51788, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51789, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51790, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51791, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51792, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52123, 'AdminView.java', 380, 'DEBUG', 'Hiding unwanted attributes', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51793, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51794, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51795, 'SchedulerView.java', 137, 'DEBUG', 'Initializing screen components behaviour', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51796, 'SchedulerView.java', 171, 'DEBUG', 'Closing file input stream', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51797, 'SchedulerView.java', 207, 'DEBUG', 'Closing file input stream', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51798, 'SchedulerView.java', 227, 'DEBUG', 'Setting component caption', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51799, 'SchedulerView.java', 647, 'DEBUG', 'Setting click listener for cancel button', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51800, 'SchedulerView.java', 650, 'DEBUG', 'Setting click listener for save button', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51801, 'Logs.java', 71, 'DEBUG', 'MF22_M_SCHEDULER records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51802, 'SchedulerView.java', 656, 'DEBUG', 'Getting all Order Category records', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51803, 'SchedulerView.java', 665, 'DEBUG', 'Wrapping Order Category record to container that can be understood by Grid', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51804, 'SchedulerView.java', 689, 'DEBUG', 'Hiding unwanted attributes', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51805, 'SchedulerView.java', 695, 'DEBUG', 'Filling grid with data', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51806, 'SchedulerView.java', 717, 'DEBUG', 'Adding "edit" column', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51807, 'Logs.java', 59, 'DEBUG', 'LD04_T_AUDIT record is successfully saved into the database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51808, 'Logs.java', 59, 'DEBUG', 'MF22_M_SCHEDULER record is successfully saved into the database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51809, 'Logs.java', 71, 'DEBUG', 'MF22_M_SCHEDULER records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51810, 'Logs.java', 59, 'DEBUG', 'MF22_M_SCHEDULER record is successfully saved into the database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51811, 'Logs.java', 71, 'DEBUG', 'MF22_M_SCHEDULER records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51812, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51813, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51814, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51815, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51816, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51817, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51818, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51819, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51820, 'GSTView.java', 138, 'DEBUG', 'Inside GST View', NULL, 'PO-AD16L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51821, 'GSTView.java', 164, 'DEBUG', 'Initializing screen components behaviour', NULL, 'PO-AD16L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51822, 'GSTView.java', 234, 'DEBUG', 'Setting component caption', NULL, 'PO-AD16L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51823, 'Logs.java', 71, 'DEBUG', 'MF16_M_GSTMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51824, 'GSTView.java', 239, 'DEBUG', 'Getting all GST records', NULL, 'PO-AD16L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51825, 'GSTView.java', 325, 'DEBUG', 'Setting fields validator', NULL, 'PO-AD16L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51826, 'GSTView.java', 394, 'DEBUG', 'Setting fields value change listener', NULL, 'PO-AD16L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51827, 'GSTView.java', 401, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-AD16L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51828, 'GSTView.java', 426, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-AD16L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51829, 'GSTView.java', 1083, 'DEBUG', 'Entering GST Screen', NULL, 'PO-AD16L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51830, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51831, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51832, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51833, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51834, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51835, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51836, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51837, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51838, 'MasterDataView.java', 105, 'DEBUG', 'Initializing screen components behaviour', NULL, 'PO-AD06L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51839, 'MasterDataView.java', 142, 'DEBUG', 'Closing file input stream', NULL, 'PO-AD06L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51840, 'MasterDataView.java', 165, 'DEBUG', 'Setting component caption', NULL, 'PO-AD06L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51841, 'MasterDataView.java', 183, 'DEBUG', 'Setting fields value change listener', NULL, 'PO-AD06L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51842, 'MasterDataView.java', 187, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-AD06L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51843, 'MasterDataView.java', 212, 'DEBUG', 'Setting click listener for save button', NULL, 'PO-AD06L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51844, 'Logs.java', 71, 'DEBUG', 'MF06_R_MASTDATA records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51845, 'MasterDataView.java', 224, 'DEBUG', 'Getting all master data records', NULL, 'PO-AD06L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51846, 'MasterDataView.java', 232, 'DEBUG', 'Wrapping Job Master record to container that can be understood by Grid', NULL, 'PO-AD06L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51847, 'MasterDataView.java', 249, 'DEBUG', 'Hiding unwanted attributes', NULL, 'PO-AD06L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51848, 'MasterDataView.java', 256, 'DEBUG', 'Filling grid with data', NULL, 'PO-AD06L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51849, 'MasterDataView.java', 288, 'DEBUG', 'Adding "edit" column', NULL, 'PO-AD06L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51850, 'MasterDataView.java', 749, 'DEBUG', 'Entering Master Data Screen', NULL, 'PO-AD06L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51851, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51852, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51853, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51854, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51855, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51856, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51857, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.No entity found for query', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51858, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51859, 'CompanyView.java', 200, 'DEBUG', 'Initializing screen components behaviour', NULL, 'PO-AD11L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51860, 'CompanyView.java', 244, 'DEBUG', 'Closing file input stream', NULL, 'PO-AD11L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51861, 'CompanyView.java', 274, 'DEBUG', 'Setting component caption', NULL, 'PO-AD11L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51862, 'CompanyView.java', 283, 'DEBUG', 'Setting fields validator', NULL, 'PO-AD11L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51863, 'CompanyView.java', 647, 'DEBUG', 'Setting fields value change listener', NULL, 'PO-AD11L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51864, 'CompanyView.java', 663, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-AD11L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51865, 'CompanyView.java', 714, 'DEBUG', 'Setting click listener for save button', NULL, 'PO-AD11L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51866, 'Logs.java', 71, 'DEBUG', 'MF11_M_COMPMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51867, 'CompanyView.java', 1584, 'DEBUG', 'Entering Company Screen', NULL, 'PO-AD11L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51868, 'Logs.java', 71, 'DEBUG', 'MF11_M_COMPMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51869, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51870, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51871, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51872, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51873, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51874, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51875, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51876, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51877, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51878, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51879, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51880, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51881, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51882, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51883, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51884, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51885, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51886, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51887, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51888, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51889, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51890, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51891, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51892, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51893, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51894, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51895, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51896, 'MainScreenView.java', 90, 'DEBUG', 'Building menu content for superadmin user', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51897, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51898, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51899, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51900, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51901, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51902, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51903, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51904, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51905, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51906, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51907, 'AdminView.java', 150, 'DEBUG', 'Initializing screen components behaviour', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51908, 'AdminView.java', 169, 'DEBUG', 'ERROR: SYSPARAM: User validity Period is missing', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51909, 'AdminView.java', 226, 'DEBUG', 'Setting component caption', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51910, 'AdminView.java', 299, 'DEBUG', 'Setting fields value change listener', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51911, 'AdminView.java', 318, 'DEBUG', 'Setting fields validator', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51912, 'AdminView.java', 325, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51913, 'AdminView.java', 350, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51914, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51915, 'AdminView.java', 355, 'DEBUG', 'Getting all admin records', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51916, 'AdminView.java', 366, 'DEBUG', 'Binding data to paged container', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51917, 'AdminView.java', 380, 'DEBUG', 'Hiding unwanted attributes', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51918, 'AdminView.java', 387, 'DEBUG', 'Setting data source of grid', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51919, 'AdminView.java', 415, 'DEBUG', 'Setting initial state of grid', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51920, 'AdminView.java', 434, 'DEBUG', 'Adding "edit" column', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51921, 'MainScreenView.java', 194, 'DEBUG', 'Building header', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51922, 'MainScreenView.java', 201, 'DEBUG', 'Setting logo in header', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51923, 'MainScreenView.java', 234, 'DEBUG', 'Setting menu for screen setting', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51924, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51925, 'MainScreenView.java', 274, 'DEBUG', 'Setting menu for user profile setting', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51926, 'MainScreenView.java', 339, 'DEBUG', 'JavaScript function for closing popup when page scrolls', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51927, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51928, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51929, 'AdminView.java', 464, 'DEBUG', 'Getting all admin records', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51930, 'AdminView.java', 469, 'DEBUG', 'Binding data to paged container', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51931, 'AdminView.java', 890, 'DEBUG', 'Entering Company Screen', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51932, 'Logs.java', 59, 'DEBUG', 'LD04_T_AUDIT record is successfully saved into the database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51933, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51934, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51935, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51936, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51937, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51938, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51939, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51940, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51941, 'SchedulerView.java', 137, 'DEBUG', 'Initializing screen components behaviour', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51942, 'SchedulerView.java', 171, 'DEBUG', 'Closing file input stream', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51943, 'SchedulerView.java', 207, 'DEBUG', 'Closing file input stream', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51944, 'SchedulerView.java', 227, 'DEBUG', 'Setting component caption', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51945, 'SchedulerView.java', 647, 'DEBUG', 'Setting click listener for cancel button', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51946, 'SchedulerView.java', 650, 'DEBUG', 'Setting click listener for save button', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51947, 'Logs.java', 71, 'DEBUG', 'MF22_M_SCHEDULER records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51948, 'SchedulerView.java', 656, 'DEBUG', 'Getting all Order Category records', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51949, 'SchedulerView.java', 665, 'DEBUG', 'Wrapping Order Category record to container that can be understood by Grid', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51950, 'SchedulerView.java', 689, 'DEBUG', 'Hiding unwanted attributes', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51951, 'SchedulerView.java', 695, 'DEBUG', 'Filling grid with data', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51952, 'SchedulerView.java', 717, 'DEBUG', 'Adding "edit" column', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51953, 'Logs.java', 59, 'DEBUG', 'MF22_M_SCHEDULER record is successfully saved into the database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51954, 'Logs.java', 71, 'DEBUG', 'MF22_M_SCHEDULER records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51955, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51956, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51957, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51958, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51959, 'BU.java', 85, 'DEBUG', 'Closing file input stream', NULL, 'com.parkson.poAdmin.backend.csv.BU');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51960, 'MasterFileUpload.java', 137, 'ERROR', 'unable to create log file', NULL, 'com.parkson.poAdmin.backend.csv.MasterFileUpload');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51961, 'DepartmentMaster.java', 308, 'DEBUG', 'Failed updating data', NULL, 'PO-AD14L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51962, 'DepartmentMaster.java', 308, 'DEBUG', 'Failed updating data', NULL, 'PO-AD14L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51963, 'DepartmentMaster.java', 308, 'DEBUG', 'Failed updating data', NULL, 'PO-AD14L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51964, 'DepartmentMaster.java', 308, 'DEBUG', 'Failed updating data', NULL, 'PO-AD14L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51965, 'DepartmentMaster.java', 308, 'DEBUG', 'Failed updating data', NULL, 'PO-AD14L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51966, 'DepartmentMaster.java', 308, 'DEBUG', 'Failed updating data', NULL, 'PO-AD14L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51967, 'DepartmentMaster.java', 308, 'DEBUG', 'Failed updating data', NULL, 'PO-AD14L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51968, 'BU.java', 116, 'ERROR', 'The Master File NTPO_AD1705051818_080.TXT Failed to be uploaded. All rows failed. ', NULL, 'com.parkson.poAdmin.backend.csv.BU');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51969, 'BU.java', 116, 'ERROR', 'The Master File NTPO_AD1706051818_080.TXT Failed to be uploaded. All rows failed. ', NULL, 'com.parkson.poAdmin.backend.csv.BU');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51970, 'BU.java', 133, 'ERROR', 'Failed connecting to database', NULL, 'com.parkson.poAdmin.backend.csv.BU');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51971, 'BU.java', 143, 'ERROR', 'Failed Dropping Database.', NULL, 'com.parkson.poAdmin.backend.csv.BU');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51972, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51973, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51974, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51975, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51976, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51977, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51978, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51979, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51980, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51981, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51982, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51983, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51984, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51985, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51986, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51987, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51988, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51989, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51990, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51991, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51992, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51993, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51994, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51995, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51996, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51997, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51998, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (51999, 'MainScreenView.java', 90, 'DEBUG', 'Building menu content for superadmin user', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52000, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52001, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52002, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52003, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52004, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52005, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52006, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52007, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52008, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52009, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52010, 'AdminView.java', 150, 'DEBUG', 'Initializing screen components behaviour', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52011, 'AdminView.java', 169, 'DEBUG', 'ERROR: SYSPARAM: User validity Period is missing', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52012, 'AdminView.java', 226, 'DEBUG', 'Setting component caption', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52013, 'AdminView.java', 299, 'DEBUG', 'Setting fields value change listener', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52014, 'AdminView.java', 318, 'DEBUG', 'Setting fields validator', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52015, 'AdminView.java', 325, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52016, 'AdminView.java', 350, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52017, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52018, 'AdminView.java', 355, 'DEBUG', 'Getting all admin records', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52019, 'AdminView.java', 366, 'DEBUG', 'Binding data to paged container', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52020, 'AdminView.java', 380, 'DEBUG', 'Hiding unwanted attributes', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52021, 'AdminView.java', 387, 'DEBUG', 'Setting data source of grid', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52022, 'AdminView.java', 415, 'DEBUG', 'Setting initial state of grid', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52023, 'AdminView.java', 434, 'DEBUG', 'Adding "edit" column', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52024, 'MainScreenView.java', 194, 'DEBUG', 'Building header', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52025, 'MainScreenView.java', 201, 'DEBUG', 'Setting logo in header', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52026, 'MainScreenView.java', 234, 'DEBUG', 'Setting menu for screen setting', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52027, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52028, 'MainScreenView.java', 274, 'DEBUG', 'Setting menu for user profile setting', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52029, 'MainScreenView.java', 339, 'DEBUG', 'JavaScript function for closing popup when page scrolls', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52030, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52031, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52032, 'AdminView.java', 464, 'DEBUG', 'Getting all admin records', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52033, 'AdminView.java', 469, 'DEBUG', 'Binding data to paged container', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52034, 'AdminView.java', 890, 'DEBUG', 'Entering Company Screen', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52035, 'Logs.java', 59, 'DEBUG', 'LD04_T_AUDIT record is successfully saved into the database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52036, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52037, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52038, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52039, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52040, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52041, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52042, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52043, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52044, 'SchedulerView.java', 137, 'DEBUG', 'Initializing screen components behaviour', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52045, 'SchedulerView.java', 171, 'DEBUG', 'Closing file input stream', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52046, 'SchedulerView.java', 207, 'DEBUG', 'Closing file input stream', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52047, 'SchedulerView.java', 227, 'DEBUG', 'Setting component caption', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52048, 'SchedulerView.java', 647, 'DEBUG', 'Setting click listener for cancel button', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52049, 'SchedulerView.java', 650, 'DEBUG', 'Setting click listener for save button', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52050, 'Logs.java', 71, 'DEBUG', 'MF22_M_SCHEDULER records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52051, 'SchedulerView.java', 656, 'DEBUG', 'Getting all Order Category records', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52052, 'SchedulerView.java', 665, 'DEBUG', 'Wrapping Order Category record to container that can be understood by Grid', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52053, 'SchedulerView.java', 689, 'DEBUG', 'Hiding unwanted attributes', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52054, 'SchedulerView.java', 695, 'DEBUG', 'Filling grid with data', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52055, 'SchedulerView.java', 717, 'DEBUG', 'Adding "edit" column', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52056, 'Logs.java', 59, 'DEBUG', 'MF22_M_SCHEDULER record is successfully saved into the database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52057, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52058, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52059, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52060, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52061, 'BU.java', 85, 'DEBUG', 'Closing file input stream', NULL, 'com.parkson.poAdmin.backend.csv.BU');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52062, 'MasterFileUpload.java', 137, 'ERROR', 'unable to create log file', NULL, 'com.parkson.poAdmin.backend.csv.MasterFileUpload');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52063, 'Logs.java', 71, 'DEBUG', 'MF22_M_SCHEDULER records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52064, 'DepartmentMaster.java', 309, 'DEBUG', 'Failed updating data', NULL, 'PO-AD14L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52065, 'DepartmentMaster.java', 309, 'DEBUG', 'Failed updating data', NULL, 'PO-AD14L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52066, 'DepartmentMaster.java', 309, 'DEBUG', 'Failed updating data', NULL, 'PO-AD14L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52067, 'DepartmentMaster.java', 309, 'DEBUG', 'Failed updating data', NULL, 'PO-AD14L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52068, 'DepartmentMaster.java', 309, 'DEBUG', 'Failed updating data', NULL, 'PO-AD14L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52069, 'DepartmentMaster.java', 309, 'DEBUG', 'Failed updating data', NULL, 'PO-AD14L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52070, 'DepartmentMaster.java', 309, 'DEBUG', 'Failed updating data', NULL, 'PO-AD14L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52071, 'BU.java', 116, 'ERROR', 'The Master File NTPO_AD1705051818_080.TXT Failed to be uploaded. All rows failed. ', NULL, 'com.parkson.poAdmin.backend.csv.BU');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52072, 'BU.java', 116, 'ERROR', 'The Master File NTPO_AD1706051818_080.TXT Failed to be uploaded. All rows failed. ', NULL, 'com.parkson.poAdmin.backend.csv.BU');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52073, 'BU.java', 133, 'ERROR', 'Failed connecting to database', NULL, 'com.parkson.poAdmin.backend.csv.BU');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52074, 'BU.java', 143, 'ERROR', 'Failed Dropping Database.', NULL, 'com.parkson.poAdmin.backend.csv.BU');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52075, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52076, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52077, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52078, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52079, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52080, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52081, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52082, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52083, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52084, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52085, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52086, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52087, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52088, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52089, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52090, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52091, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52092, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52093, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52094, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52095, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52096, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52097, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52098, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52099, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52100, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52101, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52102, 'MainScreenView.java', 90, 'DEBUG', 'Building menu content for superadmin user', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52103, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52104, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52105, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52106, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52107, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52108, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52109, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52110, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52111, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52112, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52113, 'AdminView.java', 150, 'DEBUG', 'Initializing screen components behaviour', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52114, 'AdminView.java', 169, 'DEBUG', 'ERROR: SYSPARAM: User validity Period is missing', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52115, 'AdminView.java', 226, 'DEBUG', 'Setting component caption', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52116, 'AdminView.java', 299, 'DEBUG', 'Setting fields value change listener', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52117, 'AdminView.java', 318, 'DEBUG', 'Setting fields validator', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52118, 'AdminView.java', 325, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52119, 'AdminView.java', 350, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52120, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52121, 'AdminView.java', 355, 'DEBUG', 'Getting all admin records', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52124, 'AdminView.java', 387, 'DEBUG', 'Setting data source of grid', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52125, 'AdminView.java', 415, 'DEBUG', 'Setting initial state of grid', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52126, 'AdminView.java', 434, 'DEBUG', 'Adding "edit" column', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52127, 'MainScreenView.java', 194, 'DEBUG', 'Building header', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52128, 'MainScreenView.java', 201, 'DEBUG', 'Setting logo in header', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52129, 'MainScreenView.java', 234, 'DEBUG', 'Setting menu for screen setting', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52130, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52131, 'MainScreenView.java', 274, 'DEBUG', 'Setting menu for user profile setting', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52132, 'MainScreenView.java', 339, 'DEBUG', 'JavaScript function for closing popup when page scrolls', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52133, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52134, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52135, 'AdminView.java', 464, 'DEBUG', 'Getting all admin records', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52136, 'AdminView.java', 469, 'DEBUG', 'Binding data to paged container', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52137, 'AdminView.java', 890, 'DEBUG', 'Entering Company Screen', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52138, 'Logs.java', 59, 'DEBUG', 'LD04_T_AUDIT record is successfully saved into the database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52139, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52140, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52141, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52142, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52143, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52144, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52145, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52146, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52147, 'SchedulerView.java', 137, 'DEBUG', 'Initializing screen components behaviour', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52148, 'SchedulerView.java', 171, 'DEBUG', 'Closing file input stream', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52149, 'SchedulerView.java', 207, 'DEBUG', 'Closing file input stream', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52150, 'SchedulerView.java', 227, 'DEBUG', 'Setting component caption', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52151, 'SchedulerView.java', 647, 'DEBUG', 'Setting click listener for cancel button', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52152, 'SchedulerView.java', 650, 'DEBUG', 'Setting click listener for save button', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52153, 'Logs.java', 71, 'DEBUG', 'MF22_M_SCHEDULER records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52154, 'SchedulerView.java', 656, 'DEBUG', 'Getting all Order Category records', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52155, 'SchedulerView.java', 665, 'DEBUG', 'Wrapping Order Category record to container that can be understood by Grid', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52156, 'SchedulerView.java', 689, 'DEBUG', 'Hiding unwanted attributes', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52157, 'SchedulerView.java', 695, 'DEBUG', 'Filling grid with data', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52158, 'SchedulerView.java', 717, 'DEBUG', 'Adding "edit" column', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52159, 'Logs.java', 59, 'DEBUG', 'MF22_M_SCHEDULER record is successfully saved into the database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52160, 'Logs.java', 71, 'DEBUG', 'MF22_M_SCHEDULER records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52161, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52162, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52163, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52164, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52165, 'BU.java', 85, 'DEBUG', 'Closing file input stream', NULL, 'com.parkson.poAdmin.backend.csv.BU');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52166, 'MasterFileUpload.java', 137, 'ERROR', 'unable to create log file', NULL, 'com.parkson.poAdmin.backend.csv.MasterFileUpload');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52167, 'BU.java', 116, 'ERROR', 'The Master File NTPO_AD1705051818_080.TXT uploaded partially. ', NULL, 'com.parkson.poAdmin.backend.csv.BU');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52168, 'BU.java', 116, 'ERROR', 'The Master File NTPO_AD1706051818_080.TXT uploaded successfully. ', NULL, 'com.parkson.poAdmin.backend.csv.BU');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52169, 'BU.java', 143, 'ERROR', 'Failed Dropping Database.', NULL, 'com.parkson.poAdmin.backend.csv.BU');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52170, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52171, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52172, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52173, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52174, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52175, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52176, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52177, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52178, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52179, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52180, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52181, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52182, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52183, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52184, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52185, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52186, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52187, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52188, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52189, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52190, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52191, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52192, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52193, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52194, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52195, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52196, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52197, 'MainScreenView.java', 90, 'DEBUG', 'Building menu content for superadmin user', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52198, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52199, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52200, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52201, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52202, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52203, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52204, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52205, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52206, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52207, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52208, 'AdminView.java', 150, 'DEBUG', 'Initializing screen components behaviour', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52209, 'AdminView.java', 169, 'DEBUG', 'ERROR: SYSPARAM: User validity Period is missing', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52210, 'AdminView.java', 226, 'DEBUG', 'Setting component caption', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52211, 'AdminView.java', 299, 'DEBUG', 'Setting fields value change listener', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52212, 'AdminView.java', 318, 'DEBUG', 'Setting fields validator', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52213, 'AdminView.java', 325, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52214, 'AdminView.java', 350, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52215, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52216, 'AdminView.java', 355, 'DEBUG', 'Getting all admin records', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52217, 'AdminView.java', 366, 'DEBUG', 'Binding data to paged container', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52218, 'AdminView.java', 380, 'DEBUG', 'Hiding unwanted attributes', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52219, 'AdminView.java', 387, 'DEBUG', 'Setting data source of grid', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52220, 'AdminView.java', 415, 'DEBUG', 'Setting initial state of grid', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52221, 'AdminView.java', 434, 'DEBUG', 'Adding "edit" column', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52222, 'MainScreenView.java', 194, 'DEBUG', 'Building header', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52223, 'MainScreenView.java', 201, 'DEBUG', 'Setting logo in header', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52224, 'MainScreenView.java', 234, 'DEBUG', 'Setting menu for screen setting', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52225, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52226, 'MainScreenView.java', 274, 'DEBUG', 'Setting menu for user profile setting', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52227, 'MainScreenView.java', 339, 'DEBUG', 'JavaScript function for closing popup when page scrolls', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52228, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52229, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52230, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52231, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52232, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52233, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52234, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52235, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52236, 'SchedulerView.java', 137, 'DEBUG', 'Initializing screen components behaviour', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52237, 'SchedulerView.java', 171, 'DEBUG', 'Closing file input stream', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52238, 'SchedulerView.java', 207, 'DEBUG', 'Closing file input stream', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52239, 'SchedulerView.java', 227, 'DEBUG', 'Setting component caption', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52240, 'SchedulerView.java', 647, 'DEBUG', 'Setting click listener for cancel button', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52241, 'SchedulerView.java', 650, 'DEBUG', 'Setting click listener for save button', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52242, 'Logs.java', 71, 'DEBUG', 'MF22_M_SCHEDULER records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52243, 'SchedulerView.java', 656, 'DEBUG', 'Getting all Order Category records', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52244, 'SchedulerView.java', 665, 'DEBUG', 'Wrapping Order Category record to container that can be understood by Grid', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52245, 'SchedulerView.java', 689, 'DEBUG', 'Hiding unwanted attributes', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52246, 'SchedulerView.java', 695, 'DEBUG', 'Filling grid with data', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52247, 'SchedulerView.java', 717, 'DEBUG', 'Adding "edit" column', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52248, 'Logs.java', 59, 'DEBUG', 'LD04_T_AUDIT record is successfully saved into the database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52249, 'SchedulerView.java', 137, 'DEBUG', 'Initializing screen components behaviour', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52250, 'SchedulerView.java', 171, 'DEBUG', 'Closing file input stream', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52251, 'SchedulerView.java', 207, 'DEBUG', 'Closing file input stream', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52252, 'SchedulerView.java', 227, 'DEBUG', 'Setting component caption', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52511, 'AdminView.java', 469, 'DEBUG', 'Binding data to paged container', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52253, 'SchedulerView.java', 647, 'DEBUG', 'Setting click listener for cancel button', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52254, 'SchedulerView.java', 650, 'DEBUG', 'Setting click listener for save button', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52255, 'Logs.java', 71, 'DEBUG', 'MF22_M_SCHEDULER records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52256, 'SchedulerView.java', 656, 'DEBUG', 'Getting all Order Category records', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52257, 'SchedulerView.java', 665, 'DEBUG', 'Wrapping Order Category record to container that can be understood by Grid', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52258, 'SchedulerView.java', 689, 'DEBUG', 'Hiding unwanted attributes', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52259, 'SchedulerView.java', 695, 'DEBUG', 'Filling grid with data', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52260, 'SchedulerView.java', 717, 'DEBUG', 'Adding "edit" column', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52261, 'Logs.java', 59, 'DEBUG', 'MF22_M_SCHEDULER record is successfully saved into the database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52262, 'Logs.java', 71, 'DEBUG', 'MF22_M_SCHEDULER records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52263, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52264, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52265, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52266, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52267, 'BU.java', 85, 'DEBUG', 'Closing file input stream', NULL, 'com.parkson.poAdmin.backend.csv.BU');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52268, 'BU.java', 116, 'ERROR', 'The Master File NTPO_AD1705051818_080.TXT uploaded partially. ', NULL, 'com.parkson.poAdmin.backend.csv.BU');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52269, 'BU.java', 116, 'ERROR', 'The Master File NTPO_AD1706051818_080.TXT uploaded successfully. ', NULL, 'com.parkson.poAdmin.backend.csv.BU');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52270, 'BU.java', 143, 'ERROR', 'Failed Dropping Database.', NULL, 'com.parkson.poAdmin.backend.csv.BU');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52271, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52272, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52273, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52274, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52275, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52276, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52277, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52278, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52279, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52280, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52281, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52282, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52283, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52284, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52285, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52286, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52287, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52288, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52289, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52290, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52291, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52292, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52293, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52294, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52295, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52296, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52297, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52298, 'MainScreenView.java', 90, 'DEBUG', 'Building menu content for superadmin user', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52299, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52300, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52301, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52302, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52303, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52304, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52305, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52306, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52307, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52308, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52309, 'AdminView.java', 150, 'DEBUG', 'Initializing screen components behaviour', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52310, 'AdminView.java', 169, 'DEBUG', 'ERROR: SYSPARAM: User validity Period is missing', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52311, 'AdminView.java', 226, 'DEBUG', 'Setting component caption', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52312, 'AdminView.java', 299, 'DEBUG', 'Setting fields value change listener', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52313, 'AdminView.java', 318, 'DEBUG', 'Setting fields validator', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52314, 'AdminView.java', 325, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52315, 'AdminView.java', 350, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52512, 'AdminView.java', 890, 'DEBUG', 'Entering Company Screen', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52316, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52317, 'AdminView.java', 355, 'DEBUG', 'Getting all admin records', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52318, 'AdminView.java', 366, 'DEBUG', 'Binding data to paged container', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52319, 'AdminView.java', 380, 'DEBUG', 'Hiding unwanted attributes', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52320, 'AdminView.java', 387, 'DEBUG', 'Setting data source of grid', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52321, 'AdminView.java', 415, 'DEBUG', 'Setting initial state of grid', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52322, 'AdminView.java', 434, 'DEBUG', 'Adding "edit" column', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52323, 'MainScreenView.java', 194, 'DEBUG', 'Building header', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52324, 'MainScreenView.java', 201, 'DEBUG', 'Setting logo in header', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52325, 'MainScreenView.java', 234, 'DEBUG', 'Setting menu for screen setting', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52326, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52327, 'MainScreenView.java', 274, 'DEBUG', 'Setting menu for user profile setting', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52328, 'MainScreenView.java', 339, 'DEBUG', 'JavaScript function for closing popup when page scrolls', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52329, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52330, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52331, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52332, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52333, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52334, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52335, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52336, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52337, 'SchedulerView.java', 137, 'DEBUG', 'Initializing screen components behaviour', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52338, 'SchedulerView.java', 171, 'DEBUG', 'Closing file input stream', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52339, 'SchedulerView.java', 207, 'DEBUG', 'Closing file input stream', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52340, 'SchedulerView.java', 227, 'DEBUG', 'Setting component caption', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52341, 'SchedulerView.java', 647, 'DEBUG', 'Setting click listener for cancel button', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52342, 'SchedulerView.java', 650, 'DEBUG', 'Setting click listener for save button', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52343, 'Logs.java', 71, 'DEBUG', 'MF22_M_SCHEDULER records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52344, 'SchedulerView.java', 656, 'DEBUG', 'Getting all Order Category records', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52345, 'SchedulerView.java', 665, 'DEBUG', 'Wrapping Order Category record to container that can be understood by Grid', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52346, 'SchedulerView.java', 689, 'DEBUG', 'Hiding unwanted attributes', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52347, 'SchedulerView.java', 695, 'DEBUG', 'Filling grid with data', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52348, 'SchedulerView.java', 717, 'DEBUG', 'Adding "edit" column', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52349, 'Logs.java', 59, 'DEBUG', 'LD04_T_AUDIT record is successfully saved into the database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52350, 'Logs.java', 59, 'DEBUG', 'MF22_M_SCHEDULER record is successfully saved into the database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52351, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52352, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52353, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52354, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52355, 'BU.java', 85, 'DEBUG', 'Closing file input stream', NULL, 'com.parkson.poAdmin.backend.csv.BU');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52356, 'Logs.java', 71, 'DEBUG', 'MF22_M_SCHEDULER records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52357, 'BU.java', 116, 'ERROR', 'The Master File NTPO_PC1705051818_080.TXT uploaded successfully. ', NULL, 'com.parkson.poAdmin.backend.csv.BU');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52358, 'BU.java', 143, 'ERROR', 'Failed Dropping Database.', NULL, 'com.parkson.poAdmin.backend.csv.BU');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52359, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52360, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52361, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52362, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52363, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52364, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52365, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52366, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52367, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52368, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52369, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52370, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52371, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52372, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52373, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52374, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52375, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52376, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52377, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52378, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52379, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52380, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52381, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52382, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52383, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52384, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52385, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52386, 'MainScreenView.java', 90, 'DEBUG', 'Building menu content for superadmin user', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52387, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52388, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52389, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52390, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52391, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52392, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52393, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52394, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52395, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52396, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52397, 'AdminView.java', 150, 'DEBUG', 'Initializing screen components behaviour', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52398, 'AdminView.java', 169, 'DEBUG', 'ERROR: SYSPARAM: User validity Period is missing', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52399, 'AdminView.java', 226, 'DEBUG', 'Setting component caption', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52400, 'AdminView.java', 299, 'DEBUG', 'Setting fields value change listener', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52401, 'AdminView.java', 318, 'DEBUG', 'Setting fields validator', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52402, 'AdminView.java', 325, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52403, 'AdminView.java', 350, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52404, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52405, 'AdminView.java', 355, 'DEBUG', 'Getting all admin records', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52406, 'AdminView.java', 366, 'DEBUG', 'Binding data to paged container', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52407, 'AdminView.java', 380, 'DEBUG', 'Hiding unwanted attributes', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52408, 'AdminView.java', 387, 'DEBUG', 'Setting data source of grid', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52409, 'AdminView.java', 415, 'DEBUG', 'Setting initial state of grid', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52410, 'AdminView.java', 434, 'DEBUG', 'Adding "edit" column', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52411, 'MainScreenView.java', 194, 'DEBUG', 'Building header', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52412, 'MainScreenView.java', 201, 'DEBUG', 'Setting logo in header', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52413, 'MainScreenView.java', 234, 'DEBUG', 'Setting menu for screen setting', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52414, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52415, 'MainScreenView.java', 274, 'DEBUG', 'Setting menu for user profile setting', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52416, 'MainScreenView.java', 339, 'DEBUG', 'JavaScript function for closing popup when page scrolls', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52417, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52418, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52419, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52420, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52421, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52422, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52423, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52424, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52425, 'SchedulerView.java', 137, 'DEBUG', 'Initializing screen components behaviour', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52426, 'SchedulerView.java', 171, 'DEBUG', 'Closing file input stream', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52427, 'SchedulerView.java', 207, 'DEBUG', 'Closing file input stream', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52428, 'SchedulerView.java', 227, 'DEBUG', 'Setting component caption', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52429, 'SchedulerView.java', 647, 'DEBUG', 'Setting click listener for cancel button', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52430, 'SchedulerView.java', 650, 'DEBUG', 'Setting click listener for save button', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52431, 'Logs.java', 71, 'DEBUG', 'MF22_M_SCHEDULER records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52432, 'SchedulerView.java', 656, 'DEBUG', 'Getting all Order Category records', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52433, 'SchedulerView.java', 665, 'DEBUG', 'Wrapping Order Category record to container that can be understood by Grid', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52434, 'SchedulerView.java', 689, 'DEBUG', 'Hiding unwanted attributes', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52435, 'SchedulerView.java', 695, 'DEBUG', 'Filling grid with data', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52436, 'SchedulerView.java', 717, 'DEBUG', 'Adding "edit" column', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52437, 'Logs.java', 59, 'DEBUG', 'LD04_T_AUDIT record is successfully saved into the database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52438, 'Logs.java', 59, 'DEBUG', 'MF22_M_SCHEDULER record is successfully saved into the database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52439, 'Logs.java', 71, 'DEBUG', 'MF22_M_SCHEDULER records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52440, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52441, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52442, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52443, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52444, 'BU.java', 85, 'DEBUG', 'Closing file input stream', NULL, 'com.parkson.poAdmin.backend.csv.BU');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52579, 'AdminView.java', 226, 'DEBUG', 'Setting component caption', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52445, 'BU.java', 116, 'ERROR', 'The Master File NTPO_AD1705051818_080.TXT uploaded partially. ', NULL, 'com.parkson.poAdmin.backend.csv.BU');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52446, 'BU.java', 116, 'ERROR', 'The Master File NTPO_AD1706051818_080.TXT uploaded successfully. ', NULL, 'com.parkson.poAdmin.backend.csv.BU');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52447, 'BU.java', 116, 'ERROR', 'The Master File NTPO_SM1705051818_080.TXT uploaded successfully. ', NULL, 'com.parkson.poAdmin.backend.csv.BU');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52448, 'BU.java', 116, 'ERROR', 'The Master File NTPO_PC1705051818_080.TXT uploaded successfully. ', NULL, 'com.parkson.poAdmin.backend.csv.BU');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52449, 'BU.java', 143, 'ERROR', 'Failed Dropping Database.', NULL, 'com.parkson.poAdmin.backend.csv.BU');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52450, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52451, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52452, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52453, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52454, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52455, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52456, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52457, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52458, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52459, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52460, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52461, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52462, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52463, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52464, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52465, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52466, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52467, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52468, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52469, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52470, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52471, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52472, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52473, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52474, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52475, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52476, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52477, 'MainScreenView.java', 90, 'DEBUG', 'Building menu content for superadmin user', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52478, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52479, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52480, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52481, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52482, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52483, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52484, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52485, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52486, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52487, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52488, 'AdminView.java', 150, 'DEBUG', 'Initializing screen components behaviour', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52489, 'AdminView.java', 169, 'DEBUG', 'ERROR: SYSPARAM: User validity Period is missing', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52490, 'AdminView.java', 226, 'DEBUG', 'Setting component caption', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52491, 'AdminView.java', 299, 'DEBUG', 'Setting fields value change listener', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52492, 'AdminView.java', 318, 'DEBUG', 'Setting fields validator', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52493, 'AdminView.java', 325, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52494, 'AdminView.java', 350, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52495, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52496, 'AdminView.java', 355, 'DEBUG', 'Getting all admin records', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52497, 'AdminView.java', 366, 'DEBUG', 'Binding data to paged container', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52498, 'AdminView.java', 380, 'DEBUG', 'Hiding unwanted attributes', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52499, 'AdminView.java', 387, 'DEBUG', 'Setting data source of grid', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52500, 'AdminView.java', 415, 'DEBUG', 'Setting initial state of grid', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52501, 'AdminView.java', 434, 'DEBUG', 'Adding "edit" column', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52502, 'MainScreenView.java', 194, 'DEBUG', 'Building header', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52503, 'MainScreenView.java', 201, 'DEBUG', 'Setting logo in header', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52504, 'MainScreenView.java', 234, 'DEBUG', 'Setting menu for screen setting', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52505, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52506, 'MainScreenView.java', 274, 'DEBUG', 'Setting menu for user profile setting', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52507, 'MainScreenView.java', 339, 'DEBUG', 'JavaScript function for closing popup when page scrolls', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52508, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52509, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52510, 'AdminView.java', 464, 'DEBUG', 'Getting all admin records', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52513, 'Logs.java', 59, 'DEBUG', 'LD04_T_AUDIT record is successfully saved into the database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52514, 'LogView.java', 176, 'DEBUG', 'Getting all log records', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52515, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52516, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52517, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52518, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52519, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52520, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52521, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52522, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52523, 'OrderCategoryView.java', 155, 'DEBUG', 'Initializing screen components behaviour', NULL, 'PO-AD17L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52524, 'OrderCategoryView.java', 190, 'DEBUG', 'Closing file input stream', NULL, 'PO-AD17L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52525, 'OrderCategoryView.java', 214, 'DEBUG', 'Setting component caption', NULL, 'PO-AD17L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52526, 'OrderCategoryView.java', 314, 'DEBUG', 'Setting fields validator', NULL, 'PO-AD17L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52527, 'OrderCategoryView.java', 328, 'DEBUG', 'Setting fields value change listener', NULL, 'PO-AD17L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52528, 'OrderCategoryView.java', 338, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-AD17L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52529, 'OrderCategoryView.java', 358, 'DEBUG', 'Setting click listener for save button', NULL, 'PO-AD17L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52530, 'Logs.java', 71, 'DEBUG', 'MF17_M_ORDCATMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52531, 'OrderCategoryView.java', 366, 'DEBUG', 'Getting all Order Category records', NULL, 'PO-AD17L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52532, 'OrderCategoryView.java', 375, 'DEBUG', 'Wrapping Order Category record to container that can be understood by Grid', NULL, 'PO-AD17L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52533, 'OrderCategoryView.java', 391, 'DEBUG', 'Hiding unwanted attributes', NULL, 'PO-AD17L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52534, 'OrderCategoryView.java', 397, 'DEBUG', 'Filling grid with data', NULL, 'PO-AD17L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52535, 'OrderCategoryView.java', 428, 'DEBUG', 'Adding "edit" column', NULL, 'PO-AD17L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52536, 'OrderCategoryView.java', 1112, 'DEBUG', 'Inside Order Category View', NULL, 'PO-AD17L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52537, 'Logs.java', 71, 'DEBUG', 'MF15_M_PRMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52538, 'LogView.java', 176, 'DEBUG', 'Getting all log records', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52539, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52540, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52541, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52542, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52543, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52544, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52545, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52546, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52547, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52548, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52549, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52550, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52551, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52552, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52553, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52554, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52555, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52556, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52557, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52558, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52559, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52560, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52561, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52562, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52563, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52564, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52565, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52566, 'MainScreenView.java', 90, 'DEBUG', 'Building menu content for superadmin user', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52567, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52568, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52569, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52570, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52571, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52572, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52573, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52574, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52575, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52576, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52577, 'AdminView.java', 150, 'DEBUG', 'Initializing screen components behaviour', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52578, 'AdminView.java', 169, 'DEBUG', 'ERROR: SYSPARAM: User validity Period is missing', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52580, 'AdminView.java', 299, 'DEBUG', 'Setting fields value change listener', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52581, 'AdminView.java', 318, 'DEBUG', 'Setting fields validator', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52582, 'AdminView.java', 325, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52583, 'AdminView.java', 350, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52584, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52585, 'AdminView.java', 355, 'DEBUG', 'Getting all admin records', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52586, 'AdminView.java', 366, 'DEBUG', 'Binding data to paged container', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52587, 'AdminView.java', 380, 'DEBUG', 'Hiding unwanted attributes', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52588, 'AdminView.java', 387, 'DEBUG', 'Setting data source of grid', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52589, 'AdminView.java', 415, 'DEBUG', 'Setting initial state of grid', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52590, 'AdminView.java', 434, 'DEBUG', 'Adding "edit" column', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52591, 'MainScreenView.java', 194, 'DEBUG', 'Building header', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52592, 'MainScreenView.java', 201, 'DEBUG', 'Setting logo in header', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52593, 'MainScreenView.java', 234, 'DEBUG', 'Setting menu for screen setting', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52594, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52595, 'MainScreenView.java', 274, 'DEBUG', 'Setting menu for user profile setting', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52596, 'MainScreenView.java', 339, 'DEBUG', 'JavaScript function for closing popup when page scrolls', NULL, 'PO-HM01S');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52597, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52598, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52599, 'AdminView.java', 464, 'DEBUG', 'Getting all admin records', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52600, 'AdminView.java', 469, 'DEBUG', 'Binding data to paged container', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52601, 'AdminView.java', 890, 'DEBUG', 'Entering Company Screen', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52602, 'Logs.java', 59, 'DEBUG', 'LD04_T_AUDIT record is successfully saved into the database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52603, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52604, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52605, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52606, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52607, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52608, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52609, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52610, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52611, 'ProfitCenterView.java', 178, 'DEBUG', 'Initializing screen components behaviour', NULL, 'PO-AD13L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52612, 'ProfitCenterView.java', 196, 'DEBUG', 'Closing file input stream', NULL, 'PO-AD13L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52613, 'Logs.java', 71, 'DEBUG', 'MF14_M_DEPTMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52614, 'Logs.java', 71, 'DEBUG', 'MF11_M_COMPMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52615, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.[Ljava.lang.Object; cannot be cast to com.parkson.poAdmin.backend.data.MF02_M_STRMAST', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52616, 'Logs.java', 71, 'DEBUG', 'MF02_M_STRMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52617, 'Logs.java', 71, 'DEBUG', 'MF13_M_PCMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52618, 'ProfitCenterView.java', 721, 'DEBUG', 'Setting fields value change listener', NULL, 'PO-AD13L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52619, 'Logs.java', 71, 'DEBUG', 'MF14_M_DEPTMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52620, 'Logs.java', 71, 'DEBUG', 'MF11_M_COMPMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52621, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.[Ljava.lang.Object; cannot be cast to com.parkson.poAdmin.backend.data.MF02_M_STRMAST', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52622, 'Logs.java', 71, 'DEBUG', 'MF02_M_STRMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52623, 'Logs.java', 71, 'DEBUG', 'MF11_M_COMPMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52624, 'Logs.java', 71, 'DEBUG', 'MF14_M_DEPTMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52625, 'Logs.java', 71, 'DEBUG', 'MF12_M_EMPMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52626, 'Logs.java', 71, 'DEBUG', 'MF12_M_EMPMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52627, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.[Ljava.lang.Object; cannot be cast to com.parkson.poAdmin.backend.data.MF02_M_STRMAST', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52628, 'Logs.java', 71, 'DEBUG', 'MF02_M_STRMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52629, 'ProfitCenterView.java', 1927, 'DEBUG', 'Entering profit center Screen', NULL, 'PO-AD13L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52630, 'ProfitCenterView.java', 320, 'DEBUG', 'DEBUG on rendering Company Code', NULL, 'PO-AD13L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52631, 'ProfitCenterView.java', 357, 'DEBUG', 'DEBUG on rendering Department Code', NULL, 'PO-AD13L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52632, 'ProfitCenterView.java', 338, 'DEBUG', 'DEBUG on rendering Store Code', NULL, 'PO-AD13L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52633, 'ProfitCenterView.java', 320, 'DEBUG', 'DEBUG on rendering Company Code', NULL, 'PO-AD13L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52634, 'ProfitCenterView.java', 357, 'DEBUG', 'DEBUG on rendering Department Code', NULL, 'PO-AD13L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52635, 'ProfitCenterView.java', 338, 'DEBUG', 'DEBUG on rendering Store Code', NULL, 'PO-AD13L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52636, 'ProfitCenterView.java', 320, 'DEBUG', 'DEBUG on rendering Company Code', NULL, 'PO-AD13L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52637, 'ProfitCenterView.java', 357, 'DEBUG', 'DEBUG on rendering Department Code', NULL, 'PO-AD13L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52638, 'ProfitCenterView.java', 338, 'DEBUG', 'DEBUG on rendering Store Code', NULL, 'PO-AD13L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52639, 'ProfitCenterView.java', 320, 'DEBUG', 'DEBUG on rendering Company Code', NULL, 'PO-AD13L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52640, 'ProfitCenterView.java', 357, 'DEBUG', 'DEBUG on rendering Department Code', NULL, 'PO-AD13L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52641, 'ProfitCenterView.java', 338, 'DEBUG', 'DEBUG on rendering Store Code', NULL, 'PO-AD13L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52642, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52643, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52644, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52645, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52646, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52647, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52648, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52649, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52650, 'JobMasterView.java', 98, 'DEBUG', 'Initializing screen components behaviour', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52651, 'JobMasterView.java', 132, 'DEBUG', 'Closing file input stream', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52652, 'JobMasterView.java', 154, 'DEBUG', 'Setting component caption', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52653, 'JobMasterView.java', 159, 'DEBUG', 'Setting fields validator', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52654, 'JobMasterView.java', 170, 'DEBUG', 'Setting fields value change listener', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52655, 'JobMasterView.java', 175, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52656, 'JobMasterView.java', 198, 'DEBUG', 'Setting click listener for save button', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52657, 'Logs.java', 71, 'DEBUG', 'MF19_M_JOBMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52658, 'JobMasterView.java', 203, 'DEBUG', 'Getting all job master records', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52659, 'JobMasterView.java', 211, 'DEBUG', 'Wrapping Job Master record to container that can be understood by Grid', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52660, 'JobMasterView.java', 226, 'DEBUG', 'Hiding unwanted attributes', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52661, 'JobMasterView.java', 233, 'DEBUG', 'Filling grid with data', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52662, 'JobMasterView.java', 265, 'DEBUG', 'Adding "edit" column', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52663, 'JobMasterView.java', 699, 'DEBUG', 'Entering Company Screen', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52664, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52665, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52666, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52667, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52668, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52669, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52670, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52671, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52672, 'SchedulerView.java', 137, 'DEBUG', 'Initializing screen components behaviour', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52673, 'SchedulerView.java', 171, 'DEBUG', 'Closing file input stream', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52674, 'SchedulerView.java', 207, 'DEBUG', 'Closing file input stream', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52675, 'SchedulerView.java', 227, 'DEBUG', 'Setting component caption', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52676, 'SchedulerView.java', 647, 'DEBUG', 'Setting click listener for cancel button', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52677, 'SchedulerView.java', 650, 'DEBUG', 'Setting click listener for save button', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52678, 'Logs.java', 71, 'DEBUG', 'MF22_M_SCHEDULER records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52679, 'SchedulerView.java', 656, 'DEBUG', 'Getting all Order Category records', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52680, 'SchedulerView.java', 665, 'DEBUG', 'Wrapping Order Category record to container that can be understood by Grid', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52681, 'SchedulerView.java', 689, 'DEBUG', 'Hiding unwanted attributes', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52682, 'SchedulerView.java', 695, 'DEBUG', 'Filling grid with data', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52683, 'SchedulerView.java', 717, 'DEBUG', 'Adding "edit" column', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52684, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52685, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52686, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52687, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52688, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52689, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52690, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52691, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52692, 'OrderSubCatView.java', 104, 'DEBUG', 'Initializing screen components behaviour', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52693, 'OrderSubCatView.java', 138, 'DEBUG', 'Closing file input stream', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52694, 'OrderSubCatView.java', 161, 'DEBUG', 'Setting component caption', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52695, 'OrderSubCatView.java', 167, 'DEBUG', 'Setting fields validator', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52696, 'OrderSubCatView.java', 184, 'DEBUG', 'Setting fields value change listener', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52697, 'OrderSubCatView.java', 195, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52698, 'OrderSubCatView.java', 217, 'DEBUG', 'Setting click listener for save button', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52699, 'Logs.java', 71, 'DEBUG', 'MF18_M_ORDSUBCMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52700, 'OrderSubCatView.java', 222, 'DEBUG', 'Getting all order sub category records', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52701, 'OrderSubCatView.java', 228, 'DEBUG', 'Wrapping order sub category records to container that can be understood by Grid', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52702, 'OrderSubCatView.java', 243, 'DEBUG', 'Hiding unwanted attributes', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52703, 'OrderSubCatView.java', 250, 'DEBUG', 'Filling grid with data', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52704, 'OrderSubCatView.java', 281, 'DEBUG', 'Adding "edit" column', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52705, 'OrderSubCatView.java', 731, 'DEBUG', 'Inside Order Sub Category View', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52706, 'Logs.java', 71, 'DEBUG', 'MF17_M_ORDCATMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52707, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52708, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52709, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52710, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52711, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52712, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52713, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52714, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52715, 'OrderCategoryView.java', 155, 'DEBUG', 'Initializing screen components behaviour', NULL, 'PO-AD17L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52716, 'OrderCategoryView.java', 190, 'DEBUG', 'Closing file input stream', NULL, 'PO-AD17L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52717, 'OrderCategoryView.java', 214, 'DEBUG', 'Setting component caption', NULL, 'PO-AD17L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52718, 'OrderCategoryView.java', 314, 'DEBUG', 'Setting fields validator', NULL, 'PO-AD17L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52719, 'OrderCategoryView.java', 328, 'DEBUG', 'Setting fields value change listener', NULL, 'PO-AD17L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52720, 'OrderCategoryView.java', 338, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-AD17L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52721, 'OrderCategoryView.java', 358, 'DEBUG', 'Setting click listener for save button', NULL, 'PO-AD17L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52722, 'Logs.java', 71, 'DEBUG', 'MF17_M_ORDCATMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52723, 'OrderCategoryView.java', 366, 'DEBUG', 'Getting all Order Category records', NULL, 'PO-AD17L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52724, 'OrderCategoryView.java', 375, 'DEBUG', 'Wrapping Order Category record to container that can be understood by Grid', NULL, 'PO-AD17L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52725, 'OrderCategoryView.java', 391, 'DEBUG', 'Hiding unwanted attributes', NULL, 'PO-AD17L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52726, 'OrderCategoryView.java', 397, 'DEBUG', 'Filling grid with data', NULL, 'PO-AD17L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52727, 'OrderCategoryView.java', 428, 'DEBUG', 'Adding "edit" column', NULL, 'PO-AD17L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52728, 'OrderCategoryView.java', 1112, 'DEBUG', 'Inside Order Category View', NULL, 'PO-AD17L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52729, 'Logs.java', 71, 'DEBUG', 'MF15_M_PRMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52730, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52731, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52732, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52733, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52734, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52735, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52736, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52737, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52738, 'StoreView.java', 149, 'DEBUG', 'Initializing screen components behaviour', NULL, 'PO-AD02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52739, 'StoreView.java', 189, 'DEBUG', 'Closing file input stream', NULL, 'PO-AD02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52740, 'StoreView.java', 215, 'DEBUG', 'Setting component caption', NULL, 'PO-AD02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52741, 'StoreView.java', 221, 'DEBUG', 'Setting component styles', NULL, 'PO-AD02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52742, 'StoreView.java', 228, 'DEBUG', 'Setting fields validator', NULL, 'PO-AD02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52743, 'StoreView.java', 239, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-AD02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52744, 'StoreView.java', 251, 'DEBUG', 'Setting state of combobox', NULL, 'PO-AD02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52745, 'StoreView.java', 258, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-AD02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52746, 'StoreView.java', 284, 'DEBUG', 'Setting click listener for save button', NULL, 'PO-AD02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52747, 'Logs.java', 71, 'DEBUG', 'MF02_M_STRMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52748, 'StoreView.java', 292, 'DEBUG', 'Getting all store records', NULL, 'PO-AD02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52749, 'StoreView.java', 957, 'DEBUG', 'Entering Store Screen', NULL, 'PO-AD02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52750, 'Logs.java', 71, 'DEBUG', 'MF11_M_COMPMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52751, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52752, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52753, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52754, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52755, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52756, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52757, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52758, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52759, 'SystemParametersView.java', 103, 'DEBUG', 'Initializing screen components behaviour', NULL, 'PO-AD07L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52760, 'SystemParametersView.java', 139, 'DEBUG', 'Closing file input stream', NULL, 'PO-AD07L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52761, 'SystemParametersView.java', 161, 'DEBUG', 'Setting component caption', NULL, 'PO-AD07L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52762, 'SystemParametersView.java', 167, 'DEBUG', 'Setting fields validator', NULL, 'PO-AD07L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52763, 'SystemParametersView.java', 181, 'DEBUG', 'Setting fields value change listener', NULL, 'PO-AD07L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52764, 'SystemParametersView.java', 185, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-AD07L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52765, 'SystemParametersView.java', 208, 'DEBUG', 'Setting click listener for save button', NULL, 'PO-AD07L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52766, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52767, 'SystemParametersView.java', 218, 'DEBUG', 'Getting all System Parameters records', NULL, 'PO-AD07L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52768, 'SystemParametersView.java', 226, 'DEBUG', 'Wrapping System Parameters record to container that can be understood by Grid', NULL, 'PO-AD07L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52769, 'SystemParametersView.java', 244, 'DEBUG', 'Hiding unwanted attributes', NULL, 'PO-AD07L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52770, 'SystemParametersView.java', 251, 'DEBUG', 'Filling grid with data', NULL, 'PO-AD07L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52771, 'SystemParametersView.java', 282, 'DEBUG', 'Adding "edit" column', NULL, 'PO-AD07L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52772, 'SystemParametersView.java', 731, 'DEBUG', 'Inside System Parameters View', NULL, 'PO-AD07L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52773, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52774, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52775, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52776, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52777, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52778, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52779, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52780, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52781, 'MasterDataView.java', 105, 'DEBUG', 'Initializing screen components behaviour', NULL, 'PO-AD06L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52782, 'MasterDataView.java', 142, 'DEBUG', 'Closing file input stream', NULL, 'PO-AD06L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52783, 'MasterDataView.java', 165, 'DEBUG', 'Setting component caption', NULL, 'PO-AD06L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52784, 'MasterDataView.java', 183, 'DEBUG', 'Setting fields value change listener', NULL, 'PO-AD06L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52785, 'MasterDataView.java', 187, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-AD06L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52786, 'MasterDataView.java', 212, 'DEBUG', 'Setting click listener for save button', NULL, 'PO-AD06L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52787, 'Logs.java', 71, 'DEBUG', 'MF06_R_MASTDATA records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52788, 'MasterDataView.java', 224, 'DEBUG', 'Getting all master data records', NULL, 'PO-AD06L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52789, 'MasterDataView.java', 232, 'DEBUG', 'Wrapping Job Master record to container that can be understood by Grid', NULL, 'PO-AD06L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52790, 'MasterDataView.java', 249, 'DEBUG', 'Hiding unwanted attributes', NULL, 'PO-AD06L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52791, 'MasterDataView.java', 256, 'DEBUG', 'Filling grid with data', NULL, 'PO-AD06L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52792, 'MasterDataView.java', 288, 'DEBUG', 'Adding "edit" column', NULL, 'PO-AD06L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52793, 'MasterDataView.java', 749, 'DEBUG', 'Entering Master Data Screen', NULL, 'PO-AD06L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52794, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52795, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52796, 'AdminView.java', 464, 'DEBUG', 'Getting all admin records', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52797, 'AdminView.java', 469, 'DEBUG', 'Binding data to paged container', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52798, 'AdminView.java', 890, 'DEBUG', 'Entering Company Screen', NULL, 'PO-SA20L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52799, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52800, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52801, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52802, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52803, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52804, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52805, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52806, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52807, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52808, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52809, 'CreatePOuserView.java', 168, 'DEBUG', 'Initializing screen components behaviour', NULL, 'com.parkson.poAdmin.master.CreatePOuserView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52810, 'CreatePOuserView.java', 227, 'DEBUG', 'Setting component caption', NULL, 'com.parkson.poAdmin.master.CreatePOuserView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52811, 'CreatePOuserView.java', 233, 'DEBUG', 'Setting click listener for cancel button', NULL, 'com.parkson.poAdmin.master.CreatePOuserView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52812, 'CreatePOuserView.java', 257, 'DEBUG', 'Setting click listener for cancel button', NULL, 'com.parkson.poAdmin.master.CreatePOuserView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52813, 'Logs.java', 71, 'DEBUG', 'PO06_T_USER records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52814, 'CreatePOuserView.java', 262, 'DEBUG', 'Getting all admin records', NULL, 'com.parkson.poAdmin.master.CreatePOuserView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52815, 'CreatePOuserView.java', 270, 'DEBUG', 'Binding data to paged container', NULL, 'com.parkson.poAdmin.master.CreatePOuserView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52816, 'CreatePOuserView.java', 284, 'DEBUG', 'Hiding unwanted attributes', NULL, 'com.parkson.poAdmin.master.CreatePOuserView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52817, 'CreatePOuserView.java', 291, 'DEBUG', 'Setting data source of grid', NULL, 'com.parkson.poAdmin.master.CreatePOuserView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52818, 'CreatePOuserView.java', 307, 'DEBUG', 'Setting initial state of grid', NULL, 'com.parkson.poAdmin.master.CreatePOuserView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52819, 'CreatePOuserView.java', 326, 'DEBUG', 'Adding "edit" column', NULL, 'com.parkson.poAdmin.master.CreatePOuserView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52820, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.PO06_T_USER', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52821, 'Logs.java', 71, 'DEBUG', 'MF12_M_EMPMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52822, 'Logs.java', 71, 'DEBUG', 'MF20_M_ADMINMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52823, 'Logs.java', 71, 'DEBUG', 'PO06_T_USER records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52824, 'Logs.java', 71, 'DEBUG', 'MF12_M_EMPMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52825, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.PO06_T_USER', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52826, 'CreatePOuserView.java', 682, 'DEBUG', 'Entering Company Screen', NULL, 'com.parkson.poAdmin.master.CreatePOuserView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52827, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52828, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52829, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52830, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52831, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52832, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52833, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52834, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52835, 'CompanyView.java', 200, 'DEBUG', 'Initializing screen components behaviour', NULL, 'PO-AD11L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52836, 'CompanyView.java', 244, 'DEBUG', 'Closing file input stream', NULL, 'PO-AD11L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52837, 'CompanyView.java', 274, 'DEBUG', 'Setting component caption', NULL, 'PO-AD11L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52838, 'CompanyView.java', 283, 'DEBUG', 'Setting fields validator', NULL, 'PO-AD11L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52839, 'CompanyView.java', 647, 'DEBUG', 'Setting fields value change listener', NULL, 'PO-AD11L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52840, 'CompanyView.java', 663, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-AD11L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52841, 'CompanyView.java', 714, 'DEBUG', 'Setting click listener for save button', NULL, 'PO-AD11L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52842, 'Logs.java', 71, 'DEBUG', 'MF11_M_COMPMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52843, 'CompanyView.java', 1584, 'DEBUG', 'Entering Company Screen', NULL, 'PO-AD11L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52844, 'Logs.java', 71, 'DEBUG', 'MF11_M_COMPMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52845, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52846, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52847, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52848, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52849, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52850, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52851, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52852, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52853, 'DepartmentView.java', 102, 'DEBUG', 'Initializing screen components behaviour', NULL, 'PO-AD14L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52854, 'DepartmentView.java', 137, 'DEBUG', 'Closing file input stream', NULL, 'PO-AD14L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52855, 'DepartmentView.java', 159, 'DEBUG', 'Setting component caption', NULL, 'PO-AD14L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52856, 'DepartmentView.java', 174, 'DEBUG', 'Setting fields validator', NULL, 'PO-AD14L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52857, 'DepartmentView.java', 182, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-AD14L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52858, 'DepartmentView.java', 211, 'DEBUG', 'Setting click listener for save button', NULL, 'PO-AD14L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52859, 'Logs.java', 71, 'DEBUG', 'MF14_M_DEPTMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52860, 'DepartmentView.java', 217, 'DEBUG', 'Getting all department records', NULL, 'PO-AD14L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52861, 'DepartmentView.java', 417, 'DEBUG', 'Clear form after click cancel button', NULL, 'PO-AD14L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52862, 'DepartmentView.java', 714, 'DEBUG', 'Entering department Screen', NULL, 'PO-AD14L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52863, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52864, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52865, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52866, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52867, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52868, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52869, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52870, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52871, 'GSTView.java', 138, 'DEBUG', 'Inside GST View', NULL, 'PO-AD16L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52872, 'GSTView.java', 164, 'DEBUG', 'Initializing screen components behaviour', NULL, 'PO-AD16L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52873, 'GSTView.java', 234, 'DEBUG', 'Setting component caption', NULL, 'PO-AD16L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52874, 'Logs.java', 71, 'DEBUG', 'MF16_M_GSTMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52875, 'GSTView.java', 239, 'DEBUG', 'Getting all GST records', NULL, 'PO-AD16L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52876, 'GSTView.java', 325, 'DEBUG', 'Setting fields validator', NULL, 'PO-AD16L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52877, 'GSTView.java', 394, 'DEBUG', 'Setting fields value change listener', NULL, 'PO-AD16L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52878, 'GSTView.java', 401, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-AD16L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52879, 'GSTView.java', 426, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-AD16L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52880, 'GSTView.java', 1083, 'DEBUG', 'Entering GST Screen', NULL, 'PO-AD16L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52881, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52882, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52883, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52884, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52885, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52886, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52887, 'Logs.java', 77, 'ERROR', 'Failed fetching records from database.Index: 0, Size: 0', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52888, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52889, 'PurchaseRequisitionView.java', 129, 'DEBUG', 'Initializing screen components behaviour', NULL, 'PO-AD15L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52890, 'PurchaseRequisitionView.java', 162, 'DEBUG', 'Closing file input stream', NULL, 'PO-AD15L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52891, 'PurchaseRequisitionView.java', 185, 'DEBUG', 'Setting component caption', NULL, 'PO-AD15L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52892, 'PurchaseRequisitionView.java', 191, 'DEBUG', 'Setting fields validator', NULL, 'PO-AD15L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52893, 'PurchaseRequisitionView.java', 201, 'DEBUG', 'Setting fields value change listener', NULL, 'PO-AD15L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52894, 'PurchaseRequisitionView.java', 214, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-AD15L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52895, 'PurchaseRequisitionView.java', 239, 'DEBUG', 'Setting click listener for save button', NULL, 'PO-AD15L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52896, 'Logs.java', 71, 'DEBUG', 'MF15_M_PRMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52897, 'PurchaseRequisitionView.java', 247, 'DEBUG', 'Getting all purchase requisition records', NULL, 'PO-AD15L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52898, 'PurchaseRequisitionView.java', 719, 'DEBUG', 'Entering Purchase Requisition Screen', NULL, 'PO-AD15L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52899, 'OrderCategoryView.java', 155, 'DEBUG', 'Initializing screen components behaviour', NULL, 'PO-AD17L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52900, 'OrderCategoryView.java', 190, 'DEBUG', 'Closing file input stream', NULL, 'PO-AD17L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52901, 'OrderCategoryView.java', 214, 'DEBUG', 'Setting component caption', NULL, 'PO-AD17L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52902, 'OrderCategoryView.java', 314, 'DEBUG', 'Setting fields validator', NULL, 'PO-AD17L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52903, 'OrderCategoryView.java', 328, 'DEBUG', 'Setting fields value change listener', NULL, 'PO-AD17L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52904, 'OrderCategoryView.java', 338, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-AD17L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52905, 'OrderCategoryView.java', 358, 'DEBUG', 'Setting click listener for save button', NULL, 'PO-AD17L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52906, 'Logs.java', 71, 'DEBUG', 'MF17_M_ORDCATMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52907, 'OrderCategoryView.java', 366, 'DEBUG', 'Getting all Order Category records', NULL, 'PO-AD17L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52908, 'OrderCategoryView.java', 375, 'DEBUG', 'Wrapping Order Category record to container that can be understood by Grid', NULL, 'PO-AD17L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52909, 'OrderCategoryView.java', 391, 'DEBUG', 'Hiding unwanted attributes', NULL, 'PO-AD17L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52910, 'OrderCategoryView.java', 397, 'DEBUG', 'Filling grid with data', NULL, 'PO-AD17L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52911, 'OrderCategoryView.java', 428, 'DEBUG', 'Adding "edit" column', NULL, 'PO-AD17L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52912, 'OrderCategoryView.java', 1112, 'DEBUG', 'Inside Order Category View', NULL, 'PO-AD17L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52913, 'Logs.java', 71, 'DEBUG', 'MF15_M_PRMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52914, 'OrderSubCatView.java', 104, 'DEBUG', 'Initializing screen components behaviour', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52915, 'OrderSubCatView.java', 138, 'DEBUG', 'Closing file input stream', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52916, 'OrderSubCatView.java', 161, 'DEBUG', 'Setting component caption', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52917, 'OrderSubCatView.java', 167, 'DEBUG', 'Setting fields validator', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52918, 'OrderSubCatView.java', 184, 'DEBUG', 'Setting fields value change listener', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52919, 'OrderSubCatView.java', 195, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52920, 'OrderSubCatView.java', 217, 'DEBUG', 'Setting click listener for save button', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52921, 'Logs.java', 71, 'DEBUG', 'MF18_M_ORDSUBCMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52922, 'OrderSubCatView.java', 222, 'DEBUG', 'Getting all order sub category records', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52923, 'OrderSubCatView.java', 228, 'DEBUG', 'Wrapping order sub category records to container that can be understood by Grid', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52924, 'OrderSubCatView.java', 243, 'DEBUG', 'Hiding unwanted attributes', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52925, 'OrderSubCatView.java', 250, 'DEBUG', 'Filling grid with data', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52926, 'OrderSubCatView.java', 281, 'DEBUG', 'Adding "edit" column', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52927, 'OrderSubCatView.java', 731, 'DEBUG', 'Inside Order Sub Category View', NULL, 'PO-AD18L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52928, 'Logs.java', 71, 'DEBUG', 'MF17_M_ORDCATMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52929, 'SchedulerView.java', 137, 'DEBUG', 'Initializing screen components behaviour', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52930, 'SchedulerView.java', 171, 'DEBUG', 'Closing file input stream', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52931, 'SchedulerView.java', 207, 'DEBUG', 'Closing file input stream', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52932, 'SchedulerView.java', 227, 'DEBUG', 'Setting component caption', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52933, 'SchedulerView.java', 647, 'DEBUG', 'Setting click listener for cancel button', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52934, 'SchedulerView.java', 650, 'DEBUG', 'Setting click listener for save button', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52935, 'Logs.java', 71, 'DEBUG', 'MF22_M_SCHEDULER records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52936, 'SchedulerView.java', 656, 'DEBUG', 'Getting all Order Category records', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52937, 'SchedulerView.java', 665, 'DEBUG', 'Wrapping Order Category record to container that can be understood by Grid', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52938, 'SchedulerView.java', 689, 'DEBUG', 'Hiding unwanted attributes', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52939, 'SchedulerView.java', 695, 'DEBUG', 'Filling grid with data', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52940, 'SchedulerView.java', 717, 'DEBUG', 'Adding "edit" column', NULL, 'com.parkson.poAdmin.master.SchedulerView');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52941, 'JobMasterView.java', 98, 'DEBUG', 'Initializing screen components behaviour', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52942, 'JobMasterView.java', 132, 'DEBUG', 'Closing file input stream', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52943, 'JobMasterView.java', 154, 'DEBUG', 'Setting component caption', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52944, 'JobMasterView.java', 159, 'DEBUG', 'Setting fields validator', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52945, 'JobMasterView.java', 170, 'DEBUG', 'Setting fields value change listener', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52946, 'JobMasterView.java', 175, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52947, 'JobMasterView.java', 198, 'DEBUG', 'Setting click listener for save button', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52948, 'Logs.java', 71, 'DEBUG', 'MF19_M_JOBMAST records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52949, 'JobMasterView.java', 203, 'DEBUG', 'Getting all job master records', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52950, 'JobMasterView.java', 211, 'DEBUG', 'Wrapping Job Master record to container that can be understood by Grid', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52951, 'JobMasterView.java', 226, 'DEBUG', 'Hiding unwanted attributes', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52952, 'JobMasterView.java', 233, 'DEBUG', 'Filling grid with data', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52953, 'JobMasterView.java', 265, 'DEBUG', 'Adding "edit" column', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52954, 'JobMasterView.java', 699, 'DEBUG', 'Entering Company Screen', NULL, 'PO-AD19L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52955, 'MasterDataView.java', 105, 'DEBUG', 'Initializing screen components behaviour', NULL, 'PO-AD06L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52956, 'MasterDataView.java', 142, 'DEBUG', 'Closing file input stream', NULL, 'PO-AD06L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52957, 'MasterDataView.java', 165, 'DEBUG', 'Setting component caption', NULL, 'PO-AD06L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52958, 'MasterDataView.java', 183, 'DEBUG', 'Setting fields value change listener', NULL, 'PO-AD06L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52959, 'MasterDataView.java', 187, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-AD06L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52960, 'MasterDataView.java', 212, 'DEBUG', 'Setting click listener for save button', NULL, 'PO-AD06L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52961, 'Logs.java', 71, 'DEBUG', 'MF06_R_MASTDATA records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52962, 'MasterDataView.java', 224, 'DEBUG', 'Getting all master data records', NULL, 'PO-AD06L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52963, 'MasterDataView.java', 232, 'DEBUG', 'Wrapping Job Master record to container that can be understood by Grid', NULL, 'PO-AD06L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52964, 'MasterDataView.java', 249, 'DEBUG', 'Hiding unwanted attributes', NULL, 'PO-AD06L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52965, 'MasterDataView.java', 256, 'DEBUG', 'Filling grid with data', NULL, 'PO-AD06L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52966, 'MasterDataView.java', 288, 'DEBUG', 'Adding "edit" column', NULL, 'PO-AD06L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52967, 'MasterDataView.java', 749, 'DEBUG', 'Entering Master Data Screen', NULL, 'PO-AD06L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52968, 'SystemParametersView.java', 103, 'DEBUG', 'Initializing screen components behaviour', NULL, 'PO-AD07L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52969, 'SystemParametersView.java', 139, 'DEBUG', 'Closing file input stream', NULL, 'PO-AD07L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52970, 'SystemParametersView.java', 161, 'DEBUG', 'Setting component caption', NULL, 'PO-AD07L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52971, 'SystemParametersView.java', 167, 'DEBUG', 'Setting fields validator', NULL, 'PO-AD07L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52972, 'SystemParametersView.java', 181, 'DEBUG', 'Setting fields value change listener', NULL, 'PO-AD07L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52973, 'SystemParametersView.java', 185, 'DEBUG', 'Setting click listener for cancel button', NULL, 'PO-AD07L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52974, 'SystemParametersView.java', 208, 'DEBUG', 'Setting click listener for save button', NULL, 'PO-AD07L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52975, 'Logs.java', 71, 'DEBUG', 'MF07_R_SYSPARAM records are succesfully fetched from database.', NULL, 'PO-UT02L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52976, 'SystemParametersView.java', 218, 'DEBUG', 'Getting all System Parameters records', NULL, 'PO-AD07L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52977, 'SystemParametersView.java', 226, 'DEBUG', 'Wrapping System Parameters record to container that can be understood by Grid', NULL, 'PO-AD07L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52978, 'SystemParametersView.java', 244, 'DEBUG', 'Hiding unwanted attributes', NULL, 'PO-AD07L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52979, 'SystemParametersView.java', 251, 'DEBUG', 'Filling grid with data', NULL, 'PO-AD07L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52980, 'SystemParametersView.java', 282, 'DEBUG', 'Adding "edit" column', NULL, 'PO-AD07L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52981, 'SystemParametersView.java', 731, 'DEBUG', 'Inside System Parameters View', NULL, 'PO-AD07L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52982, 'OrderCategoryView.java', 267, 'DEBUG', 'Closing file ouput stream', NULL, 'PO-AD17L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52983, 'CompanyView.java', 334, 'DEBUG', 'Closing file ouput stream', NULL, 'PO-AD11L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52984, 'CompanyView.java', 421, 'DEBUG', 'Closing file ouput stream', NULL, 'PO-AD11L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52985, 'CompanyView.java', 505, 'DEBUG', 'Closing file ouput stream', NULL, 'PO-AD11L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52986, 'CompanyView.java', 589, 'DEBUG', 'Closing file ouput stream', NULL, 'PO-AD11L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52987, 'OrderCategoryView.java', 267, 'DEBUG', 'Closing file ouput stream', NULL, 'PO-AD17L');
INSERT INTO "LD02_T_ACSLOG" ("ld02_logId", "ld02_classFileName", "ld02_lineNo", "ld02_logLevel", "ld02_logMessage", "ld02_logTime", "ld02_screenId") VALUES (52988, 'SchedulerView.java', 276, 'DEBUG', 'Closing file ouput stream', NULL, 'com.parkson.poAdmin.master.SchedulerView$2');


--
-- TOC entry 2418 (class 0 OID 87466)
-- Dependencies: 185
-- Data for Name: LD03_T_STGLOG; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO "LD03_T_STGLOG" ("ld03_logSetting", "ld03_logSettingId") VALUES (1, 2);


--
-- TOC entry 2420 (class 0 OID 87472)
-- Dependencies: 187
-- Data for Name: LD04_T_AUDIT; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO "LD04_T_AUDIT" ("ld04_auditId", ld04_username, ld04_message, ld04_operation, "ld04_createdOn") VALUES (195, 'superadmin', 'User superadmin with IP address 0:0:0:0:0:0:0:1 is signed in', 'Log In', '2018-04-03 17:38:09.147');
INSERT INTO "LD04_T_AUDIT" ("ld04_auditId", ld04_username, ld04_message, ld04_operation, "ld04_createdOn") VALUES (196, 'superadmin', 'User superadmin with IP address 0:0:0:0:0:0:0:1 is signed in', 'Log In', '2018-04-03 17:59:59.634');
INSERT INTO "LD04_T_AUDIT" ("ld04_auditId", ld04_username, ld04_message, ld04_operation, "ld04_createdOn") VALUES (197, 'superadmin', 'User superadmin with IP address 0:0:0:0:0:0:0:1 is signed in', 'Log In', '2018-04-04 11:11:47.457');
INSERT INTO "LD04_T_AUDIT" ("ld04_auditId", ld04_username, ld04_message, ld04_operation, "ld04_createdOn") VALUES (198, 'superadmin', 'New company is saved', 'Save', '2018-04-04 11:12:04.104');
INSERT INTO "LD04_T_AUDIT" ("ld04_auditId", ld04_username, ld04_message, ld04_operation, "ld04_createdOn") VALUES (199, 'superadmin', 'New Job Master is saved', 'Save', '2018-04-04 11:12:38.775');
INSERT INTO "LD04_T_AUDIT" ("ld04_auditId", ld04_username, ld04_message, ld04_operation, "ld04_createdOn") VALUES (200, 'superadmin', 'User superadmin with IP address 0:0:0:0:0:0:0:1 is signed in', 'Log In', '2018-04-04 11:14:55.452');
INSERT INTO "LD04_T_AUDIT" ("ld04_auditId", ld04_username, ld04_message, ld04_operation, "ld04_createdOn") VALUES (201, 'superadmin', 'User superadmin with IP address 0:0:0:0:0:0:0:1 is signed in', 'Log In', '2018-04-05 12:56:43.657');
INSERT INTO "LD04_T_AUDIT" ("ld04_auditId", ld04_username, ld04_message, ld04_operation, "ld04_createdOn") VALUES (202, 'superadmin', 'User superadmin with IP address 0:0:0:0:0:0:0:1 is signed in', 'Log In', '2018-04-05 12:57:48.429');
INSERT INTO "LD04_T_AUDIT" ("ld04_auditId", ld04_username, ld04_message, ld04_operation, "ld04_createdOn") VALUES (203, 'superadmin', 'User superadmin with IP address 0:0:0:0:0:0:0:1 is signed in', 'Log In', '2018-04-05 13:00:09.382');
INSERT INTO "LD04_T_AUDIT" ("ld04_auditId", ld04_username, ld04_message, ld04_operation, "ld04_createdOn") VALUES (204, 'superadmin', 'User superadmin with IP address 0:0:0:0:0:0:0:1 is signed in', 'Log In', '2018-04-05 13:02:15.52');
INSERT INTO "LD04_T_AUDIT" ("ld04_auditId", ld04_username, ld04_message, ld04_operation, "ld04_createdOn") VALUES (205, 'superadmin', 'User superadmin with IP address 0:0:0:0:0:0:0:1 is signed in', 'Log In', '2018-04-05 13:03:45.134');
INSERT INTO "LD04_T_AUDIT" ("ld04_auditId", ld04_username, ld04_message, ld04_operation, "ld04_createdOn") VALUES (206, 'superadmin', 'User superadmin with IP address 0:0:0:0:0:0:0:1 is signed in', 'Log In', '2018-04-05 16:17:17.748');
INSERT INTO "LD04_T_AUDIT" ("ld04_auditId", ld04_username, ld04_message, ld04_operation, "ld04_createdOn") VALUES (207, 'superadmin', 'User superadmin with IP address 0:0:0:0:0:0:0:1 is signed in', 'Log In', '2018-04-12 17:55:30.01');
INSERT INTO "LD04_T_AUDIT" ("ld04_auditId", ld04_username, ld04_message, ld04_operation, "ld04_createdOn") VALUES (208, 'superadmin', 'User superadmin with IP address 0:0:0:0:0:0:0:1 is signed in', 'Log In', '2018-04-23 20:41:44.78');


--
-- TOC entry 2421 (class 0 OID 87479)
-- Dependencies: 188
-- Data for Name: MF02_M_STRMAST; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO "MF02_M_STRMAST" ("mf02_storeCode", "mf02_storeAbbrName", "mf02_storeName", "mf11MCompmast_mf11_compCode", "mf02_createdBy", "mf02_createdOn", "mf02_lastModifiedBy", "mf02_lastModifiedOn", "mf02_deactivatedBy", "mf02_deactivatedOn", "mf02_reactivatedBy", "mf02_reactivatedOn", "mf02_isActive", "mf02_jsonField") VALUES ('1111', 'StoreAbbrName#2', 'StoreName#2', '081', 'superadmin', '2017-11-07 09:56:39.179', 'superadmin', '2018-05-01 18:31:32.163', 'superadmin', '2017-11-07 09:56:55.249', 'superadmin', '2017-11-07 10:20:02.506', true, NULL);
INSERT INTO "MF02_M_STRMAST" ("mf02_storeCode", "mf02_storeAbbrName", "mf02_storeName", "mf11MCompmast_mf11_compCode", "mf02_createdBy", "mf02_createdOn", "mf02_lastModifiedBy", "mf02_lastModifiedOn", "mf02_deactivatedBy", "mf02_deactivatedOn", "mf02_reactivatedBy", "mf02_reactivatedOn", "mf02_isActive", "mf02_jsonField") VALUES ('0125', 'WCKLCC', 'West Coast KLCC', '080', 'superadmin', '2017-09-23 14:27:14.404', 'superadmin', '2018-05-01 18:37:30.567', NULL, NULL, NULL, NULL, true, NULL);
INSERT INTO "MF02_M_STRMAST" ("mf02_storeCode", "mf02_storeAbbrName", "mf02_storeName", "mf11MCompmast_mf11_compCode", "mf02_createdBy", "mf02_createdOn", "mf02_lastModifiedBy", "mf02_lastModifiedOn", "mf02_deactivatedBy", "mf02_deactivatedOn", "mf02_reactivatedBy", "mf02_reactivatedOn", "mf02_isActive", "mf02_jsonField") VALUES ('0123', 'Park KLCCnew', 'Parkson KLCC', '080', 'superadmin', '2017-09-22 14:31:26.162', 'superadmin', '2018-05-01 23:57:35.691', NULL, NULL, NULL, NULL, true, NULL);
INSERT INTO "MF02_M_STRMAST" ("mf02_storeCode", "mf02_storeAbbrName", "mf02_storeName", "mf11MCompmast_mf11_compCode", "mf02_createdBy", "mf02_createdOn", "mf02_lastModifiedBy", "mf02_lastModifiedOn", "mf02_deactivatedBy", "mf02_deactivatedOn", "mf02_reactivatedBy", "mf02_reactivatedOn", "mf02_isActive", "mf02_jsonField") VALUES ('0126', 'ACKLCCnew', 'Angela Corp KLCC', '080', 'A_ROMI1', '2017-09-23 20:04:55.389', 'superadmin', '2018-05-01 23:57:48.964', NULL, NULL, NULL, NULL, true, NULL);
INSERT INTO "MF02_M_STRMAST" ("mf02_storeCode", "mf02_storeAbbrName", "mf02_storeName", "mf11MCompmast_mf11_compCode", "mf02_createdBy", "mf02_createdOn", "mf02_lastModifiedBy", "mf02_lastModifiedOn", "mf02_deactivatedBy", "mf02_deactivatedOn", "mf02_reactivatedBy", "mf02_reactivatedOn", "mf02_isActive", "mf02_jsonField") VALUES ('0195', 'kj', 'kjjj', '001', 'superadmin', '2018-05-02 17:02:44.604', 'superadmin', '2018-05-02 17:02:44.604', NULL, NULL, NULL, NULL, true, NULL);


--
-- TOC entry 2422 (class 0 OID 87485)
-- Dependencies: 189
-- Data for Name: MF06_R_MASTDATA; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO "MF06_R_MASTDATA" ("mf06_codeDesc", "mf06_codeType", "mf06_codeValue", "mf06_createdBy", "mf06_createdOn", "mf06_isActive", "mf06_lastModifiedBy", "mf06_lastModifiedOn", "mf06_sysName", "mf06_jsonField", "mf06_deactivatedBy", "mf06_reactivatedBy", "mf06_reactivatedOn", "mf06_deactivatedOn", "mf06_mastDataId") VALUES ('INNA', 'INNA', 'INNA', 'superadmin', '2017-10-23 11:13:30.175', false, 'superadmin', '2017-11-07 11:04:26.34', 'INNA', NULL, 'superadmin', 'superadmin', '2017-10-31 11:26:51.58+08', '2017-11-07 11:04:26.34+08', 9);
INSERT INTO "MF06_R_MASTDATA" ("mf06_codeDesc", "mf06_codeType", "mf06_codeValue", "mf06_createdBy", "mf06_createdOn", "mf06_isActive", "mf06_lastModifiedBy", "mf06_lastModifiedOn", "mf06_sysName", "mf06_jsonField", "mf06_deactivatedBy", "mf06_reactivatedBy", "mf06_reactivatedOn", "mf06_deactivatedOn", "mf06_mastDataId") VALUES ('ssdsd', 'sdsdsd', 'sdsdbhg', 'superadmin', '2017-10-16 15:37:15.012', false, 'superadmin', '2018-05-01 23:53:03.268', 'SNIKERS', NULL, 'superadmin', NULL, NULL, '2017-10-16 15:37:47.969+08', 6);
INSERT INTO "MF06_R_MASTDATA" ("mf06_codeDesc", "mf06_codeType", "mf06_codeValue", "mf06_createdBy", "mf06_createdOn", "mf06_isActive", "mf06_lastModifiedBy", "mf06_lastModifiedOn", "mf06_sysName", "mf06_jsonField", "mf06_deactivatedBy", "mf06_reactivatedBy", "mf06_reactivatedOn", "mf06_deactivatedOn", "mf06_mastDataId") VALUES ('INNA@gh', 'INNA@', 'INN@', 'superadmin', '2017-10-23 11:11:23.628', true, 'superadmin', '2018-05-01 23:53:19.571', '', NULL, NULL, NULL, NULL, NULL, 8);
INSERT INTO "MF06_R_MASTDATA" ("mf06_codeDesc", "mf06_codeType", "mf06_codeValue", "mf06_createdBy", "mf06_createdOn", "mf06_isActive", "mf06_lastModifiedBy", "mf06_lastModifiedOn", "mf06_sysName", "mf06_jsonField", "mf06_deactivatedBy", "mf06_reactivatedBy", "mf06_reactivatedOn", "mf06_deactivatedOn", "mf06_mastDataId") VALUES ('/././.', ';llllllllll', 'lklklklklk', 'superadmin', '2018-05-02 20:01:21.361', true, 'superadmin', '2018-05-02 20:01:21.361', 'llolol', NULL, NULL, NULL, NULL, NULL, 1);


--
-- TOC entry 2483 (class 0 OID 0)
-- Dependencies: 190
-- Name: MF06_R_MASTDATA_mf06_mastDataId_seq; Type: SEQUENCE SET; Schema: mfmaindb; Owner: postgres
--


--
-- TOC entry 2424 (class 0 OID 87493)
-- Dependencies: 191
-- Data for Name: MF07_R_SYSPARAM; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO "MF07_R_SYSPARAM" ("mf07_createdBy", "mf07_createdOn", mf07_desc, "mf07_isActive", "mf07_lastModifiedBy", "mf07_lastModifiedOn", "mf07_propName", "mf07_propValue", "mf07_sysName", "mf07_jsonField", "mf07_deactivatedBy", "mf07_reactivatedBy", "mf07_reactivatedOn", "mf07_deactivatedOn", "mf07_sysParamCode") VALUES ('superadmin', '2017-09-25 18:32:04.697', '', true, 'user123', '2017-09-25 18:32:04.697', 'MF_LDAPFULLURL', 'ldap://localhost:389/dc=parksondev,dc=com,dc=my', 'NTPO', NULL, NULL, NULL, NULL, NULL, 13);
INSERT INTO "MF07_R_SYSPARAM" ("mf07_createdBy", "mf07_createdOn", mf07_desc, "mf07_isActive", "mf07_lastModifiedBy", "mf07_lastModifiedOn", "mf07_propName", "mf07_propValue", "mf07_sysName", "mf07_jsonField", "mf07_deactivatedBy", "mf07_reactivatedBy", "mf07_reactivatedOn", "mf07_deactivatedOn", "mf07_sysParamCode") VALUES ('superadmin', '2017-09-25 18:34:36.318', '', true, 'user123', '2017-09-25 18:34:36.318', 'MF_LDAPURL', 'ldap://localhost:389', '', NULL, NULL, NULL, NULL, NULL, 12);
INSERT INTO "MF07_R_SYSPARAM" ("mf07_createdBy", "mf07_createdOn", mf07_desc, "mf07_isActive", "mf07_lastModifiedBy", "mf07_lastModifiedOn", "mf07_propName", "mf07_propValue", "mf07_sysName", "mf07_jsonField", "mf07_deactivatedBy", "mf07_reactivatedBy", "mf07_reactivatedOn", "mf07_deactivatedOn", "mf07_sysParamCode") VALUES ('superadmin', '2017-09-25 18:34:36.318', '', true, 'user123', '2017-09-25 18:34:36.318', 'MF_LDAPURL', 'ldap://localhost:389', '', NULL, NULL, NULL, NULL, NULL, 14);
INSERT INTO "MF07_R_SYSPARAM" ("mf07_createdBy", "mf07_createdOn", mf07_desc, "mf07_isActive", "mf07_lastModifiedBy", "mf07_lastModifiedOn", "mf07_propName", "mf07_propValue", "mf07_sysName", "mf07_jsonField", "mf07_deactivatedBy", "mf07_reactivatedBy", "mf07_reactivatedOn", "mf07_deactivatedOn", "mf07_sysParamCode") VALUES ('superadmin', '2017-10-23 11:14:30.135', 'INNA', true, 'user123', '2017-10-23 11:14:30.135', 'MF_LDAPBASE', 'dc=parksondev,dc=com,dc=my', 'NTPO', NULL, NULL, NULL, NULL, NULL, 15);
INSERT INTO "MF07_R_SYSPARAM" ("mf07_createdBy", "mf07_createdOn", mf07_desc, "mf07_isActive", "mf07_lastModifiedBy", "mf07_lastModifiedOn", "mf07_propName", "mf07_propValue", "mf07_sysName", "mf07_jsonField", "mf07_deactivatedBy", "mf07_reactivatedBy", "mf07_reactivatedOn", "mf07_deactivatedOn", "mf07_sysParamCode") VALUES ('superadmin', '2017-10-23 11:14:30.135', 'INNA', true, 'user123', '2017-10-23 11:14:30.135', 'MF_LDAPMGRDN', 'cn=admin,dc=parksondev,dc=com,dc=my', '', NULL, NULL, NULL, NULL, NULL, 16);
INSERT INTO "MF07_R_SYSPARAM" ("mf07_createdBy", "mf07_createdOn", mf07_desc, "mf07_isActive", "mf07_lastModifiedBy", "mf07_lastModifiedOn", "mf07_propName", "mf07_propValue", "mf07_sysName", "mf07_jsonField", "mf07_deactivatedBy", "mf07_reactivatedBy", "mf07_reactivatedOn", "mf07_deactivatedOn", "mf07_sysParamCode") VALUES ('superadmin', '2017-10-23 11:14:30.135', 'INNA', true, 'user123', '2017-10-23 11:14:30.135', 'MF_LDAPMGRPASS', 'siroi12admin', '', NULL, NULL, NULL, NULL, NULL, 17);
INSERT INTO "MF07_R_SYSPARAM" ("mf07_createdBy", "mf07_createdOn", mf07_desc, "mf07_isActive", "mf07_lastModifiedBy", "mf07_lastModifiedOn", "mf07_propName", "mf07_propValue", "mf07_sysName", "mf07_jsonField", "mf07_deactivatedBy", "mf07_reactivatedBy", "mf07_reactivatedOn", "mf07_deactivatedOn", "mf07_sysParamCode") VALUES ('superadmin', '2017-10-23 11:14:30.135', 'INNA', true, 'user123', '2017-10-23 11:14:30.135', 'FAILED_FOLDER_PATH', 'D:\\Failed Folder', 'NTPO', NULL, NULL, NULL, NULL, NULL, 24);
INSERT INTO "MF07_R_SYSPARAM" ("mf07_createdBy", "mf07_createdOn", mf07_desc, "mf07_isActive", "mf07_lastModifiedBy", "mf07_lastModifiedOn", "mf07_propName", "mf07_propValue", "mf07_sysName", "mf07_jsonField", "mf07_deactivatedBy", "mf07_reactivatedBy", "mf07_reactivatedOn", "mf07_deactivatedOn", "mf07_sysParamCode") VALUES ('superadmin', '2017-10-30 15:33:20.596', 'YJ', true, 'user123', '2017-10-30 15:33:20.596', 'SUCCESS_FOLDER_PATH', 'D:\\Success Folder', '', NULL, NULL, NULL, NULL, NULL, 25);
INSERT INTO "MF07_R_SYSPARAM" ("mf07_createdBy", "mf07_createdOn", mf07_desc, "mf07_isActive", "mf07_lastModifiedBy", "mf07_lastModifiedOn", "mf07_propName", "mf07_propValue", "mf07_sysName", "mf07_jsonField", "mf07_deactivatedBy", "mf07_reactivatedBy", "mf07_reactivatedOn", "mf07_deactivatedOn", "mf07_sysParamCode") VALUES ('superadmin', '2017-10-23 11:14:30.135', 'INNA', true, 'user123', '2017-10-23 11:14:30.135', 'LOG_FOLDER_PATH', 'D:\\Log Folder', 'NTPO', NULL, NULL, NULL, NULL, NULL, 26);
INSERT INTO "MF07_R_SYSPARAM" ("mf07_createdBy", "mf07_createdOn", mf07_desc, "mf07_isActive", "mf07_lastModifiedBy", "mf07_lastModifiedOn", "mf07_propName", "mf07_propValue", "mf07_sysName", "mf07_jsonField", "mf07_deactivatedBy", "mf07_reactivatedBy", "mf07_reactivatedOn", "mf07_deactivatedOn", "mf07_sysParamCode") VALUES ('superadmin', '2017-10-30 15:33:20.596', 'YJ', true, 'user123', '2017-10-30 15:33:20.596', 'BACKUP_FOLDER_PATH', 'D:\\', '', NULL, NULL, NULL, NULL, NULL, 28);
INSERT INTO "MF07_R_SYSPARAM" ("mf07_createdBy", "mf07_createdOn", mf07_desc, "mf07_isActive", "mf07_lastModifiedBy", "mf07_lastModifiedOn", "mf07_propName", "mf07_propValue", "mf07_sysName", "mf07_jsonField", "mf07_deactivatedBy", "mf07_reactivatedBy", "mf07_reactivatedOn", "mf07_deactivatedOn", "mf07_sysParamCode") VALUES ('superadmin', '2017-10-30 15:33:20.596', 'YJ', true, 'user123', '2017-10-30 15:33:20.596', 'PASSWORD_LIFETIME', '30', '', NULL, NULL, NULL, NULL, NULL, 29);
INSERT INTO "MF07_R_SYSPARAM" ("mf07_createdBy", "mf07_createdOn", mf07_desc, "mf07_isActive", "mf07_lastModifiedBy", "mf07_lastModifiedOn", "mf07_propName", "mf07_propValue", "mf07_sysName", "mf07_jsonField", "mf07_deactivatedBy", "mf07_reactivatedBy", "mf07_reactivatedOn", "mf07_deactivatedOn", "mf07_sysParamCode") VALUES ('superadmin', '2017-10-30 15:33:20.596', 'YJ', true, 'user123', '2017-10-30 15:33:20.596', 'PASSWORD_NOTIF', '2', '', NULL, NULL, NULL, NULL, NULL, 30);
INSERT INTO "MF07_R_SYSPARAM" ("mf07_createdBy", "mf07_createdOn", mf07_desc, "mf07_isActive", "mf07_lastModifiedBy", "mf07_lastModifiedOn", "mf07_propName", "mf07_propValue", "mf07_sysName", "mf07_jsonField", "mf07_deactivatedBy", "mf07_reactivatedBy", "mf07_reactivatedOn", "mf07_deactivatedOn", "mf07_sysParamCode") VALUES ('superadmin', '2017-10-30 15:33:20.596', 'YJ', true, 'user123', '2017-10-30 15:33:20.596', 'ACCT_NOTIF', '2', '', NULL, NULL, NULL, NULL, NULL, 32);
INSERT INTO "MF07_R_SYSPARAM" ("mf07_createdBy", "mf07_createdOn", mf07_desc, "mf07_isActive", "mf07_lastModifiedBy", "mf07_lastModifiedOn", "mf07_propName", "mf07_propValue", "mf07_sysName", "mf07_jsonField", "mf07_deactivatedBy", "mf07_reactivatedBy", "mf07_reactivatedOn", "mf07_deactivatedOn", "mf07_sysParamCode") VALUES ('superadmin', '2017-10-30 15:33:20.596', 'YJ', true, 'user123', '2017-10-30 15:33:20.596', 'M_DEFAULT_LOGO', 'img/logo.jpg', '', NULL, NULL, NULL, NULL, NULL, 35);
INSERT INTO "MF07_R_SYSPARAM" ("mf07_createdBy", "mf07_createdOn", mf07_desc, "mf07_isActive", "mf07_lastModifiedBy", "mf07_lastModifiedOn", "mf07_propName", "mf07_propValue", "mf07_sysName", "mf07_jsonField", "mf07_deactivatedBy", "mf07_reactivatedBy", "mf07_reactivatedOn", "mf07_deactivatedOn", "mf07_sysParamCode") VALUES ('superadmin', '2017-10-30 15:33:20.596', 'YJ', true, 'user123', '2017-10-30 15:33:20.596', 'MFPDF', 'D:\\', '', NULL, NULL, NULL, NULL, NULL, 36);
INSERT INTO "MF07_R_SYSPARAM" ("mf07_createdBy", "mf07_createdOn", mf07_desc, "mf07_isActive", "mf07_lastModifiedBy", "mf07_lastModifiedOn", "mf07_propName", "mf07_propValue", "mf07_sysName", "mf07_jsonField", "mf07_deactivatedBy", "mf07_reactivatedBy", "mf07_reactivatedOn", "mf07_deactivatedOn", "mf07_sysParamCode") VALUES ('superadmin', '2017-10-30 15:33:20.596', 'YJnewwww', true, 'superadmin', '2018-05-01 23:50:40.193', 'UPLOAD_FOLDER_PATH', 'D:\\My Folder', '', NULL, NULL, NULL, NULL, NULL, 27);
INSERT INTO "MF07_R_SYSPARAM" ("mf07_createdBy", "mf07_createdOn", mf07_desc, "mf07_isActive", "mf07_lastModifiedBy", "mf07_lastModifiedOn", "mf07_propName", "mf07_propValue", "mf07_sysName", "mf07_jsonField", "mf07_deactivatedBy", "mf07_reactivatedBy", "mf07_reactivatedOn", "mf07_deactivatedOn", "mf07_sysParamCode") VALUES ('superadmin', '2017-10-30 15:33:20.596', 'YJ', true, 'superadmin', '2018-05-01 23:59:31.186', 'ACCT_LIFETIME', '30', 'new', NULL, NULL, NULL, NULL, NULL, 31);


--
-- TOC entry 2484 (class 0 OID 0)
-- Dependencies: 192
-- Name: MF07_R_SYSPARAM_mf07_sysParamId_seq; Type: SEQUENCE SET; Schema: mfmaindb; Owner: postgres
--



--
-- TOC entry 2426 (class 0 OID 87501)
-- Dependencies: 193
-- Data for Name: MF11_M_COMPMAST; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO "MF11_M_COMPMAST" ("mf11_compCode", "mf11_compName", "mf11_compRegNo", "mf11_createdBy", "mf11_createdOn", "mf11_isActive", "mf11_lastModifiedBy", "mf11_lastModifiedOn", "mf11_compCodeHRIS", "mf11_compAbbrName", "mf11_compLogo", "mf11_companyLetterHeading", "mf11_deactivatedBy", "mf11_deactivatedOn", "mf11_reactivatedBy", "mf11_reactivatedOn", "mf11_jsonField", "mf11_poTAndCPortrait", "mf11_poTAndCLandscape") VALUES ('001', 'Twitterwire', '71-116-7188', 'Ps0031', '2017-05-14 17:05:25', true, 'Ps0059', '2018-01-29 09:44:08', '001', 'nisl', NULL, NULL, 'Ps0062', '2017-07-10 23:46:12', 'Ps0045', '2018-01-18 07:59:56', NULL, NULL, NULL);
INSERT INTO "MF11_M_COMPMAST" ("mf11_compCode", "mf11_compName", "mf11_compRegNo", "mf11_createdBy", "mf11_createdOn", "mf11_isActive", "mf11_lastModifiedBy", "mf11_lastModifiedOn", "mf11_compCodeHRIS", "mf11_compAbbrName", "mf11_compLogo", "mf11_companyLetterHeading", "mf11_deactivatedBy", "mf11_deactivatedOn", "mf11_reactivatedBy", "mf11_reactivatedOn", "mf11_jsonField", "mf11_poTAndCPortrait", "mf11_poTAndCLandscape") VALUES ('002', 'Shuffledrive', '82-977-5651', 'Ps0051', '2017-07-21 19:39:10', false, 'Ps0083', '2017-04-11 22:16:41', '002', 'mattis egestas', NULL, NULL, 'Ps0044', '2017-04-14 01:07:23', 'Ps0059', '2017-12-20 05:00:10', NULL, NULL, NULL);
INSERT INTO "MF11_M_COMPMAST" ("mf11_compCode", "mf11_compName", "mf11_compRegNo", "mf11_createdBy", "mf11_createdOn", "mf11_isActive", "mf11_lastModifiedBy", "mf11_lastModifiedOn", "mf11_compCodeHRIS", "mf11_compAbbrName", "mf11_compLogo", "mf11_companyLetterHeading", "mf11_deactivatedBy", "mf11_deactivatedOn", "mf11_reactivatedBy", "mf11_reactivatedOn", "mf11_jsonField", "mf11_poTAndCPortrait", "mf11_poTAndCLandscape") VALUES ('010', 'Browsecat', '35-459-9781', 'Ps0048', '2017-08-17 22:54:30', false, 'Ps0008', '2017-07-16 05:22:36', '010', 'dapibus', NULL, NULL, 'Ps0010', '2017-08-28 17:37:02', 'Ps0045', '2017-09-05 23:21:07', NULL, NULL, NULL);
INSERT INTO "MF11_M_COMPMAST" ("mf11_compCode", "mf11_compName", "mf11_compRegNo", "mf11_createdBy", "mf11_createdOn", "mf11_isActive", "mf11_lastModifiedBy", "mf11_lastModifiedOn", "mf11_compCodeHRIS", "mf11_compAbbrName", "mf11_compLogo", "mf11_companyLetterHeading", "mf11_deactivatedBy", "mf11_deactivatedOn", "mf11_reactivatedBy", "mf11_reactivatedOn", "mf11_jsonField", "mf11_poTAndCPortrait", "mf11_poTAndCLandscape") VALUES ('098', 'Zooxo', '58-128-9656', 'Ps0023', '2017-08-16 13:47:34', true, 'Ps0039', '2017-06-14 02:06:04', '098', 'ligula in', NULL, NULL, 'Ps0038', '2018-03-18 00:37:06', 'Ps0079', '2017-08-01 13:04:59', NULL, NULL, NULL);
INSERT INTO "MF11_M_COMPMAST" ("mf11_compCode", "mf11_compName", "mf11_compRegNo", "mf11_createdBy", "mf11_createdOn", "mf11_isActive", "mf11_lastModifiedBy", "mf11_lastModifiedOn", "mf11_compCodeHRIS", "mf11_compAbbrName", "mf11_compLogo", "mf11_companyLetterHeading", "mf11_deactivatedBy", "mf11_deactivatedOn", "mf11_reactivatedBy", "mf11_reactivatedOn", "mf11_jsonField", "mf11_poTAndCPortrait", "mf11_poTAndCLandscape") VALUES ('101', 'Yoveo', '33-849-1782', 'Ps0068', '2017-08-31 18:41:25', false, 'Ps0057', '2017-06-03 09:55:39', '101', 'pede ac', NULL, NULL, 'Ps0008', '2017-04-07 19:43:52', 'Ps0036', '2017-07-07 20:19:40', NULL, NULL, NULL);
INSERT INTO "MF11_M_COMPMAST" ("mf11_compCode", "mf11_compName", "mf11_compRegNo", "mf11_createdBy", "mf11_createdOn", "mf11_isActive", "mf11_lastModifiedBy", "mf11_lastModifiedOn", "mf11_compCodeHRIS", "mf11_compAbbrName", "mf11_compLogo", "mf11_companyLetterHeading", "mf11_deactivatedBy", "mf11_deactivatedOn", "mf11_reactivatedBy", "mf11_reactivatedOn", "mf11_jsonField", "mf11_poTAndCPortrait", "mf11_poTAndCLandscape") VALUES ('898', 'Abata', '14-841-4208', 'Ps0050', '2018-01-20 10:05:38', false, 'Ps0019', '2017-08-13 09:07:07', '898', 'praesent', NULL, NULL, 'Ps0081', '2017-11-18 20:19:11', 'Ps0002', '2017-09-01 04:23:37', NULL, NULL, NULL);
INSERT INTO "MF11_M_COMPMAST" ("mf11_compCode", "mf11_compName", "mf11_compRegNo", "mf11_createdBy", "mf11_createdOn", "mf11_isActive", "mf11_lastModifiedBy", "mf11_lastModifiedOn", "mf11_compCodeHRIS", "mf11_compAbbrName", "mf11_compLogo", "mf11_companyLetterHeading", "mf11_deactivatedBy", "mf11_deactivatedOn", "mf11_reactivatedBy", "mf11_reactivatedOn", "mf11_jsonField", "mf11_poTAndCPortrait", "mf11_poTAndCLandscape") VALUES ('082', 'Kwimbee', '66-929-7232', 'Ps0002', '2017-06-04 03:44:44', true, 'Ps0062', '2017-08-15 16:52:56', '082', 'sapien placerat', NULL, NULL, 'Ps0033', '2017-12-21 19:33:12', 'Ps0035', '2017-12-13 00:35:13', NULL, NULL, NULL);
INSERT INTO "MF11_M_COMPMAST" ("mf11_compCode", "mf11_compName", "mf11_compRegNo", "mf11_createdBy", "mf11_createdOn", "mf11_isActive", "mf11_lastModifiedBy", "mf11_lastModifiedOn", "mf11_compCodeHRIS", "mf11_compAbbrName", "mf11_compLogo", "mf11_companyLetterHeading", "mf11_deactivatedBy", "mf11_deactivatedOn", "mf11_reactivatedBy", "mf11_reactivatedOn", "mf11_jsonField", "mf11_poTAndCPortrait", "mf11_poTAndCLandscape") VALUES ('081', 'Geba', '25-873-2523', 'Ps0018', '2017-04-30 20:24:55', true, 'Ps0080', '2017-04-11 07:51:10', '081', 'vehicula', NULL, NULL, 'Ps0063', '2017-03-31 11:44:06', 'Ps0077', '2018-01-22 06:46:01', NULL, NULL, NULL);
INSERT INTO "MF11_M_COMPMAST" ("mf11_compCode", "mf11_compName", "mf11_compRegNo", "mf11_createdBy", "mf11_createdOn", "mf11_isActive", "mf11_lastModifiedBy", "mf11_lastModifiedOn", "mf11_compCodeHRIS", "mf11_compAbbrName", "mf11_compLogo", "mf11_companyLetterHeading", "mf11_deactivatedBy", "mf11_deactivatedOn", "mf11_reactivatedBy", "mf11_reactivatedOn", "mf11_jsonField", "mf11_poTAndCPortrait", "mf11_poTAndCLandscape") VALUES ('020', 'Myntenew', '21-146-1579', 'superadmin', '2018-05-01 21:59:15.102', false, 'superadmin', '2018-05-01 21:59:15.102', '020', 'quam', NULL, NULL, 'Ps0098', '2018-03-01 23:39:11', 'Ps0096', '2017-07-30 08:45:45', NULL, NULL, NULL);
INSERT INTO "MF11_M_COMPMAST" ("mf11_compCode", "mf11_compName", "mf11_compRegNo", "mf11_createdBy", "mf11_createdOn", "mf11_isActive", "mf11_lastModifiedBy", "mf11_lastModifiedOn", "mf11_compCodeHRIS", "mf11_compAbbrName", "mf11_compLogo", "mf11_companyLetterHeading", "mf11_deactivatedBy", "mf11_deactivatedOn", "mf11_reactivatedBy", "mf11_reactivatedOn", "mf11_jsonField", "mf11_poTAndCPortrait", "mf11_poTAndCLandscape") VALUES ('069', 'Rooxo', '00-769-6133', 'superadmin', '2018-05-01 23:58:11.429', true, 'superadmin', '2018-05-01 23:58:11.429', '069', 'vestibulumnew', NULL, NULL, 'Ps0001', '2018-01-19 03:30:51', 'Ps0059', '2017-05-14 22:59:29', NULL, NULL, NULL);
INSERT INTO "MF11_M_COMPMAST" ("mf11_compCode", "mf11_compName", "mf11_compRegNo", "mf11_createdBy", "mf11_createdOn", "mf11_isActive", "mf11_lastModifiedBy", "mf11_lastModifiedOn", "mf11_compCodeHRIS", "mf11_compAbbrName", "mf11_compLogo", "mf11_companyLetterHeading", "mf11_deactivatedBy", "mf11_deactivatedOn", "mf11_reactivatedBy", "mf11_reactivatedOn", "mf11_jsonField", "mf11_poTAndCPortrait", "mf11_poTAndCLandscape") VALUES ('123', 'Skiveenew', '08-658-1845', 'superadmin', '2018-05-02 00:19:49.022', true, 'superadmin', '2018-05-02 00:19:49.022', '123', 'aenean sit', NULL, NULL, 'Ps0049', '2017-06-29 01:30:54', 'Ps0007', '2017-12-21 00:25:06', NULL, NULL, NULL);
INSERT INTO "MF11_M_COMPMAST" ("mf11_compCode", "mf11_compName", "mf11_compRegNo", "mf11_createdBy", "mf11_createdOn", "mf11_isActive", "mf11_lastModifiedBy", "mf11_lastModifiedOn", "mf11_compCodeHRIS", "mf11_compAbbrName", "mf11_compLogo", "mf11_companyLetterHeading", "mf11_deactivatedBy", "mf11_deactivatedOn", "mf11_reactivatedBy", "mf11_reactivatedOn", "mf11_jsonField", "mf11_poTAndCPortrait", "mf11_poTAndCLandscape") VALUES ('080', 'Agivu', '59-653-6395', 'Ps0072', '2017-12-28 08:52:12', true, 'Ps0057', '2018-03-12 18:40:21', '080', 'erat', NULL, NULL, 'Ps0018', '2017-07-30 17:24:55', 'Ps0086', '2017-05-20 09:24:46', NULL, NULL, NULL);


--
-- TOC entry 2427 (class 0 OID 87507)
-- Dependencies: 194
-- Data for Name: MF12_M_EMPMAST; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO "MF12_M_EMPMAST" ("mf12_psId", "mf12_altId", "mf12_badgeNo", "mf12_confirmDt", "mf12_createdBy", "mf12_createdOn", "mf12_deptDescHRIS", "mf12_deptCodeHRIS", mf12_dob, mf12_email, "mf12_empCat", "mf12_empName", mf12_ethnic, mf12_ext, "mf12_foreignLocal", "mf12_frmCompCode", "mf12_frmLoc", mf12_gender, "mf12_hireDt", "mf12_hrStatus", "mf12_isActive", "mf12_lastModifiedBy", "mf12_lastModifiedOn", mf12_loc, "mf12_locDesc", "mf12_lstDtWrk", "mf12_lstUpdDtTm", "mf12_maritalStat", "mf12_newICNo", "mf12_oldICNo", "mf12_passportNo", "mf12_payGrp", "mf12_payrollComp", "mf12_payrollStatus", "mf12_phoneNo", "mf12_posNo", "mf12_posTitle", "mf12_prcDtTm", "mf12_prefName", "mf12_rptToEmpId", "mf12_rptToPosNo", "mf12_salaryGrade", "mf12_salaryPlan", "mf12_toCompCode", "mf12_toLoc", "mf12_trfDt", "mf12_trfSndFlag", "mf12_jsonField", "mf12_expJobEndDt", "mf11MCompmast_mf11_compCodeHRIS", "mf13MPcmast_mf13_pcCode", "mf19MJobmast_mf19_jobCode", "mf12_fromCostChargeCompany", "mf12_fromProfitCentre", "mf12_toCostChargeCompany", "mf12_toProfitCentre", mf12_isactive) VALUES ('0002049', ' 524636', 'P052463', '2015-10-02', 'admin', '2017-07-04 15:16:07.014', 'PCS0000234', 'P0231-040 ', '1959-01-11', 'f@g.com', 'R', 'E Geak', 'CHINSE', NULL, 'L', NULL, NULL, 'F', '2015-04-02', 'A', true, 'admin', '2017-07-04 15:16:07.014', 'P0231', 'Parkson Mahkota Parade', '2019-07-03', '2017-07-03 00:00:00', NULL, '590111045096', NULL, NULL, 'PSNONEXC', 'PCS', 'A', '4563563456', '00001362', 'Senior Cashier', '2017-07-03 00:00:00', NULL, '0002263', '00001360', 'E01', 'BE01', NULL, NULL, '2017-07-03', NULL, 'null', NULL, '001', '0123001', 'ACCNT', NULL, NULL, NULL, NULL, NULL);
INSERT INTO "MF12_M_EMPMAST" ("mf12_psId", "mf12_altId", "mf12_badgeNo", "mf12_confirmDt", "mf12_createdBy", "mf12_createdOn", "mf12_deptDescHRIS", "mf12_deptCodeHRIS", mf12_dob, mf12_email, "mf12_empCat", "mf12_empName", mf12_ethnic, mf12_ext, "mf12_foreignLocal", "mf12_frmCompCode", "mf12_frmLoc", mf12_gender, "mf12_hireDt", "mf12_hrStatus", "mf12_isActive", "mf12_lastModifiedBy", "mf12_lastModifiedOn", mf12_loc, "mf12_locDesc", "mf12_lstDtWrk", "mf12_lstUpdDtTm", "mf12_maritalStat", "mf12_newICNo", "mf12_oldICNo", "mf12_passportNo", "mf12_payGrp", "mf12_payrollComp", "mf12_payrollStatus", "mf12_phoneNo", "mf12_posNo", "mf12_posTitle", "mf12_prcDtTm", "mf12_prefName", "mf12_rptToEmpId", "mf12_rptToPosNo", "mf12_salaryGrade", "mf12_salaryPlan", "mf12_toCompCode", "mf12_toLoc", "mf12_trfDt", "mf12_trfSndFlag", "mf12_jsonField", "mf12_expJobEndDt", "mf11MCompmast_mf11_compCodeHRIS", "mf13MPcmast_mf13_pcCode", "mf19MJobmast_mf19_jobCode", "mf12_fromCostChargeCompany", "mf12_fromProfitCentre", "mf12_toCostChargeCompany", "mf12_toProfitCentre", mf12_isactive) VALUES ('0002039', ' 524636', 'P052463', '2015-10-02', 'admin', '2017-07-04 15:16:07.014', 'PCS0000234', 'P0231-040 ', '1959-01-11', 'a@f.com', 'R', 'E Geak', 'CHINSE', NULL, 'L', NULL, NULL, 'F', '2015-04-02', 'A', true, 'admin', '2017-07-04 15:16:07.014', 'P0231', 'Parkson Mahkota Parade', '2019-07-03', '2017-07-03 00:00:00', NULL, '590111045096', NULL, NULL, 'PSNONEXC', 'PCS', 'A', '574674687478', '00001362', 'Senior Cashier', '2017-07-03 00:00:00', NULL, '0002263', '00001360', 'E01', 'BE01', NULL, NULL, '2017-07-03', NULL, 'null', NULL, '002', '0125002', 'INNA2', NULL, NULL, NULL, NULL, NULL);
INSERT INTO "MF12_M_EMPMAST" ("mf12_psId", "mf12_altId", "mf12_badgeNo", "mf12_confirmDt", "mf12_createdBy", "mf12_createdOn", "mf12_deptDescHRIS", "mf12_deptCodeHRIS", mf12_dob, mf12_email, "mf12_empCat", "mf12_empName", mf12_ethnic, mf12_ext, "mf12_foreignLocal", "mf12_frmCompCode", "mf12_frmLoc", mf12_gender, "mf12_hireDt", "mf12_hrStatus", "mf12_isActive", "mf12_lastModifiedBy", "mf12_lastModifiedOn", mf12_loc, "mf12_locDesc", "mf12_lstDtWrk", "mf12_lstUpdDtTm", "mf12_maritalStat", "mf12_newICNo", "mf12_oldICNo", "mf12_passportNo", "mf12_payGrp", "mf12_payrollComp", "mf12_payrollStatus", "mf12_phoneNo", "mf12_posNo", "mf12_posTitle", "mf12_prcDtTm", "mf12_prefName", "mf12_rptToEmpId", "mf12_rptToPosNo", "mf12_salaryGrade", "mf12_salaryPlan", "mf12_toCompCode", "mf12_toLoc", "mf12_trfDt", "mf12_trfSndFlag", "mf12_jsonField", "mf12_expJobEndDt", "mf11MCompmast_mf11_compCodeHRIS", "mf13MPcmast_mf13_pcCode", "mf19MJobmast_mf19_jobCode", "mf12_fromCostChargeCompany", "mf12_fromProfitCentre", "mf12_toCostChargeCompany", "mf12_toProfitCentre", mf12_isactive) VALUES ('0002040', ' 524636', 'P052463', '2015-10-02', 'admin', '2017-07-04 15:16:07.014', 'PCS0000234', 'P0231-040 ', '1959-01-11', 'a@gmail.com', 'R', 'E Geak', 'CHINSE', NULL, 'L', NULL, NULL, 'F', '2015-04-02', 'A', true, 'admin', '2017-07-04 15:16:07.014', 'P0231', 'Parkson Mahkota Parade', '2019-07-03', '2017-07-03 00:00:00', NULL, '590111045096', NULL, NULL, 'PSNONEXC', 'PCS', 'A', '111111111111111', '00001362', 'Senior Cashier', '2017-07-03 00:00:00', NULL, '0002263', '00001360', 'E01', 'BE01', NULL, NULL, '2017-07-03', NULL, 'null', NULL, '001', '0125002', 'INNA2', NULL, NULL, NULL, NULL, NULL);
INSERT INTO "MF12_M_EMPMAST" ("mf12_psId", "mf12_altId", "mf12_badgeNo", "mf12_confirmDt", "mf12_createdBy", "mf12_createdOn", "mf12_deptDescHRIS", "mf12_deptCodeHRIS", mf12_dob, mf12_email, "mf12_empCat", "mf12_empName", mf12_ethnic, mf12_ext, "mf12_foreignLocal", "mf12_frmCompCode", "mf12_frmLoc", mf12_gender, "mf12_hireDt", "mf12_hrStatus", "mf12_isActive", "mf12_lastModifiedBy", "mf12_lastModifiedOn", mf12_loc, "mf12_locDesc", "mf12_lstDtWrk", "mf12_lstUpdDtTm", "mf12_maritalStat", "mf12_newICNo", "mf12_oldICNo", "mf12_passportNo", "mf12_payGrp", "mf12_payrollComp", "mf12_payrollStatus", "mf12_phoneNo", "mf12_posNo", "mf12_posTitle", "mf12_prcDtTm", "mf12_prefName", "mf12_rptToEmpId", "mf12_rptToPosNo", "mf12_salaryGrade", "mf12_salaryPlan", "mf12_toCompCode", "mf12_toLoc", "mf12_trfDt", "mf12_trfSndFlag", "mf12_jsonField", "mf12_expJobEndDt", "mf11MCompmast_mf11_compCodeHRIS", "mf13MPcmast_mf13_pcCode", "mf19MJobmast_mf19_jobCode", "mf12_fromCostChargeCompany", "mf12_fromProfitCentre", "mf12_toCostChargeCompany", "mf12_toProfitCentre", mf12_isactive) VALUES ('0002041', ' 524636', 'P052463', '2015-10-02', 'admin', '2017-07-04 15:16:07.014', 'PCS0000234', 'P0231-040 ', '1959-01-11', 'a@h.com', 'R', 'E Geak', 'CHINSE', NULL, 'L', NULL, NULL, 'F', '2015-04-02', 'A', true, 'admin', '2017-07-04 15:16:07.014', 'P0231', 'Parkson Mahkota Parade', '2019-07-03', '2017-07-03 00:00:00', NULL, '590111045096', NULL, NULL, 'PSNONEXC', 'PCS', 'A', '78787878', '00001362', 'Senior Cashier', '2017-07-03 00:00:00', NULL, '0002263', '00001360', 'E01', 'BE01', NULL, NULL, '2017-07-03', NULL, 'null', NULL, '002', '0126004', 'INNA2', NULL, NULL, NULL, NULL, NULL);


--
-- TOC entry 2428 (class 0 OID 87513)
-- Dependencies: 195
-- Data for Name: MF13_M_PCMAST; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO "MF13_M_PCMAST" ("mf13_createdBy", "mf13_createdOn", "mf13_lastModifiedBy", "mf13_lastModifiedOn", "mf13_pcCode", "mf13_pcName", "mf13_isActive", "mf13_addressLine1", "mf13_addressLine2", "mf13_addressLine3", "mf13_postCode", "mf13_postArea", "mf13_pcHead", "mf13_pcViewRights", "mf13_deactivatedBy", "mf13_deactivatedOn", "mf13_reactivatedBy", "mf13_reactivatedOn", "mf13_jsonField", "mf11MCompmast_mf11_compCode", "mf02MStrmast_mf02_storeCode", "mf14MDeptmast_mf14_deptCode", "mf13_pcAbbrName", "mf13_storeType", "mf13_startDate", "mf13_endDate") VALUES ('superadmin', '2017-10-24 09:49:56.137', 'superadmin', '2017-10-24 10:47:09.071', '0123001', 'Inna Profit Center 2', true, 'Endah2', 'Promenade2', 'C92', '570002', 'Sri Petaling2', ']', ']', 'superadmin', '2017-10-24 09:50:07.505', NULL, NULL, NULL, '080', '0126', '444', 'Inna Prof Cent2', 'S', '2017-10-24', '2017-10-31');
INSERT INTO "MF13_M_PCMAST" ("mf13_createdBy", "mf13_createdOn", "mf13_lastModifiedBy", "mf13_lastModifiedOn", "mf13_pcCode", "mf13_pcName", "mf13_isActive", "mf13_addressLine1", "mf13_addressLine2", "mf13_addressLine3", "mf13_postCode", "mf13_postArea", "mf13_pcHead", "mf13_pcViewRights", "mf13_deactivatedBy", "mf13_deactivatedOn", "mf13_reactivatedBy", "mf13_reactivatedOn", "mf13_jsonField", "mf11MCompmast_mf11_compCode", "mf02MStrmast_mf02_storeCode", "mf14MDeptmast_mf14_deptCode", "mf13_pcAbbrName", "mf13_storeType", "mf13_startDate", "mf13_endDate") VALUES ('superadmin', '2017-11-07 10:18:32.697', 'superadmin', '2017-11-07 10:18:32.697', '0125002', 'ProfitCenterName#1', true, 'Profit Center #1 Address Line 1', 'Profit Center #1 Address Line 2', 'Profit Center #1 Address Line 3', 'PC1PosCode', 'Profit Center #1 Post Area', ']', ']', NULL, NULL, NULL, NULL, NULL, '080', '0125', '101', 'ProfCentAbrNa#1', 'W', '2017-11-07', '2017-11-28');
INSERT INTO "MF13_M_PCMAST" ("mf13_createdBy", "mf13_createdOn", "mf13_lastModifiedBy", "mf13_lastModifiedOn", "mf13_pcCode", "mf13_pcName", "mf13_isActive", "mf13_addressLine1", "mf13_addressLine2", "mf13_addressLine3", "mf13_postCode", "mf13_postArea", "mf13_pcHead", "mf13_pcViewRights", "mf13_deactivatedBy", "mf13_deactivatedOn", "mf13_reactivatedBy", "mf13_reactivatedOn", "mf13_jsonField", "mf11MCompmast_mf11_compCode", "mf02MStrmast_mf02_storeCode", "mf14MDeptmast_mf14_deptCode", "mf13_pcAbbrName", "mf13_storeType", "mf13_startDate", "mf13_endDate") VALUES ('superadmin', '2017-11-07 10:24:11.082', 'superadmin', '2017-11-07 10:24:39.278', '0126009', 'ProfitCenterName#2', true, 'Profit Center #2 Address Line 1', 'Profit Center #2 Address Line 2', 'Profit Center #2 Address Line 3', 'PC2PosCode', 'Profit Center #2 Post Area', ']', ']', 'superadmin', '2017-11-07 10:24:39.278', NULL, NULL, NULL, '081', '0123', '101', 'PforCentAbrNa#2', 'S', '2017-11-07', '2017-11-14');
INSERT INTO "MF13_M_PCMAST" ("mf13_createdBy", "mf13_createdOn", "mf13_lastModifiedBy", "mf13_lastModifiedOn", "mf13_pcCode", "mf13_pcName", "mf13_isActive", "mf13_addressLine1", "mf13_addressLine2", "mf13_addressLine3", "mf13_postCode", "mf13_postArea", "mf13_pcHead", "mf13_pcViewRights", "mf13_deactivatedBy", "mf13_deactivatedOn", "mf13_reactivatedBy", "mf13_reactivatedOn", "mf13_jsonField", "mf11MCompmast_mf11_compCode", "mf02MStrmast_mf02_storeCode", "mf14MDeptmast_mf14_deptCode", "mf13_pcAbbrName", "mf13_storeType", "mf13_startDate", "mf13_endDate") VALUES ('superadmin', '2018-05-01 19:36:30.899', 'superadmin', '2018-05-02 16:59:02.43', '1111444', 'fg', true, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '081', '1111', '444', 'fg', 'H', NULL, NULL);
INSERT INTO "MF13_M_PCMAST" ("mf13_createdBy", "mf13_createdOn", "mf13_lastModifiedBy", "mf13_lastModifiedOn", "mf13_pcCode", "mf13_pcName", "mf13_isActive", "mf13_addressLine1", "mf13_addressLine2", "mf13_addressLine3", "mf13_postCode", "mf13_postArea", "mf13_pcHead", "mf13_pcViewRights", "mf13_deactivatedBy", "mf13_deactivatedOn", "mf13_reactivatedBy", "mf13_reactivatedOn", "mf13_jsonField", "mf11MCompmast_mf11_compCode", "mf02MStrmast_mf02_storeCode", "mf14MDeptmast_mf14_deptCode", "mf13_pcAbbrName", "mf13_storeType", "mf13_startDate", "mf13_endDate") VALUES ('superadmin', '2017-11-07 10:24:11.082', 'superadmin', '2018-05-03 01:23:38.287', '0126004', 'ProfitCenterName#2', true, 'Profit Center #2 Address Line 1', 'Profit Center #2 Address Line 2', 'Profit Center #2 Address Line 3', 'PC2PosCode', 'Profit Center #2 Post Area', ']', ']', 'superadmin', '2017-11-07 10:24:39.278', NULL, NULL, NULL, '080', '0123', '009', 'PforCentAbrNa#2', 'S', '2017-11-07', '2017-11-14');


--
-- TOC entry 2429 (class 0 OID 87519)
-- Dependencies: 196
-- Data for Name: MF14_M_DEPTMAST; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO "MF14_M_DEPTMAST" ("mf14_deptCode", "mf14_deptName", "mf14_createdBy", "mf14_createdOn", "mf14_lastModifiedBy", "mf14_lastModifiedOn", "mf14_deactivatedBy", "mf14_deactivatedOn", "mf14_reactivatedBy", "mf14_reactivatedOn", "mf14_isActive", "mf14_jsonField", "mf14_deptAbbrName") VALUES ('014', 'sdafsd', 'superadmin', '2017-09-23 17:44:44.325', 'superadmin', '2017-09-23 17:44:44.325', NULL, NULL, NULL, NULL, true, NULL, '34');
INSERT INTO "MF14_M_DEPTMAST" ("mf14_deptCode", "mf14_deptName", "mf14_createdBy", "mf14_createdOn", "mf14_lastModifiedBy", "mf14_lastModifiedOn", "mf14_deactivatedBy", "mf14_deactivatedOn", "mf14_reactivatedBy", "mf14_reactivatedOn", "mf14_isActive", "mf14_jsonField", "mf14_deptAbbrName") VALUES ('003', 'IT Department', 'A_ROMI1', '2017-09-23 20:06:23.778', 'A_ROMI1', '2017-09-23 20:06:23.778', NULL, NULL, NULL, NULL, true, NULL, '01');
INSERT INTO "MF14_M_DEPTMAST" ("mf14_deptCode", "mf14_deptName", "mf14_createdBy", "mf14_createdOn", "mf14_lastModifiedBy", "mf14_lastModifiedOn", "mf14_deactivatedBy", "mf14_deactivatedOn", "mf14_reactivatedBy", "mf14_reactivatedOn", "mf14_isActive", "mf14_jsonField", "mf14_deptAbbrName") VALUES ('004', 'SPECIAL COST CENTRE', 'A_ROMI1', '2017-09-23 20:07:24.346', 'superadmin', '2018-04-05 16:17:40.919861', NULL, NULL, NULL, NULL, true, NULL, 'we');
INSERT INTO "MF14_M_DEPTMAST" ("mf14_deptCode", "mf14_deptName", "mf14_createdBy", "mf14_createdOn", "mf14_lastModifiedBy", "mf14_lastModifiedOn", "mf14_deactivatedBy", "mf14_deactivatedOn", "mf14_reactivatedBy", "mf14_reactivatedOn", "mf14_isActive", "mf14_jsonField", "mf14_deptAbbrName") VALUES ('009', 'INTERNAL AUDIT', 'superadmin', '2018-04-05 13:00:22.696754', 'superadmin', '2018-04-05 16:17:40.941862', NULL, NULL, NULL, NULL, true, NULL, 'IN');
INSERT INTO "MF14_M_DEPTMAST" ("mf14_deptCode", "mf14_deptName", "mf14_createdBy", "mf14_createdOn", "mf14_lastModifiedBy", "mf14_lastModifiedOn", "mf14_deactivatedBy", "mf14_deactivatedOn", "mf14_reactivatedBy", "mf14_reactivatedOn", "mf14_isActive", "mf14_jsonField", "mf14_deptAbbrName") VALUES ('010', 'OPERATION', 'superadmin', '2018-04-05 13:00:22.897765', 'superadmin', '2018-04-05 16:17:40.962864', NULL, NULL, NULL, NULL, true, NULL, 'OP');
INSERT INTO "MF14_M_DEPTMAST" ("mf14_deptCode", "mf14_deptName", "mf14_createdBy", "mf14_createdOn", "mf14_lastModifiedBy", "mf14_lastModifiedOn", "mf14_deactivatedBy", "mf14_deactivatedOn", "mf14_reactivatedBy", "mf14_reactivatedOn", "mf14_isActive", "mf14_jsonField", "mf14_deptAbbrName") VALUES ('101', 'Parkson Malaysia2', 'superadmin', '2017-10-04 15:22:53.285', 'superadmin', '2017-10-04 15:53:05.131', NULL, NULL, NULL, NULL, true, NULL, 'Pu');
INSERT INTO "MF14_M_DEPTMAST" ("mf14_deptCode", "mf14_deptName", "mf14_createdBy", "mf14_createdOn", "mf14_lastModifiedBy", "mf14_lastModifiedOn", "mf14_deactivatedBy", "mf14_deactivatedOn", "mf14_reactivatedBy", "mf14_reactivatedOn", "mf14_isActive", "mf14_jsonField", "mf14_deptAbbrName") VALUES ('444', 'Parkson Pavillion', 'superadmin', '2017-10-03 10:10:11.185', 'superadmin', '2017-10-03 10:10:11.185', NULL, NULL, NULL, NULL, true, NULL, 'Pj');
INSERT INTO "MF14_M_DEPTMAST" ("mf14_deptCode", "mf14_deptName", "mf14_createdBy", "mf14_createdOn", "mf14_lastModifiedBy", "mf14_lastModifiedOn", "mf14_deactivatedBy", "mf14_deactivatedOn", "mf14_reactivatedBy", "mf14_reactivatedOn", "mf14_isActive", "mf14_jsonField", "mf14_deptAbbrName") VALUES ('002', 'MANAGEMENT CENTRENew', 'superadmin', '2018-04-05 13:00:22.787759', 'superadmin', '2018-05-01 21:59:39.757', NULL, NULL, NULL, NULL, true, NULL, 'wo');
INSERT INTO "MF14_M_DEPTMAST" ("mf14_deptCode", "mf14_deptName", "mf14_createdBy", "mf14_createdOn", "mf14_lastModifiedBy", "mf14_lastModifiedOn", "mf14_deactivatedBy", "mf14_deactivatedOn", "mf14_reactivatedBy", "mf14_reactivatedOn", "mf14_isActive", "mf14_jsonField", "mf14_deptAbbrName") VALUES ('001', 'CORPORATE1new', 'superadmin', '2018-04-05 13:00:22.745756', 'superadmin', '2018-05-01 23:57:19.552', NULL, NULL, NULL, NULL, true, NULL, 'CO');


--
-- TOC entry 2430 (class 0 OID 87525)
-- Dependencies: 197
-- Data for Name: MF15_M_PRMAST; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO "MF15_M_PRMAST" ("mf15_prAbbrName", "mf15_prDesc", "mf15_createdBy", "mf15_createdOn", "mf15_lastModifiedBy", "mf15_lastModifiedOn", "mf15_deactivatedBy", "mf15_deactivatedOn", "mf15_reactivatedBy", "mf15_reactivatedOn", "mf15_prTypeCode", "mf15_isActive", "mf15_jsonField") VALUES ('OX', 'Operational-Expenses', 'A_ROMI1', '2017-09-23 20:38:18.748', 'A_ROMI1', '2017-09-23 20:38:18.748', NULL, NULL, NULL, NULL, 'OPEX', true, NULL);
INSERT INTO "MF15_M_PRMAST" ("mf15_prAbbrName", "mf15_prDesc", "mf15_createdBy", "mf15_createdOn", "mf15_lastModifiedBy", "mf15_lastModifiedOn", "mf15_deactivatedBy", "mf15_deactivatedOn", "mf15_reactivatedBy", "mf15_reactivatedOn", "mf15_prTypeCode", "mf15_isActive", "mf15_jsonField") VALUES ('PurchaseReqAbbrName#1', 'Purchase Requisition Description #1', 'superadmin', '2017-11-07 10:34:02.679', 'superadmin', '2017-11-07 10:34:02.679', NULL, NULL, NULL, NULL, 'PURREQTY#1', true, NULL);
INSERT INTO "MF15_M_PRMAST" ("mf15_prAbbrName", "mf15_prDesc", "mf15_createdBy", "mf15_createdOn", "mf15_lastModifiedBy", "mf15_lastModifiedOn", "mf15_deactivatedBy", "mf15_deactivatedOn", "mf15_reactivatedBy", "mf15_reactivatedOn", "mf15_prTypeCode", "mf15_isActive", "mf15_jsonField") VALUES ('PurchaseReqAbbrName#2', 'Purchase Requisition Description2', 'superadmin', '2017-11-07 10:36:14.952', 'superadmin', '2017-11-07 10:36:29.296', 'superadmin', '2017-11-07 10:36:29.296', NULL, NULL, 'PURREQTY#2', false, NULL);
INSERT INTO "MF15_M_PRMAST" ("mf15_prAbbrName", "mf15_prDesc", "mf15_createdBy", "mf15_createdOn", "mf15_lastModifiedBy", "mf15_lastModifiedOn", "mf15_deactivatedBy", "mf15_deactivatedOn", "mf15_reactivatedBy", "mf15_reactivatedOn", "mf15_prTypeCode", "mf15_isActive", "mf15_jsonField") VALUES ('CAPEXnew', 'Captial Expenditures', 'superadmin', '2017-09-22 19:57:41.735', 'superadmin', '2018-05-01 23:55:16.951', NULL, NULL, NULL, NULL, 'CAPEX', true, NULL);


--
-- TOC entry 2431 (class 0 OID 87531)
-- Dependencies: 198
-- Data for Name: MF16_M_GSTMAST; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO "MF16_M_GSTMAST" ("mf16_taxCode", "mf16_taxDesc", "mf16_effectiveStartingDate", "mf16_effectiveEndingDate", "mf16_createdBy", "mf16_createdOn", "mf16_lastModifiedBy", "mf16_lastModifiedOn", "mf16_deactivatedBy", "mf16_deactivatedOn", "mf16_reactivatedBy", "mf16_reactivatedOn", "mf16_taxAbbrDesc", "mf16_isActive", "mf16_jsonField", "mf16_taxRate", "mf16_allowedVariance") VALUES ('TXCODE2', 'Tax Code Description #2', '2017-11-14', '2017-11-21', 'superadmin', '2017-11-07 10:31:07.95', 'superadmin', '2017-11-07 10:31:42.115', 'superadmin', '2017-11-07 10:31:42.115', NULL, NULL, 'Tax Code Abbr Description #2', false, NULL, 0.0000, 0.0000);
INSERT INTO "MF16_M_GSTMAST" ("mf16_taxCode", "mf16_taxDesc", "mf16_effectiveStartingDate", "mf16_effectiveEndingDate", "mf16_createdBy", "mf16_createdOn", "mf16_lastModifiedBy", "mf16_lastModifiedOn", "mf16_deactivatedBy", "mf16_deactivatedOn", "mf16_reactivatedBy", "mf16_reactivatedOn", "mf16_taxAbbrDesc", "mf16_isActive", "mf16_jsonField", "mf16_taxRate", "mf16_allowedVariance") VALUES ('GST11111', 'TextTextTextTextnew', '2017-10-11', '2017-10-31', 'superadmin', '2017-10-04 16:31:21.974', 'superadmin', '2018-05-01 23:55:39.248', NULL, NULL, NULL, NULL, 'Tax Description', true, NULL, 11.0000, 1.0000);
INSERT INTO "MF16_M_GSTMAST" ("mf16_taxCode", "mf16_taxDesc", "mf16_effectiveStartingDate", "mf16_effectiveEndingDate", "mf16_createdBy", "mf16_createdOn", "mf16_lastModifiedBy", "mf16_lastModifiedOn", "mf16_deactivatedBy", "mf16_deactivatedOn", "mf16_reactivatedBy", "mf16_reactivatedOn", "mf16_taxAbbrDesc", "mf16_isActive", "mf16_jsonField", "mf16_taxRate", "mf16_allowedVariance") VALUES ('DISCOUNTFLOWERS00001', 'When purchase 10 flowers discount is 10%', '2017-10-17', '2017-10-31', 'superadmin', '2017-10-16 14:38:46.075', 'superadmin', '2018-05-01 23:56:04.285', 'superadmin', '2017-10-16 14:39:36.15', NULL, NULL, 'FLOWERS00001new', false, NULL, 2.0000, 1.0000);
INSERT INTO "MF16_M_GSTMAST" ("mf16_taxCode", "mf16_taxDesc", "mf16_effectiveStartingDate", "mf16_effectiveEndingDate", "mf16_createdBy", "mf16_createdOn", "mf16_lastModifiedBy", "mf16_lastModifiedOn", "mf16_deactivatedBy", "mf16_deactivatedOn", "mf16_reactivatedBy", "mf16_reactivatedOn", "mf16_taxAbbrDesc", "mf16_isActive", "mf16_jsonField", "mf16_taxRate", "mf16_allowedVariance") VALUES ('TXCODE1', 'Tax Code Description #1', '2017-11-07', '2017-11-14', 'superadmin', '2017-11-07 10:28:04.836', 'superadmin', '2018-05-01 23:56:24.484', NULL, NULL, NULL, NULL, 'Tax Abbr Description #1new', true, NULL, 2.0000, 1.0000);


--
-- TOC entry 2432 (class 0 OID 87537)
-- Dependencies: 199
-- Data for Name: MF17_M_ORDCATMAST; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO "MF17_M_ORDCATMAST" ("mf17_ordCatCode", "mf17_ordCatDesc", "mf17_ordCatAuthorizationGrid", "mf17_createdBy", "mf17_createdOn", "mf17_lastModifiedBy", "mf17_lastModifiedOn", "mf17_deactivatedBy", "mf17_deactivatedOn", "mf17_reactivatedBy", "mf17_reactivatedOn", "mf17_jsonField", "mf17_ordCatAbbrDesc", "mf15MPrmast_mf15_prTypeCode", "mf17_isActive") VALUES ('REQUISITION_OF_PERFUME#2', 'Need to buy perfume for .................', 'uploaded files/authorization grid/geek-inside.sh-600x600.png', 'superadmin', '2017-10-16 15:02:55.684', 'superadmin', '2017-10-16 15:02:55.684', NULL, NULL, NULL, NULL, NULL, 'PERFUME', 'OPEX', true);
INSERT INTO "MF17_M_ORDCATMAST" ("mf17_ordCatCode", "mf17_ordCatDesc", "mf17_ordCatAuthorizationGrid", "mf17_createdBy", "mf17_createdOn", "mf17_lastModifiedBy", "mf17_lastModifiedOn", "mf17_deactivatedBy", "mf17_deactivatedOn", "mf17_reactivatedBy", "mf17_reactivatedOn", "mf17_jsonField", "mf17_ordCatAbbrDesc", "mf15MPrmast_mf15_prTypeCode", "mf17_isActive") VALUES ('ORDERCATCODE#1', 'Order Category Description #1', 'uploaded files/authorization grid/scan_002.png', 'superadmin', '2017-11-07 10:46:00.975', 'superadmin', '2017-11-07 10:46:00.975', NULL, NULL, NULL, NULL, NULL, 'OrderCatAbbrDesc#1', 'PURREQTY#1', true);
INSERT INTO "MF17_M_ORDCATMAST" ("mf17_ordCatCode", "mf17_ordCatDesc", "mf17_ordCatAuthorizationGrid", "mf17_createdBy", "mf17_createdOn", "mf17_lastModifiedBy", "mf17_lastModifiedOn", "mf17_deactivatedBy", "mf17_deactivatedOn", "mf17_reactivatedBy", "mf17_reactivatedOn", "mf17_jsonField", "mf17_ordCatAbbrDesc", "mf15MPrmast_mf15_prTypeCode", "mf17_isActive") VALUES ('ORDERCATCODE#2', 'order category description #2', 'uploaded files/authorization grid/Intuglo1.png', 'superadmin', '2017-11-07 10:48:21.515', 'superadmin', '2017-11-07 10:49:19.967', 'superadmin', '2017-11-07 10:49:19.967', NULL, NULL, NULL, 'OrderCatAbbrDesc#2', 'PURREQTY#2', false);
INSERT INTO "MF17_M_ORDCATMAST" ("mf17_ordCatCode", "mf17_ordCatDesc", "mf17_ordCatAuthorizationGrid", "mf17_createdBy", "mf17_createdOn", "mf17_lastModifiedBy", "mf17_lastModifiedOn", "mf17_deactivatedBy", "mf17_deactivatedOn", "mf17_reactivatedBy", "mf17_reactivatedOn", "mf17_jsonField", "mf17_ordCatAbbrDesc", "mf15MPrmast_mf15_prTypeCode", "mf17_isActive") VALUES ('REQ-NEW STORE-IT', 'This is for New Store especially for IT', 'uploaded files/authorization grid/Sample Authorization Grid.pdf', 'A_ROMI1', '2017-09-23 20:43:28.342', 'superadmin', '2018-05-01 23:54:59.468', NULL, NULL, NULL, NULL, NULL, 'R-NS-ITnew', 'CAPEX', true);


--
-- TOC entry 2433 (class 0 OID 87543)
-- Dependencies: 200
-- Data for Name: MF18_M_ORDSUBCMAST; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO "MF18_M_ORDSUBCMAST" ("mf18_ordSubCCode", "mf18_ordSubCDesc", "mf17MOrdcatmast_mf17_ordCatCode", "mf18_createdBy", "mf18_createdOn", "mf18_lastModifiedOn", "mf18_deactivatedBy", "mf18_deactivatedOn", "mf18_reactivatedBy", "mf18_reactivatedOn", "mf18_jsonField", "mf18_lastModifiedBy", "mf18_ordSubCAbbrDesc", "mf18_isActive") VALUES ('Care supplies1', 'Cosmetics, skin care etc. *@&2', 'REQ-NEW STORE-IT', 'superadmin', '2017-10-05 10:40:47.003', '2017-10-05 10:59:22.155', 'superadmin', '2017-10-05 10:59:22.155', NULL, NULL, NULL, 'superadmin', 'Health Care1@', false);
INSERT INTO "MF18_M_ORDSUBCMAST" ("mf18_ordSubCCode", "mf18_ordSubCDesc", "mf17MOrdcatmast_mf17_ordCatCode", "mf18_createdBy", "mf18_createdOn", "mf18_lastModifiedOn", "mf18_deactivatedBy", "mf18_deactivatedOn", "mf18_reactivatedBy", "mf18_reactivatedOn", "mf18_jsonField", "mf18_lastModifiedBy", "mf18_ordSubCAbbrDesc", "mf18_isActive") VALUES ('InnaSubCode2', 'Inna Sub Category Desc2', 'REQUISITION_OF_PERFUME#2', 'superadmin', '2017-10-24 11:14:03.95', '2017-10-24 11:14:30.664', 'superadmin', '2017-10-24 11:14:30.664', NULL, NULL, NULL, 'superadmin', 'Inna Sub Category Abbr2', false);
INSERT INTO "MF18_M_ORDSUBCMAST" ("mf18_ordSubCCode", "mf18_ordSubCDesc", "mf17MOrdcatmast_mf17_ordCatCode", "mf18_createdBy", "mf18_createdOn", "mf18_lastModifiedOn", "mf18_deactivatedBy", "mf18_deactivatedOn", "mf18_reactivatedBy", "mf18_reactivatedOn", "mf18_jsonField", "mf18_lastModifiedBy", "mf18_ordSubCAbbrDesc", "mf18_isActive") VALUES ('OrdSubCatCode#1', 'Order Sub Category Desc #1', 'ORDERCATCODE#1', 'superadmin', '2017-11-07 10:52:21.963', '2017-11-07 10:52:21.963', NULL, NULL, NULL, NULL, NULL, 'superadmin', 'Order Sub Cat Abbreviation #1', true);
INSERT INTO "MF18_M_ORDSUBCMAST" ("mf18_ordSubCCode", "mf18_ordSubCDesc", "mf17MOrdcatmast_mf17_ordCatCode", "mf18_createdBy", "mf18_createdOn", "mf18_lastModifiedOn", "mf18_deactivatedBy", "mf18_deactivatedOn", "mf18_reactivatedBy", "mf18_reactivatedOn", "mf18_jsonField", "mf18_lastModifiedBy", "mf18_ordSubCAbbrDesc", "mf18_isActive") VALUES ('OrdSubCatCode#2', 'Order Sub Category Description #2', 'ORDERCATCODE#2', 'superadmin', '2017-11-07 10:53:20.807', '2017-11-07 10:53:20.807', NULL, NULL, NULL, NULL, NULL, 'superadmin', 'Order Sub Category Abbr #2', true);


--
-- TOC entry 2434 (class 0 OID 87549)
-- Dependencies: 201
-- Data for Name: MF19_M_JOBMAST; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO "MF19_M_JOBMAST" ("mf19_jobCode", "mf19_jobName", "mf19_jobAbbrName", "mf19_createdBy", "mf19_createdOn", "mf19_lastModifiedBy", "mf19_lastModifiedOn", "mf19_deactivatedBy", "mf19_deactivatedOn", "mf19_reactivatedBy", "mf19_reactivatedOn", "mf19_jsonField", "mf19_isActive") VALUES ('Test77', 'Network Administrator!@3', 'NetADMIN', 'superadmin', '2017-10-05 11:58:00.736', 'superadmin', '2017-10-05 11:58:00.736', NULL, NULL, NULL, NULL, NULL, true);
INSERT INTO "MF19_M_JOBMAST" ("mf19_jobCode", "mf19_jobName", "mf19_jobAbbrName", "mf19_createdBy", "mf19_createdOn", "mf19_lastModifiedBy", "mf19_lastModifiedOn", "mf19_deactivatedBy", "mf19_deactivatedOn", "mf19_reactivatedBy", "mf19_reactivatedOn", "mf19_jsonField", "mf19_isActive") VALUES ('ACCNT', 'Accounter', 'Accounter', 'superadmin', '2017-10-16 15:32:18.662', 'superadmin', '2017-10-16 15:32:18.662', NULL, NULL, NULL, NULL, NULL, true);
INSERT INTO "MF19_M_JOBMAST" ("mf19_jobCode", "mf19_jobName", "mf19_jobAbbrName", "mf19_createdBy", "mf19_createdOn", "mf19_lastModifiedBy", "mf19_lastModifiedOn", "mf19_deactivatedBy", "mf19_deactivatedOn", "mf19_reactivatedBy", "mf19_reactivatedOn", "mf19_jsonField", "mf19_isActive") VALUES ('111111', 'JobName#2', 'JobAbbr#2', 'superadmin', '2017-11-07 11:03:23.908', 'superadmin', '2017-11-07 11:03:23.908', NULL, NULL, NULL, NULL, NULL, true);
INSERT INTO "MF19_M_JOBMAST" ("mf19_jobCode", "mf19_jobName", "mf19_jobAbbrName", "mf19_createdBy", "mf19_createdOn", "mf19_lastModifiedBy", "mf19_lastModifiedOn", "mf19_deactivatedBy", "mf19_deactivatedOn", "mf19_reactivatedBy", "mf19_reactivatedOn", "mf19_jsonField", "mf19_isActive") VALUES ('dfdf45', 'frd', 'df', 'superadmin', '2018-04-04 11:12:38.783', 'superadmin', '2018-04-04 11:12:38.783', NULL, NULL, NULL, NULL, NULL, true);
INSERT INTO "MF19_M_JOBMAST" ("mf19_jobCode", "mf19_jobName", "mf19_jobAbbrName", "mf19_createdBy", "mf19_createdOn", "mf19_lastModifiedBy", "mf19_lastModifiedOn", "mf19_deactivatedBy", "mf19_deactivatedOn", "mf19_reactivatedBy", "mf19_reactivatedOn", "mf19_jsonField", "mf19_isActive") VALUES ('INNA2', 'Job Abbr 2', 'Job Abbr 2new', 'superadmin', '2017-10-23 11:10:09.408', 'DR', '2018-05-01 23:53:35.141', 'superadmin', '2017-10-23 11:10:19.793', NULL, NULL, NULL, false);


--
-- TOC entry 2435 (class 0 OID 87555)
-- Dependencies: 202
-- Data for Name: MF20_M_ADMINMAST; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO "MF20_M_ADMINMAST" (mf20_username, mf20_password, "mf20_startDate", "mf20_endDate", "mf20_isActive", "mf20_createdOn", "mf20_lastModifiedOn", "mf20_deactivatedOn", "mf20_reactivatedOn", mf20_blocked, "mf20_jsonField", "mf20_recentPassword", "mf20_isLocked", password_reset_token) VALUES ('A_BEH12', 'c20ad4d76fe97759aa27a0c99bff6710', '2017-09-22', '2018-05-01', true, '2017-09-22 12:55:52.807', '2018-05-01 16:56:16.805', '2018-05-01 16:52:58.532', '2018-05-01 16:56:16.805', 0, NULL, '[c20ad4d76fe97759aa27a0c99bff6710]', false, NULL);
INSERT INTO "MF20_M_ADMINMAST" (mf20_username, mf20_password, "mf20_startDate", "mf20_endDate", "mf20_isActive", "mf20_createdOn", "mf20_lastModifiedOn", "mf20_deactivatedOn", "mf20_reactivatedOn", mf20_blocked, "mf20_jsonField", "mf20_recentPassword", "mf20_isLocked", password_reset_token) VALUES ('A_MORI2', 'bb8e84bcd669d6b5a5ae44993566e8c5', '2017-10-05', '2017-10-05', true, '2017-10-05 15:31:11.453', '2018-05-02 01:05:45.233', NULL, '2018-05-02 01:05:45.233', 1, NULL, '[bb8e84bcd669d6b5a5ae44993566e8c5]', false, NULL);
INSERT INTO "MF20_M_ADMINMAST" (mf20_username, mf20_password, "mf20_startDate", "mf20_endDate", "mf20_isActive", "mf20_createdOn", "mf20_lastModifiedOn", "mf20_deactivatedOn", "mf20_reactivatedOn", mf20_blocked, "mf20_jsonField", "mf20_recentPassword", "mf20_isLocked", password_reset_token) VALUES ('A_HGFG6', '5af544a97950e81f263b556493e4d154', '2018-05-02', '2018-05-02', true, '2018-05-02 15:12:43.564', '2018-05-02 15:12:43.564', NULL, NULL, 0, NULL, '5af544a97950e81f263b556493e4d154', false, NULL);
INSERT INTO "MF20_M_ADMINMAST" (mf20_username, mf20_password, "mf20_startDate", "mf20_endDate", "mf20_isActive", "mf20_createdOn", "mf20_lastModifiedOn", "mf20_deactivatedOn", "mf20_reactivatedOn", mf20_blocked, "mf20_jsonField", "mf20_recentPassword", "mf20_isLocked", password_reset_token) VALUES ('A_INNAA', '827ccb0eea8a706c4c34a16891f84e7b', '2017-10-16', '2017-10-16', true, '2017-10-16 11:32:38.089', '2018-05-02 19:01:59.639', '2017-10-16 11:35:23.525', '2018-05-02 19:01:59.639', 0, NULL, '[827ccb0eea8a706c4c34a16891f84e7b]', true, NULL);
INSERT INTO "MF20_M_ADMINMAST" (mf20_username, mf20_password, "mf20_startDate", "mf20_endDate", "mf20_isActive", "mf20_createdOn", "mf20_lastModifiedOn", "mf20_deactivatedOn", "mf20_reactivatedOn", mf20_blocked, "mf20_jsonField", "mf20_recentPassword", "mf20_isLocked", password_reset_token) VALUES ('A_ABC23', '5af544a97950e81f263b556493e4d154', '2018-05-01', '2018-05-01', true, '2018-05-01 16:56:52.02', '2018-05-02 19:07:38.146', NULL, '2018-05-02 19:07:38.146', 0, NULL, '5af544a97950e81f263b556493e4d154', false, NULL);
INSERT INTO "MF20_M_ADMINMAST" (mf20_username, mf20_password, "mf20_startDate", "mf20_endDate", "mf20_isActive", "mf20_createdOn", "mf20_lastModifiedOn", "mf20_deactivatedOn", "mf20_reactivatedOn", mf20_blocked, "mf20_jsonField", "mf20_recentPassword", "mf20_isLocked", password_reset_token) VALUES ('A_ROMI1', '$2a$08$vrmtYm/9JQAIvU77MxouFeeVP7v.15Z7hE0ML8m9CxCmSTVF1Gtky', '2017-09-23', '2018-05-01', false, '2017-09-23 18:36:51.698', '2018-05-02 19:08:13.479', '2018-05-02 19:08:13.479', '2018-05-01 22:16:42.653', 0, NULL, '[3d9d8555cc65310173c1129790169c1d]', false, NULL);
INSERT INTO "MF20_M_ADMINMAST" (mf20_username, mf20_password, "mf20_startDate", "mf20_endDate", "mf20_isActive", "mf20_createdOn", "mf20_lastModifiedOn", "mf20_deactivatedOn", "mf20_reactivatedOn", mf20_blocked, "mf20_jsonField", "mf20_recentPassword", "mf20_isLocked", password_reset_token) VALUES ('superadmin', '$2a$08$vrmtYm/9JQAIvU77MxouFeeVP7v.15Z7hE0ML8m9CxCmSTVF1Gtky', NULL, NULL, false, '2017-09-12 15:19:02.651', '2017-09-12 15:19:02.651', NULL, NULL, 0, NULL, NULL, false, NULL);


--
-- TOC entry 2439 (class 0 OID 87567)
-- Dependencies: 206
-- Data for Name: Mf22_M_SCHEDULER; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO "Mf22_M_SCHEDULER" (mf22_schedulerid, mf22_task, mf22_cronfrequency_occurrence, mf22_startdate, mf22_noenddate, mf22_cronfrequency_amount, mf22_status, mf22_database, mf22_table, mf22_scheduletype, mf22_enddate, mf22_upload, mf22_path, mf22_createdon, mf22_createdby, mf22_deactivatedby, mf22_deactivatedon, mf22_jobname) VALUES (1, 'Master Table Upload', NULL, '2018-04-08 11:15:00+08', false, NULL, 'Scheduled', NULL, NULL, 'One-time', NULL, NULL, NULL, '11:15:18.369', 'superadmin', NULL, NULL, 'MASTER TABLE UPLOAD 1');
INSERT INTO "Mf22_M_SCHEDULER" (mf22_schedulerid, mf22_task, mf22_cronfrequency_occurrence, mf22_startdate, mf22_noenddate, mf22_cronfrequency_amount, mf22_status, mf22_database, mf22_table, mf22_scheduletype, mf22_enddate, mf22_upload, mf22_path, mf22_createdon, mf22_createdby, mf22_deactivatedby, mf22_deactivatedon, mf22_jobname) VALUES (2, 'PO Expiry', 'Daily', '2018-04-16 11:15:00+08', false, NULL, 'Scheduled', NULL, NULL, 'Recurring', NULL, NULL, NULL, '11:15:31.87', 'superadmin', NULL, NULL, 'PO EXPIRY 2');
INSERT INTO "Mf22_M_SCHEDULER" (mf22_schedulerid, mf22_task, mf22_cronfrequency_occurrence, mf22_startdate, mf22_noenddate, mf22_cronfrequency_amount, mf22_status, mf22_database, mf22_table, mf22_scheduletype, mf22_enddate, mf22_upload, mf22_path, mf22_createdon, mf22_createdby, mf22_deactivatedby, mf22_deactivatedon, mf22_jobname) VALUES (3, 'Master Table Upload', NULL, '2018-04-05 12:56:00+08', false, NULL, 'Scheduled', NULL, NULL, 'One-time', NULL, NULL, NULL, '12:56:59.804', 'superadmin', NULL, NULL, 'MASTER TABLE UPLOAD 3');
INSERT INTO "Mf22_M_SCHEDULER" (mf22_schedulerid, mf22_task, mf22_cronfrequency_occurrence, mf22_startdate, mf22_noenddate, mf22_cronfrequency_amount, mf22_status, mf22_database, mf22_table, mf22_scheduletype, mf22_enddate, mf22_upload, mf22_path, mf22_createdon, mf22_createdby, mf22_deactivatedby, mf22_deactivatedon, mf22_jobname) VALUES (4, 'Master Table Upload', NULL, '2018-04-05 12:58:00+08', false, NULL, 'Scheduled', NULL, NULL, 'One-time', NULL, NULL, NULL, '12:58:15.769', 'superadmin', NULL, NULL, 'MASTER TABLE UPLOAD 4');
INSERT INTO "Mf22_M_SCHEDULER" (mf22_schedulerid, mf22_task, mf22_cronfrequency_occurrence, mf22_startdate, mf22_noenddate, mf22_cronfrequency_amount, mf22_status, mf22_database, mf22_table, mf22_scheduletype, mf22_enddate, mf22_upload, mf22_path, mf22_createdon, mf22_createdby, mf22_deactivatedby, mf22_deactivatedon, mf22_jobname) VALUES (5, 'Master Table Upload', NULL, '2018-04-05 13:00:00+08', false, NULL, 'Scheduled', NULL, NULL, 'One-time', NULL, NULL, NULL, '13:00:22.354', 'superadmin', NULL, NULL, 'MASTER TABLE UPLOAD 5');
INSERT INTO "Mf22_M_SCHEDULER" (mf22_schedulerid, mf22_task, mf22_cronfrequency_occurrence, mf22_startdate, mf22_noenddate, mf22_cronfrequency_amount, mf22_status, mf22_database, mf22_table, mf22_scheduletype, mf22_enddate, mf22_upload, mf22_path, mf22_createdon, mf22_createdby, mf22_deactivatedby, mf22_deactivatedon, mf22_jobname) VALUES (6, 'Master Table Upload', NULL, '2018-04-05 13:02:00+08', false, NULL, 'Scheduled', NULL, NULL, 'One-time', NULL, NULL, NULL, '13:02:26.758', 'superadmin', NULL, NULL, 'MASTER TABLE UPLOAD 6');
INSERT INTO "Mf22_M_SCHEDULER" (mf22_schedulerid, mf22_task, mf22_cronfrequency_occurrence, mf22_startdate, mf22_noenddate, mf22_cronfrequency_amount, mf22_status, mf22_database, mf22_table, mf22_scheduletype, mf22_enddate, mf22_upload, mf22_path, mf22_createdon, mf22_createdby, mf22_deactivatedby, mf22_deactivatedon, mf22_jobname) VALUES (7, 'Master Table Upload', NULL, '2018-04-05 13:03:00+08', false, NULL, 'Scheduled', NULL, NULL, 'One-time', NULL, NULL, NULL, '13:03:57.775', 'superadmin', NULL, NULL, 'MASTER TABLE UPLOAD 7');
INSERT INTO "Mf22_M_SCHEDULER" (mf22_schedulerid, mf22_task, mf22_cronfrequency_occurrence, mf22_startdate, mf22_noenddate, mf22_cronfrequency_amount, mf22_status, mf22_database, mf22_table, mf22_scheduletype, mf22_enddate, mf22_upload, mf22_path, mf22_createdon, mf22_createdby, mf22_deactivatedby, mf22_deactivatedon, mf22_jobname) VALUES (8, 'Master Table Upload', NULL, '2018-04-05 16:17:00+08', false, NULL, 'Scheduled', NULL, NULL, 'One-time', NULL, NULL, NULL, '16:17:40.314', 'superadmin', NULL, NULL, 'MASTER TABLE UPLOAD 8');


--
-- TOC entry 2443 (class 0 OID 87583)
-- Dependencies: 210
-- Data for Name: PO01_LOGINLIST; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO "PO01_LOGINLIST" (po01_loginlist_seq, po01_year, po01_companycode, po01_companyname) VALUES (31, '2018', '081', 'siroi solns');
INSERT INTO "PO01_LOGINLIST" (po01_loginlist_seq, po01_year, po01_companycode, po01_companyname) VALUES (28, '2017', '080', 'erat');
INSERT INTO "PO01_LOGINLIST" (po01_loginlist_seq, po01_year, po01_companycode, po01_companyname) VALUES (29, '2015', '080', 'erat');
INSERT INTO "PO01_LOGINLIST" (po01_loginlist_seq, po01_year, po01_companycode, po01_companyname) VALUES (30, '2016', '080', 'erat');
INSERT INTO "PO01_LOGINLIST" (po01_loginlist_seq, po01_year, po01_companycode, po01_companyname) VALUES (27, '2018', '080', 'erat');


--
-- TOC entry 2457 (class 0 OID 88190)
-- Dependencies: 225
-- Data for Name: PO01_T_MAIN; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--



--
-- TOC entry 2441 (class 0 OID 87575)
-- Dependencies: 208
-- Data for Name: PO06_T_USER; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO "PO06_T_USER" (po06_psid, po06_empname, po06_firsttimelogin, po06_lastlogin, po06_numberoflogin, po06_isactive, po06_createdby, po06_createdon, po06_lastmodifiedby, po06_lastmodifiedon, po06_deactivatedby, po06_deactivatedon, po06_reactivatedby, po06_reactivatedon, po06_json) VALUES ('0002050', 'E Geak Neo', NULL, NULL, 0, true, 'superadmin', '2017-10-30 12:10:17.398', 'superadmin', '2017-10-30 12:11:00.18', NULL, NULL, 'superadmin', '2017-10-30 12:11:00.18', NULL);
INSERT INTO "PO06_T_USER" (po06_psid, po06_empname, po06_firsttimelogin, po06_lastlogin, po06_numberoflogin, po06_isactive, po06_createdby, po06_createdon, po06_lastmodifiedby, po06_lastmodifiedon, po06_deactivatedby, po06_deactivatedon, po06_reactivatedby, po06_reactivatedon, po06_json) VALUES ('0002042', 'E Geak Neo', NULL, NULL, 0, true, 'superadmin', '2017-10-30 15:23:15.178', 'superadmin', '2017-11-06 09:46:53.092', NULL, NULL, 'superadmin', '2017-11-06 09:46:53.092', NULL);
INSERT INTO "PO06_T_USER" (po06_psid, po06_empname, po06_firsttimelogin, po06_lastlogin, po06_numberoflogin, po06_isactive, po06_createdby, po06_createdon, po06_lastmodifiedby, po06_lastmodifiedon, po06_deactivatedby, po06_deactivatedon, po06_reactivatedby, po06_reactivatedon, po06_json) VALUES ('0002041', 'E Geak Neocc', NULL, NULL, 0, true, NULL, NULL, 'superadmin', '2018-05-01 22:30:11.406', NULL, NULL, NULL, NULL, NULL);
INSERT INTO "PO06_T_USER" (po06_psid, po06_empname, po06_firsttimelogin, po06_lastlogin, po06_numberoflogin, po06_isactive, po06_createdby, po06_createdon, po06_lastmodifiedby, po06_lastmodifiedon, po06_deactivatedby, po06_deactivatedon, po06_reactivatedby, po06_reactivatedon, po06_json) VALUES ('0002040', 'E Geak Neo', NULL, NULL, 0, true, NULL, NULL, 'superadmin', '2018-05-01 22:31:02.825', NULL, NULL, NULL, NULL, NULL);
INSERT INTO "PO06_T_USER" (po06_psid, po06_empname, po06_firsttimelogin, po06_lastlogin, po06_numberoflogin, po06_isactive, po06_createdby, po06_createdon, po06_lastmodifiedby, po06_lastmodifiedon, po06_deactivatedby, po06_deactivatedon, po06_reactivatedby, po06_reactivatedon, po06_json) VALUES ('0002039', 'E Geak Neoww', NULL, NULL, 0, true, NULL, NULL, 'superadmin', '2018-05-01 22:38:37.473', NULL, NULL, NULL, NULL, NULL);
INSERT INTO "PO06_T_USER" (po06_psid, po06_empname, po06_firsttimelogin, po06_lastlogin, po06_numberoflogin, po06_isactive, po06_createdby, po06_createdon, po06_lastmodifiedby, po06_lastmodifiedon, po06_deactivatedby, po06_deactivatedon, po06_reactivatedby, po06_reactivatedon, po06_json) VALUES ('0002049', '@WWWe', NULL, NULL, 0, false, NULL, NULL, 'superadmin', '2018-05-02 19:10:48.726', NULL, NULL, NULL, NULL, NULL);


--
-- TOC entry 2445 (class 0 OID 88063)
-- Dependencies: 213
-- Data for Name: audit; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--



--
-- TOC entry 2485 (class 0 OID 0)
-- Dependencies: 186
-- Name: audit_auditid_seq; Type: SEQUENCE SET; Schema: mfmaindb; Owner: postgres
--



--
-- TOC entry 2486 (class 0 OID 0)
-- Dependencies: 212
-- Name: audit_auditid_seq1; Type: SEQUENCE SET; Schema: mfmaindb; Owner: postgres
--



--
-- TOC entry 2446 (class 0 OID 88072)
-- Dependencies: 214
-- Data for Name: ld02_t_acslog; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--



--
-- TOC entry 2487 (class 0 OID 0)
-- Dependencies: 183
-- Name: ld02_t_acslog_sequence; Type: SEQUENCE SET; Schema: mfmaindb; Owner: postgres
--



--
-- TOC entry 2447 (class 0 OID 88080)
-- Dependencies: 215
-- Data for Name: ld03_t_stglog; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO ld03_t_stglog (ld03_logsettingid, ld03_logsetting) VALUES (1, 1);


--
-- TOC entry 2488 (class 0 OID 0)
-- Dependencies: 203
-- Name: ld03_t_stglog_sequence; Type: SEQUENCE SET; Schema: mfmaindb; Owner: postgres
--



--
-- TOC entry 2460 (class 0 OID 96278)
-- Dependencies: 228
-- Data for Name: menu; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO menu (menu_id, menu_code, menu_desc, menu_url, menu_reference) VALUES (1, 'user-menu', 'User Menu', '/#', 'user-menu');
INSERT INTO menu (menu_id, menu_code, menu_desc, menu_url, menu_reference) VALUES (2, 'user-management', 'User Management', '/#', 'user-management');
INSERT INTO menu (menu_id, menu_code, menu_desc, menu_url, menu_reference) VALUES (3, 'adminuser-management', 'Admin User Management', '/users/adminuser', 'admin-user-management');
INSERT INTO menu (menu_id, menu_code, menu_desc, menu_url, menu_reference) VALUES (4, 'pouser-management', 'PO User Management', '/users/pouser', 'pouser-management');
INSERT INTO menu (menu_id, menu_code, menu_desc, menu_url, menu_reference) VALUES (5, 'audit-trail', 'Audit Trail', '/#', 'audit-trail');
INSERT INTO menu (menu_id, menu_code, menu_desc, menu_url, menu_reference) VALUES (6, 'audit-log', 'Audit Log', '/audit/logs', 'audit-log');
INSERT INTO menu (menu_id, menu_code, menu_desc, menu_url, menu_reference) VALUES (7, 'audit-traillist', 'Audit Trail List', '/audit/audittraillist', 'audit-traillist');
INSERT INTO menu (menu_id, menu_code, menu_desc, menu_url, menu_reference) VALUES (8, 'master-management', 'Master File Management', '/#', 'master-management');
INSERT INTO menu (menu_id, menu_code, menu_desc, menu_url, menu_reference) VALUES (9, 'company', 'Company', '/master/company', 'company');
INSERT INTO menu (menu_id, menu_code, menu_desc, menu_url, menu_reference) VALUES (10, 'store', 'Store', '/master/store', 'store');
INSERT INTO menu (menu_id, menu_code, menu_desc, menu_url, menu_reference) VALUES (11, 'department', 'Department', '/master/department', 'department');
INSERT INTO menu (menu_id, menu_code, menu_desc, menu_url, menu_reference) VALUES (12, 'profitcenter', 'Profit Center', '/master/profitcenter', 'profitcenter');
INSERT INTO menu (menu_id, menu_code, menu_desc, menu_url, menu_reference) VALUES (13, 'gst', 'Goods & Services Tax (GST)', '/master/gst', 'gst');
INSERT INTO menu (menu_id, menu_code, menu_desc, menu_url, menu_reference) VALUES (14, 'purchase-requisition', 'Purchase Requisition', '/master/purchase', 'purchase-requisition');
INSERT INTO menu (menu_id, menu_code, menu_desc, menu_url, menu_reference) VALUES (15, 'order-category', 'Order Category', '/master/ordercategory', 'order-category');
INSERT INTO menu (menu_id, menu_code, menu_desc, menu_url, menu_reference) VALUES (16, 'order-subcategory', 'Order Sub Category', '/master/ordersubcategory', 'order-subcategory');
INSERT INTO menu (menu_id, menu_code, menu_desc, menu_url, menu_reference) VALUES (17, 'scheduler', 'Scheduler', '/master/scheduler', 'scheduler');
INSERT INTO menu (menu_id, menu_code, menu_desc, menu_url, menu_reference) VALUES (18, 'employee-management', 'Employee Management', '/#', 'employee-management');
INSERT INTO menu (menu_id, menu_code, menu_desc, menu_url, menu_reference) VALUES (19, 'employee', 'Employee', '/employee/emplist', 'employee');
INSERT INTO menu (menu_id, menu_code, menu_desc, menu_url, menu_reference) VALUES (20, 'job-master', 'Job Master', '/employee/jobmaster', 'job-master');
INSERT INTO menu (menu_id, menu_code, menu_desc, menu_url, menu_reference) VALUES (21, 'system-configuration', 'System Configuration', '/#', 'system-configuration');
INSERT INTO menu (menu_id, menu_code, menu_desc, menu_url, menu_reference) VALUES (22, 'masterdata', 'Master Data', '/system/masterdata', 'masterdata');
INSERT INTO menu (menu_id, menu_code, menu_desc, menu_url, menu_reference) VALUES (23, 'system-parameter', 'System Parameter', '/system/parameter', 'system-parameter');


--
-- TOC entry 2462 (class 0 OID 96287)
-- Dependencies: 230
-- Data for Name: menu_grp; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO menu_grp (menu_grp_id, menu_grp_parent, menu_code, menu_desc, menu_url, menu_reference, menu_grp_root, menu_id, menu_grp_enable) VALUES (1, 0, 'user-menu', 'User menu', '/#', 'user-menu', 'user-menu', 1, true);
INSERT INTO menu_grp (menu_grp_id, menu_grp_parent, menu_code, menu_desc, menu_url, menu_reference, menu_grp_root, menu_id, menu_grp_enable) VALUES (2, 1, 'user-management', 'User Management', '/#', 'user-management', 'user-menu', 2, true);
INSERT INTO menu_grp (menu_grp_id, menu_grp_parent, menu_code, menu_desc, menu_url, menu_reference, menu_grp_root, menu_id, menu_grp_enable) VALUES (3, 2, 'adminuser-management', 'Admin User Management', '/users/adminuser', 'admin-user-management', 'user-menu', 3, true);
INSERT INTO menu_grp (menu_grp_id, menu_grp_parent, menu_code, menu_desc, menu_url, menu_reference, menu_grp_root, menu_id, menu_grp_enable) VALUES (4, 2, 'pouser-management', 'PO User Management', '/users/pouser', 'pouser-management', 'user-menu', 4, true);
INSERT INTO menu_grp (menu_grp_id, menu_grp_parent, menu_code, menu_desc, menu_url, menu_reference, menu_grp_root, menu_id, menu_grp_enable) VALUES (5, 1, 'audit-trail', 'Audit Trail', '/#', 'audit-trail', 'user-menu', 5, true);
INSERT INTO menu_grp (menu_grp_id, menu_grp_parent, menu_code, menu_desc, menu_url, menu_reference, menu_grp_root, menu_id, menu_grp_enable) VALUES (6, 5, 'audit-log', 'Audit Log', '/audit/logs', 'audit-log', 'user-menu', 6, true);
INSERT INTO menu_grp (menu_grp_id, menu_grp_parent, menu_code, menu_desc, menu_url, menu_reference, menu_grp_root, menu_id, menu_grp_enable) VALUES (7, 5, 'audit-traillist', 'Audit Trail List', '/audit/audittraillist', 'audit-traillist', 'user-menu', 7, true);
INSERT INTO menu_grp (menu_grp_id, menu_grp_parent, menu_code, menu_desc, menu_url, menu_reference, menu_grp_root, menu_id, menu_grp_enable) VALUES (8, 1, 'master-management', 'Master File Management', '/#', 'master-management', 'user-menu', 8, true);
INSERT INTO menu_grp (menu_grp_id, menu_grp_parent, menu_code, menu_desc, menu_url, menu_reference, menu_grp_root, menu_id, menu_grp_enable) VALUES (9, 8, 'company', 'Company', '/master/company', 'company', 'user-menu', 9, true);
INSERT INTO menu_grp (menu_grp_id, menu_grp_parent, menu_code, menu_desc, menu_url, menu_reference, menu_grp_root, menu_id, menu_grp_enable) VALUES (10, 8, 'store', 'Store', '/master/store', 'store', 'user-menu', 10, true);
INSERT INTO menu_grp (menu_grp_id, menu_grp_parent, menu_code, menu_desc, menu_url, menu_reference, menu_grp_root, menu_id, menu_grp_enable) VALUES (11, 8, 'department', 'Department', '/master/department', 'department', 'user-menu', 11, true);
INSERT INTO menu_grp (menu_grp_id, menu_grp_parent, menu_code, menu_desc, menu_url, menu_reference, menu_grp_root, menu_id, menu_grp_enable) VALUES (12, 8, 'profitcenter', 'Profit Center', '/master/profitcenter', 'profitcenter', 'user-menu', 12, true);
INSERT INTO menu_grp (menu_grp_id, menu_grp_parent, menu_code, menu_desc, menu_url, menu_reference, menu_grp_root, menu_id, menu_grp_enable) VALUES (13, 8, 'gst', 'Goods & Services Tax (GST)', '/master/gst', 'gst', 'user-menu', 13, true);
INSERT INTO menu_grp (menu_grp_id, menu_grp_parent, menu_code, menu_desc, menu_url, menu_reference, menu_grp_root, menu_id, menu_grp_enable) VALUES (14, 8, 'purchase-requisition', 'Purchase Requisition', '/master/purchase', 'purchase-requisition', 'user-menu', 14, true);
INSERT INTO menu_grp (menu_grp_id, menu_grp_parent, menu_code, menu_desc, menu_url, menu_reference, menu_grp_root, menu_id, menu_grp_enable) VALUES (15, 8, 'order-category', 'Order Category', '/master/ordercategory', 'order-category', 'user-menu', 15, true);
INSERT INTO menu_grp (menu_grp_id, menu_grp_parent, menu_code, menu_desc, menu_url, menu_reference, menu_grp_root, menu_id, menu_grp_enable) VALUES (16, 8, 'order-subcategory', 'Order Sub Category', '/master/ordersubcategory', 'order-subcategory', 'user-menu', 16, true);
INSERT INTO menu_grp (menu_grp_id, menu_grp_parent, menu_code, menu_desc, menu_url, menu_reference, menu_grp_root, menu_id, menu_grp_enable) VALUES (17, 8, 'scheduler', 'Scheduler', '/master/scheduler', 'scheduler', 'user-menu', 17, true);
INSERT INTO menu_grp (menu_grp_id, menu_grp_parent, menu_code, menu_desc, menu_url, menu_reference, menu_grp_root, menu_id, menu_grp_enable) VALUES (18, 1, 'employee-management', 'Employee Management', '/#', 'employee-management', 'user-menu', 18, true);
INSERT INTO menu_grp (menu_grp_id, menu_grp_parent, menu_code, menu_desc, menu_url, menu_reference, menu_grp_root, menu_id, menu_grp_enable) VALUES (19, 18, 'employee', 'Employee', '/employee/emplist', 'employee', 'user-menu', 19, true);
INSERT INTO menu_grp (menu_grp_id, menu_grp_parent, menu_code, menu_desc, menu_url, menu_reference, menu_grp_root, menu_id, menu_grp_enable) VALUES (20, 18, 'job-master', 'Job Master', '/employee/jobmaster', 'job-master', 'user-menu', 20, true);
INSERT INTO menu_grp (menu_grp_id, menu_grp_parent, menu_code, menu_desc, menu_url, menu_reference, menu_grp_root, menu_id, menu_grp_enable) VALUES (21, 1, 'system-configuration', 'System Configuration', '/#', 'system-configuration', 'user-menu', 21, true);
INSERT INTO menu_grp (menu_grp_id, menu_grp_parent, menu_code, menu_desc, menu_url, menu_reference, menu_grp_root, menu_id, menu_grp_enable) VALUES (22, 21, 'masterdata', 'Master Data', '/system/masterdata', 'masterdata', 'user-menu', 22, true);
INSERT INTO menu_grp (menu_grp_id, menu_grp_parent, menu_code, menu_desc, menu_url, menu_reference, menu_grp_root, menu_id, menu_grp_enable) VALUES (23, 21, 'system-parameter', 'System Parameter', '/system/parameter', 'system-parameter', 'user-menu', 23, true);


--
-- TOC entry 2464 (class 0 OID 96296)
-- Dependencies: 232
-- Data for Name: menu_grp_role; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO menu_grp_role (id, menu_grp_id, role_id) VALUES (1, 1, 1);


--
-- TOC entry 2489 (class 0 OID 0)
-- Dependencies: 231
-- Name: menu_grp_role_id_seq; Type: SEQUENCE SET; Schema: mfmaindb; Owner: postgres
--



--
-- TOC entry 2490 (class 0 OID 0)
-- Dependencies: 229
-- Name: menu_grp_seq; Type: SEQUENCE SET; Schema: mfmaindb; Owner: postgres
--



--
-- TOC entry 2491 (class 0 OID 0)
-- Dependencies: 227
-- Name: menu_seq; Type: SEQUENCE SET; Schema: mfmaindb; Owner: postgres
--



--
-- TOC entry 2448 (class 0 OID 88085)
-- Dependencies: 216
-- Data for Name: mf01_m_vendorbranch; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--



--
-- TOC entry 2449 (class 0 OID 88093)
-- Dependencies: 217
-- Data for Name: mf02_m_creditterms; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--



--
-- TOC entry 2450 (class 0 OID 88101)
-- Dependencies: 218
-- Data for Name: mf03_m_vendorcrterms; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--



--
-- TOC entry 2451 (class 0 OID 88109)
-- Dependencies: 219
-- Data for Name: mf04_m_profitcenterlastponumber; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--



--
-- TOC entry 2492 (class 0 OID 0)
-- Dependencies: 204
-- Name: mf06_r_mastdata_sequence; Type: SEQUENCE SET; Schema: mfmaindb; Owner: postgres
--



--
-- TOC entry 2493 (class 0 OID 0)
-- Dependencies: 205
-- Name: mf07_r_sysparam_sequence; Type: SEQUENCE SET; Schema: mfmaindb; Owner: postgres
--



--
-- TOC entry 2458 (class 0 OID 96252)
-- Dependencies: 226
-- Data for Name: mf12_m_empmast; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--



--
-- TOC entry 2452 (class 0 OID 88117)
-- Dependencies: 220
-- Data for Name: mf22_m_scheduler; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--



--
-- TOC entry 2494 (class 0 OID 0)
-- Dependencies: 207
-- Name: mf22_m_scheduler_mf22_schedulerid_seq; Type: SEQUENCE SET; Schema: mfmaindb; Owner: postgres
--



--
-- TOC entry 2465 (class 0 OID 96300)
-- Dependencies: 233
-- Data for Name: permissions; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO permissions (id, description, name) VALUES (1, NULL, 'MANAGE_AUDIT');
INSERT INTO permissions (id, description, name) VALUES (2, NULL, 'MANAGE_COMPANY');
INSERT INTO permissions (id, description, name) VALUES (3, NULL, 'MANAGE_DEPARTMENT');
INSERT INTO permissions (id, description, name) VALUES (4, NULL, 'MANAGE_EMPLOYEE_MANAGEMENT');
INSERT INTO permissions (id, description, name) VALUES (5, NULL, 'MANAGE_GST');
INSERT INTO permissions (id, description, name) VALUES (6, NULL, 'MANAGE_ORDER');
INSERT INTO permissions (id, description, name) VALUES (7, NULL, 'MANAGE_ROLE');
INSERT INTO permissions (id, description, name) VALUES (8, NULL, 'MANAGE_PERMISSION');
INSERT INTO permissions (id, description, name) VALUES (9, NULL, 'MANAGE_SETTINGS');
INSERT INTO permissions (id, description, name) VALUES (10, NULL, 'MANAGE_PROFIT_CENTER');
INSERT INTO permissions (id, description, name) VALUES (11, NULL, 'MANAGE_PURCHASE');
INSERT INTO permissions (id, description, name) VALUES (12, NULL, 'MANAGE_SCHEDULER');
INSERT INTO permissions (id, description, name) VALUES (13, NULL, 'MANAGE_STORE');
INSERT INTO permissions (id, description, name) VALUES (14, NULL, 'MANAGE_SYSTEM_PARAMETER');
INSERT INTO permissions (id, description, name) VALUES (15, NULL, 'MANAGE_USER');


--
-- TOC entry 2453 (class 0 OID 88125)
-- Dependencies: 221
-- Data for Name: po06_t_user; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--



--
-- TOC entry 2454 (class 0 OID 88133)
-- Dependencies: 222
-- Data for Name: po_dblist2; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--



--
-- TOC entry 2495 (class 0 OID 0)
-- Dependencies: 223
-- Name: podblist__seq; Type: SEQUENCE SET; Schema: mfmaindb; Owner: postgres
--



--
-- TOC entry 2496 (class 0 OID 0)
-- Dependencies: 209
-- Name: pologinlist__seq; Type: SEQUENCE SET; Schema: mfmaindb; Owner: postgres
--



--
-- TOC entry 2466 (class 0 OID 96306)
-- Dependencies: 234
-- Data for Name: role_permission; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO role_permission (role_id, perm_id) VALUES (1, 1);
INSERT INTO role_permission (role_id, perm_id) VALUES (1, 2);
INSERT INTO role_permission (role_id, perm_id) VALUES (1, 3);
INSERT INTO role_permission (role_id, perm_id) VALUES (1, 4);
INSERT INTO role_permission (role_id, perm_id) VALUES (1, 5);
INSERT INTO role_permission (role_id, perm_id) VALUES (1, 6);
INSERT INTO role_permission (role_id, perm_id) VALUES (1, 7);
INSERT INTO role_permission (role_id, perm_id) VALUES (1, 8);
INSERT INTO role_permission (role_id, perm_id) VALUES (1, 9);
INSERT INTO role_permission (role_id, perm_id) VALUES (1, 10);
INSERT INTO role_permission (role_id, perm_id) VALUES (1, 11);
INSERT INTO role_permission (role_id, perm_id) VALUES (1, 12);
INSERT INTO role_permission (role_id, perm_id) VALUES (1, 13);
INSERT INTO role_permission (role_id, perm_id) VALUES (1, 14);
INSERT INTO role_permission (role_id, perm_id) VALUES (1, 15);
INSERT INTO role_permission (role_id, perm_id) VALUES (2, 1);
INSERT INTO role_permission (role_id, perm_id) VALUES (2, 2);
INSERT INTO role_permission (role_id, perm_id) VALUES (2, 3);
INSERT INTO role_permission (role_id, perm_id) VALUES (2, 4);
INSERT INTO role_permission (role_id, perm_id) VALUES (2, 5);
INSERT INTO role_permission (role_id, perm_id) VALUES (2, 6);
INSERT INTO role_permission (role_id, perm_id) VALUES (2, 9);
INSERT INTO role_permission (role_id, perm_id) VALUES (2, 10);
INSERT INTO role_permission (role_id, perm_id) VALUES (2, 11);
INSERT INTO role_permission (role_id, perm_id) VALUES (2, 12);
INSERT INTO role_permission (role_id, perm_id) VALUES (2, 13);
INSERT INTO role_permission (role_id, perm_id) VALUES (2, 14);
INSERT INTO role_permission (role_id, perm_id) VALUES (3, 1);
INSERT INTO role_permission (role_id, perm_id) VALUES (3, 2);
INSERT INTO role_permission (role_id, perm_id) VALUES (3, 3);
INSERT INTO role_permission (role_id, perm_id) VALUES (3, 4);
INSERT INTO role_permission (role_id, perm_id) VALUES (3, 5);
INSERT INTO role_permission (role_id, perm_id) VALUES (3, 6);
INSERT INTO role_permission (role_id, perm_id) VALUES (3, 11);
INSERT INTO role_permission (role_id, perm_id) VALUES (3, 12);


--
-- TOC entry 2467 (class 0 OID 96309)
-- Dependencies: 235
-- Data for Name: roles; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO roles (role_id, role_description, role_name) VALUES (1, NULL, 'ROLE_SUPER_ADMIN');
INSERT INTO roles (role_id, role_description, role_name) VALUES (2, NULL, 'ROLE_ADMIN');
INSERT INTO roles (role_id, role_description, role_name) VALUES (3, NULL, 'ROLE_CMS_ADMIN');
INSERT INTO roles (role_id, role_description, role_name) VALUES (4, NULL, 'ROLE_USER');


--
-- TOC entry 2468 (class 0 OID 96315)
-- Dependencies: 236
-- Data for Name: user_role; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--

INSERT INTO user_role (user_id, role_id) VALUES ('superadmin', 1);
INSERT INTO user_role (user_id, role_id) VALUES ('superadmin', 2);
INSERT INTO user_role (user_id, role_id) VALUES ('superadmin', 3);
INSERT INTO user_role (user_id, role_id) VALUES ('superadmin', 4);


--
-- TOC entry 2469 (class 0 OID 96318)
-- Dependencies: 237
-- Data for Name: users; Type: TABLE DATA; Schema: mfmaindb; Owner: postgres
--




--
-- TOC entry 2456 (class 0 OID 88146)
-- Dependencies: 224
-- Data for Name: po01_t_main; Type: TABLE DATA; Schema: pocomptrans; Owner: postgres
--




--
-- TOC entry 2215 (class 2606 OID 87591)
-- Name: LD02_T_ACSLOG_pkey; Type: CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY "LD02_T_ACSLOG"
    ADD CONSTRAINT "LD02_T_ACSLOG_pkey" PRIMARY KEY ("ld02_logId");


--
-- TOC entry 2217 (class 2606 OID 87593)
-- Name: LD03_T_STGLOG_pkey; Type: CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY "LD03_T_STGLOG"
    ADD CONSTRAINT "LD03_T_STGLOG_pkey" PRIMARY KEY ("ld03_logSettingId");


--
-- TOC entry 2221 (class 2606 OID 87595)
-- Name: MF02_M_STRMAST_pkey; Type: CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY "MF02_M_STRMAST"
    ADD CONSTRAINT "MF02_M_STRMAST_pkey" PRIMARY KEY ("mf02_storeCode");


--
-- TOC entry 2223 (class 2606 OID 87597)
-- Name: MF06_R_MASTDATA_pkey; Type: CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY "MF06_R_MASTDATA"
    ADD CONSTRAINT "MF06_R_MASTDATA_pkey" PRIMARY KEY ("mf06_mastDataId");


--
-- TOC entry 2225 (class 2606 OID 87599)
-- Name: MF07_R_SYSPARAM_pkey; Type: CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY "MF07_R_SYSPARAM"
    ADD CONSTRAINT "MF07_R_SYSPARAM_pkey" PRIMARY KEY ("mf07_sysParamCode");


--
-- TOC entry 2227 (class 2606 OID 87601)
-- Name: MF11_M_COMPMAST_mf11_compCodeHRIS_key; Type: CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY "MF11_M_COMPMAST"
    ADD CONSTRAINT "MF11_M_COMPMAST_mf11_compCodeHRIS_key" UNIQUE ("mf11_compCodeHRIS");


--
-- TOC entry 2229 (class 2606 OID 87603)
-- Name: MF11_M_COMPMAST_pkey; Type: CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY "MF11_M_COMPMAST"
    ADD CONSTRAINT "MF11_M_COMPMAST_pkey" PRIMARY KEY ("mf11_compCode");


--
-- TOC entry 2233 (class 2606 OID 87605)
-- Name: MF12_M_EMPMAST_pkey; Type: CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY "MF12_M_EMPMAST"
    ADD CONSTRAINT "MF12_M_EMPMAST_pkey" PRIMARY KEY ("mf12_psId");


--
-- TOC entry 2235 (class 2606 OID 87607)
-- Name: MF13_M_PCMAST_pkey; Type: CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY "MF13_M_PCMAST"
    ADD CONSTRAINT "MF13_M_PCMAST_pkey" PRIMARY KEY ("mf13_pcCode");


--
-- TOC entry 2237 (class 2606 OID 87609)
-- Name: MF14_M_DEPTMAST_pkey; Type: CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY "MF14_M_DEPTMAST"
    ADD CONSTRAINT "MF14_M_DEPTMAST_pkey" PRIMARY KEY ("mf14_deptCode");


--
-- TOC entry 2239 (class 2606 OID 87611)
-- Name: MF15_M_PRMAST_pkey; Type: CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY "MF15_M_PRMAST"
    ADD CONSTRAINT "MF15_M_PRMAST_pkey" PRIMARY KEY ("mf15_prTypeCode");


--
-- TOC entry 2241 (class 2606 OID 87613)
-- Name: MF16_M_GSTMAST_pkey; Type: CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY "MF16_M_GSTMAST"
    ADD CONSTRAINT "MF16_M_GSTMAST_pkey" PRIMARY KEY ("mf16_taxCode");


--
-- TOC entry 2243 (class 2606 OID 87615)
-- Name: MF17_M_ORDCATMAST_pkey; Type: CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY "MF17_M_ORDCATMAST"
    ADD CONSTRAINT "MF17_M_ORDCATMAST_pkey" PRIMARY KEY ("mf17_ordCatCode");


--
-- TOC entry 2245 (class 2606 OID 87617)
-- Name: MF18_M_ORDSUBCMAST_pkey; Type: CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY "MF18_M_ORDSUBCMAST"
    ADD CONSTRAINT "MF18_M_ORDSUBCMAST_pkey" PRIMARY KEY ("mf18_ordSubCCode");


--
-- TOC entry 2247 (class 2606 OID 87619)
-- Name: MF19_M_JOBMAST_pkey; Type: CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY "MF19_M_JOBMAST"
    ADD CONSTRAINT "MF19_M_JOBMAST_pkey" PRIMARY KEY ("mf19_jobCode");


--
-- TOC entry 2249 (class 2606 OID 87621)
-- Name: MF20_M_ADMINMAST_pkey; Type: CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY "MF20_M_ADMINMAST"
    ADD CONSTRAINT "MF20_M_ADMINMAST_pkey" PRIMARY KEY (mf20_username);


--
-- TOC entry 2279 (class 2606 OID 88197)
-- Name: PO01_T_MAIN_pkey; Type: CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY "PO01_T_MAIN"
    ADD CONSTRAINT "PO01_T_MAIN_pkey" PRIMARY KEY ("po01_poNo");


--
-- TOC entry 2219 (class 2606 OID 87623)
-- Name: audit_pkey; Type: CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY "LD04_T_AUDIT"
    ADD CONSTRAINT audit_pkey PRIMARY KEY ("ld04_auditId");


--
-- TOC entry 2257 (class 2606 OID 88071)
-- Name: audit_pkey1; Type: CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY audit
    ADD CONSTRAINT audit_pkey1 PRIMARY KEY (auditid);


--
-- TOC entry 2259 (class 2606 OID 88079)
-- Name: ld02_t_acslog_pkey; Type: CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY ld02_t_acslog
    ADD CONSTRAINT ld02_t_acslog_pkey PRIMARY KEY (ld02_logid);


--
-- TOC entry 2261 (class 2606 OID 88084)
-- Name: ld03_t_stglog_pkey; Type: CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY ld03_t_stglog
    ADD CONSTRAINT ld03_t_stglog_pkey PRIMARY KEY (ld03_logsettingid);


--
-- TOC entry 2263 (class 2606 OID 88092)
-- Name: mf01_m_vendorbranch_pkey; Type: CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY mf01_m_vendorbranch
    ADD CONSTRAINT mf01_m_vendorbranch_pkey PRIMARY KEY (mf01_vendorcode, mf01_vendorbranchcode);


--
-- TOC entry 2265 (class 2606 OID 88100)
-- Name: mf02_m_creditterms_pkey; Type: CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY mf02_m_creditterms
    ADD CONSTRAINT mf02_m_creditterms_pkey PRIMARY KEY (mf02_credittermscode);


--
-- TOC entry 2267 (class 2606 OID 88108)
-- Name: mf03_m_vendorcrterms_pkey; Type: CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY mf03_m_vendorcrterms
    ADD CONSTRAINT mf03_m_vendorcrterms_pkey PRIMARY KEY (mf02_creditterms_mf02_credittermscode, mf01_vendorbranch_mf01_vendorcode, mf01_vendorbranch_mf01_vendorbranchcode);


--
-- TOC entry 2269 (class 2606 OID 88116)
-- Name: mf04_m_profitcenterlastponumber_pkey; Type: CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY mf04_m_profitcenterlastponumber
    ADD CONSTRAINT mf04_m_profitcenterlastponumber_pkey PRIMARY KEY (mf13mpcmast_mf04_pccode);


--
-- TOC entry 2281 (class 2606 OID 96259)
-- Name: mf12_m_empmast_pkey; Type: CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY mf12_m_empmast
    ADD CONSTRAINT mf12_m_empmast_pkey PRIMARY KEY ("mf12_psId");


--
-- TOC entry 2251 (class 2606 OID 87625)
-- Name: mf22_m_scheduler_pkey; Type: CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY "Mf22_M_SCHEDULER"
    ADD CONSTRAINT mf22_m_scheduler_pkey PRIMARY KEY (mf22_schedulerid);


--
-- TOC entry 2271 (class 2606 OID 88124)
-- Name: mf22_m_scheduler_pkey1; Type: CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY mf22_m_scheduler
    ADD CONSTRAINT mf22_m_scheduler_pkey1 PRIMARY KEY (mf22_schedulerid);


--
-- TOC entry 2255 (class 2606 OID 87627)
-- Name: pkey_new; Type: CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY "PO01_LOGINLIST"
    ADD CONSTRAINT pkey_new PRIMARY KEY (po01_loginlist_seq);


--
-- TOC entry 2253 (class 2606 OID 87629)
-- Name: po06_t_user_pkey; Type: CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY "PO06_T_USER"
    ADD CONSTRAINT po06_t_user_pkey PRIMARY KEY (po06_psid);


--
-- TOC entry 2273 (class 2606 OID 88132)
-- Name: po06_t_user_pkey1; Type: CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY po06_t_user
    ADD CONSTRAINT po06_t_user_pkey1 PRIMARY KEY (po06_psid);


--
-- TOC entry 2275 (class 2606 OID 88140)
-- Name: po_dblist2_pkey; Type: CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY po_dblist2
    ADD CONSTRAINT po_dblist2_pkey PRIMARY KEY (dblistseq);


--
-- TOC entry 2231 (class 2606 OID 88142)
-- Name: uk_qsuapl4l7d6g2sjbamje97fsc; Type: CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY "MF11_M_COMPMAST"
    ADD CONSTRAINT uk_qsuapl4l7d6g2sjbamje97fsc UNIQUE ("mf11_compCodeHRIS");



--
-- TOC entry 2277 (class 2606 OID 88153)
-- Name: po01_t_main_pkey; Type: CONSTRAINT; Schema: pocomptrans; Owner: postgres
--

ALTER TABLE ONLY po01_t_main
    ADD CONSTRAINT po01_t_main_pkey PRIMARY KEY (po01_pono);



--
-- TOC entry 2282 (class 2606 OID 87630)
-- Name: MF02_M_STRMAST_mf11MCompmast_mf11_compCode_fkey; Type: FK CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY "MF02_M_STRMAST"
    ADD CONSTRAINT "MF02_M_STRMAST_mf11MCompmast_mf11_compCode_fkey" FOREIGN KEY ("mf11MCompmast_mf11_compCode") REFERENCES "MF11_M_COMPMAST"("mf11_compCode");


--
-- TOC entry 2283 (class 2606 OID 87635)
-- Name: MF12_M_EMPMAST_mf11MCompmast_mf11_compCodeHRIS_fkey; Type: FK CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY "MF12_M_EMPMAST"
    ADD CONSTRAINT "MF12_M_EMPMAST_mf11MCompmast_mf11_compCodeHRIS_fkey" FOREIGN KEY ("mf11MCompmast_mf11_compCodeHRIS") REFERENCES "MF11_M_COMPMAST"("mf11_compCodeHRIS");


--
-- TOC entry 2284 (class 2606 OID 87640)
-- Name: MF12_M_EMPMAST_mf13MPcmast_mf13_pcCode_fkey; Type: FK CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY "MF12_M_EMPMAST"
    ADD CONSTRAINT "MF12_M_EMPMAST_mf13MPcmast_mf13_pcCode_fkey" FOREIGN KEY ("mf13MPcmast_mf13_pcCode") REFERENCES "MF13_M_PCMAST"("mf13_pcCode");


--
-- TOC entry 2285 (class 2606 OID 87645)
-- Name: MF12_M_EMPMAST_mf19MJobmast_mf19_jobCode_fkey; Type: FK CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY "MF12_M_EMPMAST"
    ADD CONSTRAINT "MF12_M_EMPMAST_mf19MJobmast_mf19_jobCode_fkey" FOREIGN KEY ("mf19MJobmast_mf19_jobCode") REFERENCES "MF19_M_JOBMAST"("mf19_jobCode");


--
-- TOC entry 2289 (class 2606 OID 87650)
-- Name: MF13_M_PCMAST_mf02MStrmast_mf02_storeCode_fkey; Type: FK CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY "MF13_M_PCMAST"
    ADD CONSTRAINT "MF13_M_PCMAST_mf02MStrmast_mf02_storeCode_fkey" FOREIGN KEY ("mf02MStrmast_mf02_storeCode") REFERENCES "MF02_M_STRMAST"("mf02_storeCode");


--
-- TOC entry 2290 (class 2606 OID 87655)
-- Name: MF13_M_PCMAST_mf11MCompmast_mf11_compCode_fkey; Type: FK CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY "MF13_M_PCMAST"
    ADD CONSTRAINT "MF13_M_PCMAST_mf11MCompmast_mf11_compCode_fkey" FOREIGN KEY ("mf11MCompmast_mf11_compCode") REFERENCES "MF11_M_COMPMAST"("mf11_compCode");


--
-- TOC entry 2291 (class 2606 OID 87660)
-- Name: MF13_M_PCMAST_mf14MDeptmast_mf14_deptCode_fkey; Type: FK CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY "MF13_M_PCMAST"
    ADD CONSTRAINT "MF13_M_PCMAST_mf14MDeptmast_mf14_deptCode_fkey" FOREIGN KEY ("mf14MDeptmast_mf14_deptCode") REFERENCES "MF14_M_DEPTMAST"("mf14_deptCode");


--
-- TOC entry 2295 (class 2606 OID 87665)
-- Name: MF17_M_ORDCATMAST_mf15mprmast_mf15_prtypecode_fkey; Type: FK CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY "MF17_M_ORDCATMAST"
    ADD CONSTRAINT "MF17_M_ORDCATMAST_mf15mprmast_mf15_prtypecode_fkey" FOREIGN KEY ("mf15MPrmast_mf15_prTypeCode") REFERENCES "MF15_M_PRMAST"("mf15_prTypeCode");


--
-- TOC entry 2296 (class 2606 OID 87670)
-- Name: MF18_M_ORDSUBCMAST_mf17mordcatmast_mf17_ordcatcode_fkey; Type: FK CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY "MF18_M_ORDSUBCMAST"
    ADD CONSTRAINT "MF18_M_ORDSUBCMAST_mf17mordcatmast_mf17_ordcatcode_fkey" FOREIGN KEY ("mf17MOrdcatmast_mf17_ordCatCode") REFERENCES "MF17_M_ORDCATMAST"("mf17_ordCatCode");


--
-- TOC entry 2298 (class 2606 OID 96260)
-- Name: fk162bv2mt3b7827lu4q68wlpn1; Type: FK CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY mf12_m_empmast
    ADD CONSTRAINT fk162bv2mt3b7827lu4q68wlpn1 FOREIGN KEY ("mf11MCompmast_mf11_compCodeHRIS") REFERENCES "MF11_M_COMPMAST"("mf11_compCodeHRIS");


--
-- TOC entry 2294 (class 2606 OID 88179)
-- Name: fk2akc8g8uoc0oa0aukljipks4d; Type: FK CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY "MF13_M_PCMAST"
    ADD CONSTRAINT fk2akc8g8uoc0oa0aukljipks4d FOREIGN KEY ("mf14MDeptmast_mf14_deptCode") REFERENCES "MF14_M_DEPTMAST"("mf14_deptCode");


--
-- TOC entry 2300 (class 2606 OID 96270)
-- Name: fk4v3jha1yk68kpgumvn75kmd5c; Type: FK CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY mf12_m_empmast
    ADD CONSTRAINT fk4v3jha1yk68kpgumvn75kmd5c FOREIGN KEY ("mf19MJobmast_mf19_jobCode") REFERENCES "MF19_M_JOBMAST"("mf19_jobCode");


--
-- TOC entry 2292 (class 2606 OID 88169)
-- Name: fk74usyo6dof7tra76n0b1rxp79; Type: FK CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY "MF13_M_PCMAST"
    ADD CONSTRAINT fk74usyo6dof7tra76n0b1rxp79 FOREIGN KEY ("mf02MStrmast_mf02_storeCode") REFERENCES "MF02_M_STRMAST"("mf02_storeCode");


--
-- TOC entry 2287 (class 2606 OID 88159)
-- Name: fk8pym919200bfogdchsyhp904y; Type: FK CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY "MF12_M_EMPMAST"
    ADD CONSTRAINT fk8pym919200bfogdchsyhp904y FOREIGN KEY ("mf13MPcmast_mf13_pcCode") REFERENCES "MF13_M_PCMAST"("mf13_pcCode");


--
-- TOC entry 2293 (class 2606 OID 88174)
-- Name: fk8xtu8raqpqnxxfexh4ggsjy19; Type: FK CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY "MF13_M_PCMAST"
    ADD CONSTRAINT fk8xtu8raqpqnxxfexh4ggsjy19 FOREIGN KEY ("mf11MCompmast_mf11_compCode") REFERENCES "MF11_M_COMPMAST"("mf11_compCode");


--
-- TOC entry 2297 (class 2606 OID 88184)
-- Name: fkaf3qjhwx7n0b6y93ses502atc; Type: FK CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY "MF18_M_ORDSUBCMAST"
    ADD CONSTRAINT fkaf3qjhwx7n0b6y93ses502atc FOREIGN KEY ("mf17MOrdcatmast_mf17_ordCatCode") REFERENCES "MF17_M_ORDCATMAST"("mf17_ordCatCode");


--
-- TOC entry 2299 (class 2606 OID 96265)
-- Name: fkc9ku5gf7vieecmba3g5o1tbqf; Type: FK CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY mf12_m_empmast
    ADD CONSTRAINT fkc9ku5gf7vieecmba3g5o1tbqf FOREIGN KEY ("mf13MPcmast_mf13_pcCode") REFERENCES "MF13_M_PCMAST"("mf13_pcCode");


--
-- TOC entry 2286 (class 2606 OID 88154)
-- Name: fkcyocjkbxxqfvcr686q46sqevm; Type: FK CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY "MF12_M_EMPMAST"
    ADD CONSTRAINT fkcyocjkbxxqfvcr686q46sqevm FOREIGN KEY ("mf11MCompmast_mf11_compCodeHRIS") REFERENCES "MF11_M_COMPMAST"("mf11_compCodeHRIS");


--
-- TOC entry 2288 (class 2606 OID 88164)
-- Name: fkgwwd3ag7h696nmkaux3xpyn9d; Type: FK CONSTRAINT; Schema: mfmaindb; Owner: postgres
--

ALTER TABLE ONLY "MF12_M_EMPMAST"
    ADD CONSTRAINT fkgwwd3ag7h696nmkaux3xpyn9d FOREIGN KEY ("mf19MJobmast_mf19_jobCode") REFERENCES "MF19_M_JOBMAST"("mf19_jobCode");


