-- **************************************
-- *         CREATING TABLES			*
-- *------------------------------------*
-- *	LD02_T_ACSLOG					*
-- *	LD03_T_STGLOG					*
-- *	LD04_T_AUDIT					*
-- *	MF02_M_STRMAST					*
-- *	MF06_R_MASTDATA					*
-- *	MF07_R_SYSPARAM					*
-- *	MF11_M_COMPMAST					*
-- *	MF12_M_EMPMAST					*
-- *	MF13_M_PCMAST					*
-- *	MF14_M_DEPTMAST					*
-- *	MF15_M_PRMAST					*
-- *	MF16_M_GSTMAST					*
-- *	MF17_M_ORDCATMAST				*
-- *	MF18_M_ORDSUBCMAST				*
-- *	MF19_M_JOBMAST					*
-- *	MF20_M_ADMINMAST				*
-- *	Mf22_M_SCHEDULER				*
-- *	PO06_T_USER						*
-- *	PO01_LOGINLIST					*
-- *	PO01_T_MAIN						*
-- *	menu							*
-- *	menu_grp						*
-- *	menu_grp_role					*
-- *	permissions						*
-- *	role_permission					*
-- *	roles							*
-- *	user_role						*
-- *	users							*
-- *  --------------------------------  *
-- *	hibernate_sequence				*
-- **************************************
  

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;
SET default_tablespace = '';

CREATE SCHEMA "mfmaindb";
ALTER SCHEMA "mfmaindb" OWNER TO "postgres";


SET search_path = "mfmaindb", pg_catalog;
SET default_with_oids = false;



--*********************
--   LD02_T_ACSLOG
--*********************

CREATE SEQUENCE "ld02_t_acslog_sequence"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE "ld02_t_acslog_sequence" OWNER TO "postgres";

CREATE TABLE "LD02_T_ACSLOG" (
    "ld02_logId" integer DEFAULT "nextval"('"ld02_t_acslog_sequence"'::"regclass") NOT NULL,
    "ld02_classFileName" character varying(255),
    "ld02_lineNo" integer NOT NULL,
    "ld02_logLevel" character varying(255),
    "ld02_logMessage" character varying(511),
    "ld02_logTime" timestamp without time zone,
    "ld02_screenId" character varying(100)
);
ALTER TABLE "LD02_T_ACSLOG" OWNER TO "postgres";







--*********************
--   LD03_T_STGLOG
--*********************

CREATE SEQUENCE "ld03_t_stglog_sequence"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE "ld03_t_stglog_sequence" OWNER TO "postgres";

CREATE TABLE "LD03_T_STGLOG" (
    "ld03_logSetting" integer,
    "ld03_logSettingId" integer DEFAULT "nextval"('"ld03_t_stglog_sequence"'::"regclass") NOT NULL
);
ALTER TABLE "LD03_T_STGLOG" OWNER TO "postgres";






--*********************
--   LD04_T_AUDIT
--*********************

CREATE SEQUENCE "audit_auditid_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE "audit_auditid_seq" OWNER TO "postgres";

CREATE TABLE "LD04_T_AUDIT" (
    "ld04_auditId" integer DEFAULT "nextval"('"audit_auditid_seq"'::"regclass") NOT NULL,
    "ld04_username" "text",
    "ld04_message" "text",
    "ld04_operation" "text",
    "ld04_createdOn" timestamp without time zone
);
ALTER TABLE "LD04_T_AUDIT" OWNER TO "postgres";








--*********************
--   MF02_M_STRMAST
--*********************

CREATE TABLE "MF02_M_STRMAST" (
    "mf02_storeCode" character varying(4) NOT NULL,
    "mf02_storeAbbrName" character varying(15),
    "mf02_storeName" character varying(30),
    "mf11MCompmast_mf11_compCode" character varying(3) NOT NULL,
    "mf02_createdBy" character varying(255) NOT NULL,
    "mf02_createdOn" timestamp without time zone NOT NULL,
    "mf02_lastModifiedBy" character varying(255) NOT NULL,
    "mf02_lastModifiedOn" timestamp without time zone NOT NULL,
    "mf02_deactivatedBy" character varying(255),
    "mf02_deactivatedOn" timestamp without time zone,
    "mf02_reactivatedBy" character varying(255),
    "mf02_reactivatedOn" timestamp without time zone,
    "mf02_isActive" boolean NOT NULL,
    "mf02_jsonField" "json"
);
ALTER TABLE "MF02_M_STRMAST" OWNER TO "postgres";









--*********************
--   MF06_R_MASTDATA
--*********************

CREATE SEQUENCE "MF06_R_MASTDATA_mf06_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE "MF06_R_MASTDATA_mf06_seq" OWNER TO "postgres";

CREATE TABLE "MF06_R_MASTDATA" (
    "mf06_codeDesc" character varying(255) NOT NULL,
    "mf06_codeType" character varying(45) NOT NULL,
    "mf06_codeValue" character varying(255) NOT NULL,
    "mf06_createdBy" character varying(255) NOT NULL,
    "mf06_createdOn" timestamp without time zone NOT NULL,
    "mf06_isActive" boolean NOT NULL,
    "mf06_lastModifiedBy" character varying(255) NOT NULL,
    "mf06_lastModifiedOn" timestamp without time zone NOT NULL,
    "mf06_sysName" character varying(10),
    "mf06_jsonField" "json",
    "mf06_deactivatedBy" character varying(255),
    "mf06_reactivatedBy" character varying(255),
    "mf06_reactivatedOn" timestamp with time zone,
    "mf06_deactivatedOn" timestamp with time zone,
    "mf06_mastDataId" bigint DEFAULT "nextval"('"MF06_R_MASTDATA_mf06_seq"'::"regclass") NOT NULL
);
ALTER TABLE "MF06_R_MASTDATA" OWNER TO "postgres";










--*********************
--   MF07_R_SYSPARAM
--*********************

CREATE SEQUENCE "mf07_r_sysparam_sequence"
    START WITH 50
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE "mf07_r_sysparam_sequence" OWNER TO "postgres";

CREATE TABLE "MF07_R_SYSPARAM" (
    "mf07_createdBy" character varying(255) NOT NULL,
    "mf07_createdOn" timestamp without time zone NOT NULL,
    "mf07_desc" character varying(255),
    "mf07_isActive" boolean NOT NULL,
    "mf07_lastModifiedBy" character varying(255) NOT NULL,
    "mf07_lastModifiedOn" timestamp without time zone NOT NULL,
    "mf07_propName" character varying(30) NOT NULL,
    "mf07_propValue" character varying(255) NOT NULL,
    "mf07_sysName" character varying(10),
    "mf07_jsonField" "json",
    "mf07_deactivatedBy" character varying(255),
    "mf07_reactivatedBy" character varying(255),
    "mf07_reactivatedOn" timestamp with time zone,
    "mf07_deactivatedOn" timestamp with time zone,
    "mf07_sysParamCode" bigint DEFAULT "nextval"('"mf07_r_sysparam_sequence"'::"regclass") NOT NULL
);
ALTER TABLE "MF07_R_SYSPARAM" OWNER TO "postgres";










--*********************
--   MF11_M_COMPMAST
--*********************

CREATE TABLE "MF11_M_COMPMAST" (
    "mf11_compCode" character varying(3) NOT NULL,
    "mf11_compName" character varying(70) NOT NULL,
    "mf11_compRegNo" character varying(20),
    "mf11_createdBy" character varying(255) NOT NULL,
    "mf11_createdOn" timestamp without time zone NOT NULL,
    "mf11_isActive" boolean NOT NULL,
    "mf11_lastModifiedBy" character varying(255) NOT NULL,
    "mf11_lastModifiedOn" timestamp without time zone NOT NULL,
    "mf11_compCodeHRIS" character varying(3) NOT NULL,
    "mf11_compAbbrName" character varying(15),
    "mf11_compLogo" character varying(255),
    "mf11_companyLetterHeading" character varying(255),
    "mf11_deactivatedBy" character varying(255),
    "mf11_deactivatedOn" timestamp without time zone,
    "mf11_reactivatedBy" character varying(255),
    "mf11_reactivatedOn" timestamp without time zone,
    "mf11_jsonField" "json",
    "mf11_poTAndCPortrait" character varying(255),
    "mf11_poTAndCLandscape" character varying(255)
);
ALTER TABLE "MF11_M_COMPMAST" OWNER TO "postgres";








--*********************
--   MF12_M_EMPMAST
--*********************

CREATE TABLE "MF12_M_EMPMAST" (
    "mf12_psId" character varying(11) NOT NULL,
    "mf12_altId" character varying(11),
    "mf12_badgeNo" character varying(20),
    "mf12_confirmDt" "date",
    "mf12_createdBy" character varying(255) NOT NULL,
    "mf12_createdOn" timestamp without time zone NOT NULL,
    "mf12_deptDescHRIS" character varying(30),
    "mf12_deptCodeHRIS" character varying(10),
    "mf12_dob" "date",
    "mf12_email" character varying(70),
    "mf12_empCat" character(1),
    "mf12_empName" character varying(50),
    "mf12_ethnic" character varying(8),
    "mf12_ext" character varying(6),
    "mf12_foreignLocal" character(1),
    "mf12_frmCompCode" character varying(3),
    "mf12_frmLoc" character varying(10),
    "mf12_gender" character(1),
    "mf12_hireDt" "date",
    "mf12_hrStatus" character(1),
    "mf12_isactive" boolean NOT NULL,
    "mf12_lastModifiedBy" character varying(255) NOT NULL,
    "mf12_lastModifiedOn" timestamp without time zone NOT NULL,
    "mf12_loc" character varying(10),
    "mf12_locDesc" character varying(30),
    "mf12_lstDtWrk" "date",
    "mf12_lstUpdDtTm" timestamp without time zone,
    "mf12_maritalStat" character(1),
    "mf12_newICNo" character varying(20),
    "mf12_oldICNo" character varying(20),
    "mf12_passportNo" character varying(15),
    "mf12_payGrp" character varying(10),
    "mf12_payrollComp" character varying(3),
    "mf12_payrollStatus" character(1),
    "mf12_phoneNo" character varying(24),
    "mf12_posNo" character varying(8),
    "mf12_posTitle" character varying(30),
    "mf12_prcDtTm" timestamp without time zone,
    "mf12_prefName" character varying(50),
    "mf12_rptToEmpId" character varying(11),
    "mf12_rptToPosNo" character varying(8),
    "mf12_salaryGrade" character varying(3),
    "mf12_salaryPlan" character varying(4),
    "mf12_toCompCode" character varying(3),
    "mf12_toLoc" character varying(10),
    "mf12_trfDt" "date",
    "mf12_trfSndFlag" character(1),
    "mf12_jsonField" "json",
    "mf12_expJobEndDt" timestamp without time zone,
    "mf11MCompmast_mf11_compCodeHRIS" character varying(3),
    "mf13MPcmast_mf13_pcCode" character varying(7),
    "mf19MJobmast_mf19_jobCode" character varying(6),
    "mf12_fromCostChargeCompany" character varying(3),
    "mf12_fromProfitCentre" character varying(15),
    "mf12_toCostChargeCompany" character varying(3),
    "mf12_toProfitCentre" character varying(15)
);
ALTER TABLE "MF12_M_EMPMAST" OWNER TO "postgres";








--*********************
--   MF13_M_PCMAST
--*********************

CREATE TABLE "MF13_M_PCMAST" (
    "mf13_createdBy" character varying(255) NOT NULL,
    "mf13_createdOn" timestamp without time zone NOT NULL,
    "mf13_lastModifiedBy" character varying(255) NOT NULL,
    "mf13_lastModifiedOn" timestamp without time zone NOT NULL,
    "mf13_pcCode" character varying(7) NOT NULL,
    "mf13_pcName" character varying(30),
    "mf13_isActive" boolean NOT NULL,
    "mf13_addressLine1" character varying(70),
    "mf13_addressLine2" character varying(70),
    "mf13_addressLine3" character varying(70),
    "mf13_postCode" character varying(10),
    "mf13_postArea" character varying(70),
    "mf13_pcHead" "text",
    "mf13_pcViewRights" "text",
    "mf13_deactivatedBy" character varying(255),
    "mf13_deactivatedOn" timestamp without time zone,
    "mf13_reactivatedBy" character varying(255),
    "mf13_reactivatedOn" timestamp without time zone,
    "mf13_jsonField" "json",
    "mf11MCompmast_mf11_compCode" character varying(3),
    "mf02MStrmast_mf02_storeCode" character varying(4),
    "mf14MDeptmast_mf14_deptCode" character varying(3),
    "mf13_pcAbbrName" character varying(15),
    "mf13_storeType" character varying(1),
    "mf13_startDate" "date",
    "mf13_endDate" "date"
);
ALTER TABLE "MF13_M_PCMAST" OWNER TO "postgres";









--*********************
--   MF14_M_DEPTMAST
--*********************

CREATE TABLE "MF14_M_DEPTMAST" (
    "mf14_deptCode" character varying(3) NOT NULL,
    "mf14_deptName" character varying(30),
    "mf14_createdBy" character varying(255) NOT NULL,
    "mf14_createdOn" timestamp without time zone NOT NULL,
    "mf14_lastModifiedBy" character varying(255) NOT NULL,
    "mf14_lastModifiedOn" timestamp without time zone NOT NULL,
    "mf14_deactivatedBy" character varying(255),
    "mf14_deactivatedOn" timestamp without time zone,
    "mf14_reactivatedBy" character varying(255),
    "mf14_reactivatedOn" timestamp without time zone,
    "mf14_isActive" boolean NOT NULL,
    "mf14_jsonField" "json",
    "mf14_deptAbbrName" character varying(15)
);
ALTER TABLE "MF14_M_DEPTMAST" OWNER TO "postgres";









--*********************
--   MF15_M_PRMAST
--*********************

CREATE TABLE "MF15_M_PRMAST" (
    "mf15_prAbbrName" character varying(30) NOT NULL,
    "mf15_prDesc" character varying(255),
    "mf15_createdBy" character varying(255) NOT NULL,
    "mf15_createdOn" timestamp without time zone NOT NULL,
    "mf15_lastModifiedBy" character varying(255) NOT NULL,
    "mf15_lastModifiedOn" timestamp without time zone NOT NULL,
    "mf15_deactivatedBy" character varying(255),
    "mf15_deactivatedOn" timestamp without time zone,
    "mf15_reactivatedBy" character varying(255),
    "mf15_reactivatedOn" timestamp without time zone,
    "mf15_prTypeCode" character varying(10) NOT NULL,
    "mf15_isActive" boolean NOT NULL,
    "mf15_jsonField" "json"
);
ALTER TABLE "MF15_M_PRMAST" OWNER TO "postgres";










--*********************
--   MF16_M_GSTMAST
--*********************

CREATE TABLE "MF16_M_GSTMAST" (
    "mf16_taxCode" character varying(20) NOT NULL,
    "mf16_taxDesc" character varying(255) NOT NULL,
    "mf16_effectiveStartingDate" "date",
    "mf16_effectiveEndingDate" "date",
    "mf16_createdBy" character varying(255) NOT NULL,
    "mf16_createdOn" timestamp without time zone NOT NULL,
    "mf16_lastModifiedBy" character varying(255) NOT NULL,
    "mf16_lastModifiedOn" timestamp without time zone NOT NULL,
    "mf16_deactivatedBy" character varying(255),
    "mf16_deactivatedOn" timestamp without time zone,
    "mf16_reactivatedBy" character varying(255),
    "mf16_reactivatedOn" timestamp without time zone,
    "mf16_taxAbbrDesc" character varying(30),
    "mf16_isActive" boolean NOT NULL,
    "mf16_jsonField" "json",
    "mf16_taxRate" numeric(10,4),
    "mf16_allowedVariance" numeric(10,4)
);
ALTER TABLE "MF16_M_GSTMAST" OWNER TO "postgres";









--*********************
--   MF17_M_ORDCATMAST
--*********************

CREATE TABLE "MF17_M_ORDCATMAST" (
    "mf17_ordCatCode" character varying(70) NOT NULL,
    "mf17_ordCatDesc" character varying(255),
    "mf17_ordCatAuthorizationGrid" character varying(255) NOT NULL,
    "mf17_createdBy" character varying(255) NOT NULL,
    "mf17_createdOn" timestamp without time zone NOT NULL,
    "mf17_lastModifiedBy" character varying(255) NOT NULL,
    "mf17_lastModifiedOn" timestamp without time zone NOT NULL,
    "mf17_deactivatedBy" character varying(255),
    "mf17_deactivatedOn" timestamp without time zone,
    "mf17_reactivatedBy" character varying(255),
    "mf17_reactivatedOn" timestamp without time zone,
    "mf17_jsonField" "json",
    "mf17_ordCatAbbrDesc" character varying(30),
    "mf15MPrmast_mf15_prTypeCode" character varying(10),
    "mf17_isActive" boolean NOT NULL
);
ALTER TABLE "MF17_M_ORDCATMAST" OWNER TO "postgres";










--*********************
--   MF18_M_ORDSUBCMAST
--*********************

CREATE TABLE "MF18_M_ORDSUBCMAST" (
    "mf18_ordSubCCode" character varying(15) NOT NULL,
    "mf18_ordSubCDesc" character varying(255),
    "mf17MOrdcatmast_mf17_ordCatCode" character varying(70) NOT NULL,
    "mf18_createdBy" character varying(255) NOT NULL,
    "mf18_createdOn" timestamp without time zone NOT NULL,
    "mf18_lastModifiedOn" timestamp without time zone NOT NULL,
    "mf18_deactivatedBy" character varying(255),
    "mf18_deactivatedOn" timestamp without time zone,
    "mf18_reactivatedBy" character varying(255),
    "mf18_reactivatedOn" timestamp without time zone,
    "mf18_jsonField" "json",
    "mf18_lastModifiedBy" character varying(255) NOT NULL,
    "mf18_ordSubCAbbrDesc" character varying(30),
    "mf18_isActive" boolean NOT NULL
);
ALTER TABLE "MF18_M_ORDSUBCMAST" OWNER TO "postgres";










--*********************
--   MF19_M_JOBMAST
--*********************

CREATE TABLE "MF19_M_JOBMAST" (
    "mf19_jobCode" character varying(6) NOT NULL,
    "mf19_jobName" character varying(30),
    "mf19_jobAbbrName" character varying(15),
    "mf19_createdBy" character varying(255) NOT NULL,
    "mf19_createdOn" timestamp without time zone NOT NULL,
    "mf19_lastModifiedBy" character varying(255) NOT NULL,
    "mf19_lastModifiedOn" timestamp without time zone NOT NULL,
    "mf19_deactivatedBy" character varying(70),
    "mf19_deactivatedOn" timestamp without time zone,
    "mf19_reactivatedBy" character varying(70),
    "mf19_reactivatedOn" timestamp without time zone,
    "mf19_jsonField" "json",
    "mf19_isActive" boolean
);
ALTER TABLE "MF19_M_JOBMAST" OWNER TO "postgres";









--*********************
--   MF20_M_ADMINMAST
--*********************

CREATE TABLE "MF20_M_ADMINMAST" (
    "mf20_username" character varying(10) NOT NULL,
    "mf20_password" character varying(255),
    "mf20_startDate" "date",
    "mf20_endDate" "date",
    "mf20_isActive" boolean,
    "mf20_createdBy" character varying(255),
    "mf20_lastModifiedBy" character varying(255),
    "mf20_deactivatedBy" character varying(255),
  	"mf20_reactivatedBy" character varying(255),
    "mf20_createdOn" timestamp without time zone,
    "mf20_lastModifiedOn" timestamp without time zone,
    "mf20_deactivatedOn" timestamp without time zone,
    "mf20_reactivatedOn" timestamp without time zone,
    "mf20_blocked" smallint,
    "mf20_jsonField" "json",
    "mf20_recentPassword" character varying(255),
    "mf20_isLocked" boolean,
    "password_reset_token" character varying(100),
  	"mf20_email" character varying(30)
);
ALTER TABLE "MF20_M_ADMINMAST" OWNER TO "postgres";











--*********************
--   Mf22_M_SCHEDULER
--*********************

CREATE SEQUENCE "mf22_m_scheduler_mf22_schedulerid_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE "mf22_m_scheduler_mf22_schedulerid_seq" OWNER TO "postgres";

CREATE TABLE "Mf22_M_SCHEDULER" (
    "mf22_schedulerid" integer DEFAULT "nextval"('"mf22_m_scheduler_mf22_schedulerid_seq"'::"regclass") NOT NULL,
    "mf22_task" character varying(255),
    "mf22_cronfrequency_occurrence" character varying(50),
    "mf22_startdate" timestamp(6) with time zone,
    "mf22_noenddate" boolean,
    "mf22_cronfrequency_amount" character varying(50),
    "mf22_status" character varying(20),
    "mf22_database" character varying(30),
    "mf22_table" character varying(30),
    "mf22_scheduletype" character varying(30),
    "mf22_enddate" "date",
    "mf22_upload" character varying(255),
    "mf22_path" character varying(50),
    "mf22_createdon" time without time zone,
    "mf22_createdby" character varying,
    "mf22_deactivatedby" character varying,
    "mf22_deactivatedon" timestamp without time zone,
    "mf22_jobname" character varying(50)
);
ALTER TABLE "Mf22_M_SCHEDULER" OWNER TO "postgres";









--*********************
--   PO06_T_USER
--*********************

CREATE TABLE "PO06_T_USER" (
    "po06_psid" character varying(11) NOT NULL,
    "po06_empname" character varying(50),
    "po06_firsttimelogin" timestamp without time zone,
    "po06_lastlogin" timestamp without time zone,
    "po06_numberoflogin" integer,
    "po06_isactive" boolean,
    "po06_createdby" character varying(50),
    "po06_createdon" timestamp without time zone,
    "po06_lastmodifiedby" character varying(50),
    "po06_lastmodifiedon" timestamp without time zone,
    "po06_deactivatedby" character varying(50),
    "po06_deactivatedon" timestamp without time zone,
    "po06_reactivatedby" character varying(50),
    "po06_reactivatedon" timestamp without time zone,
    "po06_json" "json",
    "po06_created_by" character varying(255),
    "po06_created_on" timestamp without time zone,
    "po06_deactivated_by" character varying(255),
    "po06_deactivated_on" timestamp without time zone,
    "po06_emp_name" character varying(255),
    "po06_first_time_login" timestamp without time zone,
    "po06_is_active" boolean,
    "po06_last_login" timestamp without time zone,
    "po06_last_modified_by" character varying(255),
    "po06_last_modified_on" timestamp without time zone,
    "po06_number_of_login" integer,
    "po06_reactivated_by" character varying(255),
    "po06_reactivated_on" timestamp without time zone
);
ALTER TABLE "PO06_T_USER" OWNER TO "postgres";










--*********************
--   PO01_LOGINLIST
--*********************

CREATE SEQUENCE pologinlist__seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE pologinlist__seq OWNER TO postgres;

CREATE TABLE "PO01_LOGINLIST" (
    po01_loginlist_seq integer DEFAULT nextval('pologinlist__seq'::regclass) NOT NULL,
    po01_year character varying(4),
    po01_companycode character varying(60),
    po01_companyname character varying(20)
);
ALTER TABLE "PO01_LOGINLIST" OWNER TO postgres;









--*********************
--   PO01_T_MAIN
--*********************

CREATE TABLE "PO01_T_MAIN" (
    "po01_poNo" character varying(255) NOT NULL,
    "po01_addressLine1" character varying(255),
    "po01_addressLine2" character varying(255),
    "po01_addressLine3" character varying(255),
    "po01_authorizeRemark" character varying(255),
    "po01_authorizedOn" timestamp without time zone,
    "mf12MEmpMast_po01_authorizer" character varying(255),
    "po01_cancelRemark" character varying(255),
    "po01_cancelledBy" character varying(255),
    "po01_cancelledOn" timestamp without time zone,
    "po01_closeException" character varying(255),
    "po01_closedBy" character varying(255),
    "po01_closedOn" timestamp without time zone,
    "mf11MCompMast_po01_compCode" character varying(255),
    "po01_contactPerson" character varying(255),
    "po01_contactPersonEmail" character varying(255),
    "po01_contactPersonName" character varying(255),
    "po01_contactPersonPhone" character varying(255),
    "mf02MCreditTerms_po01_creditTermsCode" character varying(255),
    po01_currency character varying(255),
    "po01_deleteRemark" character varying(255),
    "po01_deletedBy" character varying(255),
    "po01_deletedOn" timestamp without time zone,
    "po01_deliveryInstruction" character varying(255),
    "po01_departmentName" character varying(255),
    "po01_expireDate" timestamp without time zone,
    "po01_expiryDays" integer,
    "po01_expiryStatus" boolean,
    "po01_firstApprover" character varying(255),
    "po01_firstApproverDesignation" character varying(255),
	po01_forwardingstatus character varying(255),
    "po01_issuedOn" timestamp without time zone,
    "mf12MEmpMast_po01_issuer" character varying(255),
    "po01_issuerRemark" character varying(255),
    "po01_lastPrintedBy" character varying(255),
    "po01_lastPrintedOn" timestamp without time zone,
    "mf18MOrdSubCMast_po01_ordSubCCode" character varying(255),
    "mf17MOrdCatMast_po01_ordCatCode" character varying(255),
    "po01_orderCategoryAuthorizationGrid" character varying(255),
    "po01_overAllDiscount" double precision,
    "po01_paymentTerms" character varying(255),
    "mf13MPCMast_po01_pcCode" character varying(255),
    "po01_postArea" character varying(255),
    "po01_postCode" character varying(255),
    "mf15MPRMast_po01_prTypeCode" character varying(255),
    "po03TProject_po01_projectCode" character varying(255),
    "po01_quoteDate" timestamp without time zone,
    "po01_quoteNo" character varying(255),
    "po01_redraftRemark" character varying(255),
    "po01_rejectRemark" character varying(255),
    "po01_rejectedBy" character varying(255),
    "po01_rejectedOn" timestamp without time zone,
    "po01_secondApprover" character varying(255),
    "po01_secondApproverDesignation" character varying(255),
    po01_status character varying(255),
    "po01_storeName" character varying(255),
    "po01_termsAndConditionsLandscape" character varying(255),
    "po01_termsAndConditionsPortrait" character varying(255),
    "po01_thirdApprover" character varying(255),
    "po01_thirdApproverDesignation" character varying(255),
    "po01_totExclGST" double precision,
    "po01_totGST" double precision,
    "po01_totInclGST" double precision,
    "po01_vendorAddressLine1" character varying(255),
    "po01_vendorAddressLine2" character varying(255),
    "po01_vendorAddressLine3" character varying(255),
    "po01_vendorName" character varying(255),
    "po01_vendorPersonInCharge" character varying(255),
    "po01_vendorPersonInChargeEmail" character varying(255),
    "po01_vendorPersonInChargePhone" character varying(255),
    "po01_vendorPostArea" character varying(255),
    "po01_vendorPostCode" character varying(255),
    mf01mvendorbranch_po01_vendorbranchcode character varying(255),
    "mf01MVendorBranch_po01_vendorCode" character varying(255),
    "po01_verifiedOn" timestamp without time zone,
    "mf12MEmpMast_po01_verifier" character varying(255),
    "po01_verifyRemark" character varying(255)
);
ALTER TABLE "PO01_T_MAIN" OWNER TO postgres;









--*********************
--   menu
--*********************

CREATE SEQUENCE "menu_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999999999999999
    CACHE 1;
ALTER TABLE "menu_seq" OWNER TO "postgres";

CREATE TABLE "menu" (
    "menu_id" bigint DEFAULT "nextval"('"menu_seq"'::"regclass") NOT NULL,
    "menu_code" character varying(100),
    "menu_desc" character varying(200),
    "menu_url" character varying(100),
    "menu_reference" character varying(100)
);
ALTER TABLE "menu" OWNER TO "postgres";










--*********************
--   menu_grp
--*********************

CREATE SEQUENCE "menu_grp_seq"
    START WITH 107
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;
ALTER TABLE "menu_grp_seq" OWNER TO "postgres";

CREATE TABLE "menu_grp" (
    "menu_grp_id" bigint DEFAULT "nextval"('"menu_grp_seq"'::"regclass") NOT NULL,
    "menu_grp_parent" bigint NOT NULL,
    "menu_code" character varying(100),
    "menu_desc" character varying(200),
    "menu_url" character varying(100),
    "menu_reference" character varying(100),
    "menu_grp_root" character varying(100),
    "menu_id" bigint,
    "menu_grp_enable" boolean
);
ALTER TABLE "menu_grp" OWNER TO "postgres";










--*********************
--   menu_grp_role
--*********************

CREATE SEQUENCE "menu_grp_role_id_seq"
    START WITH 11
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE "menu_grp_role_id_seq" OWNER TO "postgres";

CREATE TABLE "menu_grp_role" (
    "id" bigint DEFAULT "nextval"('"menu_grp_role_id_seq"'::"regclass") NOT NULL,
    "menu_grp_id" bigint,
    "role_id" bigint
);
ALTER TABLE "menu_grp_role" OWNER TO "postgres";









--*********************
--   permissions
--*********************

CREATE TABLE "permissions" (
    "id" integer NOT NULL,
    "description" character varying(1024),
    "name" character varying(255) NOT NULL
);
ALTER TABLE "permissions" OWNER TO "postgres";










--*********************
--   role_permission
--*********************

CREATE TABLE "role_permission" (
    "role_id" integer NOT NULL,
    "perm_id" integer NOT NULL
);
ALTER TABLE "role_permission" OWNER TO "postgres";










--*********************
--   roles
--*********************

CREATE TABLE "roles" (
    "role_id" integer NOT NULL,
    "role_description" character varying(1024),
    "role_name" character varying(255) NOT NULL
);
ALTER TABLE "roles" OWNER TO "postgres";









--*********************
--   user_role
--*********************

CREATE TABLE "user_role" (
    "user_id" character varying(100) NOT NULL,
    "role_id" integer NOT NULL
);
ALTER TABLE "user_role" OWNER TO "postgres";









--*********************
--   users
--*********************

CREATE TABLE "users" (
    "id" integer NOT NULL,
    "email" character varying(255) NOT NULL,
    "name" character varying(255) NOT NULL,
    "password" character varying(255) NOT NULL,
    "password_reset_token" character varying(255)
);
ALTER TABLE "users" OWNER TO "postgres";









--*********************
--   hibernate_sequence
--*********************

CREATE SEQUENCE "hibernate_sequence"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE "hibernate_sequence" OWNER TO "postgres";










--*********************
--   vw01_loginview
--*********************

CREATE VIEW vw01_loginview AS
 SELECT c."mf11_compCode",
    l.po01_year,
    c."mf11_compAbbrName",
    c."mf11_compName",
    c."mf11_isActive"
   FROM ("PO01_LOGINLIST" l
     JOIN "MF11_M_COMPMAST" c ON (((l.po01_companycode)::text = (c."mf11_compCode")::text)));


ALTER TABLE vw01_loginview OWNER TO postgres;