//https://getbootstrap.com/docs/4.0/getting-started/browsers-devices/#validators
//https://careydevelopment.us/2017/05/24/implement-form-field-validation-spring-boot-thymeleaf/
//http://bootstrapvalidator.votintsev.ru/getting-started/
$(document)
		.ready(
				function() {
				 
					
					
					$('.adminuserform')
							.bootstrapValidator(
									{
										message : 'This value is not valid',
										feedbackIcons : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											username : {
												message : 'The username is not valid',
												validators : {
													notEmpty : {
														message : 'The username is required and cannot be empty'
													},
													stringCase: {
								                        message: 'The Username should be in uppercase',
								                        'case': 'upper'
								                    },
													stringLength : {
														
														max : 7,
														message : 'The username must be 7 characters long'
													},
													/*regexp : {
														regexp : /^(A_|a_)[A-Z0-9]{5}/, 
														message : 'Please start with \"A_\" followed by 5 alphanumerics. Example : A_BC123',
									
													},*/
													callback: {
														message: 'The username is not valid',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															if (value === 'superadmin'){
								                                return {
								                                    valid: false,
								                                    message: 'Superadmin already exists'
								                                }
								                            }
															
															  // The password doesn't contain any uppercase character
								                           
														//If found return true the above error message is displayed
														 if (value.search(/^(A_)[A-Z0-9]{5}/) < 0) {
															 
								                                return {
								                                    valid: false,
								                                    message: 'The Username should be 7 characters long, Please start with \"A_\" followed by 5 alphanumerics. Example : A_BC123'
								                                };
								                            } else {
								                            	return true;
								                            }
														 
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: 'The username should not start or end with empty spaces.'
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
													
							
												}

											},
											password : {
												message : 'The password is not valid',
												validators : {
													notEmpty : {
														message : 'The password is required and cannot be empty'
													},
													regexp : {
														regexp : /^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!_%*#?&])[A-Za-z\d$@$!%*#_?&]{8,}$/,
														message : 'Invalid format! Please enter at least 8 alphanumerics with at least 1 special characters, 1 alphabet and 1 number.'
													}
												}
											},
											recentPassword : {
												message : 'The Confirm password is not valid',
												validators : {
													notEmpty : {
														message : 'The Confirm password is required and cannot be empty'
													},
													identical: {
								                        field: 'password',
								                        message: 'The password and its confirm are not the same'
								                    }
												}
											}
										}
									});
					
					
					$('.resetpwdform')
					.bootstrapValidator(
							{
								message : 'This value is not valid',
								feedbackIcons : {
									valid : 'glyphicon glyphicon-ok',
									invalid : 'glyphicon glyphicon-remove',
									validating : 'glyphicon glyphicon-refresh'
								},
								fields : {

									newpassword : {
										message : 'The new password is not valid',
										validators : {
											notEmpty : {
												message : 'The new password is required and cannot be empty'
											},
											regexp : {
												regexp : /^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!_%*#?&])[A-Za-z\d$@$!%*#_?&]{8,}$/,
												message : 'Invalid format! Please enter at least 8 alphanumerics with at least 1 special characters, 1 alphabet and 1 number.'
											}
										}
									},
									confirmpassword : {
										message : 'The confirm password is not valid',
										validators : {
											notEmpty : {
												message : 'The confirm password is required and cannot be empty'
											},
											regexp : {
												regexp : /^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!_%*#?&])[A-Za-z\d$@$!%*#_?&]{8,}$/,
												message : 'Invalid format! Please enter at least 8 alphanumerics with at least 1 special characters, 1 alphabet and 1 number.'
											}
										}
									}
								}
							});

					$('.jobmasterform')
							.bootstrapValidator(
									{
										message : 'This value required',
										feedbackIcons : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											code : {
												message : 'The Job Code is required',
												validators : {
													notEmpty : {
														message : 'The Job Code is required and cannot be empty'
													},
													stringLength : {
														max : 6,
														message : 'The Job Code should be maximum 6 characters long'
													},
													callback: {
														message: 'The Job Code should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											jobname : {
												message : 'The jobname is not valid',
												validators : {
													stringLength : {
														max : 30,
														message : 'The Job Name should be maximum 30 characters long'
													},
													callback: {
														message: 'The Job Name should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											abbreviation : {
												message : 'The Abbreviation is not valid',
												validators : {
													stringLength : {
														max : 15,
														message : 'The Job Abbreviation should be maximum 15 characters long'
													},
													callback: {
														message: 'The Job Abbreviation should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											}
										}
									});
					// master data form

					$('.masterdataform')
							.bootstrapValidator(
									{
										message : 'This value required',
										feedbackIcons : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											codetype : {
												message : 'The Code Type is required',
												validators : {
													notEmpty : {
														message : 'The Code Type cannot be empty'
													},
													stringLength : {
														min :1,
														max : 45,
														message : 'The Code Type should be maximum 45 characters long'
													},
													callback: {
														message: 'The Code Type should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											codeValue : {
												message : 'The Code Value is required',
												validators : {
													stringLength : {
														max : 255,
														message : 'The Code Value should be maximum 255  characters long'
													},
													callback: {
														message: 'The Code Value should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											codeDesc : {
												message : 'The Code Desc is required',
												validators : {
													stringLength : {
														max : 255,
														message : 'The Code Desc should be maximum 255  characters long'
													},
													callback: {
														message: 'The Code Desc should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											}
										}
									});

					// parameter

					$('.systemparamform')
					.bootstrapValidator(
							{
								message : 'This value required',
								feedbackIcons : {
									valid : 'glyphicon glyphicon-ok',
									invalid : 'glyphicon glyphicon-remove',
									validating : 'glyphicon glyphicon-refresh'
								},
								fields : {
									propName : {
										message : 'The Property Name is required',
										validators : {
											notEmpty : {
												message : 'The Property Name cannot be empty'
											},
											stringLength : {
												min :1,
												max : 30,
												message : 'The Property Name should be maximum 30 characters long'
											},
											
											callback: {
												message: 'The Property Name should not start or end with empty spaces',
												callback: function(value, validator, $field){
						                         
													if (value === '') {
						                                return true;
						                            }
													
													
												//If found return true the above error message is displayed
												 if (value.search(/^\s|\s$/) < 0) {
													 
						                                return {
						                                    valid: true,
						                                    message: ''
						                                };
						                            } else {
						                            	return false;
						                            }
												 
											
											
												return true;
											}
										}
										}
									},
									propValue : {
										message : 'The Property Value is required',
										validators : {
											notEmpty : {
												message : 'The Property Value is required and cannot be empty'
											},
											stringLength : {
												min :1,
												max : 255,
												message : 'The Property Value should be maximum 255 characters long'
											},
											
											callback: {
												message: 'The Property Value should not start or end with empty spaces',
												callback: function(value, validator, $field){
						                         
													if (value === '') {
						                                return true;
						                            }
													
													
												//If found return true the above error message is displayed
												 if (value.search(/^\s|\s$/) < 0) {
													 
						                                return {
						                                    valid: true,
						                                    message: ''
						                                };
						                            } else {
						                            	return false;
						                            }
												 
											
											
												return true;
											}
										}
										}
									},
									sysName:{
										message : 'The Property Value is not Valid',
										validators : {
											stringLength : {
												max : 10,
												message : 'The System Name should be maximum 10 characters long'
											},
											
											callback: {
												message: 'The System Name should not start or end with empty spaces',
												callback: function(value, validator, $field){
						                         
													if (value === '') {
						                                return true;
						                            }
												
												
												 if (value.search(/^\s|\s$/) < 0) {
													 
						                                return {
						                                    valid: true,
						                                    message: ''
						                                };
						                            } else {
						                            	return false;
						                            }
												 
											
											
												return true;
											}
										}
											
										}
										
									},
									desc:{
										message : 'The Description is not Valid',
										validators : {
											stringLength : {
												max : 255,
												message : 'The Description should be maximum 255 characters long'
											},
											
											callback: {
												message: 'The Description should not start or end with empty spaces',
												callback: function(value, validator, $field){
						                         
													if (value === '') {
						                                return true;
						                            }
												
												
												 if (value.search(/^\s|\s$/) < 0) {
													 
						                                return {
						                                    valid: true,
						                                    message: ''
						                                };
						                            } else {
						                            	return false;
						                            }
												 
											
											
												return true;
											}
										}
											
										}
										
									}
								}
							});

					$('.storeform')
							.bootstrapValidator(
									{
										message : 'This value is not valid',
										feedbackIcons : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											storeAbbrName : {
												message : 'The Store Abbreviation Name is not valid',
												validators : {
													stringLength : {
														min :1,
														max : 15,
														message : 'The Store Abbreviation Name should be less than 15 characters long'
													},
													callback: {
														message: 'The Abbr Name should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											storeName : {
												message : 'The Store Name is not valid',
												validators : {
													/*notEmpty : {
														message : 'The Store Name is required and cannot be empty'
													},*/
													stringLength : {
														min :1,
														max : 30,
														message : 'The Store Name should be more than 1 and less than 30 characters long'
													},
													callback: {
														message: 'The Name should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											storeCode : {
												message : 'The Code is not valid',
												validators : {
													notEmpty : {
														message : 'The Code is required and cannot be empty'
													},
													stringLength : {
														
														max : 4,
														message : 'The Store Code should be 4 characters long'
													},
													stringCase: {
								                        message: 'The Code should be in uppercase',
								                        'case': 'upper'
								                    },
													
													regexp : {
														regexp : /[A-Z0-9]{4}/, 
														message : 'Please enter 4 alphanumerics without special character. Example : 012A',
									
													},
													callback: {
														message: 'The code should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											}
										}
									});
					
					

					$('.purchaseform')
							.bootstrapValidator(
									{
										message : 'This value is not valid',
										feedbackIcons : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											prAbbrName : {
												message : 'The Abbreviation is required and cannot be empty',
												validators : {
													stringLength : {
														min :1,
														max : 30,
														message : 'Please enter up to 30 alphanumerics.'
													},
													callback: {
														message: 'The Code Abbr should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											prDesc : {
												message : 'The Desc is is required and cannot be empty',
												validators : {
													stringLength : {
														min :1,
														max : 255,
														message : 'Please enter up to 255 alphanumerics.'
													},
													callback: {
														message: 'The Code Desc should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											prTypeCode : {
												message : 'The Code is not valid',
												validators : {
													notEmpty : {
														message : 'The Code is required and cannot be empty'
													},
													stringLength : {
														min :1,
														max : 10,
														message : 'Please enter up to 10 alphanumerics. Example : A43287DD@J'
													},
													stringCase: {
								                        message: 'The Code should be in uppercase',
								                        'case': 'upper'
								                    },
													
													callback: {
														message: 'The Code should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											}
										}
									});

					$('.profitcenterform')
							.bootstrapValidator(
									{
										message : 'This value is not valid',
										feedbackIcons : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											pcName : {
												message : 'The Name is required and cannot be empty',
												validators : {
													notEmpty : {
														message : 'The Name is required and cannot be empty'
													},
		
													stringLength : {
														min :1,
														max : 30,
														message : 'The Profit Center Name should be more than 1 and less than 30 characters long'
													},
													callback: {
														message: 'The Profit Center Name should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											pcAbbrName : {
			
												validators : {
													
													stringLength : {
														min :1,
														max : 15,
														message : 'The Profit Center Abbr Name should be more than 1 and less than 15 characters long'
													},
													callback: {
														message: 'The Profit Center Abbr Name should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											
											addressLine1 :{
												validators : {
													stringLength : {
														min :1,
														max : 70,
														message : 'The Profit Center Address should be more than 1 and less than 70 characters long'
													},
													callback: {
														message: 'The Profit Center Address should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											
											addressLine2 :{
												validators : {
													stringLength : {
														min :1,
														max : 70,
														message : 'The Profit Center Address should be more than 1 and less than 70 characters long'
													},
													callback: {
														message: 'The Profit Center Address should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											
											addressLine3 :{
												validators : {
													stringLength : {
														min :1,
														max : 70,
														message : 'The Profit Center Address should be more than 1 and less than 70 characters long'
													},
													callback: {
														message: 'The Profit Center Address should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											
											postArea :{
												validators : {
													stringLength : {
														min :1,
														max : 70,
														message : 'The Profit Center Post Area should be more than 1 and less than 70 characters long'
													},
													callback: {
														message: 'The Profit Center Post Area should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											
											postCode :{
												validators : {
												/*	stringLength : {
														min :1,
														max : 10,
														message : ''
													},*/
													regexp : {
														regexp : /^[a-zA-Z0-9]{1,10}$/, 
														message : 'Please enter up to 10 alphanumerics without special character. Example : 57000A'
									
													},
													callback: {
														message: 'The Profit Center Post Code should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											
											
											company : {
												message : 'Select Company',
												validators : {
													notEmpty : {
														message : 'Select Company'
													}
												}
											},
											store : {
												message : 'Select Store',
												validators : {
													notEmpty : {
														message : 'Select Store'
													}
												}
											},
											deartment : {
												message : 'Select Department',
												validators : {
													notEmpty : {
														message : 'Select Department'
													}
												}
											}
										}
									});

					$('.pouserform')
							.bootstrapValidator(
									{
										message : 'This value is not valid',
										feedbackIcons : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											empName : {
												message : 'The empname is not valid',
												validators : {
													notEmpty : {
														message : 'The empname is required and cannot be empty'
													}
												}
											},
											name : {
												message : 'Select PSID',
												validators : {
													notEmpty : {
														message : 'Select PSID'
													}
												}
											}
										}
									});

					$('.ordercategoryform')
							.bootstrapValidator(
									{
										message : 'This value is not valid',
										feedbackIcons : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											ordCatCode : {
												message : 'The Code is not valid',
												validators : {
													notEmpty : {
														message : 'The Code is required and cannot be empty'
													},
													stringLength : {
														max : 70,
														message : 'The code should be less than 70 characters long'
													},
													callback: {
														message: 'The Code should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											ordCatDesc : {
								
												validators : {
													
													stringLength : {
														max : 255,
														message : 'The Desc should be less than 255 characters long'
													},
													callback: {
														message: 'The Desc should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											ordCatAbbrDesc : {
												
												validators : {
													stringLength : {
														max : 30,
														message : 'The Abbr Desc should be less than 30 characters long'
													},
													callback: {
														message: 'The Abbr Desc should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											authgrid : {
												message : 'The Order Category Authorization Grid is not valid',
												validators : {
													/*notEmpty : {
														message : 'Please Select Order Category Authorization Grid File.'
													},*/
													 file: {
									                        extension: 'jpeg,png,jpg,pdf',
									                        type: 'image/jpeg,image/png,image/jpg,application/pdf',
									                        maxSize: 2048 * 1024,
									                        message: 'Order Category Authorization Grid should only be in JPEG,PNG,JPG or PDF Format'
									                    }
												}
											},
											
									authgrid_hidden : {
												message : 'The Order Category Authorization Grid is not valid',
												validators : {
													notEmpty : {
														message : 'Please Select Order Category Authorization Grid File again to keep the data safe.'
													},
													 file: {
									                        extension: 'jpeg,png,jpg,pdf',
									                        type: 'image/jpeg,image/png,image/jpg,application/pdf',
									                        maxSize: 2048 * 1024,
									                        message: 'Order Category Authorization Grid should only be in JPEG,PNG,JPG or PDF Format'
									                    }
												}
											},
											
											prtypecode : {
												 
												validators : {
													notEmpty : {
														message : 'Please Select Purchase Requisition Type Code.'
													}
													
												}
											}
										}
									});

					$('.ordersubcategoryform')
							.bootstrapValidator(
									{
										message : 'This value is not valid',
										feedbackIcons : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											ordSubCCode : {
												message : 'The Code is not valid',
												validators : {
													notEmpty : {
														message : 'The Code is required and cannot be empty'
													},
													stringLength : {
														max : 15,
														message : 'The code should be less than 15 characters long'
													},
													callback: {
														message: 'The Code should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											ordcat : {
												message : 'Select Order Category',
												validators : {
													notEmpty : {
														message : 'Select Order Category'
													}
												}
											},
											ordSubCDesc : {
												message : 'The Desc is not valid',
												validators : {
													
													stringLength : {
														max : 255,
														message : 'The Desc should be less than 255 characters long'
													},
													callback: {
														message: 'The Desc should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											ordSubCAbbrDesc : {
												message : 'The Abbr Desc is not valid',
												validators : {
													
													stringLength : {
														max : 30,
														message : 'The Abbr Desc should be less than 30 characters long'
													},
													callback: {
														message: 'The Abbr Desc should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											}
										}
									});

					$('.companyform')
							.bootstrapValidator(
									{
										message : 'This value is not valid',
										feedbackIcons : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											compCode : {
												message : 'The Company Code is not valid',
												validators : {
													notEmpty : {
														message : 'The Company Code is required and cannot be empty'
													},
													stringLength : {
														min : 3,
														max : 3,
														message : 'The Company Code should be 3 characters long'
													},
													regexp : {
														regexp : /[0-9]{3}/, 
														message : 'Please enter numerics without special character. Example : 012',
									
													},
													callback: {
														message: 'The Code should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											compAbbrName : {
												message : 'The Company Abbreviation Name is required',
												validators : {
													notEmpty : {
														message : 'The Company Abbreviation Name is required and cannot be empty'
													},
													stringLength : {
														min : 1,
														max : 15,
														message : 'The Company Abbreviation Name must be more than 1 and less than 15 characters long'
													},
				
													callback: {
														message: 'The Company Abbreviation Name should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											compName : {
												message : 'The Company Name is required',
												validators : {
													notEmpty : {
														message : 'The company name cannot be empty'
													},
													stringLength : {
														min : 1,
														max : 70,
														message : 'The Company Name must be more than 1 and less than 70 characters long'
													},
				
													callback: {
														message: 'The Company Name should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											compCodeHRIS : {
												message : 'The Company Code HRIS is required',
												validators : {
													notEmpty : {
														message : 'The Company Code HRIS is required and cannot be empty'
													},
													stringCase: {
								                        message: 'The Company Code HRIS should be in uppercase',
								                        'case': 'upper'
								                    },
													regexp : {
														regexp : /[A-Z0-9]{3}/, 
														message : 'The Company Code HRIS can only consist of alphabetical and numbers. Example : PC1',
									
													},
													callback: {
														message: 'The Company Code HRIS should not have empty spaces at start/between/end.',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											
											compRegNo : {
												/*message : 'The Company Reg No is required',*/
												validators : {
													/*notEmpty : {
														message : 'The Company Reg No is required and cannot be empty'
													},*/
													stringLength : {
														min : 1,
														max : 20,
														message : 'The Company Reg No must be less than 20 characters long'
													},
													callback: {
														message: 'The Company Reg No should not start or end with empty spaces.',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											compLogofile:{
											    validators: {
											   
								                    file: {
								                        extension: 'jpeg,png,jpg',
								                        type: 'image/jpeg,image/png,image/jpg',
								                        maxSize: 2048 * 1024,
								                        message: 'Company Logo should only be in JPEG,PNG or JPG Format'
								                    }
								                }
												
											},
											compLetterheadfile:{
											    validators: {
								                    file: {
								                        extension: 'jpeg,png,jpg,pdf',
								                        type: 'image/jpeg,image/png,image/jpg,application/pdf',
								                        maxSize: 2048 * 1024,
								                        message: 'Company Letter Heading should only be in JPEG,PNG,JPG or PDF Format'
								                    }
								                }
												
											},
											
											termsLandscapefile:{
											    validators: {
								                    file: {
								                        extension: 'pdf',
								                        type: 'application/pdf',
								              
								                        message: 'Terms and Conditions Landscape should only be in PDF Format'
								                    }
								                }
												
											},
											
											termsPortraitfile:{
											    validators: {
								                    file: {
								                        extension: 'pdf',
								                        type: 'application/pdf',
								              
								                        message: 'Terms and Conditions Portrait should only be in PDF Format'
								                    }
								                }
												
											}
										}
									});

					$('.depatform')
							.bootstrapValidator(
									{
										message : 'This value is not valid',
										feedbackIcons : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											deptAbbrName : {
												message : 'The Department ID is not valid',
												validators : {
													notEmpty : {
														message : 'The Department ID is required and cannot be empty'
													},
													stringLength : {
														min : 2,
														max : 2,
														message : 'The Department ID must be 2 characters long'
													},
													callback: {
														message: 'The Department ID should not start or end with empty spaces.',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											deptName : {
												message : 'The Name is required',
												validators : {
													notEmpty : {
														message : 'The Department Name is required and cannot be empty'
													},
													stringLength : {
														min : 1,
														max : 30,
														message : 'The Department Name should be less than 30 characters long'
													},
													callback: {
														message: 'The Department Name should not start or end with empty spaces.',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											deptCode : {
												message : 'The Department Code is not valid',
												validators : {
													notEmpty : {
														message : 'The Department Code is required and cannot be empty'
													},
													regexp : {
														regexp : /[A-Za-z0-9]{3}/, 
														message : 'The Department Code can only consist of alphabetical and numbers without special character. Example : 014',
									
													},
													callback: {
														message: 'The Department Code should not start or end with empty spaces.',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											}
										}
									});

					$('.gstform')
							.bootstrapValidator(
									{
										message : 'This value is not valid',
										feedbackIcons : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											taxRate : {
												message : 'The Tax Rate should be number',
												validators : {
													notEmpty : {
														message : 'The Tax Rate is required and cannot be empty'
													},
													/*between: {
									                    min: 0,
									                    max: 100,
									                    message: 'The Tax Rate must be between 0 and 100'
									                },*/
									                regexp : {
														regexp : /^\d{1,2}(\.\d{1,2})?$/, 
														message : 'The Allowed Variance can only between 0 to 100 with only two desimal place. Example : 99.22',
									
													}
												}
											},
											allowedVariance : {
												message : 'The Allowed Variance is not valid',
												validators : {
													notEmpty : {
														message : 'The Allowed Variance is required and cannot be empty'
													},
													
									                regexp : {
														regexp : /^\d{1,2}(\.\d{1,2})?$/, 
														message : 'The Allowed Variance can only between 0 to 100 with only two desimal place. Example : 99.22',
									
													}
									                
												}
											},
											taxAbbrDesc : {
												message : 'The Tax Abbreviation Desc is not valid',
												validators : {
													stringLength : {
														min : 1,
														max : 30,
														message : 'The  Tax Abbreviation Desc must be less than 30 characters long'
													},
													callback: {
														message: 'The Tax Abbreviation Desc should not start or end with empty spaces.',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											taxDesc : {
												message : 'The tax Desc is not valid',
												validators : {
													stringLength : {
														min : 1,
														max : 255,
														message : 'The  Tax Desc must be less than 255 characters long'
													},
													callback: {
														message: 'The Tax Desc should not start or end with empty spaces.',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											
											taxCode : {
												message : 'The The taxCode is not valid',
												validators : {
													notEmpty : {
														message : 'The Tax Code is required and cannot be empty'
													},
													stringCase: {
								                        message: 'The Tax Code should be in uppercase',
								                        'case': 'upper'
								                    },
													stringLength : {
														min : 1,
														max : 20,
														message : 'The Tax Code should be between 1 and 20 characters long'
													},
													regexp : {
														regexp : /(^[\w\-]+$){1,20}/, 
														message : 'Please enter up to 20 alphanumerics, Only \"-\" Allowed as special character. Example : GST0003321',
									
													},
													callback: {
														message: 'The Tax Code should not start or end with empty spaces.',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
														
												
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
													
							
												}

											}
										
										}
									});
					
					$('.CreditTermsForm')
					.bootstrapValidator(
							{
								message : 'This value is not valid',
								feedbackIcons : {
									valid : 'glyphicon glyphicon-ok',
									invalid : 'glyphicon glyphicon-remove',
									validating : 'glyphicon glyphicon-refresh'
								},
								fields : {
									credittermscode : {
										message : 'The Credit Terms Code is not valid',
										validators : {
											/*stringLength : {
												
												max : 2,
												message : 'The Credit Terms Code should be 1 or 2 characters long'
											},*/
											regexp : {
												regexp : /^[a-zA-Z0-9]{1,2}$/, 
												message : 'Please enter up to 2 alphanumerics without special character. Example : CR'
							
											},
											callback: {
												message: 'The Credit Terms Code should not start or end with empty spaces',
												callback: function(value, validator, $field){
						                         
													if (value === '') {
						                                return true;
						                            }
													
													
												//If found return true the above error message is displayed
												 if (value.search(/^\s|\s$/) < 0) {
													 
						                                return {
						                                    valid: true,
						                                    message: ''
						                                };
						                            } else {
						                            	return false;
						                            }
												 
											
											
												return true;
											}
										}
										}
									},
									credittermsdescription : {
										message : 'The Credit Terms Description is not valid',
										validators : {
											/*notEmpty : {
												message : 'The Store Name is required and cannot be empty'
											},*/
											stringLength : {
												min :1,
												max : 255,
												message : 'The Credit Terms Description should be more than 1 and less than 255 characters long'
											},
											callback: {
												message: 'The Credit Terms Description should not start or end with empty spaces',
												callback: function(value, validator, $field){
						                         
													if (value === '') {
						                                return true;
						                            }
													
													
												//If found return true the above error message is displayed
												 if (value.search(/^\s|\s$/) < 0) {
													 
						                                return {
						                                    valid: true,
						                                    message: ''
						                                };
						                            } else {
						                            	return false;
						                            }
												 
											
											
												return true;
											}
										}
										}
									}
								}
							});
					
					
					$('.acceptinvoiceform')
					.bootstrapValidator(
							{
								message : 'This value is not valid',
								feedbackIcons : {
									valid : 'glyphicon glyphicon-ok',
									invalid : 'glyphicon glyphicon-remove',
									validating : 'glyphicon glyphicon-refresh'
								},
								fields : {
									invoiceNumber : {
										message : 'The Invoice Number is not valid',
										validators : {
											notEmpty : {
												message : 'The Invoice Number is required and cannot be empty'
											},
											stringLength : {
												
												max : 20,
												message : 'The Invoice Number should be less than 20 characters long'
											},
											callback: {
												message: 'The Invoice Number should not start or end with empty spaces',
												callback: function(value, validator, $field){
						                         
													if (value === '') {
						                                return true;
						                            }
													
													
												
													
												//If found return true the above error message is displayed
												 if (value.search(/^\s|\s$/) < 0) {
													 
						                                return {
						                                    valid: true,
						                                    message: ''
						                                };
						                            } else {
						                            	return false;
						                            }
												 
											
											
												return true;
											}
										}
										}
									},
									invoiceAmount : {
										message : 'The Invoice Amount is not valid',
										validators : {
											notEmpty : {
												message : 'The Invoice Amount is required and cannot be empty'
											},
										/*	stringLength : {
												min :1,
												max : 10,
												message : 'The Invoice Amount should be more than 1 and less than 255 characters long'
											},*/
											callback: {
												message: 'The Invoice Amount should not start or end with empty spaces',
												callback: function(value, validator, $field){
						                         
													if (value === '') {
						                                return true;
						                            }
													
													 if (value.search(/^(?!\.?$)\d{0,10}(\.\d{0,2})?$/) < 0) {
														 
							                                return {
							                                    valid: false,
							                                    message: 'The Invoice Amount should be decimal up to 10 digits before decimal point and 2 digits after decimal point, Example : 12.87'
							                                };
							                            } else {
							                            	return true;
							                            }
												//If found return true the above error message is displayed
												 if (value.search(/^\s|\s$/) < 0) {
													 
						                                return {
						                                    valid: true,
						                                    message: ''
						                                };
						                            } else {
						                            	return false;
						                            }
												 
											
											
												return true;
											}
										}
										}
									},
									invoicenote : {
										message : 'Select Invoice Note',
										validators : {
											notEmpty : {
												message : 'Select Invoice Note'
											}
										}
									},
									invoiceRecipient:{
										message : 'The Invoice Recepient is not valid',
										validators : {
											notEmpty : {
												message : 'The Invoice Recepient is required and cannot be empty'
											},
											regexp : {
												regexp : /[a-zA-Z]{1,20}/, 
												message : 'The Invoice Recepient can only consist of alphabetical without special character up to 20 length. Example : Ed Shereen',
							
											},
											stringLength : {
												min :1,
												max : 20,
												message : 'The Invoice Recepient should be up to 20 characters long'
											},
											callback: {
												message: 'The Invoice Recepient should not start or end with empty spaces',
												callback: function(value, validator, $field){
						                         
													if (value === '') {
						                                return true;
						                            }
													
												
												//If found return true the above error message is displayed
												 if (value.search(/^\s|\s$/) < 0) {
													 
						                                return {
						                                    valid: true,
						                                    message: ''
						                                };
						                            } else {
						                            	return false;
						                            }
												 
											
											
												return true;
											}
										}
										
			
										}
									}
								}
							});
					
					
					
					$('.VendorBranchForm')
					.bootstrapValidator(
							{
								message : 'This value is not valid',
								feedbackIcons : {
									valid : 'glyphicon glyphicon-ok',
									invalid : 'glyphicon glyphicon-remove',
									validating : 'glyphicon glyphicon-refresh'
								},
								fields : {
									vendorCode : {
										message : 'The Vendor Code is not valid',
										validators : {
											stringLength : {
												
												max : 6,
												message : 'The Vendor Code should be 6 characters long'
											},
											notEmpty : {
											message : 'The Vendor Code is required and cannot be empty'
										},
											regexp : {
												regexp : /^[0-9]*$/,
												message : 'Invalid format! Please enter 6 numerics.'
											},
											callback: {
												message: 'The Vendor Code should not start or end with empty spaces',
												callback: function(value, validator, $field){
						                         
													if (value === '') {
						                                return true;
						                            }
													
													
													
													
												//If found return true the above error message is displayed
												 if (value.search(/^\s|\s$/) < 0) {
													 
						                                return {
						                                    valid: true,
						                                    message: ''
						                                };
						                            } else {
						                            	return false;
						                            }
												 
											
											
												return true;
											}
										}
										}
									},
									vendorBranchCode : {
										message : 'The Vendor Branch Code is not valid',
										validators : {
											notEmpty : {
												message : 'The Vendor Branch Code is required and cannot be empty'
											},
											stringLength : {
												
												max : 2,
												message : 'The Vendor Branch Code should be 2 Numeric characters long'
											},
											regexp : {
												regexp : /^[0-9]*$/,
												message : 'Invalid format! Please enter 2 numerics.'
											},
											callback: {
												message: 'The Vendor Branch Code should not start or end with empty spaces',
												callback: function(value, validator, $field){
						                         
													if (value === '') {
						                                return true;
						                            }
													
													
												//If found return true the above error message is displayed
												 if (value.search(/^\s|\s$/) < 0) {
													 
						                                return {
						                                    valid: true,
						                                    message: ''
						                                };
						                            } else {
						                            	return false;
						                            }
												 
											
											
												return true;
											}
										}
										}
									},
									name : {
										message : 'The Vendor Name is not valid',
										validators : {
											stringLength : {
												
												max : 255,
												message : 'The Vendor Name should be up to 255 characters long'
											},
											notEmpty : {
												message : 'The Vendor Name is required and cannot be empty'
											},
											callback: {
												message: 'The Vendor Name should not start or end with empty spaces',
												callback: function(value, validator, $field){
						                         
													if (value === '') {
						                                return true;
						                            }
													
													
												//If found return true the above error message is displayed
												 if (value.search(/^\s|\s$/) < 0) {
													 
						                                return {
						                                    valid: true,
						                                    message: ''
						                                };
						                            } else {
						                            	return false;
						                            }
												 
											
											
												return true;
											}
										}
										}
									},
									
									branchDescription : {
										message : 'The Vendor Branch Description is not valid',
										validators : {
											stringLength : {
												
												max : 255,
												message : 'The Vendor Branch Description should be up to 255 characters long'
											},
											notEmpty : {
												message : 'The Vendor Branch Description is required and cannot be empty'
											},
											callback: {
												message: 'The Vendor Branch Description should not start or end with empty spaces',
												callback: function(value, validator, $field){
						                         
													if (value === '') {
						                                return true;
						                            }
													
													
												//If found return true the above error message is displayed
												 if (value.search(/^\s|\s$/) < 0) {
													 
						                                return {
						                                    valid: true,
						                                    message: ''
						                                };
						                            } else {
						                            	return false;
						                            }
												 
											
											
												return true;
											}
										}
										}
									},
									
									vendorAddressLine1 : {
										message : 'The Vendor Address Line is not valid',
										validators : {
											stringLength : {
												
												max : 255,
												message : 'The Vendor Address Line should be up to 255 characters long'
											},
											callback: {
												message: 'The Vendor Address Line should not start or end with empty spaces',
												callback: function(value, validator, $field){
						                         
													if (value === '') {
						                                return true;
						                            }
													
													
												//If found return true the above error message is displayed
												 if (value.search(/^\s|\s$/) < 0) {
													 
						                                return {
						                                    valid: true,
						                                    message: ''
						                                };
						                            } else {
						                            	return false;
						                            }
												 
											
											
												return true;
											}
										}
										}
									},
									
									vendorAddressLine2 : {
										message : 'The Vendor Address Line is not valid',
										validators : {
											stringLength : {
												
												max : 255,
												message : 'The Vendor Address Line should be up to 255 characters long'
											},
											callback: {
												message: 'The Vendor Address Line should not start or end with empty spaces',
												callback: function(value, validator, $field){
						                         
													if (value === '') {
						                                return true;
						                            }
													
													
												//If found return true the above error message is displayed
												 if (value.search(/^\s|\s$/) < 0) {
													 
						                                return {
						                                    valid: true,
						                                    message: ''
						                                };
						                            } else {
						                            	return false;
						                            }
												 
											
											
												return true;
											}
										}
										}
									},
									
									vendorAddressLine3 : {
										message : 'The Vendor Address Line is not valid',
										validators : {
											stringLength : {
												
												max : 255,
												message : 'The Vendor Address Line should be up to 255 characters long'
											},
											callback: {
												message: 'The Vendor Address Line should not start or end with empty spaces',
												callback: function(value, validator, $field){
						                         
													if (value === '') {
						                                return true;
						                            }
													
													
												//If found return true the above error message is displayed
												 if (value.search(/^\s|\s$/) < 0) {
													 
						                                return {
						                                    valid: true,
						                                    message: ''
						                                };
						                            } else {
						                            	return false;
						                            }
												 
											
											
												return true;
											}
										}
										}
									},
									
									postCode : {
										message : 'The Vendor Post Code is not valid',
										validators : {
											stringLength : {
												
												max : 10,
												message : 'The Vendor Post Code should be up to 10 characters long'
											},
											regexp : {
												/*regexp : /^[a-zA-Z0-9]{1,10}$/, */
												regexp : /^[0-9]{1,10}$/,
												message : 'Please enter up to 10 numerics. Example : 57000'
							
											},
											callback: {
												message: 'The Vendor Post Code should not start or end with empty spaces',
												callback: function(value, validator, $field){
						                         
													if (value === '') {
						                                return true;
						                            }
													
													
												//If found return true the above error message is displayed
												 if (value.search(/^\s|\s$/) < 0) {
													 
						                                return {
						                                    valid: true,
						                                    message: ''
						                                };
						                            } else {
						                            	return false;
						                            }
												 
											
											
												return true;
											}
										}
										}
									},
									
									postCodeArea : {
										message : 'The Vendor Post Code Area is not valid',
										validators : {
											stringLength : {
												
												max : 10,
												message : 'The Vendor Post Code Area should be up to 70 characters long'
											},
											callback: {
												message: 'The Vendor Post Code Area should not start or end with empty spaces',
												callback: function(value, validator, $field){
						                         
													if (value === '') {
						                                return true;
						                            }
													
													
												//If found return true the above error message is displayed
												 if (value.search(/^\s|\s$/) < 0) {
													 
						                                return {
						                                    valid: true,
						                                    message: ''
						                                };
						                            } else {
						                            	return false;
						                            }
												 
											
											
												return true;
											}
										}
										}
									},
								}
							});

			
					$('.ProjectForm')
					.bootstrapValidator(
							{
								message : 'This value is not valid',
								feedbackIcons : {
									valid : 'glyphicon glyphicon-ok',
									invalid : 'glyphicon glyphicon-remove',
									validating : 'glyphicon glyphicon-refresh'
								},
								fields : {
									projectCode : {
										message : 'The Project Code is required and cannot be empty',
										validators : {
											notEmpty : {
												message : 'The Project Code is required and cannot be empty'
											},

											stringLength : {
												min :1,
												max : 15,
												message : 'The Project Code should be more than 1 and less than 15 characters long'
											},
											callback: {
												message: 'The Project Code should not start or end with empty spaces',
												callback: function(value, validator, $field){
						                         
													if (value === '') {
						                                return true;
						                            }
													
													
												//If found return true the above error message is displayed
												 if (value.search(/^\s|\s$/) < 0) {
													 
						                                return {
						                                    valid: true,
						                                    message: ''
						                                };
						                            } else {
						                            	return false;
						                            }
												 
											
											
												return true;
											}
										}
										}
									},
									projectName : {
	
										validators : {
											
											stringLength : {
												min :1,
												max : 70,
												message : 'The Project Name should be more than 1 and less than 70 characters long'
											},
											callback: {
												message: 'The Project Name should not start or end with empty spaces',
												callback: function(value, validator, $field){
						                         
													if (value === '') {
						                                return true;
						                            }
													
													
												//If found return true the above error message is displayed
												 if (value.search(/^\s|\s$/) < 0) {
													 
						                                return {
						                                    valid: true,
						                                    message: ''
						                                };
						                            } else {
						                            	return false;
						                            }
												 
											
											
												return true;
											}
										}
										}
									},
									projectDescription : {
	
										validators : {
											
											stringLength : {
												min :1,
												max : 255,
												message : 'The Project Description should be more than 1 and less than 255 characters long'
											},
											callback: {
												message: 'The Project Description should not start or end with empty spaces',
												callback: function(value, validator, $field){
						                         
													if (value === '') {
						                                return true;
						                            }
													
													
												//If found return true the above error message is displayed
												 if (value.search(/^\s|\s$/) < 0) {
													 
						                                return {
						                                    valid: true,
						                                    message: ''
						                                };
						                            } else {
						                            	return false;
						                            }
												 
											
											
												return true;
											}
										}
										}
									}
									
									
								}
							});
					
					
					$('.poForm')
					.bootstrapValidator(
							{
								message : 'This value is not valid',
								feedbackIcons : {
									valid : 'glyphicon glyphicon-ok',
									invalid : 'glyphicon glyphicon-remove',
									validating : 'glyphicon glyphicon-refresh'
								},
								submitButtons: 'button[type="submit"]',
								fields : {
									contactPersonPhone : {
										message : 'The Contact Person Phone is required and cannot be empty',
										validators : {
											regexp : {
												/*regexp : /\+?\d+/,*/
												regexp : /^[0-9][0-9]{10}$/ ,
												message : 'Invalid format! Please enter Valid 10 digits Phone Number.'
											},
											 /*integer: {
							                        message: 'The Contact Person Phone should be digit'
							                    },*/
											notEmpty : {
												message : 'The Contact Person Phone is required and cannot be empty'
											},
											callback: {
												message: 'The Contact Person Phone should not start or end with empty spaces',
												callback: function(value, validator, $field){
						                         
													if (value === '') {
						                                return true;
						                            }
													
													
												//If found return true the above error message is displayed
												 if (value.search(/^\s|\s$/) < 0) {
													 
						                                return {
						                                    valid: true,
						                                    message: ''
						                                };
						                            } else {
						                            	return false;
						                            }
												 
											
											
												return true;
											}
										}
										}
									},
									
								
									
									ordCatCode:{
										
										message : 'Select Valid Order Category Code which has Authorization Grid',
										validators : {
											notEmpty : {
												message : 'Select Valid Order Category Code which has Authorization Grid'
											}
										}
										
									},
									vendorCode:{
										
										message : 'Select Valid Vendor Code',
										validators : {
											notEmpty : {
												message : 'Select Valid Vendor Code'
											}
										}
										
									},
									
									ordSubCCode:{
										
										message : 'Select Valid Order Category Code',
										validators : {
											notEmpty : {
												message : 'Select Valid Order Category Code'
											}
										}
									},
									verifier:{
										
										message : 'Select Verifier',
										validators : {
											notEmpty : {
												message : 'Select Verifier'
											}
										}
									},
									authorizer:{
										
										message : 'Select Authorizer',
										validators : {
											notEmpty : {
												message : 'Select Authorizer'
											}
										}
									},
									contactPersonEmail : {
	
										validators : {
											emailAddress:{
												
												 message: 'The value is not a valid email address'
											},
											notEmpty : {
												message : 'The email address is required and cannot be empty'
											},
											stringLength : {
												min :1,
												max : 70,
												message : 'The Contact Person Email should be more than 1 and less than 70 characters long'
											},
											callback: {
												message: 'The Contact Person Email should not start or end with empty spaces',
												callback: function(value, validator, $field){
						                         
													if (value === '') {
						                                return true;
						                            }
													
													
												//If found return true the above error message is displayed
												 if (value.search(/^\s|\s$/) < 0) {
													 
						                                return {
						                                    valid: true,
						                                    message: ''
						                                };
						                            } else {
						                            	return false;
						                            }
												 
											
											
												return true;
											}
										}
										}
									},vendorPersonInCharge : {
										message : 'The Vendor Person In Charge is required and cannot be empty',
										validators : {
											 
											notEmpty : {
												message : 'The Vendor Person In Charge is required and cannot be empty'
											},
											callback: {
												message: 'The Vendor Person In Charge should not start or end with empty spaces',
												callback: function(value, validator, $field){
						                         
													if (value === '') {
						                                return true;
						                            }
													
													
												//If found return true the above error message is displayed
												 if (value.search(/^\s|\s$/) < 0) {
													 
						                                return {
						                                    valid: true,
						                                    message: ''
						                                };
						                            } else {
						                            	return false;
						                            }
												 
											
											
												return true;
											}
										}
										}
									},
									vendorPersonInChargePhone : {
										message : 'The Vendor Contact Phone is required and cannot be empty',
										validators : {
											regexp : {
												/*regexp : /\+?\d+/,*/
												regexp : /^[0-9][0-9]{10}$/ ,
												message : 'Invalid format! Please enter Valid 10 digits Phone Number.'
											},
											
											/*regexp : {
												regexp : /\+?\d+/,
												message : 'Invalid format! Please enter Phone Number.'
											},*/
											/* digit: {
							                        message: 'The Vendor Contact Phone should be digit'
							                    },*/
											notEmpty : {
												message : 'The Vendor Contact Phone is required and cannot be empty'
											},
											callback: {
												message: 'The Vendor Contact Phone should not start or end with empty spaces',
												callback: function(value, validator, $field){
						                         
													if (value === '') {
						                                return true;
						                            }
													
													
												//If found return true the above error message is displayed
												 if (value.search(/^\s|\s$/) < 0) {
													 
						                                return {
						                                    valid: true,
						                                    message: ''
						                                };
						                            } else {
						                            	return false;
						                            }
												 
											
											
												return true;
											}
										}
										}
									},
									vendorPersonInChargeEmail : {
	
										validators : {
											emailAddress:{
												
												 message: 'The Vendor Person In Charge Email is not a valid email address'
											},
											notEmpty : {
												message : 'The Vendor Person In Charge Email is required and cannot be empty'
											},
											stringLength : {
												min :1,
												max : 70,
												message : 'The Vendor Person In Charge Email should be more than 1 and less than 70 characters long'
											},
											callback: {
												message: 'The Vendor Person In Charge Email should not start or end with empty spaces',
												callback: function(value, validator, $field){
						                         
													if (value === '') {
						                                return true;
						                            }
													
													
												//If found return true the above error message is displayed
												 if (value.search(/^\s|\s$/) < 0) {
													 
						                                return {
						                                    valid: true,
						                                    message: ''
						                                };
						                            } else {
						                            	return false;
						                            }
												 
											
											
												return true;
											}
										}
										}
									},
									vendorAddressLine1 : {
	
										validators : {
										
											callback: {
												message: 'The Vendor Address should not start or end with empty spaces',
												callback: function(value, validator, $field){
						                         
													if (value === '') {
						                                return true;
						                            }
													
													
												//If found return true the above error message is displayed
												 if (value.search(/^\s|\s$/) < 0) {
													 
						                                return {
						                                    valid: true,
						                                    message: ''
						                                };
						                            } else {
						                            	return false;
						                            }
												 
											
											
												return true;
											}
										}
										}
									},
									vendorAddressLine2 : {
	
										validators : {
										
											callback: {
												message: 'The Vendor Address should not start or end with empty spaces',
												callback: function(value, validator, $field){
						                         
													if (value === '') {
						                                return true;
						                            }
													
													
												//If found return true the above error message is displayed
												 if (value.search(/^\s|\s$/) < 0) {
													 
						                                return {
						                                    valid: true,
						                                    message: ''
						                                };
						                            } else {
						                            	return false;
						                            }
												 
											
											
												return true;
											}
										}
										}
									},
									vendorAddressLine3 : {
	
										validators : {
										
											callback: {
												message: 'The Vendor Address should not start or end with empty spaces',
												callback: function(value, validator, $field){
						                         
													if (value === '') {
						                                return true;
						                            }
													
													
												//If found return true the above error message is displayed
												 if (value.search(/^\s|\s$/) < 0) {
													 
						                                return {
						                                    valid: true,
						                                    message: ''
						                                };
						                            } else {
						                            	return false;
						                            }
												 
											
											
												return true;
											}
										}
										}
									},
									vendorPostArea : {
	
										validators : {
											
										
											callback: {
												message: 'The Post Area should not start or end with empty spaces',
												callback: function(value, validator, $field){
						                         
													if (value === '') {
						                                return true;
						                            }
													
													
												//If found return true the above error message is displayed
												 if (value.search(/^\s|\s$/) < 0) {
													 
						                                return {
						                                    valid: true,
						                                    message: ''
						                                };
						                            } else {
						                            	return false;
						                            }
												 
											
											
												return true;
											}
										}
										}
									},	quoteNo : {
	
										validators : {
											
											notEmpty : {
												message : 'The Quotation Number is required and cannot be empty'
											},
											callback: {
												message: 'The Quotation Number should not start or end with empty spaces',
												callback: function(value, validator, $field){
						                         
													if (value === '') {
						                                return true;
						                            }
													
													
												//If found return true the above error message is displayed
												 if (value.search(/^\s|\s$/) < 0) {
													 
						                                return {
						                                    valid: true,
						                                    message: ''
						                                };
						                            } else {
						                            	return false;
						                            }
												 
											
											
												return true;
											}
										}
										}
									},
									
							/*		quoteDate : {
										validators : {
											notEmpty : {
												message : 'The Date is required and cannot be empty'
											}
											
										}
										
									},*/
									
									paymentTerms : {
										message : 'The Payment Terms is required and cannot be empty',
										validators : {
											 
											notEmpty : {
												message : 'The Payment Terms is required and cannot be empty'
											},
											callback: {
												message: 'The Payment Terms should not start or end with empty spaces',
												callback: function(value, validator, $field){
						                         
													if (value === '') {
						                                return true;
						                            }
													
													
												//If found return true the above error message is displayed
												 if (value.search(/^\s|\s$/) < 0) {
													 
						                                return {
						                                    valid: true,
						                                    message: ''
						                                };
						                            } else {
						                            	return false;
						                            }
												 
											
											
												return true;
											}
										}
										}
									},
									deliveryInstruction : {
										message : 'The Delivery Instruction is required and cannot be empty',
										validators : {
											 
											notEmpty : {
												message : 'The Delivery Instruction is required and cannot be empty'
											},
											callback: {
												message: 'The Delivery Instruction should not start or end with empty spaces',
												callback: function(value, validator, $field){
						                         
													if (value === '') {
						                                return true;
						                            }
													
													
												//If found return true the above error message is displayed
												 if (value.search(/^\s|\s$/) < 0) {
													 
						                                return {
						                                    valid: true,
						                                    message: ''
						                                };
						                            } else {
						                            	return false;
						                            }
												 
											
											
												return true;
											}
										}
										}
									},
									issuerRemark : {
	
										validators : {
											
										
											callback: {
												message: 'The Issuer Remarks should not start or end with empty spaces',
												callback: function(value, validator, $field){
						                         
													if (value === '') {
						                                return true;
						                            }
													
													
												//If found return true the above error message is displayed
												 if (value.search(/^\s|\s$/) < 0) {
													 
						                                return {
						                                    valid: true,
						                                    message: ''
						                                };
						                            } else {
						                            	return false;
						                            }
												 
											
											
												return true;
											}
										}
										}
									},	firstApprover : {
										message : 'The First Approver is required and cannot be empty',
										validators : {
											 
											notEmpty : {
												message : 'The First Approver is required and cannot be empty'
											},
											different: {
						                        field: 'secondApprover,thirdApprover',
						                        message: 'The Approvers cannot be the same'
						                    },
											callback: {
												message: 'The First Approver should not start or end with empty spaces',
												callback: function(value, validator, $field){
						                         
													if (value === '') {
						                                return true;
						                            }
													
													
												//If found return true the above error message is displayed
												 if (value.search(/^\s|\s$/) < 0) {
													 
						                                return {
						                                    valid: true,
						                                    message: ''
						                                };
						                            } else {
						                            	return false;
						                            }
												 
											
											
												return true;
											}
										}
										}
									},
									secondApprover : {
										message : 'The Second Approver is required and cannot be empty',
										validators : {
											 
											notEmpty : {
												message : 'The Second Approver is required and cannot be empty'
											},
											different: {
						                        field: 'firstApprover,thirdApprover',
						                        message: 'The Approvers cannot be the same'
						                    },
											callback: {
												message: 'The Second Approver should not start or end with empty spaces',
												callback: function(value, validator, $field){
						                         
													if (value === '') {
						                                return true;
						                            }
													
													
												//If found return true the above error message is displayed
												 if (value.search(/^\s|\s$/) < 0) {
													 
						                                return {
						                                    valid: true,
						                                    message: ''
						                                };
						                            } else {
						                            	return false;
						                            }
												 
											
											
												return true;
											}
										}
										}
									},
									thirdApprover : {
	
										validators : {
											
											notEmpty : {
												message : 'The Third Approver is required and cannot be empty'
											},
											different: {
						                        field: 'firstApprover,secondApprover',
						                        message: 'The Approvers cannot be the same'
						                    },
											callback: {
												message: 'The Third Approver should not start or end with empty spaces',
												callback: function(value, validator, $field){
						                         
													if (value === '') {
						                                return true;
						                            }
													
													
												//If found return true the above error message is displayed
												 if (value.search(/^\s|\s$/) < 0) {
													 
						                                return {
						                                    valid: true,
						                                    message: ''
						                                };
						                            } else {
						                            	return false;
						                            }
												 
											
											
												return true;
											}
										}
										}
									},
									firstApproverDesignation : {
	
										validators : {
											
											notEmpty : {
												message : 'The First Approver Designation is required and cannot be empty'
											},
											callback: {
												message: 'The First Approver Designation should not start or end with empty spaces',
												callback: function(value, validator, $field){
						                         
													if (value === '') {
						                                return true;
						                            }
													
													
												//If found return true the above error message is displayed
												 if (value.search(/^\s|\s$/) < 0) {
													 
						                                return {
						                                    valid: true,
						                                    message: ''
						                                };
						                            } else {
						                            	return false;
						                            }
												 
											
											
												return true;
											}
										}
										}
									},
									firstApproverDesignation : {
	
										validators : {
											
											notEmpty : {
												message : 'The First Approver Designation is required and cannot be empty'
											},
											callback: {
												message: 'The First Approver Designation should not start or end with empty spaces',
												callback: function(value, validator, $field){
						                         
													if (value === '') {
						                                return true;
						                            }
													
													
												//If found return true the above error message is displayed
												 if (value.search(/^\s|\s$/) < 0) {
													 
						                                return {
						                                    valid: true,
						                                    message: ''
						                                };
						                            } else {
						                            	return false;
						                            }
												 
											
											
												return true;
											}
										}
										}
									},
									secondApproverDesignation : {
	
										validators : {
											
											notEmpty : {
												message : 'The Second Approver Designation is required and cannot be empty'
											},
											callback: {
												message: 'The Second Approver Designation should not start or end with empty spaces',
												callback: function(value, validator, $field){
						                         
													if (value === '') {
						                                return true;
						                            }
													
													
												//If found return true the above error message is displayed
												 if (value.search(/^\s|\s$/) < 0) {
													 
						                                return {
						                                    valid: true,
						                                    message: ''
						                                };
						                            } else {
						                            	return false;
						                            }
												 
											
											
												return true;
											}
										}
										}
									},
									thirdApproverDesignation : {
	
										validators : {
											
											notEmpty : {
												message : 'The Third Approver Designation is required and cannot be empty'
											},
											callback: {
												message: 'The Third Approver Designation should not start or end with empty spaces',
												callback: function(value, validator, $field){
						                         
													if (value === '') {
						                                return true;
						                            }
													
													
												//If found return true the above error message is displayed
												 if (value.search(/^\s|\s$/) < 0) {
													 
						                                return {
						                                    valid: true,
						                                    message: ''
						                                };
						                            } else {
						                            	return false;
						                            }
												 
											
											
												return true;
											}
										}
										}
									}
									
									
								}
							});
				
					
					$('.ItemForm')
					.bootstrapValidator(
							{
								message : 'This value is not valid',
								feedbackIcons : {
									valid : 'glyphicon glyphicon-ok',
									invalid : 'glyphicon glyphicon-remove',
									validating : 'glyphicon glyphicon-refresh'
								},
								fields : {

									/*itemOrder : {
										message : 'Item Order is not valid',
										validators : {
											notEmpty : {
												message : 'Item Order is required and cannot be empty'
											}
										}
									},*/
									particulars : {
										message : 'Particulars is not valid',
										validators : {
											notEmpty : {
												message : 'Particulars is required and cannot be empty'
											}
										}
									},
									packing : {
										message : 'Packing is not valid',
										validators : {
											notEmpty : {
												message : 'Packing is required and cannot be empty'
											}
										}
									},
									quantity : {
										message : 'Quantity is not valid',
										validators : {
											notEmpty : {
												message : 'Quantity is required and cannot be empty'
											},
											regexp : {
												regexp : /^[0-9]*$/,
												message : 'Invalid format! Please enter numerics.'
											}
										}
									},
									taxCode : {
										message : 'Tax Code is not valid',
										validators : {
											notEmpty : {
												message : 'Tax Code is required and cannot be empty'
											}
										}
									},
									taxRate : {
										message : 'Tax Rate is not valid',
										validators : {
											notEmpty : {
												message : 'Tax Rate is required and cannot be empty'
											},
											
							                regexp : {
												/*regexp : /^\d{1,2}(\.\d{1,2})?$/, */
												regexp : /(?:\b|-)([1-9]{1,2}[0]?|100)\b/,
												message : 'Tax Rate can only be between 0 to 100 with only two decimal places. Example : 99.22',
							
											}
										}
									},
									unitPriceExclGST : {
										message : 'Unit Price Excl GST is not valid',
										validators : {
											notEmpty : {
												message : 'Unit Price Excl GST is required and cannot be empty'
											}
										}
									},
									unitPriceInclGST : {
										message : 'Unit Price Incl GST is not valid',
										validators : {
											notEmpty : {
												message : 'Unit Price Incl GST is required and cannot be empty'
											}
										}
									},
									taxAmount : {
										message : 'Tax Amount is not valid',
										validators : {
											notEmpty : {
												message : 'Tax Amount is required and cannot be empty'
											}
										}
									},
									profitCenter : {
										message : 'Profit Center is not valid',
										validators : {
											notEmpty : {
												message : 'Profit Center is required and cannot be empty'
											}
										}
									},
									itemNo : {
										message : 'Item No is not valid',
										validators : {
											notEmpty : {
												message : 'Item No is required and cannot be empty'
											}
										}
									},
									discount : {
										message : 'Item No is not valid',
										validators : {
											 regexp : {
													regexp : /(?:\b|-)([1-9]{1,2}[0]?|100)\b/, 
													message : 'The discount can only be between 0 to 100 with only two decimal places. Example : 99.22',
												}
										}
									},
									variance : {
										message : 'Variance is not valid',
										validators : {
											notEmpty : {
												message : 'Variance is required and cannot be empty'
											},
											
							                regexp : {
												regexp : /(?:\b|-)([1-9]{1,2}[0]?|100)\b/, 
												message : 'The Allowed Variance can only be between 0 to 100 with only two decimal places. Example : 99.22',
							
											}
										}
									}
							
								}
							});

				});