
$(document).ready(function(){ 
	
    var today = new Date();
    var month = today.getMonth() + 1;
    var day = today.getDate();
    var year = today.getFullYear();
    var last_three_year = today.getFullYear() - 3;
    var last_three_month = today.getMonth() + 1 - 3;
    if(month < 10)
        month = '0' + month.toString();
    if(day < 10)
        day = '0' + day.toString();    
    var maxDate = year + '-' + month + '-' + day;
    var minDate_AdvSearch = last_three_year + '-' + month + '-' + day;
    var minDate_quote = year + '-' + last_three_month + '-' + day;
    var minDate_val = new Date(minDate_AdvSearch);
    var quote_minDate_val = new Date(minDate_quote);
    
    
    $( '#startDate' ).datepicker({dateFormat: 'dd-MM-yy', minDate : new Date()});
    $( '#endDate' ).datepicker({dateFormat: 'dd-MM-yy', minDate : new Date()});
    $( '#issuedDate' ).datepicker({dateFormat: 'dd-MM-yy', minDate : minDate_val, maxDate : new Date()});
    $( '#quote_Date' ).datepicker({dateFormat: 'dd-MM-yy', minDate : quote_minDate_val, maxDate : new Date()});
    $( '#createdOn_Date' ).datepicker({dateFormat: 'dd-MM-yy', maxDate : minDate_val});
	
	$('#startDate').blur(function(){
		var Date_val = new Date($('#startDate').val());
	    $( '#endDate' ).datepicker({dateFormat: 'dd-MM-yy', minDate :  Date_val}); 
		var myDate = new Date(Date_val); 
		var calcDate = new Date(myDate.getTime() + 30*24*60*60*1000);
		month = calcDate.getMonth() + 1;
	    day = calcDate.getDate();
	    year = calcDate.getFullYear();
	    if(month < 10)
	        month = '0' + month.toString();
	    if(day < 10)
	        day = '0' + day.toString();    
	    maxDate = year + '-' + month + '-' + day;
		$('#endDates').val($('#startDates').val());
	});
		

	
	$('#startDate').on('change', function(){
		
		$('#endDate').removeClass('calendarclass');
		$('#endDate').removeClass('hasDatepicker');
		$('#endDate').unbind();
		
	
		$('#endDate').val('');
		
		var myDate = new Date($('#startDate').val());
	    $( '#endDate' ).datepicker({dateFormat: 'dd-MM-yy', minDate : myDate});
	    
	    if(myDate !=null){
	    	console.log(myDate);
	    	$('#startDate').datepicker({dateFormat: 'dd-MM-yy', minDate : myDate});
	  //  	$('#POForm').data('bootstrapValidator').updateStatus('quoteDate', 'VALID', null);
	    }
	    

	});
	
	$('#reset').on('click', function(){
		
		$('#endDate').val('');
		$('#startDate').val('');
		
	    
	});
	

	});