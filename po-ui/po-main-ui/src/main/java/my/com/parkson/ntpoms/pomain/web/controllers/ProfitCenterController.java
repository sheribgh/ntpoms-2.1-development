package my.com.parkson.ntpoms.pomain.web.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.common.entities.Company;
import my.com.parkson.ntpoms.common.entities.EmployeeMaster;
import my.com.parkson.ntpoms.common.entities.ProfitCenter;
import my.com.parkson.ntpoms.common.entities.Store;
import my.com.parkson.ntpoms.common.model.Pager;
import my.com.parkson.ntpoms.common.services.CompanyService;
import my.com.parkson.ntpoms.common.services.DepartmentService;
import my.com.parkson.ntpoms.common.services.EmployeeMasterService;
import my.com.parkson.ntpoms.common.services.ProfitCenterService;
import my.com.parkson.ntpoms.common.services.StoreService;
import my.com.parkson.ntpoms.pomain.web.models.AutocompleteForm;
import my.com.parkson.ntpoms.pomain.web.models.ProfitCenterForm;




@Slf4j
@Controller

public class ProfitCenterController extends AbstractMainController {

	private static final int BUTTONS_TO_SHOW = 5;
	private static final int INITIAL_PAGE = 0;
	private static final int INITIAL_PAGE_SIZE = 5;
	private static final int[] PAGE_SIZES = { 5, 10, 20 };

	@Autowired
	private ProfitCenterService pcrepo;

	@Autowired
	private CompanyService comrepo;

	@Autowired
	private StoreService storerepo;

	@Autowired
	private DepartmentService deptrepo;

	@Autowired
	private EmployeeMasterService empservice;

	@GetMapping("/master/profitcenter")
	public ModelAndView onPageLoadPC(@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page, @RequestParam("codefil") Optional<String> code,
			@RequestParam("companyfil") Optional<String> company, @RequestParam("namefil") Optional<String> name,
			@RequestParam("isIsactive") Optional<String> isActive,
			@RequestParam("departmentfil") Optional<String> department,
			@RequestParam("storefil") Optional<String> store) {
		ModelAndView modelAndView = new ModelAndView("/master/profitcenter");
		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
		Page<ProfitCenter> persons;

		Specification<ProfitCenter> s = new Specification<ProfitCenter>() {
			@Override
			public Predicate toPredicate(Root<ProfitCenter> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> ls = new ArrayList<>();
				if (code.isPresent() && !code.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("code")), "%" + code.get().toLowerCase() + "%"));
				}
				if (company.isPresent() && !company.get().isEmpty()) {

					ls.add(cb.like(cb.lower(root.get("company").get("code")), "%" + company.get().toLowerCase() + "%"));
				}
				if (name.isPresent() && !name.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("name")), "%" + name.get().toLowerCase() + "%"));
				}
				if (department.isPresent() && !department.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("department").get("code")),
							"%" + department.get().toLowerCase() + "%"));
				}
				if (store.isPresent() && !store.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("store").get("code")), "%" + store.get().toLowerCase() + "%"));
				}
				if (isActive.isPresent() && !isActive.get().isEmpty()) {
					ls.add(cb.equal(root.get("isActive"), "Active".equals(isActive.get())));
				}

				return cb.and(ls.toArray(new Predicate[0]));
			}
		};
		if (code.isPresent() || company.isPresent() || name.isPresent() || isActive.isPresent()
				|| department.isPresent() || store.isPresent()) {
			persons = pcrepo.getAllPageableProfitCenter(s, new PageRequest(evalPage, evalPageSize));
		} else {
			persons = pcrepo.getAllPageableProfitCenter(new PageRequest(evalPage, evalPageSize));
		}

		Pager pager = new Pager(persons.getTotalPages(), persons.getNumber(), BUTTONS_TO_SHOW);
		modelAndView.addObject("list", persons);
		modelAndView.addObject("selectedPageSize", evalPageSize);
		modelAndView.addObject("pageSizes", PAGE_SIZES);
		modelAndView.addObject("pager", pager);
		modelAndView.addObject("storetypelist", storeTypeforPC());
		modelAndView.addObject("depatlist", deptrepo.getAllActiveDepartment());
		modelAndView.addObject("cmplist", comrepo.getAllActiveCompany());
		modelAndView.addObject("pcheads", getProfitCenterMultiSelect());
		modelAndView.addObject("pcrights", getProfitCenterMultiSelect());
		modelAndView.addObject("codefil", code.isPresent() ? code.get() : "");
		modelAndView.addObject("companyfil", company.isPresent() ? company.get() : "");
		modelAndView.addObject("namefil", name.isPresent() ? name.get() : "");
		modelAndView.addObject("isIsactive", isActive.isPresent() ? isActive.get() : "");
		modelAndView.addObject("departmentfil", department.isPresent() ? department.get() : "");
		modelAndView.addObject("storefil", store.isPresent() ? store.get() : "");
		modelAndView.getModelMap().addAttribute("pcobj", new ProfitCenterForm());
		return modelAndView;
	}

	@RequestMapping(value = "/master/profitcenter/storebasedoncompany", method = RequestMethod.GET)
	public @ResponseBody List<Store> getStoreBasedOnCompany(@RequestParam("companycode") String companycode) {
		return storerepo.getAllActiveStoreByCompanyCode(companycode);
	}

	@PostMapping(value = "/master/createprofitcenter")
	public String createPC(@Valid @ModelAttribute("pcobj") ProfitCenterForm obj, BindingResult result, Model model,
			RedirectAttributes redirectAttributes) {
		if (!result.hasErrors() && obj.getCompany() != null && obj.getStore() != null && obj.getDeartment() != null) {
			ProfitCenter tobj = pojoToEntity(obj);
			tobj.setCode(obj.getStore() + obj.getDeartment());
			tobj.setCreatedBy(getCurrentUser().isPresent() ? getCurrentUser().get(): null);
			tobj.setCreatedOn(new Date());
			tobj.setIsactive(true);
			//tobj.setLastModifiedBy(getCurrentUser() != null ? getCurrentUser().getUsername() : null);
			tobj.setLastModifiedOn(new Date());
			pcrepo.createProfitCenter(tobj);
			redirectAttributes.addFlashAttribute("info", "Profit Center created successfully");
		} else {
			redirectAttributes.addFlashAttribute("error", "Select Company, Store and Department");
		}
		return "redirect:/master/profitcenter";
	}

	@PostMapping(value = "/master/updateprofitcenter")
	public String updateProfitCenter(@Valid @ModelAttribute("pcobj") ProfitCenterForm obj, BindingResult result,
			Model model, RedirectAttributes redirectAttributes) {
		if (!result.hasErrors() && obj.getCompany() != null && obj.getStore() != null && obj.getDeartment() != null) {
			ProfitCenter tobj = pcrepo.getProfitCenterByCode(obj.getPcCode());
			if (tobj != null) {
				//tobj.setLastModifiedBy(getCurrentUser() != null ? getCurrentUser().getUsername() : null);
				tobj.setAddressLine1(obj.getAddressLine1());
				tobj.setStoreType(obj.getStoreType());
				tobj.setAddressLine2(obj.getAddressLine2());
				tobj.setAddressLine3(obj.getAddressLine3());
				tobj.setStore(storerepo.getStoreByCode(obj.getStore()));
				tobj.setCompany(comrepo.getCompanyByCode(obj.getCompany()));
				tobj.setDepartment(deptrepo.getDepartmentByCode(obj.getDeartment()));
				tobj.setViewRights(obj.getPcViewRightsto());
				tobj.setHead(obj.getPcHeadto());
				tobj.setPostArea(obj.getPostArea());
				tobj.setPostCode(obj.getPostCode());
				tobj.setLastModifiedOn(new Date());
				tobj.setIsactive("Active".equals(obj.getIsactive()));
				pcrepo.createProfitCenter(tobj);
				redirectAttributes.addFlashAttribute("info", "Profit Center updated successfully");
			} else {
				redirectAttributes.addFlashAttribute("error", "Object not exists");
			}
		} else {
			redirectAttributes.addFlashAttribute("error", "Select Company, Store and Department");
		}
		return "redirect:/master/profitcenter";
	}

	@GetMapping(value = "/master/editprofitcenter/{pccode}")
	public String editPOUser(@PathVariable String pccode, Model model) {
		ProfitCenter tobj = pcrepo.getProfitCenterByCode(pccode);
		if (tobj != null) {
			model.addAttribute("pcobj", entityToPojo(tobj));
			model.addAttribute("storetypelist", storeTypeforPC());
			model.addAttribute("cmplist", comrepo.getAllActiveCompany());
			model.addAttribute("pcheads", getProfitCenterMultiSelect());
			model.addAttribute("pcrights", getProfitCenterMultiSelect());
			if (tobj.getHead() != null && !tobj.getHead().isEmpty())
				model.addAttribute("pcheadsto", Arrays.asList(tobj.getHead().split("\\s*,\\s*")));
			if (tobj.getViewRights() != null && !tobj.getViewRights().isEmpty())
				model.addAttribute("pcrightsto", Arrays.asList(tobj.getViewRights().split("\\s*,\\s*")));
			if (tobj.getCompany() != null) {
				model.addAttribute("strlist", storerepo.getAllActiveStoreByCompanyCode(tobj.getCompany().getCode()));
			} else {
				model.addAttribute("strlist", new ArrayList<>());

			}
			model.addAttribute("depatlist", deptrepo.getAllActiveDepartment());
			model.addAttribute("cmplist", comrepo.getAllActiveCompany());
			return "/master/edit_profitcenter";
		} else {
			return "redirect:/master/profitcenter";
		}
	}

	private ProfitCenterForm entityToPojo(ProfitCenter tobj) {
		ProfitCenterForm obj = new ProfitCenterForm();
		obj.setPcCode(tobj.getCode());
		obj.setAddressLine1(tobj.getAddressLine1());
		obj.setAddressLine2(tobj.getAddressLine2());
		obj.setAddressLine3(tobj.getAddressLine3());
		obj.setCreatedBy(tobj.getCreatedBy());
		//obj.setCreatedOn(tobj.getCreatedOn());
		obj.setDeactivatedBy(tobj.getDeactivatedBy());
		//obj.setDeactivatedOn(tobj.getDeactivatedOn());
		obj.setEndDate(tobj.getEndDate());
		obj.setIsactive(tobj.isIsactive() ? "Active" : "InActive");
		obj.setLastModifiedBy(tobj.getLastModifiedBy());
		//obj.setLastModifiedOn(tobj.getLastModifiedOn());
		obj.setStore(tobj.getStore() != null ? tobj.getStore().getCode() : null);
		obj.setStoreType(tobj.getStoreType());
		obj.setCompany(tobj.getCompany() != null ? tobj.getCompany().getCode() : null);
		obj.setDeartment(tobj.getDepartment() != null ? tobj.getDepartment().getCode() : null);
		obj.setPcAbbrName(tobj.getAbbrName());
		obj.setPcHeadto(tobj.getHead());
		obj.setPcName(tobj.getName());
		obj.setPostArea(tobj.getPostArea());
		obj.setPostCode(tobj.getPostCode());
		obj.setPcViewRightsto(tobj.getViewRights());
		obj.setReactivatedBy(tobj.getReactivatedBy());
		//obj.setReactivatedOn(tobj.getReactivatedOn());
		obj.setStartDate(tobj.getStartDate());
		return obj;
	}

	private ProfitCenter pojoToEntity(ProfitCenterForm tobj) {
		
		ProfitCenter obj = new ProfitCenter();
		obj.setCode(tobj.getPcCode());
		obj.setAddressLine1(tobj.getAddressLine1());
		obj.setAddressLine2(tobj.getAddressLine2());
		obj.setAddressLine3(tobj.getAddressLine3());
		obj.setCreatedBy(tobj.getCreatedBy());
		//obj.setCreatedOn(tobj.getCreatedOn());
		obj.setDeactivatedBy(tobj.getDeactivatedBy());
		//obj.setDeactivatedOn(tobj.getDeactivatedOn());
		obj.setEndDate(tobj.getEndDate());
		obj.setLastModifiedBy(tobj.getLastModifiedBy());
		//obj.setLastModifiedOn(tobj.getLastModifiedOn());
		obj.setIsactive("Active".equals(tobj.getIsactive()));
		obj.setStore(storerepo.getStoreByCode(tobj.getStore()));
		obj.setStoreType(tobj.getStoreType());
		obj.setCompany(comrepo.getCompanyByCode(tobj.getCompany()));
		obj.setDepartment(deptrepo.getDepartmentByCode(tobj.getDeartment()));
		obj.setAbbrName(tobj.getPcAbbrName());
		obj.setHead(tobj.getPcHeadto());
		obj.setName(tobj.getPcName());
		obj.setPostArea(tobj.getPostArea());
		obj.setPostCode(tobj.getPostCode());
		obj.setViewRights(tobj.getPcViewRightsto());
		obj.setReactivatedBy(tobj.getReactivatedBy());
		//obj.setReactivatedOn(tobj.getReactivatedOn());
		obj.setStartDate(tobj.getStartDate());
		
		return obj;
	}

	private List<String> storeTypeforPC() {
		List<String> lis = new ArrayList<>();
		lis.add("S");
		lis.add("W");
		lis.add("H");
		lis.add("O");
		return lis;
	}

	private List<String> getProfitCenterMultiSelect() {
		List<String> lis = new ArrayList<>();
		List<EmployeeMaster> temp = empservice.getAllActiveEmployeeMaster();
		if (temp != null) {
			for (EmployeeMaster e : temp) {
				StringBuilder s = new StringBuilder();
				s.append(e.getPsId());
				s.append("-");
				s.append(e.getName());
				lis.add(s.toString());
			}
		}
		return lis;
	}

	@Override
	protected String getHeaderTitle() {
		return "Manage ProfitCenter";
	}

	@RequestMapping(value = "/master/profitcenter/companyautocomplete", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<AutocompleteForm> autocompleteSuggestions(@RequestParam("query") String searchstr) {
		List<AutocompleteForm> lis = new ArrayList<>();
		for (Company m : comrepo.findBySearchCode(searchstr)) {
			AutocompleteForm a = new AutocompleteForm();
			a.setId(m.getName());
			a.setName(m.getCode());
			lis.add(a);
		}
		return lis;
	}
}