/**
 *****************************************************
 * COPYRIGHT UMAIR OTHMAN
 * 2017
 * IT Exec
 *****************************************************
 */
package my.com.parkson.ntpoms.pomain.email;

import java.util.Map;
import java.util.Set;

/**
 **************************************************************
 * Generic Email Criteria used to construct email body
 **************************************************************
 */
public class EmailCriteria {
	
	private String recipient;
	private Set<String> cc;
	private Map<String, String> params;
	private Set<String> attachment;
	private String templateName;
	private String systemName;	
	
	public String getSystemName() {
		return systemName;
	}
	
	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}
	
	public String getRecipient() {
		return recipient;
	}
	
	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}
	
	public Set<String> getCc() {
		return cc;
	}
	
	public void setCc(Set<String> cc) {
		this.cc = cc;
	}
	
	public Map<String, String> getParams() {
		return params;
	}
	
	public void setParams(Map<String, String> params) {
		this.params = params;
	}
	
	public Set<String> getAttachment() {
		return attachment;
	}
	
	public void setAttachment(Set<String> attachment) {
		this.attachment = attachment;
	}
	
	public String getTemplateName() {
		return templateName;
	}
	
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}
}