package my.com.parkson.ntpoms.pomain.web.models;

import java.io.Serializable;

public class PcLastPONumberForm implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String profitCode;
	
	private int runNumber;
	
	private String lastPONumber;
	
	private String year;

	public String getProfitCode() {
		return profitCode;
	}

	public void setProfitCode(String profitCode) {
		this.profitCode = profitCode;
	}

	public int getRunNumber() {
		return runNumber;
	}

	public void setRunNumber(int runNumber) {
		this.runNumber = runNumber;
	}

	public String getLastPONumber() {
		return lastPONumber;
	}

	public void setLastPONumber(String lastPONumber) {
		this.lastPONumber = lastPONumber;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}


	
}
