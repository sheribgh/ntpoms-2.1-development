package my.com.parkson.ntpoms.pomain.login;

public interface UserRepository {

	public User findUser(String username,String password, String company, String year);

}
