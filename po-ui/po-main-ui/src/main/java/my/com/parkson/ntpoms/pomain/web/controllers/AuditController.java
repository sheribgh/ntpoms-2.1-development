/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.pomain.web.controllers;

import static my.com.parkson.ntpoms.common.MessageCodes.FAILED_PERSIST_EXCEPTION;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.LogMessages;
import my.com.parkson.ntpoms.common.entities.SystemParameter;
import my.com.parkson.ntpoms.common.services.MessageService;
import my.com.parkson.ntpoms.common.services.ProfitCenterService;
import my.com.parkson.ntpoms.common.services.SystemParameterService;
import my.com.parkson.ntpoms.main.entities.AccessLogs;
import my.com.parkson.ntpoms.main.entities.AccessLogsSetting;
import my.com.parkson.ntpoms.main.entities.Audits;
import my.com.parkson.ntpoms.main.services.AccessLogsService;
import my.com.parkson.ntpoms.main.services.AccessLogsSettingService;
import my.com.parkson.ntpoms.main.services.AuditsService;
import my.com.parkson.ntpoms.pomain.web.models.LogSettingForm;
import my.com.parkson.ntpoms.pomain.web.models.Pager;

@Slf4j
@Controller
@RequiredArgsConstructor
public class AuditController extends AbstractMainController {
	private static final Logger LOGGER = Logger.getLogger(AuditController.class);
	private static final int BUTTONS_TO_SHOW = 5;
	private static final int INITIAL_PAGE = 0;
	private static final int INITIAL_PAGE_SIZE = 5;
	private static final int[] PAGE_SIZES = { 5, 10, 20 };

	@Autowired
	private AuditsService auditService;

	@Autowired
	private ProfitCenterService profitcenterService;

	@Autowired
	private AccessLogsSettingService logsettingrepo;

	/** The messages. */
	@Autowired
	private MessageService messages;

	@Autowired
	private AccessLogsService logrepo;

	@Autowired
	private SystemParameterService sysRepo;

	/**
	 * 
	 * @return
	 */
	@GetMapping("/audit/audittraillist")
	public ModelAndView showPersonsPage() {
		return new ModelAndView("/audit/audittrail");
	}

	/**
	 * 
	 * @param pageSize
	 * @param page
	 * @param username
	 * @param auditId
	 * @param operation
	 * @param message
	 * @param createdon
	 * @param company
	 * @param profitcenter
	 * @param sortingkey
	 * @param sortingorder
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/audit/audittraillistgrid", method = RequestMethod.GET)
	public String auditGrid(@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page, @RequestParam("username") Optional<String> username,
			@RequestParam("auditId") Optional<Integer> auditId, @RequestParam("operation") Optional<String> operation,
			@RequestParam("message") Optional<String> message, @RequestParam("createdon") Optional<String> createdon,
			@RequestParam("company") Optional<String> company,
			@RequestParam("profitcenter") Optional<String> profitcenter,
			@RequestParam("sortingkey") Optional<String> sortingkey,
			@RequestParam("sortingorder") Optional<String> sortingorder, Model model) {
		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
		Page<Audits> list;
		Specification<Audits> s = new Specification<Audits>() {
			@Override
			public Predicate toPredicate(Root<Audits> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> ls = new ArrayList<>();
				if (username.isPresent() && !username.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("username")), "%" + username.get().toLowerCase() + "%"));
				}
				if (auditId.isPresent()) {
					ls.add(cb.equal(root.get("id"), auditId.get()));
				}
				if (message.isPresent() && !message.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("message")), "%" + message.get().toLowerCase() + "%"));
				}
				if (createdon.isPresent() && !createdon.get().isEmpty()) {
					Expression<String> dateStringExpr = cb.function("to_char", String.class, root.get("createdOn"),
							cb.literal("YYYY-MM-DD HH12:MI:SS"));
					ls.add(cb.like(cb.lower(dateStringExpr), "%" + createdon.get().toLowerCase() + "%"));
				}
				if (operation.isPresent() && !operation.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("operation")), "%" + operation.get().toLowerCase() + "%"));
				}
				if (company.isPresent() && !company.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("company")), "%" + company.get().toLowerCase() + "%"));
				}
				if (profitcenter.isPresent() && !profitcenter.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("profitcenter")), "%" + profitcenter.get().toLowerCase() + "%"));
				}
				ls.add(cb.equal(root.get("company"), getCompany().isPresent() ? getCompany().get() : null));
				if (getRoleListCurrentUser().contains("ROLE_POUSER") && getCurrentUser().isPresent()) {
					List<Predicate> lspc = new ArrayList<>();
					for (String s : getPCHeadsBasedOnUserLogin(getCurrentUser().get())) {
						lspc.add(cb.equal(root.get("profitcenter"), s));
					}
					ls.add(cb.or(lspc.toArray(new Predicate[0])));
				}
				return cb.and(ls.toArray(new Predicate[0]));
			}
		};
		Sort sorttemp = new Sort(Sort.Direction.DESC, "id");
		if (sortingkey.isPresent() && sortingorder.isPresent()) {
			sorttemp = new Sort(sortingorder.get().equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC,
					sortingkey.get());
		}
		if (username.isPresent() || auditId.isPresent() || message.isPresent() || operation.isPresent()
				|| createdon.isPresent()) {
			list = auditService.getAllPagableAudit(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		} else {
			list = auditService.getAllPagableAudit(new PageRequest(evalPage, evalPageSize, sorttemp));
		}
		int serialid = 0;
		int pagesize = evalPage + 1;
		if (pagesize > 1) {
			int start = evalPageSize * pagesize;
			serialid = start - evalPageSize;
		}
		Pager pager = new Pager(list.getTotalPages(), list.getNumber(), BUTTONS_TO_SHOW);
		model.addAttribute("serialid", serialid);
		model.addAttribute("list", list);
		model.addAttribute("selectedPageSize", evalPageSize);
		model.addAttribute("pageSizes", PAGE_SIZES);
		model.addAttribute("pager", pager);
		model.addAttribute("totalRecords", list.getTotalElements());
		model.addAttribute("username", username.isPresent() ? username.get() : "");
		model.addAttribute("operation", operation.isPresent() ? operation.get() : "");
		model.addAttribute("message", message.isPresent() ? message.get() : "");
		model.addAttribute("company", company.isPresent() ? company.get() : "");
		model.addAttribute("profitcenter", profitcenter.isPresent() ? profitcenter.get() : "");
		model.addAttribute("auditId", auditId.isPresent() ? auditId.get() : "");
		model.addAttribute("createdon", createdon.isPresent() ? createdon.get() : "");
		model.addAttribute("sortingorder", sortingorder.isPresent() ? sortingorder.get() : "DESC");
		model.addAttribute("sortingkey", sortingkey.isPresent() ? sortingkey.get() : "id");
		return "audit/audittrail :: result-table";
	}

	/**
	 * 
	 * @param username
	 * @return
	 */
	private Set<String> getPCHeadsBasedOnUserLogin(String username) {
		Set<String> temp = new HashSet<>();
		temp.addAll(profitcenterService.getPCHeadsBasedOnUserLogin("%" + username + "%"));
		return temp;
	}

	/**
	 * 
	 * @return
	 */
	@GetMapping("/audit/logs")
	public ModelAndView logsPage() {
		ModelAndView modelAndView = new ModelAndView("/audit/logs");
		AccessLogsSetting templist = logsettingrepo.getLastStagingLog();
		LogSettingForm obj = new LogSettingForm();
		if (templist != null) {
			obj.setLogSetting(templist.getSetting());
			obj.setLogSettingId(templist.getId());
		}
		modelAndView.getModelMap().addAttribute("auditobj", obj);
		return modelAndView;
	}

	@PostMapping(value = "/audit/updatesetting")
	public String updateSetting(@Valid @ModelAttribute("auditobj") LogSettingForm obj, BindingResult result,
			Model model, RedirectAttributes redirectAttributes) {
		MDC.put("sessionId",
				AbstractMainController.getCompany().isPresent() ? AbstractMainController.getCompany().get() : "");
		if (!result.hasErrors()) {
			List<AccessLogsSetting> templist = logsettingrepo.getAllStagingLog();
			AccessLogsSetting tobj;
			if (templist != null && !templist.isEmpty()) {
				tobj = templist.get(0);
			} else {
				tobj = new AccessLogsSetting();
			}
			tobj.setSetting(obj.getLogSetting());
			try {
				logsettingrepo.createAccessLogSetting(tobj);
				auditService.putAudit(new Date(), "Log Setting: " + obj.getLogSetting() + " updated successfully",
						getCurrentUser().get(), "Update Log Settings");
				redirectAttributes.addFlashAttribute("info", "Log Settings updated successfully");
			} catch (POException e) {
				LOGGER.error(e);
			} catch (Exception e) {
				LOGGER.error(e);
			}
		}
		return "redirect:/audit/logs";
	}

	@RequestMapping(value = "/audit/loggrid", method = RequestMethod.GET)
	public String logsGrid(@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page, @RequestParam("classFileName") Optional<String> classFileName,
			@RequestParam("lineNo") Optional<Integer> lineNo, @RequestParam("id") Optional<Integer> id,
			@RequestParam("level") Optional<String> level, @RequestParam("message") Optional<String> message,
			@RequestParam("company") Optional<String> company, @RequestParam("screenId") Optional<String> screenId,
			@RequestParam("logTime") Optional<String> logTime, @RequestParam("sortingkey") Optional<String> sortingkey,
			@RequestParam("sortingorder") Optional<String> sortingorder, Model model) {

		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
		Page<AccessLogs> list;

		String date_format = "dd-MM-yyyy hh:mm:ss";
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_TIME_FORMAT");
		if (dateObj != null) {
			if (!dateObj.getPropValue().isEmpty()) {
				date_format = dateObj.getPropValue();
			}
		}
		Specification<AccessLogs> s = new Specification<AccessLogs>() {
			@Override
			public Predicate toPredicate(Root<AccessLogs> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> ls = new ArrayList<>();
				if (classFileName.isPresent() && !classFileName.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("classFileName")), "%" + classFileName.get().toLowerCase() + "%"));
				}
				if (lineNo.isPresent()) {

					ls.add(cb.equal(root.get("lineNo"), lineNo.get()));
				}
				if (id.isPresent()) {
					ls.add(cb.equal(root.get("id"), id.get()));
				}
				if (level.isPresent() && !level.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("level")), "%" + level.get().toLowerCase() + "%"));
				}

				if (message.isPresent() && !message.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("message")), "%" + message.get().toLowerCase() + "%"));
				}
				if (screenId.isPresent() && !screenId.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("screenId")), "%" + screenId.get().toLowerCase() + "%"));
				}
				if (company.isPresent() && !company.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("company")), "%" + company.get().toLowerCase() + "%"));
				}
				ls.add(cb.equal(root.get("company"), getCompany().isPresent() ? getCompany().get() : null));
				if (logTime.isPresent() && !logTime.get().isEmpty()) {

					Expression<String> dateStringExpr = cb.function("to_char", String.class, root.get("time"),
							cb.literal("YYYY-MM-DD HH12:MI:SS"));
					ls.add(cb.like(cb.lower(dateStringExpr), "%" + logTime.get().toLowerCase() + "%"));

				}

				return cb.and(ls.toArray(new Predicate[0]));
			}
		};
		Sort sorttemp = new Sort(Sort.Direction.DESC, "id");
		if (sortingkey.isPresent() && sortingorder.isPresent()) {
			sorttemp = new Sort(sortingorder.get().equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC,
					sortingkey.get());
		}
		if (classFileName.isPresent() || lineNo.isPresent() || id.isPresent() || level.isPresent()
				|| message.isPresent() || screenId.isPresent() || logTime.isPresent()) {
			list = logrepo.getAllPageableAccessLog(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		} else {
			list = logrepo.getAllPageableLogOrdered(new PageRequest(evalPage, evalPageSize, sorttemp));
		}

		int serialid = 0;

		int pagesize = evalPage + 1;

		if (pagesize > 1) {
			int start = evalPageSize * pagesize;
			serialid = start - evalPageSize;
		}
		List<AccessLogs> totalRecords = logrepo.getAllAccessLog();
		int size = totalRecords.size();
		Pager pager = new Pager(list.getTotalPages(), list.getNumber(), BUTTONS_TO_SHOW);
		model.addAttribute("serialid", serialid);
		model.addAttribute("list", list);
		model.addAttribute("totalRecords", size);
		model.addAttribute("selectedPageSize", evalPageSize);
		model.addAttribute("pageSizes", PAGE_SIZES);
		model.addAttribute("pager", pager);
		model.addAttribute("date_format", date_format);
		model.addAttribute("classFileName", classFileName.isPresent() ? classFileName.get() : "");
		model.addAttribute("lineNo", lineNo.isPresent() ? lineNo.get() : "");
		model.addAttribute("id", id.isPresent() ? id.get() : "");
		model.addAttribute("level", level.isPresent() ? level.get() : "");
		model.addAttribute("message", message.isPresent() ? message.get() : "");
		model.addAttribute("screenId", screenId.isPresent() ? screenId.get() : "");
		model.addAttribute("logTime", logTime.isPresent() ? logTime.get() : "");
		model.addAttribute("company", company.isPresent() ? company.get() : "");
		model.addAttribute("sortingorder", sortingorder.isPresent() ? sortingorder.get() : "DESC");
		model.addAttribute("sortingkey", sortingkey.isPresent() ? sortingkey.get() : "id");
		AccessLogsSetting templist = logsettingrepo.getLastStagingLog();
		LogSettingForm obj = new LogSettingForm();
		if (templist != null) {
			obj.setLogSetting(templist.getSetting());
			obj.setLogSettingId(templist.getId());
		}

		return "audit/logs :: result-table";
	}

	@Override
	protected String getHeaderTitle() {
		return "Manage Audits";
	}
}
