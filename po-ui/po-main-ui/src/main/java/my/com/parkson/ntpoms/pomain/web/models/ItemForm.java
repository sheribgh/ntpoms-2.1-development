package my.com.parkson.ntpoms.pomain.web.models;

import java.io.Serializable;

public class ItemForm implements Serializable{
	
	private int code;

	private int itemOrder;
	
	private String mappingId;
	
	private String profitCenter;
	
	private String itemNo;
	
	private String particulars;
	
	private String packing;
	
	private Double quantity;
	
	private String taxCode;
	
	private Double taxRate;
	
	private Double variance;
	
	private Double discount;
	
	private Double unitPriceExclGST;
	
	private Double unitPriceInclGST;
	
	private Double taxAmount;
	
	private String vendorcode;
	
	private String vendorbranchcode;
	
	private String poNo;
	
	private boolean oldlst;
	
	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getVendorcode() {
		return vendorcode;
	}

	public void setVendorcode(String vendorcode) {
		this.vendorcode = vendorcode;
	}

	public String getVendorbranchcode() {
		return vendorbranchcode;
	}

	public void setVendorbranchcode(String vendorbranchcode) {
		this.vendorbranchcode = vendorbranchcode;
	}

	public String getPoNo() {
		return poNo;
	}

	public void setPoNo(String poNo) {
		this.poNo = poNo;
	}

	public int getItemOrder() {
		return itemOrder;
	}

	public void setItemOrder(int itemOrder) {
		this.itemOrder = itemOrder;
	}

	public String getMappingId() {
		return mappingId;
	}

	public void setMappingId(String mappingId) {
		this.mappingId = mappingId;
	}

	public String getProfitCenter() {
		return profitCenter;
	}

	public void setProfitCenter(String profitCenter) {
		this.profitCenter = profitCenter;
	}

	public String getItemNo() {
		return itemNo;
	}

	public void setItemNo(String itemNo) {
		this.itemNo = itemNo;
	}

	public String getParticulars() {
		return particulars;
	}

	public void setParticulars(String particulars) {
		this.particulars = particulars;
	}

	public String getPacking() {
		return packing;
	}

	public void setPacking(String packing) {
		this.packing = packing;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public String getTaxCode() {
		return taxCode;
	}

	public void setTaxCode(String taxCode) {
		this.taxCode = taxCode;
	}

	public Double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}

	public Double getVariance() {
		return variance;
	}

	public void setVariance(Double variance) {
		this.variance = variance;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public Double getUnitPriceExclGST() {
		return unitPriceExclGST;
	}

	public void setUnitPriceExclGST(Double unitPriceExclGST) {
		this.unitPriceExclGST = unitPriceExclGST;
	}

	public Double getUnitPriceInclGST() {
		return unitPriceInclGST;
	}

	public void setUnitPriceInclGST(Double unitPriceInclGST) {
		this.unitPriceInclGST = unitPriceInclGST;
	}

	public Double getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public boolean isOldlst() {
		return oldlst;
	}

	public void setOldlst(boolean oldlst) {
		this.oldlst = oldlst;
	}
	

}
