package my.com.parkson.ntpoms.pomain.web.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.main.entities.ProfitCenterLastPONumber;
import my.com.parkson.ntpoms.main.services.PcLastPONumberService;
import my.com.parkson.ntpoms.pomain.web.models.Pager;

@Slf4j
@Controller
@RequiredArgsConstructor
public class PCLastPONumberController {
	
	private static final int BUTTONS_TO_SHOW = 5;
	private static final int INITIAL_PAGE = 0;
	private static final int INITIAL_PAGE_SIZE = 5;
	private static final int[] PAGE_SIZES = { 5, 10, 20 };
	
	
	@Autowired
	private PcLastPONumberService pcService;
	
	@GetMapping("/main/pclastponumber")
	public ModelAndView onPageLoadCrTerms(){
		ModelAndView modelAndView = new ModelAndView("/main/pclastponumber");
		return modelAndView;
	}
	
	
	
	@RequestMapping(value = "/main/pclastpogrid", method = RequestMethod.GET)
	public String crtGrid(@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page, @RequestParam("pcCode") Optional<String> pcCode,
			@RequestParam("lastPoNumber") Optional<String> lastPoNumber,
			@RequestParam("runningNumber") Optional<Integer> runningNumber, 
			@RequestParam("year") Optional<String> year
			,@RequestParam("sortingkey") Optional<String> sortingkey,
			@RequestParam("sortingorder") Optional<String> sortingorder,
			Model model) {
		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
		Page<ProfitCenterLastPONumber> list;
		Specification<ProfitCenterLastPONumber> s = new Specification<ProfitCenterLastPONumber>() {
			@Override
			public Predicate toPredicate(Root<ProfitCenterLastPONumber> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> ls = new ArrayList<>();
				if (pcCode.isPresent() && !pcCode.get().isEmpty()) {
					
					ls.add(cb.like(cb.lower(root.get("pcCode")), "%" + pcCode.get().toLowerCase() + "%"));
				}

				if (lastPoNumber.isPresent() && !lastPoNumber.get().isEmpty()) {
					ls.add(cb.equal(root.get("lastPoNumber"), "%" + lastPoNumber.get().toLowerCase() + "%"));
				}
				if (runningNumber.isPresent()) {
					ls.add(cb.equal(root.get("runningNumber"), runningNumber.get()));
				}
				
				
				if (year.isPresent() && !year.get().isEmpty()) {
					ls.add(cb.equal(root.get("year"), "%" + year.get().toLowerCase() + "%"));
				}

				return cb.and(ls.toArray(new Predicate[0]));
			}
		};
		
		
		Sort sorttemp = new Sort(Sort.Direction.DESC, "year");
		
		if (sortingkey.isPresent() && sortingorder.isPresent()) {
			sorttemp = new Sort(sortingorder.get().equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC,
					sortingkey.get());
		}
		if (pcCode.isPresent() || lastPoNumber.isPresent() || runningNumber.isPresent() || year.isPresent()) {
			list = pcService.getAllPagablePc(s, new PageRequest(evalPage, evalPageSize,sorttemp));
		} else {
			list = pcService.getAllPagablePc(new PageRequest(evalPage, evalPageSize,sorttemp));
		}
		int serialid = 0;
		int pagesize = evalPage + 1;
		if (pagesize > 1) {
			int start = evalPageSize * pagesize;
			serialid = start - evalPageSize;
		}
		Pager pager = new Pager(list.getTotalPages(), list.getNumber(), BUTTONS_TO_SHOW);
		model.addAttribute("serialid", serialid);
		model.addAttribute("list", list);
		model.addAttribute("selectedPageSize", evalPageSize);
		model.addAttribute("pageSizes", PAGE_SIZES);
		model.addAttribute("pager", pager);
		model.addAttribute("totalRecords", list.getTotalElements());
		model.addAttribute("pcCode", pcCode.isPresent() ? pcCode.get() : "");
		model.addAttribute("runningNumber", runningNumber.isPresent() ? runningNumber.get() : "");
		model.addAttribute("lastPoNumber", lastPoNumber.isPresent() ? lastPoNumber.get() : "");
		model.addAttribute("year", year.isPresent() ? year.get() : "");
		model.addAttribute("sortingorder", sortingorder.isPresent() ? sortingorder.get() : "DESC");
		model.addAttribute("sortingkey", sortingkey.isPresent() ? sortingkey.get() : "year");
		
		return  "/main/pclastponumber :: result-table";
	}
	

}