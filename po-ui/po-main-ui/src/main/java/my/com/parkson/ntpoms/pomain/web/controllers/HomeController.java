/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.pomain.web.controllers;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.common.entities.Company;
import my.com.parkson.ntpoms.common.entities.LoginList;
import my.com.parkson.ntpoms.common.entities.UserRole;
import my.com.parkson.ntpoms.common.services.CompanyService;
import my.com.parkson.ntpoms.common.services.LogInService;
import my.com.parkson.ntpoms.pomain.web.utils.CreatePdf;

@Slf4j
@Controller
@RequiredArgsConstructor
public class HomeController extends AbstractMainController {
	private static final String HOME = "home";

	@Autowired
	private CompanyService comrepo;

	@Autowired
	private LogInService loginrepo;

	/**
	 * Home.
	 *
	 * @param model
	 *            the model
	 * @return the string
	 */
	@GetMapping("/home")
	public String home(Model model) {
		System.out.println(getCurrentUser());
		System.out.println(getCompany());
		System.out.println(getYear());
		System.out.println(getRoleListCurrentUser());
		model.addAttribute("userid", getCurrentUser().get());
		return HOME;
	}

	@GetMapping("/login")
	public String login(Model model) {
		
		List<Company> companylist = comrepo.getAllCompany();
		List<String> company = new ArrayList<>();
		List<Integer> year = new ArrayList<>();
		if (companylist != null) {
			for (Company c : companylist) {
				company.add(c.getCode());
			}
		}
		/* year.add(2018); */
		model.addAttribute("year", year);
		model.addAttribute("company", companylist);
		return "/public/login";
	}

	@GetMapping("/loginredirect")
	public String loginredirect(Model model) {
		System.out.println("98888888888");
		List<String> rolelist = getRoleListCurrentUser();
		if (rolelist.contains("ROLE_POADMIN")) {
			return "redirect:/main/dashboard";
		} else if (rolelist.contains("ROLE_POUSER")) {
			// return "redirect:/main/createpo_menu";
			return "redirect:/main/po_dashboard";
		} else {
			return "redirect:/home";
		}

	}

	@RequestMapping(value = "/api/selectedcompany", method = RequestMethod.GET)
	public @ResponseBody List<String> get_Year(@RequestParam("companycode") String companycode) {
		List<LoginList> login_list = loginrepo.getYearByCompanyCode(companycode);
		List<String> year = new ArrayList<>();
		for (LoginList a : login_list) {
			year.add(a.getYear());

		}
		return year;

	};

	@Override
	protected String getHeaderTitle() {
		return "Manage Home";
	}
}