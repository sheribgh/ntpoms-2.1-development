package my.com.parkson.ntpoms.pomain.web.models;

import java.io.Serializable;
import java.util.Date;

import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.DateBridge;
import org.hibernate.search.annotations.EncodingType;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.NumericField;
import org.hibernate.search.annotations.Resolution;
import org.hibernate.search.annotations.Store;

public class PoArchiveForm implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String poNo;

	private String compCode;

	private String vendorCode;

	private String pcCode;

	private String  vendorName;

	private String  issuer;

	private String  issuerName;

	private double  totalAmt;

	private String  archivedTable;

	private String  users;

	private String  archivedYear;

	private String  createdOn;
	
	private int fetchResults;
	
	
	/***
	 * 
	 * Getters and Setters
	 * 
	 * ***/
	
	public int getFetchResults() {
		return fetchResults;
	}


	public void setFetchResults(int fetchResults) {
		this.fetchResults = fetchResults;
	}
	
	public String getPoNo() {		
		return poNo;
	} 
	
	public void setPoNo(String poNo) {
		this.poNo = poNo;		
	}

	public String getCompCode() {
		return compCode;
	}

	public void setCompCode(String compCode) {
		this.compCode = compCode;
	}

	public String getVendorCode() {
		return vendorCode;
	}

	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}

	public String getPcCode() {
		return pcCode;
	}

	public void setPcCode(String pcCode) {
		this.pcCode = pcCode;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public String getIssuer() {
		return issuer;
	}

	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}

	public String getIssuerName() {
		return issuerName;
	}

	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}

	public double getTotalAmt() {
		return totalAmt;
	}

	public void setTotalAmt(double totalAmt) {
		this.totalAmt = totalAmt;
	}

	public String getArchivedTable() {
		return archivedTable;
	}

	public void setArchivedTable(String archivedTable) {
		this.archivedTable = archivedTable;
	}

	public String getUsers() {
		return users;
	}

	public void setUsers(String users) {
		this.users = users;
	}

	public String getArchivedYear() {
		return archivedYear;
	}

	public void setArchivedYear(String archivedYear) {
		this.archivedYear = archivedYear;
	}


	public String getCreatedOn() {
		return createdOn;
	}


	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	
	
	
}
