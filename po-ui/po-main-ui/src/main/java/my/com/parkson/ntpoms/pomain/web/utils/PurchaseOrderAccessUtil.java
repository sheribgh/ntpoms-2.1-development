package my.com.parkson.ntpoms.pomain.web.utils;

import java.util.Calendar;

import my.com.parkson.ntpoms.common.entities.DataSource;
import my.com.parkson.ntpoms.common.services.DataSourceService;
import my.com.parkson.ntpoms.common.services.SecurityService;
import my.com.parkson.ntpoms.main.entities.PurchaseOrder;

public class PurchaseOrderAccessUtil {
	
	private static final String DRAFT = "DRAFT";	
	private static final String ISSUED = "ISSUED";	
	private static final String AUTHORIZED = "AUTHORIZED";	
	private static final String VERIFIED = "VERIFIED";
	
	private boolean isIssuer = false;	
	private boolean isVerifier = false;	
	private boolean isAuthorizer = false;	
	private boolean isSuperadmin = false;	
	private boolean isAdmin = false;
	
	private String status = "";
	private String currentUser;
	private String compCode;
	
	private PurchaseOrder purchaseOrder;
	
	private SecurityService securityService;	
	private DataSourceService dataSourceService;
	
	public PurchaseOrderAccessUtil(PurchaseOrder purchaseOrder) {
		if(purchaseOrder != null) {
			this.purchaseOrder = purchaseOrder;
			if (this.purchaseOrder.getIssuer() != null && this.purchaseOrder.getIssuer().equalsIgnoreCase(currentUser)) {				
				isIssuer = true;
			}			
			if (this.purchaseOrder.getCompCode() != null) {				
				this.compCode = purchaseOrder.getCompCode();
			}
			if (this.purchaseOrder.getVerifier() != null && this.purchaseOrder.getVerifier().equalsIgnoreCase(currentUser)) {				
				isVerifier = true;
			}			
			if (this.purchaseOrder.getAuthorizer() != null && this.purchaseOrder.getAuthorizer().equalsIgnoreCase(currentUser)) {				
				isAuthorizer = true;
			}			
			if (this.purchaseOrder.getStatus() != null) {				
				status = purchaseOrder.getStatus();
			}
		}		
		if (currentUser.contains("superadmin")) {
			isSuperadmin = true;
		}		
		if (securityService.getUserByUsername(currentUser) != null) {
			isAdmin = true;
		}
	}
	
	public boolean canCreate() {		
		return true;
	}
	
	public boolean canDuplicate () {		
		boolean canDuplicate = true;		
		DataSource database = dataSourceService.getDataSourceByCompany(this.compCode);
		int databaseYear = Integer.parseInt(database.getYear());
		int currentYear = Calendar.getInstance().get(Calendar.YEAR);		
		if (databaseYear < currentYear) {			
			canDuplicate = false;
		}		
		return canDuplicate;
	}
	
	public boolean canEdit() {		
		boolean canEdit = false;		
		if ((isIssuer || isSuperadmin) && status.contains(DRAFT)) {			
			canEdit = true;
		}		
		return canEdit;
	}
	
	public boolean canSubmit() {		
		boolean canSubmit = false;		
		if ((isIssuer || isSuperadmin) && status.contains(DRAFT)) {			
			canSubmit = true;
		}		
		return canSubmit;
	}
	
	public boolean canVerify() {		
		boolean canVerify = false;		
		if ((isVerifier || isSuperadmin) && status.equalsIgnoreCase(ISSUED)) {			
			canVerify = true;
		}		
		return canVerify;
	}
	
	public boolean canAuthorize() {		
		boolean canAuthorize = false;		
		if ((isAuthorizer || isSuperadmin) && status.equalsIgnoreCase(VERIFIED)) {			
			canAuthorize = true;
		}		
		return canAuthorize;
	}
	
	public boolean canInvoice() {		
		boolean canInvoice = false;		
		if ((isIssuer || isVerifier || isAuthorizer || isSuperadmin) && status.equalsIgnoreCase(AUTHORIZED)) {			
			canInvoice = true;
		}		
		return canInvoice;
	}
	
	public boolean canClose() {		
		boolean canClose = false;		
		if ((isIssuer || isVerifier || isAuthorizer || isSuperadmin || isAdmin) && status.equalsIgnoreCase("INVOICED")) {			
			canClose = true;
		}		
		return canClose;
	}
	
	public boolean canForwardVerifier() {		
		boolean canForwardVerifier = false;		
		if ((isIssuer || isVerifier || isSuperadmin || isAdmin) && status.equalsIgnoreCase(ISSUED)) {			
			canForwardVerifier = true;
		}		
		return canForwardVerifier;
	}
	
	public boolean canForwardAuthorizer() {		
		boolean canForwardAuthorizer = false;		
		if ((isIssuer || isAuthorizer || isSuperadmin || isAdmin) && status.equalsIgnoreCase(VERIFIED)) {			
			canForwardAuthorizer = true;
		}		
		return canForwardAuthorizer;
	}
	
	public boolean canForwardInvoicer() {		
		boolean canForwardInvoiceRecipient = false;		
		if ((isIssuer || isVerifier || isAuthorizer || isSuperadmin || isAdmin) && status.equalsIgnoreCase(AUTHORIZED)) {			
			canForwardInvoiceRecipient = true;
		}		
		return canForwardInvoiceRecipient;
	}
	
	public boolean canRedraft() {		
		boolean canRedraft = false;		
		if (((isVerifier || isSuperadmin) && status.equalsIgnoreCase(ISSUED))
			|| ((isAuthorizer || isSuperadmin) && status.equalsIgnoreCase(VERIFIED))) {			
			canRedraft = true;
		}
		return canRedraft;
	}
	
	public boolean canReject() {		
		boolean canReject = false;		
		if (((isVerifier || isSuperadmin) && status.equalsIgnoreCase(ISSUED))
			|| ((isAuthorizer || isSuperadmin) && status.equalsIgnoreCase(VERIFIED))) {			
			canReject = true;
		}		
		return canReject;
	}
	
	public boolean canCancel() {		
		boolean canCancel = false;		
		if ((isIssuer || isVerifier || isAuthorizer || isSuperadmin) && status.equalsIgnoreCase(AUTHORIZED)) {			
			canCancel = true;
		}		
		return canCancel;
	}
	
	public boolean canDelete() {		
		boolean canDelete = false;		
		if (((isIssuer || isSuperadmin || isAdmin) && status.contains(DRAFT))
			|| ((isIssuer || isSuperadmin || isAdmin) && status.equalsIgnoreCase(ISSUED))
			|| ((isIssuer || isSuperadmin || isAdmin) && status.equalsIgnoreCase(VERIFIED))
			|| ((isSuperadmin || isAdmin) && status.equalsIgnoreCase(AUTHORIZED))) {			
			canDelete = true;
		}		
		return canDelete;
	}
	
	public boolean canReopen() {		
		boolean canReopen = false;		
		if((isSuperadmin || isAdmin) && (status.contains("Closed"))) {			
			canReopen = true;
		}		
		return canReopen;		
	}
}