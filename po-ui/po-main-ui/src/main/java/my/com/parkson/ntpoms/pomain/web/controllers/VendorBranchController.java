package my.com.parkson.ntpoms.pomain.web.controllers;

import static my.com.parkson.ntpoms.common.MessageCodes.FAILED_FETCH_RECORDS_EXCEPTION;
import static my.com.parkson.ntpoms.common.MessageCodes.FAILED_PERSIST_EXCEPTION;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.LogMessages;
import my.com.parkson.ntpoms.common.entities.SystemParameter;
import my.com.parkson.ntpoms.common.model.Pager;

import my.com.parkson.ntpoms.common.services.SystemParameterService;
import my.com.parkson.ntpoms.main.entities.VendorBranch;
import my.com.parkson.ntpoms.main.entities.VendorBranchId;
import my.com.parkson.ntpoms.common.services.MessageService;
import my.com.parkson.ntpoms.main.services.AccessLogsService;
import my.com.parkson.ntpoms.main.services.AuditsService;
import my.com.parkson.ntpoms.main.services.MessagesService;
import my.com.parkson.ntpoms.main.services.VendorBranchService;
import my.com.parkson.ntpoms.pomain.web.models.VendorBranchForm;

@Slf4j
@Controller
@RequiredArgsConstructor
public class VendorBranchController extends AbstractMainController {

	private static final int BUTTONS_TO_SHOW = 5;
	private static final int INITIAL_PAGE = 0;
	private static final int INITIAL_PAGE_SIZE = 5;
	private static final int[] PAGE_SIZES = { 5, 10, 20 };

	private static final Logger LOGGER = Logger.getLogger(VendorBranchController.class);

	/** The messages. */
	@Autowired
	private MessagesService messages;

	@Autowired
	private AuditsService auditService;

	@Autowired
	private VendorBranchService vbService;

	@Autowired
	private SystemParameterService sysRepo;

	private VendorBranchForm tempobj;

	boolean edit = false;

	/**
	 * On page load vendorbranch.
	 * 
	 * @return the model and view
	 */

	@GetMapping("/main/vendorbranch")
	public ModelAndView onPageLoadVendor() {

		ModelAndView modelAndView = new ModelAndView("/main/vendorbranch");
		modelAndView.getModelMap().addAttribute("vbranchobj", tempobj != null ? tempobj : new VendorBranchForm());
		return modelAndView;

	}

	@RequestMapping(value = "/main/vendorbranchgrid", method = RequestMethod.GET)
	public String vendorBranchGrid(@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page, @RequestParam("code") Optional<String> code,
			@RequestParam("name") Optional<String> name, @RequestParam("status") Optional<String> isActive,
			@RequestParam("branchCode") Optional<String> branchCode,
			@RequestParam("sortingkey") Optional<String> sortingkey,
			@RequestParam("sortingorder") Optional<String> sortingorder, Model model) {

		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
		Page<VendorBranch> list;
		Specification<VendorBranch> s = new Specification<VendorBranch>() {
			@Override
			public Predicate toPredicate(Root<VendorBranch> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> ls = new ArrayList<>();
				if (code.isPresent() && !code.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("vendorBranchId").get("code")),
							"%" + code.get().toLowerCase() + "%"));
				}

				if (branchCode.isPresent() && !branchCode.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("vendorBranchId").get("branchCode")),
							"%" + branchCode.get().toLowerCase() + "%"));
				}

				if (name.isPresent() && !name.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("name")), "%" + name.get().toLowerCase() + "%"));
				}
				if (isActive.isPresent() && !isActive.get().isEmpty() && !isActive.get().equals("All")) {
					ls.add(cb.equal(root.get("isActive"), "Active".equals(isActive.get())));
				}

				return cb.and(ls.toArray(new Predicate[0]));
			}
		};

		Sort sorttemp = new Sort(Sort.Direction.DESC, "lastModifiedOn");

		if (sortingkey.isPresent() && sortingorder.isPresent()) {
			sorttemp = new Sort(sortingorder.get().equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC,
					sortingkey.get());
		}
		if (code.isPresent() || name.isPresent() || isActive.isPresent() || branchCode.isPresent()) {
			list = vbService.getAllPageableVendorBranch(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		} else {
			list = vbService.getAllPageableVendorBranchOrdered(new PageRequest(evalPage, evalPageSize, sorttemp));
		}
		int serialid = 0;
		int pagesize = evalPage + 1;
		if (pagesize > 1) {
			int start = evalPageSize * pagesize;
			serialid = start - evalPageSize;
		}
		Pager pager = new Pager(list.getTotalPages(), list.getNumber(), BUTTONS_TO_SHOW);
		model.addAttribute("serialid", serialid);
		model.addAttribute("list", list);
		model.addAttribute("selectedPageSize", evalPageSize);
		model.addAttribute("pageSizes", PAGE_SIZES);
		model.addAttribute("pager", pager);
		model.addAttribute("totalRecords", list.getTotalElements());
		model.addAttribute("code", code.isPresent() ? code.get() : "");
		model.addAttribute("name", name.isPresent() ? name.get() : "");
		model.addAttribute("branchCode", branchCode.isPresent() ? branchCode.get() : "");
		model.addAttribute("status", isActive.isPresent() ? isActive.get() : "");
		model.addAttribute("sortingorder", sortingorder.isPresent() ? sortingorder.get() : "DESC");
		model.addAttribute("sortingkey", sortingkey.isPresent() ? sortingkey.get() : "lastModifiedOn");

		return "/main/vendorbranch :: result-table";

	}

	/**
	 * Creates the vendor branch.
	 *
	 * @param obj
	 *            the obj
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @param redirectAttributes
	 *            the redirect attributes
	 * @return the string
	 */
	@PostMapping("/main/createvendorbranch")
	public String createVendor(@Valid @ModelAttribute("vbranchobj") VendorBranchForm obj, BindingResult result,
			Model model, RedirectAttributes redirectAttributes) {
		VendorBranchId pkey = new VendorBranchId(obj.getVendorCode(), obj.getVendorBranchCode(), getCompany().get());
		MDC.put("sessionId",
				AbstractMainController.getCompany().isPresent() ? AbstractMainController.getCompany().get() : "");
		if (!result.hasErrors() && obj.getVendorCode() != null && obj.getVendorBranchCode() != null
				&& vbService.getVendorBranchbyVendorId(pkey) == null) {

			VendorBranch vendorBranch = new VendorBranch();
			// VendorBranchId vbId = new
			// VendorBranchId(obj.getVendorCode(),obj.getVendorBranchCode() ,
			// getCompany().get());

			vendorBranch.setCreatedBy(getCurrentUser().get());
			vendorBranch.setCreatedOn(new Date());
			vendorBranch.setLastModifiedBy(getCurrentUser().get());
			vendorBranch.setLastModifiedOn(new Date());
			vendorBranch.setActive(true);

			vendorBranch.setVendorBranchId(pkey);
			vendorBranch.setName(obj.getVendorName());
			vendorBranch.setBranchDescription(obj.getBranchDescription());
			vendorBranch.setAddressLine1(obj.getVendorAddressLine1());
			vendorBranch.setAddressLine2(obj.getVendorAddressLine2());
			vendorBranch.setAddressLine3(obj.getVendorAddressLine3());
			vendorBranch.setPostCode(obj.getPostCode());
			vendorBranch.setPostCodeArea(obj.getPostCodeArea());
			vendorBranch.setLastContactPersonName(obj.getLastContactPersonName());
			vendorBranch.setLastContactPersonContactNumber(obj.getLastContactPersonContactNumber());
			vendorBranch.setLastContactPersonEmailAddress(obj.getLastContactPersonEmailAddress());
			vendorBranch.setLastPOCurrency(obj.getLastPOCurrency());
			vendorBranch.setCreditTermsCode(obj.getCredittermscode());

			// if(vbService.getVendorBranchbyVendorId(vendorBranch.getVendorBranchId())
			// == null){
			try {
				vbService.createVendorBranch(vendorBranch);
				auditService.createAudit(new Date(),
						" Vendor Branch: " + obj.getVendorName() + " created successfully. ", getCurrentUser().get(),
						" Create Vendor Branch", getCompany().get(), "");
				redirectAttributes.addFlashAttribute("info", "Vendor Branch created successfully");
				tempobj = null;

			} catch (Exception e) {
				LOGGER.error(e);
			}

		} else {
			redirectAttributes.addFlashAttribute("error", "Vendor Code - Branch Code record already exists.");
			tempobj = obj;
		}
		return "redirect:/main/vendorbranch";
	}

	/**
	 * Updates Vendor Branch.
	 *
	 * @param code
	 *            the code
	 * @param model
	 *            the model
	 * @return the string
	 */
	@PostMapping("/main/updatevendorbranch")
	public String updateVendor(@Valid @ModelAttribute("vbranchobj") VendorBranchForm obj, BindingResult result,
			Model model, RedirectAttributes redirectAttributes) {
		MDC.put("sessionId",
				AbstractMainController.getCompany().isPresent() ? AbstractMainController.getCompany().get() : "");
		VendorBranchId pkey = new VendorBranchId(obj.getVendorCode(), obj.getVendorBranchCode(), getCompany().get());
		if (!result.hasErrors()) {
			VendorBranch vendorBranch = vbService.getVendorBranchbyVendorId(pkey);
			boolean isActive;

			try {
				if (vendorBranch != null) {

					vendorBranch.setLastModifiedBy(getCurrentUser().get());
					vendorBranch.setLastModifiedOn(new Date());

					vendorBranch.setVendorBranchId(pkey);
					vendorBranch.setName(obj.getVendorName());
					vendorBranch.setBranchDescription(obj.getBranchDescription());
					vendorBranch.setAddressLine1(obj.getVendorAddressLine1());
					vendorBranch.setAddressLine2(obj.getVendorAddressLine2());
					vendorBranch.setAddressLine3(obj.getVendorAddressLine3());
					vendorBranch.setPostCode(obj.getPostCode());
					vendorBranch.setPostCodeArea(obj.getPostCodeArea());
					vendorBranch.setLastContactPersonName(obj.getLastContactPersonName());
					vendorBranch.setLastContactPersonContactNumber(obj.getLastContactPersonContactNumber());
					vendorBranch.setLastContactPersonEmailAddress(obj.getLastContactPersonEmailAddress());
					vendorBranch.setLastPOCurrency(obj.getLastPOCurrency());
					vendorBranch.setCreditTermsCode(obj.getCredittermscode());

					if (obj.getIsActive().contains("InActive")) {
						isActive = false;
					} else {
						isActive = true;
					}

					if (isActive != vendorBranch.isActive()) {

						if (obj.getIsActive().contains("InActive")) {
							vendorBranch.setDeactivatedBy(getCurrentUser().get());
							vendorBranch.setDeactivatedOn(new Date());
						} else {
							vendorBranch.setReactivatedOn(new Date());
							vendorBranch.setReactivatedBy(getCurrentUser().get());
						}
					}

					vendorBranch.setActive("Active".equals(obj.getIsActive()));

					try {
						vbService.createVendorBranch(vendorBranch);
						redirectAttributes.addFlashAttribute("info", "Vendor Branch updated successfully");
						auditService.createAudit(new Date(),
								"Vendor Branch: " + obj.getVendorName() + " updated successfully. ",
								getCurrentUser().get(), " Update Vendor Branch", getCompany().get(), "");

					} catch (Exception e) {
						LOGGER.error(e);
					}

				} else {
					redirectAttributes.addFlashAttribute("error", "Vendor Branch does not exist");
					return "/main/edit_VendorBranch";
				}
			} catch (Exception e) {
				LOGGER.error(e);
			}

		}
		return "redirect:/main/vendorbranch";
	}

	/**
	 * Edits the Vendor Branch.
	 *
	 * @param code
	 *            the code
	 * @param model
	 *            the model
	 * @return the string
	 */
	@GetMapping("/main/editvendorbranch")
	public String editVendor(@RequestParam("code") Optional<String> code,
			@RequestParam("vendorBranchCode") Optional<String> vendorBranchCode, Model model) {

		VendorBranchId pkey = new VendorBranchId(code.get(), vendorBranchCode.get(), getCompany().get());
		VendorBranch vendorBranch = vbService.getVendorBranchbyVendorId(pkey);
		edit = true;

		if (vendorBranch != null) {

			VendorBranchForm vendorForm = new VendorBranchForm();
			String date_format = "dd-MM-yyyy hh:mm:ss";
			SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_TIME_FORMAT");

			if (dateObj != null) {
				if (!dateObj.getPropValue().isEmpty()) {
					date_format = dateObj.getPropValue();
				}
			}

			DateFormat formatter = new SimpleDateFormat(date_format);
			String createdOn = "";
			String modifiedOn = "";
			String reactivatedOn = "";
			String deactivatedOn = "";

			if (vendorBranch.getCreatedOn() != null) {
				createdOn = formatter.format(vendorBranch.getCreatedOn());
			}
			if (vendorBranch.getLastModifiedOn() != null) {
				modifiedOn = formatter.format(vendorBranch.getLastModifiedOn());
			}
			if (vendorBranch.getReactivatedOn() != null) {
				reactivatedOn = formatter.format(vendorBranch.getReactivatedOn());
			}
			if (vendorBranch.getDeactivatedOn() != null) {
				deactivatedOn = formatter.format(vendorBranch.getDeactivatedOn());
			}

			vendorForm.setCreatedOn(createdOn);
			vendorForm.setDeactivatedOn(deactivatedOn);
			vendorForm.setReactivatedOn(reactivatedOn);
			vendorForm.setLastModifiedOn(modifiedOn);
			vendorForm.setIsActive(vendorBranch.isActive() ? "Active" : "InActive");
			vendorForm.setVendorCode(vendorBranch.getVendorBranchId().getCode());
			vendorForm.setVendorBranchCode(vendorBranch.getVendorBranchId().getBranchCode());
			vendorForm.setVendorName(vendorBranch.getName());
			vendorForm.setBranchDescription(vendorBranch.getBranchDescription());
			vendorForm.setVendorAddressLine1(vendorBranch.getAddressLine1());
			vendorForm.setVendorAddressLine2(vendorBranch.getAddressLine2());
			vendorForm.setVendorAddressLine3(vendorBranch.getAddressLine3());
			vendorForm.setPostCode(vendorBranch.getPostCode());
			vendorForm.setPostCodeArea(vendorBranch.getPostCodeArea());
			vendorForm.setLastContactPersonName(vendorBranch.getLastContactPersonName());
			vendorForm.setLastContactPersonContactNumber(vendorBranch.getLastContactPersonContactNumber());
			vendorForm.setLastContactPersonEmailAddress(vendorBranch.getLastContactPersonEmailAddress());
			vendorForm.setLastPOCurrency(vendorBranch.getLastPOCurrency());
			vendorForm.setCredittermscode(vendorBranch.getCreditTermsCode());
			vendorForm.setCreatedBy(vendorBranch.getCreatedBy());
			vendorForm.setLastModifiedBy(vendorBranch.getLastModifiedBy());
			vendorForm.setDeactivatedBy(vendorBranch.getDeactivatedBy());
			vendorForm.setReactivatedBy(vendorBranch.getReactivatedBy());
			model.addAttribute("vbranchobj", vendorForm);
			model.addAttribute("edit", edit);

			return "/main/edit_vendorbranch";
		} else {
			return "redirect:/main/vendorbranch";

		}
	}

	@RequestMapping(value = "/vendorbranch/existingvendor", method = RequestMethod.GET)
	public @ResponseBody VendorBranch checkVendorBranch(@RequestParam("vendorcode") Optional<String> code,
			@RequestParam("vendorbranchcode") Optional<String> vendorBranchCode, RedirectAttributes redirectAttributes,
			Model model) {

		VendorBranchId pkey = new VendorBranchId(code.get(), vendorBranchCode.get(), getCompany().get());

		VendorBranch vendorBranch = vbService.getVendorBranchbyVendorId(pkey);

		return vendorBranch;

	}

	@Override
	protected String getHeaderTitle() {
		// TODO Auto-generated method stub
		return "Manage Vendor Branch";
	}

}
