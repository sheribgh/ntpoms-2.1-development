package my.com.parkson.ntpoms.pomain.web.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;

public class VendorBranchForm implements Serializable {


	private static final long serialVersionUID = 1L;

	private String  vendorCode;

	private String  vendorBranchCode;

	private String  vendorName;

	private String  branchDescription;

	private String  vendorAddressLine1;

	private String  vendorAddressLine2;

	private String  vendorAddressLine3;

	private String  postCode;

	private String  postCodeArea;

	private String  lastContactPersonName;

	private String  lastContactPersonContactNumber;

	private String  lastContactPersonEmailAddress;

	private String  lastPOCurrency;

	private String  createdBy;

	private String    createdOn;

	private String  lastModifiedBy;

	private String    lastModifiedOn;

	private String  deactivatedBy;

	private String    deactivatedOn;

	private String  reactivatedBy;

	private String    reactivatedOn;

	private String isActive;

	private String credittermscode;

	private String compCode;

	public String getVendorCode() {
		return vendorCode;
	}

	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}

	public String getVendorBranchCode() {
		return vendorBranchCode;
	}

	public void setVendorBranchCode(String vendorBranchCode) {
		this.vendorBranchCode = vendorBranchCode;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public String getBranchDescription() {
		return branchDescription;
	}

	public void setBranchDescription(String branchDescription) {
		this.branchDescription = branchDescription;
	}

	public String getVendorAddressLine1() {
		return vendorAddressLine1;
	}

	public void setVendorAddressLine1(String vendorAddressLine1) {
		this.vendorAddressLine1 = vendorAddressLine1;
	}

	public String getVendorAddressLine2() {
		return vendorAddressLine2;
	}

	public void setVendorAddressLine2(String vendorAddressLine2) {
		this.vendorAddressLine2 = vendorAddressLine2;
	}

	public String getVendorAddressLine3() {
		return vendorAddressLine3;
	}

	public void setVendorAddressLine3(String vendorAddressLine3) {
		this.vendorAddressLine3 = vendorAddressLine3;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	public String getPostCodeArea() {
		return postCodeArea;
	}

	public void setPostCodeArea(String postCodeArea) {
		this.postCodeArea = postCodeArea;
	}

	public String getLastContactPersonName() {
		return lastContactPersonName;
	}

	public void setLastContactPersonName(String lastContactPersonName) {
		this.lastContactPersonName = lastContactPersonName;
	}

	public String getLastContactPersonContactNumber() {
		return lastContactPersonContactNumber;
	}

	public void setLastContactPersonContactNumber(String lastContactPersonContactNumber) {
		this.lastContactPersonContactNumber = lastContactPersonContactNumber;
	}

	public String getLastContactPersonEmailAddress() {
		return lastContactPersonEmailAddress;
	}

	public void setLastContactPersonEmailAddress(String lastContactPersonEmailAddress) {
		this.lastContactPersonEmailAddress = lastContactPersonEmailAddress;
	}

	public String getLastPOCurrency() {
		return lastPOCurrency;
	}

	public void setLastPOCurrency(String lastPOCurrency) {
		this.lastPOCurrency = lastPOCurrency;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public String getLastModifiedOn() {
		return lastModifiedOn;
	}

	public void setLastModifiedOn(String lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

	public String getDeactivatedBy() {
		return deactivatedBy;
	}

	public void setDeactivatedBy(String deactivatedBy) {
		this.deactivatedBy = deactivatedBy;
	}

	public String getDeactivatedOn() {
		return deactivatedOn;
	}

	public void setDeactivatedOn(String deactivatedOn) {
		this.deactivatedOn = deactivatedOn;
	}

	public String getReactivatedBy() {
		return reactivatedBy;
	}

	public void setReactivatedBy(String reactivatedBy) {
		this.reactivatedBy = reactivatedBy;
	}

	public String getReactivatedOn() {
		return reactivatedOn;
	}

	public void setReactivatedOn(String reactivatedOn) {
		this.reactivatedOn = reactivatedOn;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getCredittermscode() {
		return credittermscode;
	}

	public void setCredittermscode(String credittermscode) {
		this.credittermscode = credittermscode;
	}

	public String getCompCode() {
		return compCode;
	}

	public void setCompCode(String compCode) {
		this.compCode = compCode;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	



}
