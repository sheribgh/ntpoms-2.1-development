package my.com.parkson.ntpoms.pomain.web.models;

import java.io.Serializable;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class ProjectForm implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String projectCode;
	
	private String projectName;
	
	private String projectDescription;
		
	private String prHeadFrom;

	private String prHeadTo;

	private String prTeamFrom;
	
	private String prTeamTo;
	
	private String pcFrom;
	
	private String pcTo;

	private String pcCode;

	private String completionDate;
	
	private String createdBy;
	
	private String createdOn;
	
	private String lastModifiedBy;
	
	private String lastModifiedOn;
	
	private String deactivatedBy;
	
	private String deactivatedOn;
	
	private String reactivatedBy;
	
	private String reactivatedOn;
	
	private String isActive;


	public String getProjectCode() {
		return projectCode;
	}

	public void setProjectCode(String projectCode) {
		this.projectCode = projectCode;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getProjectDescription() {
		return projectDescription;
	}

	public void setProjectDescription(String projectDescription) {
		this.projectDescription = projectDescription;
	}

	
	public String getPcCode() {
		return pcCode;
	}

	public void setPcCode(String pcCode) {
		this.pcCode = pcCode;
	}

	

	public String getCompletionDate() {
		return completionDate;
	}

	public void setCompletionDate(String completionDate) {
		this.completionDate = completionDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn2) {
		this.createdOn = createdOn2;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public String getLastModifiedOn() {
		return lastModifiedOn;
	}

	public void setLastModifiedOn(String modifiedOn) {
		this.lastModifiedOn = modifiedOn;
	}

	public String getDeactivatedBy() {
		return deactivatedBy;
	}

	public void setDeactivatedBy(String deactivatedBy) {
		this.deactivatedBy = deactivatedBy;
	}

	public String getDeactivatedOn() {
		return deactivatedOn;
	}

	public void setDeactivatedOn(String deactivatedOn2) {
		this.deactivatedOn = deactivatedOn2;
	}

	public String getReactivatedBy() {
		return reactivatedBy;
	}

	public void setReactivatedBy(String reactivatedBy) {
		this.reactivatedBy = reactivatedBy;
	}

	public String getReactivatedOn() {
		return reactivatedOn;
	}

	public void setReactivatedOn(String reactivatedOn2) {
		this.reactivatedOn = reactivatedOn2;
	}

	public String isActive() {
		return isActive;
	}

	public void setActive(String isActive) {
		this.isActive = isActive;
	}
	
	

	public String getPrTeamTo() {
		return prTeamTo;
	}

	public void setPrTeamTo(String prTeamTo) {
		this.prTeamTo = prTeamTo;
	}


	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getPrHeadFrom() {
		return prHeadFrom;
	}

	public void setPrHeadFrom(String prHeadFrom) {
		this.prHeadFrom = prHeadFrom;
	}

	public String getPrHeadTo() {
		return prHeadTo;
	}

	public void setPrHeadTo(String prHeadTo) {
		this.prHeadTo = prHeadTo;
	}

	public String getPrTeamFrom() {
		return prTeamFrom;
	}

	public void setPrTeamFrom(String prTeamFrom) {
		this.prTeamFrom = prTeamFrom;
	}

	public String getPcFrom() {
		return pcFrom;
	}

	public void setPcFrom(String pcFrom) {
		this.pcFrom = pcFrom;
	}

	public String getPcTo() {
		return pcTo;
	}

	public void setPcTo(String pcTo) {
		this.pcTo = pcTo;
	}

	
}
