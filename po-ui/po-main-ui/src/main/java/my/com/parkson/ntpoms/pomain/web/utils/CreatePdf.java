/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.pomain.web.utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;

import com.itextpdf.barcodes.Barcode128;
import com.itextpdf.io.font.FontConstants;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfPage;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.canvas.PdfCanvas;
import com.itextpdf.kernel.pdf.canvas.PdfCanvasConstants;
import com.itextpdf.layout.Canvas;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.element.AreaBreak;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Tab;
import com.itextpdf.layout.element.TabStop;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.element.Text;
import com.itextpdf.layout.property.AreaBreakType;
import com.itextpdf.layout.property.HorizontalAlignment;
import com.itextpdf.layout.property.TabAlignment;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.property.VerticalAlignment;
import com.itextpdf.layout.renderer.CellRenderer;
import com.itextpdf.layout.renderer.DrawContext;

import my.com.parkson.ntpoms.common.entities.Company;
import my.com.parkson.ntpoms.common.services.AuditService;
import my.com.parkson.ntpoms.common.services.CompanyService;
import my.com.parkson.ntpoms.common.services.EmployeeMasterService;
import my.com.parkson.ntpoms.common.services.SystemParameterService;
import my.com.parkson.ntpoms.main.entities.ItemMap;
import my.com.parkson.ntpoms.main.entities.PurchaseOrder;
import my.com.parkson.ntpoms.main.services.ItemMapService;
import my.com.parkson.ntpoms.main.services.PurchaseOrderService;
import my.com.parkson.ntpoms.pomain.web.controllers.AbstractMainController;
import my.com.parkson.ntpoms.utils.ApplicationContextProvider;

public class CreatePdf extends AbstractMainController {
	private static final Logger LOGGER = Logger.getLogger(CreatePdf.class);

	private String termAndConditionsDestination = "";
	private String image = "";
	private int rowCount = 0;
	private PdfDocument pdfDocument;
	private PdfDocument termAndConditionsDocument;
	private Document document;
	private PurchaseOrder purchaseOrder = null;

	@Autowired
	AuditService auditService;

	@Autowired
	CompanyService companyService;

	@Autowired
	SystemParameterService systemParameterService;

	@Autowired
	EmployeeMasterService employeeMasterService;

	@Autowired
	PurchaseOrderService purchaseOrderService;

	@Autowired
	ItemMapService itemMapService;

	public ByteArrayOutputStream buildPdf(String poNum, ByteArrayOutputStream outputStream) throws java.io.IOException {
		MDC.put("sessionId",
				AbstractMainController.getCompany().isPresent() ? AbstractMainController.getCompany().get() : "");
		if (systemParameterService == null) {
			systemParameterService = ApplicationContextProvider.getContext().getBean(SystemParameterService.class);
		}

		if (auditService == null) {
			auditService = ApplicationContextProvider.getContext().getBean(AuditService.class);
		}

		if (companyService == null) {
			companyService = ApplicationContextProvider.getContext().getBean(CompanyService.class);
		}

		if (employeeMasterService == null) {
			employeeMasterService = ApplicationContextProvider.getContext().getBean(EmployeeMasterService.class);
		}

		if (itemMapService == null) {
			itemMapService = ApplicationContextProvider.getContext().getBean(ItemMapService.class);
		}

		if (purchaseOrderService == null) {
			purchaseOrderService = ApplicationContextProvider.getContext().getBean(PurchaseOrderService.class);
		}
		purchaseOrder = purchaseOrderService.getPurchaseOrderByPoNo(poNum);
		if (purchaseOrder == null) {
			LOGGER.info("Unable to get po specified in the request");
			return outputStream;
		}
		Company companyDetails = companyService.getCompanyByCode(purchaseOrder.getCompCode());
		if (companyDetails != null) {
			termAndConditionsDestination = companyDetails.getPoTermsAndConditionsLandscape();
			image = companyDetails.getLetterHeading();
		} else {
			LOGGER.info("Unable to find terms and conditions.");
		}
		pdfDocument = new PdfDocument(new PdfWriter(outputStream));
		document = new Document(pdfDocument, new PageSize(PageSize.A4).rotate());
		document.setMargins(0, 0, 0, 0);

		List<ItemMap> itemMap = itemMapService.getAllItemMapByPurchaseOrderNo(poNum);
		List<List<ItemMap>> fullList = new ArrayList<>();
		List<Integer> noOfLines = new ArrayList<>();
		int noLinPPag = 0;
		int noOfLine = 0;
		int previousIndex = 0;
		double[] subTotal = new double[3];
		for (int i = 0; i < itemMap.size(); i++) {
			ItemMap tempItem = itemMap.get(i);
			subTotal[0] = subTotal[0] + tempItem.getTotalExclGST();
			subTotal[1] = subTotal[1] + tempItem.getTotalGST();
			subTotal[2] = subTotal[2] + tempItem.getTotalInclGST();
			try {
				if (StringUtils.isNotBlank(tempItem.getParticulars())) {
					if (tempItem.getParticulars().length() > 60) {
						noOfLine = noOfLine + tempItem.getParticulars().length() / 60;
					} else {
						noOfLine++;
					}
				}
			} catch (Exception e) {
				LOGGER.error(e);
			}
			if (noOfLine > 15 && noOfLine < 20) {
				fullList.add(itemMap.subList(previousIndex, i + 1));
				noOfLine = 0;
				previousIndex = i + 1;
				noOfLines.add(noLinPPag);
				noLinPPag++;
			}
		}
		fullList.add(itemMap.subList(previousIndex, itemMap.size()));
		noOfLines.add(noLinPPag);

		int perPage = 1;
		int totNoPages = fullList.size();
		double[] balCF = new double[] { 0, 0, 0, 1 };
		double[] balBF = new double[] { 0, 0, 0, 1 };
		for (List<ItemMap> itemMapActual : fullList) {
			balBF = createPage(itemMapActual, poNum, perPage, totNoPages, subTotal, balBF, balCF);
			perPage++;
		}

		PdfReader tncReader = null;
		try {
			pdfDocument.removePage(pdfDocument.getLastPage());
			if (StringUtils.isNotBlank(termAndConditionsDestination)) {
				File tncCheck = new File(termAndConditionsDestination);
				if (tncCheck.exists()) {
					tncReader = new PdfReader(termAndConditionsDestination);
					if (tncReader != null) {
						termAndConditionsDocument = new PdfDocument(new PdfReader(termAndConditionsDestination));
						termAndConditionsDocument.copyPagesTo(1, termAndConditionsDocument.getNumberOfPages(),
								pdfDocument);
						termAndConditionsDocument.close();
					}
					tncReader.close();
				}
			}
			pdfDocument.close();
			document.close();
		} catch (NullPointerException e) {
			LOGGER.error(e);
		} finally {
			if (termAndConditionsDocument != null) {
				termAndConditionsDocument.close();
			}
			if (tncReader != null) {
				tncReader.close();
			}
			if (pdfDocument != null) {
				pdfDocument.close();
			}
			if (document != null) {
				document.close();
			}
		}
		String printedBy = employeeMasterService.getEmployeeMasterByPsId(getCurrentUser().get()).getName();
		purchaseOrder.setLastPrintedBy(printedBy);
		purchaseOrder.setLastPrintedOn(new Date());
		purchaseOrderService.createPurchaseOrder(purchaseOrder);
		return outputStream;
	}

	private double[] createPage(List<ItemMap> itemMapActual, String poNum, int perPage, int totNoPages,
			double[] subTotal, double[] balBF, double[] balCF) throws java.io.IOException {
		ILineDash solid = new Solid();
		pdfDocument.addNewPage();
		try {
			topContent(poNum, perPage, totNoPages);
		} catch (java.io.IOException e) {
			LOGGER.error(e);
		}

		PdfFont font = PdfFontFactory.createFont(FontConstants.TIMES_ROMAN);
		Table table = new Table(new float[] { 10, 42, 62, 218, 20, 42, 62, 40, 62, 62, 62, 62, 62 });
		table.setFontSize(8);
		table.setWidth(840);
		table.setMargins(0, 0, 0, 0);
		table.setFont(font);

		/*** Header Cells ***/
		Cell no = new Cell();
		no.add("No.");
		no.setBorder(Border.NO_BORDER);
		no.setNextRenderer(new CustomBorder3Renderer(no, new ILineDash[] { solid, solid, null, null }));
		table.addCell(no);

		Cell pc = new Cell();
		pc.add("Profit Center");
		pc.setBorder(Border.NO_BORDER);
		pc.setNextRenderer(new CustomBorder3Renderer(pc, new ILineDash[] { solid, null, null, null }));
		table.addCell(pc);

		Cell item = new Cell();
		item.add("Item/Part");
		item.add("Article No.");
		item.setBorder(Border.NO_BORDER);
		item.setTextAlignment(TextAlignment.CENTER);
		item.setNextRenderer(new CustomBorder3Renderer(item, new ILineDash[] { solid, null, null, null }));
		table.addCell(item);

		Cell particulars = new Cell();
		particulars.add("Particulars");
		particulars.setBorder(Border.NO_BORDER);
		particulars.setTextAlignment(TextAlignment.CENTER);
		particulars
				.setNextRenderer(new CustomBorder3Renderer(particulars, new ILineDash[] { solid, null, null, null }));
		table.addCell(particulars);

		Cell pack = new Cell();
		pack.add("UOM/");
		pack.add("Packing");
		pack.setBorder(Border.NO_BORDER);
		pack.setNextRenderer(new CustomBorder3Renderer(pack, new ILineDash[] { solid, null, null, null }));
		table.addCell(pack);

		Cell quantity = new Cell();
		quantity.add("Quantity");
		quantity.add("Ordered");
		quantity.setBorder(Border.NO_BORDER);
		quantity.setTextAlignment(TextAlignment.CENTER);
		quantity.setNextRenderer(new CustomBorder3Renderer(quantity, new ILineDash[] { solid, null, null, null }));
		table.addCell(quantity);

		Cell unitPrice = new Cell();
		unitPrice.add("Unit Price").setTextAlignment(TextAlignment.CENTER);
		unitPrice.add("(Excl.GST)").setTextAlignment(TextAlignment.CENTER);
		unitPrice.setBorder(Border.NO_BORDER);
		unitPrice.setNextRenderer(new CustomBorder3Renderer(unitPrice, new ILineDash[] { solid, null, null, null }));
		table.addCell(unitPrice);

		Cell gst = new Cell();
		gst.add("GST%").setTextAlignment(TextAlignment.CENTER);
		gst.setBorder(Border.NO_BORDER);
		gst.setTextAlignment(TextAlignment.CENTER);
		gst.setNextRenderer(new CustomBorder3Renderer(gst, new ILineDash[] { solid, null, null, null }));
		table.addCell(gst);

		Cell UPInc = new Cell();
		UPInc.add("Unit Price").setTextAlignment(TextAlignment.CENTER);
		UPInc.add("(Incl.GST)").setTextAlignment(TextAlignment.CENTER);
		UPInc.setBorder(Border.NO_BORDER);
		UPInc.setNextRenderer(new CustomBorder3Renderer(UPInc, new ILineDash[] { solid, null, null, null }));
		table.addCell(UPInc);

		Cell disCount = new Cell();
		disCount.add("Discount%");
		disCount.setBorder(Border.NO_BORDER);
		disCount.setTextAlignment(TextAlignment.CENTER);
		disCount.setNextRenderer(new CustomBorder3Renderer(disCount, new ILineDash[] { solid, null, null, null }));
		table.addCell(disCount);

		Cell totEx = new Cell();
		totEx.add("Total").setTextAlignment(TextAlignment.CENTER);
		totEx.add("(Excl.GST)").setTextAlignment(TextAlignment.CENTER);
		totEx.setBorder(Border.NO_BORDER);
		totEx.setNextRenderer(new CustomBorder3Renderer(totEx, new ILineDash[] { solid, null, null, null }));
		table.addCell(totEx);

		Cell totGst = new Cell();
		totGst.add("Total").setTextAlignment(TextAlignment.CENTER);
		totGst.add("GST Amount").setTextAlignment(TextAlignment.CENTER);
		totGst.setBorder(Border.NO_BORDER);
		totGst.setNextRenderer(new CustomBorder3Renderer(totGst, new ILineDash[] { solid, null, null, null }));
		table.addCell(totGst);

		Cell totIncGst = new Cell();
		totIncGst.add("Total").setTextAlignment(TextAlignment.CENTER);
		totIncGst.add("(Incl.GST)").setTextAlignment(TextAlignment.CENTER);
		totIncGst.setBorder(Border.NO_BORDER);
		totIncGst.setNextRenderer(new CustomBorder3Renderer(totIncGst, new ILineDash[] { solid, null, null, solid }));
		table.addCell(totIncGst);

		// Add before carry forward
		for (int crInx = 0; crInx < 9; crInx++) {
			if (crInx == 0) {
				Cell spaceFirst = new Cell();
				spaceFirst.add(" ");
				spaceFirst.setBorder(Border.NO_BORDER);
				spaceFirst.setNextRenderer(
						new CustomBorder3Renderer(spaceFirst, new ILineDash[] { solid, solid, null, null }));
				table.addCell(spaceFirst);
			} else {
				Cell spaceCell = new Cell();
				spaceCell.add(" ");
				spaceCell.setBorder(Border.NO_BORDER);
				spaceCell.setNextRenderer(
						new CustomBorder3Renderer(spaceCell, new ILineDash[] { solid, null, null, null }));
				table.addCell(spaceCell);
			}
		}

		Cell balance = new Cell();
		if (perPage != 1) {
			balance.add("Balance b/f:").setFont(font).setBold();
			rowCount++;
		} else {
			balance.add(" ").setFont(font).setBold();
		}
		balance.setBorder(Border.NO_BORDER);
		balance.setTextAlignment(TextAlignment.RIGHT);
		balance.setNextRenderer(new CustomBorder3Renderer(balance, new ILineDash[] { solid, null, null, null }));
		if (rowCount % 2 != 0) {
			balance.setBackgroundColor(Color.LIGHT_GRAY);
		}
		table.addCell(balance);

		Cell balance0 = new Cell();
		if (perPage != 1) {
			balance0.add(String.format("%,.2f", balBF[0]));
		} else {
			balance0.add(" ").setFont(font).setBold();
		}
		balance0.setBorder(Border.NO_BORDER);
		balance0.setTextAlignment(TextAlignment.RIGHT);
		balance0.setNextRenderer(new CustomBorder3Renderer(balance0, new ILineDash[] { solid, null, null, null }));
		if (rowCount % 2 != 0) {
			balance0.setBackgroundColor(Color.LIGHT_GRAY);
		}
		table.addCell(balance0);

		Cell balance1 = new Cell();
		if (perPage != 1) {
			balance1.add(String.format("%,.2f", balBF[1]));
		} else {
			balance1.add(" ").setFont(font).setBold();
		}
		balance1.setBorder(Border.NO_BORDER);
		balance1.setTextAlignment(TextAlignment.RIGHT);
		balance1.setNextRenderer(new CustomBorder3Renderer(balance1, new ILineDash[] { solid, null, null, null }));
		if (rowCount % 2 != 0) {
			balance1.setBackgroundColor(Color.LIGHT_GRAY);
		}
		table.addCell(balance1);

		Cell balance2 = new Cell();
		if (perPage != 1) {
			balance2.add(String.format("%,.2f", balBF[2]));
		} else {
			balance2.add(" ").setFont(font).setBold();
		}
		balance2.setBorder(Border.NO_BORDER);
		balance2.setTextAlignment(TextAlignment.RIGHT);
		balance2.setNextRenderer(new CustomBorder3Renderer(balance2, new ILineDash[] { solid, null, null, solid }));
		if (rowCount % 2 != 0) {
			balance2.setBackgroundColor(Color.LIGHT_GRAY);
		}
		table.addCell(balance2);

		rowCount++;

		/*** Main Cells ***/
		int slNo = (int) balBF[3];
		for (ItemMap itemMap : itemMapActual) {
			Cell slNoCell = new Cell();
			slNoCell.add(Integer.toString(slNo));
			slNoCell.setBorder(Border.NO_BORDER);
			slNoCell.setNextRenderer(new CustomBorder3Renderer(slNoCell, new ILineDash[] { null, solid, null, null }));
			if (rowCount % 2 != 0) {
				slNoCell.setBackgroundColor(Color.LIGHT_GRAY);
			}
			table.addCell(slNoCell);

			// Change with fetching the pc from sepererate Db field
			Cell profCenter = new Cell();
			try {
				if (itemMap != null) {
					profCenter.add(itemMap.getPcCode()).setFont(font);
				} else {
					LOGGER.info("item map profit center code is empty. ");
				}
			} catch (Exception e) {
				LOGGER.error(e);
				profCenter.add("");
			}
			profCenter.setBorder(Border.NO_BORDER);
			profCenter
					.setNextRenderer(new CustomBorder3Renderer(profCenter, new ILineDash[] { null, null, null, null }));
			if (rowCount % 2 != 0) {
				profCenter.setBackgroundColor(Color.LIGHT_GRAY);
			}
			table.addCell(profCenter);

			Cell items = new Cell();
			try {
				items.add(itemMap.getItemNo()).setFont(font);
			} catch (Exception e) {
				LOGGER.error("Items is empty. ", null);
				items.add("");
			}
			items.setBorder(Border.NO_BORDER);
			items.setTextAlignment(TextAlignment.CENTER);
			items.setNextRenderer(new CustomBorder3Renderer(items, new ILineDash[] { null, null, null, null }));
			if (rowCount % 2 != 0) {
				items.setBackgroundColor(Color.LIGHT_GRAY);
			}
			table.addCell(items);

			String particular = itemMap.getParticulars();
			String particularNew = null;
			Cell particularCell = new Cell();
			try {
				if (!particular.isEmpty() || particular != null) {
					if (particular.length() > 60) {
						particularNew = particular.replaceAll(".{60}(?!$)", "$0\n");
					} else {
						particularNew = particular;
					}
				}
			} catch (Exception e) {
				LOGGER.error("particulars in table is empty. ", e);
			}
			particularCell.setBorder(Border.NO_BORDER).setFont(font);
			particularCell.setNextRenderer(
					new CustomBorder3Renderer(particularCell, new ILineDash[] { null, null, null, null }));
			if (rowCount % 2 != 0) {
				particularCell.setBackgroundColor(Color.LIGHT_GRAY);
			}
			table.addCell(particularCell.add(particularNew));

			Cell packing = new Cell();
			try {
				packing.add(itemMap.getPacking()).setFont(font);
			} catch (Exception e) {
				LOGGER.error("packing is empty. ", e);
				packing.add("");
			}
			packing.setBorder(Border.NO_BORDER);
			packing.setTextAlignment(TextAlignment.CENTER);
			packing.setNextRenderer(new CustomBorder3Renderer(packing, new ILineDash[] { null, null, null, null }));
			if (rowCount % 2 != 0) {
				packing.setBackgroundColor(Color.LIGHT_GRAY);
			}
			table.addCell(packing);

			Cell quantity1 = new Cell();
			try {
				quantity1.add(String.valueOf(itemMap.getQuantity())).setFont(font);
			} catch (Exception e) {
				LOGGER.error("quantity is empty. ", e);
				quantity1.add("");
			}
			quantity1.setBorder(Border.NO_BORDER);
			quantity1.setTextAlignment(TextAlignment.CENTER);
			quantity1.setNextRenderer(new CustomBorder3Renderer(quantity1, new ILineDash[] { null, null, null, null }));
			if (rowCount % 2 != 0) {
				quantity1.setBackgroundColor(Color.LIGHT_GRAY);
			}
			table.addCell(quantity1);

			Cell unitExclGst = new Cell();
			String unitExclGstFormated = String.format("%,.2f", itemMap.getUnitPriceExclGST());
			try {
				unitExclGst.add(unitExclGstFormated).setFont(font);
			} catch (Exception e) {
				LOGGER.error("unit price excluding GST is empty. ", e);
				unitExclGst.add("");
			}
			unitExclGst.setBorder(Border.NO_BORDER);
			unitExclGst.setTextAlignment(TextAlignment.RIGHT);
			unitExclGst.setNextRenderer(
					new CustomBorder3Renderer(unitExclGst, new ILineDash[] { null, null, null, null }));
			if (rowCount % 2 != 0) {
				unitExclGst.setBackgroundColor(Color.LIGHT_GRAY);
			}
			table.addCell(unitExclGst);

			Cell gstCell = new Cell();
			gstCell.add(itemMap.getTaxRate() + "% " + itemMap.getTaxcode()).setFont(font);
			gstCell.setBorder(Border.NO_BORDER);
			gstCell.setTextAlignment(TextAlignment.RIGHT);
			gstCell.setNextRenderer(new CustomBorder3Renderer(gstCell, new ILineDash[] { null, null, null, null }));
			if (rowCount % 2 != 0) {
				gstCell.setBackgroundColor(Color.LIGHT_GRAY);
			}
			table.addCell(gstCell);

			Cell unPriceInclGST = new Cell();
			String unPriceInclGSTFormated = String.format("%,.2f", itemMap.getUnitPriceInclGST());
			try {
				unPriceInclGST.add(String.valueOf(unPriceInclGSTFormated)).setFont(font);
			} catch (Exception e) {
				LOGGER.error("unit price including GST is empty. ", e);
				unPriceInclGST.add("");
			}
			unPriceInclGST.setBorder(Border.NO_BORDER);
			unPriceInclGST.setTextAlignment(TextAlignment.RIGHT);
			unPriceInclGST.setNextRenderer(
					new CustomBorder3Renderer(unPriceInclGST, new ILineDash[] { null, null, null, null }));
			if (rowCount % 2 != 0) {
				unPriceInclGST.setBackgroundColor(Color.LIGHT_GRAY);
			}
			table.addCell(unPriceInclGST);

			Cell discount = new Cell();
			try {
				discount.add(String.valueOf(itemMap.getDiscount())).setFont(font);
			} catch (Exception e) {
				LOGGER.error("discount is empty. ", e);
				discount.add("");
			}
			discount.setBorder(Border.NO_BORDER);
			discount.setTextAlignment(TextAlignment.RIGHT);
			discount.setNextRenderer(new CustomBorder3Renderer(discount, new ILineDash[] { null, null, null, null }));
			if (rowCount % 2 != 0) {
				discount.setBackgroundColor(Color.LIGHT_GRAY);
			}
			table.addCell(discount);

			Cell totalExclGST = new Cell();
			String totalExclGSTFormated = String.format("%,.2f", itemMap.getTotalExclGST());
			try {
				totalExclGST.add(totalExclGSTFormated).setFont(font);
			} catch (Exception e) {
				totalExclGST.add("");
				LOGGER.error("total excluding GST is empty. ", e);
			}
			totalExclGST.setBorder(Border.NO_BORDER);
			totalExclGST.setTextAlignment(TextAlignment.RIGHT);
			totalExclGST.setNextRenderer(
					new CustomBorder3Renderer(totalExclGST, new ILineDash[] { null, null, null, null }));
			if (rowCount % 2 != 0) {
				totalExclGST.setBackgroundColor(Color.LIGHT_GRAY);
			}
			table.addCell(totalExclGST);

			Cell totalGST = new Cell();
			String totalGSTFormated = String.format("%,.2f", itemMap.getTotalGST());
			try {
				totalGST.add(String.valueOf(totalGSTFormated)).setFont(font);
			} catch (Exception e) {
				totalGST.add("");
				LOGGER.error("total GST is empty. ", e);
			}
			totalGST.setBorder(Border.NO_BORDER);
			totalGST.setTextAlignment(TextAlignment.RIGHT);
			totalGST.setNextRenderer(new CustomBorder3Renderer(totalGST, new ILineDash[] { null, null, null, null }));
			if (rowCount % 2 != 0) {
				totalGST.setBackgroundColor(Color.LIGHT_GRAY);
			}
			table.addCell(totalGST);

			Cell totalInclGST = new Cell();
			String angleFormated = String.format("%,.2f", itemMap.getTotalInclGST());
			try {
				totalInclGST.add(String.valueOf(angleFormated)).setFont(font);
			} catch (Exception e) {
				totalInclGST.add("");
				LOGGER.error("total including GST is empty. ", e);
			}
			totalInclGST.setBorder(Border.NO_BORDER);
			totalInclGST.setTextAlignment(TextAlignment.RIGHT);
			totalInclGST.setNextRenderer(
					new CustomBorder3Renderer(totalInclGST, new ILineDash[] { null, null, null, solid }));
			if (rowCount % 2 != 0) {
				totalInclGST.setBackgroundColor(Color.LIGHT_GRAY);
			}
			table.addCell(totalInclGST);

			slNo++;
			balCF[0] = balCF[0] + itemMap.getTotalExclGST();
			balCF[1] = balCF[1] + itemMap.getTotalGST();
			balCF[2] = balCF[2] + itemMap.getTotalInclGST();
			rowCount++;
		}

		for (int crInx = 0; crInx < 9; crInx++) {
			if (crInx == 0) {
				Cell spaceFirst = new Cell();
				spaceFirst.add(" ");
				spaceFirst.setBorder(Border.NO_BORDER);
				spaceFirst.setNextRenderer(
						new CustomBorder3Renderer(spaceFirst, new ILineDash[] { null, solid, solid, null }));
				table.addCell(spaceFirst);
			} else {
				Cell spaceCell = new Cell();
				spaceCell.add(" ");
				spaceCell.setBorder(Border.NO_BORDER);
				spaceCell.setNextRenderer(
						new CustomBorder3Renderer(spaceCell, new ILineDash[] { null, null, solid, null }));
				table.addCell(spaceCell);
			}
		}

		Cell balancebot = new Cell();
		if (perPage != totNoPages) {
			Text balanceTxt = new Text("Balance c/f:");
			balancebot.add(new Paragraph(balanceTxt)).setFont(font).setBold();
		} else {
			balancebot.add(" ").setFont(font).setBold();
		}
		balancebot.setBorder(Border.NO_BORDER);
		balancebot.setTextAlignment(TextAlignment.RIGHT);
		balancebot.setNextRenderer(new CustomBorder3Renderer(balancebot, new ILineDash[] { null, null, solid, null }));
		table.addCell(balancebot);

		Cell balancebot0 = new Cell();
		if (perPage != totNoPages) {
			balancebot0.add(String.format("%,.2f", balCF[0]));
		} else {
			balancebot0.add(" ");
		}
		balancebot0.setBorder(Border.NO_BORDER);
		balancebot0.setTextAlignment(TextAlignment.RIGHT);
		balancebot0
				.setNextRenderer(new CustomBorder3Renderer(balancebot0, new ILineDash[] { null, null, solid, null }));
		table.addCell(balancebot0);

		Cell balancebot1 = new Cell();
		if (perPage != totNoPages) {
			balancebot1.add(String.format("%.2f", balCF[1]));
		} else {
			balancebot1.add(" ");
		}
		balancebot1.setBorder(Border.NO_BORDER);
		balancebot1.setTextAlignment(TextAlignment.RIGHT);
		balancebot1
				.setNextRenderer(new CustomBorder3Renderer(balancebot1, new ILineDash[] { null, null, solid, null }));
		table.addCell(balancebot1);

		Cell balancebot2 = new Cell();
		if (perPage != totNoPages) {
			balancebot2.add(String.format("%,.2f", balCF[2]));
		} else {
			balancebot2.add(" ");
		}
		balancebot2.setBorder(Border.NO_BORDER);
		balancebot2.setTextAlignment(TextAlignment.RIGHT);
		balancebot2
				.setNextRenderer(new CustomBorder3Renderer(balancebot2, new ILineDash[] { null, null, solid, solid }));
		table.addCell(balancebot2);
		Table botTable = new Table(new float[] { 50, 32, 42, 42, 42 });
		botTable.setFontSize(7);
		botTable.setWidth(250);
		botTable.setMarginTop(10);
		botTable.setMarginRight(0);
		botTable.setMarginLeft(575);
		botTable.setFont(font);
		for (int inx = 0; inx < 3; inx++) {
			if (inx == 0) {
				botTable.addCell(new Cell().setBorder(Border.NO_BORDER).add("Sub-Total   :").setFontSize(7));
			}
			if (inx == 1) {
				botTable.addCell(new Cell().add("Discount    :").setBorder(Border.NO_BORDER).setFontSize(7));
			}
			if (inx == 2) {
				botTable.addCell(new Cell().setBorder(Border.NO_BORDER).add("Total       :").setFontSize(7).setBold());
			}
			botTable.addCell(new Cell().add(" "));
			if (perPage == totNoPages) {
				double totExclGSTDis = 0;
				double totIncGSTDis = 0;
				double totGSTDis = 0;
				double totExclGST = 0;
				double totIncGST = 0;
				double totGST = 0;
				if (inx == 0) {
					botTable.addCell(new Cell().add(String.format("%,.2f", subTotal[0]))
							.setTextAlignment(TextAlignment.RIGHT).setFontSize(7).setBold());
					botTable.addCell(new Cell().add(String.format("%,.2f", subTotal[1]))
							.setTextAlignment(TextAlignment.RIGHT).setFontSize(7).setBold());
					botTable.addCell(new Cell().add(String.format("%,.2f", subTotal[2]))
							.setTextAlignment(TextAlignment.RIGHT).setFontSize(7).setBold());
				}
				if (inx == 1) {
					botTable.addCell(new Cell().add(String.format("%,.2f", totExclGSTDis))
							.setTextAlignment(TextAlignment.RIGHT).setFontSize(7).setBold());
					botTable.addCell(new Cell().add(String.format("%,.2f", totGSTDis))
							.setTextAlignment(TextAlignment.RIGHT).setFontSize(7).setBold());
					botTable.addCell(new Cell().add(String.format("%,.2f", totIncGSTDis))
							.setTextAlignment(TextAlignment.RIGHT).setFontSize(7).setBold());
				}
				if (inx == 2) {
					totExclGST = subTotal[0] - totExclGSTDis;
					totIncGST = subTotal[2] - totIncGSTDis;
					totGST = subTotal[1] - totGSTDis;
					botTable.addCell(new Cell().add(String.format("%,.2f", totExclGST))
							.setTextAlignment(TextAlignment.RIGHT).setFontSize(7).setBold());
					botTable.addCell(new Cell().add(String.format("%,.2f", totGST))
							.setTextAlignment(TextAlignment.RIGHT).setFontSize(7).setBold());
					botTable.addCell(new Cell().add(String.format("%,.2f", totIncGST))
							.setTextAlignment(TextAlignment.RIGHT).setFontSize(7).setBold());
				}
			} else {
				botTable.addCell(new Cell().add(" "));
				botTable.addCell(new Cell().add(" "));
				botTable.addCell(new Cell().add(" "));
			}
		}

		document.add(table);
		document.add(botTable);
		document.add(new AreaBreak(AreaBreakType.NEXT_PAGE));

		balCF[3] = slNo;

		PdfPage page = pdfDocument.getPage(perPage);
		PdfCanvas pdfCanvas = new PdfCanvas(page);
		PdfCanvas lineCanvas = new PdfCanvas(page);
		PdfCanvas lineCanvasDes = new PdfCanvas(page);
		PdfCanvas lineCanvas2 = new PdfCanvas(page);
		PdfCanvas lineCanvasDes2 = new PdfCanvas(page);
		PdfCanvas lineCanvas3 = new PdfCanvas(page);
		PdfCanvas lineCanvasDes3 = new PdfCanvas(page);

		/*** Create Boxes ***/
		Rectangle payTerms = new Rectangle(3, 10, 228, 110);
		Rectangle delivInst = new Rectangle(260, 72, 290, 47);
		Rectangle delivBot = new Rectangle(260, 10, 290, 67);

		/*** Create Label Boxes ***/
		Rectangle delivInstLable = new Rectangle(260, 115, 100, 20);
		Rectangle paymentTermsLable = new Rectangle(20, 115, 100, 20);

		Rectangle line1Lable = new Rectangle(560, 8, 100, 20);
		Rectangle line2Lable = new Rectangle(650, 8, 100, 20);
		Rectangle line3Lable = new Rectangle(740, 8, 100, 20);
		Rectangle approver1 = new Rectangle(560, 35, 100, 42);
		Rectangle designation1 = new Rectangle(560, -25, 100, 42);
		Rectangle approver2 = new Rectangle(650, 35, 100, 42);
		Rectangle designation2 = new Rectangle(650, -25, 100, 42);
		Rectangle approver3 = new Rectangle(740, 35, 100, 42);
		Rectangle designation3 = new Rectangle(740, -25, 100, 42);

		/*** Add Boxes to Canvas ***/
		pdfCanvas.rectangle(payTerms);
		pdfCanvas.rectangle(delivInst);

		lineCanvas.moveTo(560, 25);
		lineCanvasDes.moveTo(560, 25);
		lineCanvas.lineTo(640, 25);
		lineCanvasDes.lineTo(640, 25);
		lineCanvas.closePathStroke();
		lineCanvasDes.closePathStroke();

		lineCanvas2.moveTo(650, 25);
		lineCanvasDes2.moveTo(650, 25);
		lineCanvas2.lineTo(730, 25);
		lineCanvasDes2.lineTo(730, 25);
		lineCanvas2.closePathStroke();
		lineCanvasDes2.closePathStroke();

		lineCanvas3.moveTo(740, 25);
		lineCanvasDes3.moveTo(740, 25);
		lineCanvas3.lineTo(820, 25);
		lineCanvasDes3.lineTo(820, 25);
		lineCanvas3.closePathStroke();
		lineCanvasDes3.closePathStroke();

		pdfCanvas.stroke();

		/*** Add Label Rectangle to the Canvas ***/
		pdfCanvas.rectangle(delivInstLable);
		pdfCanvas.rectangle(paymentTermsLable);

		Canvas canvas = new Canvas(pdfCanvas, pdfDocument, payTerms);
		Canvas canvasDeliv = new Canvas(pdfCanvas, pdfDocument, delivInst);
		Canvas canvasDelivBot = new Canvas(pdfCanvas, pdfDocument, delivBot);
		canvasDelivBot.setBorder(Border.NO_BORDER);
		Canvas canvasDelivLab = new Canvas(pdfCanvas, pdfDocument, delivInstLable);
		Canvas canvasPayLab = new Canvas(pdfCanvas, pdfDocument, paymentTermsLable);
		Canvas canvasLine1 = new Canvas(lineCanvas, pdfDocument, line1Lable);
		Canvas canvasLine2 = new Canvas(lineCanvas2, pdfDocument, line2Lable);
		Canvas canvasLine3 = new Canvas(lineCanvas3, pdfDocument, line3Lable);
		Canvas canvasLine1App = new Canvas(lineCanvas, pdfDocument, approver1);
		Canvas canvasLineDesig1 = new Canvas(lineCanvasDes, pdfDocument, designation1);
		Canvas canvasLine2App = new Canvas(lineCanvas2, pdfDocument, approver2);
		Canvas canvasLineDesig2 = new Canvas(lineCanvasDes2, pdfDocument, designation2);
		Canvas canvasLine3App = new Canvas(lineCanvas3, pdfDocument, approver3);
		Canvas canvasLineDesig3 = new Canvas(lineCanvasDes3, pdfDocument, designation3);

		String deliveryInst = " ";
		if (purchaseOrder.getDeliveryInstruction() != null) {
			deliveryInst = purchaseOrder.getDeliveryInstruction();
		}

		String payTnc = " ";
		if (purchaseOrder.getPaymentTerms() != null) {
			payTnc = purchaseOrder.getPaymentTerms();
		}

		Paragraph del = new Paragraph();
		del.add(deliveryInst).setFont(font).setFontSize(7);
		del.setPadding(4);
		canvasDeliv.add(del);

		Paragraph pay = new Paragraph();
		pay.add(payTnc).setFont(font).setFontSize(7);
		pay.setPadding(4);
		canvas.add(pay);

		/*** Create Label ***/
		Text dlInst = new Text("Delivery Instructions:").setFontSize(8).setFont(font);
		Text paymentTerms = new Text("Payment Terms:").setFontSize(8).setFont(font);
		Text line1 = new Text("1st Approval By,").setFontSize(7).setFont(font);
		Text line2 = new Text("2nd Approval By,").setFontSize(7).setFont(font);
		Text line3 = new Text("3rd Approval By,").setFontSize(7).setFont(font);

		String issuer = purchaseOrder.getIssuer();
		Date issuedOn = purchaseOrder.getIssuedOn();
		String verifier = purchaseOrder.getVerifier();
		Date verifyOn = purchaseOrder.getVerifiedOn();
		String authorizer = purchaseOrder.getAuthorizer();
		Date authorizeOn = purchaseOrder.getAuthorizedOn();
		String printedBy = employeeMasterService.getEmployeeMasterByPsId(getCurrentUser().get()).getName();

		String textIssu = "";
		String textVerify = "";
		String textAuthorize = "";
		String textPrinted = "";
		Date printedOn = new Date();
		Paragraph tot = new Paragraph();
		Text issuLab;
		try {
			if (!issuer.isEmpty() || issuer != null) {
				textIssu += " " + " " + "Issued by" + " " + "<" + issuer + ">";
			}
			if (issuedOn != null) {
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyy hh:mm");
				textIssu += " " + "on" + " " + sdf.format(issuedOn) + "\n";
			}
			issuLab = new Text(textIssu).setFontSize(6).setFont(font);
			tot.add(issuLab).setMarginTop(20);
		} catch (NullPointerException e1) {
			LOGGER.error("issued by is empty. ", e1);
		} catch (IllegalArgumentException e) {
			LOGGER.error("issued date format is illegal. ", e);
		}
		if (purchaseOrder.getStatus().equals("Verified") || purchaseOrder.getStatus().equals("Authorized")) {
			try {
				if (!verifier.isEmpty() || verifier != null) {
					textVerify += " " + " " + "Verified by" + " " + "<" + verifier + ">";
				}
				if (verifyOn != null) {
					SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyy hh:mm");
					textVerify += " " + "on" + " " + sdf.format(verifyOn) + "\n";
				}
				Text verifyLab = new Text(textVerify).setFontSize(6).setFont(font);
				tot.add(verifyLab);
			} catch (NullPointerException e1) {
				LOGGER.error("verified by is empty. ", e1);
			} catch (IllegalArgumentException e) {
				LOGGER.error("verified date format is illegal. ", e);
			}
		}
		if (purchaseOrder.getStatus().equals("Authorized")) {
			try {
				if (!authorizer.isEmpty() || authorizer != null) {
					textAuthorize += " " + " " + "Authorized by" + " " + "<" + authorizer + ">";
				}
				if (authorizeOn != null) {
					SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyy hh:mm");
					textAuthorize += " " + "on" + " " + sdf.format(authorizeOn) + "\n";
				}
				Text authorizeLab = new Text(textAuthorize).setFontSize(6).setFont(font);
				tot.add((authorizeLab));
			} catch (NullPointerException e1) {
				LOGGER.error("authorized by is empty. ", e1);
			} catch (IllegalArgumentException e) {
				LOGGER.error("authorized date format is illegal. ", e);
			}
		}
		try {
			if (!printedBy.isEmpty() || printedBy != null) {
				textPrinted += " " + " " + "Printed by" + " " + "<" + printedBy + ">";
			}
			if (printedOn != null) {
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyy hh:mm");
				textPrinted += " " + "on" + " " + sdf.format(printedOn);
			}
			Text printLab = new Text(textPrinted).setFontSize(6).setFont(font);
			tot.add((printLab));
		} catch (NullPointerException e1) {
			LOGGER.error("printed by is empty. ", e1);
		} catch (IllegalArgumentException e) {
			LOGGER.error("printed date format is illegal. ", e);
		}
		canvasDelivBot.add(tot);

		if (purchaseOrder.getStatus().equals("Authorized")) {
			try {
				if (!purchaseOrder.getFirstApprover().isEmpty() || purchaseOrder.getFirstApprover() != null) {
					Text approverText = new Text(purchaseOrder.getFirstApprover()).setFontSize(7);
					canvasLine1.add(new Paragraph(approverText).setTextAlignment(TextAlignment.CENTER));
				} else {
					canvasLine1App.add(new Paragraph(" "));
				}
			} catch (NullPointerException | IllegalArgumentException ex) {
				LOGGER.error("first approver is empty. ", ex);
			}
			try {
				if (!purchaseOrder.getFirstApproverDesignation().isEmpty()
						|| purchaseOrder.getFirstApproverDesignation() != null) {
					Text desig1 = new Text(purchaseOrder.getFirstApproverDesignation()).setFontSize(7);
					canvasLineDesig1.add(new Paragraph(desig1).setTextAlignment(TextAlignment.CENTER));
				} else {
					canvasLineDesig1.add(new Paragraph(" "));
				}
			} catch (NullPointerException | IllegalArgumentException ex) {
				LOGGER.error("first approver designation is empty. ", ex);
			}
			try {
				if (!purchaseOrder.getSecondApprover().isEmpty() || purchaseOrder.getSecondApprover() != null) {
					Text app2 = new Text(purchaseOrder.getSecondApprover()).setFontSize(7)
							.setTextAlignment(TextAlignment.CENTER);
					canvasLine2.add(new Paragraph(app2).setTextAlignment(TextAlignment.CENTER));
				} else {
					canvasLine2App.add(new Paragraph(" "));
				}
			} catch (NullPointerException | IllegalArgumentException ex) {
				LOGGER.error("second approver is empty. ", ex);
			}
			try {
				if (!purchaseOrder.getSecondApproverDesignation().isEmpty()
						|| purchaseOrder.getSecondApproverDesignation() != null) {
					Text desig2 = new Text(purchaseOrder.getSecondApproverDesignation()).setFontSize(7);
					canvasLineDesig2.add(new Paragraph(desig2).setTextAlignment(TextAlignment.CENTER));
				} else {
					canvasLineDesig2.add(new Paragraph(" "));
				}
			} catch (NullPointerException | IllegalArgumentException ex) {
				LOGGER.error("second approver designation is empty. ", ex);
			}
			try {
				if (!purchaseOrder.getThirdApprover().isEmpty() || purchaseOrder.getThirdApprover() != null) {
					Text app3 = new Text(purchaseOrder.getThirdApprover()).setFontSize(7).setFontColor(Color.BLACK)
							.setTextAlignment(TextAlignment.CENTER);
					canvasLine3.add(new Paragraph(app3).setTextAlignment(TextAlignment.CENTER));
				} else {
					canvasLine3App.add(new Paragraph(" "));
				}
			} catch (NullPointerException | IllegalArgumentException ex) {
				LOGGER.error("third approver is empty. ", ex);
			}
			try {
				if (!purchaseOrder.getThirdApproverDesignation().isEmpty()
						|| purchaseOrder.getThirdApproverDesignation() != null) {
					Text desig3 = new Text(purchaseOrder.getThirdApproverDesignation()).setFontSize(7);
					canvasLineDesig3.add(new Paragraph(desig3).setTextAlignment(TextAlignment.CENTER));
				} else {
					canvasLineDesig3.add(new Paragraph(" "));
				}
			} catch (NullPointerException | IllegalArgumentException ex) {
				LOGGER.error("third approver designation is empty. ", ex);
			}
		}
		canvasDelivLab.add(new Paragraph(dlInst));
		canvasPayLab.add(new Paragraph(paymentTerms));
		if (purchaseOrder.getStatus().equals("Authorized")) {
			canvasLine1App.add(new Paragraph(line1).setTextAlignment(TextAlignment.CENTER));
			canvasLine2App.add(new Paragraph(line2).setTextAlignment(TextAlignment.CENTER));
			canvasLine3App.add(new Paragraph(line3).setTextAlignment(TextAlignment.CENTER));
		}
		return balCF;
	}

	interface ILineDash {
		void applyLineDash(PdfCanvas canvas);
	}

	class Solid implements ILineDash {
		public void applyLineDash(PdfCanvas canvas) {
		}
	}

	class Dotted implements ILineDash {
		public void applyLineDash(PdfCanvas canvas) {
			canvas.setLineCapStyle(PdfCanvasConstants.LineCapStyle.ROUND);
			canvas.setLineDash(0, 4, 2);
		}
	}

	class Dashed implements ILineDash {
		public void applyLineDash(PdfCanvas canvas) {
			canvas.setLineDash(3, 3);
		}
	}

	class CustomBorder3Renderer extends CellRenderer {
		ILineDash[] borders;

		public CustomBorder3Renderer(Cell modelElement, ILineDash[] borders) {
			super(modelElement);
			this.borders = new ILineDash[borders.length];
			for (int i = 0; i < this.borders.length; i++) {
				this.borders[i] = borders[i];
			}
		}

		@Override
		public void draw(DrawContext drawContext) {
			super.draw(drawContext);
			PdfCanvas canvas = drawContext.getCanvas();
			Rectangle position = getOccupiedAreaBBox();
			canvas.saveState();
			if (null != borders[0]) {
				canvas.saveState();
				borders[0].applyLineDash(canvas);
				canvas.moveTo(position.getRight(), position.getTop());
				canvas.lineTo(position.getLeft(), position.getTop());
				canvas.stroke();
				canvas.restoreState();
			}
			if (null != borders[2]) {
				canvas.saveState();
				borders[2].applyLineDash(canvas);
				canvas.moveTo(position.getRight(), position.getBottom());
				canvas.lineTo(position.getLeft(), position.getBottom());
				canvas.stroke();
				canvas.restoreState();
			}
			if (null != borders[3]) {
				canvas.saveState();
				borders[3].applyLineDash(canvas);
				canvas.moveTo(position.getRight(), position.getTop());
				canvas.lineTo(position.getRight(), position.getBottom());
				canvas.stroke();
				canvas.restoreState();
			}
			if (null != borders[1]) {
				canvas.saveState();
				borders[1].applyLineDash(canvas);
				canvas.moveTo(position.getLeft(), position.getTop());
				canvas.lineTo(position.getLeft(), position.getBottom());
				canvas.stroke();
				canvas.restoreState();
			}
			canvas.stroke();
			canvas.restoreState();
		}
	}

	public void topContent(String poNum, int perPage, int totNoPage) throws java.io.IOException {

		/*** Third Column ***/
		String status = purchaseOrder.getStatus();
		String orderNumber = purchaseOrder.getPoNo();
		String creditTerms = purchaseOrder.getCreditTermsCode();
		String currency = purchaseOrder.getCurrency();
		String orderCategory = purchaseOrder.getOrderCatCode();
		String orderSubCategory = purchaseOrder.getOrdSubCatCode();
		String prCode = purchaseOrder.getPrTypeCode();

		Barcode128 code128 = new Barcode128(pdfDocument);
		code128.setFont(null);
		code128.setBarHeight(33);
		code128.setCode(orderNumber);
		code128.setCodeType(Barcode128.CODE128);
		Image code128Image = new Image(code128.createFormXObject(pdfDocument));
		Cell cell = new Cell();
		cell.add(new Paragraph("PO #: " + orderNumber));
		cell.add(code128Image);

		Paragraph header = new Paragraph();
		header.setMarginTop(0);
		try {
			if (image != null && !image.isEmpty()) {
				File imgCheck = new File(image);
				if (imgCheck.exists()) {
					if (new File(image).isFile()) {
						Image img = new Image(ImageDataFactory.create(image));
						img.scaleToFit(80, 80);
						img.setMarginTop(0);
						img.setHorizontalAlignment(HorizontalAlignment.LEFT);
						header.add(img);
					} else {
						header.add("\n");
					}
				}
			}
		} catch (Exception e) {
			header.add("\n");
			LOGGER.error("Unable to load letter head" + e.getMessage(), e);
			/*
			 * Notification notif = new Notification(
			 * "Unable to load letter head", e.getClass(), totNoPage);
			 * notif.notify();
			 */
		}

		PdfFont font = PdfFontFactory.createFont(FontConstants.TIMES_ROMAN);
		header.addTabStops(new TabStop(620, TabAlignment.RIGHT));
		header.add(new Tab());
		status = (status != null ? status : "");
		if (!status.equals("Authorized")) {
			header.add(new Text("<").setFontColor(Color.BLACK));
			if (StringUtils.isNotEmpty(status)) {
				header.add(status).setFont(font).setFontSize(10);
			} else {
				header.add(" ");
			}
			header.add(new Text(">").setFontColor(Color.BLACK));
		}
		header.addTabStops(new TabStop(765, TabAlignment.RIGHT));
		header.add(new Tab());
		header.add(new Text("PURCHASE ORDER"));

		Text pageNo = new Text(String.format("page %s of %s", perPage, totNoPage));
		Paragraph pageNoParagraph = new Paragraph().setHeight(13);
		pageNoParagraph.add(pageNo);
		document.add(pageNoParagraph.setFontSize(7).setTextAlignment(TextAlignment.RIGHT));
		document.add(header);

		Table topContent = new Table(new float[] { 61, 360, 20, 51, 168, 30, 75, 162 });
		topContent.setWidth(820);
		topContent.setHorizontalAlignment(HorizontalAlignment.CENTER);
		topContent.setVerticalAlignment(VerticalAlignment.MIDDLE);
		topContent.setFont(font);
		String[] headers = new String[] { "To", "Quote Ref", "Attention" };
		topContent.addCell(setHeaderColumn(headers, 0));
		topContent.addCell(getFirstColumn("Dated:"));
		Cell toSpace = new Cell();
		toSpace.add(new Paragraph("\n"));
		toSpace.setBorder(Border.NO_BORDER);
		topContent.addCell(toSpace);

		headers = new String[] { " ", "From", "Contact" };
		Cell fromHeader = setHeaderColumn(headers, 1);
		topContent.addCell(fromHeader);
		topContent.addCell(getSecondColumn());
		Cell fromSpace = new Cell();
		fromSpace.add(new Paragraph("\n"));
		fromSpace.setBorder(Border.NO_BORDER);
		topContent.addCell(fromSpace);

		headers = new String[] { "Order No", "Order Date", "Credit Terms", "Currency", "Order category", " ",
				"<PR Type> No" };
		Cell orderHeader = setHeaderColumn(headers, 0);
		orderHeader.setFontSize(9);
		topContent.addCell(orderHeader);
		Cell order = getThirdColumn(code128Image, creditTerms, currency, orderCategory, orderSubCategory, prCode,
				poNum);
		order.setFontSize(9);
		topContent.addCell(order);
		Cell orderSpace = new Cell();
		orderSpace.add(new Paragraph("\n"));
		orderSpace.setBorder(Border.NO_BORDER);
		topContent.addCell(orderSpace);
		document.add(topContent);
	}

	private Cell setHeaderColumn(String[] headers, int newLinePosition) {
		Cell headerCell = new Cell();
		headerCell.setBorder(Border.NO_BORDER);
		for (int i = 0; i < headers.length; i++) {
			Paragraph paragraph = new Paragraph();
			paragraph.add(headers[i]);
			paragraph.addTabStops(new TabStop(1000, TabAlignment.RIGHT));
			paragraph.add(new Tab());
			paragraph.add(new Text(":"));
			headerCell.add(paragraph);
			if (i == newLinePosition) {
				headerCell.add(new Paragraph("\n\n\n\n"));
			}
		}
		headerCell.setFontSize(9);

		return headerCell;
	}

	public Cell getFirstColumn(String quoteDateLabel) throws java.io.IOException {
		PdfFont font = PdfFontFactory.createFont(FontConstants.TIMES_ROMAN);
		Cell firstcell = new Cell();
		firstcell.setBorder(Border.NO_BORDER);
		firstcell.setTextAlignment(TextAlignment.JUSTIFIED);
		firstcell.setFont(font);
		if (StringUtils.isNotEmpty(purchaseOrder.getVendorName())) {
			firstcell.add(new Paragraph(purchaseOrder.getVendorName()));
		} else {
			LOGGER.error("company name is empty. ", null);
			firstcell.add(new Paragraph("\n"));
		}
		if (StringUtils.isNotEmpty(purchaseOrder.getVendorAddressLine1())) {
			firstcell.add(new Paragraph(purchaseOrder.getVendorAddressLine1()));
		} else {
			LOGGER.error("vendor address line 1 is empty. ", null);
			firstcell.add(new Paragraph("\n"));
		}
		if (StringUtils.isNotEmpty(purchaseOrder.getVendorAddressLine2())) {
			firstcell.add(new Paragraph(purchaseOrder.getVendorAddressLine2()));
		} else {
			LOGGER.error("vendor address line 2 is empty. ", null);
			firstcell.add(new Paragraph("\n"));
		}
		if (StringUtils.isNotEmpty(purchaseOrder.getVendorAddressLine3())) {
			firstcell.add(new Paragraph(purchaseOrder.getVendorAddressLine3()));
		} else {
			LOGGER.error("vendor address line 3 is empty. ", null);
			firstcell.add(new Paragraph("\n"));
		}
		if (StringUtils.isNotEmpty(purchaseOrder.getVendorPostCode())) {
			firstcell.add(new Paragraph(purchaseOrder.getVendorPostCode()));
		} else {
			LOGGER.error("vendor address post code is empty. ", null);
			firstcell.add(new Paragraph("\n"));
		}
		if (StringUtils.isNotEmpty(purchaseOrder.getVendorPostArea())) {
			firstcell.add(new Paragraph(purchaseOrder.getVendorPostArea()));
		} else {
			LOGGER.error("vendor post area is empty. ", null);
			firstcell.add(new Paragraph("\n"));
		}
		if (StringUtils.isNotEmpty(quoteDateLabel)) {
			firstcell.add(getQuoteRefObj(quoteDateLabel));
		} else {
			LOGGER.error("quotation refrence line is empty. ", null);
			firstcell.add(new Paragraph(" Dated:\n"));
		}
		if (StringUtils.isNotEmpty(purchaseOrder.getVendorPersonInCharge())) {
			firstcell.add(new Paragraph(purchaseOrder.getVendorPersonInCharge()));
		} else {
			LOGGER.error("person in charge is empty. ", null);
			firstcell.add(new Paragraph("\n"));
		}
		if (StringUtils.isNotEmpty(purchaseOrder.getVendorPersonInChargePhone())) {
			firstcell.add(new Paragraph(purchaseOrder.getVendorPersonInChargePhone()));
		} else {
			LOGGER.error("person in charge contact number is empty. ", null);
			firstcell.add(new Paragraph("\n"));
		}
		if (StringUtils.isNotEmpty(purchaseOrder.getVendorPersonInChargeEmail())) {
			firstcell.add(new Paragraph(purchaseOrder.getVendorPersonInChargeEmail()));
		} else {
			LOGGER.error("person in charge Email is empty. ", null);
			firstcell.add(new Paragraph("\n"));
		}
		firstcell.setFontSize(9);

		return firstcell;
	}

	private Paragraph getQuoteRefObj(String quoteDateLbl) {
		Paragraph quoteRef = new Paragraph();
		try {
			Text quoteDateLabel = new Text(quoteDateLbl);
			Text quoteDate = new Text("");
			if (purchaseOrder.getQuoteDate() != null) {
				SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
				quoteDate = new Text(sdf.format(purchaseOrder.getQuoteDate()));
			} else {
				LOGGER.error("quotation date is empty. ", null);
			}
			if (StringUtils.isNotEmpty(purchaseOrder.getQuoteNo())) {
				quoteRef.add(purchaseOrder.getQuoteNo());
			} else {
				LOGGER.error("quotation number is empty. ", null);
				quoteRef.add(new Text(" "));
			}
			quoteRef.add("," + " ");
			quoteRef.add(quoteDateLabel);
			quoteRef.add(" ");
			quoteRef.add(quoteDate);
		} catch (IllegalArgumentException e1) {
			LOGGER.error("quotation refrence illegal type ", e1);
		}
		return quoteRef;
	}

	public Cell getSecondColumn() {
		Cell secondcell = new Cell();
		secondcell.setBorder(Border.NO_BORDER);
		if (StringUtils.isNotEmpty(purchaseOrder.getPcCode())) {
			secondcell.add(new Paragraph(purchaseOrder.getPcCode()));
		} else {
			LOGGER.error("profit center code is empty. ", null);
			secondcell.add(new Paragraph("\n"));
		}
		if (StringUtils.isNotEmpty(purchaseOrder.getStoreName())) {
			secondcell.add(new Paragraph(purchaseOrder.getStoreName()));
		} else {
			LOGGER.error("store name is empty. ", null);
			secondcell.add(new Paragraph("\n"));
		}
		if (StringUtils.isNotEmpty(purchaseOrder.getDepartmentName())) {
			secondcell.add(new Paragraph(purchaseOrder.getDepartmentName()));
		} else {
			LOGGER.error("department name is empty. ", null);
			secondcell.add(new Paragraph("\n"));
		}
		if (StringUtils.isNotEmpty(purchaseOrder.getAddressLine1())) {
			secondcell.add(new Paragraph(purchaseOrder.getAddressLine1()));
		} else {
			LOGGER.error("profit center address line 1 is empty. ", null);
			secondcell.add(new Paragraph("\n"));
		}
		if (StringUtils.isNotEmpty(purchaseOrder.getAddressLine2())) {
			secondcell.add(new Paragraph(purchaseOrder.getAddressLine2()));
		} else {
			LOGGER.error("profit center address line 2 is empty. ", null);
			secondcell.add(new Paragraph("\n"));
		}
		if (StringUtils.isNotEmpty(purchaseOrder.getAddressLine3())) {
			secondcell.add(new Paragraph(purchaseOrder.getAddressLine3()));
		} else {
			LOGGER.error("profit center address line 3 is empty. ", null);
			secondcell.add(new Paragraph("\n"));
		}
		if (StringUtils.isNotEmpty(purchaseOrder.getPostCode())) {
			secondcell.add(new Paragraph(purchaseOrder.getPostCode()));
		} else {
			LOGGER.error("profit center post code is empty. ", null);
			secondcell.add(new Paragraph("\n"));
		}
		if (StringUtils.isNotEmpty(purchaseOrder.getPostArea())) {
			secondcell.add(new Paragraph(purchaseOrder.getPostArea()));
		} else {
			LOGGER.error("profit center post area is empty. ", null);
			secondcell.add(new Paragraph("\n"));
		}
		if (StringUtils.isNotEmpty(purchaseOrder.getContactPersonName())) {
			secondcell.add(new Paragraph(purchaseOrder.getContactPersonName()));
		} else {
			LOGGER.error("contact person name in second column is empty. ", null);
			secondcell.add(new Paragraph("\n"));
		}
		if (StringUtils.isNotEmpty(purchaseOrder.getContactPersonPhone())) {
			secondcell.add(new Paragraph(purchaseOrder.getContactPersonPhone()));
		} else {
			LOGGER.error("profit Center contact No in second column is empty. ", null);
			secondcell.add(new Paragraph("\n"));
		}
		if (StringUtils.isNotEmpty(purchaseOrder.getContactPersonEmail())) {
			secondcell.add(new Paragraph(purchaseOrder.getContactPersonEmail()));
		} else {
			LOGGER.error("profit center email is empty. ", null);
			secondcell.add(new Paragraph("\n"));
		}
		secondcell.setFontSize(9);

		return secondcell;
	}

	private Cell getThirdColumn(Image code128Image, String creditTerms, String currency, String orderCat,
			String orderSpace, String prCode, String poNum) {
		Cell thirdcell = new Cell();
		thirdcell.setBorder(Border.NO_BORDER);
		thirdcell.setFontSize(9);
		if (purchaseOrder.getStatus().equals("Authorized")) {
			thirdcell.add(
					(!poNum.isEmpty() || poNum != null) ? new Paragraph(poNum).setFontSize(14) : new Paragraph("\n"));
			thirdcell.add(code128Image);
		} else {
			try {
				thirdcell.add((!poNum.isEmpty() || poNum != null) ? new Paragraph(poNum) : new Paragraph("\n"));
			} catch (Exception e) {
				thirdcell.add(new Paragraph("\n"));
				LOGGER.error("po number is empty. ", e);
			}
			thirdcell.add("\n\n\n");
		}
		thirdcell.add("\n");
		try {
			if (purchaseOrder.getStatus().equals("Authorized")) {
				DateFormat df = new SimpleDateFormat("dd MMM YYYY");
				String orderDate = "";
				if (purchaseOrder.getAuthorizedOn() != null) {
					orderDate = df.format(purchaseOrder.getAuthorizedOn());
				}
				thirdcell.add(
						(!orderDate.isEmpty() || orderDate != null) ? new Paragraph(orderDate) : new Paragraph("\n"));
			} else {
				thirdcell.add(new Paragraph("\n"));
			}
		} catch (Exception e) {
			thirdcell.add(new Paragraph("\n"));
			LOGGER.error("order date is empty. ", e);
		}
		try {
			thirdcell.add(
					(!creditTerms.isEmpty() || creditTerms != null) ? new Paragraph(creditTerms) : new Paragraph("\n"));
		} catch (Exception e) {
			thirdcell.add(new Paragraph("\n"));
			LOGGER.error("credit terms is empty. ", e);
		}
		try {
			thirdcell.add((!currency.isEmpty() || currency != null) ? new Paragraph(currency) : new Paragraph("\n"));
		} catch (Exception e) {
			thirdcell.add(new Paragraph("\n"));
			LOGGER.error("currency is empty. ", e);
		}
		try {
			thirdcell.add((!orderCat.isEmpty() || orderCat != null) ? new Paragraph(orderCat) : new Paragraph("\n"));
		} catch (Exception e) {
			thirdcell.add(new Paragraph("\n"));
			LOGGER.error("order category is empty. ", e);
		}
		try {
			thirdcell.add(
					(!orderSpace.isEmpty() || orderSpace != null) ? new Paragraph(orderSpace) : new Paragraph("\n"));
		} catch (Exception e) {
			thirdcell.add(new Paragraph("\n"));
			LOGGER.error("order sub category is empty. ", e);
		}
		try {
			thirdcell.add((!prCode.isEmpty() || prCode != null) ? new Paragraph(prCode) : new Paragraph("\n"));
		} catch (Exception e) {
			thirdcell.add(new Paragraph("\n"));
			LOGGER.error("pr code is empty. ", e);
		}
		return thirdcell;
	}

	@Override
	protected String getHeaderTitle() {
		return "PDF Export";
	}
}