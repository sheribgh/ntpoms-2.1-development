package my.com.parkson.ntpoms.pomain.web.controllers;

import static my.com.parkson.ntpoms.common.MessageCodes.FAILED_PERSIST_EXCEPTION;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.common.LogMessages;
import my.com.parkson.ntpoms.common.entities.GoodsServicesTax;
import my.com.parkson.ntpoms.common.entities.ProfitCenter;
import my.com.parkson.ntpoms.common.model.Pager;
import my.com.parkson.ntpoms.common.services.GoodsServicesTaxService;
import my.com.parkson.ntpoms.common.services.ProfitCenterService;
import my.com.parkson.ntpoms.main.entities.ItemInformation;
import my.com.parkson.ntpoms.main.entities.ItemMap;
import my.com.parkson.ntpoms.main.entities.ItemsTotal;
import my.com.parkson.ntpoms.main.entities.PurchaseOrder;
import my.com.parkson.ntpoms.main.entities.VendorBranch;
import my.com.parkson.ntpoms.main.entities.VendorBranchId;
import my.com.parkson.ntpoms.main.repositories.ItemMapRepository;
import my.com.parkson.ntpoms.main.services.AccessLogsService;
import my.com.parkson.ntpoms.main.services.AuditsService;
import my.com.parkson.ntpoms.main.services.ItemInformationService;
import my.com.parkson.ntpoms.main.services.ItemMapService;
import my.com.parkson.ntpoms.main.services.MessagesService;
import my.com.parkson.ntpoms.main.services.PurchaseOrderService;
import my.com.parkson.ntpoms.main.services.VendorBranchService;
import my.com.parkson.ntpoms.pomain.web.models.ItemForm;
import my.com.parkson.ntpoms.pomain.web.models.ItemInfoForm;

@Slf4j
@Controller
@RequiredArgsConstructor
public class ItemFormController extends AbstractMainController {

	private static final Logger LOGGER = Logger.getLogger(ItemFormController.class);

	private static final int BUTTONS_TO_SHOW = 5;
	private static final int INITIAL_PAGE = 0;
	private static final int INITIAL_PAGE_SIZE = 5;
	private static final int[] PAGE_SIZES = { 5, 10, 20 };

	@Autowired
	private PurchaseOrderService poMainService;

	@Autowired
	private VendorBranchService vbService;

	@Autowired
	private ItemInformationService itemInfoRepo;

	@Autowired
	private ItemMapService itemMapRepo;

	@Autowired
	private ProfitCenterService pcrepo;

	@Autowired
	private GoodsServicesTaxService gstrepo;

	@Autowired
	private ItemMapRepository itemMapRepository;

	/** The messages. */
	@Autowired
	private MessagesService messages;

	@Autowired
	private AuditsService auditService;

	boolean edit = false;

	@RequestMapping(value = "/main/itemformgrid", method = RequestMethod.GET)
	public String itemGrid(@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page, @RequestParam("itemNo") Optional<String> itemNo,
			@RequestParam("particulars") Optional<String> particulars, @RequestParam("pcCode") Optional<String> pcCode,
			@RequestParam("packing") Optional<String> packing, @RequestParam("quantity") Optional<Double> quantity,
			@RequestParam("unitPriceExclGST") Optional<Double> unitPriceExclGST,
			@RequestParam("taxRate") Optional<Double> taxRate,
			@RequestParam("unitPriceInclGST") Optional<Double> unitPriceInclGST,
			@RequestParam("discount") Optional<Double> discount,
			@RequestParam("totalExclGST") Optional<Double> totalExclGST,
			@RequestParam("totalGST") Optional<Double> totalGST,
			@RequestParam("totalInclGST") Optional<Double> totalInclGST,
			@RequestParam("sortingkey") Optional<String> sortingkey,
			@RequestParam("sortingorder") Optional<String> sortingorder,
			@RequestParam("overAllDiscount") String overAllDiscount, @RequestParam("poNo") String poNo,
			@RequestParam("redirectto") Optional<String> redirectto, Model model) {
		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
		Page<ItemMap> list;
		Specification<ItemMap> s = new Specification<ItemMap>() {
			@Override
			public Predicate toPredicate(Root<ItemMap> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> ls = new ArrayList<>();
				List<Predicate> temp = new ArrayList<>();

				temp.add(cb.equal(root.get("poNo"), poNo));

				if (itemNo.isPresent() && !itemNo.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("itemNo")), "%" + itemNo.get().toLowerCase() + "%"));

				}

				if (particulars.isPresent() && !particulars.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("particulars")), "%" + particulars.get().toLowerCase() + "%"));

				}

				if (pcCode.isPresent() && !pcCode.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("pcCode")), "%" + pcCode.get().toLowerCase() + "%"));

				}

				if (packing.isPresent() && !packing.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("packing")), "%" + packing.get().toLowerCase() + "%"));

				}

				if (quantity.isPresent()) {
					ls.add(cb.equal(root.get("quantity"), quantity.get()));

				}

				if (unitPriceExclGST.isPresent()) {

					ls.add(cb.equal(root.get("unitPriceExclGST"), unitPriceExclGST.get()));
				}

				if (taxRate.isPresent()) {

					ls.add(cb.equal(root.get("taxRate"), taxRate.get()));
				}

				if (unitPriceInclGST.isPresent()) {

					ls.add(cb.equal(root.get("unitPriceInclGST"), unitPriceInclGST.get()));
				}

				if (discount.isPresent()) {

					ls.add(cb.equal(root.get("discount"), discount.get()));
				}

				if (totalExclGST.isPresent()) {

					ls.add(cb.equal(root.get("totalExclGST"), totalExclGST.get()));
				}

				if (totalGST.isPresent()) {

					ls.add(cb.equal(root.get("totalGST"), totalGST.get()));
				}

				if (totalInclGST.isPresent()) {

					ls.add(cb.equal(root.get("totalInclGST"), totalInclGST.get()));
				}

				ls.add(cb.or(temp.toArray(new Predicate[0])));

				return cb.and(ls.toArray(new Predicate[0]));
			}
		};

		Sort sorttemp = new Sort(Sort.Direction.DESC, "itemOrder");

		if (sortingkey.isPresent() && sortingorder.isPresent()) {
			sorttemp = new Sort(sortingorder.get().equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC,
					sortingkey.get());
		}
		if (poNo != null && poNo != "" || itemNo.isPresent() || particulars.isPresent() || pcCode.isPresent() ||

				packing.isPresent() || quantity.isPresent() || unitPriceExclGST.isPresent()
				|| unitPriceInclGST.isPresent() || taxRate.isPresent() || discount.isPresent()
				|| totalExclGST.isPresent() || totalGST.isPresent() || totalInclGST.isPresent()

		) {
			list = itemMapRepo.getAllPageableItemMap(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		} else {
			list = itemMapRepo.getAllPageableItemMap(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		}
		int serialid = 0;
		int pagesize = evalPage + 1;
		if (pagesize > 1) {
			int start = evalPageSize * pagesize;
			serialid = start - evalPageSize;
		}
		double disc = 0;
		if (!overAllDiscount.isEmpty()) {
			disc = Double.parseDouble(overAllDiscount);
		}
		
		PurchaseOrder poobjall = poMainService.getAllPurchaseOrderByPoNo(poNo);
		if(poobjall.getOveralldiscountpercent() > 0) {
			model.addAttribute("overAllDiscountPercentage", poobjall.getOveralldiscountpercent());
		}else {
			model.addAttribute("overAllDiscountPercentage", disc);
		}
		

		ItemsTotal itemtotal = getItemTotal(disc, poNo);
		
		Pager pager = new Pager(list.getTotalPages(), list.getNumber(), BUTTONS_TO_SHOW);
		model.addAttribute("serialid", serialid);
		model.addAttribute("list", list);
		model.addAttribute("selectedPageSize", evalPageSize);
		model.addAttribute("pageSizes", PAGE_SIZES);
		model.addAttribute("pager", pager);
		model.addAttribute("totExclGST", per(disc, itemtotal.getTotalexclgst()));
		model.addAttribute("totGST", per(disc, itemtotal.getTotalgst()));
		model.addAttribute("totInclGST", per(disc, itemtotal.getTotalinclgst()));
		model.addAttribute("overAllDiscount", per(disc, itemtotal.getTotaldiscount()));
		
		model.addAttribute("poNo", poNo);
		model.addAttribute("itemNo", itemNo.isPresent() ? itemNo.get() : "");
		model.addAttribute("particulars", particulars.isPresent() ? particulars.get() : "");
		model.addAttribute("pcCode", pcCode.isPresent() ? pcCode.get() : "");
		model.addAttribute("packing", packing.isPresent() ? packing.get() : "");
		model.addAttribute("quantity", quantity.isPresent() ? quantity.get() : "");
		model.addAttribute("unitPriceExclGST", unitPriceExclGST.isPresent() ? unitPriceExclGST.get() : "");
		model.addAttribute("unitPriceInclGST", unitPriceInclGST.isPresent() ? unitPriceInclGST.get() : "");
		model.addAttribute("taxRate", taxRate.isPresent() ? taxRate.get() : "");
		model.addAttribute("discount", discount.isPresent() ? discount.get() : "");
		model.addAttribute("totalExclGST", totalExclGST.isPresent() ? totalExclGST.get() : "");
		model.addAttribute("totalGST", totalGST.isPresent() ? totalGST.get() : "");
		model.addAttribute("totalInclGST", totalInclGST.isPresent() ? totalInclGST.get() : "");
		model.addAttribute("totalRecords", list.getTotalElements());
		model.addAttribute("sortingorder", sortingorder.isPresent() ? sortingorder.get() : "DESC");
		model.addAttribute("sortingkey", sortingkey.isPresent() ? sortingkey.get() : "itemOrder");
		if (redirectto.isPresent()) {
			return redirectto.get() + " :: result-table";
		}
		return "/main/createpo :: result-table";

	}

	/**
	 * Creates the vendor branch.
	 *
	 * @param obj
	 *            the obj
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @param redirectAttributes
	 *            the redirect attributes
	 * @return the string
	 */
	@PostMapping("/main/createitemform")
	public String createItem(@Valid @ModelAttribute("itemobj") ItemForm obj,
			@RequestParam("redirectto") String redirectto, BindingResult result, Model model,
			RedirectAttributes redirectAttributes) {
		MDC.put("sessionId",
				AbstractMainController.getCompany().isPresent() ? AbstractMainController.getCompany().get() : "");
		PurchaseOrder po = poMainService.getAllPurchaseOrderByPoNo(obj.getPoNo());
		ItemInformation iteminfo = new ItemInformation();
		iteminfo.setItemNo(obj.getItemNo());
		iteminfo.setParticulars(obj.getParticulars());
		iteminfo.setPacking(obj.getPacking());
		iteminfo.setVendorBranchCode(po.getVendorbranchcode());
		iteminfo.setVendorCode(po.getVendorcode());
		itemInfoRepo.createItemInformation(iteminfo);
		
		ItemMap itemmap = new ItemMap();
		
		
		if (obj.getCode() > 0) {
			itemmap.setCode(obj.getCode());
		} else {
			ItemMap item_obj = itemMapRepo.getLatestItemMapCode();
			int item_code = item_obj.getCode();
			itemmap.setCode(item_code + 1);
		}
		itemmap.setPoNo(obj.getPoNo());
		itemmap.setItemNo(obj.getItemNo());
		itemmap.setParticulars(obj.getParticulars());
		itemmap.setPacking(obj.getPacking());
		itemmap.setItemOrder(obj.getItemOrder());
		itemmap.setTaxcode(obj.getTaxCode());
		itemmap.setTaxRate(obj.getTaxRate());
		itemmap.setVariance(obj.getVariance());
		itemmap.setUnitPriceExclGST(obj.getUnitPriceExclGST());
		itemmap.setUnitPriceInclGST(obj.getUnitPriceInclGST());
		if (obj.getDiscount() != null) {
			itemmap.setDiscount(obj.getDiscount());
		}
		double unitPrice = obj.getUnitPriceExclGST();
		double quantity = obj.getQuantity();
		double totalunitprice = 0;
		totalunitprice = unitPrice * quantity;
		double totalDiscount = 0;
		double discount = 0;
		if (obj.getDiscount() != null) {
			discount = obj.getDiscount();
			totalDiscount = per(discount, totalunitprice);
			totalunitprice = totalunitprice - totalDiscount;
		} else {
			totalunitprice = unitPrice;
		}
		double tax = obj.getTaxRate();
		double gst = per(tax, totalunitprice);
		itemmap.setTotalExclGST(totalunitprice);
		itemmap.setTotalInclGST(totalunitprice + gst);
		itemmap.setTotalGST(gst);
		itemmap.setTotalDiscount(per(discount, unitPrice));
		itemmap.setQuantity(obj.getQuantity());
		itemmap.setUnitGST(obj.getTaxRate());
		String pccode = obj.getProfitCenter();
		itemmap.setPcCode(pccode.substring(0, 7));
		itemmap.setOldlst(false);
		try {
			itemMapRepo.createItemMap(itemmap);
			auditService.createAudit(new Date(), " New Item Map Created successfully", getCurrentUser().get(),
					" Item Map Creation", getCompany().get(), po.getPcCode());
			redirectAttributes.addFlashAttribute("info", "Item is added successfully");
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("info", e.getMessage());
			LOGGER.error(e);
			e.printStackTrace();
		}
		if (redirectto != null && !redirectto.isEmpty()) {
			return "redirect:" + redirectto;
		}
		return "redirect:/main/createpo";
	}

	@PostMapping("/main/createiteminfo")
	public String createItemInfo(@Valid @ModelAttribute("itemobj") ItemInfoForm obj,
			@RequestParam("redirectto") String redirectto, BindingResult result, Model model,
			RedirectAttributes redirectAttributes) {
		ItemInformation info = itemInfoRepo.getItemInformationByItemNo(obj.getIteminfo_itemno());
		PurchaseOrder po = poMainService.getAllPurchaseOrderByPoNo(obj.getIteminfo_vendorcode());
		MDC.put("sessionId",
				AbstractMainController.getCompany().isPresent() ? AbstractMainController.getCompany().get() : "");
		if (info == null && po != null) {
			ItemInformation iteminfo = new ItemInformation();
			iteminfo.setItemNo(obj.getIteminfo_itemno());
			iteminfo.setParticulars(obj.getIteminfo_particulars());
			iteminfo.setPacking(obj.getIteminfo_packing());
			iteminfo.setVendorBranchCode(po.getVendorbranchcode());
			iteminfo.setVendorCode(po.getVendorcode());
			try {
				itemInfoRepo.createItemInformation(iteminfo);
				auditService.createAudit(new Date(), " New Item Info Created successfully", getCurrentUser().get(),
						" Item Ino Creation", getCompany().get(), po.getPcCode());
				redirectAttributes.addFlashAttribute("info", "Item Info is added successfully");
			} catch (Exception e) {
				LOGGER.error(e);
			}
		} else {

			redirectAttributes.addFlashAttribute("error", "Item No already exists! / PO not exists");
		}
		if (redirectto != null && !redirectto.isEmpty()) {
			return "redirect:" + redirectto;
		}
		return "redirect:/main/createpo";
	}

	@GetMapping(value = "/main/itemform/deleteItem/{deleteNo}")
	public String deleteItem(@PathVariable Integer deleteNo, Model model, RedirectAttributes redirectAttributes) {
		ItemMap item = itemMapRepo.getItemMapByCode(deleteNo);
		MDC.put("sessionId",
				AbstractMainController.getCompany().isPresent() ? AbstractMainController.getCompany().get() : "");
		if (item != null) {
			try {
				itemMapRepository.delete(item);
				auditService.createAudit(new Date(), " Item Map: " + item.getItemNo() + " Deleted successfully",
						getCurrentUser().get(), " Item Map Deletion", getCompany().get(), item.getPcCode());
				redirectAttributes.addFlashAttribute("info", "Item was successfully deleted.");
			} catch (Exception e) {
				LOGGER.error(e);
			}
		} else {
			redirectAttributes.addFlashAttribute("info", "Item Object not exists");
		}
		return "redirect:/main/createpo";
	}

	@GetMapping(value = "/main/itemform/deleteItem_edit_mode/{deleteNo}")
	public String deleteItem_edit(@PathVariable Integer deleteNo, Model model, RedirectAttributes redirectAttributes) {
		ItemMap item = itemMapRepo.getItemMapByCode(deleteNo);
		MDC.put("sessionId",
				AbstractMainController.getCompany().isPresent() ? AbstractMainController.getCompany().get() : "");
		if (item != null) {
			try {
				itemMapRepository.delete(item);
				auditService.createAudit(new Date(), " Item Map: " + item.getItemNo() + " Deleted successfully",
						getCurrentUser().get(), " Item Map Deletion", getCompany().get(), item.getPcCode());
				redirectAttributes.addFlashAttribute("info", "Item was successfully deleted.");
			} catch (Exception e) {
				LOGGER.error(e);
			}
		} else {
			redirectAttributes.addFlashAttribute("info", "Item Object not exists");
		}
		return "redirect:/main/edit_draftpo";
	}

	/**
	 * Updates Vendor Branch.
	 *
	 * @param code
	 *            the code
	 * @param model
	 *            the model
	 * @return the string
	 */

	@RequestMapping(value = "/itemform/existingvendor", method = RequestMethod.GET)
	public @ResponseBody VendorBranch checkVendorBranch(@RequestParam("vendorcode") Optional<String> code,
			@RequestParam("vendorbranchcode") Optional<String> vendorBranchCode, RedirectAttributes redirectAttributes,
			Model model) {
		VendorBranchId pkey = new VendorBranchId(code.get(), vendorBranchCode.get(), getCompany().get());
		VendorBranch vendorBranch = vbService.getVendorBranchbyVendorId(pkey);
		return vendorBranch;

	}

	@RequestMapping(value = "/itemform/getItem", method = RequestMethod.GET)
	public @ResponseBody ItemInformation getIItemInfoByItemNo(@RequestParam("itemNo") String itemNo) {
		return itemInfoRepo.getItemInformationByItemNo(itemNo);
	}

	@RequestMapping(value = "/itemform/savePOitemInfo", method = RequestMethod.GET)
	public @ResponseBody String savePOitemInfo(@RequestParam("poNo") String poNo,
			@RequestParam("totExclGST") String totExclGST, @RequestParam("totGST") String totGST,
			@RequestParam("totInclGST") String totInclGST, @RequestParam("overAllDiscount") String overAllDiscount,
			@RequestParam("overalldiscountpercent") String overAllDiscountPercent) {
		PurchaseOrder po = poMainService.getAllPurchaseOrderByPoNo(poNo);
		MDC.put("sessionId",
				AbstractMainController.getCompany().isPresent() ? AbstractMainController.getCompany().get() : "");
		if (po != null) {
			po.setTotalExclGST(Double.parseDouble(totExclGST));
			po.setTotalGST(Double.parseDouble(totGST));
			po.setTotalInclGST(Double.parseDouble(totInclGST));
			po.setOverAllDiscount(Double.parseDouble(overAllDiscount));
			po.setOveralldiscountpercent(Double.parseDouble(overAllDiscountPercent));
			try {
				poMainService.createPurchaseOrder(po);
				auditService.createAudit(new Date(), " Item Info Added to PO: " + po.getPoNo() + " successfully",
						getCurrentUser().get(), " Item Info Added to PO", getCompany().get(), po.getPcCode());
			} catch (Exception e) {
				LOGGER.error(e);
			}
		}
		return "OK";
	}

	@RequestMapping(value = "/itemform/getItemNo", method = RequestMethod.GET)
	public @ResponseBody int getIItemInfoByItemNoMax(@RequestParam("pccode") String pccode) {
		return getItemOrder("");
	}

	@RequestMapping(value = "/itemform/getItemMap", method = RequestMethod.GET)
	public @ResponseBody ItemMap getIItemMapByCode(@RequestParam("code") int code) {
		ItemMap item = itemMapRepo.getItemMapByCode(code);
		ProfitCenter pc = pcrepo.getProfitCenterByCode(item.getPcCode());
		item.setPcCode(item.getPcCode() + "-" + pc.getName());
		return item;
	}

	@RequestMapping(value = "/itemform/getGST", method = RequestMethod.GET)
	public @ResponseBody GoodsServicesTax getGSTbyTaxCode(@RequestParam("taxCode") String taxCode) {
		return gstrepo.getGoodsServicesTaxByCode(taxCode);
	}

	private ItemsTotal getItemTotal(double discount, String poNo) {
		ItemsTotal total = new ItemsTotal();
		if (discount == 0) {
			total.setTotaldiscount(0.00);
			total.setTotalexclgst(0.00);
			total.setTotalgst(0.00);
			total.setTotalinclgst(0.00);
			List<ItemMap> list = itemMapRepo.getAllItemMapByPurchaseOrderNo(poNo);
			if (list != null) {
				for (ItemMap e : list) {
					total.setTotaldiscount(total.getTotaldiscount() + e.getDiscount());
					total.setTotalexclgst(total.getTotalexclgst() + e.getTotalExclGST());
					total.setTotalgst(total.getTotalgst() + e.getTotalGST());
					total.setTotalinclgst(total.getTotalinclgst() + e.getTotalInclGST());
				}
			}
		} else {
			total.setTotaldiscount(0.00);
			total.setTotalexclgst(0.00);
			total.setTotalgst(0.00);
			total.setTotalinclgst(0.00);
			List<ItemMap> list = itemMapRepo.getAllItemMapByPurchaseOrderNo(poNo);
			if (list != null) {
				for (ItemMap e : list) {
					total.setTotaldiscount(total.getTotaldiscount() + e.getDiscount());
					total.setTotalexclgst(total.getTotalexclgst() + e.getTotalExclGST());
					total.setTotalgst(total.getTotalgst() + e.getTotalGST());
					total.setTotalinclgst(total.getTotalinclgst() + e.getTotalInclGST());
				}
			}
		}
		return total;
	}

	private double per(double discount, double amount) {
		if (discount != 0) {
			double per = 0;
			per = discount * amount;
			double percentage = per / 100;
			return percentage;
		} else {
			return amount;
		}
	}

	private int getItemOrder(String poNo) {
		return itemMapRepo.getAllItemMapByPurchaseOrderNo(poNo).size() + 1;
	}

	@Override
	protected String getHeaderTitle() {
		return "Manage Vendor Branch";
	}
	
	
	@RequestMapping(value = "/itemform/overallpercentagesave", method = RequestMethod.GET)
	public @ResponseBody Double overallpercentagesave(@RequestParam("overAllDiscountPercentage") Double percentage,@RequestParam("poNo") String poNo,Model model) {
		PurchaseOrder po = poMainService.getAllPurchaseOrderByPoNo(poNo);
		po.setOveralldiscountpercent(percentage);
		poMainService.createPurchaseOrder(po);
		return percentage;
	}

}
