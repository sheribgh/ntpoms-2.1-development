package my.com.parkson.ntpoms.pomain.login;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

public class CustomAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

	public static final String SPRING_SECURITY_FORM_COMPANY_KEY = "company";

	public static final String SPRING_SECURITY_FORM_YEAR_KEY = "year";

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException {

		if (!request.getMethod().equals("POST")) {
			throw new AuthenticationServiceException("Authentication method not supported: " + request.getMethod());
		}

		CustomAuthenticationToken authRequest = getAuthRequest(request);
		setDetails(request, authRequest);
		return this.getAuthenticationManager().authenticate(authRequest);
	}

	private CustomAuthenticationToken getAuthRequest(HttpServletRequest request) {
		String username = obtainUsername(request);
		String password = obtainPassword(request);
		String company = obtainCompany(request);
		String year = obtainYear(request);

		if (username == null) {
			username = "";
		}
		if (password == null) {
			password = "";
		}
		if (company == null) {
			company = "";
		}
		if (year == null) {
			year = "";
		}

		return new CustomAuthenticationToken(username, password, company, year);
	}

	private String obtainCompany(HttpServletRequest request) {
		return request.getParameter(SPRING_SECURITY_FORM_COMPANY_KEY);
	}

	private String obtainYear(HttpServletRequest request) {
		return request.getParameter(SPRING_SECURITY_FORM_YEAR_KEY);
	}
}
