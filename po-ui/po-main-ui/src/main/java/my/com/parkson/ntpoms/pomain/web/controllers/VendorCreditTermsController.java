package my.com.parkson.ntpoms.pomain.web.controllers;

import static my.com.parkson.ntpoms.common.MessageCodes.FAILED_FETCH_RECORDS_EXCEPTION;
import static my.com.parkson.ntpoms.common.MessageCodes.FAILED_PERSIST_EXCEPTION;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.LogMessages;
import my.com.parkson.ntpoms.common.entities.SystemParameter;
import my.com.parkson.ntpoms.common.services.SystemParameterService;
import my.com.parkson.ntpoms.main.entities.CreditTerms;
import my.com.parkson.ntpoms.main.entities.CreditTermsId;
import my.com.parkson.ntpoms.main.entities.VendorBranch;
import my.com.parkson.ntpoms.main.entities.VendorBranchId;
import my.com.parkson.ntpoms.main.entities.VendorCreditTerms;
import my.com.parkson.ntpoms.main.entities.VendorCreditTermsId;
import my.com.parkson.ntpoms.main.services.AccessLogsService;
import my.com.parkson.ntpoms.main.services.AuditsService;
import my.com.parkson.ntpoms.main.services.CreditTermsService;
import my.com.parkson.ntpoms.main.services.MessagesService;
import my.com.parkson.ntpoms.main.services.VendorBranchService;
import my.com.parkson.ntpoms.main.services.VendorCreditTermsService;
import my.com.parkson.ntpoms.pomain.web.models.AutocompleteForm;
import my.com.parkson.ntpoms.pomain.web.models.Pager;
import my.com.parkson.ntpoms.pomain.web.models.VendorCrtForm;

@Controller
public class VendorCreditTermsController extends AbstractMainController {

	private static final int BUTTONS_TO_SHOW = 5;
	private static final int INITIAL_PAGE = 0;
	private static final int INITIAL_PAGE_SIZE = 5;
	private static final int[] PAGE_SIZES = { 5, 10, 20 };

	/** The messages. */
	@Autowired
	private MessagesService messages;

	@Autowired
	private SystemParameterService sysRepo;

	@Autowired
	private AuditsService auditService;

	@Autowired
	private VendorCreditTermsService vcrService;

	@Autowired
	private VendorBranchService vbService;

	private static final Logger LOGGER = Logger.getLogger(VendorCreditTermsController.class);
	@Autowired
	private CreditTermsService crService;

	private VendorCrtForm tempobj;

	boolean edit = false;

	@GetMapping("/main/vendorcreditterms")
	public ModelAndView onPageLoadVendCRT() {
		ModelAndView modelAndView = new ModelAndView("/main/vendorcreditterms");

		modelAndView.getModelMap().addAttribute("vcrobj", tempobj != null ? tempobj : new VendorCrtForm());
		modelAndView.addObject("vendorbranchlist", getVendorCodeandBranchCodeandName());
		modelAndView.addObject("creditlist", getCrtCodeandDescription());

		return modelAndView;
	}

	@RequestMapping(value = "/main/vendorcredittermsgrid", method = RequestMethod.GET)
	public String VcrTermsgrid(@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page, @RequestParam("vendorCode") Optional<String> vendorcode,
			@RequestParam("vendorBranchCode") Optional<String> vendorBranchCode,
			@RequestParam("creditTermsCode") Optional<String> crtcode,
			@RequestParam("companyCode") Optional<String> compCode, @RequestParam("status") Optional<String> isActive,
			@RequestParam("sortingkey") Optional<String> sortingkey,
			@RequestParam("sortingorder") Optional<String> sortingorder, Model model) {

		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
		Page<VendorCreditTerms> list;

		Specification<VendorCreditTerms> s = new Specification<VendorCreditTerms>() {
			@Override
			public Predicate toPredicate(Root<VendorCreditTerms> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> ls = new ArrayList<>();
				if (vendorcode.isPresent() && !vendorcode.get().isEmpty()) {

					ls.add(cb.like(cb.lower(root.get("vendcrtId").get("vendorCode")),
							"%" + vendorcode.get().toLowerCase() + "%"));
				}

				if (compCode.isPresent() && !compCode.get().isEmpty()) {

					ls.add(cb.like(cb.lower(root.get("vendcrtId").get("companyCode")),
							"%" + compCode.get().toLowerCase() + "%"));
				}

				if (vendorBranchCode.isPresent() && !vendorBranchCode.get().isEmpty()) {

					ls.add(cb.like(cb.lower(root.get("vendcrtId").get("vendorBranchCode")),
							"%" + vendorBranchCode.get().toLowerCase() + "%"));
				}

				if (crtcode.isPresent() && !crtcode.get().isEmpty()) {

					ls.add(cb.like(cb.lower(root.get("vendcrtId").get("creditTermsCode")),
							"%" + crtcode.get().toLowerCase() + "%"));
				}

				if (isActive.isPresent() && !isActive.get().isEmpty() && !isActive.get().equals("All")) {
					ls.add(cb.equal(root.get("isActive"), "Active".equals(isActive.get())));
				}
				return cb.and(ls.toArray(new Predicate[0]));
			}
		};

		Sort sorttemp = new Sort(Sort.Direction.DESC, "lastModifiedOn");

		if (sortingkey.isPresent() && sortingorder.isPresent()) {
			sorttemp = new Sort(sortingorder.get().equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC,
					sortingkey.get());
		}

		if (vendorcode.isPresent() || compCode.isPresent() || vendorBranchCode.isPresent() || crtcode.isPresent()
				|| isActive.isPresent()) {
			list = vcrService.getPagebleAll(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		} else {
			list = vcrService.getAllPageableVendCrtOrdered(new PageRequest(evalPage, evalPageSize, sorttemp));
		}

		int serialid = 0;
		int pagesize = evalPage + 1;
		if (pagesize > 1) {
			int start = evalPageSize * pagesize;
			serialid = start - evalPageSize;
		}

		Pager pager = new Pager(list.getTotalPages(), list.getNumber(), BUTTONS_TO_SHOW);
		model.addAttribute("serialid", serialid);
		model.addAttribute("list", list);
		model.addAttribute("selectedPageSize", evalPageSize);
		model.addAttribute("pageSizes", PAGE_SIZES);
		model.addAttribute("pager", pager);
		model.addAttribute("totalRecords", list.getTotalElements());
		model.addAttribute("vendorCode", vendorcode.isPresent() ? vendorcode.get() : "");
		model.addAttribute("vendorBranchCode", vendorBranchCode.isPresent() ? vendorBranchCode.get() : "");
		model.addAttribute("companyCode", compCode.isPresent() ? compCode.get() : "");
		model.addAttribute("creditTermsCode", crtcode.isPresent() ? crtcode.get() : "");
		model.addAttribute("status", isActive.isPresent() ? isActive.get() : "");
		model.addAttribute("sortingorder", sortingorder.isPresent() ? sortingorder.get() : "DESC");
		model.addAttribute("sortingkey", sortingkey.isPresent() ? sortingkey.get() : "lastModifiedOn");

		return "/main/vendorcreditterms :: result-table";
	}

	private List<String> getVendorCodeandBranchCodeandName() {
		List<String> lis = new ArrayList<>();
		List<VendorBranch> temp = vbService.getAllActiveVendorBranch();
		if (temp != null) {
			for (VendorBranch e : temp) {
				StringBuilder s = new StringBuilder();
				s.append(e.getVendorBranchId().getCode());
				s.append("-");
				s.append(e.getName());
				s.append("-");
				s.append(e.getVendorBranchId().getBranchCode());
				lis.add(s.toString());
			}
		}
		return lis;
	}
	
	private List<String> getCrtCodeandDescription() {
		List<String> lis = new ArrayList<>();
		List<CreditTerms> temp = crService.getAllActiveCreditTerms();
		if (temp != null) {
			for (CreditTerms e : temp) {
				StringBuilder s = new StringBuilder();
				s.append(e.getCrtId().getCode());
				s.append("-");
				s.append(e.getDescription());
				lis.add(s.toString());
			}
		}
		return lis;
	}

	@PostMapping(value = "/main/createvendorcreditterms")
	public String createCreditTerms(@Valid @ModelAttribute("vcrobj") VendorCrtForm obj, BindingResult result,
			Model model, RedirectAttributes redirectAttributes) {

		MDC.put("sessionId",
				AbstractMainController.getCompany().isPresent() ? AbstractMainController.getCompany().get() : "");
		
		HashMap<String, String> vend_info_Map = new HashMap<String, String>();
		VendorCreditTermsId priKey = new VendorCreditTermsId();
		
		try{
		if(obj.getVendorbranchcode()!=null){
		vend_info_Map = extract_vendinfo(obj.getVendorbranchcode());
		}
		
		if(vend_info_Map.size()>0){
		 priKey = new VendorCreditTermsId(vend_info_Map.get("vendorcode"), vend_info_Map.get("vendorbranchcode"),
				obj.getCredittermscode(), getCompany().get());
		}
		}catch(Exception e){
			LOGGER.error(e);
			
		}

		if (!result.hasErrors() && obj.getCredittermscode() != null && priKey.getVendorCode() != null && priKey.getVendorBranchCode() != null
				&& vcrService.getVendorCreditTermsById(priKey) == null) {
			VendorCreditTerms ad = new VendorCreditTerms();
			VendorCreditTermsId adpk = new VendorCreditTermsId(priKey.getVendorCode(), priKey.getVendorBranchCode(),
					obj.getCredittermscode(), getCompany().get());
			ad.setCreatedby(getCurrentUser().get());
			ad.setCreatedon(new Date());
			ad.setLastmodifiedby(getCurrentUser().get());
			ad.setLastModifiedOn(new Date());
			ad.setActive(true);
			ad.setVendcrtId(adpk);

			try {
				vcrService.createVendorCreditTerms(ad);
				auditService.createAudit(new Date(),
						" Vendor Credit Terms: " + obj.getVendorcode() + " created successfully. ",
						getCurrentUser().get(), " Create Vendor Credit Terms", getCompany().get(), "");
				redirectAttributes.addFlashAttribute("info", "Vendor Credit Terms created successfully");
				tempobj = null;
			} catch (POException e) {
				LOGGER.error(e);
			} catch (Exception e) {
				LOGGER.error(e);
			}
		} else {
			redirectAttributes.addFlashAttribute("error", "Vendor Credit Terms code already exists");
			tempobj = obj;
		}
		return "redirect:/main/vendorcreditterms";
	}

	@Override
	protected String getHeaderTitle() {
		return "Manage Vendor Credit Terms";
	}

	@PostMapping("/main/updatevendorcreditterms")
	public String updateVcrTerms(@Valid @ModelAttribute("vcrobj") VendorCrtForm obj, BindingResult result, Model model,
			RedirectAttributes redirectAttributes) {
		MDC.put("sessionId",
				AbstractMainController.getCompany().isPresent() ? AbstractMainController.getCompany().get() : "");
		VendorCreditTermsId priKey = new VendorCreditTermsId(obj.getVendorcode(), obj.getVendorbranchcode(),
				obj.getCredittermscode(), getCompany().get());

		if (!result.hasErrors()) {

			VendorCreditTerms ad = vcrService.getVendorCreditTermsById(priKey);
			boolean isActive;
			try {
				if (ad != null) {
					ad.setLastmodifiedby(getCurrentUser().get());
					ad.setLastModifiedOn(new Date());
					if (obj.getIsActive().contains("InActive")) {
						isActive = false;
					} else {
						isActive = true;
					}

					if (isActive != ad.isActive()) {

						if (obj.getIsActive().contains("InActive")) {
							ad.setDeactivatedby(getCurrentUser().get());
							ad.setDeactivatedon(new Date());
						} else {
							ad.setReactivatedon(new Date());
							ad.setReactivatedby(getCurrentUser().get());
						}
					}
					ad.setActive("Active".equals(obj.getIsActive()));
					ad.setVendcrtId(priKey);

					try {
						vcrService.createVendorCreditTerms(ad);
						auditService.createAudit(new Date(),
								" Vendor Credit Terms: " + obj.getVendorcode() + " updated successfully. ",
								getCurrentUser().get(), " Update Vendor Credit Terms"

								, getCompany().get(), "");

						redirectAttributes.addFlashAttribute("info", "Vendor Credit Terms updated successfully");
					} catch (POException e) {
						LOGGER.error(e);
					} catch (Exception e) {
						LOGGER.error(e);
					}
				} else {
					redirectAttributes.addFlashAttribute("error", "Vendor Credit Terms Object not exists");
					return "/main/edit_VendorCreditTerms";
				}
			} catch (Exception e) {
				LOGGER.error(e);
			}
		}
		return "redirect:/main/vendorcreditterms";

	}

	@GetMapping(value = "/main/editvcrterms")
	public String editVendorCreditTerms(@RequestParam("code") Optional<String> code,
			@RequestParam("vendorBranchCode") Optional<String> vendorBranchCode,
			@RequestParam("creditterm") Optional<String> creditterm, Model model,
			RedirectAttributes redirectAttributes) {
		VendorCreditTermsId priKey = new VendorCreditTermsId(code.get(), vendorBranchCode.get(), creditterm.get(),
				getCompany().get());

		VendorCreditTerms ad = vcrService.getVendorCreditTermsById(priKey);

		VendorBranchId pkey = new VendorBranchId(code.get(), vendorBranchCode.get(), getCompany().get());
		VendorBranch vendorBranch = vbService.getVendorBranchbyVendorId(pkey);

		edit = true;
		if (ad != null) {
			VendorCrtForm obj = new VendorCrtForm();
			String date_format = "dd-MM-yyyy hh:mm:ss";
			SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_TIME_FORMAT");
			if (dateObj != null) {
				if (!dateObj.getPropValue().isEmpty()) {
					date_format = dateObj.getPropValue();
				}
			}
			DateFormat formatter = new SimpleDateFormat(date_format);
			String createdOn = "";
			String modifiedOn = "";
			String reactivatedOn = "";
			String deactivatedOn = "";

			if (ad.getCreatedon() != null) {
				createdOn = formatter.format(ad.getCreatedon());
			}
			if (ad.getLastModifiedOn() != null) {
				modifiedOn = formatter.format(ad.getLastModifiedOn());
			}
			if (ad.getReactivatedon() != null) {
				reactivatedOn = formatter.format(ad.getReactivatedon());
			}
			if (ad.getDeactivatedon() != null) {
				deactivatedOn = formatter.format(ad.getDeactivatedon());
			}
			obj.setCreatedon(createdOn);
			obj.setDeactivatedon(deactivatedOn);
			obj.setReactivatedon(reactivatedOn);
			obj.setLastmodifiedon(modifiedOn);
			obj.setVendorcode(ad.getVendcrtId().getVendorCode());
			obj.setVendorbranchcode(ad.getVendcrtId().getVendorBranchCode());
			if (vendorBranch != null) {
				obj.setVendCodevendBCode(ad.getVendcrtId().getVendorCode() + "-" + vendorBranch.getName() + "-"
						+ ad.getVendcrtId().getVendorBranchCode());
			}
			obj.setCredittermscode(ad.getVendcrtId().getCreditTermsCode());
			obj.setcompCode(getCompany().get());
			obj.setCreatedby(ad.getCreatedby());
			obj.setLastmodifiedby(ad.getLastmodifiedby());
			obj.setReactivatedby(ad.getReactivatedby());
			obj.setDeactivatedby(ad.getDeactivatedby());
			obj.setIsActive(ad.isActive() ? "Active" : "InActive");
			model.addAttribute("vcrobj", obj);
			model.addAttribute("edit", edit);
			return "/main/edit_vendorcreditterms";
		} else {
			redirectAttributes.addFlashAttribute("error", "Vendor Credit Terms Object not exists");
			return "redirect:/main/vendorcreditterms";
		}

	}

	@RequestMapping(value = "/api/existingvendorcrt", method = RequestMethod.GET)
	public @ResponseBody VendorCreditTerms checkVendorBranch(@RequestParam("vendorcode") Optional<String> code,
			@RequestParam("crt") Optional<String> crt, RedirectAttributes redirectAttributes, Model model) {
		
		HashMap<String, String> vend_info_Map = new HashMap<String, String>();
		if(code.get()!=null && crt.get()!=null){
			vend_info_Map = extract_vendinfo(code.get());
			VendorCreditTermsId pkey = new VendorCreditTermsId(vend_info_Map.get("vendorcode"), vend_info_Map.get("vendorbranchcode"), crt.get(), getCompany().get());
			VendorCreditTerms vendorCrt = vcrService.getVendorCreditTermsById(pkey);
			
			if(vendorCrt!=null){
			return vendorCrt;
			}
		}

		

		return new VendorCreditTerms();

	}
	
	public HashMap<String, String> extract_vendinfo(String vendorcode) {

		if(vendorcode!=null){
		int last_index = vendorcode.lastIndexOf("-");
		int first_index = vendorcode.indexOf("-");
		
		String vendCode_sub = vendorcode.substring(0, first_index);
		String vendBCode_sub = vendorcode.substring(last_index+1);
		String vendName_sub = vendorcode.substring(first_index+1, last_index);
		
		HashMap<String, String> vend_info = new HashMap<String, String>();
		vend_info.put("vendorcode",vendCode_sub );
		vend_info.put("vendorbranchcode", vendBCode_sub);
		vend_info.put("vendorname", vendName_sub);
		return vend_info;
		}
		
		return null;
		
	}
	
	/**
	 * 
	 * @param searchval
	 * @param redirectAttributes
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/api/vendorbranchautocomplete", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody List<AutocompleteForm> vendorbranchsearch(@RequestParam("query") String searchval) {
		
		try{
		
		Specification<VendorBranch> sp = new Specification<VendorBranch>() {
			@Override
			public Predicate toPredicate(Root<VendorBranch> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> ls = new ArrayList<>();
				List<Predicate> templs = new ArrayList<>();
				if (searchval != null && !searchval.isEmpty()) {
				ls.add(cb.like(cb.lower(root.get("vendorBranchId").get("code")),
						"%" + searchval.toLowerCase() + "%"));
				ls.add(cb.like(cb.lower(root.get("vendorBranchId").get("branchCode")),
						"%" + searchval.toLowerCase() + "%"));
				ls.add(cb.like(cb.lower(root.get("name")), "%" + searchval.toLowerCase() + "%"));
				templs.add(cb.or(ls.toArray(new Predicate[0])));
				}
				
				templs.add(cb.equal(root.get("isActive"), true));
				if(getCompany().isPresent()){
				templs.add(cb.like(cb.lower(root.get("vendorBranchId").get("companyCode")),
						"%" + getCompany().get()+ "%"));
				}
				return cb.and(templs.toArray(new Predicate[0]));
			}
		};
		Page<VendorBranch> list;
		if (searchval != null && !searchval.isEmpty()) {
			list = vbService.getAllPageableVendorBranch(sp, new PageRequest(0, 20));
			
		} else {
			list = vbService.getAllPageableVendorBranch(new PageRequest(0, 20));
		}
		List<AutocompleteForm> temp = new ArrayList<>();
		
		for (VendorBranch e : list.getContent()) {
			AutocompleteForm a = new AutocompleteForm();
			StringBuilder s = new StringBuilder();
			s.append(e.getVendorBranchId().getCode());
			s.append("-");
			s.append(e.getName());
			s.append("-");
			s.append(e.getVendorBranchId().getBranchCode());
			a.setId(s.toString());
			a.setName(s.toString());
			temp.add(a);
		}
		return temp;
		}catch(Exception e){
			LOGGER.error(e);
		}
		
		return new ArrayList<>();
	}

}
