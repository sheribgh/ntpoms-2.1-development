/*
 * ********************************************************************************
 * Copyright (c) 2018 - Brentin Services, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.brent-in.com
 * @Copyright  : Copyright 2018 Brentin Services, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.pomain.security;

import javax.servlet.http.HttpSessionListener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.header.writers.frameoptions.XFrameOptionsHeaderWriter;
import org.springframework.security.web.session.ConcurrentSessionFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import my.com.parkson.ntpoms.common.security.CustomConcurrentSessionFilter;
import my.com.parkson.ntpoms.common.security.CustomHttpSessionEventPublisher;
import my.com.parkson.ntpoms.common.security.CustomSimpleUrlAuthenticationFailureHandler;
import my.com.parkson.ntpoms.pomain.login.CustomAuthenticationFilter;
import my.com.parkson.ntpoms.pomain.login.CustomUserDetailsAuthenticationProvider;

@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private my.com.parkson.ntpoms.pomain.login.CustomUserDetailsService userDetailsService;

	@Autowired
	private Environment env;

	@Autowired
	private EmployeeAuthenticationSuccessHandler successHandler;

	@Autowired
	private CustomizeLogoutSuccessHandler customizeLogoutSuccessHandler;

	private static String LOGIN = "/login";
	private static String LOGOUT = "/logout";
	private static String LOGIN_INVALID_SESSION_URL = "/login?invalidSessionUrl";
	private static String ACCESS_DENIED = "/403";

	private static final String[] UNSECURED_RESOURCE_LIST = new String[] { LOGIN, LOGIN_INVALID_SESSION_URL,
			ACCESS_DENIED };

	/**
	 * Instantiates a new web security config.
	 *
	 * @param customUserDetailsService
	 *            the custom user details service
	 */
	public WebSecurityConfig(my.com.parkson.ntpoms.pomain.login.CustomUserDetailsService customUserDetailsService) {
		super();
		this.userDetailsService = customUserDetailsService;
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.addFilterBefore(customConcurrentSessionFilter(), ConcurrentSessionFilter.class)
				.addFilterBefore(authenticationFilter(), UsernamePasswordAuthenticationFilter.class).csrf().and()
				.headers()
				.addHeaderWriter(new XFrameOptionsHeaderWriter(XFrameOptionsHeaderWriter.XFrameOptionsMode.SAMEORIGIN))
				.xssProtection();
		http.authorizeRequests().antMatchers(UNSECURED_RESOURCE_LIST).permitAll()
				.antMatchers("/resources/**", "/webjars/**", "/assets/**", "/api/**", "/loginredirect/**").permitAll().anyRequest()
				.authenticated().and().formLogin().loginPage(LOGIN).successHandler(successHandler)
				.failureHandler(customSimpleUrlAuthenticationFailureHandler()).permitAll().and().logout()
				.logoutSuccessHandler(customizeLogoutSuccessHandler)
				.logoutRequestMatcher(new AntPathRequestMatcher(LOGOUT)).invalidateHttpSession(false).permitAll().and()
				.sessionManagement().sessionFixation().migrateSession().invalidSessionUrl(LOGIN_INVALID_SESSION_URL)
				.maximumSessions(1).maxSessionsPreventsLogin(false);
		http.exceptionHandling().accessDeniedPage(ACCESS_DENIED);
	}

	public CustomAuthenticationFilter authenticationFilter() throws Exception {
		CustomAuthenticationFilter filter = new CustomAuthenticationFilter();
		filter.setAuthenticationManager(authenticationManagerBean());
		filter.setAuthenticationFailureHandler(customSimpleUrlAuthenticationFailureHandler());
		return filter;
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(authProvider());
	}

	public AuthenticationProvider authProvider() {
		CustomUserDetailsAuthenticationProvider provider = new CustomUserDetailsAuthenticationProvider(
				passwordEncoder(), userDetailsService);
		return provider;
	}

	public SimpleUrlAuthenticationFailureHandler failureHandler() {
		return new SimpleUrlAuthenticationFailureHandler("/login?error=true");
	}

	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	/**
	 * Session registry.
	 *
	 * @return the session registry
	 */
	@Bean
	public SessionRegistry sessionRegistry() {
		return new SessionRegistryImpl();
	}

	/**
	 * Http session event publisher.
	 *
	 * @return the http session listener
	 */
	@Bean
	public HttpSessionListener httpSessionEventPublisher() {
		int sessionTimeout = env.getProperty("server.session-timeout", Integer.class);
		System.out.println("session test-->" +sessionTimeout);
		CustomHttpSessionEventPublisher customHttpSessionEventPublisher = new CustomHttpSessionEventPublisher();
		customHttpSessionEventPublisher.setSessionTimeout(sessionTimeout);
		return customHttpSessionEventPublisher;
	}

	/**
	 * Custom simple url authentication failure handler.
	 *
	 * @return the custom simple url authentication failure handler
	 */
	@Bean
	public CustomSimpleUrlAuthenticationFailureHandler customSimpleUrlAuthenticationFailureHandler() {
		return new CustomSimpleUrlAuthenticationFailureHandler();
	}

	/**
	 * Servlet listener registration bean.
	 *
	 * @return the servlet listener registration bean
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Bean
	public ServletListenerRegistrationBean servletListenerRegistrationBean() {
		return new ServletListenerRegistrationBean(httpSessionEventPublisher());
	}

	/**
	 * Custom concurrent session filter.
	 *
	 * @return the custom concurrent session filter
	 */
	@Bean
	public CustomConcurrentSessionFilter customConcurrentSessionFilter() {
		return new CustomConcurrentSessionFilter(sessionRegistry(), LOGIN_INVALID_SESSION_URL);
	}
}