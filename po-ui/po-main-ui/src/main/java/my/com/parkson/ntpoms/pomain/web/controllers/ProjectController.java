package my.com.parkson.ntpoms.pomain.web.controllers;

import static my.com.parkson.ntpoms.common.MessageCodes.FAILED_FETCH_RECORDS_EXCEPTION;
import static my.com.parkson.ntpoms.common.MessageCodes.FAILED_PERSIST_EXCEPTION;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.LogMessages;
import my.com.parkson.ntpoms.common.entities.EmployeeMaster;
import my.com.parkson.ntpoms.common.entities.ProfitCenter;

import my.com.parkson.ntpoms.common.entities.SystemParameter;
import my.com.parkson.ntpoms.common.LogMessages;
import my.com.parkson.ntpoms.common.model.Pager;
import my.com.parkson.ntpoms.common.services.EmployeeMasterService;

import my.com.parkson.ntpoms.common.services.ProfitCenterService;

import my.com.parkson.ntpoms.common.services.SystemParameterService;
import my.com.parkson.ntpoms.main.entities.Project;
import my.com.parkson.ntpoms.main.services.AccessLogsService;
import my.com.parkson.ntpoms.main.services.AuditsService;
import my.com.parkson.ntpoms.main.services.MessagesService;
import my.com.parkson.ntpoms.main.services.ProjectService;
import my.com.parkson.ntpoms.pomain.web.models.ProjectForm;

@Slf4j
@Controller
@RequiredArgsConstructor
public class ProjectController extends AbstractMainController {

	private static final int BUTTONS_TO_SHOW = 5;
	private static final int INITIAL_PAGE = 0;
	private static final int INITIAL_PAGE_SIZE = 5;
	private static final int[] PAGE_SIZES = { 5, 10, 20 };
	boolean edit = false;

	@Autowired
	private SystemParameterService sysRepo;

	@Autowired
	private ProjectService projectService;

	@Autowired
	private ProfitCenterService pcService;

	@Autowired
	private EmployeeMasterService empservice;

	private static final Logger LOGGER = Logger.getLogger(ProjectController.class);

	/** The messages. */
	@Autowired
	private MessagesService messages;

	@Autowired
	private AuditsService auditService;

	private ProjectForm tempobj;

	private static final String DATE_FORMAT_KEY = "DATE_FORMAT";
	private static final String DATE_FORMAT_VALUE = "dd-MM-yyyy";

	@GetMapping("/main/project")
	public ModelAndView onPageLoadProject() {
		ModelAndView modelAndView = new ModelAndView("/main/project");

		String dateFormat = "dd-MM-yyyy";
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_FORMAT");
		if (dateObj != null && !dateObj.getPropValue().isEmpty()) {
			dateFormat = dateObj.getPropValue();
		}

		modelAndView.addObject("date_format", dateFormat);

		modelAndView.addObject("projectMember", getProjectHeadMultiSelect());
		modelAndView.addObject("profitCenter", getProfitCenterMultiSelect());
		modelAndView.getModelMap().addAttribute("projectobj", tempobj != null ? tempobj : new ProjectForm());

		return modelAndView;

	}

	@GetMapping("/main/projectgrid")
	public String projectGrid(@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page, @RequestParam("code") Optional<String> code,
			@RequestParam("name") Optional<String> name, @RequestParam("status") Optional<String> isActive,
			@RequestParam("sortingkey") Optional<String> sortingkey,
			@RequestParam("sortingorder") Optional<String> sortingorder, Model model) {

		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
		Page<Project> list;

		Specification<Project> s = new Specification<Project>() {
			@Override
			public Predicate toPredicate(Root<Project> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> ls = new ArrayList<>();
				if (code.isPresent() && !code.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("code")), "%" + code.get().toLowerCase() + "%"));
				}

				if (name.isPresent() && !name.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("name")), "%" + name.get().toLowerCase() + "%"));
				}

				if (isActive.isPresent() && !isActive.get().isEmpty() && !isActive.get().equals("All")) {
					ls.add(cb.equal(root.get("isActive"), "Active".equals(isActive.get())));
				}

				return cb.and(ls.toArray(new Predicate[0]));
			}
		};

		Sort sorttemp = new Sort(Sort.Direction.DESC, "lastModifiedOn");

		if (sortingkey.isPresent() && sortingorder.isPresent()) {
			sorttemp = new Sort(sortingorder.get().equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC,
					sortingkey.get());
		}
		if (code.isPresent() || name.isPresent() || isActive.isPresent()) {
			list = projectService.getAllPageableProject(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		} else {
			list = projectService.getAllPageableProjectOrdered(new PageRequest(evalPage, evalPageSize, sorttemp));
		}
		int serialid = 0;
		int pagesize = evalPage + 1;
		if (pagesize > 1) {
			int start = evalPageSize * pagesize;
			serialid = start - evalPageSize;
		}
		Pager pager = new Pager(list.getTotalPages(), list.getNumber(), BUTTONS_TO_SHOW);
		model.addAttribute("serialid", serialid);
		model.addAttribute("list", list);
		model.addAttribute("selectedPageSize", evalPageSize);
		model.addAttribute("pageSizes", PAGE_SIZES);
		model.addAttribute("pager", pager);
		model.addAttribute("totalRecords", list.getTotalElements());
		model.addAttribute("projectMember", getProjectHeadMultiSelect());
		model.addAttribute("profitCenter", getProfitCenterMultiSelect());
		model.addAttribute("code", code.isPresent() ? code.get() : "");
		model.addAttribute("name", name.isPresent() ? name.get() : "");
		model.addAttribute("status", isActive.isPresent() ? isActive.get() : "");
		model.addAttribute("sortingorder", sortingorder.isPresent() ? sortingorder.get() : "DESC");
		model.addAttribute("sortingkey", sortingkey.isPresent() ? sortingkey.get() : "lastModifiedOn");

		return "/main/project :: result-table";

	}

	@PostMapping("/main/createproject")
	public String createProject(@Valid @ModelAttribute("projectobj") ProjectForm obj, BindingResult result, Model model,
			RedirectAttributes redirectAttributes) throws ParseException {
		MDC.put("sessionId",
				AbstractMainController.getCompany().isPresent() ? AbstractMainController.getCompany().get() : "");
		if (!result.hasErrors() && obj.getProjectCode() != null
				&& projectService.getProjectByCode(obj.getProjectCode()) == null) {
			Project project = new Project();
			SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_FORMAT");
			String dateFormat = "dd-MM-yyyy";
			if (dateObj != null && !dateObj.getPropValue().isEmpty()) {
				dateFormat = dateObj.getPropValue();
			}
			DateFormat formatter = new SimpleDateFormat(dateFormat);
			project.setCode(obj.getProjectCode());
			project.setName(obj.getProjectName());
			project.setDescription(obj.getProjectDescription());
			project.setHead(obj.getPrHeadTo());
			project.setTeamMember(obj.getPrTeamTo());
			project.setPcCode(obj.getPcTo());

			if (obj.getCompletionDate() != null && !obj.getCompletionDate().isEmpty()) {
				project.setCompletionDate(formatter.parse(obj.getCompletionDate()));
			}
			project.setCreatedBy(getCurrentUser().get());
			project.setCreatedOn(new Date());
			project.setLastModifiedBy(getCurrentUser().get());
			project.setLastModifiedOn(new Date());
			project.setActive(true);

			try {
				projectService.createProject(project);
				auditService.createAudit(new Date(), "Project: " + obj.getProjectCode() + " created successfully. ",
						getCurrentUser().get(), " Create Project", getCompany().get(), "");
				redirectAttributes.addFlashAttribute("info", "Project Record created successfully");
				tempobj = null;
			} catch (Exception e) {
				LOGGER.error(e);
			}

		} else {
			redirectAttributes.addFlashAttribute("error", "Project code already exists");
			tempobj = obj;
		}

		return "redirect:/main/project";

	}

	/**
	 * Update Project.
	 *
	 * @param obj
	 *            the obj
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @param redirectAttributes
	 *            the redirect attributes
	 * @return the string
	 */
	@PostMapping("/main/updateproject")
	public String updateProject(@Valid @ModelAttribute("projectobj") ProjectForm obj, BindingResult result, Model model,
			RedirectAttributes redirectAttributes) {
		MDC.put("sessionId",
				AbstractMainController.getCompany().isPresent() ? AbstractMainController.getCompany().get() : "");
		if (!result.hasErrors()) {

			Project project = projectService.getProjectByCode(obj.getProjectCode());
			boolean isActive;

			SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_FORMAT");
			String dateFormat = "dd-MM-yyyy";
			if (dateObj != null && !dateObj.getPropValue().isEmpty()) {
				dateFormat = dateObj.getPropValue();
			}
			DateFormat formatter = new SimpleDateFormat(dateFormat);

			try {
				if (project != null) {

					project.setLastModifiedBy(getCurrentUser().get());
					project.setLastModifiedOn(new Date());
					if (obj.getIsActive().contains("InActive")) {
						isActive = false;
					} else {
						isActive = true;
					}

					if (isActive != project.isActive()) {

						if (obj.getIsActive().contains("InActive")) {
							project.setDeactivatedBy(getCurrentUser().get());
							project.setDeactivatedOn(new Date());
						} else {
							project.setReactivatedOn(new Date());
							project.setReactivatedBy(getCurrentUser().get());
						}
					}

					project.setActive("Active".equals(obj.getIsActive()));
					project.setCode(obj.getProjectCode());
					project.setName(obj.getProjectName());
					project.setDescription(obj.getProjectDescription());
					project.setHead(obj.getPrHeadTo());
					project.setTeamMember(obj.getPrTeamTo());
					project.setPcCode(obj.getPcTo());

					if (obj.getCompletionDate() != "" && !(obj.getCompletionDate().isEmpty())) {
						project.setCompletionDate(formatter.parse(obj.getCompletionDate()));
					}

					try {
						projectService.createProject(project);
						auditService.createAudit(new Date(),
								"Project: " + obj.getProjectCode() + " updated successfully. ", getCurrentUser().get(),
								" Update Project", getCompany().get(), "");
					} catch (Exception e) {
						LOGGER.error(e);
					}
					redirectAttributes.addFlashAttribute("info", "Project Updated successfully");

				} else {
					redirectAttributes.addFlashAttribute("error", "Project Object does not exist");
					return "/main/edit_project";
				}

			} catch (Exception e) {
				LOGGER.error(e);
			}
		}
		return "redirect:/main/project";
	}

	/**
	 * Edits Project.
	 *
	 * @param code
	 *            the code
	 * @param model
	 *            the model
	 * @return the string
	 */

	@GetMapping(value = "/main/edit_project/{code}")
	public String editProject(@PathVariable String code, Model model) {

		Project project = projectService.getProjectByCode(code);
		edit = true;

		if (project != null) {

			String dateFormat = "dd-MM-yyyy hh:mm:ss";
			SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_TIME_FORMAT");
			if (dateObj != null && !dateObj.getPropValue().isEmpty()) {
				dateFormat = dateObj.getPropValue();
			}
			DateFormat formatter = new SimpleDateFormat(dateFormat);
			String dateFormat2 = DATE_FORMAT_VALUE;
			SystemParameter dateObj2 = sysRepo.getSystemParameterByPropName(DATE_FORMAT_KEY);
			if (dateObj2 != null && !dateObj2.getPropValue().isEmpty()) {
				dateFormat2 = dateObj2.getPropValue();
			}
			DateFormat format = new SimpleDateFormat(dateFormat2);
			String createdOn = "";
			String modifiedOn = "";
			String reactivatedOn = "";
			String deactivatedOn = "";

			if (project.getCreatedOn() != null) {
				createdOn = formatter.format(project.getCreatedOn());
			}

			if (project.getLastModifiedOn() != null) {
				modifiedOn = formatter.format(project.getLastModifiedOn());
			}

			if (project.getReactivatedOn() != null) {
				reactivatedOn = formatter.format(project.getReactivatedOn());
			}

			if (project.getDeactivatedOn() != null) {
				deactivatedOn = formatter.format(project.getDeactivatedOn());
			}

			ProjectForm obj = new ProjectForm();

			obj.setCreatedOn(createdOn);
			obj.setDeactivatedOn(deactivatedOn);
			obj.setReactivatedOn(reactivatedOn);
			obj.setLastModifiedOn(modifiedOn);

			obj.setCreatedBy(project.getCreatedBy());
			obj.setDeactivatedBy(project.getDeactivatedBy());
			obj.setReactivatedBy(project.getReactivatedBy());
			obj.setLastModifiedBy(project.getLastModifiedBy());

			obj.setProjectCode(project.getCode());
			obj.setProjectName(project.getName());
			obj.setProjectDescription(project.getDescription());

			if (project.getCompletionDate() != null) {
				obj.setCompletionDate("" + format.format(project.getCompletionDate()));
			}
			obj.setPrHeadTo(project.getHead());
			obj.setPrTeamTo(project.getTeamMember());
			obj.setPcTo(project.getPcCode());
			obj.setIsActive(project.isActive() ? "Active" : "InActive");

			model.addAttribute("projectMember", getProjectHeadMultiSelect());

			if (project.getHead() != null && !project.getHead().isEmpty()) {
				model.addAttribute("prHeadTo", Arrays.asList(project.getHead().split("\\s*,\\s*")));
			}

			if (project.getPcCode() != null && !project.getPcCode().isEmpty()) {
				model.addAttribute("pcTo", Arrays.asList(project.getPcCode().split("\\s*,\\s*")));
			}

			if (project.getTeamMember() != null && !project.getTeamMember().isEmpty()) {
				model.addAttribute("prTeamTo", Arrays.asList(project.getTeamMember().split("\\s*,\\s*")));
			}

			model.addAttribute("profitCenter", getProfitCenterMultiSelect());
			model.addAttribute("projectMember", getProjectHeadMultiSelect());
			model.addAttribute("projectobj", obj);
			model.addAttribute("edit", edit);

			return "/main/edit_project";
		} else {
			return "redirect:/main/project";
		}

	}

	private List<String> getProjectHeadMultiSelect() {
		List<String> lis = new ArrayList<>();
		Page<EmployeeMaster> temp = empservice.getAllPagableEmployee(new PageRequest(0, 20));
		if (temp != null) {
			for (EmployeeMaster e : temp) {
				StringBuilder s = new StringBuilder();
				s.append(e.getPsId());
				s.append("-");
				s.append(e.getName());
				s.append("(");
				s.append(e.getPosTitle());
				s.append(")");
				lis.add(s.toString());
			}
		}
		return lis;
	}

	private List<String> getProfitCenterMultiSelect() {
		List<String> lis = new ArrayList<>();
		Page<ProfitCenter> temp = pcService.getAllPageableProfitCenter(new PageRequest(0, 20));
		if (temp != null) {
			for (ProfitCenter e : temp) {
				StringBuilder s = new StringBuilder();
				s.append(e.getCode());
				s.append("-");
				s.append(e.getName());

				lis.add(s.toString());
			}
		}
		return lis;
	}

	@RequestMapping(value = "/main/project/employeesearch", method = RequestMethod.GET)
	public @ResponseBody List<String> employeeSearch(@RequestParam("searchval") String searchval,
			RedirectAttributes redirectAttributes, Model model) {
		Specification<EmployeeMaster> sp = new Specification<EmployeeMaster>() {
			@Override
			public Predicate toPredicate(Root<EmployeeMaster> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> ls = new ArrayList<>();
				ls.add(cb.like(cb.lower(root.get("psId")), "%" + searchval.toLowerCase() + "%"));
				ls.add(cb.like(cb.lower(root.get("name")), "%" + searchval.toLowerCase() + "%"));
				ls.add(cb.like(cb.lower(root.get("posTitle")), "%" + searchval.toLowerCase() + "%"));
				return cb.or(ls.toArray(new Predicate[0]));
			}
		};
		Page<EmployeeMaster> list;
		if (searchval != null && !searchval.isEmpty()) {
			list = empservice.getAllPagableEmployee(sp, new PageRequest(0, 20));
		} else {
			list = empservice.getAllPagableEmployee(new PageRequest(0, 20));
		}
		List<String> temp = new ArrayList<>();
		for (EmployeeMaster e : list.getContent()) {
			StringBuilder s = new StringBuilder();
			s.append(e.getPsId());
			s.append("-");
			s.append(e.getName());
			s.append("(");
			s.append(e.getPosTitle());
			s.append(")");
			temp.add(s.toString());
		}
		return temp;
	}

	@RequestMapping(value = "/main/project/profitcentersearch", method = RequestMethod.GET)
	public @ResponseBody List<String> pcSearch(@RequestParam("searchval") String searchval,
			RedirectAttributes redirectAttributes, Model model) {
		Specification<ProfitCenter> sp = new Specification<ProfitCenter>() {
			@Override
			public Predicate toPredicate(Root<ProfitCenter> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> ls = new ArrayList<>();
				ls.add(cb.like(cb.lower(root.get("code")), "%" + searchval.toLowerCase() + "%"));
				ls.add(cb.like(cb.lower(root.get("name")), "%" + searchval.toLowerCase() + "%"));

				return cb.or(ls.toArray(new Predicate[0]));
			}
		};
		Page<ProfitCenter> list;
		if (searchval != null && !searchval.isEmpty()) {
			list = pcService.getAllPageableProfitCenter(sp, new PageRequest(0, 20));
		} else {
			list = pcService.getAllPageableProfitCenter(new PageRequest(0, 20));
		}
		List<String> temp = new ArrayList<>();
		for (ProfitCenter e : list.getContent()) {
			StringBuilder s = new StringBuilder();
			s.append(e.getCode());
			s.append("-");
			s.append(e.getName());

			temp.add(s.toString());
		}
		return temp;
	}

	@Override
	protected String getHeaderTitle() {
		// TODO Auto-generated method stub
		return null;
	}

	@RequestMapping(value = "/project/existingproject", method = RequestMethod.GET)
	public @ResponseBody Project checkProject(@RequestParam("projectcode") String projectcode,
			RedirectAttributes redirectAttributes, Model model) {

		return projectService.getProjectByCode(projectcode);

	}

}
