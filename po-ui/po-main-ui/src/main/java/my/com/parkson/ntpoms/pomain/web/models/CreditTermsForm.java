package my.com.parkson.ntpoms.pomain.web.models;

import java.io.Serializable;
import java.util.Date;

public class CreditTermsForm implements Serializable {
		
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String credittermscode;
	private String credittermsdescription;
	private String createdby;
	private String createdon;
	private String lastmodifiedby;
	private String lastModifiedOn;
	private String deactivatedby;
	private String deactivatedon;
	private String reactivatedby;
	private String reactivatedon;
	private String isActive;	
	private String compCode;
	
	public String getCredittermscode() {
		return credittermscode;
	}
	public void setCredittermscode(String credittermscode) {
		this.credittermscode = credittermscode;
	}
	public String getCredittermsdescription() {
		return credittermsdescription;
	}
	public void setCredittermsdescription(String credittermsdescription) {
		this.credittermsdescription = credittermsdescription;
	}
	
	
	public String getIsActive() {
		return isActive;
	}

	/**
	 * @param isActive
	 *            the isActive to set
	 */
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	
	
	
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public String getCreatedon() {
		return createdon;
	}
	public void setCreatedon(String createdon) {
		this.createdon = createdon;
	}
	public String getLastmodifiedby() {
		return lastmodifiedby;
	}
	public void setLastmodifiedby(String lastmodifiedby) {
		this.lastmodifiedby = lastmodifiedby;
	}
	public String getLastModifiedOn() {
		return lastModifiedOn;
	}
	public void setLastModifiedOn(String lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}
	public String getDeactivatedby() {
		return deactivatedby;
	}
	public void setDeactivatedby(String deactivatedby) {
		this.deactivatedby = deactivatedby;
	}
	public String getDeactivatedon() {
		return deactivatedon;
	}
	public void setDeactivatedon(String deactivatedon) {
		this.deactivatedon = deactivatedon;
	}
	public String getReactivatedby() {
		return reactivatedby;
	}
	public void setReactivatedby(String reactivatedby) {
		this.reactivatedby = reactivatedby;
	}
	public String getReactivatedon() {
		return reactivatedon;
	}
	public void setReactivatedon(String reactivatedon) {
		this.reactivatedon = reactivatedon;
	}
	public String getCompCode() {
		return compCode;
	}
	public void setCompCode(String compCode) {
		this.compCode = compCode;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	

}
