/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.pomain.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.thymeleaf.extras.springsecurity4.dialect.SpringSecurityDialect;

import my.com.parkson.ntpoms.pomain.security.PostAuthorizationFilter;

/**
 * The Class WebConfig.
 */
@Configuration
public class WebConfig extends WebMvcConfigurerAdapter {

	private PostAuthorizationFilter postAuthorizationFilter;

	private MessageSource messageSource;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter#
	 * addViewControllers(org.springframework.web.servlet.config.annotation.
	 * ViewControllerRegistry)
	 */
	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		super.addViewControllers(registry);
		
		registry.addRedirectViewController("/", "/loginredirect");

	}

	/**
	 * Instantiates a new web config.
	 *
	 * @param postAuthorizationFilter
	 *            the post authorization filter
	 * @param messageSource
	 *            the message source
	 */
	@Autowired
	public WebConfig(PostAuthorizationFilter postAuthorizationFilter, MessageSource messageSource) {
		this.postAuthorizationFilter = postAuthorizationFilter;
		this.messageSource = messageSource;
	}

	/**
	 * Jsr validator.
	 *
	 * @return the validator
	 */
	@Bean
	public Validator jsrValidator() {
		LocalValidatorFactoryBean factory = new LocalValidatorFactoryBean();
		factory.setValidationMessageSource(messageSource);
		return factory;
	}

	/**
	 * Post authorization filter registration bean.
	 *
	 * @return the filter registration bean
	 */
	@Bean
	public FilterRegistrationBean postAuthorizationFilterRegistrationBean() {
		FilterRegistrationBean registrationBean = new FilterRegistrationBean();
		registrationBean.setFilter(postAuthorizationFilter);
		registrationBean.setOrder(Integer.MAX_VALUE);
		return registrationBean;
	}

	/**
	 * Security dialect.
	 *
	 * @return the spring security dialect
	 */
	@Bean
	public SpringSecurityDialect securityDialect() {
		return new SpringSecurityDialect();
	}
}
