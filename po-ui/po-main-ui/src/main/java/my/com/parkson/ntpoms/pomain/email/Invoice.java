/**
 *****************************************************
 * COPYRIGHT UMAIR OTHMAN
 * 2017
 * IT Exec
 *****************************************************
 */
package my.com.parkson.ntpoms.pomain.email;

import java.math.BigDecimal;
import java.util.Date;


/**
 **************************************************************
 * Class use to store invoice list objects in Po Email Criteria
 **************************************************************
 */

public class Invoice {

	String invoiceNumber;
	Date invoiceDate;
	BigDecimal value;
	String invoiceRecipient;
	
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	
	public Date getInvoiceDate() {
		return invoiceDate;
	}
	
	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	
	public BigDecimal getValue() {
		return value;
	}
	
	public void setValue(BigDecimal value) {
		this.value = value;
	}
	
	public String getInvoiceRecipient() {
		return invoiceRecipient;
	}
	
	public void setInvoiceRecipient(String invoiceRecipient) {
		this.invoiceRecipient = invoiceRecipient;
	}
}