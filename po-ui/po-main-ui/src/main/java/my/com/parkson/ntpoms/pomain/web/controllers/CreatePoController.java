package my.com.parkson.ntpoms.pomain.web.controllers;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.UriUtils;

import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.entities.EmployeeMaster;
import my.com.parkson.ntpoms.common.entities.OrderCategory;
import my.com.parkson.ntpoms.common.entities.OrderSubCategory;
import my.com.parkson.ntpoms.common.entities.ProfitCenter;
import my.com.parkson.ntpoms.common.entities.PurchaseOrderUser;
import my.com.parkson.ntpoms.common.entities.PurchaseRequisition;
import my.com.parkson.ntpoms.common.entities.SystemParameter;
import my.com.parkson.ntpoms.common.services.EmployeeMasterService;
import my.com.parkson.ntpoms.common.services.GoodsServicesTaxService;
import my.com.parkson.ntpoms.common.services.OrderCategoryService;
import my.com.parkson.ntpoms.common.services.OrderSubCategoryService;
import my.com.parkson.ntpoms.common.services.ProfitCenterService;
import my.com.parkson.ntpoms.common.services.PurchaseOrderUserService;
import my.com.parkson.ntpoms.common.services.PurchaseRequisitionService;
import my.com.parkson.ntpoms.common.services.SystemParameterService;
import my.com.parkson.ntpoms.main.entities.CreditTerms;
import my.com.parkson.ntpoms.main.entities.CreditTermsId;
import my.com.parkson.ntpoms.main.entities.ItemInformation;
import my.com.parkson.ntpoms.main.entities.ItemMap;
import my.com.parkson.ntpoms.main.entities.ProfitCenterLastPONumber;
import my.com.parkson.ntpoms.main.entities.Project;
import my.com.parkson.ntpoms.main.entities.PurchaseOrder;
import my.com.parkson.ntpoms.main.entities.PurchaseOrderUserAccess;
import my.com.parkson.ntpoms.main.entities.PurchaseOrderUserAccessPk;
import my.com.parkson.ntpoms.main.entities.VendorBranch;
import my.com.parkson.ntpoms.main.entities.VendorBranchId;
import my.com.parkson.ntpoms.main.entities.VendorCreditTerms;
import my.com.parkson.ntpoms.main.repositories.PurchaseOrderUserAccessRepository;
import my.com.parkson.ntpoms.main.services.AuditsService;
import my.com.parkson.ntpoms.main.services.CreditTermsService;
import my.com.parkson.ntpoms.main.services.ItemInformationService;
import my.com.parkson.ntpoms.main.services.ItemMapService;
import my.com.parkson.ntpoms.main.services.MessagesService;
import my.com.parkson.ntpoms.main.services.PcLastPONumberService;
import my.com.parkson.ntpoms.main.services.ProjectService;
import my.com.parkson.ntpoms.main.services.PurchaseOrderService;
import my.com.parkson.ntpoms.main.services.PurchaseOrderUserAccessService;
import my.com.parkson.ntpoms.main.services.VendorBranchService;
import my.com.parkson.ntpoms.main.services.VendorCreditTermsService;
import my.com.parkson.ntpoms.pomain.web.models.AutocompleteForm;
import my.com.parkson.ntpoms.pomain.web.models.ItemForm;
import my.com.parkson.ntpoms.pomain.web.models.ItemInfoForm;
import my.com.parkson.ntpoms.pomain.web.models.PoMainForm;
import my.com.parkson.ntpoms.pomain.web.utils.CreatePdf;
import my.com.parkson.ntpoms.pomain.web.utils.PurchaseOrderSendMailUtil;

@Controller
public class CreatePoController extends AbstractMainController {

	private static final Logger LOGGER = Logger.getLogger(CreatePoController.class);

	@Autowired
	private PurchaseOrderService poMainService;

	@Autowired
	private CreditTermsService crtService;

	@Autowired
	private OrderCategoryService orderCatService;

	@Autowired
	private OrderSubCategoryService orderSubCatService;

	@Autowired
	private ProfitCenterService pcrepo;

	@Autowired
	private VendorBranchService vbService;

	@Autowired
	private VendorCreditTermsService vcrtService;

	@Autowired
	private PurchaseRequisitionService prService;

	@Autowired
	private ItemInformationService itemInfoRepo;

	@Autowired
	private ItemMapService itemMapRepo;

	@Autowired
	private GoodsServicesTaxService gstrepo;

	@Autowired
	private ProjectService projectService;

	@Autowired
	private ProfitCenterService profitCenterService;

	@Autowired
	private EmployeeMasterService employeeService;

	@Autowired
	private PurchaseOrderUserService poUserService;

	@Autowired
	private PurchaseOrderUserAccessService poUserAccessService;

	@Autowired
	private SystemParameterService sysRepo;

	/** The messages. */
	@Autowired
	private MessagesService messages;

	@Autowired
	private AuditsService auditService;

	@Autowired
	private PcLastPONumberService pcLastrepo;

	@Autowired
	private PurchaseOrderUserAccessRepository poaccessrepo;

	private PoMainForm tempobj;

	private PoMainForm tempobj_duplicate;
	private PoMainForm tempobj_draft;
	private PoMainForm tempobj_issued;
	private PoMainForm tempobj_verified;
	private PoMainForm tempobj_authorized;
	private PoMainForm tempobj_redraft;
	private PoMainForm tempobj_delete;

	private EmployeeMaster employee;

	private List<EmployeeMaster> employeeList;

	private VendorBranch vendorbranch;

	private PurchaseOrderUser purchaseorderuser;

	private List<PurchaseRequisition> purchaserequ;

	@Autowired
	private PcLastPONumberService pcLastPONumberService;

	private List<VendorBranch> vendorlist;

	private List<ProfitCenter> profitcenterlist;

	private ProfitCenter profitCenterCode;

	private String poNumber;

	private boolean empsaved = false;

	private static String AUTHGRID_UPLOAD_PATH = "AUTHGRID_FOLDER_PATH";

	private String authDir;

	private String ordAuth;

	private String newPath;

	private String activetab = "tab1";

	private Calendar calendar = new GregorianCalendar();

	private VendorCreditTerms lastcrt;

	private List<Project> projectList;

	private List<PurchaseOrderUser> userlist;

	private OrderCategory ordercat;

	private String orderAuthGrid;

	private String issuedon;

	private String verifiedon;

	private String rejectedon;

	private String cancelledon;

	private String deletedon;

	private String authorizedon;

	private String crt_edit;

	private String path_edit;

	private String choosen_view;

	private boolean disable_verify_button;

	private String verifier_forward;

	private List<PurchaseOrderUser> forward_verifylist;

	private List<PurchaseOrderUser> forward_invoicerlist;

	private List<PurchaseOrderUser> forward_authorizelist;

	private boolean disable_verify_button_issuer;

	private boolean disable_authorize_button_verified;

	private boolean disable_reject_button;

	private String pono_duplicate;

	private String poNum;

	private List<ItemMap> oldlist = new ArrayList<>();

	/* boolean pc_chk_active = true; */
	boolean pc_st_match;
	boolean pc_dept_match;
	boolean pc_add1_match;
	boolean pc_add2_match;
	boolean pc_add3_match;
	boolean pc_postcode_match;
	boolean pc_postarea_match;
	boolean vend_name_match;
	boolean vend_number_match;
	boolean vend_email_match;
	boolean vend_add1_match;
	boolean vend_add2_match;
	boolean vend_add3_match;
	boolean vend_postcode_match;
	boolean vend_postarea_match;

	HashMap<String, Boolean> pc_info_list = new HashMap<>();
	HashMap<String, Boolean> vendor_info_list = new HashMap<>();

	String st_old;
	String dept_old;
	String add1_old;
	String add2_old;
	String add3_old;
	String postcode_old;
	String postarea_old;

	String st_new;
	String dept_new;
	String add1_new;
	String add2_new;
	String add3_new;
	String postcode_new;
	String postarea_new;

	String vend_name_new;
	String vend_number_new;
	String vend_email_new;
	String vend_add1_new;
	String vend_add2_new;
	String vend_add3_new;
	String vend_postcode_new;
	String vend_postarea_new;

	private boolean inactive_pc;
	private boolean empty_vendlist;
	private boolean empty_ordlist;
	private boolean empty_ordsublist;
	private boolean empty_prtypelist;
	private boolean empty_activepclist;

	private boolean inactive_pccode;

	private VendorBranch check_vendor_obj;

	private PurchaseOrder purchaseorder_view;

	private PoMainForm search_obj;

	private PurchaseOrder purchaseorder;

	@GetMapping("/main/createpo_menu")
	public ModelAndView onCreatePOLoad_menu(RedirectAttributes redirectattributes) {

		ModelAndView modelAndView = new ModelAndView("/main/createpo");
		tempobj = null;
		poNum = null;
		orderAuthGrid = null;
		PoMainForm obj = new PoMainForm();
		String dateFormat = "dd-MM-yyyy";
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_FORMAT");
		if (dateObj != null && !dateObj.getPropValue().isEmpty()) {
			dateFormat = dateObj.getPropValue();
		}
		purchaseorderuser = poUserService.getPurchaseOrderUserById(getCurrentUser().get());
		if (purchaseorderuser != null) {
			employee = employeeService.getEmployeeMasterByPsId(purchaseorderuser.getId());
		}

		if (employee != null) {
			profitCenterCode = profitCenterService
					.getProfitCenterByCode(employee.getProfitCenter().getCode().toString());
			if (!profitCenterCode.isIsactive()) {
				inactive_pccode = true;
				modelAndView.addObject("inactive_pccode", inactive_pccode);
			}
		}
		boolean active = true;
		profitcenterlist = profitCenterService.getAllProfitCenterByCompanyCode(getCompany().get(), active);
		vendorlist = vbService.getAllActiveVendorBranchBycompCode(getCompany().get());
		purchaserequ = prService.getAllActivePurchaseRequisition();

		if (profitCenterCode != null) {
			projectList = projectService.getActiveProjectByProfitCenterCode(profitCenterCode.getCode());
		}
		userlist = poUserService.getAllActivePurchaseOrderUser();

		String[] purchaseorderuserArray = purchaseorderuser.toString().split("-");

		for (int i = 0; i < userlist.size(); i++) {

			if (userlist.get(i).getId().equals(purchaseorderuserArray[0].trim())) {
				int keyLocation = userlist.indexOf(userlist.get(i));
				userlist.remove(keyLocation);

			}

		}
		modelAndView.addObject("profitCenterlist", getProfitCenterCodeAndProfitcenterName());
		modelAndView.addObject("itemNolist", itemInfoRepo.getAllItemInformation());
		modelAndView.addObject("taxCodelist", gstrepo.getAllActiveGoodsServicesTax());
		modelAndView.addObject("iteminfoobj", new ItemInfoForm());

		ItemForm itemobj = new ItemForm();
		itemobj.setItemOrder(getItemOrder(poNum));
		System.out.println(""+itemobj);
		modelAndView.addObject("itemobj", itemobj);

		modelAndView.addObject("credittermscodelist", crtService.getAllActiveCreditTerms());
		modelAndView.addObject("ordCatlist", orderCatService.getAllActiveOrderCategory());
		modelAndView.addObject("ordSubClist", orderSubCatService.getAllActiveOrderSubCategory());
		modelAndView.addObject("projectCodelist", projectService.getAllActiveProject());
		modelAndView.addObject("poUserlist", verifier_info());

		if (profitCenterCode != null) {

			if (profitCenterCode.getStore().getName() != null) {
				modelAndView.addObject("profitCenter_store", populateIssuerTabStoreinfo(profitCenterCode.getCode()));
			} else {
				modelAndView.addObject("profitCenter_store",
						populateIssuerTabStoreinfo(profitCenterCode.getCode()) + " No Store Name Found");
			}

			if (profitCenterCode.getDepartment().getName() != null) {
				modelAndView.addObject("profitCenter_department",
						populateIssuerTabDeptinfo(profitCenterCode.getCode()));
			} else {

				modelAndView.addObject("profitCenter_department",
						populateIssuerTabDeptinfo(profitCenterCode.getCode()) + " No Department Name Found");
			}
			modelAndView.addObject("profitCenterCode", profitCenterCode);
		} else {
			modelAndView.addObject("profitCenterCode", "No PC Code Found");
		}

		if (employee != null) {
			modelAndView.addObject("employee", employee);
			obj.setContactPersonPhone(employee.getPhoneNo());
			obj.setContactPersonEmail(employee.getEmail());
		}
		modelAndView.getModelMap().addAttribute("pomainobj", obj);
		modelAndView.addObject("purchaseorderuser", purchaseorderuser);
		modelAndView.addObject("userlist", authorizor_info());
		modelAndView.addObject("vendor", populateVendorTabvendinfo());
		modelAndView.addObject("vendorlist", vendorlist);
		modelAndView.addObject("date_format", dateFormat);
		modelAndView.addObject("currencyList", currencyList());
		modelAndView.addObject("prtypeList", purchaserequ);
		modelAndView.addObject("projectList", projectList);
		modelAndView.addObject("profitcenterlist", populateProfitCenter());
		modelAndView.addObject("choosen_view", choosen_view);
		modelAndView.addObject("lastcrt", crt_edit);
		modelAndView.addObject("activetab", "tab1");

		return modelAndView;
	}

	@GetMapping("/main/createpo")
	public ModelAndView onCreatePOLoad() {
		authDir = sysRepo.getSystemParameterValueByPropName(AUTHGRID_UPLOAD_PATH);

		ModelAndView modelAndView = new ModelAndView("/main/createpo");
		String dateFormat = "dd-MM-yyyy";
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_FORMAT");
		if (dateObj != null && !dateObj.getPropValue().isEmpty()) {
			dateFormat = dateObj.getPropValue();
		}
		purchaseorderuser = poUserService.getPurchaseOrderUserById(getCurrentUser().get());
		employee = employeeService.getEmployeeMasterByPsId(purchaseorderuser.getId());

		profitCenterCode = profitCenterService.getProfitCenterByCode(employee.getProfitCenter().getCode().toString());
		boolean active = true;
		profitcenterlist = profitCenterService.getAllProfitCenterByCompanyCode(getCompany().get(), active);
		vendorlist = vbService.getAllActiveVendorBranchBycompCode(getCompany().get());
		if (vendorlist.size() == 0) {
			empty_vendlist = true;
			modelAndView.addObject("empty_vendorlist", empty_vendlist);
		}
		purchaserequ = prService.getAllActivePurchaseRequisition();
		if (purchaserequ.size() == 0) {
			empty_prtypelist = true;
			modelAndView.addObject("empty_prtypelist", empty_prtypelist);

		}
		projectList = projectService.getActiveProjectByProfitCenterCode(profitCenterCode.getCode());
		userlist = poUserService.getAllActivePurchaseOrderUser();

		modelAndView.getModelMap().addAttribute("profitCenterlist", populateProfitCenter());
		modelAndView.getModelMap().addAttribute("itemNolist", itemInfoRepo.getAllItemInformation());
		modelAndView.getModelMap().addAttribute("taxCodelist", gstrepo.getAllActiveGoodsServicesTax());
		modelAndView.getModelMap().addAttribute("iteminfoobj", tempobj != null ? tempobj : new ItemInfoForm());

		ItemForm tempobj_info = new ItemForm();
		tempobj_info.setItemOrder(getItemOrder(poNum));
		modelAndView.getModelMap().addAttribute("itemobj", tempobj_info);

		String[] purchaseorderuserArray = purchaseorderuser.toString().split("-");

		for (int i = 0; i < userlist.size(); i++) {

			if (userlist.get(i).getId().equals(purchaseorderuserArray[0].trim())) {
				int keyLocation = userlist.indexOf(userlist.get(i));
				userlist.remove(keyLocation);

			}

		}

		modelAndView.addObject("profitCenterlist", getProfitCenterCodeAndProfitcenterName());
		modelAndView.addObject("itemNolist", itemInfoRepo.getAllItemInformation());
		modelAndView.addObject("taxCodelist", gstrepo.getAllActiveGoodsServicesTax());
		modelAndView.addObject("iteminfoobj", new ItemInfoForm());

		ItemForm itemobj = new ItemForm();
		itemobj.setItemOrder(getItemOrder(poNum));
		modelAndView.addObject("itemobj", itemobj);

		modelAndView.addObject("credittermscodelist", crtService.getAllActiveCreditTerms());

		List<OrderCategory> orderCat_list = orderCatService.getAllActiveOrderCategory();
		if (orderCat_list.size() == 0) {
			empty_ordlist = true;
			modelAndView.addObject("empty_ordlist", empty_ordlist);
		}

		modelAndView.addObject("ordCatlist", orderCat_list);

		List<OrderSubCategory> ordersubCat_list = orderSubCatService.getAllActiveOrderSubCategory();
		if (ordersubCat_list.size() == 0) {
			empty_ordsublist = true;
			modelAndView.addObject("empty_ordsublist", empty_ordsublist);
		}
		modelAndView.addObject("ordSubClist", ordersubCat_list);
		modelAndView.addObject("projectCodelist", projectService.getAllActiveProject());
		modelAndView.addObject("poUserlist", verifier_info());
		modelAndView.addObject("profitCenter_store", populateIssuerTabStoreinfo(profitCenterCode.getCode()));
		modelAndView.addObject("profitCenter_department", populateIssuerTabDeptinfo(profitCenterCode.getCode()));
		modelAndView.addObject("profitCenterCode", profitCenterCode);
		modelAndView.addObject("employee", employee);
		modelAndView.addObject("purchaseorderuser", purchaseorderuser);
		modelAndView.addObject("userlist", authorizor_info());
		modelAndView.addObject("vendor", populateVendorTabvendinfo());
		modelAndView.addObject("vendorlist", vendorlist);
		modelAndView.addObject("date_format", dateFormat);
		modelAndView.addObject("poNumber", poNum);
		modelAndView.addObject("currencyList", currencyList());
		modelAndView.addObject("prtypeList", purchaserequ);
		modelAndView.addObject("projectList", projectList);
		modelAndView.addObject("AuthorizationGrid", orderAuthGrid);

		modelAndView.addObject("newpath", newPath);
		modelAndView.addObject("path_edit", path_edit);
		modelAndView.addObject("profitcenterlist", populateProfitCenter());
		modelAndView.addObject("choosen_view", choosen_view);

		modelAndView.addObject("lastcrt", crt_edit);

		PoMainForm obj = tempobj != null ? tempobj : new PoMainForm();
		modelAndView.getModelMap().addAttribute("pomainobj", obj);
		modelAndView.addObject("activetab", obj.getActivetab());
		modelAndView.addObject("lastcrt_used", obj.getLastcrt_used_hidden());
		modelAndView.addObject("quotNo", obj.getQuoteNo());
		modelAndView.addObject("quotDate", obj.getQuoteDate());
		modelAndView.addObject("vendincharge", obj.getVendorPersonInCharge());

		return modelAndView;
	}

	@GetMapping("/main/edit_draftpo")
	public ModelAndView onCreatePOLoad_draft(RedirectAttributes redirectAttributes) {

		String dateFormat = "dd-MM-yyyy";
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_FORMAT");
		if (dateObj != null && !dateObj.getPropValue().isEmpty()) {
			dateFormat = dateObj.getPropValue();
		}
		authDir = sysRepo.getSystemParameterValueByPropName(AUTHGRID_UPLOAD_PATH);

		ModelAndView modelAndView = new ModelAndView("/main/edit_draftpo");

		purchaseorderuser = poUserService.getPurchaseOrderUserById(getCurrentUser().get());
		employee = employeeService.getEmployeeMasterByPsId(purchaseorderuser.getId().trim());
		if (employee != null) {
			employeeList = employeeService
					.getAllEmployeebyProfitCenterCode(employee.getProfitCenter().getCode().toString(), true);
			profitCenterCode = profitCenterService
					.getProfitCenterByCode(employee.getProfitCenter().getCode().toString());
		} else {

			redirectAttributes.addFlashAttribute("error", "No Employee Found!");
			ModelAndView modelAndView_draft = new ModelAndView("/main/draftpo");
			return modelAndView_draft;
		}

		boolean active = true;
		profitcenterlist = profitCenterService.getAllProfitCenterByCompanyCode(getCompany().get(), active);
		if (profitcenterlist.size() > 0) {
			modelAndView.addObject("profitcenterlist", profitcenterlist);
		} else {
			redirectAttributes.addFlashAttribute("error", "No Profit Center List Found For This Company!");
			ModelAndView modelAndView_draft = new ModelAndView("/main/draftpo");
			return modelAndView_draft;
		}
		vendorlist = vbService.getAllActiveVendorBranchBycompCode(getCompany().get());

		if (vendorlist.size() == 0) {
			empty_vendlist = true;
			modelAndView.addObject("empty_vendorlist", empty_vendlist);
		}
		purchaserequ = prService.getAllActivePurchaseRequisition();
		if (purchaserequ.size() == 0) {
			empty_prtypelist = true;
			modelAndView.addObject("empty_prtypelist", empty_prtypelist);

		}

		userlist = poUserService.getAllActivePurchaseOrderUser();

		String[] purchaseorderuserArray = purchaseorderuser.toString().split("-");

		for (int i = 0; i < userlist.size(); i++) {

			if (userlist.get(i).getId().equals(purchaseorderuserArray[0].trim())) {
				int keyLocation = userlist.indexOf(userlist.get(i));
				userlist.remove(keyLocation);

			}

		}
		modelAndView.addObject("profitCenterlist", getProfitCenterCodeAndProfitcenterName());
		modelAndView.addObject("itemNolist", itemInfoRepo.getAllItemInformation());
		modelAndView.addObject("taxCodelist", gstrepo.getAllActiveGoodsServicesTax());
		modelAndView.addObject("iteminfoobj", new ItemInfoForm());

		ItemForm itemobj = new ItemForm();
		itemobj.setItemOrder(getItemOrder(poNum));
		modelAndView.addObject("itemobj", itemobj);

		List<OrderCategory> orderCat_list = orderCatService.getAllActiveOrderCategory();
		if (orderCat_list.size() == 0) {

			empty_ordlist = true;
			modelAndView.addObject("empty_ordlist", empty_ordlist);
		}

		if (orderCat_list.size() > 0) {
			modelAndView.addObject("ordCatlist", orderCat_list);
		}

		List<OrderSubCategory> ordersubCat_list = orderSubCatService.getAllActiveOrderSubCategory();
		if (ordersubCat_list.size() == 0) {
			empty_ordsublist = true;
			modelAndView.addObject("empty_ordsublist", empty_ordsublist);
		}

		if (ordersubCat_list.size() > 0) {
			modelAndView.addObject("ordSubClist", ordersubCat_list);
		}
		modelAndView.addObject("credittermscodelist", crtService.getAllActiveCreditTerms());

		modelAndView.addObject("projectCodelist", projectService.getAllActiveProject());

		// if(poUserService.getAllActivePurchaseOrderUser()!=null){
		modelAndView.addObject("poUserlist", verifier_info());
		// }
		modelAndView.addObject("profitCenterCode", profitCenterCode);
		modelAndView.addObject("employee", employee);
		modelAndView.addObject("employeeList", employeeList);
		modelAndView.addObject("purchaseorderuser", purchaseorderuser);
		// if(userlist.size()>0){
		modelAndView.addObject("userlist", authorizor_info());
		// }
		modelAndView.addObject("vendor", populateVendorTabvendinfo());
		modelAndView.addObject("vendorlist", vendorlist);
		modelAndView.addObject("date_format", dateFormat);
		modelAndView.addObject("contact_person_List", poUserService.getAllActivePurchaseOrderUser());
		modelAndView.addObject("currencyList", currencyList());

		if (purchaserequ.size() > 0) {
			modelAndView.addObject("prtypeList", purchaserequ);
		}
		modelAndView.addObject("AuthorizationGrid", orderAuthGrid);

		modelAndView.addObject("newpath", newPath);
		modelAndView.addObject("path_edit", path_edit);
		modelAndView.addObject("issuedon", issuedon);
		modelAndView.addObject("verifiedon", verifiedon);
		modelAndView.addObject("deletedon", deletedon);
		modelAndView.addObject("rejectedon", rejectedon);
		modelAndView.addObject("authorizedon", authorizedon);

		List<ProfitCenter> active_pc = profitCenterService.getAllProfitCenterByCompanyCode(getCompany().get(), active);
		if (active_pc.size() == 0) {
			empty_activepclist = true;
			modelAndView.addObject("empty_pc_list", empty_activepclist);
		}
		modelAndView.addObject("profitcenterlist", active_pc);
		modelAndView.addObject("choosen_view", choosen_view);

		modelAndView.addObject("lastcrt", crt_edit);

		PoMainForm obj = tempobj_draft != null ? tempobj_draft : new PoMainForm();
		modelAndView.getModelMap().addAttribute("pomainobj", obj);

		modelAndView.addObject("activetab", obj.getActivetab());
		modelAndView.addObject("lastcrt_used", obj.getLastcrt_used_hidden());
		modelAndView.addObject("quotNo", obj.getQuoteNo() != null ? obj.getQuoteNo() : "");
		modelAndView.addObject("poNumber", obj.getPoNo());
		modelAndView.addObject("status", "Draft");
		modelAndView.addObject("quotDate", obj.getQuoteDate() != null ? obj.getQuoteDate() : "");
		modelAndView.addObject("vendincharge", obj.getVendorPersonInCharge());
		modelAndView.addObject("totExclGST", Double.valueOf((obj.getTotExclGST())));

		// if(pc_info_list.size()>0){
		modelAndView.addObject("pc_validity_list", pc_info_list);
		// }

		// if(vendor_info_list.size()>0){
		modelAndView.addObject("vend_validity_list", vendor_info_list);
		// }

		/* modelAndView.addObject("pc_chk_active", pc_chk_active); */
		modelAndView.addObject("st_new", st_new);
		modelAndView.addObject("dept_new", dept_new);
		modelAndView.addObject("add1_new", add1_new);
		modelAndView.addObject("add2_new", add2_new);
		modelAndView.addObject("add3_new", add3_new);
		modelAndView.addObject("postcode_new", postcode_new);
		modelAndView.addObject("postarea_new", postarea_new);

		modelAndView.addObject("vend_name_new", vend_name_new);
		modelAndView.addObject("vend_number_new", vend_number_new);
		modelAndView.addObject("vend_email_new", vend_email_new);
		modelAndView.addObject("vend_add1_new", vend_add1_new);
		modelAndView.addObject("vend_add2_new", vend_add2_new);
		modelAndView.addObject("vend_add3_new", vend_add3_new);
		modelAndView.addObject("vend_postcode_new", vend_postcode_new);
		modelAndView.addObject("vend_postarea_new", vend_postarea_new);

		String[] pcArray = obj.getProfitcenter().split("-");

		if (pcArray[0] != null) {
			projectList = projectService.getActiveProjectByProfitCenterCode(pcArray[0]);
			if (projectList.size() > 0) {
				modelAndView.addObject("projectList", projectList);
			}
		}

		if (!obj.getIssuer().equals(obj.getVerifier())) {

			disable_verify_button = true;

		} else {

			disable_verify_button = false;
		}
		modelAndView.addObject("disable_verify_button", disable_verify_button);

		return modelAndView;
	}

	@GetMapping("/main/edit_re_draftpo")
	public ModelAndView onCreatePOLoad_redrfat() {

		authDir = sysRepo.getSystemParameterValueByPropName(AUTHGRID_UPLOAD_PATH);

		ModelAndView modelAndView = new ModelAndView("/main/edit_re_draftpo");
		String dateFormat = "dd-MM-yyyy";
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_FORMAT");
		if (dateObj != null && !dateObj.getPropValue().isEmpty()) {
			dateFormat = dateObj.getPropValue();
		}
		purchaseorderuser = poUserService.getPurchaseOrderUserById(getCurrentUser().get());
		employee = employeeService.getEmployeeMasterByPsId(purchaseorderuser.getId());

		profitCenterCode = profitCenterService.getProfitCenterByCode(employee.getProfitCenter().getCode().toString());
		boolean active = true;
		profitcenterlist = profitCenterService.getAllProfitCenterByCompanyCode(getCompany().get(), active);
		vendorlist = vbService.getAllActiveVendorBranchBycompCode(getCompany().get());
		purchaserequ = prService.getAllActivePurchaseRequisition();

		userlist = poUserService.getAllActivePurchaseOrderUser();

		String[] purchaseorderuserArray = purchaseorderuser.toString().split("-");

		for (int i = 0; i < userlist.size(); i++) {

			if (userlist.get(i).getId().equals(purchaseorderuserArray[0].trim())) {
				int keyLocation = userlist.indexOf(userlist.get(i));
				userlist.remove(keyLocation);

			}

		}

		modelAndView.addObject("profitCenterlist", getProfitCenterCodeAndProfitcenterName());
		modelAndView.addObject("itemNolist", itemInfoRepo.getAllItemInformation());
		modelAndView.addObject("taxCodelist", gstrepo.getAllActiveGoodsServicesTax());
		modelAndView.addObject("iteminfoobj", new ItemInfoForm());

		ItemForm itemobj = new ItemForm();
		itemobj.setItemOrder(getItemOrder(poNum));
		modelAndView.addObject("itemobj", itemobj);

		modelAndView.addObject("credittermscodelist", crtService.getAllActiveCreditTerms());
		modelAndView.addObject("ordCatlist", orderCatService.getAllActiveOrderCategory());
		modelAndView.addObject("ordSubClist", orderSubCatService.getAllActiveOrderSubCategory());
		modelAndView.addObject("projectCodelist", projectService.getAllActiveProject());
		modelAndView.addObject("poUserlist", poUserService.getAllActivePurchaseOrderUser());

		modelAndView.addObject("profitCenterCode", profitCenterCode);
		modelAndView.addObject("employee", employee);
		modelAndView.addObject("employeeList", employeeList);
		modelAndView.addObject("purchaseorderuser", purchaseorderuser);
		modelAndView.addObject("userlist", userlist);
		modelAndView.addObject("vendor", populateVendorTabvendinfo());
		modelAndView.addObject("vendorlist", vendorlist);
		modelAndView.addObject("date_format", dateFormat);

		modelAndView.addObject("currencyList", currencyList());
		modelAndView.addObject("prtypeList", purchaserequ);

		modelAndView.addObject("AuthorizationGrid", orderAuthGrid);

		modelAndView.addObject("pono_duplicate", pono_duplicate);

		modelAndView.addObject("newpath", newPath);
		modelAndView.addObject("path_edit", path_edit);
		modelAndView.addObject("issuedon", issuedon);
		modelAndView.addObject("verifiedon", verifiedon);
		modelAndView.addObject("deletedon", deletedon);
		modelAndView.addObject("rejectedon", rejectedon);
		modelAndView.addObject("authorizedon", authorizedon);
		modelAndView.addObject("contact_person_List", poUserService.getAllActivePurchaseOrderUser());
		modelAndView.addObject("profitcenterlist", populateProfitCenter());
		modelAndView.addObject("choosen_view", choosen_view);

		modelAndView.addObject("lastcrt", crt_edit);

		PoMainForm obj = tempobj_redraft != null ? tempobj_redraft : new PoMainForm();
		modelAndView.getModelMap().addAttribute("pomainobj", obj);

		modelAndView.addObject("activetab", obj.getActivetab());
		modelAndView.addObject("lastcrt_used", obj.getLastcrt_used());
		modelAndView.addObject("quotNo", obj.getQuoteNo());
		modelAndView.addObject("quotDate", obj.getQuoteDate());
		modelAndView.addObject("poNumber", obj.getPoNo());
		modelAndView.addObject("status", obj.getStatus());
		modelAndView.addObject("vendincharge", obj.getVendorPersonInCharge());
		String[] pcArray = obj.getProfitcenter().split("-");
		projectList = projectService.getActiveProjectByProfitCenterCode(pcArray[0]);
		modelAndView.addObject("projectList", projectList);

		if (!obj.getIssuer().equals(obj.getVerifier())) {

			disable_verify_button = true;

		} else {

			disable_verify_button = false;
		}
		modelAndView.addObject("disable_verify_button", disable_verify_button);

		return modelAndView;
	}

	@GetMapping("/main/duplicate_po")
	public ModelAndView onCreatePOLoad_duplicate() {

		authDir = sysRepo.getSystemParameterValueByPropName(AUTHGRID_UPLOAD_PATH);

		ModelAndView modelAndView = new ModelAndView("/main/duplicate_po");
		String dateFormat = "dd-MM-yyyy";
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_FORMAT");
		if (dateObj != null && !dateObj.getPropValue().isEmpty()) {
			dateFormat = dateObj.getPropValue();
		}
		purchaseorderuser = poUserService.getPurchaseOrderUserById(getCurrentUser().get());
		employee = employeeService.getEmployeeMasterByPsId(purchaseorderuser.getId());

		profitCenterCode = profitCenterService.getProfitCenterByCode(employee.getProfitCenter().getCode().toString());
		boolean active = true;
		profitcenterlist = profitCenterService.getAllProfitCenterByCompanyCode(getCompany().get(), active);
		vendorlist = vbService.getAllActiveVendorBranchBycompCode(getCompany().get());
		purchaserequ = prService.getAllActivePurchaseRequisition();

		userlist = poUserService.getAllActivePurchaseOrderUser();

		String[] purchaseorderuserArray = purchaseorderuser.toString().split("-");

		for (int i = 0; i < userlist.size(); i++) {

			if (userlist.get(i).getId().equals(purchaseorderuserArray[0].trim())) {
				int keyLocation = userlist.indexOf(userlist.get(i));
				userlist.remove(keyLocation);

			}

		}

		modelAndView.addObject("credittermscodelist", crtService.getAllActiveCreditTerms());
		modelAndView.addObject("ordCatlist", orderCatService.getAllActiveOrderCategory());
		modelAndView.addObject("ordSubClist", orderSubCatService.getAllActiveOrderSubCategory());
		modelAndView.addObject("projectCodelist", projectService.getAllActiveProject());
		modelAndView.addObject("poUserlist", poUserService.getAllActivePurchaseOrderUser());

		modelAndView.addObject("profitCenterCode", profitCenterCode);
		modelAndView.addObject("employee", employee);
		modelAndView.addObject("employeeList", employeeList);
		modelAndView.addObject("purchaseorderuser", purchaseorderuser);
		modelAndView.addObject("userlist", userlist);
		modelAndView.addObject("vendor", populateVendorTabvendinfo());
		modelAndView.addObject("vendorlist", vendorlist);
		modelAndView.addObject("date_format", dateFormat);

		modelAndView.addObject("currencyList", currencyList());
		modelAndView.addObject("prtypeList", purchaserequ);

		modelAndView.addObject("AuthorizationGrid", orderAuthGrid);

		modelAndView.addObject("pono_duplicate", pono_duplicate);

		modelAndView.addObject("newpath", newPath);
		modelAndView.addObject("path_edit", path_edit);
		modelAndView.addObject("issuedon", issuedon);
		modelAndView.addObject("verifiedon", verifiedon);
		modelAndView.addObject("deletedon", deletedon);
		modelAndView.addObject("rejectedon", rejectedon);
		modelAndView.addObject("authorizedon", authorizedon);

		modelAndView.addObject("profitcenterlist", populateProfitCenter());
		modelAndView.addObject("choosen_view", choosen_view);

		modelAndView.addObject("lastcrt", crt_edit);

		PoMainForm obj = tempobj_duplicate != null ? tempobj_duplicate : new PoMainForm();
		modelAndView.getModelMap().addAttribute("pomainobj", obj);
		modelAndView.addObject("profitCenter_store", populateIssuerTabStoreinfo(obj.getPcCode()));
		modelAndView.addObject("profitCenter_department", populateIssuerTabDeptinfo(obj.getPcCode()));
		modelAndView.addObject("activetab", obj.getActivetab());
		modelAndView.addObject("lastcrt_used", obj.getLastcrt_used());
		modelAndView.addObject("quotNo", obj.getQuoteNo());
		modelAndView.addObject("quotDate", obj.getQuoteDate());
		modelAndView.addObject("poNumber", obj.getPoNo());
		modelAndView.addObject("vendincharge", obj.getVendorPersonInCharge());
		String[] pcArray = obj.getProfitcenter().split("-");
		projectList = projectService.getActiveProjectByProfitCenterCode(pcArray[0]);
		modelAndView.addObject("projectList", projectList);
		if (!obj.getIssuer().equals(obj.getVerifier())) {

			disable_verify_button = true;

		} else {

			disable_verify_button = false;
		}
		modelAndView.addObject("disable_verify_button", disable_verify_button);

		return modelAndView;
	}

	@GetMapping("/main/edit_issuedpo")
	public ModelAndView onCreatePOLoad_issued() {

		authDir = sysRepo.getSystemParameterValueByPropName(AUTHGRID_UPLOAD_PATH);

		ModelAndView modelAndView = new ModelAndView("/main/edit_issuedpo");
		String dateFormat = "dd-MM-yyyy";
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_FORMAT");
		if (dateObj != null && !dateObj.getPropValue().isEmpty()) {
			dateFormat = dateObj.getPropValue();
		}
		purchaseorderuser = poUserService.getPurchaseOrderUserById(getCurrentUser().get());
		employee = employeeService.getEmployeeMasterByPsId(purchaseorderuser.getId());
		profitCenterCode = profitCenterService.getProfitCenterByCode(employee.getProfitCenter().getCode().toString());
		boolean active = true;
		profitcenterlist = profitCenterService.getAllProfitCenterByCompanyCode(getCompany().get(), active);
		vendorlist = vbService.getAllActiveVendorBranchBycompCode(getCompany().get());
		purchaserequ = prService.getAllActivePurchaseRequisition();

		userlist = poUserService.getAllActivePurchaseOrderUser();
		forward_verifylist = poUserService.getAllActivePurchaseOrderUser();
		forward_invoicerlist = poUserService.getAllActivePurchaseOrderUser();

		String[] purchaseorderuserArray = purchaseorderuser.toString().split("-");

		for (int i = 0; i < userlist.size(); i++) {

			if (userlist.get(i).getId().equals(purchaseorderuserArray[0].trim())) {
				int keyLocation = userlist.indexOf(userlist.get(i));
				userlist.remove(keyLocation);

			}

		}

		modelAndView.addObject("profitCenterlist", getProfitCenterCodeAndProfitcenterName());
		modelAndView.addObject("itemNolist", itemInfoRepo.getAllItemInformation());
		modelAndView.addObject("taxCodelist", gstrepo.getAllActiveGoodsServicesTax());
		modelAndView.addObject("iteminfoobj", new ItemInfoForm());

		ItemForm itemobj = new ItemForm();
		itemobj.setItemOrder(getItemOrder(poNum));
		modelAndView.addObject("itemobj", itemobj);

		modelAndView.addObject("credittermscodelist", crtService.getAllActiveCreditTerms());
		modelAndView.addObject("ordCatlist", orderCatService.getAllActiveOrderCategory());
		modelAndView.addObject("ordSubClist", orderSubCatService.getAllActiveOrderSubCategory());
		modelAndView.addObject("projectCodelist", projectService.getAllActiveProject());
		modelAndView.addObject("poUserlist", verifier_info());

		modelAndView.addObject("profitCenterCode", profitCenterCode);
		modelAndView.addObject("employee", employee);
		modelAndView.addObject("employeeList", employeeList);
		modelAndView.addObject("purchaseorderuser", purchaseorderuser);
		modelAndView.addObject("userlist", authorizor_info());
		modelAndView.addObject("vendor", populateVendorTabvendinfo());
		modelAndView.addObject("vendorlist", vendorlist);
		modelAndView.addObject("date_format", dateFormat);

		modelAndView.addObject("currencyList", currencyList());
		modelAndView.addObject("prtypeList", purchaserequ);
		modelAndView.addObject("AuthorizationGrid", orderAuthGrid);

		modelAndView.addObject("newpath", newPath);
		modelAndView.addObject("path_edit", path_edit);
		modelAndView.addObject("issuedon", issuedon);
		modelAndView.addObject("verifiedon", verifiedon);
		modelAndView.addObject("deletedon", deletedon);
		modelAndView.addObject("rejectedon", rejectedon);
		modelAndView.addObject("authorizedon", authorizedon);
		modelAndView.addObject("profitcenterlist", populateProfitCenter());
		modelAndView.addObject("choosen_view", choosen_view);

		modelAndView.addObject("lastcrt", crt_edit);

		modelAndView.addObject("inactive_pc", inactive_pc);

		PoMainForm obj = tempobj_issued != null ? tempobj_issued : new PoMainForm();
		modelAndView.getModelMap().addAttribute("pomainobj", obj);

		modelAndView.addObject("activetab", obj.getActivetab());
		modelAndView.addObject("poNumber", obj.getPoNo());
		modelAndView.addObject("status", obj.getStatus());
		modelAndView.addObject("lastcrt_used", obj.getLastcrt_used());
		modelAndView.addObject("quotNo", obj.getQuoteNo());
		modelAndView.addObject("quotDate", obj.getQuoteDate());
		modelAndView.addObject("vendincharge", obj.getVendorPersonInCharge());

		modelAndView.addObject("pc_validity_list", pc_info_list);
		modelAndView.addObject("vend_validity_list", vendor_info_list);

		/* modelAndView.addObject("pc_chk_active", pc_chk_active); */
		modelAndView.addObject("st_new", st_new);
		modelAndView.addObject("dept_new", dept_new);
		modelAndView.addObject("add1_new", add1_new);
		modelAndView.addObject("add2_new", add2_new);
		modelAndView.addObject("add3_new", add3_new);
		modelAndView.addObject("postcode_new", postcode_new);
		modelAndView.addObject("postarea_new", postarea_new);

		modelAndView.addObject("vend_name_new", vend_name_new);
		modelAndView.addObject("vend_number_new", vend_number_new);
		modelAndView.addObject("vend_email_new", vend_email_new);
		modelAndView.addObject("vend_add1_new", vend_add1_new);
		modelAndView.addObject("vend_add2_new", vend_add2_new);
		modelAndView.addObject("vend_add3_new", vend_add3_new);
		modelAndView.addObject("vend_postcode_new", vend_postcode_new);
		modelAndView.addObject("vend_postarea_new", vend_postarea_new);

		verifier_forward = obj.getVerifier();
		modelAndView.addObject("verifier", verifier_forward);

		String issuer = obj.getIssuer().trim();
		String authorizer = obj.getAuthorizer().trim();
		List<String> invoicer_list = new ArrayList<>();

		invoicer_list.add(issuer);
		if (!issuer.equals(verifier_forward)) {
			invoicer_list.add(verifier_forward.trim());
		}

		if (!verifier_forward.equals(authorizer)) {
			invoicer_list.add(authorizer);
		}

		if (!obj.getIssuer().equals(obj.getVerifier())) {

			disable_verify_button_issuer = false;
		} else {
			disable_verify_button_issuer = true;

		}

		if (extract_issuer(obj.getIssuer()).equals(getCurrentUser().get())) {

			disable_reject_button = true;
		} else {
			disable_reject_button = false;

		}

		for (int i = 0; i < forward_verifylist.size(); i++) {

			if (forward_verifylist.get(i).getId().equals(extract_issuer(verifier_forward.trim()))) {
				int keyLocation = forward_verifylist.indexOf(forward_verifylist.get(i));
				forward_verifylist.remove(keyLocation);

			}

		}

		modelAndView.addObject("forward_verifylist", forward_verifylist);
		/* modelAndView.addObject("invoicer_list", invoicer_list); */
		modelAndView.addObject("invoicer_list", forward_invoicerlist);
		modelAndView.addObject("disable_verify_button_issuer", disable_verify_button_issuer);
		modelAndView.addObject("disable_reject_button", disable_reject_button);

		return modelAndView;
	}

	@GetMapping("/main/edit_verifiedpo")
	public ModelAndView onCreatePOLoad_verified() {

		authDir = sysRepo.getSystemParameterValueByPropName(AUTHGRID_UPLOAD_PATH);

		ModelAndView modelAndView = new ModelAndView("/main/edit_verifiedpo");
		String dateFormat = "dd-MM-yyyy";
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_FORMAT");
		if (dateObj != null && !dateObj.getPropValue().isEmpty()) {
			dateFormat = dateObj.getPropValue();
		}
		purchaseorderuser = poUserService.getPurchaseOrderUserById(getCurrentUser().get());
		employee = employeeService.getEmployeeMasterByPsId(purchaseorderuser.getId());

		profitCenterCode = profitCenterService.getProfitCenterByCode(employee.getProfitCenter().getCode().toString());
		boolean active = true;
		profitcenterlist = profitCenterService.getAllProfitCenterByCompanyCode(getCompany().get(), active);
		vendorlist = vbService.getAllActiveVendorBranchBycompCode(getCompany().get());
		purchaserequ = prService.getAllActivePurchaseRequisition();

		userlist = poUserService.getAllActivePurchaseOrderUser();

		forward_authorizelist = poUserService.getAllActivePurchaseOrderUser();
		forward_invoicerlist = poUserService.getAllActivePurchaseOrderUser();

		String[] purchaseorderuserArray = purchaseorderuser.toString().split("-");

		for (int i = 0; i < userlist.size(); i++) {

			if (userlist.get(i).getId().equals(purchaseorderuserArray[0].trim())) {
				int keyLocation = userlist.indexOf(userlist.get(i));
				userlist.remove(keyLocation);

			}

		}

		modelAndView.addObject("profitCenterlist", getProfitCenterCodeAndProfitcenterName());
		modelAndView.addObject("itemNolist", itemInfoRepo.getAllItemInformation());
		modelAndView.addObject("taxCodelist", gstrepo.getAllActiveGoodsServicesTax());
		modelAndView.addObject("iteminfoobj", new ItemInfoForm());

		ItemForm itemobj = new ItemForm();
		itemobj.setItemOrder(getItemOrder(poNum));
		modelAndView.addObject("itemobj", itemobj);

		modelAndView.addObject("credittermscodelist", crtService.getAllActiveCreditTerms());
		modelAndView.addObject("ordCatlist", orderCatService.getAllActiveOrderCategory());
		modelAndView.addObject("ordSubClist", orderSubCatService.getAllActiveOrderSubCategory());
		modelAndView.addObject("projectCodelist", projectService.getAllActiveProject());
		modelAndView.addObject("poUserlist", poUserService.getAllActivePurchaseOrderUser());

		modelAndView.addObject("profitCenterCode", profitCenterCode);
		modelAndView.addObject("employee", employee);
		modelAndView.addObject("employeeList", employeeList);
		modelAndView.addObject("purchaseorderuser", purchaseorderuser);
		modelAndView.addObject("userlist", userlist);
		modelAndView.addObject("vendor", populateVendorTabvendinfo());
		modelAndView.addObject("vendorlist", vendorlist);
		modelAndView.addObject("date_format", dateFormat);

		modelAndView.addObject("currencyList", currencyList());
		modelAndView.addObject("prtypeList", purchaserequ);

		modelAndView.addObject("AuthorizationGrid", orderAuthGrid);

		modelAndView.addObject("newpath", newPath);
		modelAndView.addObject("path_edit", path_edit);
		modelAndView.addObject("issuedon", issuedon);
		modelAndView.addObject("verifiedon", verifiedon);
		modelAndView.addObject("deletedon", deletedon);
		modelAndView.addObject("rejectedon", rejectedon);
		modelAndView.addObject("cancelledon", cancelledon);
		modelAndView.addObject("authorizedon", authorizedon);
		modelAndView.addObject("profitcenterlist", populateProfitCenter());
		modelAndView.addObject("choosen_view", choosen_view);

		modelAndView.addObject("pc_validity_list", pc_info_list);
		modelAndView.addObject("vend_validity_list", vendor_info_list);

		/* modelAndView.addObject("pc_chk_active", pc_chk_active); */
		modelAndView.addObject("st_new", st_new);
		modelAndView.addObject("dept_new", dept_new);
		modelAndView.addObject("add1_new", add1_new);
		modelAndView.addObject("add2_new", add2_new);
		modelAndView.addObject("add3_new", add3_new);
		modelAndView.addObject("postcode_new", postcode_new);
		modelAndView.addObject("postarea_new", postarea_new);

		modelAndView.addObject("vend_name_new", vend_name_new);
		modelAndView.addObject("vend_number_new", vend_number_new);
		modelAndView.addObject("vend_email_new", vend_email_new);
		modelAndView.addObject("vend_add1_new", vend_add1_new);
		modelAndView.addObject("vend_add2_new", vend_add2_new);
		modelAndView.addObject("vend_add3_new", vend_add3_new);
		modelAndView.addObject("vend_postcode_new", vend_postcode_new);
		modelAndView.addObject("vend_postarea_new", vend_postarea_new);

		modelAndView.addObject("lastcrt", crt_edit);
		modelAndView.addObject("inactive_pc", inactive_pc);

		PoMainForm obj = tempobj_verified != null ? tempobj_verified : new PoMainForm();
		modelAndView.getModelMap().addAttribute("pomainobj", obj);

		modelAndView.addObject("activetab", obj.getActivetab());
		modelAndView.addObject("lastcrt_used", obj.getLastcrt_used());
		modelAndView.addObject("quotNo", obj.getQuoteNo());
		modelAndView.addObject("poNumber", obj.getPoNo());
		modelAndView.addObject("quotDate", obj.getQuoteDate());
		modelAndView.addObject("vendincharge", obj.getVendorPersonInCharge());
		modelAndView.addObject("status", obj.getStatus());

		// List<String> forward_invoicerlist = new ArrayList<>();
		String issuer = obj.getIssuer();
		String authorizer = obj.getAuthorizer().trim();
		String verifier = obj.getVerifier().trim();

		// forward_invoicerlist.add(issuer);
		// if (!issuer.equals(verifier)) {
		// forward_invoicerlist.add(verifier);
		// }

		// if (!verifier.equals(authorizer)) {
		// forward_invoicerlist.add(authorizer);
		// }

		/*
		 * Remove Current Authorizer, Issuer and verifier if issuer and verifier are
		 * same from forward Authorizer list
		 */

		for (int i = 0; i < forward_authorizelist.size(); i++) {

			if (forward_authorizelist.get(i).getId().equals(extract_issuer(authorizer))) {
				int keyLocation = forward_authorizelist.indexOf(forward_authorizelist.get(i));
				forward_authorizelist.remove(keyLocation);

			}

		}

		for (int i = 0; i < forward_authorizelist.size(); i++) {

			if (forward_authorizelist.get(i).getId().equals(extract_issuer(issuer))) {
				int keyLocation = forward_authorizelist.indexOf(forward_authorizelist.get(i));
				forward_authorizelist.remove(keyLocation);

			}

		}

		for (int i = 0; i < forward_authorizelist.size(); i++) {
			if (issuer.equals(verifier)) {
				if (forward_authorizelist.get(i).getId().equals(extract_issuer(verifier))) {
					int keyLocation = forward_authorizelist.indexOf(forward_authorizelist.get(i));
					forward_authorizelist.remove(keyLocation);

				}
			}
		}

		/* current user should not be able to authorize the po */
		if (!getCurrentUser().get().equals(extract_issuer(authorizer))) {

			disable_authorize_button_verified = false;
		} else {
			disable_authorize_button_verified = true;

		}

		modelAndView.addObject("forwardauthlist_ids", forward_authorizelist);
		modelAndView.addObject("forward_invoicerlist", forward_invoicerlist);
		modelAndView.addObject("disable_authorize_button_verified", disable_authorize_button_verified);

		return modelAndView;
	}

	@GetMapping("/main/edit_authorizedpo")
	public ModelAndView onCreatePOLoad_authorized() {

		authDir = sysRepo.getSystemParameterValueByPropName(AUTHGRID_UPLOAD_PATH);

		ModelAndView modelAndView = new ModelAndView("/main/edit_authorizedpo");
		String dateFormat = "dd-MM-yyyy";
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_FORMAT");
		if (dateObj != null && !dateObj.getPropValue().isEmpty()) {
			dateFormat = dateObj.getPropValue();
		}
		purchaseorderuser = poUserService.getPurchaseOrderUserById(getCurrentUser().get());
		employee = employeeService.getEmployeeMasterByPsId(purchaseorderuser.getId());

		profitCenterCode = profitCenterService.getProfitCenterByCode(employee.getProfitCenter().getCode().toString());
		boolean active = true;
		profitcenterlist = profitCenterService.getAllProfitCenterByCompanyCode(getCompany().get(), active);
		vendorlist = vbService.getAllActiveVendorBranchBycompCode(getCompany().get());
		purchaserequ = prService.getAllActivePurchaseRequisition();

		userlist = poUserService.getAllActivePurchaseOrderUser();

		String[] purchaseorderuserArray = purchaseorderuser.toString().split("-");

		for (int i = 0; i < userlist.size(); i++) {

			if (userlist.get(i).getId().equals(purchaseorderuserArray[0].trim())) {
				int keyLocation = userlist.indexOf(userlist.get(i));
				userlist.remove(keyLocation);

			}

		}

		modelAndView.addObject("profitCenterlist", getProfitCenterCodeAndProfitcenterName());
		modelAndView.addObject("itemNolist", itemInfoRepo.getAllItemInformation());
		modelAndView.addObject("taxCodelist", gstrepo.getAllActiveGoodsServicesTax());
		modelAndView.addObject("iteminfoobj", new ItemInfoForm());

		ItemForm itemobj = new ItemForm();
		itemobj.setItemOrder(getItemOrder(poNum));
		modelAndView.addObject("itemobj", itemobj);

		modelAndView.addObject("credittermscodelist", crtService.getAllActiveCreditTerms());
		modelAndView.addObject("ordCatlist", orderCatService.getAllActiveOrderCategory());
		modelAndView.addObject("ordSubClist", orderSubCatService.getAllActiveOrderSubCategory());
		modelAndView.addObject("projectCodelist", projectService.getAllActiveProject());
		modelAndView.addObject("poUserlist", poUserService.getAllActivePurchaseOrderUser());

		modelAndView.addObject("profitCenterCode", profitCenterCode);
		modelAndView.addObject("employee", employee);
		modelAndView.addObject("employeeList", employeeList);
		modelAndView.addObject("purchaseorderuser", purchaseorderuser);
		modelAndView.addObject("userlist", userlist);
		modelAndView.addObject("vendor", populateVendorTabvendinfo());
		modelAndView.addObject("vendorlist", vendorlist);
		modelAndView.addObject("date_format", dateFormat);

		modelAndView.addObject("currencyList", currencyList());
		modelAndView.addObject("prtypeList", purchaserequ);

		modelAndView.addObject("AuthorizationGrid", orderAuthGrid);

		modelAndView.addObject("newpath", newPath);
		modelAndView.addObject("path_edit", path_edit);
		modelAndView.addObject("issuedon", issuedon);
		modelAndView.addObject("verifiedon", verifiedon);
		modelAndView.addObject("deletedon", deletedon);
		modelAndView.addObject("rejectedon", rejectedon);
		modelAndView.addObject("cancelledon", cancelledon);
		modelAndView.addObject("authorizedon", authorizedon);
		/*
		 * modelAndView.addObject("profitcenterlist", populateProfitCenter());
		 */
		modelAndView.addObject("choosen_view", choosen_view);

		modelAndView.addObject("lastcrt", crt_edit);

		PoMainForm obj = tempobj_authorized != null ? tempobj_authorized : new PoMainForm();
		modelAndView.getModelMap().addAttribute("pomainobj", obj);
		modelAndView.addObject("activetab", obj.getActivetab());
		modelAndView.addObject("lastcrt_used", obj.getLastcrt_used());
		modelAndView.addObject("quotNo", obj.getQuoteNo());
		modelAndView.addObject("quotDate", obj.getQuoteDate());
		modelAndView.addObject("vendincharge", obj.getVendorPersonInCharge());
		modelAndView.addObject("poNumber", obj.getPoNo());
		modelAndView.addObject("poNumber", obj.getPoNo());

		// List<String> forward_invoicerlist = new ArrayList<>();
		String issuer = obj.getIssuer();
		String authorizer = obj.getAuthorizer().trim();
		String verifier = obj.getVerifier().trim();

		// forward_invoicerlist.add(issuer);
		// if (!issuer.equals(verifier)) {
		// forward_invoicerlist.add(verifier);
		// }

		// if (!verifier.equals(authorizer)) {
		// forward_invoicerlist.add(authorizer);
		// }

		/* current user should not be able to authorize the po */
		if (!issuer.equals(authorizer)) {

			disable_authorize_button_verified = false;
		} else {
			disable_authorize_button_verified = true;

		}

		modelAndView.addObject("forward_invoicerlist", forward_invoicerlist);
		modelAndView.addObject("disable_authorize_button_verified", disable_authorize_button_verified);

		return modelAndView;
	}

	@GetMapping("/main/edit_deletedpo")
	public ModelAndView onPOLoad_deleted() {

		authDir = sysRepo.getSystemParameterValueByPropName(AUTHGRID_UPLOAD_PATH);

		ModelAndView modelAndView = new ModelAndView("/main/view_deleted_cancelled_rejected_po");
		String dateFormat = "dd-MM-yyyy";
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_FORMAT");
		if (dateObj != null && !dateObj.getPropValue().isEmpty()) {
			dateFormat = dateObj.getPropValue();
		}
		modelAndView.addObject("date_format", dateFormat);
		modelAndView.addObject("AuthorizationGrid", orderAuthGrid);

		modelAndView.addObject("profitCenterlist", getProfitCenterCodeAndProfitcenterName());
		modelAndView.addObject("itemNolist", itemInfoRepo.getAllItemInformation());
		modelAndView.addObject("taxCodelist", gstrepo.getAllActiveGoodsServicesTax());
		modelAndView.addObject("iteminfoobj", new ItemInfoForm());

		ItemForm itemobj = new ItemForm();
		itemobj.setItemOrder(getItemOrder(poNum));
		modelAndView.addObject("itemobj", itemobj);

		modelAndView.addObject("newpath", newPath);
		modelAndView.addObject("path_edit", path_edit);
		modelAndView.addObject("issuedon", issuedon);
		modelAndView.addObject("verifiedon", verifiedon);
		modelAndView.addObject("deletedon", deletedon);
		modelAndView.addObject("rejectedon", rejectedon);
		modelAndView.addObject("cancelledon", cancelledon);
		modelAndView.addObject("authorizedon", authorizedon);
		modelAndView.addObject("lastcrt", crt_edit);

		PoMainForm obj = tempobj_delete != null ? tempobj_delete : new PoMainForm();
		modelAndView.getModelMap().addAttribute("pomainobj", obj);
		modelAndView.addObject("activetab", obj.getActivetab());
		modelAndView.addObject("lastcrt_used", obj.getLastcrt_used());
		modelAndView.addObject("quotNo", obj.getQuoteNo());
		modelAndView.addObject("quotDate", obj.getQuoteDate());
		modelAndView.addObject("vendincharge", obj.getVendorPersonInCharge());
		modelAndView.addObject("poNumber", obj.getPoNo());
		modelAndView.addObject("status", obj.getStatus());

		return modelAndView;
	}

	@GetMapping("/main/view_forwarded_invoice")
	public ModelAndView onCreatePOLoad_invoiced() {

		authDir = sysRepo.getSystemParameterValueByPropName(AUTHGRID_UPLOAD_PATH);

		ModelAndView modelAndView = new ModelAndView("/main/view_forwarded_invoice");
		String dateFormat = "dd-MM-yyyy";
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_FORMAT");
		if (dateObj != null && !dateObj.getPropValue().isEmpty()) {
			dateFormat = dateObj.getPropValue();
		}
		purchaseorderuser = poUserService.getPurchaseOrderUserById(getCurrentUser().get());
		employee = employeeService.getEmployeeMasterByPsId(purchaseorderuser.getId());
		employeeList = employeeService.getAllEmployeebyProfitCenterCode(employee.getProfitCenter().getCode().toString(),
				true);
		profitCenterCode = profitCenterService.getProfitCenterByCode(employee.getProfitCenter().getCode().toString());
		boolean active = true;
		profitcenterlist = profitCenterService.getAllProfitCenterByCompanyCode(getCompany().get(), active);
		vendorlist = vbService.getAllActiveVendorBranchBycompCode(getCompany().get());
		purchaserequ = prService.getAllActivePurchaseRequisition();
		projectList = projectService.getActiveProjectByProfitCenterCode(profitCenterCode.getCode());
		userlist = poUserService.getAllActivePurchaseOrderUser();

		String[] purchaseorderuserArray = purchaseorderuser.toString().split("-");

		for (int i = 0; i < userlist.size(); i++) {

			if (userlist.get(i).getId().equals(purchaseorderuserArray[0].trim())) {
				int keyLocation = userlist.indexOf(userlist.get(i));
				userlist.remove(keyLocation);

			}

		}

		modelAndView.addObject("profitCenterlist", getProfitCenterCodeAndProfitcenterName());
		modelAndView.addObject("itemNolist", itemInfoRepo.getAllItemInformation());
		modelAndView.addObject("taxCodelist", gstrepo.getAllActiveGoodsServicesTax());
		modelAndView.addObject("iteminfoobj", new ItemInfoForm());

		ItemForm itemobj = new ItemForm();
		itemobj.setItemOrder(getItemOrder(poNum));
		modelAndView.addObject("itemobj", itemobj);

		modelAndView.addObject("credittermscodelist", crtService.getAllActiveCreditTerms());
		modelAndView.addObject("ordCatlist", orderCatService.getAllActiveOrderCategory());
		modelAndView.addObject("ordSubClist", orderSubCatService.getAllActiveOrderSubCategory());
		modelAndView.addObject("projectCodelist", projectService.getAllActiveProject());
		modelAndView.addObject("poUserlist", poUserService.getAllActivePurchaseOrderUser());

		modelAndView.addObject("profitCenterCode", profitCenterCode);
		modelAndView.addObject("employee", employee);
		modelAndView.addObject("employeeList", employeeList);
		modelAndView.addObject("purchaseorderuser", purchaseorderuser);
		modelAndView.addObject("userlist", userlist);
		modelAndView.addObject("vendor", populateVendorTabvendinfo());
		modelAndView.addObject("vendorlist", vendorlist);
		modelAndView.addObject("date_format", dateFormat);

		modelAndView.addObject("currencyList", currencyList());
		modelAndView.addObject("prtypeList", purchaserequ);
		modelAndView.addObject("projectList", projectList);
		modelAndView.addObject("AuthorizationGrid", orderAuthGrid);

		modelAndView.addObject("newpath", newPath);
		modelAndView.addObject("path_edit", path_edit);
		modelAndView.addObject("issuedon", issuedon);
		modelAndView.addObject("verifiedon", verifiedon);
		modelAndView.addObject("deletedon", deletedon);
		modelAndView.addObject("rejectedon", rejectedon);
		modelAndView.addObject("authorizedon", authorizedon);
		modelAndView.addObject("profitcenterlist", populateProfitCenter());
		modelAndView.addObject("choosen_view", choosen_view);

		modelAndView.addObject("lastcrt", crt_edit);

		PoMainForm obj = tempobj != null ? tempobj : new PoMainForm();
		modelAndView.getModelMap().addAttribute("pomainobj", obj);

		modelAndView.addObject("activetab", obj.getActivetab());
		modelAndView.addObject("lastcrt_used", obj.getLastcrt_used());
		modelAndView.addObject("quotNo", obj.getQuoteNo());
		modelAndView.addObject("poNumber", obj.getPoNo());
		modelAndView.addObject("status", obj.getStatus());
		modelAndView.addObject("quotDate", obj.getQuoteDate());
		modelAndView.addObject("vendincharge", obj.getVendorPersonInCharge());

		return modelAndView;
	}

	public List<String> populateIssuerTabStoreinfo(String profitcenter) {

		List<String> pcInfo = new ArrayList<>();
		StringBuilder s = new StringBuilder();
		ProfitCenter pc_obj = profitCenterService.getProfitCenterByCode(profitcenter);
		s.append(pc_obj.getStore().getCode());
		s.append("-");
		s.append(pc_obj.getStore().getName());
		pcInfo.add(s.toString());
		return pcInfo;
	}

	public List<String> verifier_info() {

		List<String> pouserInfo = new ArrayList<>();

		List<PurchaseOrderUser> pouser_list = poUserService.getAllActivePurchaseOrderUser();

		if (pouser_list != null) {
			for (PurchaseOrderUser pouser_obj : pouser_list) {
				StringBuilder s = new StringBuilder();
				EmployeeMaster emp_obj = employeeService.getEmployeeMasterByPsId(pouser_obj.getId());
				s.append(pouser_obj.getId());
				s.append("-");
				if (emp_obj != null) {
					s.append(emp_obj.getAltId());
				}
				s.append("-");
				s.append(pouser_obj.getEmpName());
				s.append("-");
				if (emp_obj != null) {
					s.append(emp_obj.getProfitCenter().getCode());
				}
				pouserInfo.add(s.toString());
			}

		}
		return pouserInfo;
	}

	public List<String> authorizor_info() {

		userlist = poUserService.getAllActivePurchaseOrderUser();
		String[] purchaseorderuserArray = purchaseorderuser.toString().split("-");
		List<String> pouserInfo = new ArrayList<>();

		for (int i = 0; i < userlist.size(); i++) {

			if (userlist.get(i).getId().equals(purchaseorderuserArray[0].trim())) {
				int keyLocation = userlist.indexOf(userlist.get(i));
				userlist.remove(keyLocation);

			}

		}

		if (userlist != null) {
			for (PurchaseOrderUser pouser_obj : userlist) {
				StringBuilder s = new StringBuilder();
				EmployeeMaster emp_obj = employeeService.getEmployeeMasterByPsId(pouser_obj.getId());
				s.append(pouser_obj.getId());
				s.append("-");
				if (emp_obj != null) {
					s.append(emp_obj.getAltId());
				}
				s.append("-");
				s.append(pouser_obj.getEmpName());
				s.append("-");
				if (emp_obj != null) {
					s.append(emp_obj.getProfitCenter().getCode());
				}
				pouserInfo.add(s.toString());
			}
		}
		return pouserInfo;
	}

	public List<String> populateIssuerTabStoreinfo_draftPo(String profitcenter, String PoNo) {

		List<String> pcInfo = new ArrayList<>();
		StringBuilder s_store = new StringBuilder();
		if (profitcenter != null) {
			String store_code = profitcenter.substring(0, 4);

			PurchaseOrder po_obj = poMainService.getPurchaseOrderByPoNo(PoNo);
			String store_name = po_obj.getStoreName();

			s_store.append(store_code);
			s_store.append("-");
			s_store.append(store_name);
			pcInfo.add(s_store.toString());
		}
		return pcInfo;
	}

	public List<String> populateIssuerTabDeptinfo_draftPo(String profitcenter, String PoNo) {

		List<String> pcInfo = new ArrayList<>();
		StringBuilder s_store = new StringBuilder();
		if (profitcenter != null) {
			String dept_code = profitcenter.substring(4, 7);

			PurchaseOrder po_obj = poMainService.getPurchaseOrderByPoNo(PoNo);

			String dept_name = po_obj.getDepartmentName();

			s_store.append(dept_code);
			s_store.append("-");
			s_store.append(dept_name);
			pcInfo.add(s_store.toString());
		}
		return pcInfo;
	}

	public List<String> populateIssuerTabDeptinfo(String profitcenter) {

		List<String> deptInfo = new ArrayList<>();
		ProfitCenter pc_obj = profitCenterService.getProfitCenterByCode(profitcenter);
		StringBuilder s = new StringBuilder();

		s.append(pc_obj.getDepartment().getCode());
		s.append("-");
		s.append(pc_obj.getDepartment().getName());
		deptInfo.add(s.toString());

		return deptInfo;
	}

	public List<String> populateVendorTabvendinfo() {

		List<String> vendInfo = new ArrayList<>();

		for (VendorBranch e : vendorlist) {
			StringBuilder s = new StringBuilder();
			s.append(e.getVendorBranchId().getCode());
			s.append("-");
			s.append(e.getName());
			s.append("-");
			s.append(e.getVendorBranchId().getBranchCode());
			vendInfo.add(s.toString());
		}

		return vendInfo;
	}

	public HashMap<String, String> extract_vendinfo(String vendorcode) {

		if (vendorcode != null) {
			int last_index = vendorcode.lastIndexOf("-");
			int first_index = vendorcode.indexOf("-");

			String vendCode_sub = vendorcode.substring(0, first_index);
			String vendBCode_sub = vendorcode.substring(last_index + 1);
			String vendName_sub = vendorcode.substring(first_index + 1, last_index);

			HashMap<String, String> vend_info = new HashMap<String, String>();
			vend_info.put("vendorcode", vendCode_sub);
			vend_info.put("vendorbranchcode", vendBCode_sub);
			vend_info.put("vendorname", vendName_sub);
			return vend_info;
		}

		return null;

	}

	public String extract_crt(String crt) {

		if (crt != null) {
			String[] crtInfoArray = crt.split("-");
			String crtCode = crtInfoArray[0];

			return crtCode;
		}

		return null;

	}

	public String extract_issuer(String issuer) {
		if (issuer != null) {
			String[] issuerInfoArray = issuer.split("-");
			String issuer_id = issuerInfoArray[0];

			return issuer_id;
		}
		return null;
	}

	public List<String> populateProfitCenter() {

		List<String> pcInfo = new ArrayList<>();

		for (ProfitCenter e : profitcenterlist) {
			StringBuilder s = new StringBuilder();
			s.append(e.getCode());
			s.append("-");
			s.append(e.getName());

			pcInfo.add(s.toString());
		}

		return pcInfo;
	}

	public List<String> populateProfitCenter_draft(String pcCode) {

		List<String> pcInfo = new ArrayList<>();

		StringBuilder s_store = new StringBuilder();
		ProfitCenter pc_obj = profitCenterService.getProfitCenterByCode(pcCode);
		if (pc_obj != null) {

			s_store.append(pc_obj.getCode());
			s_store.append("-");
			s_store.append(pc_obj.getName());
			pcInfo.add(s_store.toString());
		}
		return pcInfo;

	}

	private int getItemOrder(String poNo) {
		return itemMapRepo.getAllItemMapByPurchaseOrderNo(poNo).size() + 1;
	}

	public String ponumber(String pcCode, RedirectAttributes redirectAttributes) {
		ProfitCenter pc_obj = profitCenterService.getProfitCenterByCode(pcCode);
		String yearToSend = Integer.toString(calendar.get(Calendar.YEAR));
		if (pc_obj != null) {
			ProfitCenterLastPONumber pcLastPO = getRunningNumber(pc_obj.getCode(), yearToSend);
			String year = yearToSend.substring(2);
			int runningNumber = pcLastPO.getRunningNumber() + 1;
			try {
				if (pc_obj.getDepartment().getAbbrName() != null || !pc_obj.getDepartment().getAbbrName().isEmpty()) {

					poNumber = getCompany().get() + "PO" + year + pc_obj.getStore().getCode()
							+ pc_obj.getDepartment().getAbbrName() + String.format("%05d", (runningNumber));
					pcLastPO.setLastPoNumber(poNumber);
					pcLastPO.setRunningNumber(runningNumber);
					pcLastPONumberService.createProfitCenterLastPONumber(pcLastPO);
					return poNumber;
				}
			} catch (Exception e) {
				LOGGER.error(e);

				return null;

			}
		}
		return null;

	}

	public synchronized ProfitCenterLastPONumber getRunningNumber(String pcCode, String year) {

		ProfitCenterLastPONumber pcLastpo = pcLastrepo.getPcLastPONumberByPcCodeAndYear(pcCode, year);
		synchronized (this) {

			if (pcLastpo == null) {

				pcLastpo = new ProfitCenterLastPONumber();
				pcLastpo.setPcCode(pcCode);
				pcLastpo.setYear(year);
				pcLastpo.setRunningNumber(0);
			}
			return pcLastpo;
		}
	}

	@RequestMapping(value = "/main/selectedvendor", method = RequestMethod.GET)
	public @ResponseBody VendorCreditTerms lastCreditterm(@RequestParam("vendorcode") String vendorcode,
			RedirectAttributes redirectAttributes, Model model) {
		HashMap<String, String> vend_info_Map = new HashMap<String, String>();
		if (vendorcode != null) {
			vend_info_Map = extract_vendinfo(vendorcode);
		}

		List<VendorCreditTerms> vcrtList = vcrtService
				.getVendorCreditTermsByVendorCodeAndVendorBranchCodeAndCompanyCode(vend_info_Map.get("vendorcode"),
						vend_info_Map.get("vendorbranchcode"), getCompany().get());

		if (!vcrtList.isEmpty() && vcrtList.size() > 0) {
			lastcrt = vcrtList.get(0);
			model.addAttribute("lastcrt", lastcrt);
			return lastcrt;
		}
		return null;
	}

	@RequestMapping(value = "/main/selectedpc", method = RequestMethod.GET)
	public @ResponseBody ProfitCenter return_pcCode(@RequestParam("pccode") String pccode,
			RedirectAttributes redirectAttributes, Model model) {

		String[] pcArray = pccode.split("-");
		String pcCode = pcArray[0];
		return profitCenterService.getProfitCenterByCode(pcCode);
	}

	@RequestMapping(value = "/main/selected_contactperson", method = RequestMethod.GET)
	public @ResponseBody EmployeeMaster return_employee_contact_info_(@RequestParam("contactvalue") String contactvalue,
			RedirectAttributes redirectAttributes, Model model) {

		String[] pouser_id_array = contactvalue.split("-");
		String po_user_id = pouser_id_array[0];
		EmployeeMaster emp_obj = employeeService.getEmployeeMasterByPsId(po_user_id.trim());

		return emp_obj;
	}

	@RequestMapping(value = "/main/populatevendInfo", method = RequestMethod.GET)
	public @ResponseBody HashMap<String, String> vendInfo(@RequestParam("vendorcode") String vendorcode,
			RedirectAttributes redirectAttributes, Model model) {

		HashMap<String, String> vend_info_Map = new HashMap<String, String>();
		if (vendorcode != null) {
			vend_info_Map = extract_vendinfo(vendorcode);
		}

		HashMap<String, String> vend_info_list = new HashMap<String, String>();

		if (vend_info_Map.size() > 0) {

			VendorBranchId pkey = new VendorBranchId(vend_info_Map.get("vendorcode"),
					vend_info_Map.get("vendorbranchcode"), getCompany().get());
			VendorBranch vend_obj = vbService.getVendorBranchbyVendorId(pkey);
			String add1 = vend_obj.getAddressLine1();
			String add2 = vend_obj.getAddressLine2();
			String add3 = vend_obj.getAddressLine3();
			String postcode = vend_obj.getPostCode();
			String postarea = vend_obj.getPostCodeArea();
			String personincharge = vend_obj.getLastContactPersonName();
			String person_contact = vend_obj.getLastContactPersonContactNumber();
			String person_email = vend_obj.getLastContactPersonEmailAddress();
			vend_info_list.put("vendor_add1", add1);
			vend_info_list.put("vendor_add2", add2);
			vend_info_list.put("vendor_add3", add3);
			vend_info_list.put("vendor_postcode", postcode);
			vend_info_list.put("vendor_postarea", postarea);
			vend_info_list.put("personincharge", personincharge);
			vend_info_list.put("person_contact", person_contact);
			vend_info_list.put("person_email", person_email);

		}

		return vend_info_list;

	}

	private String getFileExtension(File file) {
		String fileName = file.getName();
		if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
			return fileName.substring(fileName.lastIndexOf(".") + 1);
		else
			return "";
	}

	@RequestMapping(value = "/main/populateauthgrid", method = RequestMethod.GET)
	public @ResponseBody String authgrid(@RequestParam("ordercode") String ordercode,
			RedirectAttributes redirectAttributes, Model model) throws UnsupportedEncodingException {
		boolean chkauthExist = false;
		MDC.put("sessionId",
				AbstractMainController.getCompany().isPresent() ? AbstractMainController.getCompany().get() : "");
		boolean chkordauthExist = false;
		String decodeValue = "";
		String filetype = "";
		try {
			decodeValue = UriUtils.decode(ordercode, "UTF-8");

		} catch (UnsupportedEncodingException e) {
			LOGGER.error(e);
		}
		if (decodeValue != null) {
			ordercat = orderCatService.getOrderCategoryByCode(decodeValue);

			if (ordercat.getAuthorizationGrid() != null) {
				ordAuth = ordercat.getAuthorizationGrid();
				chkauthExist = authExist(new File(ordercat.getAuthorizationGrid()));

				if (chkauthExist == false) {

					model.addAttribute("missed", chkauthExist);
				}

				File chkFileType = new File(ordAuth);
				filetype = getFileExtension(chkFileType);
				String filedir = authDir;
				if (filedir != null) {
					newPath = UriUtils.encodePath(filedir + ordAuth, "UTF-8");
				}
				model.addAttribute("ordAuth", ordAuth);
				model.addAttribute("ordAuthPath", newPath);
				model.addAttribute("File_ExtensionType", filetype);

			}
			// selected order not have any authorization grid in db
			else {
				chkordauthExist = true;
				model.addAttribute("missed_ord_auth", chkordauthExist);

			}
		}

		return ordAuth + "|" + newPath + "|" + filetype + "|" + chkauthExist + chkordauthExist;
	}

	private boolean authExist(File fileName) {
		String[] filename = fileName.getName().split("_");

		boolean authexist = false;

		if (authDir != null) {

			File auth_file = new File(authDir); // If file path exists
			if (auth_file.exists()) {
				File[] files = new File(authDir).listFiles();
				for (File file : files) {
					if (file.isFile()) {

						if (file.getName().contains(filename[0])) {
							authexist = true;

						}

					}

				}
			} else {

				return authexist;
			}
		}
		return authexist;
	}

	@RequestMapping(value = "/main/populateordersublist", method = RequestMethod.GET)
	public @ResponseBody List<String> ordersubList(@RequestParam("ordercode") String ordercode,
			RedirectAttributes redirectAttributes, Model model) throws UnsupportedEncodingException {
		List<OrderSubCategory> ordsubList = new ArrayList<>();
		List<String> ordsub_lis = new ArrayList<>();
		MDC.put("sessionId",
				AbstractMainController.getCompany().isPresent() ? AbstractMainController.getCompany().get() : "");
		String decodeValue = "";
		try {
			decodeValue = UriUtils.decode(ordercode, "UTF-8");

		} catch (UnsupportedEncodingException e) {
			LOGGER.error(e);
		}

		if (decodeValue != null) {
			ordsubList = orderSubCatService.getAllActiveOrderSubCategoryByOrderCategoryCode(decodeValue);

			if (ordsubList.size() > 0) {
				for (OrderSubCategory obj : ordsubList) {
					ordsub_lis.add(obj.getCode());
				}
				model.addAttribute("ordcList", ordsubList);
			}
		}

		return ordsub_lis;
	}

	@RequestMapping(value = "/main/populateprtype", method = RequestMethod.GET)
	public @ResponseBody String prtype(@RequestParam("ordercode") String ordercode,
			RedirectAttributes redirectAttributes, Model model) throws UnsupportedEncodingException {
		MDC.put("sessionId",
				AbstractMainController.getCompany().isPresent() ? AbstractMainController.getCompany().get() : "");
		OrderCategory ord;
		String[] prtypearray = null;
		String prType = null;
		String decodeValue = "";
		try {
			decodeValue = UriUtils.decode(ordercode, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			LOGGER.error(e);
		}

		if (decodeValue != null) {
			ord = orderCatService.getOrderCategoryByCode(decodeValue);
			prType = ord.getPurchaseRequisition().toString();
			prtypearray = prType.split("-");
			prType = prtypearray[0].trim();
		}

		return prType;
	}

	@PostMapping(value = "/main/draft_po_save")
	public String edit_draft_po(@Valid @ModelAttribute("pomainobj") PoMainForm obj, BindingResult result, Model model,
			RedirectAttributes redirectAttributes) throws ParseException, UnsupportedEncodingException {
		MDC.put("sessionId",
				AbstractMainController.getCompany().isPresent() ? AbstractMainController.getCompany().get() : "");
		PurchaseOrder purchaseorder_draft = poMainService.getPurchaseOrderByPoNo(obj.getPoNo());
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_FORMAT");
		String dateFormat = "dd-MM-yyyy";
		if (dateObj != null && !dateObj.getPropValue().isEmpty()) {
			dateFormat = dateObj.getPropValue();
		}
		DateFormat formatter = new SimpleDateFormat(dateFormat);

		String date_format = "dd-MM-yyyy hh:mm:ss";
		SystemParameter dateObj_1 = sysRepo.getSystemParameterByPropName("DATE_TIME_FORMAT");
		if (dateObj_1 != null) {
			if (!dateObj_1.getPropValue().isEmpty()) {
				date_format = dateObj_1.getPropValue();
			}
		}
		DateFormat formatter_1 = new SimpleDateFormat(date_format);

		if (!result.hasErrors()) {

			if (obj.getActivetab().equals("tab2") && obj.getActivetabform().equals("issuer")
					&& obj.getActivetaboperation().equals("save")) {

				if (obj.getContactPerson() != null) {
					String[] contactArray = obj.getContactPerson().split("-");
					purchaseorder_draft.setContactPerson(contactArray[0]);

				}

				if (obj.getProfitcenter() != null) {
					String[] profitcenterArray = obj.getProfitcenter().split("-");
					purchaseorder_draft.setPcCode(profitcenterArray[0]);

				}

				if (obj.getStoreName() != null) {

					String[] store_name_array = obj.getStoreName().split("-");

					purchaseorder_draft.setStoreName(store_name_array[1]);

				}
				if (obj.getDepartmentName() != null) {
					String[] dept_name_array = obj.getDepartmentName().split("-");
					purchaseorder_draft.setDepartmentName(dept_name_array[1]);
				}

				purchaseorder_draft.setContactPersonPhone(obj.getContactPersonPhone());
				purchaseorder_draft.setContactPersonEmail(obj.getContactPersonEmail());
				if (obj.getContactPersonEmail() != null) {

					employee.setEmail(obj.getContactPersonEmail());

				}

				if (obj.getContactPersonPhone() != null) {

					employee.setPhoneNo(obj.getContactPersonPhone());
				}

				if (employee != null) {

					try {
						employeeService.createEmployeeMaster(employee);
						auditService.createAudit(new Date(),
								" Employee: " + employee.getPsId() + " Updated successfully", getCurrentUser().get(),
								" Employee Information Updated During PO Creation", getCompany().get(),
								getProfitCenterCode().getCode());
					} catch (Exception e) {
						LOGGER.error(e);

					}

				}
				purchaseorder_draft.setAddressLine1(obj.getAddressLine1());
				purchaseorder_draft.setAddressLine2(obj.getAddressLine2());
				purchaseorder_draft.setAddressLine3(obj.getAddressLine3());
				purchaseorder_draft.setPostCode(obj.getPostCode());
				purchaseorder_draft.setPostArea(obj.getPostArea());

				if (obj.getContactPersonName() != null) {

					purchaseorder_draft.setContactPersonName(obj.getContactPersonName());
				}

				purchaseorder_draft.setLastModifiedOn(new Date());

				try {
					poMainService.createPurchaseOrder(purchaseorder_draft);
					auditService.createAudit(new Date(),
							"Drafted Purchase Order: " + obj.getPoNo() + " Updated successfully",
							getCurrentUser().get(), " Updating Draft Purchase Order", getCompany().get(),
							getProfitCenterCode().getCode());

				} catch (POException e) {
					LOGGER.error(e);
				} catch (Exception e) {
					LOGGER.error(e);
				}

				tempobj_draft = obj;

			}

			else if (obj.getActivetab().equals("tab3") && obj.getActivetabform().equals("vendor")
					&& obj.getActivetaboperation().equals("save")) {

				HashMap<String, String> vend_info_Map = new HashMap<String, String>();
				if (obj.getVendorCode() != null) {
					vend_info_Map = extract_vendinfo(obj.getVendorCode());
					purchaseorder_draft.setVendorcode(vend_info_Map.get("vendorcode"));
					purchaseorder_draft.setVendorbranchcode(vend_info_Map.get("vendorbranchcode"));
				}

				purchaseorder_draft.setLastModifiedOn(new Date());

				try {
					poMainService.createPurchaseOrder(purchaseorder_draft);
					auditService.createAudit(new Date(),
							"Drafted Purchase Order: " + obj.getPoNo() + " Updated successfully",
							getCurrentUser().get(), " Updating Draft Purchase Order", getCompany().get(),
							getProfitCenterCode().getCode());

				} catch (POException e) {
					LOGGER.error(e);
				} catch (Exception e) {
					LOGGER.error(e);
				}

				model.addAttribute("quoteNo", obj.getQuoteNo());
				model.addAttribute("quotDate", formatter.parse(obj.getQuoteDate()));
				model.addAttribute("lastcrt_used", obj.getLastcrt_used_hidden());

				tempobj_draft = obj;

			}

			else if (obj.getActivetab().equals("tab4") && obj.getActivetabform().equals("others")
					&& obj.getActivetaboperation().equals("save") && obj.getPoNo() != null && obj.getQuoteNo() != null

					&& obj.getCredittermscode() != null && obj.getCurrency() != null && obj.getOrdCatCode() != null
					&& obj.getOrdSubCCode() != null && obj.getPrTypeCode() != null
					&& obj.getDeliveryInstruction() != null

					&& obj.getVendorPersonInCharge() != null && obj.getVendorPersonInChargeEmail() != null
					&& obj.getVendorPersonInChargePhone() != null

			) {

				HashMap<String, String> vend_info_Map = new HashMap<String, String>();
				if (obj.getVendorCode() != null) {
					vend_info_Map = extract_vendinfo(obj.getVendorCode());
				}

				if (obj.getLastcrt_used_hidden() != null) {
					String crt_code = extract_crt(obj.getLastcrt_used_hidden());
					if (crt_code != null) {
						purchaseorder_draft.setCreditTermsCode(extract_crt(obj.getLastcrt_used_hidden()));
					} else {
						redirectAttributes.addFlashAttribute("error", "No Credit Terms Found!");
					}
				}

				purchaseorder_draft.setPoNo(obj.getPoNo());
				purchaseorder_draft.setStatus("Draft");
				purchaseorder_draft.setCompCode(getCompany().get());

				purchaseorder_draft.setCurrency(obj.getCurrency());
				purchaseorder_draft.setOrderCatCode(obj.getOrdCatCode());
				purchaseorder_draft.setOrdSubCatCode(obj.getOrdSubCCode());
				purchaseorder_draft.setPrTypeCode(obj.getPrTypeCode());
				purchaseorder_draft.setQuoteNo(obj.getQuot_No());
				purchaseorder_draft.setVendorPersonInCharge(obj.getVendorPersonInCharge());
				purchaseorder_draft.setVendorPersonInChargeEmail(obj.getVendorPersonInChargeEmail());
				purchaseorder_draft.setVendorPersonInChargePhone(obj.getVendorPersonInChargePhone());

				if (obj.getVendorAddressLine1() != null) {
					purchaseorder_draft.setVendorAddressLine1(obj.getVendorAddressLine1());
				}
				if (obj.getVendorAddressLine2() != null) {
					purchaseorder_draft.setVendorAddressLine2(obj.getVendorAddressLine2());
				}

				if (obj.getVendorAddressLine3() != null) {
					purchaseorder_draft.setVendorAddressLine3(obj.getVendorAddressLine3());
				}

				if (obj.getVendorPostCode() != null) {
					purchaseorder_draft.setVendorPostCode(obj.getVendorPostCode());

				}

				if (obj.getVendorPostArea() != null) {
					purchaseorder_draft.setVendorPostArea(obj.getVendorPostArea());

				}

				if (!obj.getQuot_Date().isEmpty()) {
					purchaseorder_draft.setQuoteDate(formatter.parse(obj.getQuot_Date()));
				}

				if (obj.getProjectCode() != null) {
					purchaseorder_draft.setProjectCode(obj.getProjectCode());
				}

				if (obj.getPaymentTerms() != null) {
					purchaseorder_draft.setPaymentTerms(obj.getPaymentTerms());
				}

				purchaseorder_draft.setDeliveryInstruction(obj.getDeliveryInstruction());

				if (obj.getIssuerRemark() != null) {
					purchaseorder_draft.setIssuerRemark(obj.getIssuerRemark());
				}

				if (obj.getOrdCatCode() != null) {

					String authgrid = orderCatService.getOrderCategoryByCode(obj.getOrdCatCode())
							.getAuthorizationGrid();
					purchaseorder_draft.setOrderCategoryAuthorizationGrid(authgrid);
					boolean chkauthExist = false;
					orderAuthGrid = purchaseorder_draft.getOrderCategoryAuthorizationGrid();
					chkauthExist = authExist(new File(obj.getOrdCatCode()));
					if (chkauthExist == false) {

						model.addAttribute("missed", chkauthExist);
					}
					String filedir = authDir;
					newPath = UriUtils.encodePath(filedir + orderAuthGrid, "UTF-8");
				}

				purchaseorder_draft.setVendorName(vend_info_Map.get("vendorname"));

				purchaseorder_draft.setLastModifiedOn(new Date());

				tempobj_draft = obj;

				try {
					poMainService.createPurchaseOrder(purchaseorder_draft);
					auditService.createAudit(new Date(),
							"New Purchase Order:" + obj.getPoNo() + " Drafted successfully", getCurrentUser().get(),
							" Drafted Purchase Order", getCompany().get(), getProfitCenterCode().getCode());

				} catch (POException e) {
					LOGGER.error(e);
				} catch (Exception e) {
					LOGGER.error(e);
				}

			}

			else if (obj.getActivetab().equals("tab5") && obj.getActivetabform().equals("authorization")) {

				if (obj.getActivetaboperation().equals("save")) {
					purchaseorder_draft = poMainService.getPurchaseOrderByPoNo(purchaseorder_draft.getPoNo());

					if (obj.getIssuer() != null) {

						purchaseorder_draft.setIssuer(extract_issuer(obj.getIssuer()));

					}

					if (obj.getVerifier() != null) {
						purchaseorder_draft.setVerifier(extract_issuer(obj.getVerifier()));
					}

					if (obj.getAuthorizer() != null) {

						purchaseorder_draft.setAuthorizer(extract_issuer(obj.getAuthorizer()));
					}

					if (obj.getFirstApprover() != null) {
						purchaseorder_draft.setFirstApprover(obj.getFirstApprover());
					}

					if (obj.getSecondApprover() != null) {

						purchaseorder_draft.setSecondApprover(obj.getSecondApprover());
					}

					if (obj.getThirdApprover() != null) {

						purchaseorder_draft.setThirdApprover(obj.getThirdApprover());
					}

					if (obj.getFirstApproverDesignation() != null) {

						purchaseorder_draft.setFirstApproverDesignation(obj.getFirstApproverDesignation());
					}

					if (obj.getSecondApproverDesignation() != null) {

						purchaseorder_draft.setSecondApproverDesignation(obj.getSecondApproverDesignation());
					}

					if (obj.getThirdApproverDesignation() != null) {

						purchaseorder_draft.setThirdApproverDesignation(obj.getThirdApproverDesignation());
					}

					purchaseorder_draft.setLastModifiedOn(new Date());
					try {
						poMainService.createPurchaseOrder(purchaseorder_draft);
						auditService.createAudit(new Date(),
								"Purchase Order: " + obj.getPoNo() + " Updated successfully", getCurrentUser().get(),
								" Updated Draft Purchase Order", getCompany().get(), getProfitCenterCode().getCode());
						redirectAttributes.addFlashAttribute("info", "Purchase Order Saved successfully");
					} catch (POException e) {
						LOGGER.error(e);
					} catch (Exception e) {
						LOGGER.error(e);
					}

					/*
					 * PurchaseOrderUserAccess purchaseOrderUserAccess_Draft = new
					 * PurchaseOrderUserAccess(); PurchaseOrderUserAccessPk poUserAccessId_Drafter =
					 * new PurchaseOrderUserAccessPk( purchaseorder_draft.getPoNo(),
					 * purchaseorder_draft.getIssuer(), "D"); purchaseOrderUserAccess_Draft.
					 * setPurchaseorderuseraccessId(poUserAccessId_Drafter);
					 * poUserAccessService.createPurchaseOrderUserAccess(
					 * purchaseOrderUserAccess_Draft);
					 */

					tempobj_draft = null;
					return "redirect:/main/draftpo";

				}

				else if (obj.getActivetaboperation().equals("submit")) {
					purchaseorder_draft = poMainService.getPurchaseOrderByPoNo(purchaseorder_draft.getPoNo());

					/*
					 * check_profitcenter_masterdata(purchaseorder_draft);
					 * check_vendor_masterdata(purchaseorder_draft);
					 */

					if (obj.getIssuer() != null) {

						purchaseorder_draft.setIssuer(extract_issuer(obj.getIssuer()));

					}

					if (obj.getVerifier() != null) {
						purchaseorder_draft.setVerifier(extract_issuer(obj.getVerifier()));
					}

					if (obj.getAuthorizer() != null) {

						purchaseorder_draft.setAuthorizer(extract_issuer(obj.getAuthorizer()));
					}

					if (obj.getFirstApprover() != null) {
						purchaseorder_draft.setFirstApprover(obj.getFirstApprover());
					}

					if (obj.getSecondApprover() != null) {

						purchaseorder_draft.setSecondApprover(obj.getSecondApprover());
					}

					if (obj.getThirdApprover() != null) {

						purchaseorder_draft.setThirdApprover(obj.getThirdApprover());
					}

					if (obj.getFirstApproverDesignation() != null) {

						purchaseorder_draft.setFirstApproverDesignation(obj.getFirstApproverDesignation());
					}

					if (obj.getSecondApproverDesignation() != null) {

						purchaseorder_draft.setSecondApproverDesignation(obj.getSecondApproverDesignation());
					}

					if (obj.getThirdApproverDesignation() != null) {

						purchaseorder_draft.setThirdApproverDesignation(obj.getThirdApproverDesignation());
					}

					submitPurchaseOrder(purchaseorder_draft, obj, redirectAttributes);
					return "redirect:/main/verifypo";
				} else if (obj.getActivetaboperation().equals("submitandverify")) {

					purchaseorder_draft = poMainService.getPurchaseOrderByPoNo(purchaseorder_draft.getPoNo());
					if (obj.getIssuer() != null) {

						purchaseorder_draft.setIssuer(extract_issuer(obj.getIssuer()));

					}

					if (obj.getVerifier() != null) {
						purchaseorder_draft.setVerifier(extract_issuer(obj.getVerifier()));
					}

					if (obj.getAuthorizer() != null) {

						purchaseorder_draft.setAuthorizer(extract_issuer(obj.getAuthorizer()));
					}

					if (obj.getFirstApprover() != null) {
						purchaseorder_draft.setFirstApprover(obj.getFirstApprover());
					}

					if (obj.getSecondApprover() != null) {

						purchaseorder_draft.setSecondApprover(obj.getSecondApprover());
					}

					if (obj.getThirdApprover() != null) {

						purchaseorder_draft.setThirdApprover(obj.getThirdApprover());
					}

					if (obj.getFirstApproverDesignation() != null) {

						purchaseorder_draft.setFirstApproverDesignation(obj.getFirstApproverDesignation());
					}

					if (obj.getSecondApproverDesignation() != null) {

						purchaseorder_draft.setSecondApproverDesignation(obj.getSecondApproverDesignation());
					}

					if (obj.getThirdApproverDesignation() != null) {

						purchaseorder_draft.setThirdApproverDesignation(obj.getThirdApproverDesignation());
					}
					verifyPurchaseOrder_direct_fromedit(purchaseorder_draft, obj, redirectAttributes);
					return "redirect:/main/mypos";

				}

				else if (obj.getActivetaboperation().equals("duplicate")) {

					purchaseorder_draft = poMainService.getPurchaseOrderByPoNo(purchaseorder_draft.getPoNo());
					if (obj.getIssuer() != null) {

						purchaseorder_draft.setIssuer(extract_issuer(obj.getIssuer()));

					}

					if (obj.getVerifier() != null) {
						purchaseorder_draft.setVerifier(extract_issuer(obj.getVerifier()));
					}

					if (obj.getAuthorizer() != null) {

						purchaseorder_draft.setAuthorizer(extract_issuer(obj.getAuthorizer()));
					}

					if (obj.getFirstApprover() != null) {
						purchaseorder_draft.setFirstApprover(obj.getFirstApprover());
					}

					if (obj.getSecondApprover() != null) {

						purchaseorder_draft.setSecondApprover(obj.getSecondApprover());
					}

					if (obj.getThirdApprover() != null) {

						purchaseorder_draft.setThirdApprover(obj.getThirdApprover());
					}

					if (obj.getFirstApproverDesignation() != null) {

						purchaseorder_draft.setFirstApproverDesignation(obj.getFirstApproverDesignation());
					}

					if (obj.getSecondApproverDesignation() != null) {

						purchaseorder_draft.setSecondApproverDesignation(obj.getSecondApproverDesignation());
					}

					if (obj.getThirdApproverDesignation() != null) {

						purchaseorder_draft.setThirdApproverDesignation(obj.getThirdApproverDesignation());
					}

					purchaseorder_draft.setLastModifiedOn(new Date());
					duplicate(purchaseorder_draft, obj, redirectAttributes);
					redirectAttributes.addFlashAttribute("info", "Purchase Order Duplicated successfully");
					return "redirect:/main/mypos";

				} else if (obj.getActivetaboperation().equals("delete")) {
					purchaseorder_draft = poMainService.getPurchaseOrderByPoNo(purchaseorder_draft.getPoNo());

					if (obj.getIssuer() != null) {

						purchaseorder_draft.setIssuer(extract_issuer(obj.getIssuer()));

					}

					if (obj.getVerifier() != null) {
						purchaseorder_draft.setVerifier(extract_issuer(obj.getVerifier()));
					}

					if (obj.getAuthorizer() != null) {

						purchaseorder_draft.setAuthorizer(extract_issuer(obj.getAuthorizer()));
					}

					if (obj.getFirstApprover() != null) {
						purchaseorder_draft.setFirstApprover(obj.getFirstApprover());
					}

					if (obj.getSecondApprover() != null) {

						purchaseorder_draft.setSecondApprover(obj.getSecondApprover());
					}

					if (obj.getThirdApprover() != null) {

						purchaseorder_draft.setThirdApprover(obj.getThirdApprover());
					}

					if (obj.getFirstApproverDesignation() != null) {

						purchaseorder_draft.setFirstApproverDesignation(obj.getFirstApproverDesignation());
					}

					if (obj.getSecondApproverDesignation() != null) {

						purchaseorder_draft.setSecondApproverDesignation(obj.getSecondApproverDesignation());
					}

					if (obj.getThirdApproverDesignation() != null) {

						purchaseorder_draft.setThirdApproverDesignation(obj.getThirdApproverDesignation());
					}

					purchaseorder_draft.setLastModifiedOn(new Date());

					delete(purchaseorder_draft, obj, redirectAttributes);
					redirectAttributes.addFlashAttribute("info", "Purchase Order Deleted successfully");
					return "redirect:/main/mypos";
				}

			} // End of tab 5
		} // End of Error Result

		else {

			redirectAttributes.addFlashAttribute("error", "Error happened during purchase order creation");
			tempobj_draft = null;
			return "redirect:/main/createpo_menu";
		}

		return "redirect:/main/edit_draftpo";
	}

	/**
	 * @param obj
	 * @param result
	 * @param model
	 * @param redirectAttributes
	 * @return create new po
	 * @throws ParseException
	 * @throws UnsupportedEncodingException
	 */
	@PostMapping(value = "/main/pocreatesave")
	public String create_new_po(@Valid @ModelAttribute("pomainobj") PoMainForm obj, BindingResult result, Model model,
			RedirectAttributes redirectAttributes) throws ParseException, UnsupportedEncodingException {
		MDC.put("sessionId",
				AbstractMainController.getCompany().isPresent() ? AbstractMainController.getCompany().get() : "");

		// PurchaseOrder purchaseorder = new PurchaseOrder();
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_FORMAT");
		String dateFormat = "dd-MM-yyyy";
		if (dateObj != null && !dateObj.getPropValue().isEmpty()) {
			dateFormat = dateObj.getPropValue();
		}
		DateFormat formatter = new SimpleDateFormat(dateFormat);

		String date_format = "dd-MM-yyyy hh:mm:ss";
		SystemParameter dateObj_1 = sysRepo.getSystemParameterByPropName("DATE_TIME_FORMAT");
		if (dateObj_1 != null) {
			if (!dateObj_1.getPropValue().isEmpty()) {
				date_format = dateObj_1.getPropValue();
			}
		}
		DateFormat formatter_1 = new SimpleDateFormat(date_format);

		if (!result.hasErrors()) {

			if (obj.getActivetab().equals("tab2") && obj.getActivetabform().equals("issuer")

					&& obj.getActivetaboperation().equals("save")) {

				if (obj.getContactPersonEmail() != null) {

					employee.setEmail(obj.getContactPersonEmail());

				}

				if (obj.getContactPersonPhone() != null) {

					employee.setPhoneNo(obj.getContactPersonPhone());
				}

				if (employee != null) {

					try {
						employeeService.createEmployeeMaster(employee);
						auditService.createAudit(new Date(),
								" Employee: " + employee.getPsId() + " Updated successfully", getCurrentUser().get(),
								" Employee Information Updated During PO Creation", getCompany().get(),
								getProfitCenterCode().getCode());
					} catch (Exception e) {
						LOGGER.error(e);

					}

				}

				tempobj = obj;

			}

			else if (obj.getActivetab().equals("tab3") && obj.getActivetabform().equals("vendor")
					&& obj.getActivetaboperation().equals("save")) {

				if (obj.getVendorCode() != null) {
					HashMap<String, String> vend_info_Map = new HashMap<String, String>();
					VendorBranch vendor_obj = new VendorBranch();

					vend_info_Map = extract_vendinfo(obj.getVendorCode());
					VendorBranchId pkey = new VendorBranchId(vend_info_Map.get("vendorcode"),
							vend_info_Map.get("vendorbranchcode"), getCompany().get());
					vendor_obj = vbService.getVendorBranchbyVendorId(pkey);
					if (obj.getVendorPersonInCharge() != null) {

						vendor_obj.setLastContactPersonName(obj.getVendorPersonInCharge());
					}

					if (obj.getVendorPersonInChargeEmail() != null) {

						vendor_obj.setLastContactPersonEmailAddress(obj.getVendorPersonInChargeEmail());
					}

					if (obj.getVendorPersonInChargePhone() != null) {

						vendor_obj.setLastContactPersonContactNumber(obj.getVendorPersonInChargePhone());
					}

					try {
						vbService.createVendorBranch(vendor_obj);
						auditService.createAudit(new Date(),
								" VendorBranch: " + vendor_obj.getVendorBranchId().getCode() + " Updated successfully",
								getCurrentUser().get(), " VendorBranch Information Updated During PO Creation",
								getCompany().get(), getProfitCenterCode().getCode());
					} catch (Exception e) {
						LOGGER.error(e);

					}
				}

				if (poMainService.getPurchaseOrderByPoNo(obj.getPoNo()) == null) {
					if (profitCenterCode.getCode() != null) {
						poNum = ponumber(profitCenterCode.getCode().toString(), redirectAttributes);
						if (poNum == null) {
							redirectAttributes.addFlashAttribute("error",
									"Department Abbr Name Not Found. You Are Not Allowed to Create PO");
							return "redirect:/main/createpo_menu";
						}
					} else {
						redirectAttributes.addFlashAttribute("error", "Profit Center Not Found For this User");
						return "redirect:/main/createpo_menu";
					}
				}

				if (obj.getVendorPersonInCharge() != null) {

				}

				tempobj = obj;

			}

			else if (obj.getActivetab().equals("tab4") && obj.getActivetabform().equals("others")
					&& obj.getActivetaboperation().equals("save")) {

				purchaseorder = new PurchaseOrder();

				if (profitCenterCode.getCode() != null) {
					purchaseorder.setPcCode(profitCenterCode.getCode());
				}

				if (obj.getLastcrt_used_hidden() != null) {
					String crt_code = extract_crt(obj.getLastcrt_used_hidden());
					if (crt_code != null) {
						purchaseorder.setCreditTermsCode(extract_crt(obj.getLastcrt_used_hidden()));
					} else {
						redirectAttributes.addFlashAttribute("error", "No Credit Terms Found!");
					}
				}

				if (obj.getPoNo() != null) {
					purchaseorder.setPoNo(obj.getPoNo());
				}

				purchaseorder.setStatus("Draft");
				purchaseorder.setIssuer(getCurrentUser().get());

				purchaseorder.setCompCode(getCompany().get());

				if (obj.getCurrency() != null) {
					purchaseorder.setCurrency(obj.getCurrency());
				}

				if (obj.getOrdCatCode() != null) {
					purchaseorder.setOrderCatCode(obj.getOrdCatCode());
				}

				if (obj.getOrdSubCCode() != null) {
					purchaseorder.setOrdSubCatCode(obj.getOrdSubCCode());
				}

				if (obj.getPrTypeCode() != null) {
					purchaseorder.setPrTypeCode(obj.getPrTypeCode());
				}

				if (obj.getQuot_No() != null) {
					purchaseorder.setQuoteNo(obj.getQuot_No());
				}

				if (obj.getVendorPersonInCharge() != null) {
					purchaseorder.setVendorPersonInCharge(obj.getVendorPersonInCharge());
				}
				purchaseorder.setVendorPersonInChargeEmail(obj.getVendorPersonInChargeEmail());
				purchaseorder.setVendorPersonInChargePhone(obj.getVendorPersonInChargePhone());

				purchaseorder.setStoreName(profitCenterCode.getStore().getName());
				purchaseorder.setDepartmentName(profitCenterCode.getDepartment().getName());

				purchaseorder.setContactPersonPhone(obj.getContactPersonPhone());
				purchaseorder.setContactPersonEmail(obj.getContactPersonEmail());
				purchaseorder.setAddressLine1(obj.getAddressLine1());
				purchaseorder.setAddressLine2(obj.getAddressLine2());
				purchaseorder.setAddressLine3(obj.getAddressLine3());
				purchaseorder.setPostCode(obj.getPostCode());
				purchaseorder.setPostArea(obj.getPostArea());

				if (obj.getContactPersonName() != null) {

					purchaseorder.setContactPersonName(obj.getContactPersonName());
				}

				if (obj.getVendorAddressLine1() != null) {
					purchaseorder.setVendorAddressLine1(obj.getVendorAddressLine1());
				}
				if (obj.getVendorAddressLine2() != null) {
					purchaseorder.setVendorAddressLine2(obj.getVendorAddressLine2());
				}

				if (obj.getVendorAddressLine3() != null) {
					purchaseorder.setVendorAddressLine3(obj.getVendorAddressLine3());
				}

				if (obj.getVendorPostCode() != null) {
					purchaseorder.setVendorPostCode(obj.getVendorPostCode());

				}

				if (obj.getVendorPostArea() != null) {
					purchaseorder.setVendorPostArea(obj.getVendorPostArea());

				}

				if (!obj.getQuot_Date().isEmpty()) {
					purchaseorder.setQuoteDate(formatter.parse(obj.getQuot_Date()));
				}

				if (obj.getProjectCode() != null) {
					purchaseorder.setProjectCode(obj.getProjectCode());
				}

				if (obj.getPaymentTerms() != null) {
					purchaseorder.setPaymentTerms(obj.getPaymentTerms());
				}

				purchaseorder.setDeliveryInstruction(obj.getDeliveryInstruction());

				if (obj.getIssuerRemark() != null) {
					purchaseorder.setIssuerRemark(obj.getIssuerRemark());
				}

				if (obj.getOrdCatCode() != null) {

					String authgrid = orderCatService.getOrderCategoryByCode(obj.getOrdCatCode())
							.getAuthorizationGrid();
					if (authgrid != null) {
						purchaseorder.setOrderCategoryAuthorizationGrid(authgrid);

						boolean chkauthExist = false;
						orderAuthGrid = purchaseorder.getOrderCategoryAuthorizationGrid();
						chkauthExist = authExist(new File(purchaseorder.getOrderCategoryAuthorizationGrid()));
						if (chkauthExist == false) {

							model.addAttribute("missed", chkauthExist);
						}
						String filedir = authDir;

						newPath = UriUtils.encodePath(filedir + orderAuthGrid, "UTF-8");
					}
				}

				HashMap<String, String> vend_info_Map = new HashMap<String, String>();
				if (obj.getVendorCode() != null) {
					vend_info_Map = extract_vendinfo(obj.getVendorCode());
					purchaseorder.setVendorcode(vend_info_Map.get("vendorcode"));
					purchaseorder.setVendorbranchcode(vend_info_Map.get("vendorbranchcode"));
					purchaseorder.setVendorName(vend_info_Map.get("vendorname"));
				}
				try {

					poMainService.createPurchaseOrder(purchaseorder);
					auditService.createAudit(new Date(),
							" New Purchase Order: " + obj.getPoNo() + " Drafted successfully", getCurrentUser().get(),
							" Drafted Purchase Order", getCompany().get(), getProfitCenterCode().getCode());

				} catch (POException e) {
					LOGGER.error(e);

				} catch (Exception e) {
					e.printStackTrace();
					LOGGER.error(e);
				}
				PurchaseOrderUserAccess purchaseOrderUserAccess_Draft = new PurchaseOrderUserAccess();
				PurchaseOrderUserAccessPk poUserAccessId_Drafter = new PurchaseOrderUserAccessPk(
						purchaseorder.getPoNo(), purchaseorder.getIssuer(), "D");
				purchaseOrderUserAccess_Draft.setPurchaseorderuseraccessId(poUserAccessId_Drafter);

				try {
					poUserAccessService.createPurchaseOrderUserAccess(purchaseOrderUserAccess_Draft);
				} catch (POException e) {
					LOGGER.error(e);
				} catch (Exception e) {
					LOGGER.error(e);
				}

				tempobj = obj;
			}

			else if (obj.getActivetab().equals("tab5") && obj.getActivetabform().equals("authorization")) {

				if (obj.getActivetaboperation().equals("save")) {

					purchaseorder = poMainService.getPurchaseOrderByPoNo(purchaseorder.getPoNo());

					if (obj.getIssuer() != null) {

						purchaseorder.setIssuer(extract_issuer(obj.getIssuer()));

					}

					if (obj.getVerifier() != null) {
						purchaseorder.setVerifier(extract_issuer(obj.getVerifier()));
					}

					if (obj.getAuthorizer() != null) {

						purchaseorder.setAuthorizer(extract_issuer(obj.getAuthorizer()));
					}

					if (obj.getFirstApprover() != null) {
						purchaseorder.setFirstApprover(obj.getFirstApprover());
					}

					if (obj.getSecondApprover() != null) {

						purchaseorder.setSecondApprover(obj.getSecondApprover());
					}

					if (obj.getThirdApprover() != null) {

						purchaseorder.setThirdApprover(obj.getThirdApprover());
					}

					if (obj.getFirstApproverDesignation() != null) {

						purchaseorder.setFirstApproverDesignation(obj.getFirstApproverDesignation());
					}

					if (obj.getSecondApproverDesignation() != null) {

						purchaseorder.setSecondApproverDesignation(obj.getSecondApproverDesignation());
					}

					if (obj.getThirdApproverDesignation() != null) {

						purchaseorder.setThirdApproverDesignation(obj.getThirdApproverDesignation());
					}

					try {
						poMainService.createPurchaseOrder(purchaseorder);
						auditService.createAudit(new Date(),
								"Purchase Order: " + obj.getPoNo() + " Updated successfully", getCurrentUser().get(),
								" Updated Draft Purchase Order", getCompany().get(), getProfitCenterCode().getCode());
						redirectAttributes.addFlashAttribute("info", "Purchase Order Saved successfully");
					} catch (POException e) {
						LOGGER.error(e);
					} catch (Exception e) {
						LOGGER.error(e);
					}

					/*
					 * PurchaseOrderUserAccess purchaseOrderUserAccess_Draft = new
					 * PurchaseOrderUserAccess(); PurchaseOrderUserAccessPk poUserAccessId_Drafter =
					 * new PurchaseOrderUserAccessPk( purchaseorder.getPoNo(),
					 * purchaseorder.getIssuer(), "D"); purchaseOrderUserAccess_Draft.
					 * setPurchaseorderuseraccessId(poUserAccessId_Drafter);
					 * 
					 * try { poUserAccessService.createPurchaseOrderUserAccess(
					 * purchaseOrderUserAccess_Draft); } catch (POException e) {
					 * log.log(LogMessages.ERROR.value(),
					 * messages.getMessage(FAILED_PERSIST_EXCEPTION), e, getCompany().get()); }
					 * catch (Exception e) { log.log(LogMessages.ERROR.value(),
					 * messages.getMessage(FAILED_PERSIST_EXCEPTION), e, getCompany().get()); }
					 */
					tempobj = null;
					return "redirect:/main/draftpo";
				}

				else if (obj.getActivetaboperation().equals("submit")) {

					purchaseorder = poMainService.getPurchaseOrderByPoNo(purchaseorder.getPoNo());
					if (obj.getIssuer() != null) {

						purchaseorder.setIssuer(extract_issuer(obj.getIssuer()));

					}

					if (obj.getVerifier() != null) {
						purchaseorder.setVerifier(extract_issuer(obj.getVerifier()));
					}

					if (obj.getAuthorizer() != null) {

						purchaseorder.setAuthorizer(extract_issuer(obj.getAuthorizer()));
					}

					if (obj.getFirstApprover() != null) {
						purchaseorder.setFirstApprover(obj.getFirstApprover());
					}

					if (obj.getSecondApprover() != null) {

						purchaseorder.setSecondApprover(obj.getSecondApprover());
					}

					if (obj.getThirdApprover() != null) {

						purchaseorder.setThirdApprover(obj.getThirdApprover());
					}

					if (obj.getFirstApproverDesignation() != null) {

						purchaseorder.setFirstApproverDesignation(obj.getFirstApproverDesignation());
					}

					if (obj.getSecondApproverDesignation() != null) {

						purchaseorder.setSecondApproverDesignation(obj.getSecondApproverDesignation());
					}

					if (obj.getThirdApproverDesignation() != null) {

						purchaseorder.setThirdApproverDesignation(obj.getThirdApproverDesignation());
					}

					submitPurchaseOrder(purchaseorder, obj, redirectAttributes);
					return "redirect:/main/verifypo";
				} else if (obj.getActivetaboperation().equals("submitandverify")) {
					purchaseorder = poMainService.getPurchaseOrderByPoNo(purchaseorder.getPoNo());
					if (obj.getIssuer() != null) {

						purchaseorder.setIssuer(extract_issuer(obj.getIssuer()));

					}

					if (obj.getVerifier() != null) {
						purchaseorder.setVerifier(extract_issuer(obj.getVerifier()));
					}

					if (obj.getAuthorizer() != null) {

						purchaseorder.setAuthorizer(extract_issuer(obj.getAuthorizer()));
					}

					if (obj.getFirstApprover() != null) {
						purchaseorder.setFirstApprover(obj.getFirstApprover());
					}

					if (obj.getSecondApprover() != null) {

						purchaseorder.setSecondApprover(obj.getSecondApprover());
					}

					if (obj.getThirdApprover() != null) {

						purchaseorder.setThirdApprover(obj.getThirdApprover());
					}

					if (obj.getFirstApproverDesignation() != null) {

						purchaseorder.setFirstApproverDesignation(obj.getFirstApproverDesignation());
					}

					if (obj.getSecondApproverDesignation() != null) {

						purchaseorder.setSecondApproverDesignation(obj.getSecondApproverDesignation());
					}

					if (obj.getThirdApproverDesignation() != null) {

						purchaseorder.setThirdApproverDesignation(obj.getThirdApproverDesignation());
					}

					verify_newPurchaseOrder_directly(purchaseorder, obj, redirectAttributes);
					return "redirect:/main/mypos";

				}

				else if (obj.getActivetaboperation().equals("duplicate")) {

					purchaseorder = poMainService.getPurchaseOrderByPoNo(purchaseorder.getPoNo());
					if (obj.getIssuer() != null) {

						purchaseorder.setIssuer(extract_issuer(obj.getIssuer()));

					}

					if (obj.getVerifier() != null) {
						purchaseorder.setVerifier(extract_issuer(obj.getVerifier()));
					}

					if (obj.getAuthorizer() != null) {

						purchaseorder.setAuthorizer(extract_issuer(obj.getAuthorizer()));
					}

					if (obj.getFirstApprover() != null) {
						purchaseorder.setFirstApprover(obj.getFirstApprover());
					}

					if (obj.getSecondApprover() != null) {

						purchaseorder.setSecondApprover(obj.getSecondApprover());
					}

					if (obj.getThirdApprover() != null) {

						purchaseorder.setThirdApprover(obj.getThirdApprover());
					}

					if (obj.getFirstApproverDesignation() != null) {

						purchaseorder.setFirstApproverDesignation(obj.getFirstApproverDesignation());
					}

					if (obj.getSecondApproverDesignation() != null) {

						purchaseorder.setSecondApproverDesignation(obj.getSecondApproverDesignation());
					}

					if (obj.getThirdApproverDesignation() != null) {

						purchaseorder.setThirdApproverDesignation(obj.getThirdApproverDesignation());
					}
					duplicate(purchaseorder, obj, redirectAttributes);
					return "redirect:/main/mypos";

				}

			}

		} // End of if no Result Error

		else {

			redirectAttributes.addFlashAttribute("error", "Error happened during purchase order creation");
			tempobj = null;
			return "redirect:/main/createpo_menu";
		}

		return "redirect:/main/createpo";
	}

	/**
	 * @param purchaseOrder
	 * @param obj
	 * @param redirectAttributes
	 *            Issue the po
	 */
	public void submitPurchaseOrder(PurchaseOrder purchaseOrder, PoMainForm obj,
			RedirectAttributes redirectAttributes) {
		MDC.put("sessionId",
				AbstractMainController.getCompany().isPresent() ? AbstractMainController.getCompany().get() : "");
		PurchaseOrder Issue_purchaseorder = poMainService.getPurchaseOrderByPoNo(purchaseOrder.getPoNo());
		String date_format = "dd-MM-yyyy hh:mm:ss";
		SystemParameter dateObj_1 = sysRepo.getSystemParameterByPropName("DATE_TIME_FORMAT");
		if (dateObj_1 != null) {
			if (!dateObj_1.getPropValue().isEmpty()) {
				date_format = dateObj_1.getPropValue();
			}
		}
		DateFormat formatter_1 = new SimpleDateFormat(date_format);

		Issue_purchaseorder.setLastModifiedOn(new Date());
		Issue_purchaseorder.setStatus("Issued");
		Issue_purchaseorder.setIssuedOn(new Date());
		Issue_purchaseorder.setIssuer(extract_issuer(obj.getIssuer()));
		Issue_purchaseorder.setVerifier(extract_issuer(obj.getVerifier()));
		Issue_purchaseorder.setAuthorizer(extract_issuer(obj.getAuthorizer()));

		try {
			poMainService.createPurchaseOrder(Issue_purchaseorder);
			PurchaseOrderSendMailUtil email = new PurchaseOrderSendMailUtil(Issue_purchaseorder.getPoNo(),
					getCompany().get());
			email.submitPurchaseOrder();

			auditService.createAudit(new Date(), " Purchase Order: " + obj.getPoNo() + " Issued successfully",
					getCurrentUser().get(), " Issuing Purchase Order", getCompany().get(),
					getProfitCenterCode().getCode());
			redirectAttributes.addFlashAttribute("info", "Purchase Order Issued successfully");

		} catch (Exception e) {
			LOGGER.error(e);
		}

		if (Issue_purchaseorder.getIssuedOn() != null) {
			issuedon = formatter_1.format(Issue_purchaseorder.getIssuedOn());
		}

		if (Issue_purchaseorder.getIssuer() != null) {

			PurchaseOrderUserAccess purchaseOrderUserAccess_Draft = new PurchaseOrderUserAccess();
			PurchaseOrderUserAccessPk poUserAccessId_Drafter = new PurchaseOrderUserAccessPk(
					Issue_purchaseorder.getPoNo(), Issue_purchaseorder.getIssuer(), "D");
			if (poUserAccessService.getPurchaseOrderUserAccessByPK(poUserAccessId_Drafter) == null) {
				purchaseOrderUserAccess_Draft.setPurchaseorderuseraccessId(poUserAccessId_Drafter);

				try {
					poUserAccessService.createPurchaseOrderUserAccess(purchaseOrderUserAccess_Draft);
				} catch (Exception e) {
					LOGGER.error(e);
				}
			}
		}
		if (Issue_purchaseorder.getVerifier() != null && Issue_purchaseorder.getAuthorizer() != null) {
			PurchaseOrderUserAccess purchaseOrderUserAccess_Verifier = new PurchaseOrderUserAccess();
			PurchaseOrderUserAccessPk poUserAccessId_Verifier = new PurchaseOrderUserAccessPk(
					Issue_purchaseorder.getPoNo(), Issue_purchaseorder.getVerifier(), "V");
			PurchaseOrderUserAccess purchaseOrderUserAccess_Authorizer = new PurchaseOrderUserAccess();
			PurchaseOrderUserAccessPk poUserAccessId_Authorizer = new PurchaseOrderUserAccessPk(
					Issue_purchaseorder.getPoNo(), Issue_purchaseorder.getAuthorizer(), "A");

			purchaseOrderUserAccess_Verifier.setPurchaseorderuseraccessId(poUserAccessId_Verifier);

			try {
				poUserAccessService.createPurchaseOrderUserAccess(purchaseOrderUserAccess_Verifier);
			} catch (Exception e) {
				LOGGER.error(e);
			}
			purchaseOrderUserAccess_Authorizer.setPurchaseorderuseraccessId(poUserAccessId_Authorizer);

			try {
				poUserAccessService.createPurchaseOrderUserAccess(purchaseOrderUserAccess_Authorizer);
			} catch (Exception e) {
				LOGGER.error(e);
			}
		}

	}

	/**
	 * @param purchaseOrder
	 * @param obj
	 * @param redirectAttributes
	 *            Verify Purchase Order
	 */
	public void verifyPurchaseOrder(PurchaseOrder purchaseOrder, PoMainForm obj,
			RedirectAttributes redirectAttributes) {
		MDC.put("sessionId",
				AbstractMainController.getCompany().isPresent() ? AbstractMainController.getCompany().get() : "");
		PurchaseOrder verify_purchaseorder = poMainService.getPurchaseOrderByPoNo(purchaseOrder.getPoNo());
		String date_format = "dd-MM-yyyy hh:mm:ss";
		SystemParameter dateObj_1 = sysRepo.getSystemParameterByPropName("DATE_TIME_FORMAT");
		if (dateObj_1 != null) {
			if (!dateObj_1.getPropValue().isEmpty()) {
				date_format = dateObj_1.getPropValue();
			}
		}
		DateFormat formatter_1 = new SimpleDateFormat(date_format);
		PurchaseOrderUserAccessPk pkey_Authorizer_current = new PurchaseOrderUserAccessPk(
				verify_purchaseorder.getPoNo(), verify_purchaseorder.getAuthorizer(), "A");
		verify_purchaseorder.setStatus("Verified");
		verify_purchaseorder.setVerifiedOn(new Date());
		verify_purchaseorder.setVerifyRemark(obj.getVerifyRemark());
		verify_purchaseorder.setLastModifiedOn(new Date());
		verify_purchaseorder.setAuthorizer(extract_issuer(obj.getAuthorizer()));

		try {
			poMainService.createPurchaseOrder(verify_purchaseorder);
			PurchaseOrderSendMailUtil email = new PurchaseOrderSendMailUtil(verify_purchaseorder.getPoNo(),
					getCompany().get());
			email.verifyPurchaseOrder();

			redirectAttributes.addFlashAttribute("info", "Purchase Order Verified successfully");
		} catch (Exception e) {
			LOGGER.error(e);
		}

		if (verify_purchaseorder.getVerifiedOn() != null) {

			verifiedon = formatter_1.format(verify_purchaseorder.getVerifiedOn());
		}

		if (verify_purchaseorder.getAuthorizer() != null) {

			PurchaseOrderUserAccess current_authorizer_poaccess = poUserAccessService
					.getPurchaseOrderUserAccessByPK(pkey_Authorizer_current);

			if (current_authorizer_poaccess != null) {
				poaccessrepo.delete(current_authorizer_poaccess);
				PurchaseOrderUserAccessPk pkey_new = new PurchaseOrderUserAccessPk(verify_purchaseorder.getPoNo(),
						verify_purchaseorder.getAuthorizer(), "A");
				current_authorizer_poaccess.setPurchaseorderuseraccessId(pkey_new);
				try {

					poUserAccessService.createPurchaseOrderUserAccess(current_authorizer_poaccess);
				} catch (Exception e) {
					LOGGER.error(e);
				}

			}
		}
	}

	public void verifyPurchaseOrder_direct_fromedit(PurchaseOrder purchaseOrder, PoMainForm obj,
			RedirectAttributes redirectAttributes) {
		MDC.put("sessionId",
				AbstractMainController.getCompany().isPresent() ? AbstractMainController.getCompany().get() : "");
		PurchaseOrder verify_purchaseorder = poMainService.getPurchaseOrderByPoNo(purchaseOrder.getPoNo());
		String date_format = "dd-MM-yyyy hh:mm:ss";
		SystemParameter dateObj_1 = sysRepo.getSystemParameterByPropName("DATE_TIME_FORMAT");
		if (dateObj_1 != null) {
			if (!dateObj_1.getPropValue().isEmpty()) {
				date_format = dateObj_1.getPropValue();
			}
		}
		DateFormat formatter_1 = new SimpleDateFormat(date_format);

		verify_purchaseorder.setStatus("Verified");
		verify_purchaseorder.setVerifiedOn(new Date());
		verify_purchaseorder.setVerifyRemark(obj.getVerifyRemark());
		verify_purchaseorder.setLastModifiedOn(new Date());
		verify_purchaseorder.setAuthorizer(extract_issuer(obj.getAuthorizer()));

		try {
			poMainService.createPurchaseOrder(verify_purchaseorder);
			PurchaseOrderSendMailUtil email = new PurchaseOrderSendMailUtil(verify_purchaseorder.getPoNo(),
					getCompany().get());
			email.verifyPurchaseOrder();

			redirectAttributes.addFlashAttribute("info", "Purchase Order Verified successfully");
		} catch (Exception e) {
			LOGGER.error(e);
		}

		if (verify_purchaseorder.getVerifiedOn() != null) {

			verifiedon = formatter_1.format(verify_purchaseorder.getVerifiedOn());
		}

		if (verify_purchaseorder.getAuthorizer() != null && verify_purchaseorder.getVerifier() != null) {

			PurchaseOrderUserAccess new_acc_v = new PurchaseOrderUserAccess();
			PurchaseOrderUserAccess new_acc_A = new PurchaseOrderUserAccess();

			PurchaseOrderUserAccessPk pkey_new_v = new PurchaseOrderUserAccessPk(verify_purchaseorder.getPoNo(),
					verify_purchaseorder.getVerifier(), "V");
			new_acc_v.setPurchaseorderuseraccessId(pkey_new_v);

			try {

				poUserAccessService.createPurchaseOrderUserAccess(new_acc_v);
			} catch (Exception e) {
				LOGGER.error(e);
			}

			PurchaseOrderUserAccessPk pkey_new_A = new PurchaseOrderUserAccessPk(verify_purchaseorder.getPoNo(),
					verify_purchaseorder.getAuthorizer(), "A");
			new_acc_A.setPurchaseorderuseraccessId(pkey_new_A);

			try {

				poUserAccessService.createPurchaseOrderUserAccess(new_acc_A);
			} catch (Exception e) {
				LOGGER.error(e);
			}

		}
	}

	/**
	 * @param purchaseOrder
	 * @param obj
	 * @param redirectAttributes
	 *            Verify New Purchase Order_Directly
	 */
	public void verify_newPurchaseOrder_directly(PurchaseOrder purchaseOrder, PoMainForm obj,
			RedirectAttributes redirectAttributes) {
		MDC.put("sessionId",
				AbstractMainController.getCompany().isPresent() ? AbstractMainController.getCompany().get() : "");
		PurchaseOrder verify_purchaseorder = poMainService.getPurchaseOrderByPoNo(purchaseOrder.getPoNo());
		String date_format = "dd-MM-yyyy hh:mm:ss";
		SystemParameter dateObj_1 = sysRepo.getSystemParameterByPropName("DATE_TIME_FORMAT");
		if (dateObj_1 != null) {
			if (!dateObj_1.getPropValue().isEmpty()) {
				date_format = dateObj_1.getPropValue();
			}
		}
		DateFormat formatter_1 = new SimpleDateFormat(date_format);
		PurchaseOrderUserAccessPk pkey_Authorizer_current = new PurchaseOrderUserAccessPk(
				verify_purchaseorder.getPoNo(), verify_purchaseorder.getAuthorizer(), "A");
		verify_purchaseorder.setStatus("Verified");
		verify_purchaseorder.setVerifiedOn(new Date());
		verify_purchaseorder.setVerifyRemark(obj.getVerifyRemark());

		try {
			poMainService.createPurchaseOrder(verify_purchaseorder);
			PurchaseOrderSendMailUtil email = new PurchaseOrderSendMailUtil(verify_purchaseorder.getPoNo(),
					getCompany().get());
			email.verifyPurchaseOrder();

			auditService.createAudit(new Date(), " Purchase Order: " + obj.getPoNo() + " Verified successfully",
					getCurrentUser().get(), " Verifying Purchase Order", getCompany().get(),
					getProfitCenterCode().getCode());
			redirectAttributes.addFlashAttribute("info", "Purchase Order Verified successfully");
		} catch (Exception e) {
			LOGGER.error(e);
		}

		if (verify_purchaseorder.getVerifiedOn() != null) {

			verifiedon = formatter_1.format(verify_purchaseorder.getVerifiedOn());
		}

		if (verify_purchaseorder.getIssuer() != null) {

			PurchaseOrderUserAccess purchaseOrderUserAccess_Draft = new PurchaseOrderUserAccess();
			PurchaseOrderUserAccessPk poUserAccessId_Drafter = new PurchaseOrderUserAccessPk(
					verify_purchaseorder.getPoNo(), verify_purchaseorder.getIssuer(), "D");

			purchaseOrderUserAccess_Draft.setPurchaseorderuseraccessId(poUserAccessId_Drafter);

			try {
				poUserAccessService.createPurchaseOrderUserAccess(purchaseOrderUserAccess_Draft);
			} catch (Exception e) {
				LOGGER.error(e);
			}
		}
		if (verify_purchaseorder.getVerifier() != null && verify_purchaseorder.getAuthorizer() != null) {
			PurchaseOrderUserAccess purchaseOrderUserAccess_Verifier = new PurchaseOrderUserAccess();
			PurchaseOrderUserAccessPk poUserAccessId_Verifier = new PurchaseOrderUserAccessPk(
					verify_purchaseorder.getPoNo(), verify_purchaseorder.getVerifier(), "V");
			PurchaseOrderUserAccess purchaseOrderUserAccess_Authorizer = new PurchaseOrderUserAccess();
			PurchaseOrderUserAccessPk poUserAccessId_Authorizer = new PurchaseOrderUserAccessPk(
					verify_purchaseorder.getPoNo(), verify_purchaseorder.getAuthorizer(), "A");

			purchaseOrderUserAccess_Verifier.setPurchaseorderuseraccessId(poUserAccessId_Verifier);

			try {
				poUserAccessService.createPurchaseOrderUserAccess(purchaseOrderUserAccess_Verifier);
			} catch (Exception e) {
				LOGGER.error(e);
			}

			purchaseOrderUserAccess_Authorizer.setPurchaseorderuseraccessId(poUserAccessId_Authorizer);

			try {
				poUserAccessService.createPurchaseOrderUserAccess(purchaseOrderUserAccess_Authorizer);
			} catch (Exception e) {
				LOGGER.error(e);
			}
		}

	}

	/**
	 * @param purchaseOrder
	 * @param obj
	 * @param redirectAttributes
	 *            Authorizing Purchase Order
	 */
	public void authorizePurchaseOrder(PurchaseOrder purchaseOrder, PoMainForm obj,
			RedirectAttributes redirectAttributes) {
		MDC.put("sessionId",
				AbstractMainController.getCompany().isPresent() ? AbstractMainController.getCompany().get() : "");
		PurchaseOrder authorize_purchaseorder = poMainService.getPurchaseOrderByPoNo(purchaseOrder.getPoNo());
		String date_format = "dd-MM-yyyy hh:mm:ss";
		SystemParameter dateObj_1 = sysRepo.getSystemParameterByPropName("DATE_TIME_FORMAT");
		if (dateObj_1 != null) {
			if (!dateObj_1.getPropValue().isEmpty()) {
				date_format = dateObj_1.getPropValue();
			}
		}
		DateFormat formatter_1 = new SimpleDateFormat(date_format);

		authorize_purchaseorder.setStatus("Authorized");
		authorize_purchaseorder.setAuthorizedOn(new Date());
		authorize_purchaseorder.setAuthorizer(extract_issuer(obj.getAuthorizer()));
		authorize_purchaseorder.setAuthorizeRemark(obj.getVerifyRemark());

		try {
			poMainService.createPurchaseOrder(authorize_purchaseorder);
			PurchaseOrderSendMailUtil email = new PurchaseOrderSendMailUtil(authorize_purchaseorder.getPoNo(),
					getCompany().get());
			email.authorizedPurchaseOrder();
			auditService.createAudit(new Date(), "Purchase Order: " + obj.getPoNo() + " Authorized successfully",
					getCurrentUser().get(), "Authorizing Purchase Order", getCompany().get(),
					getProfitCenterCode().getCode());
			redirectAttributes.addFlashAttribute("info", "Purchase Order Authorized successfully");
		} catch (Exception e) {
			LOGGER.error(e);
		}

		if (authorize_purchaseorder.getVerifiedOn() != null) {

			authorizedon = formatter_1.format(authorize_purchaseorder.getAuthorizedOn());
		}
	}

	public void forward_to_invoicer(PurchaseOrder purchaseOrder, PoMainForm obj,
			RedirectAttributes redirectAttributes) {
		MDC.put("sessionId",
				AbstractMainController.getCompany().isPresent() ? AbstractMainController.getCompany().get() : "");
		PurchaseOrder forward_invoicer_purchaseorder = poMainService.getPurchaseOrderByPoNo(purchaseOrder.getPoNo());
		if (obj.getInvoicer() != null) {
			PurchaseOrderUserAccess invoicer = new PurchaseOrderUserAccess();
			PurchaseOrderUserAccessPk pkey = new PurchaseOrderUserAccessPk(forward_invoicer_purchaseorder.getPoNo(),
					obj.getInvoicer(), "I");
			invoicer.setPurchaseorderuseraccessId(pkey);

			try {
				poUserAccessService.createPurchaseOrderUserAccess(invoicer);
				PurchaseOrderSendMailUtil email = new PurchaseOrderSendMailUtil(
						forward_invoicer_purchaseorder.getPoNo(), getCompany().get());
				email.forwardPurchaseOrder();
				auditService.createAudit(new Date(), " Purchase Order: " + obj.getPoNo() + " Forwarded successfully",
						getCurrentUser().get(), " Authorizing Purchase Order", getCompany().get(),
						getProfitCenterCode().getCode());
			} catch (Exception e) {
				LOGGER.error(e);
			}

		}

	}

	public void forward_verifiedpo_invoicer(PurchaseOrder purchaseOrder, PoMainForm obj,
			RedirectAttributes redirectAttributes) {
		MDC.put("sessionId",
				AbstractMainController.getCompany().isPresent() ? AbstractMainController.getCompany().get() : "");
		PurchaseOrder forward_verifiedpo_invoicer = poMainService.getPurchaseOrderByPoNo(purchaseOrder.getPoNo());
		if (obj.getForward_verifiedpo_invoicer() != null) {
			PurchaseOrderUserAccess invoicer = new PurchaseOrderUserAccess();
			PurchaseOrderUserAccessPk pkey = new PurchaseOrderUserAccessPk(forward_verifiedpo_invoicer.getPoNo(),
					obj.getForward_verifiedpo_invoicer(), "I");
			invoicer.setPurchaseorderuseraccessId(pkey);

			try {
				poUserAccessService.createPurchaseOrderUserAccess(invoicer);
				PurchaseOrderSendMailUtil email = new PurchaseOrderSendMailUtil(forward_verifiedpo_invoicer.getPoNo(),
						getCompany().get());
				email.forwardPurchaseOrder();
				auditService.createAudit(new Date(),
						" Verified Purchase Order: " + forward_verifiedpo_invoicer.getPoNo() + "Forwarded successfully",
						getCurrentUser().get(), " Forwarding Purchase Order", getCompany().get(),
						getProfitCenterCode().getCode());
			} catch (Exception e) {
				LOGGER.error(e);
			}

		}

	}

	/**
	 * @param purchaseOrder
	 * @param obj
	 * @param redirectAttributes
	 *            Redraft PO Status will update to Redraft
	 */
	public void redraft(PurchaseOrder purchaseOrder, PoMainForm obj, RedirectAttributes redirectAttributes) {
		PurchaseOrder redraft_purchaseorder = poMainService.getPurchaseOrderByPoNo(purchaseOrder.getPoNo());
		MDC.put("sessionId",
				AbstractMainController.getCompany().isPresent() ? AbstractMainController.getCompany().get() : "");
		if (obj.getVerifier() != null && obj.getAuthorizer() != null) {

			PurchaseOrderUserAccess redraft_access = new PurchaseOrderUserAccess();
			PurchaseOrderUserAccessPk pkey_old_v = new PurchaseOrderUserAccessPk(redraft_purchaseorder.getPoNo(),
					redraft_purchaseorder.getVerifier(), "V");

			PurchaseOrderUserAccess redraft_access_A = new PurchaseOrderUserAccess();
			PurchaseOrderUserAccessPk pkey_old_A = new PurchaseOrderUserAccessPk(redraft_purchaseorder.getPoNo(),
					redraft_purchaseorder.getAuthorizer(), "A");

			if (pkey_old_v != null) {
				redraft_access = poUserAccessService.getPurchaseOrderUserAccessByPK(pkey_old_v);

				if (redraft_access != null) {
					poaccessrepo.delete(redraft_access);
				}
			}

			if (pkey_old_A != null) {
				redraft_access_A = poUserAccessService.getPurchaseOrderUserAccessByPK(pkey_old_A);
				if (redraft_access_A != null) {
					poaccessrepo.delete(redraft_access_A);
				}
			}
		}

		if (obj.getPoNo() != null) {
			redraft_purchaseorder.setAuthorizer(extract_issuer(obj.getAuthorizer()));
			redraft_purchaseorder.setStatus("Redraft");
			redraft_purchaseorder.setRedraftRemark(obj.getRedraftRemark());

			try {
				poMainService.createPurchaseOrder(redraft_purchaseorder);
				PurchaseOrderSendMailUtil email = new PurchaseOrderSendMailUtil(purchaseOrder.getPoNo(),
						getCompany().get());
				email.redraftPurchaseOrder();
				auditService.createAudit(new Date(), " Purchase Order: " + obj.getPoNo() + " Redraft successfully",
						getCurrentUser().get(), " Redraft Purchase Order", getCompany().get(),
						getProfitCenterCode().getCode());
				redirectAttributes.addFlashAttribute("info", "Purchase Order Redraft successfully");

			} catch (Exception e) {
				LOGGER.error(e);
			}

		}

	}

	/**
	 * @param purchaseOrder
	 * @param obj
	 * @param redirectAttributes
	 * @throws UnsupportedEncodingException
	 * @throws ParseException
	 *             Duplicate The Purchase Order if Status="Draft", in user access
	 *             table: duplicate action D Anything else than Draft, Duplicate D,A
	 *             and V Action
	 */
	public void duplicate(PurchaseOrder purchaseOrder, PoMainForm obj, RedirectAttributes redirectAttributes)
			throws UnsupportedEncodingException, ParseException {
		MDC.put("sessionId",
				AbstractMainController.getCompany().isPresent() ? AbstractMainController.getCompany().get() : "");
		PurchaseOrder current_purchaseOrder = poMainService.getPurchaseOrderByPoNo(obj.getPoNo());

		/* Create New Instance Of PO to duplicate */
		PurchaseOrder duplicate_purchaseorder = new PurchaseOrder();

		/* Get Information of po from DB to Form Object */
		tempobj_duplicate = entityToPojo(current_purchaseOrder);

		/* Bind Information of po from View To Entity Object */
		duplicate_purchaseorder = pojoToEntity(duplicate_purchaseorder, tempobj_duplicate, redirectAttributes);

		try {
			poMainService.createPurchaseOrder(duplicate_purchaseorder);
			auditService.createAudit(new Date(), " Purchase Order: " + obj.getPoNo() + " Duplicated successfully",
					getCurrentUser().get(), " Duplicating Purchase Order", getCompany().get(),
					getProfitCenterCode().getCode());
			redirectAttributes.addFlashAttribute("info", "Purchase Order Duplicated successfully");
		} catch (Exception e) {
			LOGGER.error(e);
		}

		if (duplicate_purchaseorder.getStatus().equals("Draft")
				&& duplicate_purchaseorder.getStatus().equals("Redraft")) {

			PurchaseOrderUserAccess duplicate_D = new PurchaseOrderUserAccess();
			PurchaseOrderUserAccessPk pkey_D = new PurchaseOrderUserAccessPk(duplicate_purchaseorder.getPoNo(),
					duplicate_purchaseorder.getIssuer(), "D");
			duplicate_D.setPurchaseorderuseraccessId(pkey_D);

			try {
				poUserAccessService.createPurchaseOrderUserAccess(duplicate_D);
			} catch (Exception e) {
				LOGGER.error(e);
			}
		} else {
			PurchaseOrderUserAccess duplicate_D = new PurchaseOrderUserAccess();
			PurchaseOrderUserAccessPk pkey_D = new PurchaseOrderUserAccessPk(duplicate_purchaseorder.getPoNo(),
					duplicate_purchaseorder.getIssuer(), "D");
			duplicate_D.setPurchaseorderuseraccessId(pkey_D);

			try {
				poUserAccessService.createPurchaseOrderUserAccess(duplicate_D);
			} catch (Exception e) {
				LOGGER.error(e);
			}
			PurchaseOrderUserAccess duplicate_V = new PurchaseOrderUserAccess();
			PurchaseOrderUserAccessPk pkey_V = new PurchaseOrderUserAccessPk(duplicate_purchaseorder.getPoNo(),
					duplicate_purchaseorder.getVerifier(), "V");
			duplicate_V.setPurchaseorderuseraccessId(pkey_V);

			try {
				poUserAccessService.createPurchaseOrderUserAccess(duplicate_V);
			} catch (Exception e) {
				LOGGER.error(e);
			}
			PurchaseOrderUserAccess duplicate_A = new PurchaseOrderUserAccess();
			PurchaseOrderUserAccessPk pkey_A = new PurchaseOrderUserAccessPk(duplicate_purchaseorder.getPoNo(),
					duplicate_purchaseorder.getAuthorizer(), "A");
			duplicate_A.setPurchaseorderuseraccessId(pkey_A);

			try {
				poUserAccessService.createPurchaseOrderUserAccess(duplicate_A);
			} catch (Exception e) {
				LOGGER.error(e);
			}
		}
	}

	public void reject(PurchaseOrder purchaseOrder, PoMainForm obj, RedirectAttributes redirectAttributes) {
		String date_format = "dd-MM-yyyy hh:mm:ss";
		MDC.put("sessionId",
				AbstractMainController.getCompany().isPresent() ? AbstractMainController.getCompany().get() : "");
		SystemParameter dateObj_1 = sysRepo.getSystemParameterByPropName("DATE_TIME_FORMAT");
		if (dateObj_1 != null) {
			if (!dateObj_1.getPropValue().isEmpty()) {
				date_format = dateObj_1.getPropValue();
			}
		}
		DateFormat formatter_1 = new SimpleDateFormat(date_format);
		PurchaseOrder reject_obj = poMainService.getPurchaseOrderByPoNo(obj.getPoNo());
		if (obj.getPoNo() != null) {

			reject_obj.setStatus("Rejected");
			reject_obj.setRejectRemark(obj.getRejectRemark());
			reject_obj.setRejectedBy(obj.getIssuer());
			reject_obj.setRejectedOn(new Date());
			try {
				poMainService.createPurchaseOrder(reject_obj);
				PurchaseOrderSendMailUtil email = new PurchaseOrderSendMailUtil(reject_obj.getPoNo(),
						getCompany().get());
				email.rejectPurchaseOrder();
				auditService.createAudit(new Date(), " Purchase Order: " + obj.getPoNo() + " Rejected successfully",
						getCurrentUser().get(), " Rejecting Purchase Order", getCompany().get(),
						getProfitCenterCode().getCode());
				redirectAttributes.addFlashAttribute("info", "Purchase Order Rejected successfully");
			} catch (Exception e) {
				LOGGER.error(e);
			}

		}

		if (reject_obj.getRejectedOn() != null) {

			rejectedon = formatter_1.format(reject_obj.getRejectedOn());
		}

	}

	public void cancel(PurchaseOrder purchaseOrder, PoMainForm obj, RedirectAttributes redirectAttributes) {
		String date_format = "dd-MM-yyyy hh:mm:ss";
		MDC.put("sessionId",
				AbstractMainController.getCompany().isPresent() ? AbstractMainController.getCompany().get() : "");
		SystemParameter dateObj_1 = sysRepo.getSystemParameterByPropName("DATE_TIME_FORMAT");
		if (dateObj_1 != null) {
			if (!dateObj_1.getPropValue().isEmpty()) {
				date_format = dateObj_1.getPropValue();
			}
		}
		DateFormat formatter_1 = new SimpleDateFormat(date_format);
		PurchaseOrder cancel_obj = poMainService.getPurchaseOrderByPoNo(obj.getPoNo());
		if (obj.getPoNo() != null) {

			cancel_obj.setStatus("Cancelled");
			cancel_obj.setCancelRemark(obj.getCancelRemark());
			cancel_obj.setCancelledBy(obj.getVerifier());
			cancel_obj.setCancelledOn(new Date());
			try {
				poMainService.createPurchaseOrder(cancel_obj);
				PurchaseOrderSendMailUtil email = new PurchaseOrderSendMailUtil(purchaseOrder.getPoNo(),
						getCompany().get());
				email.cancelPurchaseOrder();
				auditService.createAudit(new Date(), " Purchase Order: " + obj.getPoNo() + " Cancelled successfully",
						getCurrentUser().get(), " Cancelling Purchase Order", getCompany().get(),
						getProfitCenterCode().getCode());
				redirectAttributes.addFlashAttribute("info", "Purchase Order Cancelled successfully");
			} catch (Exception e) {
				LOGGER.error(e);
			}

		}

		if (cancel_obj.getCancelledOn() != null) {

			cancelledon = formatter_1.format(cancel_obj.getCancelledOn());
		}

	}

	@GetMapping(value = "/main/reopen_closed")
	public String Reopen_Closed_PO(@RequestParam String poNo, Model model, RedirectAttributes redirectAttributes)
			throws UnsupportedEncodingException, ParseException {
		MDC.put("sessionId",
				AbstractMainController.getCompany().isPresent() ? AbstractMainController.getCompany().get() : "");
		if (poNo != null) {

			PurchaseOrder po_obj = poMainService.getPurchaseOrderByPoNo(poNo);

			po_obj.setStatus("Invoiced");
			po_obj.setLastModifiedOn(new Date());

			try {
				poMainService.createPurchaseOrder(po_obj);
				auditService.createAudit(new Date(), " Purchase Order: " + po_obj.getPoNo() + " Reopened successfully",
						getCurrentUser().get(), " Reopening Purchase Order", getCompany().get(),
						getProfitCenterCode().getCode());
				redirectAttributes.addFlashAttribute("info", "Purchase Order Reopened successfully");
			} catch (Exception e) {
				LOGGER.error(e);
			}

		}

		return "redirect:/main/mypos";

	}

	public void reopen(PurchaseOrder purchaseOrder, PoMainForm obj, RedirectAttributes redirectAttributes) {
		PurchaseOrder reopen_purchaseOrder = poMainService.getPurchaseOrderByPoNo(obj.getPoNo());
		MDC.put("sessionId",
				AbstractMainController.getCompany().isPresent() ? AbstractMainController.getCompany().get() : "");
		if (obj.getPoNo() != null) {
			reopen_purchaseOrder.setStatus("Invoiced");
			try {
				poMainService.createPurchaseOrder(reopen_purchaseOrder);
				auditService.createAudit(new Date(), " Purchase Order: " + obj.getPoNo() + " Reopened successfully",
						getCurrentUser().get(), " Reopening Purchase Order", getCompany().get(),
						getProfitCenterCode().getCode());
				redirectAttributes.addFlashAttribute("info", "Purchase Order Reopened successfully");
			} catch (Exception e) {
				LOGGER.error(e);
			}

		}

	}

	/**
	 * @param purchaseOrder
	 * @param obj
	 * @param redirectAttributes
	 *            Delete Purchase Order status update to "Deleted"
	 */
	public void delete(PurchaseOrder purchaseOrder, PoMainForm obj, RedirectAttributes redirectAttributes) {
		MDC.put("sessionId",
				AbstractMainController.getCompany().isPresent() ? AbstractMainController.getCompany().get() : "");
		PurchaseOrder delete_purchaseOrder = poMainService.getPurchaseOrderByPoNo(obj.getPoNo());
		String date_format = "dd-MM-yyyy hh:mm:ss";
		SystemParameter dateObj_1 = sysRepo.getSystemParameterByPropName("DATE_TIME_FORMAT");
		if (dateObj_1 != null) {
			if (!dateObj_1.getPropValue().isEmpty()) {
				date_format = dateObj_1.getPropValue();
			}
		}
		DateFormat formatter_1 = new SimpleDateFormat(date_format);

		if (obj.getPoNo() != null) {
			delete_purchaseOrder.setStatus("Deleted");
			delete_purchaseOrder.setDeletedBy(obj.getIssuer());
			delete_purchaseOrder.setDeletedOn(new Date());
			delete_purchaseOrder.setDeleteRemark(obj.getDeleteRemark());
			try {
				poMainService.createPurchaseOrder(delete_purchaseOrder);
				PurchaseOrderSendMailUtil email = new PurchaseOrderSendMailUtil(delete_purchaseOrder.getPoNo(),
						getCompany().get());
				email.deletePurchaseOrder();
				auditService.createAudit(new Date(), "Purchase Order: " + obj.getPoNo() + " Deleted successfully",
						getCurrentUser().get(), "Deleting Purchase Order", getCompany().get(),
						getProfitCenterCode().getCode());
				redirectAttributes.addFlashAttribute("info", "Purchase Order Deleted successfully");
			} catch (Exception e) {
				LOGGER.error(e);
			}

		}

		if (delete_purchaseOrder.getDeletedOn() != null) {

			deletedon = formatter_1.format(delete_purchaseOrder.getDeletedOn());
		}

	}

	/**
	 * @param purchaseOrder
	 * @param obj
	 * @param redirectAttributes
	 */
	public void forward_to_verifier(PurchaseOrder purchaseOrder, PoMainForm obj,
			RedirectAttributes redirectAttributes) {
		MDC.put("sessionId",
				AbstractMainController.getCompany().isPresent() ? AbstractMainController.getCompany().get() : "");
		PurchaseOrder f_to_V_obj = poMainService.getPurchaseOrderByPoNo(obj.getPoNo());

		// If Authorizer has changed, in user access also updating A
		if (obj.getAuthorizer() != null) {

			PurchaseOrderUserAccessPk pkey_auth_current = new PurchaseOrderUserAccessPk(f_to_V_obj.getPoNo(),
					f_to_V_obj.getAuthorizer(), "A");

			PurchaseOrderUserAccess current_auth_poaccess = poUserAccessService
					.getPurchaseOrderUserAccessByPK(pkey_auth_current);

			// Set the new authorizer in po main
			f_to_V_obj.setAuthorizer(extract_issuer(obj.getAuthorizer()));

			try {
				poMainService.createPurchaseOrder(f_to_V_obj);

			} catch (Exception e) {
				LOGGER.error(e);
			}

			if (current_auth_poaccess != null) {
				poaccessrepo.delete(current_auth_poaccess);
				// create new A for user access
				PurchaseOrderUserAccessPk pkey_auth_new = new PurchaseOrderUserAccessPk(f_to_V_obj.getPoNo(),
						f_to_V_obj.getAuthorizer(), "A");
				current_auth_poaccess.setPurchaseorderuseraccessId(pkey_auth_new);
				try {

					poUserAccessService.createPurchaseOrderUserAccess(current_auth_poaccess);
				} catch (Exception e) {
					LOGGER.error(e);
				}
			}
		}

		if (obj.getForward_verifier() != null) {

			// find the record with old verifier
			PurchaseOrderUserAccessPk pkey_current = new PurchaseOrderUserAccessPk(f_to_V_obj.getPoNo(),
					f_to_V_obj.getVerifier(), "V");
			String[] forward_verifierArray = obj.getForward_verifier().split("-");

			// set new verifier to po table
			f_to_V_obj.setVerifier(forward_verifierArray[0].trim());

			// update po table with new verifier
			try {
				poMainService.createPurchaseOrder(f_to_V_obj);
				PurchaseOrderSendMailUtil email = new PurchaseOrderSendMailUtil(f_to_V_obj.getPoNo(),
						getCompany().get());
				email.forwardPurchaseOrder();
				auditService.createAudit(new Date(),
						"Purchase Order: " + obj.getPoNo() + " Forwarded to other verifier successfully",
						getCurrentUser().get(), "Forwarding Purchase Order to Verifier", getCompany().get(),
						getProfitCenterCode().getCode());
			} catch (Exception e) {
				LOGGER.error(e);
			}
			/*
			 * if user forward to new verifier , po user access also will get update
			 */
			PurchaseOrderUserAccess current_verifier_poaccess = poUserAccessService
					.getPurchaseOrderUserAccessByPK(pkey_current);

			if (current_verifier_poaccess != null) {
				poaccessrepo.delete(current_verifier_poaccess);
				PurchaseOrderUserAccessPk pkey_new = new PurchaseOrderUserAccessPk(purchaseOrder.getPoNo(),
						forward_verifierArray[0], "V");
				current_verifier_poaccess.setPurchaseorderuseraccessId(pkey_new);
				try {

					// update user access with new V record
					poUserAccessService.createPurchaseOrderUserAccess(current_verifier_poaccess);
				} catch (Exception e) {
					LOGGER.error(e);
				}
			}
		}

	}

	private PurchaseOrder pojoToEntity(PurchaseOrder purchaseorder, PoMainForm duplicate_obj,
			RedirectAttributes redirectAttributes) throws ParseException, UnsupportedEncodingException {

		PurchaseOrder pojo_entity_obj = new PurchaseOrder();
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_FORMAT");
		String dateFormat = "dd-MM-yyyy";
		if (dateObj != null && !dateObj.getPropValue().isEmpty()) {
			dateFormat = dateObj.getPropValue();
		}
		DateFormat formatter = new SimpleDateFormat(dateFormat);

		String dateFormat_1 = "dd-MM-yyyy hh:mm:ss";
		SystemParameter dateObj_1 = sysRepo.getSystemParameterByPropName("DATE_TIME_FORMAT");
		if (dateObj_1 != null && !dateObj_1.getPropValue().isEmpty()) {
			dateFormat_1 = dateObj_1.getPropValue();
		}
		DateFormat formatter_1 = new SimpleDateFormat(dateFormat_1);

		String[] profitcenter_array = duplicate_obj.getProfitcenter().split("-");
		pojo_entity_obj.setPoNo(ponumber(profitcenter_array[0], redirectAttributes));
		pojo_entity_obj.setContactPersonEmail(duplicate_obj.getContactPersonEmail());
		pojo_entity_obj.setContactPersonPhone(duplicate_obj.getContactPersonPhone());

		pojo_entity_obj.setContactPersonName(duplicate_obj.getContactPersonName());

		if (duplicate_obj.getContactPerson() != null) {
			String[] contact_person_Array = duplicate_obj.getContactPerson().split("-");
			pojo_entity_obj.setContactPerson(contact_person_Array[0]);
		}

		pojo_entity_obj.setAddressLine1(duplicate_obj.getAddressLine1());
		pojo_entity_obj.setAddressLine2(duplicate_obj.getAddressLine2());
		pojo_entity_obj.setAddressLine3(duplicate_obj.getAddressLine3());
		pojo_entity_obj.setPostCode(duplicate_obj.getPostCode());
		pojo_entity_obj.setPostArea(duplicate_obj.getPostArea());
		String[] vendorArray = duplicate_obj.getVendorCode().split("-");

		HashMap<String, String> vend_info_Map = new HashMap<String, String>();
		if (duplicate_obj.getVendorCode() != null) {
			vend_info_Map = extract_vendinfo(duplicate_obj.getVendorCode());
		}
		pojo_entity_obj.setVendorcode(vend_info_Map.get("vendorcode"));
		pojo_entity_obj.setVendorPersonInCharge(duplicate_obj.getVendorPersonInCharge());
		pojo_entity_obj.setVendorPersonInChargeEmail(duplicate_obj.getVendorPersonInChargeEmail());
		pojo_entity_obj.setVendorPersonInChargePhone(duplicate_obj.getVendorPersonInChargePhone());
		pojo_entity_obj.setVendorAddressLine1(duplicate_obj.getVendorAddressLine1());
		pojo_entity_obj.setVendorAddressLine2(duplicate_obj.getVendorAddressLine2());
		pojo_entity_obj.setVendorAddressLine3(duplicate_obj.getVendorAddressLine3());
		pojo_entity_obj.setVendorPostCode(duplicate_obj.getPostCode());
		pojo_entity_obj.setVendorPostArea(duplicate_obj.getVendorPostArea());
		String[] pc_Array = duplicate_obj.getProfitcenter().split("-");
		pojo_entity_obj.setPcCode(pc_Array[0]);
		pojo_entity_obj.setStatus(duplicate_obj.getStatus());
		pojo_entity_obj.setCompCode(duplicate_obj.getCompCode());

		/*
		 * if (duplicate_obj.getCredittermscode() != null) {
		 * pojo_entity_obj.setCreditTermsCode(duplicate_obj.getCredittermscode());
		 * crt_edit = purchaseorder.getCreditTermsCode(); }
		 */

		if (duplicate_obj.getCredittermscode() != null) {
			String crt_code = extract_crt(duplicate_obj.getCredittermscode());
			if (crt_code != null) {
				pojo_entity_obj.setCreditTermsCode(crt_code);
			} else {
				redirectAttributes.addFlashAttribute("error", "No Credit Terms Found!");
			}
		}

		pojo_entity_obj.setCurrency(duplicate_obj.getCurrency());
		pojo_entity_obj.setOrderCatCode(duplicate_obj.getOrdCatCode());
		pojo_entity_obj.setOrdSubCatCode(duplicate_obj.getOrdSubCCode());
		pojo_entity_obj.setPrTypeCode(duplicate_obj.getPrTypeCode());
		pojo_entity_obj.setQuoteNo(duplicate_obj.getQuoteNo());

		if (!duplicate_obj.getQuoteDate().isEmpty()) {
			pojo_entity_obj.setQuoteDate(formatter.parse(duplicate_obj.getQuoteDate()));
		}

		pojo_entity_obj.setProjectCode(purchaseorder.getProjectCode());
		pojo_entity_obj.setPaymentTerms(purchaseorder.getPaymentTerms());
		pojo_entity_obj.setDeliveryInstruction(purchaseorder.getDeliveryInstruction());
		pojo_entity_obj.setIssuerRemark(purchaseorder.getIssuerRemark());

		if (duplicate_obj.getOrderCategoryAuthorizationGrid() != null) {
			pojo_entity_obj.setOrderCategoryAuthorizationGrid(duplicate_obj.getOrderCategoryAuthorizationGrid());
			orderAuthGrid = pojo_entity_obj.getOrderCategoryAuthorizationGrid();

			authDir = sysRepo.getSystemParameterValueByPropName(AUTHGRID_UPLOAD_PATH);
			path_edit = UriUtils.encodePath(authDir + orderAuthGrid, "UTF-8");
			newPath = UriUtils.encodePath(authDir + orderAuthGrid, "UTF-8");

		}
		String[] store_Array = duplicate_obj.getStoreName().split("-");
		String[] dept_Array = duplicate_obj.getDepartmentName().split("-");
		pojo_entity_obj.setStoreName(store_Array[1]);
		pojo_entity_obj.setDepartmentName(dept_Array[1]);
		pojo_entity_obj.setVendorbranchcode(duplicate_obj.getVendorbranchCode());
		pojo_entity_obj.setVendorName(duplicate_obj.getVendorName());
		pojo_entity_obj.setIssuer(extract_issuer(duplicate_obj.getIssuer()));
		pojo_entity_obj.setVerifier(extract_issuer(duplicate_obj.getVerifier()));
		pojo_entity_obj.setAuthorizer(extract_issuer(duplicate_obj.getAuthorizer()));
		pojo_entity_obj.setFirstApprover(duplicate_obj.getFirstApprover());
		pojo_entity_obj.setSecondApprover(duplicate_obj.getSecondApprover());
		pojo_entity_obj.setThirdApprover(duplicate_obj.getThirdApprover());
		pojo_entity_obj.setFirstApproverDesignation(duplicate_obj.getFirstApproverDesignation());
		pojo_entity_obj.setSecondApproverDesignation(duplicate_obj.getSecondApproverDesignation());
		pojo_entity_obj.setThirdApproverDesignation(duplicate_obj.getThirdApproverDesignation());

		if (duplicate_obj.getIssuedOn() != null && !duplicate_obj.getIssuedOn().isEmpty()) {
			pojo_entity_obj.setIssuedOn(formatter_1.parse(duplicate_obj.getIssuedOn()));

		}
		if (duplicate_obj.getVerifiedOn() != null && !duplicate_obj.getVerifiedOn().isEmpty()) {
			pojo_entity_obj.setVerifiedOn(formatter_1.parse(duplicate_obj.getVerifiedOn()));
		}
		if (duplicate_obj.getAuthorizedOn() != null && !duplicate_obj.getAuthorizedOn().isEmpty()) {
			pojo_entity_obj.setAuthorizedOn(formatter_1.parse(duplicate_obj.getAuthorizedOn()));
		}

		if (duplicate_obj.getRejectedOn() != null && !duplicate_obj.getRejectedOn().isEmpty()) {
			pojo_entity_obj.setRejectedOn(formatter_1.parse(duplicate_obj.getRejectedOn()));
		}
		if (duplicate_obj.getDeletedOn() != null && !duplicate_obj.getDeletedOn().isEmpty()) {
			pojo_entity_obj.setDeletedOn(formatter_1.parse(duplicate_obj.getDeletedOn()));
		}

		if (duplicate_obj.getCancelledOn() != null && !duplicate_obj.getCancelledOn().isEmpty()) {
			pojo_entity_obj.setCancelledOn(formatter_1.parse(duplicate_obj.getCancelledOn()));
		}

		if (duplicate_obj.getDeletedBy() != null) {
			pojo_entity_obj.setDeletedBy(duplicate_obj.getDeletedBy());

		}

		if (duplicate_obj.getRejectedBy() != null) {

			pojo_entity_obj.setRejectedBy(duplicate_obj.getRejectedBy());
		}

		if (duplicate_obj.getClosedBy() != null) {
			pojo_entity_obj.setClosedBy(duplicate_obj.getClosedBy());
		}

		if (duplicate_obj.getCancelledBy() != null) {
			pojo_entity_obj.setCancelledBy(duplicate_obj.getCancelledBy());
		}

		pojo_entity_obj.setVerifyRemark(duplicate_obj.getVerifyRemark());

		pojo_entity_obj.setTotalExclGST(duplicate_obj.getTotExclGST());
		pojo_entity_obj.setTotalInclGST(duplicate_obj.getTotInclGST());
		pojo_entity_obj.setTotalGST(duplicate_obj.getTotGST());
		pojo_entity_obj.setOverAllDiscount(duplicate_obj.getOverAllDiscount());

		/***
		 * Duplicate Item Map *** /
		 * 
		 */

		List<ItemMap> current_item_list = itemMapRepo.getAllItemMapByPurchaseOrderNo(duplicate_obj.getPoNo());
		ItemMap duplicate_item = new ItemMap();
		if (current_item_list.size() > 0) {

			for (ItemMap itemlist_obj : current_item_list) {

				ItemMap item_obj = itemMapRepo.getLatestItemMapCode();
				int item_code = item_obj.getCode();
				duplicate_item.setCode(item_code + 1);
				duplicate_item.setPoNo(pojo_entity_obj.getPoNo());
				duplicate_item.setItemNo(itemlist_obj.getItemNo());
				duplicate_item.setParticulars(itemlist_obj.getParticulars());
				duplicate_item.setPacking(itemlist_obj.getPacking());
				duplicate_item.setItemOrder(itemlist_obj.getItemOrder());
				duplicate_item.setTaxcode(itemlist_obj.getTaxcode());
				duplicate_item.setTaxRate(itemlist_obj.getTaxRate());
				duplicate_item.setUnitPriceExclGST(itemlist_obj.getUnitPriceExclGST());
				duplicate_item.setUnitPriceInclGST(itemlist_obj.getUnitPriceInclGST());
				if (itemlist_obj.getDiscount() > 0) {
					duplicate_item.setDiscount(itemlist_obj.getDiscount());
				}
				double unitPrice = itemlist_obj.getUnitPriceExclGST();
				double quantity = itemlist_obj.getQuantity();
				double totalunitprice = 0;
				totalunitprice = unitPrice * quantity;
				double totalDiscount = 0;
				double discount = 0;
				if (itemlist_obj.getDiscount() > 0) {
					discount = itemlist_obj.getDiscount();
					totalDiscount = per(discount, totalunitprice);
					totalunitprice = totalunitprice - totalDiscount;
				} else {
					totalunitprice = unitPrice;
				}
				double tax = itemlist_obj.getTaxRate();
				double gst = per(tax, totalunitprice);
				duplicate_item.setTotalExclGST(totalunitprice);
				duplicate_item.setTotalInclGST(totalunitprice + gst);
				duplicate_item.setTotalGST(gst);
				duplicate_item.setTotalDiscount(per(discount, unitPrice));
				duplicate_item.setQuantity(itemlist_obj.getQuantity());
				duplicate_item.setUnitGST(itemlist_obj.getTaxRate());
				String pccode = itemlist_obj.getPcCode();
				duplicate_item.setPcCode(pccode.substring(0, 7));
				try {
					itemMapRepo.createItemMap(duplicate_item);
					auditService.createAudit(new Date(), " New Item Map Duplicated successfully",
							getCurrentUser().get(), " Item Map Duplication", getCompany().get(),
							duplicate_item.getPcCode());

				} catch (Exception e) {
					LOGGER.error(e);
				}
			}
		}

		return pojo_entity_obj;

	}

	private double per(double discount, double amount) {
		if (discount != 0) {
			double per = 0;
			per = discount * amount;
			double percentage = per / 100;
			return percentage;
		} else {
			return amount;
		}
	}

	private PoMainForm entityToPojo(PurchaseOrder purchaseorder) throws ParseException, UnsupportedEncodingException {
		PoMainForm obj = new PoMainForm();

		SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_FORMAT");
		String dateFormat = "dd-MM-yyyy";
		if (dateObj != null && !dateObj.getPropValue().isEmpty()) {
			dateFormat = dateObj.getPropValue();
		}
		DateFormat formatter = new SimpleDateFormat(dateFormat);

		String date_format = "dd-MM-yyyy hh:mm:ss";
		SystemParameter dateObj_1 = sysRepo.getSystemParameterByPropName("DATE_TIME_FORMAT");
		if (dateObj_1 != null) {
			if (!dateObj_1.getPropValue().isEmpty()) {
				date_format = dateObj_1.getPropValue();
			}
		}
		DateFormat formatter_1 = new SimpleDateFormat(date_format);

		obj.setContactPersonEmail(purchaseorder.getContactPersonEmail());
		obj.setContactPersonPhone(purchaseorder.getContactPersonPhone());
		obj.setContactPersonName(purchaseorder.getContactPersonName());
		if (purchaseorder.getContactPerson() != null) {
			obj.setContactPerson(purchaseorder.getContactPerson() + "-" + purchaseorder.getContactPersonName());
		}
		obj.setAddressLine1(purchaseorder.getAddressLine1());
		obj.setAddressLine2(purchaseorder.getAddressLine2());
		obj.setAddressLine3(purchaseorder.getAddressLine3());
		obj.setPostCode(purchaseorder.getPostCode());
		obj.setPostArea(purchaseorder.getPostArea());
		obj.setVendorCode(purchaseorder.getVendorcode() + "-" + purchaseorder.getVendorName() + "-"
				+ purchaseorder.getVendorbranchcode());
		obj.setVendorPersonInCharge(purchaseorder.getVendorPersonInCharge());
		obj.setVendorPersonInChargeEmail(purchaseorder.getVendorPersonInChargeEmail());
		obj.setVendorPersonInChargePhone(purchaseorder.getVendorPersonInChargePhone());
		obj.setVendorAddressLine1(purchaseorder.getVendorAddressLine1());
		obj.setVendorAddressLine2(purchaseorder.getVendorAddressLine2());
		obj.setVendorAddressLine3(purchaseorder.getVendorAddressLine3());
		obj.setVendorPostCode(purchaseorder.getPostCode());
		obj.setVendorPostArea(purchaseorder.getVendorPostArea());
		obj.setPoNo(purchaseorder.getPoNo());
		String pcName = profitCenterService.getProfitCenterByCode(purchaseorder.getPcCode()).getName();
		String stcode = profitCenterService.getProfitCenterByCode(purchaseorder.getPcCode()).getStore().getCode();
		String deptcode = profitCenterService.getProfitCenterByCode(purchaseorder.getPcCode()).getDepartment()
				.getCode();
		obj.setProfitcenter(purchaseorder.getPcCode() + "-" + pcName);
		obj.setStoreName(stcode + "-" + purchaseorder.getStoreName());
		obj.setDepartmentName(deptcode + "-" + purchaseorder.getDepartmentName());
		obj.setStatus(purchaseorder.getStatus());
		obj.setCompCode(purchaseorder.getCompCode());

		if (purchaseorder.getCreditTermsCode() != null) {

			CreditTermsId pkey = new CreditTermsId(purchaseorder.getCreditTermsCode(), getCompany().get());
			CreditTerms ad = crtService.getCreditTermsbyCrtId(pkey);
			obj.setCredittermscode(purchaseorder.getCreditTermsCode() + "-" + ad.getDescription());
			obj.setLastcrt_used_hidden(purchaseorder.getCreditTermsCode() + "-" + ad.getDescription());
			crt_edit = purchaseorder.getCreditTermsCode();
		}

		obj.setCurrency(purchaseorder.getCurrency());
		obj.setOrdCatCode(purchaseorder.getOrderCatCode());
		obj.setOrdSubCCode(purchaseorder.getOrdSubCatCode());
		obj.setPrTypeCode(purchaseorder.getPrTypeCode());
		obj.setQuoteNo(purchaseorder.getQuoteNo());
		if (purchaseorder.getQuoteDate() != null) {
			obj.setQuoteDate(formatter.format(purchaseorder.getQuoteDate()));
		}

		obj.setProjectCode(purchaseorder.getProjectCode());
		obj.setPaymentTerms(purchaseorder.getPaymentTerms());
		obj.setDeliveryInstruction(purchaseorder.getDeliveryInstruction());
		obj.setIssuerRemark(purchaseorder.getIssuerRemark());

		if (purchaseorder.getOrderCategoryAuthorizationGrid() != null) {
			obj.setOrderCategoryAuthorizationGrid(purchaseorder.getOrderCategoryAuthorizationGrid());
			orderAuthGrid = purchaseorder.getOrderCategoryAuthorizationGrid();

			authDir = sysRepo.getSystemParameterValueByPropName(AUTHGRID_UPLOAD_PATH);
			path_edit = UriUtils.encodePath(authDir + orderAuthGrid, "UTF-8");
			newPath = UriUtils.encodePath(authDir + orderAuthGrid, "UTF-8");

		}

		obj.setVendorbranchCode(purchaseorder.getVendorbranchcode());
		obj.setVendorName(purchaseorder.getVendorName());

		if (purchaseorder.getIssuer() != null) {
			PurchaseOrderUser po_user_obj = poUserService.getPurchaseOrderUserById(purchaseorder.getIssuer());
			if (po_user_obj != null) {
				EmployeeMaster emp_obj = employeeService.getEmployeeMasterByPsId(po_user_obj.getId());
				if (emp_obj != null) {
					obj.setIssuer(purchaseorder.getIssuer() + "-" + emp_obj.getAltId() + "-" + po_user_obj.getEmpName()
							+ "-" + emp_obj.getProfitCenter().getCode());
				}
			}
		}

		if (purchaseorder.getVerifier() != null) {
			PurchaseOrderUser po_user_obj = poUserService.getPurchaseOrderUserById(purchaseorder.getVerifier());
			if (po_user_obj != null) {
				EmployeeMaster emp_obj = employeeService.getEmployeeMasterByPsId(po_user_obj.getId());
				if (emp_obj != null) {
					obj.setVerifier(purchaseorder.getVerifier() + "-" + emp_obj.getAltId() + "-"
							+ po_user_obj.getEmpName() + "-" + emp_obj.getProfitCenter().getCode());
				}
			}
		}

		if (purchaseorder.getAuthorizer() != null) {
			PurchaseOrderUser po_user_obj = poUserService.getPurchaseOrderUserById(purchaseorder.getAuthorizer());
			if (po_user_obj != null) {
				EmployeeMaster emp_obj = employeeService.getEmployeeMasterByPsId(po_user_obj.getId());
				if (emp_obj != null) {
					obj.setAuthorizer(purchaseorder.getAuthorizer() + "-" + emp_obj.getAltId() + "-"
							+ po_user_obj.getEmpName() + "-" + emp_obj.getProfitCenter().getCode());
				}
			}
		}
		obj.setFirstApprover(purchaseorder.getFirstApprover());
		obj.setSecondApprover(purchaseorder.getSecondApprover());
		obj.setThirdApprover(purchaseorder.getThirdApprover());
		obj.setFirstApproverDesignation(purchaseorder.getFirstApproverDesignation());
		obj.setSecondApproverDesignation(purchaseorder.getSecondApproverDesignation());
		obj.setThirdApproverDesignation(purchaseorder.getThirdApproverDesignation());

		if (purchaseorder.getIssuedOn() != null) {
			obj.setIssuedOn(formatter_1.format(purchaseorder.getIssuedOn()));
		}
		if (purchaseorder.getVerifiedOn() != null) {
			obj.setVerifiedOn(formatter_1.format(purchaseorder.getVerifiedOn()));
		}
		if (purchaseorder.getAuthorizedOn() != null) {
			obj.setAuthorizedOn(formatter_1.format(purchaseorder.getAuthorizedOn()));
		}

		if (purchaseorder.getRejectedOn() != null) {
			obj.setRejectedOn(formatter_1.format(purchaseorder.getRejectedOn()));
		}
		if (purchaseorder.getDeletedOn() != null) {
			obj.setDeletedOn(formatter_1.format(purchaseorder.getDeletedOn()));
		}

		if (purchaseorder.getCancelledOn() != null) {
			obj.setCancelledOn(formatter_1.format(purchaseorder.getCancelledOn()));
		}

		if (purchaseorder.getDeletedBy() != null) {
			obj.setDeletedBy(purchaseorder.getDeletedBy());

		}

		if (purchaseorder.getRejectedBy() != null) {

			obj.setRejectedBy(purchaseorder.getRejectedBy());
		}

		if (purchaseorder.getClosedBy() != null) {
			obj.setClosedBy(purchaseorder.getClosedBy());
		}

		if (purchaseorder.getCancelledBy() != null) {
			obj.setCancelledBy(purchaseorder.getCancelledBy());
		}

		obj.setIssuerRemark(purchaseorder.getIssuerRemark());
		obj.setVerifyRemark(purchaseorder.getVerifyRemark());
		obj.setAuthorizeRemark(purchaseorder.getAuthorizeRemark());
		obj.setRedraftRemark(purchaseorder.getRedraftRemark());
		obj.setRejectRemark(purchaseorder.getRejectRemark());
		obj.setCancelRemark(purchaseorder.getCancelRemark());
		obj.setDeleteRemark(purchaseorder.getDeleteRemark());
		obj.setCloseException(purchaseorder.getCloseException());

		obj.setTotExclGST(purchaseorder.getTotalExclGST());
		obj.setTotInclGST(purchaseorder.getTotalInclGST());
		obj.setTotGST(purchaseorder.getTotalGST());
		obj.setOverAllDiscount(purchaseorder.getOverAllDiscount());
		obj.setOverAllDiscountPercent(purchaseorder.getOveralldiscountpercent());
		return obj;
	}

	@PostMapping(value = "/main/issued_pocreatesave")
	public String edit_issued_po(@Valid @ModelAttribute("pomainobj") PoMainForm obj, BindingResult result, Model model,
			RedirectAttributes redirectAttributes) throws ParseException, UnsupportedEncodingException {
		MDC.put("sessionId",
				AbstractMainController.getCompany().isPresent() ? AbstractMainController.getCompany().get() : "");
		if (!result.hasErrors() && obj.getAuthorizer() != null) {

			PurchaseOrder issued_purchaseorder = poMainService.getPurchaseOrderByPoNo(obj.getPoNo());
			PurchaseOrderUserAccessPk pkey_current = new PurchaseOrderUserAccessPk(issued_purchaseorder.getPoNo(),
					issued_purchaseorder.getAuthorizer(), "A");

			if (obj.getActivetaboperation().equals("save")) {

				issued_purchaseorder.setAuthorizer(extract_issuer(obj.getAuthorizer()));
				issued_purchaseorder.setFirstApprover(obj.getFirstApprover());
				issued_purchaseorder.setSecondApprover(obj.getSecondApprover());
				issued_purchaseorder.setThirdApprover(obj.getThirdApprover());

				if (obj.getFirstApproverDesignation() != null) {

					issued_purchaseorder.setFirstApproverDesignation(obj.getFirstApproverDesignation());
				}

				if (obj.getSecondApproverDesignation() != null) {

					issued_purchaseorder.setSecondApproverDesignation(obj.getSecondApproverDesignation());
				}

				if (obj.getThirdApproverDesignation() != null) {

					issued_purchaseorder.setThirdApproverDesignation(obj.getThirdApproverDesignation());
				}

				issued_purchaseorder.setLastModifiedOn(new Date());
				try {
					poMainService.createPurchaseOrder(issued_purchaseorder);
					auditService.createAudit(new Date(),
							" Issued Purchase Order: " + obj.getPoNo() + " Updated successfully",
							getCurrentUser().get(), " Updated Issued Purchase Order", getCompany().get(),
							getProfitCenterCode().getCode());
					redirectAttributes.addFlashAttribute("info", "Purchase Order Saved successfully");

				} catch (Exception e) {
					LOGGER.error(e);
				}

				/*
				 * in Issued status if user change the authorizer , need to get updated in
				 * access table
				 */
				PurchaseOrderUserAccess current_authorizer_poaccess = poUserAccessService
						.getPurchaseOrderUserAccessByPK(pkey_current);

				if (current_authorizer_poaccess != null) {
					poaccessrepo.delete(current_authorizer_poaccess);
					PurchaseOrderUserAccessPk pkey_new = new PurchaseOrderUserAccessPk(issued_purchaseorder.getPoNo(),
							extract_issuer(obj.getAuthorizer()), "A");
					current_authorizer_poaccess.setPurchaseorderuseraccessId(pkey_new);
					try {

						poUserAccessService.createPurchaseOrderUserAccess(current_authorizer_poaccess);
					} catch (Exception e) {
						LOGGER.error(e);
					}
				}

				return "redirect:/main/verifypo";
			}

			if (obj.getActivetaboperation().equals("submitandverify")) {
				issued_purchaseorder = poMainService.getPurchaseOrderByPoNo(obj.getPoNo());

				/* issued_purchaseorder.setAuthorizer(obj.getAuthorizer()); */
				issued_purchaseorder.setFirstApprover(obj.getFirstApprover());
				issued_purchaseorder.setSecondApprover(obj.getSecondApprover());
				issued_purchaseorder.setThirdApprover(obj.getThirdApprover());

				if (obj.getFirstApproverDesignation() != null) {

					issued_purchaseorder.setFirstApproverDesignation(obj.getFirstApproverDesignation());
				}

				if (obj.getSecondApproverDesignation() != null) {

					issued_purchaseorder.setSecondApproverDesignation(obj.getSecondApproverDesignation());
				}

				if (obj.getThirdApproverDesignation() != null) {

					issued_purchaseorder.setThirdApproverDesignation(obj.getThirdApproverDesignation());
				}
				verifyPurchaseOrder(issued_purchaseorder, obj, redirectAttributes);
				return "redirect:/main/mypos";

			}

			if (obj.getActivetaboperation().equals("redraft")) {
				issued_purchaseorder = poMainService.getPurchaseOrderByPoNo(obj.getPoNo());
				/* issued_purchaseorder.setAuthorizer(obj.getAuthorizer()); */
				issued_purchaseorder.setFirstApprover(obj.getFirstApprover());
				issued_purchaseorder.setSecondApprover(obj.getSecondApprover());
				issued_purchaseorder.setThirdApprover(obj.getThirdApprover());

				if (obj.getFirstApproverDesignation() != null) {

					issued_purchaseorder.setFirstApproverDesignation(obj.getFirstApproverDesignation());
				}

				if (obj.getSecondApproverDesignation() != null) {

					issued_purchaseorder.setSecondApproverDesignation(obj.getSecondApproverDesignation());
				}

				if (obj.getThirdApproverDesignation() != null) {

					issued_purchaseorder.setThirdApproverDesignation(obj.getThirdApproverDesignation());
				}
				issued_purchaseorder.setLastModifiedOn(new Date());
				redraft(issued_purchaseorder, obj, redirectAttributes);
				return "redirect:/main/redraftpo";

			}

			if (obj.getActivetaboperation().equals("delete")) {
				issued_purchaseorder = poMainService.getPurchaseOrderByPoNo(obj.getPoNo());
				issued_purchaseorder.setAuthorizer(extract_issuer(obj.getAuthorizer()));
				issued_purchaseorder.setFirstApprover(obj.getFirstApprover());
				issued_purchaseorder.setSecondApprover(obj.getSecondApprover());
				issued_purchaseorder.setThirdApprover(obj.getThirdApprover());

				if (obj.getFirstApproverDesignation() != null) {

					issued_purchaseorder.setFirstApproverDesignation(obj.getFirstApproverDesignation());
				}

				if (obj.getSecondApproverDesignation() != null) {

					issued_purchaseorder.setSecondApproverDesignation(obj.getSecondApproverDesignation());
				}

				if (obj.getThirdApproverDesignation() != null) {

					issued_purchaseorder.setThirdApproverDesignation(obj.getThirdApproverDesignation());
				}

				issued_purchaseorder.setLastModifiedOn(new Date());
				delete(issued_purchaseorder, obj, redirectAttributes);
				return "redirect:/main/mypos";

			}

			if (obj.getActivetaboperation().equals("reject")) {
				issued_purchaseorder = poMainService.getPurchaseOrderByPoNo(obj.getPoNo());
				issued_purchaseorder.setAuthorizer(extract_issuer(obj.getAuthorizer()));
				issued_purchaseorder.setFirstApprover(obj.getFirstApprover());
				issued_purchaseorder.setSecondApprover(obj.getSecondApprover());
				issued_purchaseorder.setThirdApprover(obj.getThirdApprover());

				if (obj.getFirstApproverDesignation() != null) {

					issued_purchaseorder.setFirstApproverDesignation(obj.getFirstApproverDesignation());
				}

				if (obj.getSecondApproverDesignation() != null) {

					issued_purchaseorder.setSecondApproverDesignation(obj.getSecondApproverDesignation());
				}

				if (obj.getThirdApproverDesignation() != null) {

					issued_purchaseorder.setThirdApproverDesignation(obj.getThirdApproverDesignation());
				}

				issued_purchaseorder.setLastModifiedOn(new Date());
				reject(issued_purchaseorder, obj, redirectAttributes);
				return "redirect:/main/mypos";

			}

			if (obj.getActivetaboperation().equals("duplicate")) {
				issued_purchaseorder = poMainService.getPurchaseOrderByPoNo(obj.getPoNo());
				issued_purchaseorder.setAuthorizer(extract_issuer(obj.getAuthorizer()));
				issued_purchaseorder.setFirstApprover(obj.getFirstApprover());
				issued_purchaseorder.setSecondApprover(obj.getSecondApprover());
				issued_purchaseorder.setThirdApprover(obj.getThirdApprover());

				if (obj.getFirstApproverDesignation() != null) {

					issued_purchaseorder.setFirstApproverDesignation(obj.getFirstApproverDesignation());
				}

				if (obj.getSecondApproverDesignation() != null) {

					issued_purchaseorder.setSecondApproverDesignation(obj.getSecondApproverDesignation());
				}

				if (obj.getThirdApproverDesignation() != null) {

					issued_purchaseorder.setThirdApproverDesignation(obj.getThirdApproverDesignation());
				}
				issued_purchaseorder.setLastModifiedOn(new Date());
				duplicate(issued_purchaseorder, obj, redirectAttributes);
				return "redirect:/main/mypos";

			}

			if (obj.getActivetaboperation().equals("forward_to_invoicer") && obj.getInvoicer() != null) {
				issued_purchaseorder = poMainService.getPurchaseOrderByPoNo(obj.getPoNo());
				issued_purchaseorder.setAuthorizer(extract_issuer(obj.getAuthorizer()));
				issued_purchaseorder.setFirstApprover(obj.getFirstApprover());
				issued_purchaseorder.setSecondApprover(obj.getSecondApprover());
				issued_purchaseorder.setThirdApprover(obj.getThirdApprover());

				if (obj.getFirstApproverDesignation() != null) {

					issued_purchaseorder.setFirstApproverDesignation(obj.getFirstApproverDesignation());
				}

				if (obj.getSecondApproverDesignation() != null) {

					issued_purchaseorder.setSecondApproverDesignation(obj.getSecondApproverDesignation());
				}

				if (obj.getThirdApproverDesignation() != null) {

					issued_purchaseorder.setThirdApproverDesignation(obj.getThirdApproverDesignation());
				}

				issued_purchaseorder.setLastModifiedOn(new Date());
				forward_to_invoicer(issued_purchaseorder, obj, redirectAttributes);
				redirectAttributes.addFlashAttribute("info", "Purchase Order Forwarded To Invoicer successfully");
				return "redirect:/main/mypos";

			}

			if (obj.getActivetaboperation().equals("forward_to_verifier") && obj.getForward_verifier() != null) {
				issued_purchaseorder = poMainService.getPurchaseOrderByPoNo(obj.getPoNo());

				/* issued_purchaseorder.setAuthorizer(obj.getAuthorizer()); */
				issued_purchaseorder.setFirstApprover(obj.getFirstApprover());
				issued_purchaseorder.setSecondApprover(obj.getSecondApprover());
				issued_purchaseorder.setThirdApprover(obj.getThirdApprover());

				if (obj.getFirstApproverDesignation() != null) {

					issued_purchaseorder.setFirstApproverDesignation(obj.getFirstApproverDesignation());
				}

				if (obj.getSecondApproverDesignation() != null) {

					issued_purchaseorder.setSecondApproverDesignation(obj.getSecondApproverDesignation());
				}

				if (obj.getThirdApproverDesignation() != null) {

					issued_purchaseorder.setThirdApproverDesignation(obj.getThirdApproverDesignation());
				}

				issued_purchaseorder.setLastModifiedOn(new Date());
				forward_to_verifier(issued_purchaseorder, obj, redirectAttributes);
				redirectAttributes.addFlashAttribute("info", "Purchase Order Forwarded To Verifier successfully");
				return "redirect:/main/mypos";

			}

		} else {

			redirectAttributes.addFlashAttribute("error", "Error happening during Editing Issued PO");
			tempobj = obj;
		}
		return "redirect:/main/edit_issuedpo";
	}

	@PostMapping(value = "/main/verified_pocreatesave")
	public String edit_verified_po(@Valid @ModelAttribute("pomainobj") PoMainForm obj, BindingResult result,
			Model model, RedirectAttributes redirectAttributes) throws ParseException, UnsupportedEncodingException {
		MDC.put("sessionId",
				AbstractMainController.getCompany().isPresent() ? AbstractMainController.getCompany().get() : "");
		if (!result.hasErrors()) {
			// get the current authorizor
			PurchaseOrderUserAccessPk pkey_current = new PurchaseOrderUserAccessPk(obj.getPoNo(), obj.getAuthorizer(),
					"A");
			PurchaseOrder verified_purchaseorder = poMainService.getPurchaseOrderByPoNo(obj.getPoNo());
			verified_purchaseorder.setFirstApprover(obj.getFirstApprover());
			verified_purchaseorder.setSecondApprover(obj.getSecondApprover());
			verified_purchaseorder.setThirdApprover(obj.getThirdApprover());

			if (obj.getFirstApproverDesignation() != null) {

				verified_purchaseorder.setFirstApproverDesignation(obj.getFirstApproverDesignation());
			}

			if (obj.getSecondApproverDesignation() != null) {

				verified_purchaseorder.setSecondApproverDesignation(obj.getSecondApproverDesignation());
			}

			if (obj.getThirdApproverDesignation() != null) {

				verified_purchaseorder.setThirdApproverDesignation(obj.getThirdApproverDesignation());
			}

			if (obj.getActivetaboperation().equals("forward_to_authorizer")) {
				if (obj.getForward_authorizer() != null) {
					String[] forward_auth_array = obj.getForward_authorizer().split("-");

					// set new authorizor to po main table
					verified_purchaseorder.setAuthorizer(forward_auth_array[0]);
					verified_purchaseorder.setLastModifiedOn(new Date());
					try {

						// update po table with new authorizor
						poMainService.createPurchaseOrder(verified_purchaseorder);
						PurchaseOrderSendMailUtil email = new PurchaseOrderSendMailUtil(
								verified_purchaseorder.getPoNo(), getCompany().get());
						email.forwardToAuthorizerPurchaseOrder();
						redirectAttributes.addFlashAttribute("info", "Purchase Order Forwarded successfully");

					} catch (Exception e) {
						LOGGER.error(e);
					}

					// create record with new auth in user access
					PurchaseOrderUserAccessPk pkey_new = new PurchaseOrderUserAccessPk(obj.getPoNo(),
							forward_auth_array[0], "A");

					/*
					 * in Verified status if user change the authorizer , need to get updated in
					 * access table
					 */
					PurchaseOrderUserAccess current_authorizer_poaccess = poUserAccessService
							.getPurchaseOrderUserAccessByPK(pkey_current);

					if (current_authorizer_poaccess != null) {
						poaccessrepo.delete(current_authorizer_poaccess);

						// update user access table
						current_authorizer_poaccess.setPurchaseorderuseraccessId(pkey_new);
						try {

							poUserAccessService.createPurchaseOrderUserAccess(current_authorizer_poaccess);
						} catch (Exception e) {
							LOGGER.error(e);
						}
					}
				}

				return "redirect:/main/mypos";
			}

			if (obj.getActivetaboperation().equals("forward_to_invoicer")
					&& obj.getForward_verifiedpo_invoicer() != null) {

				verified_purchaseorder = poMainService.getPurchaseOrderByPoNo(obj.getPoNo());
				verified_purchaseorder.setFirstApprover(obj.getFirstApprover());
				verified_purchaseorder.setSecondApprover(obj.getSecondApprover());
				verified_purchaseorder.setThirdApprover(obj.getThirdApprover());

				if (obj.getFirstApproverDesignation() != null) {

					verified_purchaseorder.setFirstApproverDesignation(obj.getFirstApproverDesignation());
				}

				if (obj.getSecondApproverDesignation() != null) {

					verified_purchaseorder.setSecondApproverDesignation(obj.getSecondApproverDesignation());
				}

				if (obj.getThirdApproverDesignation() != null) {

					verified_purchaseorder.setThirdApproverDesignation(obj.getThirdApproverDesignation());
				}
				verified_purchaseorder.setLastModifiedOn(new Date());
				forward_verifiedpo_invoicer(verified_purchaseorder, obj, redirectAttributes);
				redirectAttributes.addFlashAttribute("info", "Purchase Order Forwarded To Invoicer successfully");
				return "redirect:/main/mypos";

			}

			if (obj.getActivetaboperation().equals("delete")) {
				verified_purchaseorder = poMainService.getPurchaseOrderByPoNo(obj.getPoNo());
				verified_purchaseorder.setFirstApprover(obj.getFirstApprover());
				verified_purchaseorder.setSecondApprover(obj.getSecondApprover());
				verified_purchaseorder.setThirdApprover(obj.getThirdApprover());

				if (obj.getFirstApproverDesignation() != null) {

					verified_purchaseorder.setFirstApproverDesignation(obj.getFirstApproverDesignation());
				}

				if (obj.getSecondApproverDesignation() != null) {

					verified_purchaseorder.setSecondApproverDesignation(obj.getSecondApproverDesignation());
				}

				if (obj.getThirdApproverDesignation() != null) {

					verified_purchaseorder.setThirdApproverDesignation(obj.getThirdApproverDesignation());
				}
				verified_purchaseorder.setLastModifiedOn(new Date());
				delete(verified_purchaseorder, obj, redirectAttributes);
				return "redirect:/main/mypos";

			}

			if (obj.getActivetaboperation().equals("reject")) {

				verified_purchaseorder = poMainService.getPurchaseOrderByPoNo(obj.getPoNo());
				verified_purchaseorder.setFirstApprover(obj.getFirstApprover());
				verified_purchaseorder.setSecondApprover(obj.getSecondApprover());
				verified_purchaseorder.setThirdApprover(obj.getThirdApprover());

				if (obj.getFirstApproverDesignation() != null) {

					verified_purchaseorder.setFirstApproverDesignation(obj.getFirstApproverDesignation());
				}

				if (obj.getSecondApproverDesignation() != null) {

					verified_purchaseorder.setSecondApproverDesignation(obj.getSecondApproverDesignation());
				}

				if (obj.getThirdApproverDesignation() != null) {

					verified_purchaseorder.setThirdApproverDesignation(obj.getThirdApproverDesignation());
				}
				verified_purchaseorder.setLastModifiedOn(new Date());
				reject(verified_purchaseorder, obj, redirectAttributes);
				redirectAttributes.addFlashAttribute("info", "Purchase Order Rejected successfully");
				return "redirect:/main/mypos";

			}

			if (obj.getActivetaboperation().equals("reopen")) {

				reopen(verified_purchaseorder, obj, redirectAttributes);
				redirectAttributes.addFlashAttribute("info", "Purchase Order Re-Opened successfully");
				return "redirect:/main/mypos";

			}

			if (obj.getActivetaboperation().equals("redraft")) {

				verified_purchaseorder = poMainService.getPurchaseOrderByPoNo(obj.getPoNo());

				verified_purchaseorder.setFirstApprover(obj.getFirstApprover());
				verified_purchaseorder.setSecondApprover(obj.getSecondApprover());
				verified_purchaseorder.setThirdApprover(obj.getThirdApprover());

				if (obj.getFirstApproverDesignation() != null) {

					verified_purchaseorder.setFirstApproverDesignation(obj.getFirstApproverDesignation());
				}

				if (obj.getSecondApproverDesignation() != null) {

					verified_purchaseorder.setSecondApproverDesignation(obj.getSecondApproverDesignation());
				}

				if (obj.getThirdApproverDesignation() != null) {

					verified_purchaseorder.setThirdApproverDesignation(obj.getThirdApproverDesignation());
				}

				verified_purchaseorder.setLastModifiedOn(new Date());
				redraft(verified_purchaseorder, obj, redirectAttributes);
				redirectAttributes.addFlashAttribute("info", "Purchase Order Re-Drafted successfully");
				return "redirect:/main/mypos";

			}

			if (obj.getActivetaboperation().equals("authorize")) {
				verified_purchaseorder = poMainService.getPurchaseOrderByPoNo(obj.getPoNo());

				verified_purchaseorder.setFirstApprover(obj.getFirstApprover());
				verified_purchaseorder.setSecondApprover(obj.getSecondApprover());
				verified_purchaseorder.setThirdApprover(obj.getThirdApprover());

				if (obj.getFirstApproverDesignation() != null) {

					verified_purchaseorder.setFirstApproverDesignation(obj.getFirstApproverDesignation());
				}

				if (obj.getSecondApproverDesignation() != null) {

					verified_purchaseorder.setSecondApproverDesignation(obj.getSecondApproverDesignation());
				}

				if (obj.getThirdApproverDesignation() != null) {

					verified_purchaseorder.setThirdApproverDesignation(obj.getThirdApproverDesignation());
				}
				verified_purchaseorder.setLastModifiedOn(new Date());
				authorizePurchaseOrder(verified_purchaseorder, obj, redirectAttributes);
				redirectAttributes.addFlashAttribute("info", "Purchase Order Authorized successfully");
				return "redirect:/main/mypos";

			}

			if (obj.getActivetaboperation().equals("duplicate")) {

				verified_purchaseorder = poMainService.getPurchaseOrderByPoNo(obj.getPoNo());

				verified_purchaseorder.setFirstApprover(obj.getFirstApprover());
				verified_purchaseorder.setSecondApprover(obj.getSecondApprover());
				verified_purchaseorder.setThirdApprover(obj.getThirdApprover());

				if (obj.getFirstApproverDesignation() != null) {

					verified_purchaseorder.setFirstApproverDesignation(obj.getFirstApproverDesignation());
				}

				if (obj.getSecondApproverDesignation() != null) {

					verified_purchaseorder.setSecondApproverDesignation(obj.getSecondApproverDesignation());
				}

				if (obj.getThirdApproverDesignation() != null) {

					verified_purchaseorder.setThirdApproverDesignation(obj.getThirdApproverDesignation());
				}
				verified_purchaseorder.setLastModifiedOn(new Date());
				duplicate(verified_purchaseorder, obj, redirectAttributes);
				redirectAttributes.addFlashAttribute("info", "Purchase Order Duplicated successfully");
				return "redirect:/main/mypos";

			}

		} else {

			redirectAttributes.addFlashAttribute("error", "Error happening during Editing Verified PO");
			tempobj = obj;
		}
		return "redirect:/main/edit_verifiedpo";
	}

	@PostMapping(value = "/main/authorized_pocreatesave")
	public String edit_authorized_po(@Valid @ModelAttribute("pomainobj") PoMainForm obj, BindingResult result,
			Model model, RedirectAttributes redirectAttributes) throws ParseException, UnsupportedEncodingException {
		MDC.put("sessionId",
				AbstractMainController.getCompany().isPresent() ? AbstractMainController.getCompany().get() : "");
		if (!result.hasErrors()) {

			PurchaseOrderUserAccessPk pkey_current = new PurchaseOrderUserAccessPk(obj.getPoNo(),
					extract_issuer(obj.getAuthorizer()), "A");
			PurchaseOrder purchaseorder_authorized = poMainService.getPurchaseOrderByPoNo(obj.getPoNo());

			if (obj.getActivetaboperation().equals("forward_to_invoicer")) {

				if (obj.getForward_verifiedpo_invoicer() != null) {
					forward_verifiedpo_invoicer(purchaseorder_authorized, obj, redirectAttributes);
					redirectAttributes.addFlashAttribute("info", "Purchase Order Forwarded successfully");
				}
				return "redirect:/main/mypos";

			}

			if (obj.getActivetaboperation().equals("delete")) {

				delete(purchaseorder_authorized, obj, redirectAttributes);
				return "redirect:/main/mypos";

			}

			if (obj.getActivetaboperation().equals("invoice")) {

				return "redirect:/main/view_Invoices/" + obj.getPoNo();

			}

			if (obj.getActivetaboperation().equals("cancel")) {

				cancel(purchaseorder_authorized, obj, redirectAttributes);
				redirectAttributes.addFlashAttribute("info", "Purchase Order Cancelled successfully");
				return "redirect:/main/mypos";

			}

			if (obj.getActivetaboperation().equals("duplicate")) {

				duplicate(purchaseorder_authorized, obj, redirectAttributes);
				redirectAttributes.addFlashAttribute("info", "Purchase Order Duplicated successfully");
				return "redirect:/main/mypos";

			}

		} else {

			redirectAttributes.addFlashAttribute("error", "Error happening during Editing Authorized PO");
			tempobj_authorized = obj;
		}
		return "redirect:/main/edit_authorizedpo";
	}

	@PostMapping(value = "/main/deleted_rejected_cancelled_pocreatesave")
	public String edit_deleted_rejected_cancelled_po(@Valid @ModelAttribute("pomainobj") PoMainForm obj,
			BindingResult result, Model model, RedirectAttributes redirectAttributes)
			throws ParseException, UnsupportedEncodingException {
		MDC.put("sessionId",
				AbstractMainController.getCompany().isPresent() ? AbstractMainController.getCompany().get() : "");
		if (!result.hasErrors()) {

			PurchaseOrder dead_purchaseorder = poMainService.getPurchaseOrderByPoNo(obj.getPoNo());
			if (obj.getActivetaboperation().equals("duplicate")) {

				duplicate(dead_purchaseorder, obj, redirectAttributes);
				redirectAttributes.addFlashAttribute("info", "Purchase Order Duplicated successfully");
				return "redirect:/main/mypos";

			}

		} else {

			redirectAttributes.addFlashAttribute("error", "Error happening during Editing Authorized PO");
			tempobj_authorized = obj;
		}
		return "redirect:/main/mypos";
	}

	@GetMapping(value = "/main/edit_draft_po")
	public String draftpo(@RequestParam String poNo, @RequestParam String status, Model model,
			RedirectAttributes redirectAttributes) throws UnsupportedEncodingException, ParseException {
		MDC.put("sessionId",
				AbstractMainController.getCompany().isPresent() ? AbstractMainController.getCompany().get() : "");
		PurchaseOrder purchaseorder_draft = poMainService.getPurchaseOrderByPoNo(poNo);
		if (purchaseorder_draft != null) {
			tempobj_draft = entityToPojo(purchaseorder_draft);

			try {
				HashMap<String, Boolean> temp = check_profitcenter_masterdata(purchaseorder_draft, redirectAttributes);
				if (temp.containsKey("NO_StoreName") && temp.get("NO_StoreName").booleanValue()) {
					redirectAttributes.addFlashAttribute("error", "No Store Name Found For Selected PO");
					return "redirect:/main/mypos";
				}

				if (temp.containsKey("NO_DeptName") && temp.get("NO_DeptName").booleanValue()) {
					redirectAttributes.addFlashAttribute("error", "No Department Name Found For Selected PO");
					return "redirect:/main/mypos";
				}
				if (temp.size() == 0) {
					redirectAttributes.addFlashAttribute("error", "No PC Found For This Company");
					return "redirect:/main/draftpo";
				}
			} catch (Exception e) {
				LOGGER.error(e);
			}
			HashMap<String, Boolean> temp_vend = check_vendor_masterdata(purchaseorder_draft);

			if (temp_vend.size() == 0) {

				redirectAttributes.addFlashAttribute("error", "No Vendor Found For This Company");
				return "redirect:/main/draftpo";
			}

			return "redirect:/main/edit_draftpo";
		} else {
			return "redirect:/main/draftpo";
		}
	}

	@GetMapping(value = "/main/edit_issued_po")
	public String button_viewissuedpo(@RequestParam String poNo, @RequestParam String status, Model model,
			RedirectAttributes redirectAttributes) throws UnsupportedEncodingException, ParseException {
		MDC.put("sessionId",
				AbstractMainController.getCompany().isPresent() ? AbstractMainController.getCompany().get() : "");
		PurchaseOrder purchaseorder_issued = poMainService.getPurchaseOrderByPoNo(poNo);

		if (purchaseorder_issued != null) {

			tempobj_issued = entityToPojo(purchaseorder_issued);

			if (purchaseorder_issued.getPcCode() != null) {

				ProfitCenter pc_obj = profitCenterService.getProfitCenterByCode(purchaseorder_issued.getPcCode());
				if (!pc_obj.isIsactive()) {

					inactive_pc = true;
				}
			}

			try {
				HashMap<String, Boolean> temp = check_profitcenter_masterdata(purchaseorder_issued, redirectAttributes);
				if (temp.containsKey("NO_StoreName") && temp.get("NO_StoreName").booleanValue()) {
					redirectAttributes.addFlashAttribute("error", "No Store Name Found For Selected PO");
					return "redirect:/main/verifypo";
				}

				if (temp.containsKey("NO_DeptName") && temp.get("NO_DeptName").booleanValue()) {
					redirectAttributes.addFlashAttribute("error", "No Department Name Found For Selected PO");
					return "redirect:/main/verifypo";
				}
				if (temp.size() == 0) {
					redirectAttributes.addFlashAttribute("error", "No PC Found For This Company");
					return "redirect:/main/verifypo";
				}
			} catch (Exception e) {

				LOGGER.error(e);
				return "redirect:/main/verifypo";
			}

			try {
				HashMap<String, Boolean> temp_vend = check_vendor_masterdata(purchaseorder_issued);

				if (temp_vend.size() == 0) {

					redirectAttributes.addFlashAttribute("error", "No Vendor Found For This Company");
					return "redirect:/main/verifypo";
				}
			} catch (Exception e) {
				LOGGER.error(e);
				return "redirect:/main/mypos";
			}

			return "redirect:/main/edit_issuedpo";

		} else {
			return "redirect:/main/verifypo";
		}
	}

	@GetMapping(value = "/main/edit_verified_po")
	public String verifiedpo(@RequestParam String poNo, @RequestParam String status, Model model,
			RedirectAttributes redirectAttributes) throws UnsupportedEncodingException, ParseException {
		MDC.put("sessionId",
				AbstractMainController.getCompany().isPresent() ? AbstractMainController.getCompany().get() : "");
		PurchaseOrder purchaseorder_verified = poMainService.getPurchaseOrderByPoNo(poNo);

		if (purchaseorder_verified != null) {

			tempobj_verified = entityToPojo(purchaseorder_verified);
			try {
				HashMap<String, Boolean> temp = check_profitcenter_masterdata(purchaseorder_verified,
						redirectAttributes);
				if (temp.containsKey("NO_StoreName") && temp.get("NO_StoreName").booleanValue()) {
					redirectAttributes.addFlashAttribute("error", "No Store Name Found For Selected PO");
					return "redirect:/main/authorizepo";
				}

				if (temp.containsKey("NO_DeptName") && temp.get("NO_DeptName").booleanValue()) {
					redirectAttributes.addFlashAttribute("error", "No Department Name Found For Selected PO");
					return "redirect:/main/authorizepo";
				}
				if (temp.size() == 0) {
					redirectAttributes.addFlashAttribute("error", "No PC Found For This Company");
					return "redirect:/main/authorizepo";
				}
			} catch (Exception e) {

				LOGGER.error(e);
				return "redirect:/main/authorizepo";
			}

			try {
				HashMap<String, Boolean> temp_vend = check_vendor_masterdata(purchaseorder_verified);

				if (temp_vend.size() == 0) {

					redirectAttributes.addFlashAttribute("error", "No Vendor Found For This Company");
					return "redirect:/main/authorizepo";
				}
			} catch (Exception e) {
				LOGGER.error(e);
				return "redirect:/main/authorizepo";
			}

			return "redirect:/main/edit_verifiedpo";

		} else {
			return "redirect:/main/authorizepo";
		}
	}

	@GetMapping(value = "/main/view_invoice")
	public String view_forwarded_invoice(@RequestParam String poNo, Model model)
			throws UnsupportedEncodingException, ParseException {
		MDC.put("sessionId",
				AbstractMainController.getCompany().isPresent() ? AbstractMainController.getCompany().get() : "");
		PurchaseOrder purchaseorder_invoiced = poMainService.getPurchaseOrderByPoNo(poNo);

		if (purchaseorder_invoiced != null) {

			tempobj = entityToPojo(purchaseorder_invoiced);

			return "redirect:/main/view_forwarded_invoice";

		} else {
			return "redirect:/main/invoicepo";
		}
	}

	@GetMapping(value = "/main/view_po")
	public String view_po(@RequestParam String poNo, @RequestParam String status, Model model,
			RedirectAttributes redirectAttributes) throws UnsupportedEncodingException, ParseException {
		MDC.put("sessionId",
				AbstractMainController.getCompany().isPresent() ? AbstractMainController.getCompany().get() : "");
		PurchaseOrder purchaseorder_view = poMainService.getPurchaseOrderByPoNo(poNo);
		
		getoldlist(poNo);

		if (purchaseorder_view != null) {

			if (status.equals("Draft")) {

				tempobj_draft = entityToPojo(purchaseorder_view);

				try {
					HashMap<String, Boolean> temp = check_profitcenter_masterdata(purchaseorder_view,
							redirectAttributes);
					if (temp.containsKey("NO_StoreName") && temp.get("NO_StoreName").booleanValue()) {
						redirectAttributes.addFlashAttribute("error", "No Store Name Found For Selected PO");
						return "redirect:/main/mypos";
					}

					if (temp.containsKey("NO_DeptName") && temp.get("NO_DeptName").booleanValue()) {
						redirectAttributes.addFlashAttribute("error", "No Department Name Found For Selected PO");
						return "redirect:/main/mypos";
					}
					if (temp.size() == 0) {
						redirectAttributes.addFlashAttribute("error", "No PC Found For This Company");
						return "redirect:/main/mypos";
					}
				} catch (Exception e) {

					LOGGER.error(e);
					return "redirect:/main/mypos";
				}

				try {
					HashMap<String, Boolean> temp_vend = check_vendor_masterdata(purchaseorder_view);

					if (temp_vend.size() == 0) {

						redirectAttributes.addFlashAttribute("error", "No Vendor Found For This Company");
						return "redirect:/main/mypos";
					}
				} catch (Exception e) {
					LOGGER.error(e);
					return "redirect:/main/mypos";
				}

				return "redirect:/main/edit_draftpo";

			} else if (status.equals("Issued")) {

				tempobj_issued = entityToPojo(purchaseorder_view);
				try {
					HashMap<String, Boolean> temp = check_profitcenter_masterdata(purchaseorder_view,
							redirectAttributes);
					if (temp.containsKey("NO_StoreName") && temp.get("NO_StoreName").booleanValue()) {
						redirectAttributes.addFlashAttribute("error", "No Store Name Found For Selected PO");
						return "redirect:/main/mypos";
					}

					if (temp.containsKey("NO_DeptName") && temp.get("NO_DeptName").booleanValue()) {
						redirectAttributes.addFlashAttribute("error", "No Department Name Found For Selected PO");
						return "redirect:/main/mypos";
					}
					if (temp.size() == 0) {
						redirectAttributes.addFlashAttribute("error", "No PC Found For This Company");
						return "redirect:/main/mypos";
					}
				} catch (Exception e) {

					LOGGER.error(e);
					return "redirect:/main/mypos";
				}

				try {
					HashMap<String, Boolean> temp_vend = check_vendor_masterdata(purchaseorder_view);

					if (temp_vend.size() == 0) {

						redirectAttributes.addFlashAttribute("error", "No Vendor Found For This Company");
						return "redirect:/main/mypos";
					}
				} catch (Exception e) {
					LOGGER.error(e);
					return "redirect:/main/mypos";
				}

				return "redirect:/main/edit_issuedpo";
			} else if (status.equals("Verified")) {

				tempobj_verified = entityToPojo(purchaseorder_view);
				try {
					HashMap<String, Boolean> temp = check_profitcenter_masterdata(purchaseorder_view,
							redirectAttributes);
					if (temp.containsKey("NO_StoreName") && temp.get("NO_StoreName").booleanValue()) {
						redirectAttributes.addFlashAttribute("error", "No Store Name Found For Selected PO");
						return "redirect:/main/mypos";
					}

					if (temp.containsKey("NO_DeptName") && temp.get("NO_DeptName").booleanValue()) {
						redirectAttributes.addFlashAttribute("error", "No Department Name Found For Selected PO");
						return "redirect:/main/mypos";
					}
					if (temp.size() == 0) {
						redirectAttributes.addFlashAttribute("error", "No PC Found For This Company");
						return "redirect:/main/mypos";
					}
				} catch (Exception e) {

					LOGGER.error(e);
					return "redirect:/main/mypos";
				}

				try {
					HashMap<String, Boolean> temp_vend = check_vendor_masterdata(purchaseorder_view);

					if (temp_vend.size() == 0) {

						redirectAttributes.addFlashAttribute("error", "No Vendor Found For This Company");
						return "redirect:/main/mypos";
					}
				} catch (Exception e) {
					LOGGER.error(e);
					return "redirect:/main/mypos";
				}
				return "redirect:/main/edit_verifiedpo";
			}

			else if (status.equals("Invoiced")) {

				tempobj = entityToPojo(purchaseorder_view);
				return "redirect:/main/view_Invoices/" + poNo;
			}

			else if (status.equals("Authorized")) {

				tempobj_authorized = entityToPojo(purchaseorder_view);
				try {
					HashMap<String, Boolean> temp = check_profitcenter_masterdata(purchaseorder_view,
							redirectAttributes);
					if (temp.containsKey("NO_StoreName") && temp.get("NO_StoreName").booleanValue()) {
						redirectAttributes.addFlashAttribute("error", "No Store Name Found For Selected PO");
						return "redirect:/main/mypos";
					}

					if (temp.containsKey("NO_DeptName") && temp.get("NO_DeptName").booleanValue()) {
						redirectAttributes.addFlashAttribute("error", "No Department Name Found For Selected PO");
						return "redirect:/main/mypos";
					}
					if (temp.size() == 0) {
						redirectAttributes.addFlashAttribute("error", "No PC Found For This Company");
						return "redirect:/main/mypos";
					}
				} catch (Exception e) {

					LOGGER.error(e);
					return "redirect:/main/mypos";
				}

				try {
					HashMap<String, Boolean> temp_vend = check_vendor_masterdata(purchaseorder_view);

					if (temp_vend.size() == 0) {

						redirectAttributes.addFlashAttribute("error", "No Vendor Found For This Company");
						return "redirect:/main/mypos";
					}
				} catch (Exception e) {
					LOGGER.error(e);
					return "redirect:/main/mypos";
				}
				return "redirect:/main/edit_authorizedpo";
			}

			else if (status.equals("Redraft")) {

				tempobj_redraft = entityToPojo(purchaseorder_view);
				try {
					HashMap<String, Boolean> temp = check_profitcenter_masterdata(purchaseorder_view,
							redirectAttributes);
					if (temp.containsKey("NO_StoreName") && temp.get("NO_StoreName").booleanValue()) {
						redirectAttributes.addFlashAttribute("error", "No Store Name Found For Selected PO");
						return "redirect:/main/mypos";
					}

					if (temp.containsKey("NO_DeptName") && temp.get("NO_DeptName").booleanValue()) {
						redirectAttributes.addFlashAttribute("error", "No Department Name Found For Selected PO");
						return "redirect:/main/mypos";
					}
					if (temp.size() == 0) {
						redirectAttributes.addFlashAttribute("error", "No PC Found For This Company");
						return "redirect:/main/mypos";
					}
				} catch (Exception e) {

					LOGGER.error(e);
					return "redirect:/main/mypos";
				}

				try {
					HashMap<String, Boolean> temp_vend = check_vendor_masterdata(purchaseorder_view);

					if (temp_vend.size() == 0) {

						redirectAttributes.addFlashAttribute("error", "No Vendor Found For This Company");
						return "redirect:/main/mypos";
					}
				} catch (Exception e) {
					LOGGER.error(e);
					return "redirect:/main/mypos";
				}

				return "redirect:/main/edit_re_draftpo";
			}

			else if (status.equals("Deleted")) {

				tempobj_delete = entityToPojo(purchaseorder_view);

				return "redirect:/main/edit_deletedpo";
			} else if (status.equals("Closed")) {
				tempobj = entityToPojo(purchaseorder_view);
				reopen(purchaseorder_view, tempobj, redirectAttributes);
				return "redirect:/main/mypos";
			} else if (status.equals("Cancelled")) {

				tempobj_delete = entityToPojo(purchaseorder_view);
				return "redirect:/main/edit_deletedpo";
			} else if (status.equals("Rejected")) {

				tempobj_delete = entityToPojo(purchaseorder_view);
				return "redirect:/main/edit_deletedpo";
			}

		} else {
			return "redirect:/main/mypos";
		}
		return "redirect:/main/mypos";
	}

	private List<String> currencyList() {
		List<String> lis = new ArrayList<>();
		lis.add("EURO");
		lis.add("DOLLAR");
		lis.add("MYR");
		lis.add("RENMINBI");
		return lis;
	}

	@GetMapping(value = "/api/image")
	public ResponseEntity<InputStreamResource> getImage(@RequestParam("filepath") Optional<String> filepath) {
		MDC.put("sessionId",
				AbstractMainController.getCompany().isPresent() ? AbstractMainController.getCompany().get() : "");
		ResponseEntity<InputStreamResource> temp = ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG)
				.body(new InputStreamResource(new ByteArrayInputStream(new byte[0])));
		try {
			if (filepath.isPresent()) {

				File file = new File(filepath.get());
				if (file.exists()) {
					return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG)
							.body(new InputStreamResource(new FileInputStream(file)));
				} else {
					return temp;
				}
			} else {
				return temp;
			}
		} catch (Exception e) {
			return temp;
		}
	}

	@GetMapping(value = "/api/pdf")
	public ResponseEntity<InputStreamResource> getPDF(@RequestParam("filepath") Optional<String> filepath) {
		ResponseEntity<InputStreamResource> temp = ResponseEntity.ok().contentType(MediaType.APPLICATION_PDF)
				.body(new InputStreamResource(new ByteArrayInputStream(new byte[0])));
		try {
			if (filepath.isPresent()) {
				File file = new File(filepath.get());
				if (file.exists()) {
					return ResponseEntity.ok().contentType(MediaType.APPLICATION_PDF)
							.body(new InputStreamResource(new FileInputStream(file)));
				} else {
					return temp;
				}
			} else {
				return temp;
			}
		} catch (Exception e) {
			LOGGER.error(e);
			return temp;
		}
	}

	/**
	 * @return the activetab
	 */
	public String getActivetab() {
		return activetab;
	}

	/**
	 * @param activetab
	 *            the activetab to set
	 */
	public void setActivetab(String activetab) {
		this.activetab = activetab;
	}

	/**
	 * @return the profitCenterCode
	 */
	public ProfitCenter getProfitCenterCode() {
		return profitCenterCode;
	}

	/**
	 * @param profitCenterCode
	 *            the profitCenterCode to set
	 */
	public void setProfitCenterCode(ProfitCenter profitCenterCode) {
		this.profitCenterCode = profitCenterCode;
	}

	/**
	 * @param PurchaseOrder
	 *            Check Validity of master data and po - profitcenter
	 */
	public HashMap<String, Boolean> check_profitcenter_masterdata(PurchaseOrder purchaseOrder,
			RedirectAttributes redirectAttributes) {

		ProfitCenter check_pc_obj = profitCenterService.getProfitCenterByCode(purchaseOrder.getPcCode());
		pc_info_list = new HashMap<String, Boolean>();
		pc_info_list.put("st_name", false);
		pc_info_list.put("dept_name", false);
		pc_info_list.put("pc_add1_name", false);
		pc_info_list.put("pc_add2_name", false);
		pc_info_list.put("pc_add3_name", false);
		pc_info_list.put("pc_postcode_name", false);
		pc_info_list.put("pc_postarea_name", false);
		pc_info_list.put("NO_StoreName", false);
		pc_info_list.put("NO_DeptName", false);

		if (check_pc_obj != null) {

			if (check_pc_obj.getStore().getName() != null) {
				if (!check_pc_obj.getStore().getName().equals(purchaseOrder.getStoreName())) {

					pc_info_list.put("st_name", true);
					st_old = purchaseOrder.getStoreName();
					st_new = check_pc_obj.getStore().getName();

				}
			} else {

				pc_info_list.put("NO_StoreName", true);
				return pc_info_list;
			}

			if (check_pc_obj.getDepartment().getName() != null) {
				if (!check_pc_obj.getDepartment().getName().equals(purchaseOrder.getDepartmentName())) {

					pc_info_list.put("dept_name", true);
					dept_old = purchaseOrder.getDepartmentName();
					dept_new = check_pc_obj.getDepartment().getName();
				}
			} else {
				pc_info_list.put("NO_DeptName", true);
				return pc_info_list;
			}

			if (check_pc_obj.getAddressLine1() != null) {
				if (!check_pc_obj.getAddressLine1().equals(purchaseOrder.getAddressLine1())) {

					// pc_add1_match = false;
					pc_info_list.put("pc_add1_name", true);
					add1_old = purchaseOrder.getAddressLine1();
					add1_new = check_pc_obj.getAddressLine1();
					if (add1_new.equals("")) {
						add1_new = "' '";
					}

				}
			}

			if (check_pc_obj.getAddressLine2() != null) {
				if (!check_pc_obj.getAddressLine2().equals(purchaseOrder.getAddressLine2())) {

					// pc_add2_match = false;
					pc_info_list.put("pc_add2_name", true);
					add2_old = purchaseOrder.getAddressLine2();
					add2_new = check_pc_obj.getAddressLine2();
					if (add2_new.equals("")) {
						add2_new = "' '";
					}
				}
			}

			if (check_pc_obj.getAddressLine3() != null) {
				if (!check_pc_obj.getAddressLine3().equals(purchaseOrder.getAddressLine3())) {

					pc_add3_match = false;
					pc_info_list.put("pc_add3_name", true);
					add3_old = purchaseOrder.getAddressLine3();
					add3_new = check_pc_obj.getAddressLine3();
					if (add3_new.equals("")) {
						add3_new = "' '";
					}
				}
			}

			if (check_pc_obj.getPostCode() != null) {
				if (!check_pc_obj.getPostCode().equals(purchaseOrder.getPostCode())) {

					pc_postcode_match = false;
					pc_info_list.put("pc_postcode_name", true);
					postarea_old = purchaseOrder.getPostCode();
					postarea_new = check_pc_obj.getPostCode();
					if (postarea_new.equals("")) {
						postarea_new = "' '";
					}
				}
			}

			if (check_pc_obj.getPostArea() != null) {
				if (!check_pc_obj.getPostArea().equals(purchaseOrder.getPostArea())) {

					pc_postarea_match = false;
					pc_info_list.put("pc_postarea_name", true);
					postarea_old = purchaseOrder.getPostArea();
					postarea_new = check_pc_obj.getPostArea();
					if (postarea_new.equals("")) {
						postarea_new = "' '";
					}
				}
			}
		}
		return pc_info_list;
	}

	/**
	 * @param purchase
	 *            order validity with vendor
	 */
	public HashMap<String, Boolean> check_vendor_masterdata(PurchaseOrder purchaseOrder) {
		VendorBranchId pkey = new VendorBranchId(purchaseOrder.getVendorcode(), purchaseOrder.getVendorbranchcode(),
				getCompany().get());
		VendorBranch check_vendor_obj = vbService.getVendorBranchbyVendorId(pkey);

		vendor_info_list = new HashMap<String, Boolean>();

		vendor_info_list.put("vend_contact_name", false);
		vendor_info_list.put("vend_number", false);
		vendor_info_list.put("vend_email", false);
		vendor_info_list.put("vend_add1", false);
		vendor_info_list.put("vend_add2", false);
		vendor_info_list.put("vend_add3", false);
		vendor_info_list.put("vend_postcode", false);
		vendor_info_list.put("vend_postarea", false);

		if (check_vendor_obj != null) {

			if (check_vendor_obj.getLastContactPersonName() != null) {
				if (!check_vendor_obj.getLastContactPersonName().equals(purchaseOrder.getContactPersonName())) {
					// vend_name_match = false;
					vendor_info_list.put("vend_contact_name", true);
					vend_name_new = check_vendor_obj.getLastContactPersonName();

					if (vend_name_new.equals("")) {
						vend_name_new = "' '";
					}

				}
			}

			if (check_vendor_obj.getLastContactPersonContactNumber() != null) {
				if (!check_vendor_obj.getLastContactPersonContactNumber()
						.equals(purchaseOrder.getVendorPersonInChargePhone())) {

					// vend_number_match = false;
					vendor_info_list.put("vend_number", true);
					vend_number_new = check_vendor_obj.getLastContactPersonContactNumber();
					if (vend_number_new.equals("")) {
						vend_number_new = "' ' ";
					}
				}
			}

			if (check_vendor_obj.getLastContactPersonEmailAddress() != null) {
				if (!check_vendor_obj.getLastContactPersonEmailAddress()
						.equals(purchaseOrder.getVendorPersonInChargeEmail())) {

					vend_email_match = false;
					vendor_info_list.put("vend_email", true);
					vend_email_new = check_vendor_obj.getLastContactPersonEmailAddress();
					if (vend_email_new.equals("")) {
						vend_email_new = "' '";

					}
				}
			}

			if (check_vendor_obj.getAddressLine1() != null) {
				if (!check_vendor_obj.getAddressLine1().equals(purchaseOrder.getVendorAddressLine1())) {

					// vend_add1_match = false;
					vendor_info_list.put("vend_add1", true);
					vend_add1_new = check_vendor_obj.getAddressLine1();
					if (vend_add1_new.equals("")) {
						vend_add1_new = "' '";

					}
				}
			}

			if (check_vendor_obj.getAddressLine2() != null) {
				if (!check_vendor_obj.getAddressLine2().equals(purchaseOrder.getVendorAddressLine2())) {

					// vend_add2_match = false;
					vendor_info_list.put("vend_add2", true);
					vend_add2_new = check_vendor_obj.getAddressLine2();
					if (vend_add2_new.equals("")) {
						vend_add2_new = "' '";

					}
				}
			}

			if (check_vendor_obj.getAddressLine3() != null) {

				if (!check_vendor_obj.getAddressLine3().equals(purchaseOrder.getVendorAddressLine3())) {

					// vend_add3_match = false;
					vendor_info_list.put("vend_add3", true);
					vend_add3_new = check_vendor_obj.getAddressLine3();
					if (vend_add3_new.equals("")) {
						vend_add3_new = "' '";

					}
				}
			}

			if (check_vendor_obj.getPostCode() != null) {
				if (!check_vendor_obj.getPostCode().equals(purchaseOrder.getVendorPostCode())) {

					// vend_postcode_match = false;
					vendor_info_list.put("vend_postcode", true);
					vend_postcode_new = check_vendor_obj.getPostCode();
					if (vend_postcode_new.equals("")) {
						vend_postcode_new = "' '";

					}
				}
			}

			if (check_vendor_obj.getPostCodeArea() != null) {

				if (!check_vendor_obj.getPostCodeArea().equals(purchaseOrder.getVendorPostArea())) {

					// vend_postarea_match = false;
					vendor_info_list.put("vend_postarea", true);
					vend_postarea_new = check_vendor_obj.getPostCodeArea();
					if (vend_postarea_new.equals("")) {
						vend_postarea_new = "' '";

					}
				}
			}

		}
		return vendor_info_list;

	}

	@RequestMapping(value = "/main/redraft_inactive_pc", method = RequestMethod.GET)
	@ResponseBody
	public String redraft_inactivatedpc(@RequestParam("poNumber") Optional<String> poNumber,

			RedirectAttributes redirectAttributes, Model model) {

		PurchaseOrder po_obj = poMainService.getPurchaseOrderByPoNo(poNumber.get());

		po_obj.setStatus("Redraft");
		po_obj.setLastModifiedOn(new Date());

		try {
			poMainService.createPurchaseOrder(po_obj);
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return "redirect:/main/mypos";

	}

	private List<String> getProfitCenterCodeAndProfitcenterName() {
		List<String> lis = new ArrayList<>();
		List<ProfitCenter> temp = pcrepo.getAllActiveProfitCenter();
		if (temp != null) {
			for (ProfitCenter e : temp) {
				StringBuilder s = new StringBuilder();
				s.append(e.getCode());
				s.append("-");
				s.append(e.getName());
				lis.add(s.toString());
			}
		}
		return lis;
	}

	@Override
	protected String getHeaderTitle() {
		// TODO Auto-generated method stub
		return null;
	}

	@RequestMapping(path = "/main/pdfdownload", method = RequestMethod.GET)
	public ResponseEntity<ByteArrayResource> download(@RequestParam("pono") Optional<String> pono) throws IOException {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		ByteArrayResource resource = new ByteArrayResource(new byte[0]);
		if (pono.isPresent()) {
			resource = new ByteArrayResource(new CreatePdf().buildPdf(pono.get(), outputStream).toByteArray());
		}
		HttpHeaders headers = new HttpHeaders();
		headers.add(HttpHeaders.CONTENT_DISPOSITION,
				"attachment; filename=" + (pono.isPresent() ? pono.get() : "filepdf") + ".pdf");
		return ResponseEntity.ok().headers(headers).contentLength(outputStream.toByteArray().length)
				.contentType(MediaType.parseMediaType("application/octet-stream")).body(resource);
	}

	@GetMapping(value = "/main/view_advancesearchresult")
	public String view_advancesearchresult(@RequestParam String poNo, Model model,
			RedirectAttributes redirectAttributes) throws UnsupportedEncodingException, ParseException {

		purchaseorder_view = poMainService.getPurchaseOrderByPoNo(poNo);
		if (purchaseorder_view != null) {
			search_obj = entityToPojo(purchaseorder_view);
			return "redirect:/main/view_advancedsearch_result";

		}
		return "redirect:/main/advancedsearch_result";
	}

	@GetMapping("/main/view_advancedsearch_result")
	public ModelAndView onLoadViewResult() {
		ModelAndView modelAndView = new ModelAndView("/main/view_po");

		authDir = sysRepo.getSystemParameterValueByPropName(AUTHGRID_UPLOAD_PATH);

		String dateFormat = "dd-MM-yyyy";
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_FORMAT");
		if (dateObj != null && !dateObj.getPropValue().isEmpty()) {
			dateFormat = dateObj.getPropValue();
		}
		modelAndView.addObject("date_format", dateFormat);
		modelAndView.addObject("AuthorizationGrid", orderAuthGrid);

		modelAndView.addObject("iteminfoobj", new ItemInfoForm());

		modelAndView.addObject("lastcrt", crt_edit);
		modelAndView.addObject("newpath", newPath);
		modelAndView.addObject("path_edit", path_edit);

		PoMainForm obj = search_obj != null ? search_obj : new PoMainForm();
		modelAndView.getModelMap().addAttribute("pomainobj", obj);
		modelAndView.addObject("activetab", obj.getActivetab());
		modelAndView.addObject("lastcrt_used", obj.getLastcrt_used());
		modelAndView.addObject("quotNo", obj.getQuoteNo());
		modelAndView.addObject("quotDate", obj.getQuoteDate());
		modelAndView.addObject("vendincharge", obj.getVendorPersonInCharge());
		modelAndView.addObject("poNumber", obj.getPoNo());
		modelAndView.addObject("status", obj.getStatus());

		ItemForm itemobj = new ItemForm();
		itemobj.setItemOrder(getItemOrder(obj.getPoNo()));
		modelAndView.addObject("itemobj", itemobj);

		return modelAndView;

	}

	/**
	 * 
	 * @param searchval
	 * @param redirectAttributes
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/api/employeeautocomplete", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody List<AutocompleteForm> employeeSearch9(@RequestParam("query") String searchval) {

		try {

			Specification<VendorBranch> sp = new Specification<VendorBranch>() {
				@Override
				public Predicate toPredicate(Root<VendorBranch> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
					List<Predicate> ls = new ArrayList<>();
					List<Predicate> templs = new ArrayList<>();
					if (searchval != null && !searchval.isEmpty()) {
						ls.add(cb.like(cb.lower(root.get("vendorBranchId").get("code")),
								"%" + searchval.toLowerCase() + "%"));
						ls.add(cb.like(cb.lower(root.get("vendorBranchId").get("branchCode")),
								"%" + searchval.toLowerCase() + "%"));
						ls.add(cb.like(cb.lower(root.get("name")), "%" + searchval.toLowerCase() + "%"));
						templs.add(cb.or(ls.toArray(new Predicate[0])));
					}

					templs.add(cb.equal(root.get("isActive"), true));
					if (getCompany().isPresent()) {
						templs.add(cb.like(cb.lower(root.get("vendorBranchId").get("companyCode")),
								"%" + getCompany().get() + "%"));
					}
					return cb.and(templs.toArray(new Predicate[0]));
				}
			};
			Page<VendorBranch> list;
			if (searchval != null && !searchval.isEmpty()) {
				list = vbService.getAllPageableVendorBranch(sp, new PageRequest(0, 20));

			} else {
				list = vbService.getAllPageableVendorBranch(new PageRequest(0, 20));
			}
			List<AutocompleteForm> temp = new ArrayList<>();

			for (VendorBranch e : list.getContent()) {
				AutocompleteForm a = new AutocompleteForm();
				StringBuilder s = new StringBuilder();
				s.append(e.getVendorBranchId().getCode());
				s.append("-");
				s.append(e.getName());
				s.append("-");
				s.append(e.getVendorBranchId().getBranchCode());
				a.setId(s.toString());
				a.setName(s.toString());
				temp.add(a);
			}
			return temp;
		} catch (Exception e) {
			LOGGER.error(e);
		}

		return new ArrayList<>();
	}

	public void getoldlist(String pono) {
		oldlist = itemMapRepo.getAllItemMapByPurchaseOrderNo(pono);
		if (oldlist != null && !oldlist.isEmpty()) {
			for (ItemMap o : oldlist) {
				o.setOldlst(true);
				itemMapRepo.createItemMap(o);
			}
		}
	}
	
}
