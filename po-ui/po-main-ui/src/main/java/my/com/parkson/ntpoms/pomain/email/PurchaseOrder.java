/**
 *****************************************************
 * COPYRIGHT UMAIR OTHMAN
 * 2017
 * IT Exec
 *****************************************************
 */
package my.com.parkson.ntpoms.pomain.email;

import java.util.Date;


/**
 **************************************************************
 * Class use to store PO list objects in Po Email Criteria
 **************************************************************
 */
public class PurchaseOrder {
	
	String poNumber;
	Date poDate;
	
	public String getPoNumber() {
		return poNumber;
	}
	
	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}
	
	public Date getPoDate() {
		return poDate;
	}
	
	public void setPoDate(Date poDate) {
		this.poDate = poDate;
	}
}