package my.com.parkson.ntpoms.pomain.web.models;

import java.io.Serializable;
import java.util.Date;

public class ItemInfoForm implements Serializable {
	
	private String iteminfo_vendorcode;
	private String iteminfo_vendorbranchcode;	
	
	private String iteminfo_itemno;
	private String iteminfo_particulars;	
	private String iteminfo_packing;
	
	
	public String getIteminfo_vendorcode() {
		return iteminfo_vendorcode;
	}
	public void setIteminfo_vendorcode(String iteminfo_vendorcode) {
		this.iteminfo_vendorcode = iteminfo_vendorcode;
	}
	public String getIteminfo_vendorbranchcode() {
		return iteminfo_vendorbranchcode;
	}
	public void setIteminfo_vendorbranchcode(String iteminfo_vendorbranchcode) {
		this.iteminfo_vendorbranchcode = iteminfo_vendorbranchcode;
	}
	public String getIteminfo_itemno() {
		return iteminfo_itemno;
	}
	public void setIteminfo_itemno(String iteminfo_itemno) {
		this.iteminfo_itemno = iteminfo_itemno;
	}
	public String getIteminfo_particulars() {
		return iteminfo_particulars;
	}
	public void setIteminfo_particulars(String iteminfo_particulars) {
		this.iteminfo_particulars = iteminfo_particulars;
	}
	public String getIteminfo_packing() {
		return iteminfo_packing;
	}
	public void setIteminfo_packing(String iteminfo_packing) {
		this.iteminfo_packing = iteminfo_packing;
	}
		
	
}
