package my.com.parkson.ntpoms.pomain.web.models;

import java.io.Serializable;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class ProfitCenterForm implements Serializable {

	private static final long serialVersionUID = 1L;

	private String pcCode;

	private String pcAbbrName;

	private String company;

	private String pcName;

	private String addressLine1;

	private String addressLine2;

	private String addressLine3;

	private String isactive;

	private Date createdOn;

	private String pcHeadfrom;

	private String pcHeadto;

	private String pcViewRightsto;

	private String pcViewRightsfrom;

	private String createdBy;

	private Date lastModifiedOn;

	private String lastModifiedBy;

	private String deactivatedBy;

	private Date deactivatedOn;

	private String reactivatedBy;

	private Date reactivatedOn;

	private String store;

	private String deartment;


	private String postCode;

	private String postArea;

	private String storeType;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date startDate;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date endDate;

	/**
	 * @return the pcCode
	 */
	public String getPcCode() {
		return pcCode;
	}

	/**
	 * @param pcCode
	 *            the pcCode to set
	 */
	public void setPcCode(String pcCode) {
		this.pcCode = pcCode;
	}

	/**
	 * @return the pcAbbrName
	 */
	public String getPcAbbrName() {
		return pcAbbrName;
	}

	/**
	 * @param pcAbbrName
	 *            the pcAbbrName to set
	 */
	public void setPcAbbrName(String pcAbbrName) {
		this.pcAbbrName = pcAbbrName;
	}

	/**
	 * @return the pcName
	 */
	public String getPcName() {
		return pcName;
	}

	/**
	 * @param pcName
	 *            the pcName to set
	 */
	public void setPcName(String pcName) {
		this.pcName = pcName;
	}

	/**
	 * @return the addressLine1
	 */
	public String getAddressLine1() {
		return addressLine1;
	}

	/**
	 * @param addressLine1
	 *            the addressLine1 to set
	 */
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	/**
	 * @return the addressLine2
	 */
	public String getAddressLine2() {
		return addressLine2;
	}

	/**
	 * @param addressLine2
	 *            the addressLine2 to set
	 */
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	/**
	 * @return the addressLine3
	 */
	public String getAddressLine3() {
		return addressLine3;
	}

	/**
	 * @param addressLine3
	 *            the addressLine3 to set
	 */
	public void setAddressLine3(String addressLine3) {
		this.addressLine3 = addressLine3;
	}

	/**
	 * @return the isactive
	 */
	public String getIsactive() {
		return isactive;
	}

	/**
	 * @param isactive
	 *            the isactive to set
	 */
	public void setIsactive(String isactive) {
		this.isactive = isactive;
	}

	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn
	 *            the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the lastModifiedOn
	 */
	public Date getLastModifiedOn() {
		return lastModifiedOn;
	}

	/**
	 * @param lastModifiedOn
	 *            the lastModifiedOn to set
	 */
	public void setLastModifiedOn(Date lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

	/**
	 * @return the lastModifiedBy
	 */
	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	/**
	 * @param lastModifiedBy
	 *            the lastModifiedBy to set
	 */
	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	/**
	 * @return the deactivatedBy
	 */
	public String getDeactivatedBy() {
		return deactivatedBy;
	}

	/**
	 * @param deactivatedBy
	 *            the deactivatedBy to set
	 */
	public void setDeactivatedBy(String deactivatedBy) {
		this.deactivatedBy = deactivatedBy;
	}

	/**
	 * @return the deactivatedOn
	 */
	public Date getDeactivatedOn() {
		return deactivatedOn;
	}

	/**
	 * @param deactivatedOn
	 *            the deactivatedOn to set
	 */
	public void setDeactivatedOn(Date deactivatedOn) {
		this.deactivatedOn = deactivatedOn;
	}

	/**
	 * @return the reactivatedBy
	 */
	public String getReactivatedBy() {
		return reactivatedBy;
	}

	/**
	 * @param reactivatedBy
	 *            the reactivatedBy to set
	 */
	public void setReactivatedBy(String reactivatedBy) {
		this.reactivatedBy = reactivatedBy;
	}

	/**
	 * @return the reactivatedOn
	 */
	public Date getReactivatedOn() {
		return reactivatedOn;
	}

	/**
	 * @param reactivatedOn
	 *            the reactivatedOn to set
	 */
	public void setReactivatedOn(Date reactivatedOn) {
		this.reactivatedOn = reactivatedOn;
	}

	

	/**
	 * @return the pcHeadfrom
	 */
	public String getPcHeadfrom() {
		return pcHeadfrom;
	}

	/**
	 * @param pcHeadfrom the pcHeadfrom to set
	 */
	public void setPcHeadfrom(String pcHeadfrom) {
		this.pcHeadfrom = pcHeadfrom;
	}

	/**
	 * @return the pcHeadto
	 */
	public String getPcHeadto() {
		return pcHeadto;
	}

	/**
	 * @param pcHeadto the pcHeadto to set
	 */
	public void setPcHeadto(String pcHeadto) {
		this.pcHeadto = pcHeadto;
	}

	/**
	 * @return the pcViewRightsto
	 */
	public String getPcViewRightsto() {
		return pcViewRightsto;
	}

	/**
	 * @param pcViewRightsto the pcViewRightsto to set
	 */
	public void setPcViewRightsto(String pcViewRightsto) {
		this.pcViewRightsto = pcViewRightsto;
	}

	/**
	 * @return the pcViewRightsfrom
	 */
	public String getPcViewRightsfrom() {
		return pcViewRightsfrom;
	}

	/**
	 * @param pcViewRightsfrom the pcViewRightsfrom to set
	 */
	public void setPcViewRightsfrom(String pcViewRightsfrom) {
		this.pcViewRightsfrom = pcViewRightsfrom;
	}

	/**
	 * @return the postCode
	 */
	public String getPostCode() {
		return postCode;
	}

	/**
	 * @param postCode
	 *            the postCode to set
	 */
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	/**
	 * @return the postArea
	 */
	public String getPostArea() {
		return postArea;
	}

	/**
	 * @param postArea
	 *            the postArea to set
	 */
	public void setPostArea(String postArea) {
		this.postArea = postArea;
	}

	/**
	 * @return the storeType
	 */
	public String getStoreType() {
		return storeType;
	}

	/**
	 * @param storeType
	 *            the storeType to set
	 */
	public void setStoreType(String storeType) {
		this.storeType = storeType;
	}

	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate
	 *            the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate
	 *            the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the company
	 */
	public String getCompany() {
		return company;
	}

	/**
	 * @param company
	 *            the company to set
	 */
	public void setCompany(String company) {
		this.company = company;
	}

	/**
	 * @return the store
	 */
	public String getStore() {
		return store;
	}

	/**
	 * @param store
	 *            the store to set
	 */
	public void setStore(String store) {
		this.store = store;
	}

	/**
	 * @return the deartment
	 */
	public String getDeartment() {
		return deartment;
	}

	/**
	 * @param deartment
	 *            the deartment to set
	 */
	public void setDeartment(String deartment) {
		this.deartment = deartment;
	}

}