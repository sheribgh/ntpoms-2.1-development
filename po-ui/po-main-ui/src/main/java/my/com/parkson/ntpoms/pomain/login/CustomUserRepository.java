package my.com.parkson.ntpoms.pomain.login;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;

import my.com.parkson.ntpoms.common.entities.Company;
import my.com.parkson.ntpoms.common.entities.PurchaseOrderUser;
import my.com.parkson.ntpoms.common.entities.SystemParameter;
import my.com.parkson.ntpoms.common.repositories.CompanyRepository;
import my.com.parkson.ntpoms.common.services.CompanyService;
import my.com.parkson.ntpoms.common.services.PurchaseOrderUserService;
import my.com.parkson.ntpoms.common.services.SecurityService;
import my.com.parkson.ntpoms.common.services.SystemParameterService;

@Repository("userRepository")
public class CustomUserRepository implements UserRepository {

	private static final Logger LOGGER = Logger.getLogger(CustomUserRepository.class);

	@Autowired
	private SecurityService securityService;

	@Autowired
	private PurchaseOrderUserService poUserAccess;
	
	@Autowired
	private CompanyService comprepo;

	@Autowired
	private SystemParameterService systemParameterService;

	@Override
	public User findUser(String username, String password, String company, String year) {
		if (username == null || company == null || year == null) {
			return null;
		} else {
			List<SimpleGrantedAuthority> authorities = new ArrayList<>();
			User user = null;
			my.com.parkson.ntpoms.common.entities.User userObj = securityService.findUserByUsername(username);
			PurchaseOrderUser pouserobj = poUserAccess.getPurchaseOrderUserById(username);
			System.out.println("comp"+company);
			Company compa = comprepo.getCompanyByCode(company);
			System.out.println(compa.isActive());
			if (userObj != null && new BCryptPasswordEncoder().matches(password, userObj.getPassword())) {
				authorities.add(new SimpleGrantedAuthority("ROLE_POADMIN"));
				user = new User(username, company, year, new BCryptPasswordEncoder().encode(password), true, true, true,
						true, authorities);
			} else if (checkLDAPAuthentication(username, password) || (pouserobj != null && pouserobj.isActive())) {
				authorities.add(new SimpleGrantedAuthority("ROLE_POUSER"));
				if (compa != null && !compa.isActive()) {
					System.out.println("if");
					authorities.add(new SimpleGrantedAuthority("INACTIVE"));
				} else {
					System.out.println("else");
					authorities.add(new SimpleGrantedAuthority("ACTIVE"));
				}
				user = new User(username, company, year, new BCryptPasswordEncoder().encode(password), true, true, true,
						true, authorities);
			} else if ("superadmin".equals(username)) {
				authorities.add(new SimpleGrantedAuthority("ROLE_POUSER"));
					authorities.add(new SimpleGrantedAuthority("ACTIVE"));
			}
			return user;
		}
	}

	/**
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	public boolean checkLDAPAuthentication(String username, String password) {
		try {
			String url = null;
			if (systemParameterService != null) {
				SystemParameter ldapURL = systemParameterService.getSystemParameterByPropName("UPLOAD_FOLDER_PATH");
				url = ldapURL.getPropValue();
			}
			Hashtable<String, Object> env = new Hashtable<String, Object>(11);
			env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
			env.put(Context.PROVIDER_URL, url);
			env.put(Context.SECURITY_AUTHENTICATION, "simple");
			env.put(Context.SECURITY_PRINCIPAL, username);
			env.put(Context.SECURITY_CREDENTIALS, password);
			DirContext ctx = new InitialDirContext(env);
			ctx.close();
			return true;
		} catch (NamingException e) {
			LOGGER.error(e);
			return false;
		}
	}

}
