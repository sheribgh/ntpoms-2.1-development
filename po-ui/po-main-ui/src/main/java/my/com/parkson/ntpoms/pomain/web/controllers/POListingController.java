package my.com.parkson.ntpoms.pomain.web.controllers;

import static my.com.parkson.ntpoms.common.MessageCodes.FAILED_PERSIST_EXCEPTION;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.apache.velocity.runtime.parser.node.MathUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.LogMessages;
import my.com.parkson.ntpoms.common.entities.Company;
import my.com.parkson.ntpoms.common.entities.ProfitCenter;
import my.com.parkson.ntpoms.common.entities.SystemParameter;
import my.com.parkson.ntpoms.common.model.Pager;
import my.com.parkson.ntpoms.common.services.MessageService;
import my.com.parkson.ntpoms.common.services.SystemParameterService;
import my.com.parkson.ntpoms.main.entities.Invoice;
import my.com.parkson.ntpoms.main.entities.PurchaseOrder;
import my.com.parkson.ntpoms.main.entities.PurchaseOrderArchiveDetails;
import my.com.parkson.ntpoms.main.entities.PurchaseOrderUserAccess;
import my.com.parkson.ntpoms.main.entities.VendorBranch;
import my.com.parkson.ntpoms.main.entities.VendorCreditTerms;
import my.com.parkson.ntpoms.main.entities.VendorCreditTermsId;
import my.com.parkson.ntpoms.main.repositories.InvoiceRepository;
import my.com.parkson.ntpoms.main.repositories.PurchaseOrderRepository;
import my.com.parkson.ntpoms.main.services.AccessLogsService;
import my.com.parkson.ntpoms.main.services.AuditsService;
import my.com.parkson.ntpoms.main.services.InvoiceService;
import my.com.parkson.ntpoms.main.services.PurchaseOrderArchiveService;
import my.com.parkson.ntpoms.main.services.PurchaseOrderService;
import my.com.parkson.ntpoms.main.services.PurchaseOrderUserAccessService;
import my.com.parkson.ntpoms.pomain.web.models.InvoiceForm;
import my.com.parkson.ntpoms.pomain.web.models.PoMainForm;
import my.com.parkson.ntpoms.pomain.web.models.VendorBranchForm;
import my.com.parkson.ntpoms.pomain.web.utils.PurchaseOrderSendMailUtil;
 
@Slf4j
@Controller
@RequiredArgsConstructor
public class POListingController extends AbstractMainController {

	private static final Logger LOGGER = Logger.getLogger(POListingController.class);

	private static final int BUTTONS_TO_SHOW = 5;
	private static final int INITIAL_PAGE = 0;
	private static final int INITIAL_PAGE_SIZE = 5;
	private static final int[] PAGE_SIZES = { 5, 10, 20 };

	@Autowired
	private PurchaseOrderUserAccessService poUsrAcc;

	@Autowired
	private PurchaseOrderService purchaseorder;

	@Autowired
	private SystemParameterService sysRepo;

	@Autowired
	private InvoiceService invoiceRepo;

	private InvoiceForm tempobj;

	/** The messages. */
	@Autowired
	private MessageService messages;

	@Autowired
	private AuditsService auditService;

	@Autowired
	InvoiceRepository invoiceRepository;

	@Autowired
	private PurchaseOrderArchiveService poarchrepo;

	private ProfitCenter profitCenterCode;

	boolean closeexcept = false;

	private String poNumber;

	private boolean disable_invoice;

	private boolean disable_invoice_nonauthpo;

	private boolean disable_invoice_closed;

	private boolean disable_non_invoicepo;

	private boolean disable_close_butt_closedinvoicepo;

	private boolean last_invoice;
	///////////////////////////////////////////////////// Draft POs
	///////////////////////////////////////////////////// ////////////////////////////////////////////////////////////////////////

	@GetMapping("/main/draftpo")
	public ModelAndView onPageLoadDraftPO(Model model) {

		String date_format = "dd-MM-yyyy";
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_FORMAT");

		if (dateObj != null) {
			if (!dateObj.getPropValue().isEmpty()) {
				date_format = dateObj.getPropValue();
			}
		}

		model.addAttribute("date_format", date_format);

		ModelAndView modelAndView = new ModelAndView("/main/draftpo");
		return modelAndView;
	}

	@RequestMapping(value = "/main/DraftPoGrid", method = RequestMethod.GET)
	public String draftPOGrid(@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page, @RequestParam("poNo") Optional<String> poNo,
			@RequestParam("status") Optional<String> status, @RequestParam("vendorName") Optional<String> vendorName,
			@RequestParam("issuedOn") Optional<String> issuedOn,
			@RequestParam("sortingkey") Optional<String> sortingkey,
			@RequestParam("sortingorder") Optional<String> sortingorder, Model model) {

		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;

		String date_format = "dd-MM-yyyy";
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_FORMAT");
		if (dateObj != null) {
			if (!dateObj.getPropValue().isEmpty()) {
				date_format = dateObj.getPropValue();
			}
		}

		Page<PurchaseOrder> list;

		/***
		 * Getting List of Po Numbers Under PSID and Authorize Action and Status
		 * from user accss and po_t_main table
		 ***/
		List<PurchaseOrderUserAccess> pouser = poUsrAcc.getBypsIDAndAction(getCurrentUser().get(), "D");
		List<String> poNumber = new ArrayList<>();
		PurchaseOrder pos = new PurchaseOrder();
		List<String> purchaseorderlistFromPOMain = new ArrayList<>();

		Calendar calendar = new GregorianCalendar();
		String calendar_year = Integer.toString(calendar.get(Calendar.YEAR));
		String logedin_year = getYear().get();
		if (logedin_year.equals(calendar_year)) {

			for (PurchaseOrderUserAccess a : pouser) {

				String ponos_sub = a.getPurchaseorderuseraccessId().getPono().substring(5, 7);
				String calendar_year_sub = calendar_year.substring(2);
				String logedin_year_sub = logedin_year.substring(2);
				int last_year_sub = Integer.valueOf(logedin_year_sub) - 1;

				if (ponos_sub.equals(calendar_year_sub) || ponos_sub.equals(Integer.toString(last_year_sub))) {

					poNumber.add(a.getPurchaseorderuseraccessId().getPono());
					pos = purchaseorder.getPurchaseOrderByPoNo(a.getPurchaseorderuseraccessId().getPono());

					if (pos != null) {
						if (pos.getStatus().equals("Draft")) {
							purchaseorderlistFromPOMain.add(pos.getPoNo());
						}
					}
				}
			}

		} else {

			for (PurchaseOrderUserAccess a : pouser) {

				String ponos_sub = a.getPurchaseorderuseraccessId().getPono().substring(5, 7);

				String logedin_year_sub = logedin_year.substring(2);

				if (ponos_sub.equals(logedin_year_sub)) {

					poNumber.add(a.getPurchaseorderuseraccessId().getPono());
					pos = purchaseorder.getPurchaseOrderByPoNo(a.getPurchaseorderuseraccessId().getPono());

					if (pos != null) {
						if (pos.getStatus().equals("Draft")) {
							purchaseorderlistFromPOMain.add(pos.getPoNo());
						}
					}
				}
			}

		}

		Specification<PurchaseOrder> s = new Specification<PurchaseOrder>() {
			@Override
			public Predicate toPredicate(Root<PurchaseOrder> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> ls = new ArrayList<>();
				if (poNo.isPresent() && !poNo.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("poNo")), "%" + poNo.get().toLowerCase() + "%"));
				}
				if (status.isPresent() && !status.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("status")), "%" + status.get().toLowerCase() + "%"));

				}

				if (vendorName.isPresent() && !vendorName.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("vendorName")), "%" + vendorName.get().toLowerCase() + "%"));
				}

				if (issuedOn.isPresent() && !issuedOn.get().isEmpty()) {

					Expression<String> dateStringExpr = cb.function("to_char", String.class, root.get("issuedOn"),
							cb.literal("YYYY-MM-DD HH12:MI:SS"));
					ls.add(cb.like(cb.lower(dateStringExpr), "%" + issuedOn.get().toLowerCase() + "%"));
				}

				List<Predicate> temp = new ArrayList<>();
				for (String s : purchaseorderlistFromPOMain) {
					temp.add(cb.equal(root.get("poNo"), s));
				}
				ls.add(cb.or(temp.toArray(new Predicate[0])));
				return cb.and(ls.toArray(new Predicate[0]));

			}
		};

		Sort sorttemp = new Sort(Sort.Direction.DESC, "lastModifiedOn");

		if (sortingkey.isPresent() && sortingorder.isPresent()) {
			sorttemp = new Sort(sortingorder.get().equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC,
					sortingkey.get());
		}
		if (poNo.isPresent() || status.isPresent() || vendorName.isPresent()) {
			list = purchaseorder.getAllPageablePurchaseOrder(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		} else {
			list = purchaseorder.getAllPageablePurchaseOrder(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		}

		int serialid = 0;

		int pagesize = evalPage + 1;

		if (pagesize > 1) {
			int start = evalPageSize * pagesize;
			serialid = start - evalPageSize;
		}

		Pager pager = new Pager(list.getTotalPages(), list.getNumber(), BUTTONS_TO_SHOW);
		model.addAttribute("serialid", serialid);
		model.addAttribute("list", list);
		model.addAttribute("selectedPageSize", evalPageSize);
		model.addAttribute("pageSizes", PAGE_SIZES);
		model.addAttribute("pager", pager);
		model.addAttribute("totalRecords", list.getTotalElements());
		model.addAttribute("poNo", poNo.isPresent() ? poNo.get() : "");
		model.addAttribute("status", status.isPresent() ? status.get() : "");
		model.addAttribute("vendorName", vendorName.isPresent() ? vendorName.get() : "");
		model.addAttribute("issuedOn", issuedOn.isPresent() ? issuedOn.get() : "");
		model.addAttribute("sortingorder", sortingorder.isPresent() ? sortingorder.get() : "DESC");
		model.addAttribute("sortingkey", sortingkey.isPresent() ? sortingkey.get() : "lastModifiedOn");
		return "/main/draftpo :: result-table";

	}

	///////////////////////////////////////////////// Verify Pos
	///////////////////////////////////////////////// //////////////////////////////////////////////////////////////////////

	@GetMapping("/main/verifypo")
	public ModelAndView onPageLoadVerifyPO(Model model) {

		String date_format = "dd-MM-yyyy";
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_FORMAT");

		if (dateObj != null) {
			if (!dateObj.getPropValue().isEmpty()) {
				date_format = dateObj.getPropValue();
			}
		}

		model.addAttribute("date_format", date_format);

		ModelAndView modelAndView = new ModelAndView("/main/verifypo");
		return modelAndView;
	}

	@RequestMapping(value = "/main/VerfiyPoGrid", method = RequestMethod.GET)
	public String verifyPOGrid(@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page, @RequestParam("poNo") Optional<String> poNo,
			@RequestParam("status") Optional<String> status, @RequestParam("vendorName") Optional<String> vendorName,
			@RequestParam("issuedOn") Optional<String> issuedOn,
			@RequestParam("sortingkey") Optional<String> sortingkey,
			@RequestParam("sortingorder") Optional<String> sortingorder, Model model) {

		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;

		String date_format = "dd-MM-yyyy";
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_FORMAT");
		if (dateObj != null) {
			if (!dateObj.getPropValue().isEmpty()) {
				date_format = dateObj.getPropValue();
			}
		}

		Page<PurchaseOrder> list;
		List<PurchaseOrderUserAccess> pouser = poUsrAcc.getBypsIDAndAction(getCurrentUser().get(), "V");
		List<String> poNumber = new ArrayList<>();
		PurchaseOrder pos = new PurchaseOrder();
		List<String> purchaseorderlistFromPOMain = new ArrayList<>();

		/***
		 * Getting List of Po Numbers Under PSID and Authorize Action and Status
		 * from user accss and po_t_main table
		 ***/

		Calendar calendar = new GregorianCalendar();
		String calendar_year = Integer.toString(calendar.get(Calendar.YEAR));
		String logedin_year = getYear().get();

		// if loged in year is equal to current year, show list of pos from
		// current year and last year

		if (logedin_year.equals(calendar_year)) {

			for (PurchaseOrderUserAccess a : pouser) {

				String ponos_sub = a.getPurchaseorderuseraccessId().getPono().substring(5, 7);
				String calendar_year_sub = calendar_year.substring(2);
				String logedin_year_sub = logedin_year.substring(2);
				int last_year_sub = Integer.valueOf(logedin_year_sub) - 1;

				if (ponos_sub.equals(calendar_year_sub) || ponos_sub.equals(Integer.toString(last_year_sub))) {

					poNumber.add(a.getPurchaseorderuseraccessId().getPono());
					pos = purchaseorder.getPurchaseOrderByPoNo(a.getPurchaseorderuseraccessId().getPono());

					if (pos != null) {
						if (pos.getStatus().equals("Issued")) {
							purchaseorderlistFromPOMain.add(pos.getPoNo());
						}
					}
				}
			}

		} else {

			for (PurchaseOrderUserAccess a : pouser) {

				String ponos_sub = a.getPurchaseorderuseraccessId().getPono().substring(5, 7);

				String logedin_year_sub = logedin_year.substring(2);

				if (ponos_sub.equals(logedin_year_sub)) {

					poNumber.add(a.getPurchaseorderuseraccessId().getPono());
					pos = purchaseorder.getPurchaseOrderByPoNo(a.getPurchaseorderuseraccessId().getPono());

					if (pos != null) {
						if (pos.getStatus().equals("Issued")) {
							purchaseorderlistFromPOMain.add(pos.getPoNo());
						}
					}
				}
			}

		}

		/*
		 * for (PurchaseOrderUserAccess a : pouser) {
		 * 
		 * poNumber.add(a.getPurchaseorderuseraccessId().getPono()); pos =
		 * purchaseorder.getAllPurchaseOrderByPoNo(a.
		 * getPurchaseorderuseraccessId().getPono());
		 * 
		 * if(pos!=null){ if(pos.getStatus().equals("Issued")){
		 * purchaseorderlistFromPOMain.add(pos.getPoNo());
		 * 
		 * 
		 * } }
		 * 
		 * }
		 */

		Specification<PurchaseOrder> s = new Specification<PurchaseOrder>() {
			@Override
			public Predicate toPredicate(Root<PurchaseOrder> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> ls = new ArrayList<>();
				if (poNo.isPresent() && !poNo.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("poNo")), "%" + poNo.get().toLowerCase() + "%"));
				}
				if (status.isPresent() && !status.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("status")), "%" + status.get().toLowerCase() + "%"));

				}

				if (vendorName.isPresent() && !vendorName.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("vendorName")), "%" + vendorName.get().toLowerCase() + "%"));
				}

				if (issuedOn.isPresent() && !issuedOn.get().isEmpty()) {

					Expression<String> dateStringExpr = cb.function("to_char", String.class, root.get("issuedOn"),
							cb.literal("YYYY-MM-DD HH12:MI:SS"));
					ls.add(cb.like(cb.lower(dateStringExpr), "%" + issuedOn.get().toLowerCase() + "%"));
				}

				List<Predicate> temp = new ArrayList<>();
				for (String s : purchaseorderlistFromPOMain) {
					temp.add(cb.equal(root.get("poNo"), s));
				}
				ls.add(cb.or(temp.toArray(new Predicate[0])));
				return cb.and(ls.toArray(new Predicate[0]));
			}
		};

		Sort sorttemp = new Sort(Sort.Direction.DESC, "lastModifiedOn");

		if (sortingkey.isPresent() && sortingorder.isPresent()) {
			sorttemp = new Sort(sortingorder.get().equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC,
					sortingkey.get());
		}
		if (poNo.isPresent() || status.isPresent() || vendorName.isPresent()) {
			list = purchaseorder.getAllPageablePurchaseOrder(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		} else {
			list = purchaseorder.getAllPageablePurchaseOrder(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		}

		int serialid = 0;

		int pagesize = evalPage + 1;

		if (pagesize > 1) {
			int start = evalPageSize * pagesize;
			serialid = start - evalPageSize;
		}

		Pager pager = new Pager(list.getTotalPages(), list.getNumber(), BUTTONS_TO_SHOW);
		model.addAttribute("serialid", serialid);
		model.addAttribute("list", list);
		model.addAttribute("selectedPageSize", evalPageSize);
		model.addAttribute("pageSizes", PAGE_SIZES);
		model.addAttribute("pager", pager);
		model.addAttribute("totalRecords", list.getTotalElements());
		model.addAttribute("poNo", poNo.isPresent() ? poNo.get() : "");
		model.addAttribute("status", status.isPresent() ? status.get() : "");
		model.addAttribute("vendorName", vendorName.isPresent() ? vendorName.get() : "");
		model.addAttribute("issuedOn", issuedOn.isPresent() ? issuedOn.get() : "");
		model.addAttribute("sortingorder", sortingorder.isPresent() ? sortingorder.get() : "DESC");
		model.addAttribute("sortingkey", sortingkey.isPresent() ? sortingkey.get() : "lastModifiedOn");
		return "/main/verifypo :: result-table";

	}

	///////////////////////////////////////////////// Closed Pos
	///////////////////////////////////////////////// //////////////////////////////////////////////////////////////////////

	@GetMapping("/main/closepo")
	public ModelAndView onPageLoadClosePO(Model model) {

		String date_format = "dd-MM-yyyy";
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_FORMAT");

		if (dateObj != null) {
			if (!dateObj.getPropValue().isEmpty()) {
				date_format = dateObj.getPropValue();
			}
		}

		model.addAttribute("date_format", date_format);

		ModelAndView modelAndView = new ModelAndView("/main/closepo");
		return modelAndView;
	}

	@RequestMapping(value = "/main/ClosePoGrid", method = RequestMethod.GET)
	public String closePOGrid(@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page, @RequestParam("poNo") Optional<String> poNo,
			@RequestParam("status") Optional<String> status, @RequestParam("vendorName") Optional<String> vendorName,
			@RequestParam("issuedOn") Optional<String> issuedOn,
			@RequestParam("sortingkey") Optional<String> sortingkey,
			@RequestParam("sortingorder") Optional<String> sortingorder, Model model) {

		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;

		String date_format = "dd-MM-yyyy";
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_FORMAT");
		if (dateObj != null) {
			if (!dateObj.getPropValue().isEmpty()) {
				date_format = dateObj.getPropValue();
			}
		}

		Page<PurchaseOrder> list;
		/*
		 * List<PurchaseOrderUserAccess> pouser =
		 * poUsrAcc.getBypsIDAndAction(getCurrentUser().get(), "I");
		 */
		List<PurchaseOrderUserAccess> pouser = poUsrAcc.getBypsID(getCurrentUser().get());
		List<String> poNumber = new ArrayList<>();
		PurchaseOrder pos = new PurchaseOrder();
		List<String> purchaseorderlistFromPOMain = new ArrayList<>();
		Calendar calendar = new GregorianCalendar();
		String calendar_year = Integer.toString(calendar.get(Calendar.YEAR));
		String logedin_year = getYear().get();

		if (logedin_year.equals(calendar_year)) {
			for (PurchaseOrderUserAccess a : pouser) {

				String ponos_sub = a.getPurchaseorderuseraccessId().getPono().substring(5, 7);
				String calendar_year_sub = calendar_year.substring(2);
				String logedin_year_sub = logedin_year.substring(2);
				int last_year_sub = Integer.valueOf(logedin_year_sub) - 1;
				if (ponos_sub.equals(calendar_year_sub) || ponos_sub.equals(Integer.toString(last_year_sub))) {

					poNumber.add(a.getPurchaseorderuseraccessId().getPono());

					pos = purchaseorder.getAllPurchaseOrderByPoNo(a.getPurchaseorderuseraccessId().getPono());

					if (pos != null) {
						if (pos.getStatus().equals("Closed")) {
							purchaseorderlistFromPOMain.add(pos.getPoNo());
						}
					}

				}
			}

		} else { // if it's not current year, only see the current year po list
			for (PurchaseOrderUserAccess a : pouser) {

				String ponos_sub = a.getPurchaseorderuseraccessId().getPono().substring(5, 7);

				String logedin_year_sub = logedin_year.substring(2);

				if (ponos_sub.equals(logedin_year_sub)) {

					poNumber.add(a.getPurchaseorderuseraccessId().getPono());

					pos = purchaseorder.getAllPurchaseOrderByPoNo(a.getPurchaseorderuseraccessId().getPono());

					if (pos != null) {
						if (pos.getStatus().equals("Closed")) {
							purchaseorderlistFromPOMain.add(pos.getPoNo());
						}
					}

				}
			}

		}

		/***
		 * Getting List of Po Numbers Under PSID and Authorize Action and Status
		 * from user accss and po_t_main table
		 ***/

		/*
		 * for (PurchaseOrderUserAccess a : pouser) {
		 * 
		 * poNumber.add(a.getPurchaseorderuseraccessId().getPono()); pos =
		 * purchaseorder.getAllPurchaseOrderByPoNo(a.
		 * getPurchaseorderuseraccessId().getPono());
		 * 
		 * if(pos!=null){ if(pos.getStatus().equals("Closed")){
		 * purchaseorderlistFromPOMain.add(pos.getPoNo()); } }
		 * 
		 * 
		 * 
		 * }
		 */

		Specification<PurchaseOrder> s = new Specification<PurchaseOrder>() {
			@Override
			public Predicate toPredicate(Root<PurchaseOrder> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> ls = new ArrayList<>();
				if (poNo.isPresent() && !poNo.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("poNo")), "%" + poNo.get().toLowerCase() + "%"));
				}
				if (status.isPresent() && !status.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("status")), "%" + status.get().toLowerCase() + "%"));

				}

				if (vendorName.isPresent() && !vendorName.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("vendorName")), "%" + vendorName.get().toLowerCase() + "%"));
				}

				if (issuedOn.isPresent() && !issuedOn.get().isEmpty()) {

					Expression<String> dateStringExpr = cb.function("to_char", String.class, root.get("issuedOn"),
							cb.literal("YYYY-MM-DD HH12:MI:SS"));
					ls.add(cb.like(cb.lower(dateStringExpr), "%" + issuedOn.get().toLowerCase() + "%"));
				}

				List<Predicate> temp = new ArrayList<>();
				for (String s : purchaseorderlistFromPOMain) {
					temp.add(cb.equal(root.get("poNo"), s));
				}
				ls.add(cb.or(temp.toArray(new Predicate[0])));
				return cb.and(ls.toArray(new Predicate[0]));
			}
		};

		Sort sorttemp = new Sort(Sort.Direction.DESC, "lastModifiedOn");

		if (sortingkey.isPresent() && sortingorder.isPresent()) {
			sorttemp = new Sort(sortingorder.get().equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC,
					sortingkey.get());
		}
		if (poNo.isPresent() || status.isPresent() || vendorName.isPresent()) {
			list = purchaseorder.getAllPageablePurchaseOrder(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		} else {
			list = purchaseorder.getAllPageablePurchaseOrder(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		}

		int serialid = 0;

		int pagesize = evalPage + 1;

		if (pagesize > 1) {
			int start = evalPageSize * pagesize;
			serialid = start - evalPageSize;
		}

		Pager pager = new Pager(list.getTotalPages(), list.getNumber(), BUTTONS_TO_SHOW);
		model.addAttribute("serialid", serialid);
		model.addAttribute("list", list);
		model.addAttribute("selectedPageSize", evalPageSize);
		model.addAttribute("pageSizes", PAGE_SIZES);
		model.addAttribute("pager", pager);
		model.addAttribute("totalRecords", list.getTotalElements());
		model.addAttribute("poNo", poNo.isPresent() ? poNo.get() : "");
		model.addAttribute("status", status.isPresent() ? status.get() : "");
		model.addAttribute("vendorName", vendorName.isPresent() ? vendorName.get() : "");
		model.addAttribute("issuedOn", issuedOn.isPresent() ? issuedOn.get() : "");
		model.addAttribute("sortingorder", sortingorder.isPresent() ? sortingorder.get() : "DESC");
		model.addAttribute("sortingkey", sortingkey.isPresent() ? sortingkey.get() : "lastModifiedOn");
		return "/main/closepo :: result-table";

	}

	////////////////////////////////////////////////// My POs
	////////////////////////////////////////////////// /////////////////////////////////////////////////////////////////////////////

	@GetMapping("/main/mypos")
	public ModelAndView onPageLoadMyPO(Model model) {

		String date_format = "dd-MM-yyyy";
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_FORMAT");

		if (dateObj != null) {
			if (!dateObj.getPropValue().isEmpty()) {
				date_format = dateObj.getPropValue();
			}
		}

		model.addAttribute("date_format", date_format);

		ModelAndView modelAndView = new ModelAndView("/main/mypos");
		return modelAndView;
	}

	@RequestMapping(value = "/main/MyPOsGrid", method = RequestMethod.GET)
	public String myPOGrid(@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page, @RequestParam("poNo") Optional<String> poNo,
			@RequestParam("status") Optional<String> status, @RequestParam("vendorName") Optional<String> vendorName,
			@RequestParam("issuedOn") Optional<String> issuedOn,
			@RequestParam("sortingkey") Optional<String> sortingkey,
			@RequestParam("sortingorder") Optional<String> sortingorder, Model model) {

		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;

		String date_format = "dd-MM-yyyy";
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_FORMAT");
		if (dateObj != null) {
			if (!dateObj.getPropValue().isEmpty()) {
				date_format = dateObj.getPropValue();
			}
		}

		Page<PurchaseOrder> list;
		List<PurchaseOrderUserAccess> pouser = poUsrAcc.getBypsIDAndAction(getCurrentUser().get(), "D");
		/*
		 * List<PurchaseOrderUserAccess> pouser =
		 * poUsrAcc.getBypsID(getCurrentUser().get());
		 */
		List<String> poNumber = new ArrayList<>();

		Calendar calendar = new GregorianCalendar();
		String calendar_year = Integer.toString(calendar.get(Calendar.YEAR));
		String logedin_year = getYear().get();

		// if loged in year is equal to current year, show list of pos from
		// current year and last year
		if (logedin_year.equals(calendar_year)) {
			for (PurchaseOrderUserAccess a : pouser) {

				String ponos_sub = a.getPurchaseorderuseraccessId().getPono().substring(5, 7);
				String calendar_year_sub = calendar_year.substring(2);
				String logedin_year_sub = logedin_year.substring(2);
				int last_year_sub = Integer.valueOf(logedin_year_sub) - 1;
				if (ponos_sub.equals(calendar_year_sub) || ponos_sub.equals(Integer.toString(last_year_sub))) {

					poNumber.add(a.getPurchaseorderuseraccessId().getPono());
				}
			}

		} else { // if it's not current year, only see the current year po list
			for (PurchaseOrderUserAccess a : pouser) {

				String ponos_sub = a.getPurchaseorderuseraccessId().getPono().substring(5, 7);

				String logedin_year_sub = logedin_year.substring(2);

				if (ponos_sub.equals(logedin_year_sub)) {

					poNumber.add(a.getPurchaseorderuseraccessId().getPono());
				}
			}

		}

		Specification<PurchaseOrder> s = new Specification<PurchaseOrder>() {
			@Override
			public Predicate toPredicate(Root<PurchaseOrder> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> ls = new ArrayList<>();
				if (poNo.isPresent() && !poNo.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("poNo")), "%" + poNo.get().toLowerCase() + "%"));
				}
				if (status.isPresent() && !status.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("status")), "%" + status.get().toLowerCase() + "%"));

				}

				if (vendorName.isPresent() && !vendorName.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("vendorName")), "%" + vendorName.get().toLowerCase() + "%"));
				}

				if (issuedOn.isPresent() && !issuedOn.get().isEmpty()) {

					Expression<String> dateStringExpr = cb.function("to_char", String.class, root.get("issuedOn"),
							cb.literal("YYYY-MM-DD HH12:MI:SS"));
					ls.add(cb.like(cb.lower(dateStringExpr), "%" + issuedOn.get().toLowerCase() + "%"));
				}

				List<Predicate> temp = new ArrayList<>();
				for (String s : poNumber) {
					temp.add(cb.equal(root.get("poNo"), s));
				}
				ls.add(cb.or(temp.toArray(new Predicate[0])));
				return cb.and(ls.toArray(new Predicate[0]));
			}
		};

		Sort sorttemp = new Sort(Sort.Direction.DESC, "lastModifiedOn");

		if (sortingkey.isPresent() && sortingorder.isPresent()) {
			sorttemp = new Sort(sortingorder.get().equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC,
					sortingkey.get());
		}
		if (poNo.isPresent() || status.isPresent() || vendorName.isPresent()) {
			list = purchaseorder.getAllPageablePurchaseOrder(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		} else {
			list = purchaseorder.getAllPageablePurchaseOrder(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		}

		int serialid = 0;

		int pagesize = evalPage + 1;

		if (pagesize > 1) {
			int start = evalPageSize * pagesize;
			serialid = start - evalPageSize;
		}

		Pager pager = new Pager(list.getTotalPages(), list.getNumber(), BUTTONS_TO_SHOW);
		model.addAttribute("serialid", serialid);
		model.addAttribute("list", list);
		model.addAttribute("selectedPageSize", evalPageSize);
		model.addAttribute("pageSizes", PAGE_SIZES);
		model.addAttribute("pager", pager);
		model.addAttribute("totalRecords", list.getTotalElements());
		model.addAttribute("poNo", poNo.isPresent() ? poNo.get() : "");
		model.addAttribute("status", status.isPresent() ? status.get() : "");
		model.addAttribute("vendorName", vendorName.isPresent() ? vendorName.get() : "");
		model.addAttribute("issuedOn", issuedOn.isPresent() ? issuedOn.get() : "");
		model.addAttribute("sortingorder", sortingorder.isPresent() ? sortingorder.get() : "DESC");
		model.addAttribute("sortingkey", sortingkey.isPresent() ? sortingkey.get() : "lastModifiedOn");
		return "/main/mypos :: result-table";

	}

	///////////////////////////////////////////////// Authorize Pos
	///////////////////////////////////////////////// //////////////////////////////////////////////////////////////////////

	@GetMapping("/main/authorizepo")
	public ModelAndView onPageLoadAuthPO(Model model) {

		String date_format = "dd-MM-yyyy";
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_FORMAT");

		if (dateObj != null) {
			if (!dateObj.getPropValue().isEmpty()) {
				date_format = dateObj.getPropValue();
			}
		}

		model.addAttribute("date_format", date_format);

		ModelAndView modelAndView = new ModelAndView("/main/authorizepo");
		return modelAndView;
	}

	@RequestMapping(value = "/main/AuthorizePoGrid", method = RequestMethod.GET)
	public String authPOGrid(@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page, @RequestParam("poNo") Optional<String> poNo,
			@RequestParam("status") Optional<String> status, @RequestParam("vendorName") Optional<String> vendorName,
			@RequestParam("issuedOn") Optional<String> issuedOn,
			@RequestParam("sortingkey") Optional<String> sortingkey,
			@RequestParam("sortingorder") Optional<String> sortingorder, Model model) {

		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;

		String date_format = "dd-MM-yyyy";
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_FORMAT");
		if (dateObj != null) {
			if (!dateObj.getPropValue().isEmpty()) {
				date_format = dateObj.getPropValue();
			}
		}

		/*
		 * Here we need to check if psid in user access is same with current
		 * user then show the verified list
		 */
		Page<PurchaseOrder> list;

		// need to fetch list of po numbers based on action A and logged in user
		List<PurchaseOrderUserAccess> pouser = poUsrAcc.getBypsIDAndAction(getCurrentUser().get(), "A");
		List<String> poNumber = new ArrayList<>();
		PurchaseOrder pos = new PurchaseOrder();
		List<String> purchaseorderlistFromPOMain = new ArrayList<>();

		/***
		 * Getting List of Po Numbers Under PSID and Authorize Action and Status
		 * from user accss and po_t_main table
		 ***/

		Calendar calendar = new GregorianCalendar();
		String calendar_year = Integer.toString(calendar.get(Calendar.YEAR));
		String logedin_year = getYear().get();

		// if loged in year is equal to current year, show list of pos from
		// current year and last year

		if (logedin_year.equals(calendar_year)) {

			for (PurchaseOrderUserAccess a : pouser) {

				String ponos_sub = a.getPurchaseorderuseraccessId().getPono().substring(5, 7);
				String calendar_year_sub = calendar_year.substring(2);
				String logedin_year_sub = logedin_year.substring(2);
				int last_year_sub = Integer.valueOf(logedin_year_sub) - 1;

				if (ponos_sub.equals(calendar_year_sub) || ponos_sub.equals(Integer.toString(last_year_sub))) {

					poNumber.add(a.getPurchaseorderuseraccessId().getPono());
					pos = purchaseorder.getPurchaseOrderByPoNo(a.getPurchaseorderuseraccessId().getPono());

					if (pos != null) {
						if (pos.getStatus().equals("Verified")) {
							purchaseorderlistFromPOMain.add(pos.getPoNo());
						}
					}
				}
			}

		} else {

			for (PurchaseOrderUserAccess a : pouser) {

				String ponos_sub = a.getPurchaseorderuseraccessId().getPono().substring(5, 7);

				String logedin_year_sub = logedin_year.substring(2);

				if (ponos_sub.equals(logedin_year_sub)) {

					poNumber.add(a.getPurchaseorderuseraccessId().getPono());
					pos = purchaseorder.getPurchaseOrderByPoNo(a.getPurchaseorderuseraccessId().getPono());

					if (pos != null) {
						if (pos.getStatus().equals("Verified")) {
							purchaseorderlistFromPOMain.add(pos.getPoNo());
						}
					}
				}
			}

		}

		/*
		 * for (PurchaseOrderUserAccess a : pouser) {
		 * 
		 * poNumber.add(a.getPurchaseorderuseraccessId().getPono()); pos =
		 * purchaseorder.getAllPurchaseOrderByPoNo(a.
		 * getPurchaseorderuseraccessId().getPono());
		 * 
		 * if(pos!=null){ if(pos.getStatus().equals("Verified")){
		 * purchaseorderlistFromPOMain.add(pos.getPoNo()); }
		 * 
		 * }
		 * 
		 * 
		 * }
		 */
		Specification<PurchaseOrder> s = new Specification<PurchaseOrder>() {
			@Override
			public Predicate toPredicate(Root<PurchaseOrder> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> ls = new ArrayList<>();
				if (poNo.isPresent() && !poNo.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("poNo")), "%" + poNo.get().toLowerCase() + "%"));
				}
				if (status.isPresent() && !status.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("status")), "%" + status.get().toLowerCase() + "%"));

				}

				if (vendorName.isPresent() && !vendorName.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("vendorName")), "%" + vendorName.get().toLowerCase() + "%"));
				}

				if (issuedOn.isPresent() && !issuedOn.get().isEmpty()) {

					Expression<String> dateStringExpr = cb.function("to_char", String.class, root.get("issuedOn"),
							cb.literal("YYYY-MM-DD HH12:MI:SS"));
					ls.add(cb.like(cb.lower(dateStringExpr), "%" + issuedOn.get().toLowerCase() + "%"));
				}

				List<Predicate> temp = new ArrayList<>();
				for (String s : purchaseorderlistFromPOMain) {
					temp.add(cb.equal(root.get("poNo"), s));
				}
				ls.add(cb.or(temp.toArray(new Predicate[0])));
				return cb.and(ls.toArray(new Predicate[0]));
			}
		};

		Sort sorttemp = new Sort(Sort.Direction.DESC, "lastModifiedOn");

		if (sortingkey.isPresent() && sortingorder.isPresent()) {
			sorttemp = new Sort(sortingorder.get().equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC,
					sortingkey.get());
		}
		if (poNo.isPresent() || status.isPresent() || vendorName.isPresent()) {
			list = purchaseorder.getAllPageablePurchaseOrder(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		} else {
			list = purchaseorder.getAllPageablePurchaseOrder(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		}

		int serialid = 0;

		int pagesize = evalPage + 1;

		if (pagesize > 1) {
			int start = evalPageSize * pagesize;
			serialid = start - evalPageSize;
		}

		Pager pager = new Pager(list.getTotalPages(), list.getNumber(), BUTTONS_TO_SHOW);
		model.addAttribute("serialid", serialid);
		model.addAttribute("list", list);
		model.addAttribute("selectedPageSize", evalPageSize);
		model.addAttribute("pageSizes", PAGE_SIZES);
		model.addAttribute("pager", pager);
		model.addAttribute("totalRecords", list.getTotalElements());
		model.addAttribute("poNo", poNo.isPresent() ? poNo.get() : "");
		model.addAttribute("status", status.isPresent() ? status.get() : "");
		model.addAttribute("vendorName", vendorName.isPresent() ? vendorName.get() : "");
		model.addAttribute("issuedOn", issuedOn.isPresent() ? issuedOn.get() : "");
		model.addAttribute("sortingorder", sortingorder.isPresent() ? sortingorder.get() : "DESC");
		model.addAttribute("sortingkey", sortingkey.isPresent() ? sortingkey.get() : "lastModifiedOn");
		return "/main/authorizepo :: result-table";

	}

	///////////////////////////////////////////////// Redraft Pos
	///////////////////////////////////////////////// //////////////////////////////////////////////////////////////////////

	@GetMapping("/main/redraftpo")
	public ModelAndView onPageLoadRedraftPO(Model model) {

		String date_format = "dd-MM-yyyy";
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_FORMAT");

		if (dateObj != null) {
			if (!dateObj.getPropValue().isEmpty()) {
				date_format = dateObj.getPropValue();
			}
		}

		model.addAttribute("date_format", date_format);

		ModelAndView modelAndView = new ModelAndView("/main/redraftpo");
		return modelAndView;
	}

	@RequestMapping(value = "/main/RedraftPoGrid", method = RequestMethod.GET)
	public String redraftPOGrid(@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page, @RequestParam("poNo") Optional<String> poNo,
			@RequestParam("status") Optional<String> status, @RequestParam("vendorName") Optional<String> vendorName,
			@RequestParam("issuedOn") Optional<String> issuedOn,
			@RequestParam("sortingkey") Optional<String> sortingkey,
			@RequestParam("sortingorder") Optional<String> sortingorder, Model model) {

		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;

		String date_format = "dd-MM-yyyy";
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_FORMAT");
		if (dateObj != null) {
			if (!dateObj.getPropValue().isEmpty()) {
				date_format = dateObj.getPropValue();
			}
		}

		Page<PurchaseOrder> list;
		/*
		 * List<PurchaseOrderUserAccess> pouser =
		 * poUsrAcc.getBypsIDAndAction("0002040", "D");
		 */
		List<PurchaseOrderUserAccess> pouser = poUsrAcc.getBypsIDAndActionForRedraft(getCurrentUser().get());
		List<String> poNumber = new ArrayList<>();
		PurchaseOrder pos = new PurchaseOrder();
		List<String> purchaseorderlistFromPOMain = new ArrayList<>();

		Calendar calendar = new GregorianCalendar();
		String calendar_year = Integer.toString(calendar.get(Calendar.YEAR));
		String logedin_year = getYear().get();

		/***
		 * Getting List of Po Numbers Under PSID and Authorize Action and Status
		 * from user accss and po_t_main table
		 ***/
		if (logedin_year.equals(calendar_year)) {
			for (PurchaseOrderUserAccess a : pouser) {

				String ponos_sub = a.getPurchaseorderuseraccessId().getPono().substring(5, 7);
				String calendar_year_sub = calendar_year.substring(2);
				String logedin_year_sub = logedin_year.substring(2);
				int last_year_sub = Integer.valueOf(logedin_year_sub) - 1;

				if (ponos_sub.equals(calendar_year_sub) || ponos_sub.equals(Integer.toString(last_year_sub))) {

					poNumber.add(a.getPurchaseorderuseraccessId().getPono());
					pos = purchaseorder.getAllPurchaseOrderByPoNo(a.getPurchaseorderuseraccessId().getPono());

					if (pos != null) {
						if (pos.getStatus().equals("Redraft")) {
							purchaseorderlistFromPOMain.add(pos.getPoNo());
						}
					}

				}

			}
		} else {

			for (PurchaseOrderUserAccess a : pouser) {

				String ponos_sub = a.getPurchaseorderuseraccessId().getPono().substring(5, 7);

				String logedin_year_sub = logedin_year.substring(2);

				if (ponos_sub.equals(logedin_year_sub)) {

					poNumber.add(a.getPurchaseorderuseraccessId().getPono());
					pos = purchaseorder.getAllPurchaseOrderByPoNo(a.getPurchaseorderuseraccessId().getPono());

					if (pos != null) {
						if (pos.getStatus().equals("Redraft")) {
							purchaseorderlistFromPOMain.add(pos.getPoNo());
						}
					}

				}

			}

		}
		Specification<PurchaseOrder> s = new Specification<PurchaseOrder>() {
			@Override
			public Predicate toPredicate(Root<PurchaseOrder> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> ls = new ArrayList<>();
				if (poNo.isPresent() && !poNo.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("poNo")), "%" + poNo.get().toLowerCase() + "%"));
				}
				if (status.isPresent() && !status.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("status")), "%" + status.get().toLowerCase() + "%"));

				}

				if (vendorName.isPresent() && !vendorName.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("vendorName")), "%" + vendorName.get().toLowerCase() + "%"));
				}

				if (issuedOn.isPresent() && !issuedOn.get().isEmpty()) {

					Expression<String> dateStringExpr = cb.function("to_char", String.class, root.get("issuedOn"),
							cb.literal("YYYY-MM-DD HH12:MI:SS"));
					ls.add(cb.like(cb.lower(dateStringExpr), "%" + issuedOn.get().toLowerCase() + "%"));
				}

				List<Predicate> temp = new ArrayList<>();
				for (String s : purchaseorderlistFromPOMain) {
					temp.add(cb.equal(root.get("poNo"), s));
				}
				ls.add(cb.or(temp.toArray(new Predicate[0])));
				return cb.and(ls.toArray(new Predicate[0]));
			}
		};

		Sort sorttemp = new Sort(Sort.Direction.DESC, "lastModifiedOn");

		if (sortingkey.isPresent() && sortingorder.isPresent()) {
			sorttemp = new Sort(sortingorder.get().equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC,
					sortingkey.get());
		}
		if (poNo.isPresent() || status.isPresent() || vendorName.isPresent()) {
			list = purchaseorder.getAllPageablePurchaseOrder(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		} else {
			list = purchaseorder.getAllPageablePurchaseOrder(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		}

		int serialid = 0;

		int pagesize = evalPage + 1;

		if (pagesize > 1) {
			int start = evalPageSize * pagesize;
			serialid = start - evalPageSize;
		}

		Pager pager = new Pager(list.getTotalPages(), list.getNumber(), BUTTONS_TO_SHOW);
		model.addAttribute("serialid", serialid);
		model.addAttribute("list", list);
		model.addAttribute("selectedPageSize", evalPageSize);
		model.addAttribute("pageSizes", PAGE_SIZES);
		model.addAttribute("pager", pager);
		model.addAttribute("totalRecords", list.getTotalElements());
		model.addAttribute("poNo", poNo.isPresent() ? poNo.get() : "");
		model.addAttribute("status", status.isPresent() ? status.get() : "");
		model.addAttribute("vendorName", vendorName.isPresent() ? vendorName.get() : "");
		model.addAttribute("issuedOn", issuedOn.isPresent() ? issuedOn.get() : "");
		model.addAttribute("sortingorder", sortingorder.isPresent() ? sortingorder.get() : "DESC");
		model.addAttribute("sortingkey", sortingkey.isPresent() ? sortingkey.get() : "lastModifiedOn");
		return "/main/redraftpo :: result-table";

	}

	///////////////////////////////////////////////// Archive Pos
	///////////////////////////////////////////////// //////////////////////////////////////////////////////////////////////

	@GetMapping("/main/poarchive")
	public ModelAndView onPageLoadArchivePO(Model model) {

		/*
		 * String date_format = "dd-MM-yyyy"; SystemParameter dateObj =
		 * sysRepo.getSystemParameterByPropName("DATE_FORMAT");
		 * 
		 * if(dateObj!=null){ if(!dateObj.getPropValue().isEmpty()){ date_format
		 * = dateObj.getPropValue(); } }
		 */

		/* model.addAttribute("date_format", date_format); */

		ModelAndView modelAndView = new ModelAndView("/main/archivepo");
		return modelAndView;
	}

	@RequestMapping(value = "/main/ArchivePoGrid", method = RequestMethod.GET)
	public String archivePOGrid(@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page, @RequestParam("poNo") Optional<String> poNo,
			@RequestParam("vendorCode") Optional<String> vendorcode,
			@RequestParam("vendorName") Optional<String> vendorname, @RequestParam("pcCode") Optional<String> pccode,
			@RequestParam("issuerPsid") Optional<String> issuer, @RequestParam("compCode") Optional<String> compcode,
			@RequestParam("sortingkey") Optional<String> sortingkey,
			@RequestParam("sortingorder") Optional<String> sortingorder, Model model) {

		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;

		/*
		 * String date_format = "dd-MM-yyyy"; SystemParameter dateObj =
		 * sysRepo.getSystemParameterByPropName("DATE_FORMAT");
		 * if(dateObj!=null){ if(!dateObj.getPropValue().isEmpty()){ date_format
		 * = dateObj.getPropValue(); } }
		 */

		Page<PurchaseOrderArchiveDetails> list;
		Specification<PurchaseOrderArchiveDetails> s = new Specification<PurchaseOrderArchiveDetails>() {
			@Override
			public Predicate toPredicate(Root<PurchaseOrderArchiveDetails> root, CriteriaQuery<?> query,
					CriteriaBuilder cb) {
				List<Predicate> ls = new ArrayList<>();
				if (poNo.isPresent() && !poNo.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("poNo")), "%" + poNo.get().toLowerCase() + "%"));
				}

				if (vendorcode.isPresent() && !vendorcode.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("vendorCode")), "%" + vendorcode.get().toLowerCase() + "%"));
				}

				if (vendorname.isPresent() && !vendorname.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("vendorName")), "%" + vendorname.get().toLowerCase() + "%"));
				}

				if (pccode.isPresent() && !pccode.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("pcCode")), "%" + pccode.get().toLowerCase() + "%"));
				}

				if (issuer.isPresent() && !issuer.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("issuerPsid")), "%" + issuer.get().toLowerCase() + "%"));
				}

				if (compcode.isPresent() && !compcode.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("compCode")), "%" + compcode.get().toLowerCase() + "%"));
				}

				return cb.and(ls.toArray(new Predicate[0]));
			}
		};

		Sort sorttemp = new Sort(Sort.Direction.DESC, "poNo");

		if (sortingkey.isPresent() && sortingorder.isPresent()) {
			sorttemp = new Sort(sortingorder.get().equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC,
					sortingkey.get());
		}
		if (poNo.isPresent() || vendorcode.isPresent() || vendorname.isPresent() || pccode.isPresent()
				|| issuer.isPresent() || compcode.isPresent()) {
			list = poarchrepo.getAllPageablePurchaseOrder(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		} else {
			list = poarchrepo.getAllPageablePurchaseOrder(new PageRequest(evalPage, evalPageSize, sorttemp));
		}

		int serialid = 0;

		int pagesize = evalPage + 1;

		if (pagesize > 1) {
			int start = evalPageSize * pagesize;
			serialid = start - evalPageSize;
		}

		Pager pager = new Pager(list.getTotalPages(), list.getNumber(), BUTTONS_TO_SHOW);
		model.addAttribute("serialid", serialid);
		model.addAttribute("list", list);
		model.addAttribute("selectedPageSize", evalPageSize);
		model.addAttribute("pageSizes", PAGE_SIZES);
		model.addAttribute("pager", pager);
		model.addAttribute("totalRecords", list.getTotalElements());
		model.addAttribute("poNo", poNo.isPresent() ? poNo.get() : "");
		model.addAttribute("issuerPsid", issuer.isPresent() ? issuer.get() : "");
		model.addAttribute("compCode", compcode.isPresent() ? compcode.get() : "");
		model.addAttribute("pcCode", pccode.isPresent() ? pccode.get() : "");
		model.addAttribute("vendorCode", vendorcode.isPresent() ? vendorcode.get() : "");
		model.addAttribute("vendorName", vendorname.isPresent() ? vendorname.get() : "");
		model.addAttribute("sortingorder", sortingorder.isPresent() ? sortingorder.get() : "DESC");
		model.addAttribute("sortingkey", sortingkey.isPresent() ? sortingkey.get() : "poNo");
		return "/main/archivepo :: result-table";

	}

	///////////////////////////////////////////////// Invoice Pos
	///////////////////////////////////////////////// //////////////////////////////////////////////////////////////////////

	@GetMapping("/main/invoicepo")
	public ModelAndView onPageLoadInvoicePO(Model model) {

		/*
		 * disable_invoice_closed=false; disable_non_invoicepo = false;
		 * disable_close_butt_closedinvoicepo=false;
		 */

		String date_format = "dd-MM-yyyy";
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_FORMAT");

		if (dateObj != null) {
			if (!dateObj.getPropValue().isEmpty()) {
				date_format = dateObj.getPropValue();
			}
		}

		model.addAttribute("date_format", date_format);
		model.addAttribute("disable_invoice", disable_invoice);
		model.addAttribute("disable_closed_invoice", disable_invoice_closed);
		model.addAttribute("disable_non_invoicepo", disable_non_invoicepo);
		model.addAttribute("disable_invoice_nonauthpo", disable_invoice_nonauthpo);
		model.addAttribute("disable_close_butt_closedinvoicepo", disable_close_butt_closedinvoicepo);
		model.addAttribute("last_invoice", last_invoice);
		/* model.addAttribute("closeexcept", closeexcept); */

		if (disable_invoice_nonauthpo) {

			model.addAttribute("disable_invoice_nonauthpo", disable_invoice_nonauthpo);
		}

		if (disable_invoice_closed) {

			model.addAttribute("disable_closed_invoice", disable_invoice_closed);
		}

		if (disable_non_invoicepo) {

			model.addAttribute("disable_non_invoicepo", disable_non_invoicepo);
		}

		if (disable_close_butt_closedinvoicepo) {

			model.addAttribute("disable_close_butt_closedinvoicepo", disable_close_butt_closedinvoicepo);
		}

		if (last_invoice) {

			model.addAttribute("last_invoice", last_invoice);
		}

		ModelAndView modelAndView = new ModelAndView("/main/invoicepo");

		return modelAndView;
	}

	@GetMapping("/main/invoicepo_close/{poNo}")
	public ModelAndView Close_onPageLoadInvoicePO(@PathVariable String poNo, Model model) {

		String date_format = "dd-MM-yyyy";
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_FORMAT");

		if (dateObj != null) {
			if (!dateObj.getPropValue().isEmpty()) {
				date_format = dateObj.getPropValue();
			}
		}

		model.addAttribute("date_format", date_format);
		model.addAttribute("disable_invoice", disable_invoice);
		model.addAttribute("closeexcept", closeexcept);
		model.addAttribute("poNumber", poNo);

		ModelAndView modelAndView = new ModelAndView("/main/close_invoicedpo_modal");
		return modelAndView;
	}

	@RequestMapping(value = "/main/InvoicePoGrid", method = RequestMethod.GET)
	public String invoicePOGrid(@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page, @RequestParam("poNo") Optional<String> poNo,
			@RequestParam("status") Optional<String> status, @RequestParam("vendorName") Optional<String> vendorName,
			@RequestParam("issuedOn") Optional<String> issuedOn,
			@RequestParam("sortingkey") Optional<String> sortingkey,
			@RequestParam("sortingorder") Optional<String> sortingorder, Model model) {
		disable_invoice_nonauthpo = false;
		disable_invoice_closed = false;
		disable_non_invoicepo = false;
		disable_close_butt_closedinvoicepo = false;
		last_invoice = false;

		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;

		String date_format = "dd-MM-yyyy";
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_FORMAT");
		if (dateObj != null) {
			if (!dateObj.getPropValue().isEmpty()) {
				date_format = dateObj.getPropValue();
			}
		}

		Page<PurchaseOrder> list;
		List<String> action_list = new ArrayList<>();
		List<PurchaseOrderUserAccess> pouser = new ArrayList<>();
		List<String> pouser_list = new ArrayList<>(); // List of pos from
														// userAccess with psid
														// and action list

		List<String> poNumberListFromUserAccess = new ArrayList<>();
		PurchaseOrder pos = new PurchaseOrder();
		List<PurchaseOrder> pos_list = new ArrayList<>();
		List<String> purchaseorderlistFromPOMain = new ArrayList<>();

		Calendar calendar = new GregorianCalendar();
		String calendar_year = Integer.toString(calendar.get(Calendar.YEAR));
		String logedin_year = getYear().get();

		action_list.add("D");
		action_list.add("V");
		action_list.add("A");
		action_list.add("I");

		if (logedin_year.equals(calendar_year)) {
			for (String action_obj : action_list) {
				pouser = poUsrAcc.getBypsIDAndAction(getCurrentUser().get(), action_obj);

				if (pouser != null) {
					for (PurchaseOrderUserAccess a : pouser) {

						String ponos_sub = a.getPurchaseorderuseraccessId().getPono().substring(5, 7);
						String calendar_year_sub = calendar_year.substring(2);
						String logedin_year_sub = logedin_year.substring(2);
						int last_year_sub = Integer.valueOf(logedin_year_sub) - 1;

						if (ponos_sub.equals(calendar_year_sub) || ponos_sub.equals(Integer.toString(last_year_sub))) {
							pouser_list.add(a.getPurchaseorderuseraccessId().getPono());
						}
					}
				}

			}

			/***
			 * Getting List of Po Numbers Under PSID and Authorize Action and
			 * Status from user accss and po_t_main table
			 ***/
			for (String a : pouser_list) {

				// poNumberListFromUserAccess.add(a);
				pos = purchaseorder.getAllPurchaseOrderByPoNo(a); // list of po
																	// obj from
																	// po main

				if (pos != null) {
					pos_list.add(pos);
					if(pos.getStatus().equals("Authorized") ||
					 pos.getStatus().equals("Invoiced") ){
						 purchaseorderlistFromPOMain.add(pos.getPoNo());
							 }
				}

				// if(pos!=null){
				// if(pos.getStatus().equals("Authorized") ||
				// pos.getStatus().equals("Invoiced") ){
				// purchaseorderlistFromPOMain.add(pos.getPoNo());
				// }

				// }

			}

		} else {
			for (String action_obj : action_list) {
				pouser = poUsrAcc.getBypsIDAndAction(getCurrentUser().get(), action_obj);

				if (pouser != null) {
					for (PurchaseOrderUserAccess a : pouser) {

						String ponos_sub = a.getPurchaseorderuseraccessId().getPono().substring(5, 7);

						String logedin_year_sub = logedin_year.substring(2);

						if (ponos_sub.equals(logedin_year_sub)) {
							pouser_list.add(a.getPurchaseorderuseraccessId().getPono());
						}
					}
				}

			}

			/***
			 * Getting List of Po Numbers Under PSID and Authorize Action and
			 * Status from user accss and po_t_main table
			 ***/
			for (String a : pouser_list) {

				// poNumberListFromUserAccess.add(a);
				pos = purchaseorder.getAllPurchaseOrderByPoNo(a); // list of po
																	// obj from
																	// po main

				if (pos != null) {
					pos_list.add(pos);
					if(pos.getStatus().equals("Authorized") ||
							 pos.getStatus().equals("Invoiced") ){
							 purchaseorderlistFromPOMain.add(pos.getPoNo());
							 }
				}

				// if(pos!=null){
				// if(pos.getStatus().equals("Authorized") ||
				// pos.getStatus().equals("Invoiced") ){
				// purchaseorderlistFromPOMain.add(pos.getPoNo());
				// }

				// }

			}

		}

		/*
		 * List<String> poNumberListFromUserAccess = new ArrayList<>();
		 * PurchaseOrder pos = new PurchaseOrder(); List<PurchaseOrder> pos_list
		 * = new ArrayList<>(); List<String> purchaseorderlistFromPOMain = new
		 * ArrayList<>();
		 * 
		 * for (String a : pouser_list) {
		 * 
		 * pos = purchaseorder.getAllPurchaseOrderByPoNo(a);
		 * 
		 * if(pos!=null){ pos_list.add(pos); }
		 * 
		 * if(pos!=null){ if(pos.getStatus().equals("Authorized") ||
		 * pos.getStatus().equals("Invoiced") ){
		 * purchaseorderlistFromPOMain.add(pos.getPoNo()); }
		 * 
		 * }
		 * 
		 * 
		 * }
		 */

		/*
		 * if(pos_list!=null){ for(PurchaseOrder po_obj : pos_list){
		 * 
		 * if(po_obj.getStatus().equals("Authorized")){
		 * 
		 * disable_invoice =false; }else{
		 * 
		 * disable_invoice = true; } }
		 * 
		 * }
		 */
		/*
		 * if(pos!=null && !pos.getStatus().equals("Authorized")){
		 * 
		 * disable_invoice = true; }else{
		 * 
		 * disable_invoice =false; }
		 */
		Specification<PurchaseOrder> s = new Specification<PurchaseOrder>() {
			@Override
			public Predicate toPredicate(Root<PurchaseOrder> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> ls = new ArrayList<>();
				if (poNo.isPresent() && !poNo.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("poNo")), "%" + poNo.get().toLowerCase() + "%"));
				}
				if (status.isPresent() && !status.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("status")), "%" + status.get().toLowerCase() + "%"));

				}

				if (vendorName.isPresent() && !vendorName.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("vendorName")), "%" + vendorName.get().toLowerCase() + "%"));
				}

				if (issuedOn.isPresent() && !issuedOn.get().isEmpty()) {

					Expression<String> dateStringExpr = cb.function("to_char", String.class, root.get("issuedOn"),
							cb.literal("YYYY-MM-DD HH12:MI:SS"));
					ls.add(cb.like(cb.lower(dateStringExpr), "%" + issuedOn.get().toLowerCase() + "%"));
				}

				List<Predicate> temp = new ArrayList<>();
				for (String s : purchaseorderlistFromPOMain) {

					temp.add(cb.equal(root.get("poNo"), s));

				}
				ls.add(cb.or(temp.toArray(new Predicate[0])));

				return cb.and(ls.toArray(new Predicate[0]));
			}
		};

		Sort sorttemp = new Sort(Sort.Direction.DESC, "lastModifiedOn");

		if (sortingkey.isPresent() && sortingorder.isPresent()) {
			sorttemp = new Sort(sortingorder.get().equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC,
					sortingkey.get());
		}
		if (poNo.isPresent() || status.isPresent() || vendorName.isPresent()) {
			list = purchaseorder.getAllPageablePurchaseOrder(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		} else {
			list = purchaseorder.getAllPageablePurchaseOrder(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		}

		int serialid = 0;

		int pagesize = evalPage + 1;

		if (pagesize > 1) {
			int start = evalPageSize * pagesize;
			serialid = start - evalPageSize;
		}

		Pager pager = new Pager(list.getTotalPages(), list.getNumber(), BUTTONS_TO_SHOW);
		model.addAttribute("serialid", serialid);
		model.addAttribute("list", list);
		model.addAttribute("selectedPageSize", evalPageSize);
		model.addAttribute("pageSizes", PAGE_SIZES);
		model.addAttribute("pager", pager);
		model.addAttribute("totalRecords", list.getTotalElements());
		model.addAttribute("poNo", poNo.isPresent() ? poNo.get() : "");
		model.addAttribute("status", status.isPresent() ? status.get() : "");
		model.addAttribute("vendorName", vendorName.isPresent() ? vendorName.get() : "");
		model.addAttribute("issuedOn", issuedOn.isPresent() ? issuedOn.get() : "");
		model.addAttribute("sortingorder", sortingorder.isPresent() ? sortingorder.get() : "DESC");
		model.addAttribute("sortingkey", sortingkey.isPresent() ? sortingkey.get() : "lastModifiedOn");
		return "/main/invoicepo :: result-table";

	}

	@GetMapping(value = "/main/viewPOtoInvoice")
	public String button_invoice(@RequestParam("poNo") String poNo, @RequestParam("status") String status, Model model,
			RedirectAttributes redirectAttributes) {
		PurchaseOrder po = purchaseorder.getAllPurchaseOrderByPoNo(poNo);

		String date_format = "dd-MM-yyyy";
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_FORMAT");
		if (dateObj != null) {
			if (!dateObj.getPropValue().isEmpty()) {
				date_format = dateObj.getPropValue();
			}
		}

		if (status.equals("Authorized") || status.equals("Invoiced")) {
			InvoiceForm obj = new InvoiceForm();

			obj.setPoNo(poNo);
			obj.setPoAmount(po.getTotalInclGST());
			obj.setInvoiceAmount(calculateTotInvoiceAmount(poNo));
			obj.setBalance(calculateBalance(poNo, calculateTotInvoiceAmount(poNo)));

			model.addAttribute("poNo", poNo);
			model.addAttribute("totInvoiceAmount", calculateTotInvoiceAmount(poNo));
			model.addAttribute("poAmount", po.getTotalInclGST());
			model.addAttribute("date_format", date_format);
			model.addAttribute("invoiceobj", obj);
			model.addAttribute("invoiceobject", tempobj != null ? tempobj : new InvoiceForm());
			return "/main/accept_invoice";

		} else {

			redirectAttributes.addFlashAttribute("error", "Invoice in status: " + status + " is Not Allowed");
			return "redirect:/main/invoicepo";
		}

		/*
		 * else if(status.equals("Issued")){ disable_invoice_nonauthpo=true;
		 * redirectAttributes.addFlashAttribute("error",
		 * "Invoice in status: "+status + " is Not Allowed"); return
		 * "redirect:/main/invoicepo"; }else if(status.equals("Verified")){
		 * disable_invoice_nonauthpo=true;
		 * redirectAttributes.addFlashAttribute("error",
		 * "Invoice in status: "+status +" is Not Allowed"); return
		 * "redirect:/main/invoicepo"; }
		 * 
		 * else if(status.equals("Closed")){ disable_invoice_closed=true;
		 * redirectAttributes.addFlashAttribute("error",
		 * "Invoice in status: "+status +" is Not Allowed"); return
		 * "redirect:/main/invoicepo"; }
		 */
		// return "redirect:/main/invoicepo";
	}

	@GetMapping(value = "/main/view_Invoices/{poNo}")
	public String view_Invoices(@PathVariable String poNo, Model model, RedirectAttributes redirectAttributes) {
		PurchaseOrder po = purchaseorder.getAllPurchaseOrderByPoNo(poNo);

		System.out.println("po in invoice " + poNo);
		String date_format = "dd-MM-yyyy";
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_FORMAT");
		if (dateObj != null) {
			if (!dateObj.getPropValue().isEmpty()) {
				date_format = dateObj.getPropValue();
			}
		}

		InvoiceForm obj = new InvoiceForm();

		obj.setPoNo(poNo);
		obj.setPoAmount(po.getTotalInclGST());
		obj.setInvoiceAmount(calculateTotInvoiceAmount(poNo));
		obj.setBalance(calculateBalance(poNo, calculateTotInvoiceAmount(poNo)));

		model.addAttribute("poNo", poNo);
		model.addAttribute("totInvoiceAmount", calculateTotInvoiceAmount(poNo));
		model.addAttribute("poAmount", po.getTotalInclGST());
		model.addAttribute("date_format", date_format);
		model.addAttribute("invoiceobj", obj);

		model.addAttribute("invoiceobject", tempobj != null ? tempobj : new InvoiceForm());
		return "/main/accept_invoice";

	}

	@GetMapping(value = "/main/viewPOtoClose/{poNo}")
	public String close_button_Invoice(@PathVariable String poNo, Model model, RedirectAttributes redirectAttributes) {
		poNumber = poNo;
		InvoiceForm obj = new InvoiceForm();
		String date_format = "dd-MM-yyyy";
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_FORMAT");
		if (dateObj != null) {
			if (!dateObj.getPropValue().isEmpty()) {
				date_format = dateObj.getPropValue();
			}
		}

		PurchaseOrder po = purchaseorder.getAllPurchaseOrderByPoNo(poNo);

		if (po != null && !po.getStatus().equals("Invoiced")) {
			disable_non_invoicepo = true;
			if (po.getStatus().equals("Closed")) {

				disable_close_butt_closedinvoicepo = true;
			}
			// redirectAttributes.addFlashAttribute("error", "To Close the
			// Purchase Order,Status should be Invoiced.");
			return "redirect:/main/invoicepo";
		}

		List<Invoice> poinvoice = invoiceRepo.getInvoiceByPONo(poNo);

		if (poinvoice != null && po.getStatus().equals("Invoiced")) {

			obj.setPoNo(poNo);
			obj.setPoAmount(po.getTotalInclGST());
			obj.setInvoiceAmount(calculateTotInvoiceAmount(poNo));
			obj.setBalance(calculateBalance(poNo, calculateTotInvoiceAmount(poNo)));

			if (po.getTotalInclGST() != obj.getInvoiceAmount()) {
				closeexcept = true;

			} else {
				closeexcept = false;
			}
			model.addAttribute("poNo", poNo);
			model.addAttribute("totInvoiceAmount", calculateTotInvoiceAmount(poNo));
			model.addAttribute("poAmount", po.getTotalInclGST());
			model.addAttribute("date_format", date_format);
			model.addAttribute("invoiceobj", obj);
			model.addAttribute("invoiceobject", tempobj != null ? tempobj : new InvoiceForm());
		} else {
			redirectAttributes.addFlashAttribute("error",
					"Match At Least One Invoice Againts the Selected Purchase Order!");
			return "redirect:/main/invoicepo";

		}
		return "redirect:/main/invoicepo_close/" + poNo;

	}

	@RequestMapping(value = "/main/close_exception", method = RequestMethod.GET)
	@ResponseBody
	public String do_close_invoice_with_exception(@RequestParam("close_exception") Optional<String> close_exception,

			@RequestParam("pono_path") Optional<String> po_number, RedirectAttributes redirectAttributes, Model model) {
		MDC.put("sessionId",
				AbstractMainController.getCompany().isPresent() ? AbstractMainController.getCompany().get() : "");
		PurchaseOrder po_obj = purchaseorder.getPurchaseOrderByPoNo(po_number.get());
		po_obj.setCloseException(close_exception.get());
		po_obj.setStatus("Closed");
		po_obj.setLastModifiedOn(new Date());

		try {
			purchaseorder.createPurchaseOrder(po_obj);
			PurchaseOrderSendMailUtil email = new PurchaseOrderSendMailUtil(po_obj.getPoNo(), getCompany().get());
			email.closePurchaseOrderWithException();
			auditService.createAudit(new Date(), " Purchase Order: " + po_obj.getPoNo() + " Closed successfully",
					getCurrentUser().get(), " Closing Purchase Order", getCompany().get(), po_obj.getPcCode());
		} catch (POException e) {
			LOGGER.error(e);
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return close_exception.get() + po_number.get();

	}

	@PostMapping(value = "/main/acceptInvoice")
	public String save_Invoice(@Valid @ModelAttribute("invoiceobject") InvoiceForm obj, BindingResult result,
			Model model, RedirectAttributes redirectAttributes) throws ParseException {
		MDC.put("sessionId",
				AbstractMainController.getCompany().isPresent() ? AbstractMainController.getCompany().get() : "");
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_FORMAT");
		String date_format = "dd-MM-yyyy";
		if (dateObj != null) {
			if (!dateObj.getPropValue().isEmpty()) {
				date_format = dateObj.getPropValue();
			}
		}
		DateFormat formatter = new SimpleDateFormat(date_format);

		if (!result.hasErrors() && obj.getPoNo() != null
				&& invoiceRepo.getInvoiceByPoInvoiceNo(obj.getPoNo() + obj.getInvoiceNumber()) == null) {
			PurchaseOrder po_obj = purchaseorder.getPurchaseOrderByPoNo(obj.getPoNo());
			Invoice ad = new Invoice();
			ad.setInvoiceNo(obj.getInvoiceNumber());
			ad.setAmount(obj.getInvoiceAmount());
			ad.setInvoiceDate(formatter.parse(obj.getStartDate()));
			ad.setInvoiceNote(obj.getInvoicenote());
			ad.setRecipient(obj.getInvoiceRecipient());
			ad.setPoInvoiceNo(obj.getPoInvoiceNumber());
			ad.setPurchaseOrder(obj.getPoNo());

			try {
				invoiceRepo.createInvoice(ad);
				po_obj.setStatus("Invoiced");
				po_obj.setLastModifiedOn(new Date());
				purchaseorder.createPurchaseOrder(po_obj);
				PurchaseOrderSendMailUtil email = new PurchaseOrderSendMailUtil(po_obj.getPoNo(), getCompany().get());
				email.invoicePurchaseOrder();
				auditService.createAudit(new Date(), "Invoice: " + obj.getInvoiceNumber() + " created successfully. ",
						getCurrentUser().get(), " Created Invoice",

						getCompany().get(), po_obj.getPcCode());
				redirectAttributes.addFlashAttribute("info", "Invoice created successfully");
				tempobj = null;
			} catch (POException e) {
				LOGGER.error(e);
			} catch (Exception e) {
				LOGGER.error(e);
			}
		} else {
			redirectAttributes.addFlashAttribute("error", "This Invoice Number already exists");
			tempobj = obj;
		}
		return "redirect:/main/view_Invoices/" + obj.getPoNo();

	}

	@RequestMapping(value = "/main/AcceptInvoicePoGrid", method = RequestMethod.GET)
	public String save_InvoicePOGrid(@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page, @RequestParam("invoiceNo") Optional<String> invoiceno,
			@RequestParam("purchaseOrder") String pono, @RequestParam("poInvoiceNo") Optional<String> invoicepono,
			@RequestParam("invoiceDate") Optional<String> invoicedate,
			@RequestParam("recipient") Optional<String> invoicerecep,
			@RequestParam("amount") Optional<Double> invoiceamount,

			@RequestParam("sortingkey") Optional<String> sortingkey,
			@RequestParam("sortingorder") Optional<String> sortingorder, Model model) {

		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;

		String date_format = "dd-MM-yyyy";
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_FORMAT");
		if (dateObj != null) {
			if (!dateObj.getPropValue().isEmpty()) {
				date_format = dateObj.getPropValue();
			}
		}

		Page<Invoice> list;

		Specification<Invoice> s = new Specification<Invoice>() {
			@Override
			public Predicate toPredicate(Root<Invoice> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> ls = new ArrayList<>();
				if (invoiceno.isPresent() && !invoiceno.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("invoiceNo")), "%" + invoiceno.get().toLowerCase() + "%"));
				}
				if (invoicepono.isPresent() && !invoicepono.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("poInvoiceNo")), "%" + invoicepono.get().toLowerCase() + "%"));

				}

				if (invoicerecep.isPresent() && !invoicerecep.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("recipient")), "%" + invoicerecep.get().toLowerCase() + "%"));
				}

				if (invoiceamount.isPresent()) {
					ls.add(cb.equal(root.get("amount"), invoiceamount.get()));
				}

				if (invoicedate.isPresent() && !invoicedate.get().isEmpty()) {

					Expression<String> dateStringExpr = cb.function("to_char", String.class, root.get("invoiceDate"),
							cb.literal("YYYY-MM-DD"));
					ls.add(cb.like(cb.lower(dateStringExpr), "%" + invoicedate.get().toLowerCase() + "%"));
				}

				List<Predicate> temp = new ArrayList<>();

				temp.add(cb.equal(root.get("purchaseOrder"), pono));

				ls.add(cb.or(temp.toArray(new Predicate[0])));

				return cb.and(ls.toArray(new Predicate[0]));
			}
		};

		Sort sorttemp = new Sort(Sort.Direction.DESC, "invoiceDate");

		if (sortingkey.isPresent() && sortingorder.isPresent()) {
			sorttemp = new Sort(sortingorder.get().equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC,
					sortingkey.get());
		}
		if (invoiceno.isPresent() || invoicepono.isPresent() || invoicedate.isPresent() || invoicerecep.isPresent()
				|| invoiceamount.isPresent()) {
			list = invoiceRepo.getAllPagableInvoice(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		} else {
			list = invoiceRepo.getAllPagableInvoice(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		}
		Pager pager = new Pager(list.getTotalPages(), list.getNumber(), BUTTONS_TO_SHOW);
		model.addAttribute("list", list);
		model.addAttribute("selectedPageSize", evalPageSize);
		model.addAttribute("pageSizes", PAGE_SIZES);
		model.addAttribute("pager", pager);
		model.addAttribute("totalRecords", list.getTotalElements());
		model.addAttribute("invoiceNo", invoiceno.isPresent() ? invoiceno.get() : "");
		model.addAttribute("poInvoiceNo", invoicepono.isPresent() ? invoicepono.get() : "");
		model.addAttribute("recipient", invoicerecep.isPresent() ? invoicerecep.get() : "");
		model.addAttribute("amount", invoiceamount.isPresent() ? invoiceamount.get() : "");
		model.addAttribute("invoiceDate", invoicedate.isPresent() ? invoicedate.get() : "");
		model.addAttribute("sortingorder", sortingorder.isPresent() ? sortingorder.get() : "DESC");
		model.addAttribute("sortingkey", sortingkey.isPresent() ? sortingkey.get() : "invoiceDate");
		model.addAttribute("date_format", date_format);
		return "/main/accept_invoice :: result-table";

	}

	private double calculateTotInvoiceAmount(String poNo) {

		List<Invoice> invoiceList = invoiceRepo.getInvoiceByPONo(poNo);

		double totalInvoiceAmount = 0;
		double invoice = 0;
		double credit = 0;
		double debit = 0;

		for (Invoice invoiceobj : invoiceList) {

			if (invoiceobj.getInvoiceNote().contains("CN")) {

				credit += invoiceobj.getAmount();

			} else if (invoiceobj.getInvoiceNote().contains("DN")) {

				debit += invoiceobj.getAmount();
			} else {

				invoice += invoiceobj.getAmount();

			}
		}

		totalInvoiceAmount = invoice + credit - debit;
		return totalInvoiceAmount;
	}

	private double calculateBalance(String poNo, double totalInvoiceAmount) {

		PurchaseOrder po = purchaseorder.getAllPurchaseOrderByPoNo(poNo);
		double balance = 0;

		balance = po.getTotalInclGST() - totalInvoiceAmount;

		return balance;
	}

	@GetMapping(value = "/main/deleteinvoice/{poInvoiceNo}")
	public String deleteInvoice(@PathVariable String poInvoiceNo, Model model, RedirectAttributes redirectAttributes) {

		Invoice ad = invoiceRepo.getInvoiceByPoInvoiceNo(poInvoiceNo);
		List<Invoice> ad_poNo = invoiceRepo.getInvoiceByPONo(ad.getPurchaseOrder());
		if (ad != null) {

			// If all invoices related to that PO deleted, Status change to
			// Authorized
			if (ad_poNo.size() == 1) {
				PurchaseOrder po_obj = purchaseorder.getPurchaseOrderByPoNo(ad.getPurchaseOrder());
				po_obj.setStatus("Authorized");
				po_obj.setLastModifiedOn(new Date());
				invoiceRepository.delete(ad);
				last_invoice = true;

				// redirectAttributes.addFlashAttribute("info", "All Invoices
				// deleted");
				return "redirect:/main/invoicepo";

			}

			invoiceRepository.delete(ad);
			redirectAttributes.addFlashAttribute("info", "Invoice Number Deleted successfully");

		}

		return "redirect:/main/view_Invoices/" + ad.getPurchaseOrder();
		// return "redirect:/main/accept_invoice";
	}

	@Override
	protected String getHeaderTitle() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @return the profitCenterCode
	 */
	public ProfitCenter getProfitCenterCode() {
		return profitCenterCode;
	}

	/**
	 * @param profitCenterCode
	 *            the profitCenterCode to set
	 */
	public void setProfitCenterCode(ProfitCenter profitCenterCode) {
		this.profitCenterCode = profitCenterCode;
	}
}