package my.com.parkson.ntpoms.pomain.web.models;

import java.util.Date;

import my.com.parkson.ntpoms.main.entities.VendorBranch;

public class VendorCrtForm {

	
	
	private String  createdby;
	
	private String 	createdon ;
	
	private String  lastmodifiedby ;
	
	private String	lastmodifiedon ;
	
	private String  deactivatedby ;
	
	private String  	deactivatedon ;
	
	private String  reactivatedby ;
	
	private String reactivatedon ;
	
	private String isActive;	
	
	private String vendorcode ;	
	
	private String vendorbranchcode ;	
	
	private String credittermscode;

	private String compCode;
	
	private String vendCodevendBCode;
	
	VendorBranch vendbranch = new VendorBranch();
	
	
	public String toString () {
		vendCodevendBCode = vendorcode +" - "+"-" +vendorbranchcode;
		System.out.println(vendCodevendBCode);
		return vendCodevendBCode;
	}
	

	public String getVendCodevendBCode() {
		return vendCodevendBCode;
	}


	public void setVendCodevendBCode(String vendCodevendBCode) {
		this.vendCodevendBCode = vendCodevendBCode;
	}


	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	

	public String getLastmodifiedby() {
		return lastmodifiedby;
	}

	public void setLastmodifiedby(String lastmodifiedby) {
		this.lastmodifiedby = lastmodifiedby;
	}

	

	public String getDeactivatedby() {
		return deactivatedby;
	}

	public void setDeactivatedby(String deactivatedby) {
		this.deactivatedby = deactivatedby;
	}



	public String getReactivatedby() {
		return reactivatedby;
	}

	public void setReactivatedby(String reactivatedby) {
		this.reactivatedby = reactivatedby;
	}



	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getVendorcode() {
		return vendorcode;
	}

	public void setVendorcode(String vendorcode) {
		this.vendorcode = vendorcode;
	}

	public String getVendorbranchcode() {
		return vendorbranchcode;
	}

	public void setVendorbranchcode(String vendorbranchcode) {
		this.vendorbranchcode = vendorbranchcode;
	}

	public String getCredittermscode() {
		return credittermscode;
	}

	public void setCredittermscode(String credittermscode) {
		this.credittermscode = credittermscode;
	}

	public String getcompCode() {
		return compCode;
	}

	public void setcompCode(String mf11_compCode) {
		this.compCode = mf11_compCode;
	}

	public String getCreatedon() {
		return createdon;
	}

	public void setCreatedon(String createdon) {
		this.createdon = createdon;
	}

	public String getLastmodifiedon() {
		return lastmodifiedon;
	}

	public void setLastmodifiedon(String lastmodifiedon) {
		this.lastmodifiedon = lastmodifiedon;
	}

	public String getDeactivatedon() {
		return deactivatedon;
	}

	public void setDeactivatedon(String deactivatedon) {
		this.deactivatedon = deactivatedon;
	}

	public String getReactivatedon() {
		return reactivatedon;
	}

	public void setReactivatedon(String reactivatedon) {
		this.reactivatedon = reactivatedon;
	}
	
	
	
}
