package my.com.parkson.ntpoms.pomain.web.models;

import java.io.Serializable;
import java.util.Date;

/**
 * @author moham
 *
 */
public class PoMainForm implements Serializable {
		
	
	private static final long serialVersionUID = 1L;

	private String poNo;
	
	private String status;

	private String quoteNo;

	private String quoteDate;

	private String pcCode; 
	
	private String contactPerson;

	private String contactPersonName; 

	private String contactPersonPhone;

	private String contactPersonEmail;

	private String addressLine1;

	private String addressLine2;

	private String addressLine3;

	private String postCode;

	private String postArea;
	
	private String compCode;
	
	private String termsAndConditionsPortrait;
	
	private String termsAndConditionsLandscape;
	
	private String storeName;
	
	private String departmentName;

	private String vendorCode; 
	
	private String vendorbranchCode; 
	
	private String vendorName; 

	private String vendorPersonInCharge;

	private String vendorPersonInChargePhone;

	private String vendorPersonInChargeEmail;

	private String vendorAddressLine1;

	private String vendorAddressLine2;

	private String vendorAddressLine3;

	private String vendorPostCode;

	private String vendorPostArea;

	private String credittermscode;

	private String currency;

	private String ordCatCode; 

	private String orderCategoryAuthorizationGrid;

	private String ordSubCCode; 

	private String prTypeCode;

	private String paymentTerms;

	private String deliveryInstruction;

	private double totInclGST; 

	private double totExclGST;

	private double totGST;

	private String issuer;

	private String issuedOn; 

	private String verifier; 

	private String verifiedOn;

	private String verifyRemark;

	private String authorizer; 

	private String authorizedOn;

	private String authorizeRemark;

	private String lastPrintedBy;

	private String lastPrintedOn;

	private String closeException;

	private String redraftRemark;

	private String rejectRemark;
	
	private String deleteRemark;
	
	private String cancelRemark;

	private String firstApprover;
	
	private String firstApproverDesignation;

	private String secondApprover;
	
	private String secondApproverDesignation;

	private String thirdApprover;
	
	private String thirdApproverDesignation;
	
	private String expireDate;
	
	private String expiryStatus;
	
	private int expiryDays;
	
	private double overAllDiscount;
	
	private double overAllDiscountPercent;
	
	private String projectCode;
	
	private String issuerRemark;
	
	
	private String deletedBy;
	
	private String deletedOn;
	
	private String rejectedBy;
	
	private String rejectedOn;
	
	private String closedBy;
	
	private String closedOn;
	
	private String cancelledBy;
	
	private String cancelledOn;
	
	private String lastmodifiedon;
	
	private int fetchResults;
	
	private double totInclGSTMax;

	private double totInclGSTMin;
	
	private String activetab = "tab1";

	private String activetabform = "issuer";

	private String activetaboperation = "save";
	
	private String lastcrt_used;
	
	private String lastcrt_used_hidden;
	
	private String quot_No;
	
	private String quot_Date;
	
	private String profitcenter;
	
	private String invoicer;
	
	private String forward_verifier;
	
	private String forward_authorizer;
	
	private String forward_verifiedpo_invoicer;
	
	
	
	
	
	
	public String getLastcrt_used_hidden() {
		return lastcrt_used_hidden;
	}


	public void setLastcrt_used_hidden(String lastcrt_used_hidden) {
		this.lastcrt_used_hidden = lastcrt_used_hidden;
	}


	public String getForward_verifiedpo_invoicer() {
		return forward_verifiedpo_invoicer;
	}


	public void setForward_verifiedpo_invoicer(String forward_verifiedpo_invoicer) {
		this.forward_verifiedpo_invoicer = forward_verifiedpo_invoicer;
	}


	public String getForward_authorizer() {
		return forward_authorizer;
	}


	public void setForward_authorizer(String forward_authorizer) {
		this.forward_authorizer = forward_authorizer;
	}


	public String getForward_verifier() {
		return forward_verifier;
	}


	public void setForward_verifier(String forward_verifier) {
		this.forward_verifier = forward_verifier;
	}


	public String getInvoicer() {
		return invoicer;
	}


	public void setInvoicer(String invoicer) {
		this.invoicer = invoicer;
	}


	public int getFetchResults() {
		return fetchResults;
	}


	public void setFetchResults(int fetchResults) {
		this.fetchResults = fetchResults;
	}


	public double getTotInclGSTMax() {
		return totInclGSTMax;
	}


	public void setTotInclGSTMax(double totInclGSTMax) {
		this.totInclGSTMax = totInclGSTMax;
	}


	public double getTotInclGSTMin() {
		return totInclGSTMin;
	}


	public void setTotInclGSTMin(double totInclGSTMin) {
		this.totInclGSTMin = totInclGSTMin;
	}


	public int getfetchResults() {
		return fetchResults;
	}

	
	public void setfetchResults(int fetchResults) {
		
		this.fetchResults = fetchResults;
		
	}
	
	
	public String getPoNo() {
		return poNo;
	}

	public void setPoNo(String poNo) {
		this.poNo = poNo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getQuoteNo() {
		return quoteNo;
	}

	public void setQuoteNo(String quoteNo) {
		this.quoteNo = quoteNo;
	}

	

	public String getPcCode() {
		return pcCode;
	}

	public void setPcCode(String pcCode) {
		this.pcCode = pcCode;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getContactPersonName() {
		return contactPersonName;
	}

	public void setContactPersonName(String contactPersonName) {
		this.contactPersonName = contactPersonName;
	}

	public String getContactPersonPhone() {
		return contactPersonPhone;
	}

	public void setContactPersonPhone(String contactPersonPhone) {
		this.contactPersonPhone = contactPersonPhone;
	}

	public String getContactPersonEmail() {
		return contactPersonEmail;
	}

	public void setContactPersonEmail(String contactPersonEmail) {
		this.contactPersonEmail = contactPersonEmail;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getAddressLine3() {
		return addressLine3;
	}

	public void setAddressLine3(String addressLine3) {
		this.addressLine3 = addressLine3;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	public String getPostArea() {
		return postArea;
	}

	public void setPostArea(String postArea) {
		this.postArea = postArea;
	}

	public String getCompCode() {
		return compCode;
	}

	public void setCompCode(String compCode) {
		this.compCode = compCode;
	}

	public String getTermsAndConditionsPortrait() {
		return termsAndConditionsPortrait;
	}

	public void setTermsAndConditionsPortrait(String termsAndConditionsPortrait) {
		this.termsAndConditionsPortrait = termsAndConditionsPortrait;
	}

	public String getTermsAndConditionsLandscape() {
		return termsAndConditionsLandscape;
	}

	public void setTermsAndConditionsLandscape(String termsAndConditionsLandscape) {
		this.termsAndConditionsLandscape = termsAndConditionsLandscape;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public String getVendorCode() {
		return vendorCode;
	}

	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}

	public String getVendorbranchCode() {
		return vendorbranchCode;
	}

	public void setVendorbranchCode(String vendorbranchCode) {
		this.vendorbranchCode = vendorbranchCode;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public String getVendorPersonInCharge() {
		return vendorPersonInCharge;
	}

	public void setVendorPersonInCharge(String vendorPersonInCharge) {
		this.vendorPersonInCharge = vendorPersonInCharge;
	}

	public String getVendorPersonInChargePhone() {
		return vendorPersonInChargePhone;
	}

	public void setVendorPersonInChargePhone(String vendorPersonInChargePhone) {
		this.vendorPersonInChargePhone = vendorPersonInChargePhone;
	}

	public String getVendorPersonInChargeEmail() {
		return vendorPersonInChargeEmail;
	}

	public void setVendorPersonInChargeEmail(String vendorPersonInChargeEmail) {
		this.vendorPersonInChargeEmail = vendorPersonInChargeEmail;
	}

	public String getVendorAddressLine1() {
		return vendorAddressLine1;
	}

	public void setVendorAddressLine1(String vendorAddressLine1) {
		this.vendorAddressLine1 = vendorAddressLine1;
	}

	public String getVendorAddressLine2() {
		return vendorAddressLine2;
	}

	public void setVendorAddressLine2(String vendorAddressLine2) {
		this.vendorAddressLine2 = vendorAddressLine2;
	}

	public String getVendorAddressLine3() {
		return vendorAddressLine3;
	}

	public void setVendorAddressLine3(String vendorAddressLine3) {
		this.vendorAddressLine3 = vendorAddressLine3;
	}

	public String getVendorPostCode() {
		return vendorPostCode;
	}

	public void setVendorPostCode(String vendorPostCode) {
		this.vendorPostCode = vendorPostCode;
	}

	public String getVendorPostArea() {
		return vendorPostArea;
	}

	public void setVendorPostArea(String vendorPostArea) {
		this.vendorPostArea = vendorPostArea;
	}

	public String getCredittermscode() {
		return credittermscode;
	}

	public void setCredittermscode(String credittermscode) {
		this.credittermscode = credittermscode;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getOrdCatCode() {
		return ordCatCode;
	}

	public void setOrdCatCode(String ordCatCode) {
		this.ordCatCode = ordCatCode;
	}

	public String getOrderCategoryAuthorizationGrid() {
		return orderCategoryAuthorizationGrid;
	}

	public void setOrderCategoryAuthorizationGrid(String orderCategoryAuthorizationGrid) {
		this.orderCategoryAuthorizationGrid = orderCategoryAuthorizationGrid;
	}

	public String getOrdSubCCode() {
		return ordSubCCode;
	}

	public void setOrdSubCCode(String ordSubCCode) {
		this.ordSubCCode = ordSubCCode;
	}

	public String getPrTypeCode() {
		return prTypeCode;
	}

	public void setPrTypeCode(String prTypeCode) {
		this.prTypeCode = prTypeCode;
	}

	public String getPaymentTerms() {
		return paymentTerms;
	}

	public void setPaymentTerms(String paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	public String getDeliveryInstruction() {
		return deliveryInstruction;
	}

	public void setDeliveryInstruction(String deliveryInstruction) {
		this.deliveryInstruction = deliveryInstruction;
	}

	public double getTotInclGST() {
		return totInclGST;
	}

	public void setTotInclGST(double totInclGST) {
		this.totInclGST = totInclGST;
	}

	public double getTotExclGST() {
		return totExclGST;
	}

	public void setTotExclGST(double totExclGST) {
		this.totExclGST = totExclGST;
	}

	public double getTotGST() {
		return totGST;
	}

	public void setTotGST(double totGST) {
		this.totGST = totGST;
	}

	public String getIssuer() {
		return issuer;
	}

	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}

	

	public String getIssuedOn() {
		return issuedOn;
	}


	public void setIssuedOn(String issuedOn) {
		this.issuedOn = issuedOn;
	}


	public String getVerifier() {
		return verifier;
	}

	public void setVerifier(String verifier) {
		this.verifier = verifier;
	}

	

	public String getVerifyRemark() {
		return verifyRemark;
	}

	public void setVerifyRemark(String verifyRemark) {
		this.verifyRemark = verifyRemark;
	}

	public String getAuthorizer() {
		return authorizer;
	}

	public void setAuthorizer(String authorizer) {
		this.authorizer = authorizer;
	}

	

	public String getAuthorizeRemark() {
		return authorizeRemark;
	}

	public void setAuthorizeRemark(String authorizeRemark) {
		this.authorizeRemark = authorizeRemark;
	}

	public String getLastPrintedBy() {
		return lastPrintedBy;
	}

	public void setLastPrintedBy(String lastPrintedBy) {
		this.lastPrintedBy = lastPrintedBy;
	}

	

	public String getCloseException() {
		return closeException;
	}

	public void setCloseException(String closeException) {
		this.closeException = closeException;
	}

	public String getRedraftRemark() {
		return redraftRemark;
	}

	public void setRedraftRemark(String redraftRemark) {
		this.redraftRemark = redraftRemark;
	}

	public String getRejectRemark() {
		return rejectRemark;
	}

	public void setRejectRemark(String rejectRemark) {
		this.rejectRemark = rejectRemark;
	}

	public String getDeleteRemark() {
		return deleteRemark;
	}

	public void setDeleteRemark(String deleteRemark) {
		this.deleteRemark = deleteRemark;
	}

	public String getCancelRemark() {
		return cancelRemark;
	}

	public void setCancelRemark(String cancelRemark) {
		this.cancelRemark = cancelRemark;
	}

	public String getFirstApprover() {
		return firstApprover;
	}

	public void setFirstApprover(String firstApprover) {
		this.firstApprover = firstApprover;
	}

	public String getFirstApproverDesignation() {
		return firstApproverDesignation;
	}

	public void setFirstApproverDesignation(String firstApproverDesignation) {
		this.firstApproverDesignation = firstApproverDesignation;
	}

	public String getSecondApprover() {
		return secondApprover;
	}

	public void setSecondApprover(String secondApprover) {
		this.secondApprover = secondApprover;
	}

	public String getSecondApproverDesignation() {
		return secondApproverDesignation;
	}

	public void setSecondApproverDesignation(String secondApproverDesignation) {
		this.secondApproverDesignation = secondApproverDesignation;
	}

	public String getThirdApprover() {
		return thirdApprover;
	}

	public void setThirdApprover(String thirdApprover) {
		this.thirdApprover = thirdApprover;
	}

	public String getThirdApproverDesignation() {
		return thirdApproverDesignation;
	}

	public void setThirdApproverDesignation(String thirdApproverDesignation) {
		this.thirdApproverDesignation = thirdApproverDesignation;
	}

	

	public String getExpiryStatus() {
		return expiryStatus;
	}

	public void setExpiryStatus(String expiryStatus) {
		this.expiryStatus = expiryStatus;
	}

	public int getExpiryDays() {
		return expiryDays;
	}

	public void setExpiryDays(int expiryDays) {
		this.expiryDays = expiryDays;
	}

	public double getOverAllDiscount() {
		return overAllDiscount;
	}

	public void setOverAllDiscount(double overAllDiscount) {
		this.overAllDiscount = overAllDiscount;
	}

	public String getProjectCode() {
		return projectCode;
	}

	public void setProjectCode(String projectCode) {
		this.projectCode = projectCode;
	}

	public String getIssuerRemark() {
		return issuerRemark;
	}

	public void setIssuerRemark(String issuerRemark) {
		this.issuerRemark = issuerRemark;
	}

	public String getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(String deletedBy) {
		this.deletedBy = deletedBy;
	}

	

	public String getRejectedBy() {
		return rejectedBy;
	}

	public void setRejectedBy(String rejectedBy) {
		this.rejectedBy = rejectedBy;
	}

	
	public String getClosedBy() {
		return closedBy;
	}

	public void setClosedBy(String closedBy) {
		this.closedBy = closedBy;
	}

	

	public String getCancelledBy() {
		return cancelledBy;
	}

	public void setCancelledBy(String cancelledBy) {
		this.cancelledBy = cancelledBy;
	}

	

	

	public String getActivetab() {
		return activetab;
	}


	public void setActivetab(String activetab) {
		this.activetab = activetab;
	}


	public String getActivetabform() {
		return activetabform;
	}


	public void setActivetabform(String activetabform) {
		this.activetabform = activetabform;
	}


	public String getActivetaboperation() {
		return activetaboperation;
	}


	public void setActivetaboperation(String activetaboperation) {
		this.activetaboperation = activetaboperation;
	}


	public String getLastcrt_used() {
		return lastcrt_used;
	}


	public void setLastcrt_used(String lastcrt_used) {
		this.lastcrt_used = lastcrt_used;
	}


	public String getQuoteDate() {
		return quoteDate;
	}


	public void setQuoteDate(String quoteDate) {
		this.quoteDate = quoteDate;
	}


	public String getVerifiedOn() {
		return verifiedOn;
	}


	public void setVerifiedOn(String verifiedOn) {
		this.verifiedOn = verifiedOn;
	}


	public String getAuthorizedOn() {
		return authorizedOn;
	}


	public void setAuthorizedOn(String authorizedOn) {
		this.authorizedOn = authorizedOn;
	}


	public String getLastPrintedOn() {
		return lastPrintedOn;
	}


	public void setLastPrintedOn(String lastPrintedOn) {
		this.lastPrintedOn = lastPrintedOn;
	}


	public String getExpireDate() {
		return expireDate;
	}


	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}


	public String getDeletedOn() {
		return deletedOn;
	}


	public void setDeletedOn(String deletedOn) {
		this.deletedOn = deletedOn;
	}


	public String getRejectedOn() {
		return rejectedOn;
	}


	public void setRejectedOn(String rejectedOn) {
		this.rejectedOn = rejectedOn;
	}


	public String getClosedOn() {
		return closedOn;
	}


	public void setClosedOn(String closedOn) {
		this.closedOn = closedOn;
	}


	public String getCancelledOn() {
		return cancelledOn;
	}


	public void setCancelledOn(String cancelledOn) {
		this.cancelledOn = cancelledOn;
	}


	public String getLastmodifiedon() {
		return lastmodifiedon;
	}


	public void setLastmodifiedon(String lastmodifiedon) {
		this.lastmodifiedon = lastmodifiedon;
	}


	public String getQuot_No() {
		return quot_No;
	}


	public void setQuot_No(String quot_No) {
		this.quot_No = quot_No;
	}


	public String getQuot_Date() {
		return quot_Date;
	}


	public void setQuot_Date(String quot_Date) {
		this.quot_Date = quot_Date;
	}


	public String getProfitcenter() {
		return profitcenter;
	}


	public void setProfitcenter(String profitcenter) {
		this.profitcenter = profitcenter;
	}


	public double getOverAllDiscountPercent() {
		return overAllDiscountPercent;
	}


	public void setOverAllDiscountPercent(double overAllDiscountPercent) {
		this.overAllDiscountPercent = overAllDiscountPercent;
	}
	
	

}
