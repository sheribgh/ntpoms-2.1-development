package my.com.parkson.ntpoms.pomain.web.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.Authenticator;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.apache.velocity.tools.generic.DateTool;
import org.apache.velocity.tools.generic.NumberTool;
import org.springframework.beans.factory.annotation.Autowired;

import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.entities.EmployeeMaster;
import my.com.parkson.ntpoms.common.entities.SystemParameter;
import my.com.parkson.ntpoms.common.model.EmailCriteria;
import my.com.parkson.ntpoms.common.services.EmailService;
import my.com.parkson.ntpoms.common.services.EmployeeMasterService;
import my.com.parkson.ntpoms.common.services.SystemParameterService;
import my.com.parkson.ntpoms.main.entities.Invoice;
import my.com.parkson.ntpoms.main.entities.PurchaseOrder;
import my.com.parkson.ntpoms.main.entities.VendorBranch;
import my.com.parkson.ntpoms.main.entities.VendorBranchId;
import my.com.parkson.ntpoms.main.services.AccessLogsService;
import my.com.parkson.ntpoms.main.services.AuditsService;
import my.com.parkson.ntpoms.main.services.InvoiceService;
import my.com.parkson.ntpoms.main.services.PurchaseOrderService;
import my.com.parkson.ntpoms.main.services.VendorBranchService;
import my.com.parkson.ntpoms.pomain.web.controllers.AbstractMainController;
import my.com.parkson.ntpoms.utils.ApplicationContextProvider;
import my.com.parkson.ntpoms.utils.EmailServiceUtils;

public class PurchaseOrderSendMailUtil {

	private VendorBranch vendor = new VendorBranch();
	private EmployeeMaster verifier = new EmployeeMaster();
	private EmployeeMaster issuer = new EmployeeMaster();
	private EmployeeMaster authorizer = new EmployeeMaster();
	private SystemParameter sysParam = new SystemParameter();
	private PurchaseOrder poActual = new PurchaseOrder();
	private List<Invoice> invoices = new ArrayList<>();
	private PurchaseOrderActivitiesUtil purchaseOrderLifeCycle = new PurchaseOrderActivitiesUtil();

	private String company;
	private static final Logger LOGGER = Logger.getLogger(PurchaseOrderSendMailUtil.class);

	@Autowired
	private AuditsService auditService;

	@Autowired
	private VendorBranchService vendorService;

	@Autowired
	private PurchaseOrderService poMainService;

	@Autowired
	private EmployeeMasterService employeeService;

	@Autowired
	private InvoiceService invoicesService;

	@Autowired
	private SystemParameterService sysParamService;

	@Autowired
	private EmailService emailService;

	public PurchaseOrderSendMailUtil(String poNo, String company) {
		MDC.put("sessionId",
				AbstractMainController.getCompany().isPresent() ? AbstractMainController.getCompany().get() : "");
		if (emailService == null) {
			emailService = ApplicationContextProvider.getContext().getBean(EmailService.class);
		}
		if (poMainService == null) {
			poMainService = ApplicationContextProvider.getContext().getBean(PurchaseOrderService.class);
		}
		if (auditService == null) {
			auditService = ApplicationContextProvider.getContext().getBean(AuditsService.class);
		}

		if (vendorService == null) {
			vendorService = ApplicationContextProvider.getContext().getBean(VendorBranchService.class);
		}
		if (employeeService == null) {
			employeeService = ApplicationContextProvider.getContext().getBean(EmployeeMasterService.class);
		}
		if (invoicesService == null) {
			invoicesService = ApplicationContextProvider.getContext().getBean(InvoiceService.class);
		}
		if (sysParamService == null) {
			sysParamService = ApplicationContextProvider.getContext().getBean(SystemParameterService.class);
		}

		this.poActual = poMainService.getPurchaseOrderByPoNo(poNo);
		VendorBranchId vendorBranchId = new VendorBranchId(this.poActual.getVendorcode(),
				this.poActual.getVendorbranchcode(), this.poActual.getCompCode());
		this.vendor = vendorService.getVendorBranchbyVendorId(vendorBranchId);
		this.verifier = employeeService.getEmployeeMasterByPsId(this.poActual.getVerifier());
		this.issuer = employeeService.getEmployeeMasterByPsId(this.poActual.getIssuer());
		this.authorizer = employeeService.getEmployeeMasterByPsId(this.poActual.getAuthorizer());
		this.invoices = invoicesService.getInvoiceByPurchaseOrderNo(this.poActual.getPoNo());
		this.sysParam = sysParamService.getSystemParameterByPropName("SysUrl");
		this.company = company;
	}

	public void submitPurchaseOrder() {
		try {

			if (validate(this.verifier.getEmail())) {
				Map<String, String> context = setCommonItemsAttributes();
				context.put("verifier_name", this.verifier.getName());
				context.put("po_vendor_name", this.vendor.getName());
				context.put("issuer_name", this.issuer.getName());
				Map<String, List> contextlist = new HashMap<>();
				contextlist.put("purchadrOrderList", new ArrayList<>());
				contextlist.put("invoiceList", invoices);
				Set<String> cc = new HashSet<>();

				if (validate(this.issuer.getEmail()))
					cc.add(this.issuer.getEmail());
				Set<String> attachment = new HashSet<String>();
				EmailCriteria emailObj = new EmailCriteria();
				emailObj.setRecipient(this.verifier.getEmail());
				emailObj.setSubject("Non-Trade Purchase Order - PO pending verification");
				emailObj.setCc(cc);
				emailObj.setAttachment(attachment);
				Properties props = EmailServiceUtils.loadEmailPropertyFile();
				emailObj.setTemplateName(props.getProperty("mail.vm.template.posubmit"));
				emailObj.setEmailUser(props.getProperty("mail.user"));
				emailObj.setEmailPwd(props.getProperty("mail.passwd"));
				Session session = Session.getInstance(props, new Authenticator() {
					@Override
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(emailObj.getEmailUser(), emailObj.getEmailPwd());
					}
				});
				emailService.sendEmail(session, context, contextlist, emailObj);
				LOGGER.debug(this.issuer.getName() + " have submit a PO to be issued.");
				auditService.putAudit(new Date(), "" + this.issuer.getName() + " have submit a PO to be issued.",
						this.issuer.getName(), "PO Submit");
			}
		} catch (POException | MessagingException | IOException e) {
			LOGGER.error(e);
		}
	}

	public void verifyPurchaseOrder() {
		try {
			if (validate(this.authorizer.getEmail())) {
				Map<String, String> context = setCommonItemsAttributes();
				context.put("authorizer_name", this.authorizer.getName());
				context.put("po_vendor_name", this.vendor.getName());
				context.put("issuer_name", this.issuer.getName());
				Map<String, List> contextlist = new HashMap<>();
				contextlist.put("purchadrOrderList", new ArrayList<>());
				contextlist.put("invoiceList", new ArrayList<>());
				Set<String> cc = new HashSet<>();

				if (validate(this.verifier.getEmail())) {
					cc.add(verifier.getEmail());
				}

				if (validate(this.issuer.getEmail())) {
					cc.add(issuer.getEmail());
				}
				Set<String> attachment = new HashSet<>();
				EmailCriteria emailObj = new EmailCriteria();
				emailObj.setRecipient(this.authorizer.getEmail());
				emailObj.setSubject("Non-Trade Purchase Order - PO pending verification");
				emailObj.setCc(cc);
				emailObj.setAttachment(attachment);
				Properties props = EmailServiceUtils.loadEmailPropertyFile();
				emailObj.setTemplateName(props.getProperty("mail.vm.template.poverify"));
				emailObj.setEmailUser(props.getProperty("mail.user"));
				emailObj.setEmailPwd(props.getProperty("mail.passwd"));
				Session session = Session.getInstance(props, new Authenticator() {
					@Override
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(emailObj.getEmailUser(), emailObj.getEmailPwd());
					}
				});
				emailService.sendEmail(session, context, contextlist, emailObj);
				LOGGER.debug(this.issuer.getName() + " have submit a PO to be verified.");
				auditService.putAudit(new Date(), "" + this.issuer.getName() + " have submit a PO to be verified.",
						this.issuer.getName(), "PO Submit");
			}
		} catch (POException | MessagingException | IOException e) {
			LOGGER.error(e);
		}
	}

	public void authorizedPurchaseOrder() {
		try {
			if (validate(this.authorizer.getEmail())) {
				Map<String, String> context = setCommonItemsAttributes();
				context.put("authorizer_name", this.authorizer.getName());
				context.put("po_vendor_name", this.vendor.getName());
				context.put("issuer_name", this.issuer.getName());
				Map<String, List> contextlist = new HashMap<>();
				contextlist.put("purchadrOrderList", new ArrayList<>());
				contextlist.put("invoiceList", new ArrayList<>());
				Set<String> cc = new HashSet<>();

				if (validate(verifier.getEmail())) {
					cc.add(verifier.getEmail());
				}

				if (validate(issuer.getEmail())) {
					cc.add(issuer.getEmail());
				}
				Set<String> attachment = new HashSet<>();
				EmailCriteria emailObj = new EmailCriteria();
				emailObj.setRecipient(this.authorizer.getEmail());
				emailObj.setSubject("Non-Trade Purchase Order - PO pending authorization");
				emailObj.setCc(cc);
				emailObj.setAttachment(attachment);
				Properties props = EmailServiceUtils.loadEmailPropertyFile();
				emailObj.setTemplateName(props.getProperty("mail.vm.template.poauthorize"));
				emailObj.setEmailUser(props.getProperty("mail.user"));
				emailObj.setEmailPwd(props.getProperty("mail.passwd"));
				Session session = Session.getInstance(props, new Authenticator() {
					@Override
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(emailObj.getEmailUser(), emailObj.getEmailPwd());
					}
				});
				emailService.sendEmail(session, context, contextlist, emailObj);
				LOGGER.debug(this.issuer.getName() + " have submit a PO to be authorized.");
				auditService.putAudit(new Date(), "" + this.issuer.getName() + " have submit a PO to be authorized.",
						this.issuer.getName(), "PO Submit");
			}
		} catch (POException | MessagingException | IOException e) {
			LOGGER.error(e);
		}
	}

	public void invoicePurchaseOrder() {
		try {
			if (validate(this.issuer.getEmail())) {
				Map<String, String> context = setCommonItemsAttributes();
				if (AbstractMainController.getCurrentUser().isPresent()) {
					EmployeeMaster currentUser = employeeService
							.getEmployeeMasterByPsId(AbstractMainController.getCurrentUser().get());
					if (currentUser != null) {
						context.put("invoice_recipient", currentUser.getName());
					}

				}

				double tot_invoice = calculateTotInvoiceAmount(poActual.getPoNo());
				context.put("po_vendor_name", this.vendor.getName());
				context.put("issuer_name", this.issuer.getName());
				context.put("po_total", "" + poActual.getTotalInclGST());
				context.put("po_total_invoice", "" + tot_invoice);
				context.put("po_balance", "" + calculateBalance(poActual.getPoNo(), tot_invoice));
				if (this.invoices != null) {
					for (int i = 0; i < this.invoices.size(); i++) {
						Invoice invoice = this.invoices.get(i);
						context.put("invoice_number_" + i, invoice.getInvoiceNo());
						context.put("invoice_date_" + i, "" + invoice.getInvoiceDate());
						context.put("invoice_value_" + i, "" + invoice.getAmount());
					}
				}
				Map<String, List> contextlist = new HashMap<>();
				contextlist.put("purchadrOrderList", new ArrayList<>());
				contextlist.put("invoiceList", this.invoices);

				Set<String> cc = new HashSet<>();

				if (validate(this.verifier.getEmail())) {
					cc.add(verifier.getEmail());
				}

				cc.add(issuer.getEmail());

				if (validate(this.authorizer.getEmail())) {
					cc.add(authorizer.getEmail());
				}
				Set<String> attachment = new HashSet<>();
				EmailCriteria emailObj = new EmailCriteria();

				emailObj.setRecipient(this.issuer.getEmail());
				emailObj.setSubject("Non-Trade Purchase Order - PO Invoice Notification");
				emailObj.setCc(cc);
				emailObj.setAttachment(attachment);
				Properties props = EmailServiceUtils.loadEmailPropertyFile();
				emailObj.setTemplateName(props.getProperty("mail.vm.template.poinvoice"));
				emailObj.setEmailUser(props.getProperty("mail.user"));
				emailObj.setEmailPwd(props.getProperty("mail.passwd"));
				Session session = Session.getInstance(props, new Authenticator() {
					@Override
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(emailObj.getEmailUser(), emailObj.getEmailPwd());
					}
				});
				emailService.sendEmail(session, context, contextlist, emailObj);
				LOGGER.debug(this.issuer.getName() + " have submit a PO to be invoiced.");
				auditService.putAudit(new Date(), "" + this.issuer.getName() + " have submit a PO to be invoiced.",
						this.issuer.getName(), "PO Submit");
			}
		} catch (POException | MessagingException | IOException e) {
			LOGGER.error(e);
		}
	}

	private double calculateTotInvoiceAmount(String poNo) {

		List<Invoice> invoiceList = invoicesService.getInvoiceByPONo(poNo);

		double totalInvoiceAmount = 0;
		double invoice = 0;
		double credit = 0;
		double debit = 0;

		for (Invoice invoiceobj : invoiceList) {

			if (invoiceobj.getInvoiceNote().contains("CN")) {

				credit += invoiceobj.getAmount();

			} else if (invoiceobj.getInvoiceNote().contains("DN")) {

				debit += invoiceobj.getAmount();
			} else {

				invoice += invoiceobj.getAmount();

			}
		}

		totalInvoiceAmount = invoice + credit - debit;
		return totalInvoiceAmount;
	}

	private double calculateBalance(String poNo, double totalInvoiceAmount) {

		PurchaseOrder po = poMainService.getAllPurchaseOrderByPoNo(poNo);
		double balance = 0;

		balance = po.getTotalInclGST() - totalInvoiceAmount;

		return balance;
	}

	public void closePurchaseOrder() {
		try {

			Map<String, String> context = setCommonItemsAttributes();
			context.put("authorizer_name", this.authorizer.getName());
			context.put("po_vendor_name", this.vendor.getName());
			if (AbstractMainController.getCurrentUser().isPresent()) {
				EmployeeMaster currentUser = employeeService
						.getEmployeeMasterByPsId(AbstractMainController.getCurrentUser().get());
				if (currentUser != null && currentUser.getName() != null) {
					context.put("issuer_name", currentUser.getName());
				}

			}

			Map<String, List> contextlist = new HashMap<>();
			contextlist.put("purchadrOrderList", new ArrayList<>());
			contextlist.put("invoiceList", this.invoices);
			Set<String> cc = new HashSet<>();

			if (validate(this.verifier.getEmail()))
				cc.add(verifier.getEmail());
			if (validate(this.issuer.getEmail()))
				cc.add(issuer.getEmail());

			if (validate(this.authorizer.getEmail()))
				cc.add(authorizer.getEmail());
			Set<String> attachment = new HashSet<>();
			EmailCriteria emailObj = new EmailCriteria();
			emailObj.setRecipient(this.issuer.getEmail());
			emailObj.setSubject("Non-Trade Purchase Order - Authorization");
			emailObj.setCc(cc);
			emailObj.setAttachment(attachment);
			Properties props = EmailServiceUtils.loadEmailPropertyFile();
			emailObj.setTemplateName(props.getProperty("mail.vm.template.poauthorize"));
			emailObj.setEmailUser(props.getProperty("mail.user"));
			emailObj.setEmailPwd(props.getProperty("mail.passwd"));
			Session session = Session.getInstance(props, new Authenticator() {
				@Override
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(emailObj.getEmailUser(), emailObj.getEmailPwd());
				}
			});
			emailService.sendEmail(session, context, contextlist, emailObj);
			LOGGER.debug(this.issuer.getName() + " have submit a PO to be verified.");
			auditService.putAudit(new Date(), "" + this.issuer.getName() + " have submit a PO to be verified.",
					this.issuer.getName(), "PO Submit");
		} catch (POException | MessagingException | IOException e) {
			LOGGER.error(e);
		}
	}

	public void closeExceptionPurchaseOrder() {
		try {
			Map<String, String> context = setCommonItemsAttributes();
			context.put("authorizer_name", this.authorizer.getName());
			context.put("po_vendor_name", this.vendor.getName());
			context.put("issuer_name", this.issuer.getName());
			Map<String, List> contextlist = new HashMap<>();
			contextlist.put("purchadrOrderList", new ArrayList<>());
			contextlist.put("invoiceList", this.invoices);
			Set<String> cc = new HashSet<>();
			cc.add(verifier.getEmail());
			cc.add(issuer.getEmail());
			cc.add(authorizer.getEmail());
			Set<String> attachment = new HashSet<>();
			EmailCriteria emailObj = new EmailCriteria();
			emailObj.setRecipient(this.verifier.getEmail());
			emailObj.setSubject("Non-Trade Purchase Order - Authorization");
			emailObj.setCc(cc);
			emailObj.setAttachment(attachment);
			Properties props = EmailServiceUtils.loadEmailPropertyFile();
			emailObj.setTemplateName(props.getProperty("mail.vm.template.pocloseexception"));
			emailObj.setEmailUser(props.getProperty("mail.user"));
			emailObj.setEmailPwd(props.getProperty("mail.passwd"));
			Session session = Session.getInstance(props, new Authenticator() {
				@Override
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(emailObj.getEmailUser(), emailObj.getEmailPwd());
				}
			});
			emailService.sendEmail(session, context, contextlist, emailObj);
			LOGGER.debug(this.issuer.getName() + " have submit a PO to be verified.");
			auditService.putAudit(new Date(), "" + this.issuer.getName() + " have submit a PO to be verified.",
					this.issuer.getName(), "PO Submit");
		} catch (POException | MessagingException | IOException e) {
			LOGGER.error(e);
		}
	}

	public void cancelPurchaseOrder() {
		try {
			if (validate(this.issuer.getEmail())) {
				Map<String, String> context = setCommonItemsAttributes();
				context.put("canceller_name", authorizer.getName());
				context.put("po_vendor_name", this.vendor.getName());
				context.put("po_cancelled_reason", poActual.getCancelRemark());
				Map<String, List> contextlist = new HashMap<>();
				contextlist.put("purchadrOrderList", new ArrayList<>());
				contextlist.put("invoiceList", new ArrayList<>());

				Set<String> cc = new HashSet<>();
				if (validate(verifier.getEmail())) {
					cc.add(verifier.getEmail());
				}
				if (AbstractMainController.getCurrentUser().isPresent()) {
					EmployeeMaster currentUser = employeeService
							.getEmployeeMasterByPsId(AbstractMainController.getCurrentUser().get());
					if (currentUser != null && validate(currentUser.getEmail())) {
						cc.add(currentUser.getEmail());
					}

				}

				if (validate(authorizer.getEmail())) {
					cc.add(authorizer.getEmail());
				}
				Set<String> attachment = new HashSet<>();
				EmailCriteria emailObj = new EmailCriteria();
				emailObj.setRecipient(this.issuer.getEmail());
				emailObj.setSubject("Non-Trade Purchase Order - PO cancellation");
				emailObj.setCc(cc);
				emailObj.setAttachment(attachment);
				Properties props = EmailServiceUtils.loadEmailPropertyFile();
				emailObj.setTemplateName(props.getProperty("mail.vm.template.pocancel"));
				emailObj.setEmailUser(props.getProperty("mail.user"));
				emailObj.setEmailPwd(props.getProperty("mail.passwd"));
				Session session = Session.getInstance(props, new Authenticator() {
					@Override
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(emailObj.getEmailUser(), emailObj.getEmailPwd());
					}
				});
				emailService.sendEmail(session, context, contextlist, emailObj);
				LOGGER.debug(this.issuer.getName() + " have submit a PO to be cancelled.");
				auditService.putAudit(new Date(), "" + this.issuer.getName() + " have submit a PO to be cancelled.",
						this.issuer.getName(), "PO Submit");
			}
		} catch (POException | MessagingException | IOException e) {
			LOGGER.error(e);
		}
	}

	public void deletePurchaseOrder() {
		try {
			if (validate(this.issuer.getEmail())) {
				Map<String, String> context = setCommonItemsAttributes();

				if (AbstractMainController.getCurrentUser().isPresent()) {
					EmployeeMaster currentUser = employeeService
							.getEmployeeMasterByPsId(AbstractMainController.getCurrentUser().get());
					if (currentUser != null) {
						context.put("deleter_name", currentUser.getName());
					}

				}

				context.put("po_vendor_name", this.vendor.getName());
				context.put("po_delete_reason", poActual.getDeleteRemark());
				Map<String, List> contextlist = new HashMap<>();
				contextlist.put("purchadrOrderList", new ArrayList<>());
				contextlist.put("invoiceList", new ArrayList<>());
				Set<String> cc = new HashSet<>();

				if (this.verifier.getEmail() != null) {
					cc.add(this.verifier.getEmail());
				}

				if (this.issuer.getEmail() != null) {
					cc.add(this.issuer.getEmail());
				}

				if (this.authorizer.getEmail() != null) {
					cc.add(this.authorizer.getEmail());
				}
				Set<String> attachment = new HashSet<>();
				EmailCriteria emailObj = new EmailCriteria();
				emailObj.setRecipient(this.issuer.getEmail());
				emailObj.setSubject("Non-Trade Purchase Order - PO Delete");
				emailObj.setCc(cc);
				emailObj.setAttachment(attachment);
				Properties props = EmailServiceUtils.loadEmailPropertyFile();
				emailObj.setTemplateName(props.getProperty("mail.vm.template.podelete"));
				emailObj.setEmailUser(props.getProperty("mail.user"));
				emailObj.setEmailPwd(props.getProperty("mail.passwd"));
				Session session = Session.getInstance(props, new Authenticator() {
					@Override
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(emailObj.getEmailUser(), emailObj.getEmailPwd());
					}
				});
				emailService.sendEmail(session, context, contextlist, emailObj);
				LOGGER.debug(this.issuer.getName() + " have submit a PO to be deleted.");
				auditService.putAudit(new Date(), "" + this.issuer.getName() + " have submit a PO to be deleted.",
						this.issuer.getName(), "PO Submit");
			}
		} catch (POException | MessagingException | IOException e) {
			LOGGER.error(e);
		}
	}

	public void forwardPurchaseOrder() {
		try {
			if (validate(this.issuer.getEmail())) {
				Map<String, String> context = setCommonItemsAttributes();
				context.put("to_name", verifier.getName());

				if (AbstractMainController.getCurrentUser().isPresent()) {
					EmployeeMaster currentUser = employeeService
							.getEmployeeMasterByPsId(AbstractMainController.getCurrentUser().get());
					if (currentUser != null) {
						context.put("from_name", currentUser.getName());
					}

				}

				context.put("po_vendor_name", this.vendor.getName());
				context.put("po_delete_reason", poActual.getDeleteRemark());
				Map<String, List> contextlist = new HashMap<>();
				contextlist.put("purchadrOrderList", new ArrayList<>());
				contextlist.put("invoiceList", new ArrayList<>());
				Set<String> cc = new HashSet<>();

				if (this.verifier.getEmail() != null) {
					cc.add(verifier.getEmail());
				}
				cc.add(issuer.getEmail());

				if (this.authorizer.getEmail() != null) {
					cc.add(authorizer.getEmail());
				}
				Set<String> attachment = new HashSet<>();
				EmailCriteria emailObj = new EmailCriteria();
				emailObj.setRecipient(this.issuer.getEmail());
				emailObj.setSubject("Non-Trade Purchase Order - PO Forward");
				emailObj.setCc(cc);
				emailObj.setAttachment(attachment);
				Properties props = EmailServiceUtils.loadEmailPropertyFile();
				emailObj.setTemplateName(props.getProperty("mail.vm.template.poforward"));
				emailObj.setEmailUser(props.getProperty("mail.user"));
				emailObj.setEmailPwd(props.getProperty("mail.passwd"));
				Session session = Session.getInstance(props, new Authenticator() {
					@Override
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(emailObj.getEmailUser(), emailObj.getEmailPwd());
					}
				});
				emailService.sendEmail(session, context, contextlist, emailObj);
				LOGGER.debug(this.issuer.getName() + " have submit a PO to be verified.");
				auditService.putAudit(new Date(), "" + this.issuer.getName() + " have submit a PO to be verified.",
						this.issuer.getName(), "PO Submit");
			}
		} catch (POException | MessagingException | IOException e) {
			LOGGER.error(e);
		}
	}

	public void forwardToAuthorizerPurchaseOrder() {
		try {
			if (validate(this.issuer.getEmail())) {
				Map<String, String> context = setCommonItemsAttributes();
				context.put("to_name", authorizer.getName());

				if (AbstractMainController.getCurrentUser().isPresent()) {
					EmployeeMaster currentUser = employeeService
							.getEmployeeMasterByPsId(AbstractMainController.getCurrentUser().get());
					if (currentUser != null) {
						context.put("from_name", currentUser.getName());
					}

				}

				context.put("po_vendor_name", this.vendor.getName());
				context.put("po_delete_reason", poActual.getDeleteRemark());
				Map<String, List> contextlist = new HashMap<>();
				contextlist.put("purchadrOrderList", new ArrayList<>());
				contextlist.put("invoiceList", new ArrayList<>());
				Set<String> cc = new HashSet<>();

				if (this.verifier.getEmail() != null) {
					cc.add(verifier.getEmail());
				}
				cc.add(issuer.getEmail());

				if (this.authorizer.getEmail() != null) {
					cc.add(authorizer.getEmail());
				}
				Set<String> attachment = new HashSet<>();
				EmailCriteria emailObj = new EmailCriteria();
				emailObj.setRecipient(this.issuer.getEmail());
				emailObj.setSubject("Non-Trade Purchase Order - PO Forward");
				emailObj.setCc(cc);
				emailObj.setAttachment(attachment);
				Properties props = EmailServiceUtils.loadEmailPropertyFile();
				emailObj.setTemplateName(props.getProperty("mail.vm.template.poforward"));
				emailObj.setEmailUser(props.getProperty("mail.user"));
				emailObj.setEmailPwd(props.getProperty("mail.passwd"));
				Session session = Session.getInstance(props, new Authenticator() {
					@Override
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(emailObj.getEmailUser(), emailObj.getEmailPwd());
					}
				});
				emailService.sendEmail(session, context, contextlist, emailObj);
				LOGGER.debug(this.issuer.getName() + " have submit a PO to be authorized.");
				auditService.putAudit(new Date(), "" + this.issuer.getName() + " have submit a PO to be authorized.",
						this.issuer.getName(), "PO Submit");
			}
		} catch (POException | MessagingException | IOException e) {
			LOGGER.error(e);
		}
	}

	public void redraftPurchaseOrder() {
		try {
			if (validate(this.issuer.getEmail())) {
				Map<String, String> context = setCommonItemsAttributes();

				if (AbstractMainController.getCurrentUser().isPresent()) {
					EmployeeMaster currentUser = employeeService
							.getEmployeeMasterByPsId(AbstractMainController.getCurrentUser().get());
					if (currentUser != null) {
						context.put("redraft_name", currentUser.getName());
					}

				}

				context.put("po_vendor_name", this.vendor.getName());
				context.put("po_redraft_reason", poActual.getRedraftRemark());
				Map<String, List> contextlist = new HashMap<>();
				contextlist.put("purchadrOrderList", new ArrayList<>());
				contextlist.put("invoiceList", new ArrayList<>());
				Set<String> cc = new HashSet<>();

				if (this.verifier.getEmail() != null) {
					cc.add(verifier.getEmail());
				}
				cc.add(issuer.getEmail());

				if (this.authorizer.getEmail() != null) {
					cc.add(authorizer.getEmail());
				}
				Set<String> attachment = new HashSet<>();
				EmailCriteria emailObj = new EmailCriteria();
				emailObj.setRecipient(this.issuer.getEmail());
				emailObj.setSubject("Non-Trade Purchase Order - PO Redraft");
				emailObj.setCc(cc);
				emailObj.setAttachment(attachment);
				Properties props = EmailServiceUtils.loadEmailPropertyFile();
				emailObj.setTemplateName(props.getProperty("mail.vm.template.poredraft"));
				emailObj.setEmailUser(props.getProperty("mail.user"));
				emailObj.setEmailPwd(props.getProperty("mail.passwd"));
				Session session = Session.getInstance(props, new Authenticator() {
					@Override
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(emailObj.getEmailUser(), emailObj.getEmailPwd());
					}
				});
				emailService.sendEmail(session, context, contextlist, emailObj);
				LOGGER.debug(this.issuer.getName() + " have submit a PO to be verified.");
				auditService.putAudit(new Date(), "" + this.issuer.getName() + " have submit a PO to be verified.",
						this.issuer.getName(), "PO Submit");
			}
		} catch (POException | MessagingException | IOException e) {
			LOGGER.error(e);
		}
	}

	@SuppressWarnings("unchecked")
	public void rejectPurchaseOrder() {
		try {
			if (validate(this.issuer.getEmail())) {
				Map<String, String> context = setCommonItemsAttributes();

				if (AbstractMainController.getCurrentUser().isPresent()) {
					EmployeeMaster currentUser = employeeService
							.getEmployeeMasterByPsId(AbstractMainController.getCurrentUser().get());
					if (currentUser != null) {
						context.put("rejector_name", currentUser.getName());
					}

				}

				context.put("po_vendor_name", this.vendor.getName());
				context.put("po_reject_reason", poActual.getRejectRemark());
				Map<String, List> contextlist = new HashMap<>();
				contextlist.put("purchadrOrderList", new ArrayList<>());
				contextlist.put("invoiceList", new ArrayList<>());
				Set<String> cc = new HashSet<>();
				cc.add(this.verifier.getEmail());
				cc.add(this.authorizer.getEmail());
				Set<String> attachment = new HashSet<>();
				EmailCriteria emailObj = new EmailCriteria();
				emailObj.setRecipient(this.issuer.getEmail());
				emailObj.setSubject("Non-Trade Purchase Order - PO Rejection");
				emailObj.setCc(cc);
				emailObj.setAttachment(attachment);
				Properties props = EmailServiceUtils.loadEmailPropertyFile();
				emailObj.setTemplateName(props.getProperty("mail.vm.template.poreject"));
				emailObj.setEmailUser(props.getProperty("mail.user"));
				emailObj.setEmailPwd(props.getProperty("mail.passwd"));
				Session session = Session.getInstance(props, new Authenticator() {
					@Override
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(emailObj.getEmailUser(), emailObj.getEmailPwd());
					}
				});
				emailService.sendEmail(session, context, contextlist, emailObj);
				LOGGER.debug(this.issuer.getName() + " have submit a PO to be verified.");
				auditService.putAudit(new Date(), "" + this.issuer.getName() + " have submit a PO to be verified.",
						this.issuer.getName(), "PO Submit");
			}
		} catch (POException | MessagingException | IOException e) {
			LOGGER.error(e);
		}
	}

	public void expiryPurchaseOrder() {
		try {
			Map<String, String> context = setCommonItemsAttributes();
			context.put("num_days", "");
			context.put("user_name", issuer.getName());
			Map<String, List> contextlist = new HashMap<>();
			contextlist.put("purchadrOrderList", new ArrayList<>());
			contextlist.put("invoiceList", new ArrayList<>());
			Set<String> cc = new HashSet<>();
			cc.add(verifier.getEmail());
			cc.add(issuer.getEmail());
			cc.add(authorizer.getEmail());
			Set<String> attachment = new HashSet<>();
			EmailCriteria emailObj = new EmailCriteria();
			emailObj.setRecipient(this.issuer.getEmail());
			emailObj.setSubject("Non-Trade Purchase Order - PO pending verification");
			emailObj.setCc(cc);
			emailObj.setAttachment(attachment);
			Properties props = EmailServiceUtils.loadEmailPropertyFile();
			emailObj.setTemplateName(props.getProperty("mail.vm.template.poverify"));
			emailObj.setEmailUser(props.getProperty("mail.user"));
			emailObj.setEmailPwd(props.getProperty("mail.passwd"));
			Session session = Session.getInstance(props, new Authenticator() {
				@Override
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(emailObj.getEmailUser(), emailObj.getEmailPwd());
				}
			});
			emailService.sendEmail(session, context, contextlist, emailObj);
			LOGGER.debug(this.issuer.getName() + " have submit a PO to be verified.");
			auditService.putAudit(new Date(), "" + this.issuer.getName() + " have submit a PO to be verified.",
					this.issuer.getName(), "PO Submit");
		} catch (POException | MessagingException | IOException e) {
			LOGGER.error(e);
		}
	}

	public void pendingClosePurchaseOrder() {
		try {
			if (validate(this.issuer.getEmail())) {
				Map<String, String> context = setCommonItemsAttributes();
				if (AbstractMainController.getCurrentUser().isPresent()) {
					EmployeeMaster currentUser = employeeService
							.getEmployeeMasterByPsId(AbstractMainController.getCurrentUser().get());
					if (currentUser != null && currentUser.getName() != null) {
						context.put("po_closer", currentUser.getName());
					}

				}
				context.put("invoice_recipient", issuer.getName());
				context.put("po_vendor_name", vendor.getName());
				context.put("po_total", "" + poActual.getTotalInclGST());
				context.put("po_total_invoice", "" + purchaseOrderLifeCycle.getTotalInvoice(poActual));
				context.put("po_balance", "" + purchaseOrderLifeCycle.getPOBalance(poActual));
				Map<String, List> contextlist = new HashMap<>();
				contextlist.put("purchadrOrderList", new ArrayList<>());
				contextlist.put("invoiceList", new ArrayList<>());
				Set<String> cc = new HashSet<>();
				cc.add(verifier.getEmail());
				cc.add(authorizer.getEmail());
				Set<String> attachment = new HashSet<>();
				EmailCriteria emailObj = new EmailCriteria();
				emailObj.setRecipient(this.issuer.getEmail());
				emailObj.setSubject("Non-Trade Purchase Order - PO pending close Notification");
				emailObj.setCc(cc);
				emailObj.setAttachment(attachment);
				Properties props = EmailServiceUtils.loadEmailPropertyFile();
				emailObj.setTemplateName(props.getProperty("mail.vm.template.poverifynotification"));
				emailObj.setEmailUser(props.getProperty("mail.user"));
				emailObj.setEmailPwd(props.getProperty("mail.passwd"));
				Session session = Session.getInstance(props, new Authenticator() {
					@Override
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(emailObj.getEmailUser(), emailObj.getEmailPwd());
					}
				});
				emailService.sendEmail(session, context, contextlist, emailObj);
				LOGGER.debug(this.issuer.getName() + " have submit a PO to be closed.");
				auditService.putAudit(new Date(), "" + this.issuer.getName() + " waiting to Close the PO.",
						this.issuer.getName(), "PO Submit");
			}
		} catch (POException | MessagingException | IOException e) {
			LOGGER.error(e);
		}
	}

	public void closePurchaseOrderWithException() {
		try {

			if (validate(this.issuer.getEmail())) {
				Map<String, String> context = setCommonItemsAttributes();

				if (AbstractMainController.getCurrentUser().isPresent()) {
					EmployeeMaster currentUser = employeeService
							.getEmployeeMasterByPsId(AbstractMainController.getCurrentUser().get());
					if (currentUser != null && currentUser.getName() != null) {
						context.put("po_closer", currentUser.getName());
					}

				}

				context.put("invoice_recipient", issuer.getName());
				context.put("po_vendor_name", vendor.getName());
				context.put("po_total", "" + poActual.getTotalInclGST());
				context.put("po_total_invoice", "" + purchaseOrderLifeCycle.getTotalInvoice(poActual));
				context.put("po_balance", "" + purchaseOrderLifeCycle.getPOBalance(poActual));
				context.put("exception_msg", poActual.getCloseException());
				Map<String, List> contextlist = new HashMap<>();
				contextlist.put("purchadrOrderList", new ArrayList<>());
				contextlist.put("invoiceList", new ArrayList<>());
				Set<String> cc = new HashSet<>();
				cc.add(verifier.getEmail());
				cc.add(authorizer.getEmail());
				Set<String> attachment = new HashSet<>();
				EmailCriteria emailObj = new EmailCriteria();
				emailObj.setRecipient(this.issuer.getEmail());
				emailObj.setSubject("Non-Trade Purchase Order - PO closed with Exception Notification");
				emailObj.setCc(cc);
				emailObj.setAttachment(attachment);
				Properties props = EmailServiceUtils.loadEmailPropertyFile();
				emailObj.setTemplateName(props.getProperty("mail.vm.template.pocloseexception"));
				emailObj.setEmailUser(props.getProperty("mail.user"));
				emailObj.setEmailPwd(props.getProperty("mail.passwd"));
				Session session = Session.getInstance(props, new Authenticator() {
					@Override
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(emailObj.getEmailUser(), emailObj.getEmailPwd());
					}
				});
				emailService.sendEmail(session, context, contextlist, emailObj);
				LOGGER.debug(this.issuer.getName() + " have submit a PO to be closed with exception.");
				auditService.putAudit(new Date(),
						"" + this.issuer.getName() + " have submit a PO to be closed with exception.",
						this.issuer.getName(), "PO Submit");
			}
		} catch (POException | MessagingException | IOException e) {
			LOGGER.error(e);
		}
	}

	public void closePurchaseOrderWithoutException() {
		try {
			if (validate(this.issuer.getEmail())) {
				Map<String, String> context = setCommonItemsAttributes();
				if (AbstractMainController.getCurrentUser().isPresent()) {
					EmployeeMaster currentUser = employeeService
							.getEmployeeMasterByPsId(AbstractMainController.getCurrentUser().get());
					if (currentUser != null && currentUser.getName() != null) {
						context.put("po_closer", currentUser.getName());
					}

				}

				context.put("invoice_recipient", issuer.getName());
				context.put("po_vendor_name", vendor.getName());
				context.put("po_total", "" + poActual.getTotalInclGST());
				context.put("po_total_invoice", "" + purchaseOrderLifeCycle.getTotalInvoice(poActual));
				context.put("po_balance", "" + purchaseOrderLifeCycle.getPOBalance(poActual));
				Map<String, List> contextlist = new HashMap<>();
				contextlist.put("purchadrOrderList", new ArrayList<>());
				contextlist.put("invoiceList", new ArrayList<>());
				Set<String> cc = new HashSet<>();

				if (verifier.getEmail() != null) {
					cc.add(verifier.getEmail());
				}
				if (authorizer.getEmail() != null) {
					cc.add(authorizer.getEmail());
				}
				Set<String> attachment = new HashSet<>();
				EmailCriteria emailObj = new EmailCriteria();
				emailObj.setRecipient(this.issuer.getEmail());
				emailObj.setSubject("Non-Trade Purchase Order - PO closed without Exception Notification");
				emailObj.setCc(cc);
				emailObj.setAttachment(attachment);
				Properties props = EmailServiceUtils.loadEmailPropertyFile();
				emailObj.setTemplateName(props.getProperty("mail.vm.template.poclosenormal"));
				emailObj.setEmailUser(props.getProperty("mail.user"));
				emailObj.setEmailPwd(props.getProperty("mail.passwd"));
				Session session = Session.getInstance(props, new Authenticator() {
					@Override
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(emailObj.getEmailUser(), emailObj.getEmailPwd());
					}
				});
				emailService.sendEmail(session, context, contextlist, emailObj);
				LOGGER.debug(this.issuer.getName() + " have submit a PO to be verified.");
				auditService.putAudit(new Date(), "" + this.issuer.getName() + " have submit a PO to be verified.",
						this.issuer.getName(), "PO Submit");
			}
		} catch (POException | MessagingException | IOException e) {
			LOGGER.error(e);
		}
	}

	private Map<String, String> setCommonItemsAttributes() {
		Map<String, String> context = new HashMap<>();
		context.put("date_format", "dd MMM yyyy");
		context.put("number_format", "#,###.##");
		context.put("sys_url", this.sysParam != null ? this.sysParam.getPropValue() : "");
		context.put("system_name", "NTPOMS");
		context.put("systemName", "NTPOMS");
		context.put("proprietor", "parkson.com");
		context.put("po_number", this.poActual.getPoNo() != null ? this.poActual.getPoNo() : "");
		context.put("po_date", this.poActual.getIssuedOn() != null ? this.poActual.getIssuedOn().toString() : "");
		context.put("po_project", "PO Project");
		context.put("class_name", "EmailService.java");
		return context;
	}

	public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
			Pattern.CASE_INSENSITIVE);

	public static boolean validate(String emailStr) {
		if (emailStr != null) {
			Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
			return matcher.find();
		} else {
			return false;
		}
	}
}