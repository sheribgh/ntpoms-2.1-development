package my.com.parkson.ntpoms.pomain.login;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public interface CustomUserDetailsService {

	UserDetails loadUserByUsernameCompanyYear(String username, String password, String company, String year)
			throws UsernameNotFoundException;

}
