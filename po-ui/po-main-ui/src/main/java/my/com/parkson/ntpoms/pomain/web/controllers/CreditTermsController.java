package my.com.parkson.ntpoms.pomain.web.controllers;

import static my.com.parkson.ntpoms.common.MessageCodes.FAILED_FETCH_RECORDS_EXCEPTION;
import static my.com.parkson.ntpoms.common.MessageCodes.FAILED_PERSIST_EXCEPTION;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.LogMessages;
import my.com.parkson.ntpoms.main.entities.CreditTerms;
import my.com.parkson.ntpoms.main.entities.CreditTermsId;
import my.com.parkson.ntpoms.main.entities.VendorBranchId;
import my.com.parkson.ntpoms.common.entities.SystemParameter;
import my.com.parkson.ntpoms.main.services.AccessLogsService;
import my.com.parkson.ntpoms.main.services.AuditsService;

import my.com.parkson.ntpoms.main.services.CreditTermsService;
import my.com.parkson.ntpoms.main.services.MessagesService;
import my.com.parkson.ntpoms.common.services.MessageService;
import my.com.parkson.ntpoms.common.services.SystemParameterService;

import my.com.parkson.ntpoms.pomain.web.models.CreditTermsForm;
import my.com.parkson.ntpoms.pomain.web.models.Pager;

@Slf4j
@Controller
@RequiredArgsConstructor
public class CreditTermsController extends AbstractMainController {

	private static final int BUTTONS_TO_SHOW = 5;
	private static final int INITIAL_PAGE = 0;
	private static final int INITIAL_PAGE_SIZE = 5;
	private static final int[] PAGE_SIZES = { 5, 10, 20 };

	@Autowired
	private CreditTermsService crService;

	@Autowired
	private SystemParameterService sysRepo;

	private static final Logger LOGGER = Logger.getLogger(CreditTermsController.class);

	/** The messages. */
	@Autowired
	private MessagesService messages;

	@Autowired
	private AuditsService auditService;

	@Autowired
	MessageService messageService;

	private CreditTermsForm tempobj;

	boolean edit = false;

	@GetMapping("/main/creditterms")
	public ModelAndView onPageLoadCrTerms() {
		ModelAndView modelAndView = new ModelAndView("/main/creditterms");
		modelAndView.getModelMap().addAttribute("creditobj", tempobj != null ? tempobj : new CreditTermsForm());
		return modelAndView;
	}

	@RequestMapping(value = "/api/credittermsgrid", method = RequestMethod.GET)
	public String crtGrid(@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page, @RequestParam("code") Optional<String> code,
			@RequestParam("description") Optional<String> description,
			@RequestParam("status") Optional<String> isActive, @RequestParam("sortingkey") Optional<String> sortingkey,
			@RequestParam("sortingorder") Optional<String> sortingorder, Model model) {
		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
		Page<CreditTerms> list;
		Specification<CreditTerms> s = new Specification<CreditTerms>() {
			@Override
			public Predicate toPredicate(Root<CreditTerms> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> ls = new ArrayList<>();
				if (code.isPresent() && !code.get().isEmpty()) {

					ls.add(cb.like(cb.lower(root.get("crtId").get("code")), "%" + code.get().toLowerCase() + "%"));
				}

				if (description.isPresent() && !description.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("description")), "%" + description.get().toLowerCase() + "%"));
				}
				if (isActive.isPresent() && !isActive.get().isEmpty() && !isActive.get().equals("All")) {
					ls.add(cb.equal(root.get("isActive"), "Active".equals(isActive.get())));
				}

				return cb.and(ls.toArray(new Predicate[0]));
			}
		};

		Sort sorttemp = new Sort(Sort.Direction.DESC, "lastModifiedOn");

		if (sortingkey.isPresent() && sortingorder.isPresent()) {
			sorttemp = new Sort(sortingorder.get().equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC,
					sortingkey.get());
		}
		if (code.isPresent() || description.isPresent() || isActive.isPresent()) {
			list = crService.getAllPagableCrt(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		} else {
			list = crService.getAllPageableCrtOrdered(new PageRequest(evalPage, evalPageSize, sorttemp));
		}
		int serialid = 0;
		int pagesize = evalPage + 1;
		if (pagesize > 1) {
			int start = evalPageSize * pagesize;
			serialid = start - evalPageSize;
		}
		Pager pager = new Pager(list.getTotalPages(), list.getNumber(), BUTTONS_TO_SHOW);
		model.addAttribute("serialid", serialid);
		model.addAttribute("list", list);
		model.addAttribute("selectedPageSize", evalPageSize);
		model.addAttribute("pageSizes", PAGE_SIZES);
		model.addAttribute("pager", pager);
		model.addAttribute("totalRecords", list.getTotalElements());
		model.addAttribute("code", code.isPresent() ? code.get() : "");
		model.addAttribute("description", description.isPresent() ? description.get() : "");
		model.addAttribute("status", isActive.isPresent() ? isActive.get() : "");
		model.addAttribute("sortingorder", sortingorder.isPresent() ? sortingorder.get() : "DESC");
		model.addAttribute("sortingkey", sortingkey.isPresent() ? sortingkey.get() : "lastModifiedOn");

		return "/main/creditterms :: result-table";
	}

	/**
	 * Creates the department.
	 *
	 * @param obj
	 *            the obj
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @param redirectAttributes
	 *            the redirect attributes
	 * @return the string
	 */
	@PostMapping(value = "/main/createcreditterms")
	public String createCreditTerms(@Valid @ModelAttribute("creditobj") CreditTermsForm obj, BindingResult result,
			Model model, RedirectAttributes redirectAttributes) {
		MDC.put("sessionId",
				AbstractMainController.getCompany().isPresent() ? AbstractMainController.getCompany().get() : "");
		
		CreditTermsId pkey = new CreditTermsId(obj.getCredittermscode(), getCompany().get());
		
		if (!result.hasErrors() && obj.getCredittermscode() != null
				&& crService.getCreditTermsByCode(obj.getCredittermscode()) == null
				&& crService.getCreditTermsbyCrtId(pkey) == null) {
			CreditTerms ad = new CreditTerms();
			/*
			 * ad.setCreatedby(getCurrentUser() != null ?
			 * getCurrentUser().getUsername() : null);
			 */
			ad.setCreatedby(getCurrentUser().get());
			ad.setCreatedon(new Date());
			ad.setLastmodifiedby(getCurrentUser().get());
			ad.setLastModifiedOn(new Date());
			ad.setActive(true);
			ad.setCrtId(pkey);
			ad.setDescription(obj.getCredittermsdescription());

			try {
				crService.createCreditTerms(ad);
				auditService.createAudit(new Date(),
						"Credit Terms: " + obj.getCredittermscode() + " created successfully", getCurrentUser().get(),
						" Create Credit Terms"

						, getCompany().get(), "");
				redirectAttributes.addFlashAttribute("info", "Credit Terms created successfully");
				tempobj = null;
			} catch (Exception e) {
				LOGGER.error(e);
			}
		} else {
			redirectAttributes.addFlashAttribute("error", "Credit Terms code already exists");
			tempobj = obj;
		}
		return "redirect:/main/creditterms";
	}

	@PostMapping(value = "/main/updatecreditterms")
	public String updateDeparment(@Valid @ModelAttribute("creditobj") CreditTermsForm obj, BindingResult result,
			Model model, RedirectAttributes redirectAttributes) {
		MDC.put("sessionId",
				AbstractMainController.getCompany().isPresent() ? AbstractMainController.getCompany().get() : "");
		CreditTermsId pkey = new CreditTermsId(obj.getCredittermscode(), getCompany().get());
		if (!result.hasErrors()) {
			CreditTerms ad = crService.getCreditTermsbyCrtId(pkey);
			boolean isActive;

			try {
				if (ad != null) {
					ad.setLastmodifiedby(getCurrentUser().get());
					ad.setLastModifiedOn(new Date());
					if (obj.getIsActive().contains("InActive")) {
						isActive = false;
					} else {
						isActive = true;
					}

					if (isActive != ad.isActive()) {

						if (obj.getIsActive().contains("InActive")) {
							ad.setDeactivatedby(getCurrentUser().get());
							ad.setDeactivatedon(new Date());
						} else {
							ad.setReactivatedon(new Date());
							ad.setReactivatedby(getCurrentUser().get());
						}
					}
					ad.setActive("Active".equals(obj.getIsActive()));
					ad.setCrtId(pkey);
					ad.setDescription(obj.getCredittermsdescription());
					
					try {
						crService.createCreditTerms(ad);
						auditService.createAudit(new Date(),
								"Credit Terms: " + obj.getCredittermscode() + " updated successfully",
								getCurrentUser().get(), " Update Credit Terms", getCompany().get(), "");
					} catch (POException e) {
						LOGGER.error(e);
					} catch (Exception e) {
						LOGGER.error(e);
					}
					redirectAttributes.addFlashAttribute("info", "Credit Terms updated successfully");
				} else {
					redirectAttributes.addFlashAttribute("error", "Credit Terms Object not exists");
					return "/main/edit_creditterms";
				}
			} catch (POException e) {
				LOGGER.error(e);
			} catch (Exception e) {
				LOGGER.error(e);
			}
		}
		return "redirect:/main/creditterms";
	}

	@GetMapping(value = "/main/editcreditterms/{code}")
	public String editCreditTerms(@PathVariable String code, Model model) {
		CreditTermsId pkey = new CreditTermsId(code, getCompany().get());
		CreditTerms ad = crService.getCreditTermsbyCrtId(pkey);
		edit = true;
		
		if (ad != null) {
			CreditTermsForm obj = new CreditTermsForm();
			String date_format = "dd-MM-yyyy hh:mm:ss";
			SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_TIME_FORMAT");
			if (dateObj != null) {
				if (!dateObj.getPropValue().isEmpty()) {
					date_format = dateObj.getPropValue();
				}
			}
			DateFormat formatter = new SimpleDateFormat(date_format);
			String createdOn = "";
			String modifiedOn = "";
			String reactivatedOn = "";
			String deactivatedOn = "";

			if (ad.getCreatedon() != null) {
				createdOn = formatter.format(ad.getCreatedon());
			}
			if (ad.getLastModifiedOn() != null) {
				modifiedOn = formatter.format(ad.getLastModifiedOn());
			}
			if (ad.getReactivatedon() != null) {
				reactivatedOn = formatter.format(ad.getReactivatedon());
			}
			if (ad.getDeactivatedon() != null) {
				deactivatedOn = formatter.format(ad.getDeactivatedon());
			}
			obj.setCreatedon(createdOn);
			obj.setDeactivatedon(deactivatedOn);
			obj.setReactivatedon(reactivatedOn);
			obj.setLastModifiedOn(modifiedOn);
			obj.setCredittermscode(ad.getCrtId().getCode());
			obj.setCompCode(ad.getCrtId().getCompanycode());
			obj.setCreatedby(ad.getCreatedby());
			obj.setLastmodifiedby(ad.getLastmodifiedby());
			obj.setReactivatedby(ad.getReactivatedby());
			obj.setDeactivatedby(ad.getDeactivatedby());
			obj.setCredittermsdescription(ad.getDescription());
			obj.setIsActive(ad.isActive() ? "Active" : "InActive");
			model.addAttribute("creditobj", obj);
			model.addAttribute("edit", edit);
			return "/main/edit_creditterms";
		} else {
			return "redirect:/main/creditterms";
		}
	}

	@RequestMapping(value = "/creditterms/existingcrt", method = RequestMethod.GET)
	public @ResponseBody CreditTerms checkVendorBranch(@RequestParam("code") Optional<String> code,
			RedirectAttributes redirectAttributes, Model model) {
		CreditTermsId pkey = new CreditTermsId(code.get(), getCompany().get());
		CreditTerms crt_obj = crService.getCreditTermsbyCrtId(pkey);
		if(crt_obj!=null){
			return crt_obj;
			
		}
		
		return new CreditTerms() ;
	}

	@Override
	protected String getHeaderTitle() {
		return "Manage Credit Terms";
	}

}