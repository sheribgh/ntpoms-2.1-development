package my.com.parkson.ntpoms.pomain.login;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service("userDetailsService")
public class CustomUserDetailsServiceImpl implements CustomUserDetailsService {

	private UserRepository userRepository;

	public CustomUserDetailsServiceImpl(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public UserDetails loadUserByUsernameCompanyYear(String username, String password, String company, String year)
			throws UsernameNotFoundException {
		if (username == null || company == null || year == null) {
			throw new UsernameNotFoundException("Username and domain must be provided");
		}
		User user = userRepository.findUser(username, password, company, year);
		if (user == null) {
			throw new UsernameNotFoundException(String.format(
					"Username not found for domain, username=%s, company=%s, year=%s", username, company, year));
		}
		return user;
	}
}
