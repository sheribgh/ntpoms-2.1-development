package my.com.parkson.ntpoms.pomain.web.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import my.com.parkson.ntpoms.common.entities.EmployeeMaster;
import my.com.parkson.ntpoms.common.entities.OrderCategory;
import my.com.parkson.ntpoms.common.entities.ProfitCenter;
import my.com.parkson.ntpoms.common.entities.PurchaseRequisition;
import my.com.parkson.ntpoms.common.services.EmployeeMasterService;
import my.com.parkson.ntpoms.common.services.OrderCategoryService;
import my.com.parkson.ntpoms.common.services.ProfitCenterService;
import my.com.parkson.ntpoms.common.services.PurchaseRequisitionService;
import my.com.parkson.ntpoms.main.entities.CreditTerms;
import my.com.parkson.ntpoms.main.entities.PurchaseOrder;
import my.com.parkson.ntpoms.main.entities.VendorBranch;
import my.com.parkson.ntpoms.main.entities.VendorBranchId;
import my.com.parkson.ntpoms.main.services.CreditTermsService;
import my.com.parkson.ntpoms.main.services.VendorBranchService;

public class PurchaseOrderValidation {
	
	private PurchaseOrder purchaseOrder;	
	private PurchaseOrder updatePurchaseOrder;
		
	private EmployeeMasterService employeeService;	
	private ProfitCenterService profitCenterService;	
	private PurchaseRequisitionService purchaseRequisitionService;	
	private OrderCategoryService orderCategoryService;	
	private VendorBranchService vendorService;	
	private CreditTermsService creditTermsService;	
	
	public PurchaseOrderValidation (PurchaseOrder purchaseOrder) {
		this.purchaseOrder = purchaseOrder;
	}
	
	public List<String> isValid() {		
		List<String> notification = new ArrayList<>();
		this.updatePurchaseOrder = this.purchaseOrder;		
		if (purchaseOrder.getContactPerson() != null) {
			validateContactPerson(notification);
		}		
		if (purchaseOrder.getPcCode() != null) {
			validateIssuingProfitCenter(notification);
		}		
		if (purchaseOrder.getVendorcode() != null) {
			validateVendorBranch(notification);
		}		
		if (purchaseOrder.getOrderCatCode() != null) {
			validateOrderCategory(notification);
		}		
		return notification;
	}

	private void validateOrderCategory(List<String> notification) {
		OrderCategory orderCategory = orderCategoryService.getOrderCategoryByCode(purchaseOrder.getOrderCatCode());			
		if (orderCategory != null) {				
			if (!orderCategory.isActive()) {					
				notification.add("Order Category is not active!");
			}				
			if (purchaseOrder.getOrderCategoryAuthorizationGrid() != null && !purchaseOrder.getOrderCategoryAuthorizationGrid().equals(orderCategory.getAuthorizationGrid())) {					
				File oldAuthorizationGrid = new File(purchaseOrder.getOrderCategoryAuthorizationGrid());
				File newAuthorizationGrid = new File(orderCategory.getAuthorizationGrid());										
				notification.add("Order Category Authorization Grid has changed from " + oldAuthorizationGrid.getName() + " to " + newAuthorizationGrid.getName());
			}				
			PurchaseRequisition purchaseRequisition = purchaseRequisitionService.getPurchaseRequisitionByTypeCode(purchaseOrder.getPrTypeCode());				
			if (purchaseOrder.getPrTypeCode() != null && !purchaseOrder.getPrTypeCode().equals(purchaseRequisition.getTypeCode())) {										
				notification.add("Order Sub-Category has changed from " + purchaseRequisition.getAbbrName() + " to " + orderCategory.getPurchaseRequisition().getAbbrName());
			}
		}
	}

	private void validateVendorBranch(List<String> notification) {
		VendorBranchId vendorBranchId = new VendorBranchId(purchaseOrder.getVendorcode() , purchaseOrder.getVendorbranchcode() , purchaseOrder.getCompCode());
		VendorBranch vendor = vendorService.getVendorBranchbyVendorId(vendorBranchId);			
		if (vendor != null) {				
			if (!vendor.isActive()) {					
				notification.add("Vendor is not active!");
			}				
			if (purchaseOrder.getVendorAddressLine1() != null && !purchaseOrder.getVendorAddressLine1().equals(vendor.getAddressLine1())) {										
				notification.add("Vendor's Address (Line 1) has changed from " + purchaseOrder.getVendorAddressLine1() + " to " + vendor.getAddressLine1());
			}				
			if (purchaseOrder.getVendorAddressLine2() != null && !purchaseOrder.getVendorAddressLine2().equals(vendor.getAddressLine2())) {										
				notification.add("Vendor's Address (Line 2) has changed from " + purchaseOrder.getVendorAddressLine2() + " to " + vendor.getAddressLine2());
			}				
			if (purchaseOrder.getVendorAddressLine3() != null && !purchaseOrder.getVendorAddressLine3().equals(vendor.getAddressLine3())) {										
				notification.add("Vendor's Address (Line 3) has changed from " + purchaseOrder.getVendorAddressLine3() + " to " + vendor.getAddressLine3());
			}				
			if (purchaseOrder.getVendorPostCode() != null && !purchaseOrder.getVendorPostCode().equals(vendor.getPostCode())) {										
				notification.add("Vendor's Post Code has changed from " + purchaseOrder.getVendorPostCode() + " to " + vendor.getPostCode());
			}				
			if (purchaseOrder.getVendorPostArea() != null && !purchaseOrder.getVendorPostArea().equals(vendor.getPostCodeArea())) {										
				notification.add("Vendor's Post Area has changed from " + purchaseOrder.getVendorPostArea() + " to " + vendor.getPostCodeArea());
			}
			CreditTerms creditTerms = creditTermsService.getCreditTermsByCode(purchaseOrder.getCreditTermsCode());				
			if (creditTerms != null && purchaseOrder.getCreditTermsCode() != null && !purchaseOrder.getCreditTermsCode().equals(creditTerms.getCrtId().getCode())) {												
					//notification.add("Credit Terms has changed from " + creditTermsService.getCreditTermsByCode(vendor.getCreditTermsCode()).getDescription() + " to " + creditTerms.getDescription());
			}
		}
	}

	private void validateIssuingProfitCenter(List<String> notification) {
		ProfitCenter issuingProfitCenter = profitCenterService.getProfitCenterByCode(purchaseOrder.getPcCode());			
		if (issuingProfitCenter != null) {				
			if (!issuingProfitCenter.isIsactive()) {					
				notification.add("Profit Center is not active!");
			}				
			if (purchaseOrder.getAddressLine1() != null && !purchaseOrder.getAddressLine1().equals(issuingProfitCenter.getAddressLine1())) {										
				notification.add("Address (Line 1) has changed from " + purchaseOrder.getAddressLine1() + " to " + issuingProfitCenter.getAddressLine1());
			}				
			if (purchaseOrder.getAddressLine2() != null && !purchaseOrder.getAddressLine2().equals(issuingProfitCenter.getAddressLine2())) {										
				notification.add("Address (Line 2) has changed from " + purchaseOrder.getAddressLine2() + " to " + issuingProfitCenter.getAddressLine2());
			}				
			if (purchaseOrder.getAddressLine3() != null && !purchaseOrder.getAddressLine3().equals(issuingProfitCenter.getAddressLine3())) {										
				notification.add("Address (Line 3) has changed from " + purchaseOrder.getAddressLine3() + " to " + issuingProfitCenter.getAddressLine3());
			}				
			if (purchaseOrder.getPostCode() != null && !purchaseOrder.getPostCode().equals(issuingProfitCenter.getPostCode())) {										
				notification.add("Post Code has changed from " + purchaseOrder.getPostCode() + " to " + issuingProfitCenter.getPostCode());
			}				
			if (purchaseOrder.getPostArea() != null && !purchaseOrder.getPostArea().equals(issuingProfitCenter.getPostArea())) {										
				notification.add("Post Area has changed from " + purchaseOrder.getPostArea() + " to " + issuingProfitCenter.getPostArea());
			}
		}
	}

	private void validateContactPerson(List<String> notification) {
		EmployeeMaster employee = employeeService.getEmployeeMasterByPsId(purchaseOrder.getIssuer());			
		if (employee != null) {
			if (!employee.isActive()) {					
				notification.add("Contact person is not active");
			}				
			if (purchaseOrder.getContactPersonEmail() != null && !purchaseOrder.getContactPersonName().equals(employee.getName())) {										
				notification.add("Contact Name has changed from " + purchaseOrder.getContactPersonName() + " to " + employee.getName());
			}				
			if (purchaseOrder.getContactPersonPhone() != null && !purchaseOrder.getContactPersonPhone().equals(employee.getPhoneNo())) {										
				notification.add("Contact Number has changed from " + purchaseOrder.getContactPersonPhone() + " to " + employee.getPhoneNo());
			}				
			if (purchaseOrder.getContactPersonEmail() != null && !purchaseOrder.getContactPersonEmail().equals(employee.getEmail())) {										
				notification.add("Contact Email has changed from " + purchaseOrder.getContactPersonEmail() + " to " + employee.getEmail());
			}				
		}
	}
	
	public PurchaseOrder updatedPurchaseOrder() {
		return this.updatePurchaseOrder;
	}
}