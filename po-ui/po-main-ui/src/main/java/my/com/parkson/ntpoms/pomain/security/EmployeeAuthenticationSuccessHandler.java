package my.com.parkson.ntpoms.pomain.security;

import java.io.IOException;
import java.util.Collection;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import my.com.parkson.ntpoms.common.services.AuditService;
import my.com.parkson.ntpoms.main.services.AuditsService;
import my.com.parkson.ntpoms.pomain.web.controllers.AbstractMainController;
@Component
public class EmployeeAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

	@Autowired
	private AuditsService auditService;
	
	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

	@Override
	public void onAuthenticationSuccess(HttpServletRequest arg0, HttpServletResponse arg1,
			Authentication authentication) throws IOException, ServletException {
		
		/* Fetching Client's IP Address*/
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
		        .getRequest();

		String ip = request.getRemoteAddr();
		
		auditService.putAudit(new Date(), "User " + authentication.getName() + " with IP address " + ip + " is signed in", authentication.getName(), "Log In"
				);
		redirectStrategy.sendRedirect(arg0, arg1, "/");
	}
	

}
