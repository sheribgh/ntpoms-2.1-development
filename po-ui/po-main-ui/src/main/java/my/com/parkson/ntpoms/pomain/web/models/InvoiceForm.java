package my.com.parkson.ntpoms.pomain.web.models;

import java.io.Serializable;


import my.com.parkson.ntpoms.main.entities.PurchaseOrder;

public class InvoiceForm implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String poNo;
	private double totInvoiceAmount;
	private double poAmount;
	private double balance;
	
	private String poInvoiceNumber;
	
	private String invoiceNumber;

	private String startDate;

	private String invoiceRecipient;

	private double invoiceAmount;

	private PurchaseOrder purchaseOrder;

	private String invoicenote;
	
	private String poCloseException;
	
	
	
	
	public String getPoCloseException() {
		return poCloseException;
	}
	public void setPoCloseException(String poCloseException) {
		this.poCloseException = poCloseException;
	}
	public String getPoInvoiceNumber() {
		return poInvoiceNumber;
	}
	public void setPoInvoiceNumber(String poInvoiceNumber) {
		this.poInvoiceNumber = poInvoiceNumber;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	
	
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getInvoiceRecipient() {
		return invoiceRecipient;
	}
	public void setInvoiceRecipient(String invoiceRecipient) {
		this.invoiceRecipient = invoiceRecipient;
	}
	public double getInvoiceAmount() {
		return invoiceAmount;
	}
	public void setInvoiceAmount(double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}
	public PurchaseOrder getPurchaseOrder() {
		return purchaseOrder;
	}
	public void setPurchaseOrder(PurchaseOrder purchaseOrder) {
		this.purchaseOrder = purchaseOrder;
	}
	public String getInvoicenote() {
		return invoicenote;
	}
	public void setInvoicenote(String invoicenote) {
		this.invoicenote = invoicenote;
	}
	public String getPoNo() {
		return poNo;
	}
	public void setPoNo(String poNo) {
		this.poNo = poNo;
	}
	
	public double getTotInvoiceAmount() {
		return totInvoiceAmount;
	}
	public void setTotInvoiceAmount(double totInvoiceAmount) {
		this.totInvoiceAmount = totInvoiceAmount;
	}
	public double getPoAmount() {
		return poAmount;
	}
	public void setPoAmount(double poAmount) {
		this.poAmount = poAmount;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	
	
	
}
