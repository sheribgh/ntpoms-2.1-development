package my.com.parkson.ntpoms.pomain.config;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "archiveEntityManagerFactory", transactionManagerRef = "archiveTransactionManager", basePackages = {
		"my.com.parkson.ntpoms.archive.services" })
public class ArchiveDbConfig {

	@Bean(name = "archiveDataSource")
	@ConfigurationProperties(prefix = "archive.datasource")
	public DataSource dataSource() {
		return DataSourceBuilder.create().build();
	}

	@Bean(name = "archiveEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean archiveEntityManagerFactory(EntityManagerFactoryBuilder builder,
			@Qualifier("archiveDataSource") DataSource dataSource) {
		return builder.dataSource(dataSource).packages("my.com.parkson.ntpoms.archive.entities")
				.persistenceUnit("archive-ntpomsUI").build();
	}

	@Bean(name = "archiveTransactionManager")
	public PlatformTransactionManager archiveTransactionManager(
			@Qualifier("archiveEntityManagerFactory") EntityManagerFactory archiveEntityManagerFactory) {
		return new JpaTransactionManager(archiveEntityManagerFactory);
	}

}
