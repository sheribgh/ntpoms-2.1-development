/**
 *****************************************************
 * COPYRIGHT UMAIR OTHMAN
 * 2017
 * IT Exec
 *****************************************************
 */
package my.com.parkson.ntpoms.pomain.email;

import java.util.List;
import java.util.Map;

/**
 **************************************************************
 * Custom class that extends EmailCriteria for PO use only
 **************************************************************
 */
public class PoEmailCriteria extends EmailCriteria {	
	
	private Map<String,List<PurchaseOrder>> purchaseOrderList;
	private Map<String,List<Invoice>> invoiceList;
	
	public Map<String, List<PurchaseOrder>> getPurchaseOrderList() {
		return purchaseOrderList;
	}
	
	public void setPurchaseOrderList(Map<String, List<PurchaseOrder>> purchaseOrderList) {
		this.purchaseOrderList = purchaseOrderList;
	}
	
	public Map<String, List<Invoice>> getInvoiceList() {
		return invoiceList;
	}
	
	public void setInvoiceList(Map<String, List<Invoice>> invoiceList) {
		this.invoiceList = invoiceList;
	}
}