package my.com.parkson.ntpoms.pomain.login;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

public class User extends org.springframework.security.core.userdetails.User {

	private static final long serialVersionUID = 1L;

	private String company;

	private String year;

	public User(String username, String company, String year, String password, boolean enabled,
			boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked,
			Collection<? extends GrantedAuthority> authorities) {
		super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
		this.company = company;
		this.year = year;
	}

	/**
	 * @return the company
	 */
	public String getCompany() {
		return company;
	}

	/**
	 * @return the year
	 */
	public String getYear() {
		return year;
	}

}
