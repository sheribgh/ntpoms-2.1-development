package my.com.parkson.ntpoms.pomain.login;

import java.util.Collection;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

public class CustomAuthenticationToken extends UsernamePasswordAuthenticationToken {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String year;
	private String company;

	public CustomAuthenticationToken(Object principal, Object credentials, String company, String year) {
		super(principal, credentials);
		this.company = company;
		this.year = year;
		super.setAuthenticated(false);
	}

	public CustomAuthenticationToken(Object principal, Object credentials, String company, String year,
			Collection<? extends GrantedAuthority> authorities) {
		super(principal, credentials, authorities);
		this.company = company;
		this.year = year;
		super.setAuthenticated(true); // must use super, as we override
	}

	/**
	 * @return the year
	 */
	public String getYear() {
		return year;
	}

	/**
	 * @return the company
	 */
	public String getCompany() {
		return company;
	}

}
