/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.pomain.web.controllers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import jdk.nashorn.internal.runtime.UserAccessorProperty;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.common.entities.SystemParameter;
import my.com.parkson.ntpoms.common.model.Pager;
import my.com.parkson.ntpoms.common.services.SystemParameterService;
import my.com.parkson.ntpoms.main.entities.PurchaseOrder;
import my.com.parkson.ntpoms.main.entities.PurchaseOrderUserAccess;
import my.com.parkson.ntpoms.main.repositories.PCLastPONumberRepository;
import my.com.parkson.ntpoms.main.services.AccessLogsService;
import my.com.parkson.ntpoms.main.services.AuditsService;
import my.com.parkson.ntpoms.main.services.CreditTermsService;
import my.com.parkson.ntpoms.main.services.ProjectService;
import my.com.parkson.ntpoms.main.services.PurchaseOrderService;
import my.com.parkson.ntpoms.main.services.PurchaseOrderUserAccessService;
import my.com.parkson.ntpoms.main.services.VendorBranchService;
import my.com.parkson.ntpoms.main.services.VendorCreditTermsService;
 
@Slf4j
@Controller
@RequiredArgsConstructor
public class DashboardController extends AbstractMainController {

	@Autowired
	private VendorBranchService vendrepo;

	@Autowired
	private VendorCreditTermsService vendcrtrepo;

	@Autowired
	private CreditTermsService crtrepo;

	@Autowired
	private ProjectService projectrepo;

	@Autowired
	private PCLastPONumberRepository pclastporepo;

	@Autowired
	private PurchaseOrderUserAccessService poUsrAcc;

	@Autowired
	private PurchaseOrderService purchaseorder;

	@Autowired
	private AccessLogsService log;

	@Autowired
	private AuditsService auditService;

	@Autowired
	private SystemParameterService sysRepo;

	private static final int BUTTONS_TO_SHOW = 5;
	private static final int INITIAL_PAGE = 0;
	private static final int INITIAL_PAGE_SIZE = 5;
	private static final int[] PAGE_SIZES = { 5, 10, 20 };

	/**
	 * On page load GST.
	 *
	 * @param pageSize
	 *            the page size
	 * @param page
	 *            the page
	 * @param code
	 *            the code
	 * @param abbrDesc
	 *            the abbr desc
	 * @param isActive
	 *            the is active
	 * @return the model and view
	 */

	@GetMapping(value = "/main/dashboard")
	public String onPageLoadDashboard(Model model) {

		long total_vendorBranch = vendrepo.getCount();
		long total_active_vendorBranch = vendrepo.getCountActive();
		long total_inactive_vendorBranch = total_vendorBranch - total_active_vendorBranch;

		long total_vendorCrt = vendcrtrepo.getCount();
		long total_active_vendorCrt = vendcrtrepo.getCountActive();
		long total_inactivevendorCrt = total_vendorCrt - total_active_vendorCrt;

		long total_crt = crtrepo.getCount();
		long total_active_crt = crtrepo.getCountActive();
		long total_inactive_crt = total_crt - total_active_crt;

		long total_project = projectrepo.getCount();
		long total_active_project = projectrepo.getCountActive();
		long total_inactive_project = total_project - total_active_project;

		long total_pclastponumber = pclastporepo.count();

		long totalLogs = log.getCount();

		long totalAudit = auditService.getCount();

		model.addAttribute("total_vendorBranch", total_vendorBranch);
		model.addAttribute("total_active_vendorBranch", total_active_vendorBranch);
		model.addAttribute("total_inactive_vendorBranch", total_inactive_vendorBranch);

		model.addAttribute("total_vendorCrt", total_vendorCrt);
		model.addAttribute("total_active_vendorCrt", total_active_vendorCrt);
		model.addAttribute("total_inactivevendorCrt", total_inactivevendorCrt);

		model.addAttribute("totalLogs", totalLogs);

		model.addAttribute("totalAudit", totalAudit);

		model.addAttribute("total_crt", total_crt);
		model.addAttribute("total_active_crt", total_active_crt);
		model.addAttribute("total_inactive_crt", total_inactive_crt);

		model.addAttribute("total_project", total_project);
		model.addAttribute("total_active_project", total_active_project);
		model.addAttribute("total_inactive_project", total_inactive_project);

		model.addAttribute("total_pclastponumber", total_pclastponumber);

		return "/main/dashboard";

	}

	/**
	 * On page load GST.
	 *
	 * @param pageSize
	 *            the page size
	 * @param page
	 *            the page
	 * @param code
	 *            the code
	 * @param abbrDesc
	 *            the abbr desc
	 * @param isActive
	 *            the is active
	 * @return the model and view
	 */

	@GetMapping(value = "/main/po_dashboard")
	public String onPageLoad_PO_Dashboard(Model model) {
		int chkSizeD;
		int chkSizeV;
		int chkSizeA;
		int chkSizeI;
		
		
		List<PurchaseOrderUserAccess> pouser_D = poUsrAcc.getBypsIDAndAction(getCurrentUser().get(), "D");
		List<String> poNumber_D = new ArrayList<>();
		PurchaseOrder pos_D = new PurchaseOrder();
		List<String> purchaseorderlistFromPOMain_D = new ArrayList<>();
		/***
		 * Getting List of Po Numbers Under PSID and Authorize Action and Status
		 * from user accss and po_t_main table
		 ***/
		
		Calendar calendar = new GregorianCalendar();
	    String calendar_year = Integer.toString(calendar.get(Calendar.YEAR));
	    String logedin_year = getYear().get();
	    if(logedin_year.equals(calendar_year)){
	    	
	    	for (PurchaseOrderUserAccess a : pouser_D) {
	    		
	    			String ponos_sub = a.getPurchaseorderuseraccessId().getPono().substring(5,7);
	    	        String calendar_year_sub = calendar_year.substring(2);
	    	        String logedin_year_sub = logedin_year.substring(2);
	    	        int last_year_sub = Integer.valueOf(logedin_year_sub)-1 ;
	    	        
	    	        if(ponos_sub.equals(calendar_year_sub) || ponos_sub.equals(Integer.toString(last_year_sub))){
	    	            

	    	        	poNumber_D.add(a.getPurchaseorderuseraccessId().getPono());
	    	        	pos_D = purchaseorder.getPurchaseOrderByPoNo(a.getPurchaseorderuseraccessId().getPono());
	    				
	    				if(pos_D!=null){
	    				if(pos_D.getStatus().equals("Draft")){
	    					purchaseorderlistFromPOMain_D.add(pos_D.getPoNo());
	    				}
	    				}
	    	            }
	    	}
	    	
	    }else{
	    	
	    	for (PurchaseOrderUserAccess a : pouser_D) {
	            
	            String ponos_sub = a.getPurchaseorderuseraccessId().getPono().substring(5,7);
	          
	            String logedin_year_sub = logedin_year.substring(2);
	            
	            if(ponos_sub.equals(logedin_year_sub)){
	            

	            	poNumber_D.add(a.getPurchaseorderuseraccessId().getPono());
    	        	pos_D = purchaseorder.getPurchaseOrderByPoNo(a.getPurchaseorderuseraccessId().getPono());
    				
    				if(pos_D!=null){
    				if(pos_D.getStatus().equals("Draft")){
    					purchaseorderlistFromPOMain_D.add(pos_D.getPoNo());
    				}
    				}
	            }
	          } 
	          
	        
	    	
	    }
		
		
		/*for (PurchaseOrderUserAccess a : pouser) {
			poNumber.add(a.getPurchaseorderuseraccessId().getPono());
			pos = purchaseorder.getPurchaseOrderByPoNo(a.getPurchaseorderuseraccessId().getPono());
			if (pos != null) {
				if (pos.getStatus().equals("Draft")) {
					purchaseorderlistFromPOMain.add(pos.getPoNo());
				}
			}
		}*/
		if (purchaseorderlistFromPOMain_D != null) {
			chkSizeD = purchaseorderlistFromPOMain_D.size();
			model.addAttribute("chkSizeD", chkSizeD);
		} else {
			model.addAttribute("chkSizeD", 0);
		}
		
		
		
		
		List<PurchaseOrderUserAccess> pouser_verify = poUsrAcc.getBypsIDAndAction(getCurrentUser().get(), "V");
		List<String> poNumber_verify = new ArrayList<>();
		PurchaseOrder pos_verify = new PurchaseOrder();
		List<String> purchaseorderlistFromPOMain_verify = new ArrayList<>();
		/***
		 * Getting List of Po Numbers Under PSID and Authorize Action and Status
		 * from user accss and po_t_main table
		 ***/
	    
	    //if loged in year is equal to current year, show list of pos from current year and last year
	      
	      if(logedin_year.equals(calendar_year)){
	        
	        for (PurchaseOrderUserAccess a : pouser_verify) {
	          
	        		String ponos_sub = a.getPurchaseorderuseraccessId().getPono().substring(5,7);
	                String calendar_year_sub = calendar_year.substring(2);
	                String logedin_year_sub = logedin_year.substring(2);
	                int last_year_sub = Integer.valueOf(logedin_year_sub)-1 ;
	                
	                if(ponos_sub.equals(calendar_year_sub) || ponos_sub.equals(Integer.toString(last_year_sub))){
	                    
	                	poNumber_verify.add(a.getPurchaseorderuseraccessId().getPono());
	        			pos_verify = purchaseorder.getAllPurchaseOrderByPoNo(a.getPurchaseorderuseraccessId().getPono());
	        			if (pos_verify != null) {
	        				if (pos_verify.getStatus().equals("Issued")) {
	        					purchaseorderlistFromPOMain_verify.add(pos_verify.getPoNo());
	        				}
	        			}
	                    }
	        }
	        
	      }else{
	        
	        for (PurchaseOrderUserAccess a : pouser_verify) {
	              
	              String ponos_sub = a.getPurchaseorderuseraccessId().getPono().substring(5,7);
	            
	              String logedin_year_sub = logedin_year.substring(2);
	              
	              if(ponos_sub.equals(logedin_year_sub)){
	              
	            	  poNumber_verify.add(a.getPurchaseorderuseraccessId().getPono());
	      			pos_verify = purchaseorder.getAllPurchaseOrderByPoNo(a.getPurchaseorderuseraccessId().getPono());
	      			if (pos_verify != null) {
	      				if (pos_verify.getStatus().equals("Issued")) {
	      					purchaseorderlistFromPOMain_verify.add(pos_verify.getPoNo());
	      				}
	      			}
	              }
	            } 
	            
	          
	        
	      }
		
		
		
		
		
		
		
		/*for (PurchaseOrderUserAccess a : pouser_verify) {
			poNumber_verify.add(a.getPurchaseorderuseraccessId().getPono());
			pos_verify = purchaseorder.getAllPurchaseOrderByPoNo(a.getPurchaseorderuseraccessId().getPono());
			if (pos_verify != null) {
				if (pos_verify.getStatus().equals("Issued")) {
					purchaseorderlistFromPOMain_verify.add(pos_verify.getPoNo());
				}
			}
		}*/
		if (purchaseorderlistFromPOMain_verify != null) {
			chkSizeV = purchaseorderlistFromPOMain_verify.size();
			model.addAttribute("chkSizeV", chkSizeV);
		} else {
			model.addAttribute("chkSizeV", 0);
		}
		
		
		
		List<PurchaseOrderUserAccess> pouser_A = poUsrAcc.getBypsIDAndAction(getCurrentUser().get(), "A");
		List<String> poNumber_A = new ArrayList<>();
		PurchaseOrder pos_A = new PurchaseOrder();
		List<String> purchaseorderlistFromPOMain_A = new ArrayList<>();
		/***
		 * Getting List of Po Numbers Under PSID and Authorize Action and Status
		 * from user accss and po_t_main table
		 ***/
		
		 if(logedin_year.equals(calendar_year)){
		        
		        for (PurchaseOrderUserAccess a : pouser_A) {
		          
		            String ponos_sub = a.getPurchaseorderuseraccessId().getPono().substring(5,7);
		                String calendar_year_sub = calendar_year.substring(2);
		                String logedin_year_sub = logedin_year.substring(2);
		                int last_year_sub = Integer.valueOf(logedin_year_sub)-1 ;
		                
		                if(ponos_sub.equals(calendar_year_sub) || ponos_sub.equals(Integer.toString(last_year_sub))){
		                    
		                	poNumber_A.add(a.getPurchaseorderuseraccessId().getPono());
		        			pos_A = purchaseorder.getAllPurchaseOrderByPoNo(a.getPurchaseorderuseraccessId().getPono());

		        			if (pos_A != null) {
		        				if (pos_A.getStatus().equals("Verified")) {
		        					purchaseorderlistFromPOMain_A.add(pos_A.getPoNo());
		        				}
		        			}
		                    }
		        }
		        
		      }else{
		        
		        for (PurchaseOrderUserAccess a : pouser_A) {
		              
		              String ponos_sub = a.getPurchaseorderuseraccessId().getPono().substring(5,7);
		            
		              String logedin_year_sub = logedin_year.substring(2);
		              
		              if(ponos_sub.equals(logedin_year_sub)){
		              
		            	  poNumber_A.add(a.getPurchaseorderuseraccessId().getPono());
		      			pos_A = purchaseorder.getAllPurchaseOrderByPoNo(a.getPurchaseorderuseraccessId().getPono());

		      			if (pos_A != null) {
		      				if (pos_A.getStatus().equals("Verified")) {
		      					purchaseorderlistFromPOMain_A.add(pos_A.getPoNo());
		      				}
		      			}
		              }
		            } 
		            
		          
		        
		      }
		
		
		
		/*for (PurchaseOrderUserAccess a : pouser_A) {
			poNumber_A.add(a.getPurchaseorderuseraccessId().getPono());
			pos_A = purchaseorder.getAllPurchaseOrderByPoNo(a.getPurchaseorderuseraccessId().getPono());

			if (pos_A != null) {
				if (pos_A.getStatus().equals("Verified")) {
					purchaseorderlistFromPOMain_A.add(pos_A.getPoNo());
				}
			}
		}*/
		if (purchaseorderlistFromPOMain_A != null) {
			chkSizeA = purchaseorderlistFromPOMain_A.size();
			model.addAttribute("chkSizeA", chkSizeA);
		} else {
			model.addAttribute("chkSizeA", 0);
		}
		
		
		
		
		
	

		
		
		List<String> action_list = new ArrayList<>();
		action_list.add("D");
		action_list.add("V");
		action_list.add("A");
		action_list.add("I");
		List<PurchaseOrderUserAccess> pouser_I = new ArrayList<>();
		List<String> pouser_list = new ArrayList<>(); // List of pos from userAccess with psid and action list
	
		PurchaseOrder pos_I = new PurchaseOrder();
		List<PurchaseOrder> pos_list = new ArrayList<>();
		List<String> purchaseorderlistFromPOMain_I = new ArrayList<>();
		
	
		
	 if(logedin_year.equals(calendar_year)){
		for(String action_obj: action_list){
			pouser_I = poUsrAcc.getBypsIDAndAction(getCurrentUser().get(), action_obj);
			 
			
			 if(pouser_I!=null){
				 for (PurchaseOrderUserAccess a : pouser_I) {
					 
					 	String ponos_sub = a.getPurchaseorderuseraccessId().getPono().substring(5,7);
		                String calendar_year_sub = calendar_year.substring(2);
		                String logedin_year_sub = logedin_year.substring(2);
		                int last_year_sub = Integer.valueOf(logedin_year_sub)-1 ;
		                
					 if(ponos_sub.equals(calendar_year_sub) || ponos_sub.equals(Integer.toString(last_year_sub))){
						 pouser_list.add(a.getPurchaseorderuseraccessId().getPono());
					 }
				 }
				 }
			
			 
		}
		
		
		

		
		
		/***Getting List of Po Numbers Under PSID and Authorize Action and Status from user accss and po_t_main table ***/
		for (String a : pouser_list) {
	
			//poNumberListFromUserAccess.add(a);
			pos_I = purchaseorder.getAllPurchaseOrderByPoNo(a); // list of po obj from po main 
			
			/*if(pos_I!=null){
			pos_list.add(pos_I);
			}*/
		
			if(pos_I!=null){
			if(pos_I.getStatus().equals("Authorized") || pos_I.getStatus().equals("Invoiced") ){
				purchaseorderlistFromPOMain_I.add(pos_I.getPoNo());
			}
			
			}
		
		
		}
		
	 }else{
			for(String action_obj: action_list){
				pouser_I = poUsrAcc.getBypsIDAndAction(getCurrentUser().get(), action_obj);
				 
				
				 if(pouser_I!=null){
					 for (PurchaseOrderUserAccess a : pouser_I) {
						 
						 String ponos_sub = a.getPurchaseorderuseraccessId().getPono().substring(5,7);
				            
			              String logedin_year_sub = logedin_year.substring(2);
			               
			                
			              if(ponos_sub.equals(logedin_year_sub)){
							 pouser_list.add(a.getPurchaseorderuseraccessId().getPono());
						 }
					 }
					 }
				
				 
			}
	
			
			
			/***Getting List of Po Numbers Under PSID and Authorize Action and Status from user accss and po_t_main table ***/
			for (String a : pouser_list) {
		
				//poNumberListFromUserAccess.add(a);
				pos_I = purchaseorder.getAllPurchaseOrderByPoNo(a); // list of po obj from po main 
				
				/*if(pos_I!=null){
				pos_list.add(pos_I);
				}*/
			
				if(pos_I!=null){
				if(pos_I.getStatus().equals("Authorized") || pos_I.getStatus().equals("Invoiced") ){
					purchaseorderlistFromPOMain_I.add(pos_I.getPoNo());
				}
				
				}
			
			
			}
		 
		 
	 }
		
		if (purchaseorderlistFromPOMain_I != null) {
			chkSizeI = purchaseorderlistFromPOMain_I.size();
			model.addAttribute("chkSizeI", chkSizeI);
		} else {
			model.addAttribute("chkSizeI", 0);
		}
		return "/main/po_dashboard";
	}

	@RequestMapping(value = "/dashboard/MyPOsGrid", method = RequestMethod.GET)
	public String myPOGrid(@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page, @RequestParam("poNo") Optional<String> poNo,
			@RequestParam("status") Optional<String> status, @RequestParam("vendorName") Optional<String> vendorName,
			@RequestParam("issuedOn") Optional<String> issuedOn,
			@RequestParam("sortingkey") Optional<String> sortingkey,
			@RequestParam("sortingorder") Optional<String> sortingorder, Model model) {

		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;

		String date_format = "dd-MM-yyyy";
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_FORMAT");
		if (dateObj != null) {
			if (!dateObj.getPropValue().isEmpty()) {
				date_format = dateObj.getPropValue();
			}
		}

		Page<PurchaseOrder> list;
		List<PurchaseOrderUserAccess> pouser = poUsrAcc.getBypsIDAndAction(getCurrentUser().get(), "D");
		/*
		 * List<PurchaseOrderUserAccess> pouser =
		 * poUsrAcc.getBypsID(getCurrentUser().get());
		 */
		List<String> poNumber = new ArrayList<>();

		Calendar calendar = new GregorianCalendar();
		String calendar_year = Integer.toString(calendar.get(Calendar.YEAR));
		String logedin_year = getYear().get();
		
		//if loged in year is equal to current year, show list of pos from current year and last year
		if(logedin_year.equals(calendar_year)){
			for (PurchaseOrderUserAccess a : pouser) {
				
				String ponos_sub = a.getPurchaseorderuseraccessId().getPono().substring(5,7);
				String calendar_year_sub = calendar_year.substring(2);
				String logedin_year_sub = logedin_year.substring(2);
				int last_year_sub = Integer.valueOf(logedin_year_sub)-1 ;
				if(ponos_sub.equals(calendar_year_sub) || ponos_sub.equals(Integer.toString(last_year_sub))){
				

				poNumber.add(a.getPurchaseorderuseraccessId().getPono());
				}
			}	
			
		}else{ //if it's not current year, only see the current year po list
			for (PurchaseOrderUserAccess a : pouser) {
				
				String ponos_sub = a.getPurchaseorderuseraccessId().getPono().substring(5,7);
			
				String logedin_year_sub = logedin_year.substring(2);
				
				if(ponos_sub.equals(logedin_year_sub)){
				

				poNumber.add(a.getPurchaseorderuseraccessId().getPono());
				}
			}	
			
		}
		Specification<PurchaseOrder> s = new Specification<PurchaseOrder>() {
			@Override
			public Predicate toPredicate(Root<PurchaseOrder> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> ls = new ArrayList<>();
				if (poNo.isPresent() && !poNo.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("poNo")), "%" + poNo.get().toLowerCase() + "%"));
				}
				if (status.isPresent() && !status.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("status")), "%" + status.get().toLowerCase() + "%"));

				}

				if (vendorName.isPresent() && !vendorName.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("vendorName")), "%" + vendorName.get().toLowerCase() + "%"));
				}

				if (issuedOn.isPresent() && !issuedOn.get().isEmpty()) {

					Expression<String> dateStringExpr = cb.function("to_char", String.class, root.get("issuedOn"),
							cb.literal("YYYY-MM-DD HH12:MI:SS"));
					ls.add(cb.like(cb.lower(dateStringExpr), "%" + issuedOn.get().toLowerCase() + "%"));
				}

				List<Predicate> temp = new ArrayList<>();
				for (String s : poNumber) {
					temp.add(cb.equal(root.get("poNo"), s));
				}
				ls.add(cb.or(temp.toArray(new Predicate[0])));
				return cb.and(ls.toArray(new Predicate[0]));
			}
		};

		Sort sorttemp = new Sort(Sort.Direction.DESC, "lastModifiedOn");

		if (sortingkey.isPresent() && sortingorder.isPresent()) {
			sorttemp = new Sort(sortingorder.get().equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC,
					sortingkey.get());
		}
		if (poNo.isPresent() || status.isPresent() || vendorName.isPresent()) {
			list = purchaseorder.getAllPageablePurchaseOrder(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		} else {
			list = purchaseorder.getAllPageablePurchaseOrder(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		}

		int serialid = 0;

		int pagesize = evalPage + 1;

		if (pagesize > 1) {
			int start = evalPageSize * pagesize;
			serialid = start - evalPageSize;
		}

		Pager pager = new Pager(list.getTotalPages(), list.getNumber(), BUTTONS_TO_SHOW);
		model.addAttribute("serialid", serialid);
		model.addAttribute("list", list);
		model.addAttribute("selectedPageSize", evalPageSize);
		model.addAttribute("pageSizes", PAGE_SIZES);
		model.addAttribute("pager", pager);
		model.addAttribute("totalRecords", list.getTotalElements());
		model.addAttribute("poNo", poNo.isPresent() ? poNo.get() : "");
		model.addAttribute("status", status.isPresent() ? status.get() : "");
		model.addAttribute("vendorName", vendorName.isPresent() ? vendorName.get() : "");
		model.addAttribute("issuedOn", issuedOn.isPresent() ? issuedOn.get() : "");
		model.addAttribute("sortingorder", sortingorder.isPresent() ? sortingorder.get() : "DESC");
		model.addAttribute("sortingkey", sortingkey.isPresent() ? sortingkey.get() : "lastModifiedOn");
		return "/main/po_dashboard :: result-table";
	}

	@RequestMapping(value = "/dashboard/VerfiyPoGrid", method = RequestMethod.GET)
	public String verifyPOGrid(@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page, @RequestParam("poNo") Optional<String> poNo,
			@RequestParam("status") Optional<String> status, @RequestParam("vendorName") Optional<String> vendorName,
			@RequestParam("issuedOn") Optional<String> issuedOn,
			@RequestParam("sortingkey") Optional<String> sortingkey,
			@RequestParam("sortingorder") Optional<String> sortingorder, Model model) {

		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;

		String date_format = "dd-MM-yyyy";
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_FORMAT");
		if (dateObj != null) {
			if (!dateObj.getPropValue().isEmpty()) {
				date_format = dateObj.getPropValue();
			}
		}

		Page<PurchaseOrder> list;
		List<PurchaseOrderUserAccess> pouser = poUsrAcc.getBypsIDAndAction(getCurrentUser().get(), "V");
		List<String> poNumber = new ArrayList<>();
		PurchaseOrder pos = new PurchaseOrder();
		List<String> purchaseorderlistFromPOMain = new ArrayList<>();

		/***
		 * Getting List of Po Numbers Under PSID and Authorize Action and Status
		 * from user accss and po_t_main table
		 ***/

		Calendar calendar = new GregorianCalendar();
	    String calendar_year = Integer.toString(calendar.get(Calendar.YEAR));
	    String logedin_year = getYear().get();
	    
	    //if loged in year is equal to current year, show list of pos from current year and last year
	      
	      if(logedin_year.equals(calendar_year)){
	        
	        for (PurchaseOrderUserAccess a : pouser) {
	          
	            String ponos_sub = a.getPurchaseorderuseraccessId().getPono().substring(5,7);
	                String calendar_year_sub = calendar_year.substring(2);
	                String logedin_year_sub = logedin_year.substring(2);
	                int last_year_sub = Integer.valueOf(logedin_year_sub)-1 ;
	                
	                if(ponos_sub.equals(calendar_year_sub) || ponos_sub.equals(Integer.toString(last_year_sub))){
	                    

	                    poNumber.add(a.getPurchaseorderuseraccessId().getPono());
	                    pos = purchaseorder.getPurchaseOrderByPoNo(a.getPurchaseorderuseraccessId().getPono());
	              
	              if(pos!=null){
	              if(pos.getStatus().equals("Issued")){
	                purchaseorderlistFromPOMain.add(pos.getPoNo());
	              }
	              }
	                    }
	        }
	        
	      }else{
	        
	        for (PurchaseOrderUserAccess a : pouser) {
	              
	              String ponos_sub = a.getPurchaseorderuseraccessId().getPono().substring(5,7);
	            
	              String logedin_year_sub = logedin_year.substring(2);
	              
	              if(ponos_sub.equals(logedin_year_sub)){
	              

	              poNumber.add(a.getPurchaseorderuseraccessId().getPono());
	              pos = purchaseorder.getPurchaseOrderByPoNo(a.getPurchaseorderuseraccessId().getPono());
	        
	        if(pos!=null){
	        if(pos.getStatus().equals("Issued")){
	          purchaseorderlistFromPOMain.add(pos.getPoNo());
	        }
	        }
	              }
	            } 
	            
	          
	        
	      }
		
		
		
		
		
		
		
		
	/*	for (PurchaseOrderUserAccess a : pouser) {

			poNumber.add(a.getPurchaseorderuseraccessId().getPono());
			pos = purchaseorder.getAllPurchaseOrderByPoNo(a.getPurchaseorderuseraccessId().getPono());

			if (pos != null) {
				if (pos.getStatus().equals("Issued")) {
					purchaseorderlistFromPOMain.add(pos.getPoNo());

				}
			}

		}*/

		Specification<PurchaseOrder> s = new Specification<PurchaseOrder>() {
			@Override
			public Predicate toPredicate(Root<PurchaseOrder> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> ls = new ArrayList<>();
				if (poNo.isPresent() && !poNo.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("poNo")), "%" + poNo.get().toLowerCase() + "%"));
				}
				if (status.isPresent() && !status.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("status")), "%" + status.get().toLowerCase() + "%"));

				}

				if (vendorName.isPresent() && !vendorName.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("vendorName")), "%" + vendorName.get().toLowerCase() + "%"));
				}

				if (issuedOn.isPresent() && !issuedOn.get().isEmpty()) {

					Expression<String> dateStringExpr = cb.function("to_char", String.class, root.get("issuedOn"),
							cb.literal("YYYY-MM-DD HH12:MI:SS"));
					ls.add(cb.like(cb.lower(dateStringExpr), "%" + issuedOn.get().toLowerCase() + "%"));
				}

				List<Predicate> temp = new ArrayList<>();
				for (String s : purchaseorderlistFromPOMain) {
					temp.add(cb.equal(root.get("poNo"), s));
				}
				ls.add(cb.or(temp.toArray(new Predicate[0])));
				return cb.and(ls.toArray(new Predicate[0]));
			}
		};

		Sort sorttemp = new Sort(Sort.Direction.DESC, "lastModifiedOn");

		if (sortingkey.isPresent() && sortingorder.isPresent()) {
			sorttemp = new Sort(sortingorder.get().equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC,
					sortingkey.get());
		}
		if (poNo.isPresent() || status.isPresent() || vendorName.isPresent()) {
			list = purchaseorder.getAllPageablePurchaseOrder(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		} else {
			list = purchaseorder.getAllPageablePurchaseOrder(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		}

		int serialid = 0;

		int pagesize = evalPage + 1;

		if (pagesize > 1) {
			int start = evalPageSize * pagesize;
			serialid = start - evalPageSize;
		}

		Pager pager = new Pager(list.getTotalPages(), list.getNumber(), BUTTONS_TO_SHOW);
		model.addAttribute("serialid", serialid);
		model.addAttribute("list", list);
		model.addAttribute("selectedPageSize", evalPageSize);
		model.addAttribute("pageSizes", PAGE_SIZES);
		model.addAttribute("pager", pager);
		model.addAttribute("totalRecords", list.getTotalElements());
		model.addAttribute("poNo", poNo.isPresent() ? poNo.get() : "");
		model.addAttribute("status", status.isPresent() ? status.get() : "");
		model.addAttribute("vendorName", vendorName.isPresent() ? vendorName.get() : "");
		model.addAttribute("issuedOn", issuedOn.isPresent() ? issuedOn.get() : "");
		model.addAttribute("sortingorder", sortingorder.isPresent() ? sortingorder.get() : "DESC");
		model.addAttribute("sortingkey", sortingkey.isPresent() ? sortingkey.get() : "lastModifiedOn");
		return "/main/po_dashboard :: result-table_verify";

	}
	
	
	@RequestMapping(value = "/dashboard/AuthorizePoGrid", method = RequestMethod.GET)
	public String authPOGrid(@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page, @RequestParam("poNo") Optional<String> poNo,
			@RequestParam("status") Optional<String> status, @RequestParam("vendorName") Optional<String> vendorName,
			@RequestParam("issuedOn") Optional<String> issuedOn,
			@RequestParam("sortingkey") Optional<String> sortingkey,
			@RequestParam("sortingorder") Optional<String> sortingorder, Model model) {

		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
		
		String date_format = "dd-MM-yyyy";
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_FORMAT");
		if(dateObj!=null){
			if(!dateObj.getPropValue().isEmpty()){
			date_format = dateObj.getPropValue();
			}
		}

		/*Here we need to check if psid in user access is same with current user then show the verified list*/
		Page<PurchaseOrder> list;
		
//		need to fetch list of po numbers based on action A and logged in user
		List<PurchaseOrderUserAccess> pouser = poUsrAcc.getBypsIDAndAction(getCurrentUser().get(), "A");
		List<String> poNumber = new ArrayList<>();
		PurchaseOrder pos = new PurchaseOrder();
		List<String> purchaseorderlistFromPOMain = new ArrayList<>();

		
		
		/***Getting List of Po Numbers Under PSID and Authorize Action and Status from user accss and po_t_main table ***/
		
		Calendar calendar = new GregorianCalendar();
	    String calendar_year = Integer.toString(calendar.get(Calendar.YEAR));
	    String logedin_year = getYear().get();
	    
	    //if loged in year is equal to current year, show list of pos from current year and last year
	      
	      if(logedin_year.equals(calendar_year)){
	        
	        for (PurchaseOrderUserAccess a : pouser) {
	          
	            String ponos_sub = a.getPurchaseorderuseraccessId().getPono().substring(5,7);
	                String calendar_year_sub = calendar_year.substring(2);
	                String logedin_year_sub = logedin_year.substring(2);
	                int last_year_sub = Integer.valueOf(logedin_year_sub)-1 ;
	                
	                if(ponos_sub.equals(calendar_year_sub) || ponos_sub.equals(Integer.toString(last_year_sub))){
	                    

	                    poNumber.add(a.getPurchaseorderuseraccessId().getPono());
	                    pos = purchaseorder.getPurchaseOrderByPoNo(a.getPurchaseorderuseraccessId().getPono());
	              
	              if(pos!=null){
	              if(pos.getStatus().equals("Verified")){
	                purchaseorderlistFromPOMain.add(pos.getPoNo());
	              }
	              }
	                    }
	        }
	        
	      }else{
	        
	        for (PurchaseOrderUserAccess a : pouser) {
	              
	              String ponos_sub = a.getPurchaseorderuseraccessId().getPono().substring(5,7);
	            
	              String logedin_year_sub = logedin_year.substring(2);
	              
	              if(ponos_sub.equals(logedin_year_sub)){
	              

	              poNumber.add(a.getPurchaseorderuseraccessId().getPono());
	              pos = purchaseorder.getPurchaseOrderByPoNo(a.getPurchaseorderuseraccessId().getPono());
	        
	        if(pos!=null){
	        if(pos.getStatus().equals("Verified")){
	          purchaseorderlistFromPOMain.add(pos.getPoNo());
	        }
	        }
	              }
	            } 
	            
	          
	        
	      }
		
		
		/*for (PurchaseOrderUserAccess a : pouser) {

			poNumber.add(a.getPurchaseorderuseraccessId().getPono());
			pos = purchaseorder.getAllPurchaseOrderByPoNo(a.getPurchaseorderuseraccessId().getPono());
			
				if(pos!=null){
				if(pos.getStatus().equals("Verified")){
					purchaseorderlistFromPOMain.add(pos.getPoNo());
				}
				
				}
			

		}*/
		Specification<PurchaseOrder> s = new Specification<PurchaseOrder>() {
			@Override
			public Predicate toPredicate(Root<PurchaseOrder> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> ls = new ArrayList<>();
				if (poNo.isPresent() && !poNo.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("poNo")), "%" + poNo.get().toLowerCase() + "%"));
				}
				if (status.isPresent() && !status.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("status")), "%" + status.get().toLowerCase() + "%"));

				}

				if (vendorName.isPresent() && !vendorName.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("vendorName")), "%" + vendorName.get().toLowerCase() + "%"));
				}
				
				
				if(issuedOn.isPresent() && !issuedOn.get().isEmpty()){
					
					Expression<String> dateStringExpr = cb.function("to_char", String.class, root.get("issuedOn"),cb.literal("YYYY-MM-DD HH12:MI:SS"));
				    ls.add(cb.like(cb.lower(dateStringExpr), "%" + issuedOn.get().toLowerCase() + "%"));
				}
				
				List<Predicate> temp = new ArrayList<>();
				for (String s : purchaseorderlistFromPOMain) {
					temp.add(cb.equal(root.get("poNo"), s));
				}
				ls.add(cb.or(temp.toArray(new Predicate[0])));
				return cb.and(ls.toArray(new Predicate[0]));
			}
		};

		Sort sorttemp = new Sort(Sort.Direction.DESC, "lastModifiedOn");

		if (sortingkey.isPresent() && sortingorder.isPresent()) {
			sorttemp = new Sort(sortingorder.get().equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC,
					sortingkey.get());
		}
		if (poNo.isPresent() || status.isPresent() || vendorName.isPresent()) {
			list = purchaseorder.getAllPageablePurchaseOrder(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		} else {
			list = purchaseorder.getAllPageablePurchaseOrder(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		}
		
		int serialid = 0;
		
		int pagesize = evalPage + 1;

		if(pagesize > 1){
			int start = evalPageSize * pagesize;
			serialid = start - evalPageSize;
		}
		
		
		Pager pager = new Pager(list.getTotalPages(), list.getNumber(), BUTTONS_TO_SHOW);
		model.addAttribute("serialid", serialid);
		model.addAttribute("list", list);
		model.addAttribute("selectedPageSize", evalPageSize);
		model.addAttribute("pageSizes", PAGE_SIZES);
		model.addAttribute("pager", pager);
		model.addAttribute("totalRecords", list.getTotalElements());
		model.addAttribute("poNo", poNo.isPresent() ? poNo.get() : "");
		model.addAttribute("status", status.isPresent() ? status.get() : "");
		model.addAttribute("vendorName", vendorName.isPresent() ? vendorName.get() : "");
		model.addAttribute("issuedOn", issuedOn.isPresent() ? issuedOn.get() : "");
		model.addAttribute("sortingorder", sortingorder.isPresent() ? sortingorder.get() : "DESC");
		model.addAttribute("sortingkey", sortingkey.isPresent() ? sortingkey.get() : "lastModifiedOn");
		return "/main/po_dashboard :: result-table_auth";

	}
	
	
	@RequestMapping(value = "/dashboard/DraftPoGrid", method = RequestMethod.GET)
	public String draftPOGrid(@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page, @RequestParam("poNo") Optional<String> poNo,
			@RequestParam("status") Optional<String> status, @RequestParam("vendorName") Optional<String> vendorName,
			@RequestParam("issuedOn") Optional<String> issuedOn,
			@RequestParam("sortingkey") Optional<String> sortingkey,
			@RequestParam("sortingorder") Optional<String> sortingorder, Model model) {

		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
		
		String date_format = "dd-MM-yyyy";
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_FORMAT");
		if(dateObj!=null){
			if(!dateObj.getPropValue().isEmpty()){
			date_format = dateObj.getPropValue();
			}
		}

		Page<PurchaseOrder> list;
		List<PurchaseOrderUserAccess> pouser = poUsrAcc.getBypsIDAndAction(getCurrentUser().get(), "D");
		List<String> poNumber = new ArrayList<>();
		PurchaseOrder pos = new PurchaseOrder();
		List<String> purchaseorderlistFromPOMain = new ArrayList<>();

		
		
		/***Getting List of Po Numbers Under PSID and Authorize Action and Status from user accss and po_t_main table ***/
		
		
		
		Calendar calendar = new GregorianCalendar();
	    String calendar_year = Integer.toString(calendar.get(Calendar.YEAR));
	    String logedin_year = getYear().get();
	    if(logedin_year.equals(calendar_year)){
	    	
	    	for (PurchaseOrderUserAccess a : pouser) {
	    		
	    			String ponos_sub = a.getPurchaseorderuseraccessId().getPono().substring(5,7);
	    	        String calendar_year_sub = calendar_year.substring(2);
	    	        String logedin_year_sub = logedin_year.substring(2);
	    	        int last_year_sub = Integer.valueOf(logedin_year_sub)-1 ;
	    	        
	    	        if(ponos_sub.equals(calendar_year_sub) || ponos_sub.equals(Integer.toString(last_year_sub))){
	    	            

	    	            poNumber.add(a.getPurchaseorderuseraccessId().getPono());
	    	            pos = purchaseorder.getPurchaseOrderByPoNo(a.getPurchaseorderuseraccessId().getPono());
	    				
	    				if(pos!=null){
	    				if(pos.getStatus().equals("Draft")){
	    					purchaseorderlistFromPOMain.add(pos.getPoNo());
	    				}
	    				}
	    	            }
	    	}
	    	
	    }else{
	    	
	    	for (PurchaseOrderUserAccess a : pouser) {
	            
	            String ponos_sub = a.getPurchaseorderuseraccessId().getPono().substring(5,7);
	          
	            String logedin_year_sub = logedin_year.substring(2);
	            
	            if(ponos_sub.equals(logedin_year_sub)){
	            

	            poNumber.add(a.getPurchaseorderuseraccessId().getPono());
	            pos = purchaseorder.getPurchaseOrderByPoNo(a.getPurchaseorderuseraccessId().getPono());
				
				if(pos!=null){
				if(pos.getStatus().equals("Draft")){
					purchaseorderlistFromPOMain.add(pos.getPoNo());
				}
				}
	            }
	          } 
	          
	        
	    	
	    }
		
		

		/*for (PurchaseOrderUserAccess a : pouser) {

			poNumber.add(a.getPurchaseorderuseraccessId().getPono());
			pos = purchaseorder.getPurchaseOrderByPoNo(a.getPurchaseorderuseraccessId().getPono());
			
				if(pos!=null){
				if(pos.getStatus().equals("Draft")){
					purchaseorderlistFromPOMain.add(pos.getPoNo());
				}
				}

		}*/

		Specification<PurchaseOrder> s = new Specification<PurchaseOrder>() {
			@Override
			public Predicate toPredicate(Root<PurchaseOrder> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> ls = new ArrayList<>();
				if (poNo.isPresent() && !poNo.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("poNo")), "%" + poNo.get().toLowerCase() + "%"));
				}
				if (status.isPresent() && !status.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("status")), "%" + status.get().toLowerCase() + "%"));

				}

				if (vendorName.isPresent() && !vendorName.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("vendorName")), "%" + vendorName.get().toLowerCase() + "%"));
				}
				
				
				if(issuedOn.isPresent() && !issuedOn.get().isEmpty()){
					
					Expression<String> dateStringExpr = cb.function("to_char", String.class, root.get("issuedOn"),cb.literal("YYYY-MM-DD HH12:MI:SS"));
				    ls.add(cb.like(cb.lower(dateStringExpr), "%" + issuedOn.get().toLowerCase() + "%"));
				}
				
				List<Predicate> temp = new ArrayList<>();
				for (String s : purchaseorderlistFromPOMain) {
					temp.add(cb.equal(root.get("poNo"), s));
				}
				ls.add(cb.or(temp.toArray(new Predicate[0])));
				return cb.and(ls.toArray(new Predicate[0]));
				
				
				
			}
		};

		Sort sorttemp = new Sort(Sort.Direction.DESC, "lastModifiedOn");

		if (sortingkey.isPresent() && sortingorder.isPresent()) {
			sorttemp = new Sort(sortingorder.get().equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC,
					sortingkey.get());
		}
		if (poNo.isPresent() || status.isPresent() || vendorName.isPresent()) {
			list = purchaseorder.getAllPageablePurchaseOrder(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		} else {
			list = purchaseorder.getAllPageablePurchaseOrder(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		}
		
		int serialid = 0;
		
		int pagesize = evalPage + 1;

		if(pagesize > 1){
			int start = evalPageSize * pagesize;
			serialid = start - evalPageSize;
		}
		
		Pager pager = new Pager(list.getTotalPages(), list.getNumber(), BUTTONS_TO_SHOW);
		model.addAttribute("serialid", serialid);
		model.addAttribute("list", list);
		model.addAttribute("selectedPageSize", evalPageSize);
		model.addAttribute("pageSizes", PAGE_SIZES);
		model.addAttribute("pager", pager);
		model.addAttribute("totalRecords", list.getTotalElements());
		model.addAttribute("poNo", poNo.isPresent() ? poNo.get() : "");
		model.addAttribute("status", status.isPresent() ? status.get() : "");
		model.addAttribute("vendorName", vendorName.isPresent() ? vendorName.get() : "");
		model.addAttribute("issuedOn", issuedOn.isPresent() ? issuedOn.get() : "");
		model.addAttribute("sortingorder", sortingorder.isPresent() ? sortingorder.get() : "DESC");
		model.addAttribute("sortingkey", sortingkey.isPresent() ? sortingkey.get() : "lastModifiedOn");
		return "/main/po_dashboard :: result-table-draft";

	}
	
	@RequestMapping(value = "/dashboard/RedraftPoGrid", method = RequestMethod.GET)
	public String redraftPOGrid(@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page, @RequestParam("poNo") Optional<String> poNo,
			@RequestParam("status") Optional<String> status, @RequestParam("vendorName") Optional<String> vendorName,
			@RequestParam("issuedOn") Optional<String> issuedOn,
			@RequestParam("sortingkey") Optional<String> sortingkey,
			@RequestParam("sortingorder") Optional<String> sortingorder, Model model) {

		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
		
		String date_format = "dd-MM-yyyy";
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_FORMAT");
		if(dateObj!=null){
			if(!dateObj.getPropValue().isEmpty()){
			date_format = dateObj.getPropValue();
			}
		}

		Page<PurchaseOrder> list;
		/*List<PurchaseOrderUserAccess> pouser = poUsrAcc.getBypsIDAndAction("0002040", "D");*/
		List<PurchaseOrderUserAccess> pouser = poUsrAcc.getBypsIDAndActionForRedraft(getCurrentUser().get());
		List<String> poNumber = new ArrayList<>();
		PurchaseOrder pos = new PurchaseOrder();
		List<String> purchaseorderlistFromPOMain = new ArrayList<>();

		
		
		/***Getting List of Po Numbers Under PSID and Authorize Action and Status from user accss and po_t_main table ***/
		
		Calendar calendar = new GregorianCalendar();
	    String calendar_year = Integer.toString(calendar.get(Calendar.YEAR));
	    String logedin_year = getYear().get();
	    
	
	
	/***Getting List of Po Numbers Under PSID and Authorize Action and Status from user accss and po_t_main table ***/
	    if(logedin_year.equals(calendar_year)){
	for (PurchaseOrderUserAccess a : pouser) {
		
		String ponos_sub = a.getPurchaseorderuseraccessId().getPono().substring(5,7);
        String calendar_year_sub = calendar_year.substring(2);
        String logedin_year_sub = logedin_year.substring(2);
        int last_year_sub = Integer.valueOf(logedin_year_sub)-1 ;
        
        if(ponos_sub.equals(calendar_year_sub) || ponos_sub.equals(Integer.toString(last_year_sub))){

		poNumber.add(a.getPurchaseorderuseraccessId().getPono());
		pos = purchaseorder.getAllPurchaseOrderByPoNo(a.getPurchaseorderuseraccessId().getPono());
		
			if(pos!=null){
			if(pos.getStatus().equals("Redraft")){
				purchaseorderlistFromPOMain.add(pos.getPoNo());
			}
			}
			
        }

	}
	    }else{
	    	
	    	 for (PurchaseOrderUserAccess a : pouser) {
	    		 
	    		 
	    		 String ponos_sub = a.getPurchaseorderuseraccessId().getPono().substring(5,7);
	             
	              String logedin_year_sub = logedin_year.substring(2);
	              
	              if(ponos_sub.equals(logedin_year_sub)){
	            	  
	            	  poNumber.add(a.getPurchaseorderuseraccessId().getPono());
	  				pos = purchaseorder.getAllPurchaseOrderByPoNo(a.getPurchaseorderuseraccessId().getPono());
	  				
	  					if(pos!=null){
	  					if(pos.getStatus().equals("Redraft")){
	  						purchaseorderlistFromPOMain.add(pos.getPoNo());
	  					}
	  					}
	            	  
	              }
	              
	    	 }
	    	
	    	
	    	
	    }
		
		
		
		
		
		
		
		
		
		
		
		/*for (PurchaseOrderUserAccess a : pouser) {

			poNumber.add(a.getPurchaseorderuseraccessId().getPono());
			pos = purchaseorder.getAllPurchaseOrderByPoNo(a.getPurchaseorderuseraccessId().getPono());
			
				if(pos!=null){
				if(pos.getStatus().equals("Redraft")){
					purchaseorderlistFromPOMain.add(pos.getPoNo());
				}
				}
				
			

		}*/
		Specification<PurchaseOrder> s = new Specification<PurchaseOrder>() {
			@Override
			public Predicate toPredicate(Root<PurchaseOrder> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> ls = new ArrayList<>();
				if (poNo.isPresent() && !poNo.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("poNo")), "%" + poNo.get().toLowerCase() + "%"));
				}
				if (status.isPresent() && !status.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("status")), "%" + status.get().toLowerCase() + "%"));

				}

				if (vendorName.isPresent() && !vendorName.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("vendorName")), "%" + vendorName.get().toLowerCase() + "%"));
				}
				
				
				if(issuedOn.isPresent() && !issuedOn.get().isEmpty()){
					
					Expression<String> dateStringExpr = cb.function("to_char", String.class, root.get("issuedOn"),cb.literal("YYYY-MM-DD HH12:MI:SS"));
				    ls.add(cb.like(cb.lower(dateStringExpr), "%" + issuedOn.get().toLowerCase() + "%"));
				}
				
				List<Predicate> temp = new ArrayList<>();
				for (String s : purchaseorderlistFromPOMain) {
					temp.add(cb.equal(root.get("poNo"), s));
				}
				ls.add(cb.or(temp.toArray(new Predicate[0])));
				return cb.and(ls.toArray(new Predicate[0]));
			}
		};

		Sort sorttemp = new Sort(Sort.Direction.DESC, "lastModifiedOn");

		if (sortingkey.isPresent() && sortingorder.isPresent()) {
			sorttemp = new Sort(sortingorder.get().equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC,
					sortingkey.get());
		}
		if (poNo.isPresent() || status.isPresent() || vendorName.isPresent()) {
			list = purchaseorder.getAllPageablePurchaseOrder(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		} else {
			list = purchaseorder.getAllPageablePurchaseOrder(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		}
		
		int serialid = 0;
		
		int pagesize = evalPage + 1;

		if(pagesize > 1){
			int start = evalPageSize * pagesize;
			serialid = start - evalPageSize;
		}
		
		Pager pager = new Pager(list.getTotalPages(), list.getNumber(), BUTTONS_TO_SHOW);
		model.addAttribute("serialid", serialid);
		model.addAttribute("list", list);
		model.addAttribute("selectedPageSize", evalPageSize);
		model.addAttribute("pageSizes", PAGE_SIZES);
		model.addAttribute("pager", pager);
		model.addAttribute("totalRecords", list.getTotalElements());
		model.addAttribute("poNo", poNo.isPresent() ? poNo.get() : "");
		model.addAttribute("status", status.isPresent() ? status.get() : "");
		model.addAttribute("vendorName", vendorName.isPresent() ? vendorName.get() : "");
		model.addAttribute("issuedOn", issuedOn.isPresent() ? issuedOn.get() : "");
		model.addAttribute("sortingorder", sortingorder.isPresent() ? sortingorder.get() : "DESC");
		model.addAttribute("sortingkey", sortingkey.isPresent() ? sortingkey.get() : "lastModifiedOn");
		return "/main/po_dashboard :: result-table-rdraft";

	}
	
	
	@RequestMapping(value = "/dashboard/InvoicePoGrid", method = RequestMethod.GET)
	public String invoicePOGrid(@RequestParam("pageSize") Optional<Integer> pageSize,
	@RequestParam("page") Optional<Integer> page, @RequestParam("poNo") Optional<String> poNo,
	@RequestParam("status") Optional<String> status, @RequestParam("vendorName") Optional<String> vendorName,
	@RequestParam("issuedOn") Optional<String> issuedOn,
	@RequestParam("sortingkey") Optional<String> sortingkey,
	@RequestParam("sortingorder") Optional<String> sortingorder, Model model) {
		
	int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
	int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
	
	String date_format = "dd-MM-yyyy";
	SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_FORMAT");
	if(dateObj!=null){
	if(!dateObj.getPropValue().isEmpty()){
	date_format = dateObj.getPropValue();
	}
	}
	
	Page<PurchaseOrder> list;
	List<String> action_list = new ArrayList<>();
	List<PurchaseOrderUserAccess> pouser = new ArrayList<>();
	List<String> pouser_list = new ArrayList<>(); // List of pos from userAccess with psid and action list
	
	List<String> poNumberListFromUserAccess = new ArrayList<>();
	PurchaseOrder pos = new PurchaseOrder();
	List<PurchaseOrder> pos_list = new ArrayList<>();
	List<String> purchaseorderlistFromPOMain = new ArrayList<>();
	
	Calendar calendar = new GregorianCalendar();
	String calendar_year = Integer.toString(calendar.get(Calendar.YEAR));
	String logedin_year = getYear().get();

	action_list.add("D");
	action_list.add("V");
	action_list.add("A");
	action_list.add("I");
	
	if(logedin_year.equals(calendar_year)){
		for(String action_obj: action_list){
		 pouser = poUsrAcc.getBypsIDAndAction(getCurrentUser().get(), action_obj);
		 
		
		 if(pouser!=null){
			 for (PurchaseOrderUserAccess a : pouser) {
				 
				 	String ponos_sub = a.getPurchaseorderuseraccessId().getPono().substring(5,7);
	                String calendar_year_sub = calendar_year.substring(2);
	                String logedin_year_sub = logedin_year.substring(2);
	                int last_year_sub = Integer.valueOf(logedin_year_sub)-1 ;
	                
				 if(ponos_sub.equals(calendar_year_sub) || ponos_sub.equals(Integer.toString(last_year_sub))){
					 pouser_list.add(a.getPurchaseorderuseraccessId().getPono());
				 }
			 }
			 }
		
		 
	}
	
	
	

	
	
	/***Getting List of Po Numbers Under PSID and Authorize Action and Status from user accss and po_t_main table ***/
	for (String a : pouser_list) {

		//poNumberListFromUserAccess.add(a);
		pos = purchaseorder.getAllPurchaseOrderByPoNo(a); // list of po obj from po main 
		
		if(pos!=null){
		pos_list.add(pos);
		if(pos.getStatus().equals("Authorized") || pos.getStatus().equals("Invoiced") ){
				purchaseorderlistFromPOMain.add(pos.getPoNo());
			}
		
		}
	
		//if(pos!=null){
		//if(pos.getStatus().equals("Authorized") || pos.getStatus().equals("Invoiced") ){
		//	purchaseorderlistFromPOMain.add(pos.getPoNo());
		//}
		
		//}
	
	
	}
	
	}else{
		for(String action_obj: action_list){
			 pouser = poUsrAcc.getBypsIDAndAction(getCurrentUser().get(), action_obj);
			 
			
			 if(pouser!=null){
				 for (PurchaseOrderUserAccess a : pouser) {
					 
					 String ponos_sub = a.getPurchaseorderuseraccessId().getPono().substring(5,7);
			            
		              String logedin_year_sub = logedin_year.substring(2);
		               
		                
		              if(ponos_sub.equals(logedin_year_sub)){
						 pouser_list.add(a.getPurchaseorderuseraccessId().getPono());
					 }
				 }
				 }
			
			 
		}

		
		
		/***Getting List of Po Numbers Under PSID and Authorize Action and Status from user accss and po_t_main table ***/
		for (String a : pouser_list) {
	
			//poNumberListFromUserAccess.add(a);
			pos = purchaseorder.getAllPurchaseOrderByPoNo(a); // list of po obj from po main 
			
			if(pos!=null){
			pos_list.add(pos);
			if(pos.getStatus().equals("Authorized") || pos.getStatus().equals("Invoiced") ){
				purchaseorderlistFromPOMain.add(pos.getPoNo());
			}
			}
		
			//if(pos!=null){
			//if(pos.getStatus().equals("Authorized") || pos.getStatus().equals("Invoiced") ){
				//purchaseorderlistFromPOMain.add(pos.getPoNo());
			//}
			
			//}
		
		
		}
	 
	 
 }
	
	/*if(pos!=null && !pos.getStatus().equals("Authorized")){
		
		disable_invoice = true;
	}else{
		
		disable_invoice =false;
	}
	*/
	Specification<PurchaseOrder> s = new Specification<PurchaseOrder>() {
	@Override
	public Predicate toPredicate(Root<PurchaseOrder> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
	List<Predicate> ls = new ArrayList<>();
	if (poNo.isPresent() && !poNo.get().isEmpty()) {
	ls.add(cb.like(cb.lower(root.get("poNo")), "%" + poNo.get().toLowerCase() + "%"));
	}
	if (status.isPresent() && !status.get().isEmpty()) {
	ls.add(cb.like(cb.lower(root.get("status")), "%" + status.get().toLowerCase() + "%"));
	
	}
	
	if (vendorName.isPresent() && !vendorName.get().isEmpty()) {
	ls.add(cb.like(cb.lower(root.get("vendorName")), "%" + vendorName.get().toLowerCase() + "%"));
	}
	
	
	if(issuedOn.isPresent() && !issuedOn.get().isEmpty()){
	
	Expression<String> dateStringExpr = cb.function("to_char", String.class, root.get("issuedOn"),cb.literal("YYYY-MM-DD HH12:MI:SS"));
	ls.add(cb.like(cb.lower(dateStringExpr), "%" + issuedOn.get().toLowerCase() + "%"));
	}
	
	
	List<Predicate> temp = new ArrayList<>();
	for (String s : purchaseorderlistFromPOMain) {
		
		temp.add(cb.equal(root.get("poNo"), s));
	
	}
	ls.add(cb.or(temp.toArray(new Predicate[0])));
	
	return cb.and(ls.toArray(new Predicate[0]));
	}
	};
	
	Sort sorttemp = new Sort(Sort.Direction.DESC, "lastModifiedOn");
	
	if (sortingkey.isPresent() && sortingorder.isPresent()) {
	sorttemp = new Sort(sortingorder.get().equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC,
	sortingkey.get());
	}
	if (poNo.isPresent() || status.isPresent() || vendorName.isPresent()) {
	list = purchaseorder.getAllPageablePurchaseOrder(s, new PageRequest(evalPage, evalPageSize, sorttemp));
	} else {
	list = purchaseorder.getAllPageablePurchaseOrder(s, new PageRequest(evalPage, evalPageSize, sorttemp));
	}
	
	int serialid = 0;
	
	int pagesize = evalPage + 1;

	if(pagesize > 1){
		int start = evalPageSize * pagesize;
		serialid = start - evalPageSize;
	}
	
	Pager pager = new Pager(list.getTotalPages(), list.getNumber(), BUTTONS_TO_SHOW);
	model.addAttribute("serialid", serialid);
	model.addAttribute("list", list);
	model.addAttribute("selectedPageSize", evalPageSize);
	model.addAttribute("pageSizes", PAGE_SIZES);
	model.addAttribute("pager", pager);
	model.addAttribute("totalRecords", list.getTotalElements());
	model.addAttribute("poNo", poNo.isPresent() ? poNo.get() : "");
	model.addAttribute("status", status.isPresent() ? status.get() : "");
	model.addAttribute("vendorName", vendorName.isPresent() ? vendorName.get() : "");
	model.addAttribute("issuedOn", issuedOn.isPresent() ? issuedOn.get() : "");
	model.addAttribute("sortingorder", sortingorder.isPresent() ? sortingorder.get() : "DESC");
	model.addAttribute("sortingkey", sortingkey.isPresent() ? sortingkey.get() : "lastModifiedOn");
	return "/main/po_dashboard :: result-table-invoice";
	
	}
	

	@Override
	protected String getHeaderTitle() {
		return "Dashboard";
	}

}