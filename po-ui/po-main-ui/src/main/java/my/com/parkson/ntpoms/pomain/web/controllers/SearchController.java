package my.com.parkson.ntpoms.pomain.web.controllers;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.UriUtils;

import my.com.parkson.ntpoms.common.entities.Department;
import my.com.parkson.ntpoms.common.entities.SystemParameter;
import my.com.parkson.ntpoms.common.model.Pager;
import my.com.parkson.ntpoms.common.services.OrderCategoryService;
import my.com.parkson.ntpoms.common.services.OrderSubCategoryService;
import my.com.parkson.ntpoms.common.services.ProfitCenterService;
import my.com.parkson.ntpoms.common.services.PurchaseOrderUserService;
import my.com.parkson.ntpoms.common.services.SystemParameterService;
import my.com.parkson.ntpoms.main.entities.PurchaseOrder;
import my.com.parkson.ntpoms.main.entities.PurchaseOrderArchiveDetails;
import my.com.parkson.ntpoms.main.entities.VendorBranch;
import my.com.parkson.ntpoms.main.services.ItemMapService;
import my.com.parkson.ntpoms.main.services.PurchaseOrderArchiveService;
import my.com.parkson.ntpoms.main.services.PurchaseOrderService;
import my.com.parkson.ntpoms.pomain.web.models.ItemForm;
import my.com.parkson.ntpoms.pomain.web.models.ItemInfoForm;
import my.com.parkson.ntpoms.pomain.web.models.PoArchiveForm;
import my.com.parkson.ntpoms.pomain.web.models.PoMainForm;
import my.com.parkson.ntpoms.utils.HibernateSearch;

@Controller
public class SearchController {

	private PoMainForm tempobj;

	private PoArchiveForm archiveobj;
	
	private ItemForm itemobj;

	@Autowired
	private OrderCategoryService orderCatService;

	@Autowired
	private OrderSubCategoryService orderSubCatService;

	@Autowired
	private PurchaseOrderUserService poUserService;

	@Autowired
	private PurchaseOrderService porepo;

	@Autowired
	private PurchaseOrderArchiveService poarchrepo;

	@Autowired
	private SystemParameterService sysRepo;

	@Autowired
	private ProfitCenterService profitCenterService;

	private Page<PurchaseOrder> list;

	private Page<PurchaseOrderArchiveDetails> archlist;
	
	@Autowired
	private ItemMapService itemMapRepo;

	private String authDir;
	private String orderAuthGrid;
	private String path_edit;
	private String newPath;
	private PurchaseOrder purchaseorder_view;
	private PoMainForm search_obj;

	private static String AUTHGRID_UPLOAD_PATH = "AUTHGRID_FOLDER_PATH";

	private static final String DATE_FORMAT_KEY = "DATE_FORMAT";
	private static final String DATE_FORMAT_VALUE = "dd-MM-yyyy";

	private static final int BUTTONS_TO_SHOW = 5;
	private static final int INITIAL_PAGE = 0;
	private static final int INITIAL_PAGE_SIZE = 5;
	private static final int[] PAGE_SIZES = { 5, 10, 20 };

	@GetMapping("/main/advanced_search")
	public ModelAndView onLoadAdvanceSearch() {
		String dateFormat = DATE_FORMAT_VALUE;
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName(DATE_FORMAT_KEY);
		if (dateObj != null && !dateObj.getPropValue().isEmpty()) {
			dateFormat = dateObj.getPropValue();
		}
		ModelAndView modelAndView = new ModelAndView("/main/advanced_search");
		modelAndView.getModelMap().addAttribute("searchobj", new PoMainForm());
		modelAndView.getModelMap().addAttribute("archiveobj", new PoArchiveForm());
		modelAndView.getModelMap().addAttribute("statusList", StatusList());
		modelAndView.getModelMap().addAttribute("fetchList", fetchList());
		modelAndView.addObject("ordCatlist", orderCatService.getAllActiveOrderCategory());
		modelAndView.addObject("ordSubClist", orderSubCatService.getAllActiveOrderSubCategory());
		modelAndView.addObject("poUserlist", poUserService.getAllActivePurchaseOrderUser());
		modelAndView.addObject("date_format", dateFormat);
		return modelAndView;
	}

	@GetMapping("/main/advancedsearch_result")
	public ModelAndView onLoadAdvanceSearchResult() {
		ModelAndView modelAndView = new ModelAndView("/main/advancedsearch_result");
		modelAndView.getModelMap().addAttribute("list", null);
		PoMainForm obj = search_obj != null ? search_obj : new PoMainForm();
		modelAndView.getModelMap().addAttribute("pomainobj", obj);
		return modelAndView;
	}
	
	
	private int getItemOrder(String poNo) {
		return itemMapRepo.getAllItemMapByPurchaseOrderNo(poNo).size() + 1;
	}

	@RequestMapping(value = "/main/AdvanceSearchGrid", method = RequestMethod.GET)
	public String advancedsearchGrid(@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page, @RequestParam("poNo") Optional<String> poNo,
			@RequestParam("vendorcode") Optional<String> vendorcode,
			@RequestParam("vendorName") Optional<String> vendorname, @RequestParam("pcCode") Optional<String> pccode,
			@RequestParam("issuer") Optional<String> issuer, @RequestParam("verifier") Optional<String> verifier,
			@RequestParam("authorizer") Optional<String> authorizer,
			@RequestParam("sortingkey") Optional<String> sortingkey,
			@RequestParam("sortingorder") Optional<String> sortingorder, Model model) {

		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
		Page<PurchaseOrder> result_list;
		Specification<PurchaseOrder> s = new Specification<PurchaseOrder>() {
			@Override
			public Predicate toPredicate(Root<PurchaseOrder> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> ls = new ArrayList<>();
				if (poNo.isPresent() && !poNo.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("poNo")), "%" + poNo.get().toLowerCase() + "%"));
				}
				if (vendorcode.isPresent() && !vendorcode.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("vendorcode")), "%" + vendorcode.get().toLowerCase() + "%"));
				}

				if (vendorname.isPresent() && !vendorname.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("vendorName")), "%" + vendorname.get().toLowerCase() + "%"));
				}

				if (pccode.isPresent() && !pccode.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("pcCode")), "%" + pccode.get().toLowerCase() + "%"));
				}

				if (issuer.isPresent() && !issuer.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("issuer")), "%" + issuer.get().toLowerCase() + "%"));
				}

				if (verifier.isPresent() && !verifier.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("verifier")), "%" + verifier.get().toLowerCase() + "%"));
				}
				if (authorizer.isPresent() && !authorizer.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("authorizer")), "%" + authorizer.get().toLowerCase() + "%"));
				}

				if (tempobj != null && tempobj.getPoNo() != null && !tempobj.getPoNo().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("poNo")), "%" + tempobj.getPoNo().toLowerCase() + "%"));
				}
				if (tempobj != null && tempobj.getVerifier() != null && !tempobj.getVerifier().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("verifier")), "%" + tempobj.getVerifier().toLowerCase() + "%"));
				}
				if (tempobj != null && tempobj.getAuthorizer() != null && !tempobj.getAuthorizer().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("authorizer")), "%" + tempobj.getAuthorizer().toLowerCase() + "%"));
				}
				if (tempobj != null && tempobj.getIssuer() != null && !tempobj.getIssuer().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("issuer")), "%" + tempobj.getIssuer().toLowerCase() + "%"));
				}
				if (tempobj != null && tempobj.getVendorCode() != null && !tempobj.getVendorCode().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("vendorcode")),
							"%" + tempobj.getVendorCode().toLowerCase() + "%"));
				}
				if (tempobj != null && tempobj.getVendorName() != null && !tempobj.getVendorName().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("vendorName")),
							"%" + tempobj.getVendorName().toLowerCase() + "%"));
				}

				if (tempobj != null && tempobj.getStatus() != null && !tempobj.getStatus().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("status")), "%" + tempobj.getStatus().toLowerCase() + "%"));
				}

				if (tempobj != null && tempobj.getOrdCatCode() != null && !tempobj.getOrdCatCode().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("orderCatCode")),
							"%" + tempobj.getOrdCatCode().toLowerCase() + "%"));
				}

				if (tempobj != null && tempobj.getOrdSubCCode() != null && !tempobj.getOrdSubCCode().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("ordSubCatCode")),
							"%" + tempobj.getOrdSubCCode().toLowerCase() + "%"));
				}

				if (tempobj != null && tempobj.getRejectedBy() != null && !tempobj.getRejectedBy().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("rejectedBy")),
							"%" + tempobj.getRejectedBy().toLowerCase() + "%"));
				}

				if (tempobj != null && tempobj.getDeletedBy() != null && !tempobj.getDeletedBy().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("deletedBy")), "%" + tempobj.getDeletedBy().toLowerCase() + "%"));
				}

				if (tempobj != null && tempobj.getCancelledBy() != null && !tempobj.getCancelledBy().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("cancelledBy")),
							"%" + tempobj.getCancelledBy().toLowerCase() + "%"));
				}
				if (tempobj != null && tempobj.getClosedBy() != null && !tempobj.getClosedBy().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("closedBy")), "%" + tempobj.getClosedBy().toLowerCase() + "%"));
				}

				if (tempobj != null && tempobj.getPcCode() != null && !tempobj.getPcCode().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("pcCode")), "%" + tempobj.getPcCode().toLowerCase() + "%"));
				}
				
				if (tempobj.getTotInclGSTMax()>0) { 
					ls.add(cb.equal(root.get("totalInclGST"), tempobj.getTotInclGSTMax()));
				}
				
				if (tempobj.getTotInclGSTMin()>0) { 
					ls.add(cb.equal(root.get("totalExclGST"), tempobj.getTotInclGSTMin()));
				}

				if (tempobj != null && tempobj.getIssuedOn() != null && !tempobj.getIssuedOn().isEmpty()) {
					Expression<String> dateStringExpr = cb.function("to_char", String.class, root.get("issuedOn"),
							cb.literal("YYYY-MM-DD HH12:MI:SS"));
					ls.add(cb.like(cb.lower(dateStringExpr), "%" + tempobj.getIssuedOn().toLowerCase() + "%"));
				}

				return cb.and(ls.toArray(new Predicate[0]));

			}
		};

		Sort sorttemp = new Sort(Sort.Direction.DESC, "poNo");

		if (sortingkey.isPresent() && sortingorder.isPresent()) {
			sorttemp = new Sort(sortingorder.get().equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC,
					sortingkey.get());
		}
		if (poNo.isPresent() || vendorcode.isPresent() || vendorname.isPresent() || pccode.isPresent()
				|| issuer.isPresent() || verifier.isPresent() || authorizer.isPresent()) {
			result_list = porepo.getAllPageablePurchaseOrder(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		} else {
			result_list = porepo.getAllPageablePurchaseOrder(new PageRequest(evalPage, evalPageSize, sorttemp));
		}
		Pager pager = new Pager(result_list.getTotalPages(), result_list.getNumber(), BUTTONS_TO_SHOW);
		model.addAttribute("list", result_list);
		model.addAttribute("selectedPageSize", evalPageSize);
		model.addAttribute("pageSizes", PAGE_SIZES);
		model.addAttribute("pager", pager);
		model.addAttribute("totalRecords", result_list.getTotalElements());
		model.addAttribute("poNo", poNo.isPresent() ? poNo.get() : "");
		model.addAttribute("issuer", issuer.isPresent() ? issuer.get() : "");
		model.addAttribute("verifier", verifier.isPresent() ? verifier.get() : "");
		model.addAttribute("authorizer", authorizer.isPresent() ? authorizer.get() : "");
		model.addAttribute("pcCode", pccode.isPresent() ? pccode.get() : "");
		model.addAttribute("vendorcode", vendorcode.isPresent() ? vendorcode.get() : "");
		model.addAttribute("vendorName", vendorname.isPresent() ? vendorname.get() : "");

		model.addAttribute("sortingorder", sortingorder.isPresent() ? sortingorder.get() : "DESC");
		model.addAttribute("sortingkey", sortingkey.isPresent() ? sortingkey.get() : "poNo");

		return "/main/advancedsearch_result :: result-table";

	}

	
	private PoMainForm entityToPojo(PurchaseOrder purchaseorder) throws ParseException, UnsupportedEncodingException {
		PoMainForm obj = new PoMainForm();

		SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_FORMAT");
		String dateFormat = "dd-MM-yyyy";
		if (dateObj != null && !dateObj.getPropValue().isEmpty()) {
			dateFormat = dateObj.getPropValue();
		}
		DateFormat formatter = new SimpleDateFormat(dateFormat);

		String date_format = "dd-MM-yyyy hh:mm:ss";
		SystemParameter dateObj_1 = sysRepo.getSystemParameterByPropName("DATE_TIME_FORMAT");
		if (dateObj_1 != null) {
			if (!dateObj_1.getPropValue().isEmpty()) {
				date_format = dateObj_1.getPropValue();
			}
		}
		DateFormat formatter_1 = new SimpleDateFormat(date_format);

		obj.setContactPersonEmail(purchaseorder.getContactPersonEmail());
		obj.setContactPersonPhone(purchaseorder.getContactPersonPhone());
		obj.setContactPersonName(purchaseorder.getContactPersonName());
		if (purchaseorder.getContactPerson() != null) {
			obj.setContactPerson(purchaseorder.getContactPerson() + "-" + purchaseorder.getContactPersonName());
		}
		obj.setAddressLine1(purchaseorder.getAddressLine1());
		obj.setAddressLine2(purchaseorder.getAddressLine2());
		obj.setAddressLine3(purchaseorder.getAddressLine3());
		obj.setPostCode(purchaseorder.getPostCode());
		obj.setPostArea(purchaseorder.getPostArea());
		obj.setVendorCode(purchaseorder.getVendorcode() + "-" + purchaseorder.getVendorName() + "-"
				+ purchaseorder.getVendorbranchcode());
		obj.setVendorPersonInCharge(purchaseorder.getVendorPersonInCharge());
		obj.setVendorPersonInChargeEmail(purchaseorder.getVendorPersonInChargeEmail());
		obj.setVendorPersonInChargePhone(purchaseorder.getVendorPersonInChargePhone());
		obj.setVendorAddressLine1(purchaseorder.getVendorAddressLine1());
		obj.setVendorAddressLine2(purchaseorder.getVendorAddressLine2());
		obj.setVendorAddressLine3(purchaseorder.getVendorAddressLine3());
		obj.setVendorPostCode(purchaseorder.getPostCode());
		obj.setVendorPostArea(purchaseorder.getVendorPostArea());
		obj.setPoNo(purchaseorder.getPoNo());
		String pcName = profitCenterService.getProfitCenterByCode(purchaseorder.getPcCode()).getName();
		String stcode = profitCenterService.getProfitCenterByCode(purchaseorder.getPcCode()).getStore().getCode();
		String deptcode = profitCenterService.getProfitCenterByCode(purchaseorder.getPcCode()).getDepartment()
				.getCode();
		obj.setProfitcenter(purchaseorder.getPcCode() + "-" + pcName);
		obj.setStoreName(stcode + "-" + purchaseorder.getStoreName());
		obj.setDepartmentName(deptcode + "-" + purchaseorder.getDepartmentName());
		obj.setStatus(purchaseorder.getStatus());
		obj.setCompCode(purchaseorder.getCompCode());

		if (purchaseorder.getCreditTermsCode() != null) {
			obj.setCredittermscode(purchaseorder.getCreditTermsCode());
			obj.setLastcrt_used_hidden(purchaseorder.getCreditTermsCode());
			//crt_edit = purchaseorder.getCreditTermsCode();
		}

		obj.setCurrency(purchaseorder.getCurrency());
		obj.setOrdCatCode(purchaseorder.getOrderCatCode());
		obj.setOrdSubCCode(purchaseorder.getOrdSubCatCode());
		obj.setPrTypeCode(purchaseorder.getPrTypeCode());
		obj.setQuoteNo(purchaseorder.getQuoteNo());
		if (purchaseorder.getQuoteDate() != null) {
			obj.setQuoteDate(formatter.format(purchaseorder.getQuoteDate()));
		}

		obj.setProjectCode(purchaseorder.getProjectCode());
		obj.setPaymentTerms(purchaseorder.getPaymentTerms());
		obj.setDeliveryInstruction(purchaseorder.getDeliveryInstruction());
		obj.setIssuerRemark(purchaseorder.getIssuerRemark());

		if (purchaseorder.getOrderCategoryAuthorizationGrid() != null) {
			obj.setOrderCategoryAuthorizationGrid(purchaseorder.getOrderCategoryAuthorizationGrid());
			orderAuthGrid = purchaseorder.getOrderCategoryAuthorizationGrid();

			authDir = sysRepo.getSystemParameterValueByPropName(AUTHGRID_UPLOAD_PATH);
			path_edit = UriUtils.encodePath(authDir + orderAuthGrid, "UTF-8");
			newPath = UriUtils.encodePath(authDir + orderAuthGrid, "UTF-8");

		}

		obj.setVendorbranchCode(purchaseorder.getVendorbranchcode());
		obj.setVendorName(purchaseorder.getVendorName());
		obj.setIssuer(purchaseorder.getIssuer());
		obj.setVerifier(purchaseorder.getVerifier());
		obj.setAuthorizer(purchaseorder.getAuthorizer());
		obj.setFirstApprover(purchaseorder.getFirstApprover());
		obj.setSecondApprover(purchaseorder.getSecondApprover());
		obj.setThirdApprover(purchaseorder.getThirdApprover());
		obj.setFirstApproverDesignation(purchaseorder.getFirstApproverDesignation());
		obj.setSecondApproverDesignation(purchaseorder.getSecondApproverDesignation());
		obj.setThirdApproverDesignation(purchaseorder.getThirdApproverDesignation());

		if (purchaseorder.getIssuedOn() != null) {
			obj.setIssuedOn(formatter_1.format(purchaseorder.getIssuedOn()));
		}
		if (purchaseorder.getVerifiedOn() != null) {
			obj.setVerifiedOn(formatter_1.format(purchaseorder.getVerifiedOn()));
		}
		if (purchaseorder.getAuthorizedOn() != null) {
			obj.setAuthorizedOn(formatter_1.format(purchaseorder.getAuthorizedOn()));
		}

		if (purchaseorder.getRejectedOn() != null) {
			obj.setRejectedOn(formatter_1.format(purchaseorder.getRejectedOn()));
		}
		if (purchaseorder.getDeletedOn() != null) {
			obj.setDeletedOn(formatter_1.format(purchaseorder.getDeletedOn()));
		}

		if (purchaseorder.getCancelledOn() != null) {
			obj.setCancelledOn(formatter_1.format(purchaseorder.getCancelledOn()));
		}

		if (purchaseorder.getDeletedBy() != null) {
			obj.setDeletedBy(purchaseorder.getDeletedBy());

		}

		if (purchaseorder.getRejectedBy() != null) {

			obj.setRejectedBy(purchaseorder.getRejectedBy());
		}

		if (purchaseorder.getClosedBy() != null) {
			obj.setClosedBy(purchaseorder.getClosedBy());
		}

		if (purchaseorder.getCancelledBy() != null) {
			obj.setCancelledBy(purchaseorder.getCancelledBy());
		}

		obj.setIssuerRemark(purchaseorder.getIssuerRemark());
		obj.setVerifyRemark(purchaseorder.getVerifyRemark());
		obj.setAuthorizeRemark(purchaseorder.getAuthorizeRemark());
		obj.setRedraftRemark(purchaseorder.getRedraftRemark());
		obj.setRejectRemark(purchaseorder.getRejectRemark());
		obj.setCancelRemark(purchaseorder.getCancelRemark());
		obj.setDeleteRemark(purchaseorder.getDeleteRemark());
		obj.setCloseException(purchaseorder.getCloseException());

		return obj;
	}


	@PostMapping(value = "/main/AdvanceSearchSubmit")
	public String searchsubmit(@Valid @ModelAttribute("searchobj") PoMainForm obj, BindingResult result,
			RedirectAttributes redirectAttributes, Model model) {
		this.tempobj = obj;
		return "redirect:/main/advancedsearch_result";
	}

	public List<String> StatusList() {

		List<String> status = new ArrayList<>();
		status.add("Draft");
		status.add("Issued");
		status.add("Verified");
		status.add("Authorized");
		status.add("Invoiced");
		status.add("Redraft");
		status.add("Reject");
		status.add("Delete");
		status.add("Closed");
		status.add("Cancelled");

		return status;
	}

	public List<Integer> fetchList() {
		List<Integer> fetchList = new ArrayList<>();

		fetchList.add(50);
		fetchList.add(100);
		fetchList.add(150);
		return fetchList;

	}

	/***************************************
	 * Archive Search
	 **************************************/

	@GetMapping("/main/ArchiveAdvanceSearch")
	public ModelAndView onLoadArchiceAdvanceSearch() {

		String dateFormat = DATE_FORMAT_VALUE;
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName(DATE_FORMAT_KEY);
		if (dateObj != null && !dateObj.getPropValue().isEmpty()) {
			dateFormat = dateObj.getPropValue();
		}

		ModelAndView modelAndView = new ModelAndView("/main/archive_search");

		modelAndView.getModelMap().addAttribute("archiveobj", new PoArchiveForm());

		modelAndView.addObject("poUserlist", poUserService.getAllActivePurchaseOrderUser());
		modelAndView.addObject("date_format", dateFormat);

		return modelAndView;

	}

	@GetMapping("/main/archivesearch_result")
	public ModelAndView onLoadArchiveSearchResult() {
		ModelAndView modelAndView = new ModelAndView("/main/archivesearch_result");

		modelAndView.getModelMap().addAttribute("archlist", null);

		return modelAndView;
	}

	@PostMapping("/main/ArchiveSearchSubmit")
	public String archiveSearchSubmit(@Valid @ModelAttribute("archiveobj") PoArchiveForm obj, BindingResult result,
			Model model, RedirectAttributes redirectAttributes) {

		this.archiveobj = obj;
		return "redirect:/main/archivesearch_result";

	}

	@RequestMapping(value = "/main/archivesearchresult", method = RequestMethod.GET)
	public String archivesearchGrid(@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page, @RequestParam("poNo") Optional<String> poNo,
			@RequestParam("vendorCode") Optional<String> vendorcode,
			@RequestParam("vendorName") Optional<String> vendorname, @RequestParam("pcCode") Optional<String> pccode,
			@RequestParam("issuerPsid") Optional<String> issuer, @RequestParam("compCode") Optional<String> compcode,

			@RequestParam("sortingkey") Optional<String> sortingkey,
			@RequestParam("sortingorder") Optional<String> sortingorder, Model model) {
		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
		Page<PurchaseOrderArchiveDetails> result_arch_list;
		Specification<PurchaseOrderArchiveDetails> s = new Specification<PurchaseOrderArchiveDetails>() {
			@Override
			public Predicate toPredicate(Root<PurchaseOrderArchiveDetails> root, CriteriaQuery<?> query,
					CriteriaBuilder cb) {
				List<Predicate> ls = new ArrayList<>();
				if (poNo.isPresent() && !poNo.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("poNo")), "%" + poNo.get().toLowerCase() + "%"));
				}

				if (vendorcode.isPresent() && !vendorcode.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("vendorCode")), "%" + vendorcode.get().toLowerCase() + "%"));
				}

				if (vendorname.isPresent() && !vendorname.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("vendorName")), "%" + vendorname.get().toLowerCase() + "%"));
				}

				if (pccode.isPresent() && !pccode.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("pcCode")), "%" + pccode.get().toLowerCase() + "%"));
				}

				if (issuer.isPresent() && !issuer.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("issuerPsid")), "%" + issuer.get().toLowerCase() + "%"));
				}

				if (compcode.isPresent() && !compcode.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("compCode")), "%" + compcode.get().toLowerCase() + "%"));
				}

				if (archiveobj != null && archiveobj.getPoNo() != null && !archiveobj.getPoNo().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("poNo")), "%" + archiveobj.getPoNo().toLowerCase() + "%"));
				}

				if (archiveobj != null && archiveobj.getVendorCode() != null && !archiveobj.getVendorCode().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("vendorCode")),
							"%" + archiveobj.getVendorCode().toLowerCase() + "%"));
				}

				if (archiveobj != null && archiveobj.getVendorName() != null && !archiveobj.getVendorName().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("vendorName")),
							"%" + archiveobj.getVendorName().toLowerCase() + "%"));
				}

				if (archiveobj != null && archiveobj.getPcCode() != null && !archiveobj.getPcCode().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("pcCode")), "%" + archiveobj.getPcCode().toLowerCase() + "%"));
				}
				
				if (archiveobj != null && archiveobj.getCompCode() != null && !archiveobj.getCompCode().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("compCode")), "%" + archiveobj.getCompCode().toLowerCase() + "%"));
				}
				
				if (archiveobj != null && archiveobj.getIssuer() != null && !archiveobj.getIssuer().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("issuerPsid")), "%" + archiveobj.getIssuer().toLowerCase() + "%"));
				}
				
				if (archiveobj != null && archiveobj.getIssuerName() != null && !archiveobj.getIssuerName().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("issuerName")), "%" + archiveobj.getIssuerName().toLowerCase() + "%"));
				}
				
				if (archiveobj != null && archiveobj.getArchivedYear() != null && !archiveobj.getArchivedYear().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("archivedYear")), "%" + archiveobj.getArchivedYear().toLowerCase() + "%"));
				}
				
				if (archiveobj.getTotalAmt() !=0) {
					
					ls.add(cb.equal(root.get("totalAmt"), archiveobj.getTotalAmt()));
					
				}
				
				if (archiveobj != null && archiveobj.getCreatedOn() != null && !archiveobj.getCreatedOn().isEmpty()) {
					Expression<String> dateStringExpr = cb.function("to_char", String.class, root.get("createdOn"),
							cb.literal("YYYY-MM-DD HH12:MI:SS"));
					ls.add(cb.like(cb.lower(dateStringExpr), "%" + archiveobj.getCreatedOn().toLowerCase() + "%"));
				}

				return cb.and(ls.toArray(new Predicate[0]));
			}
		};

		Sort sorttemp = new Sort(Sort.Direction.DESC, "poNo");

		if (sortingkey.isPresent() && sortingorder.isPresent()) {
			sorttemp = new Sort(sortingorder.get().equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC,
					sortingkey.get());
		}

		if (poNo.isPresent() || vendorcode.isPresent() || vendorname.isPresent() || pccode.isPresent()
				|| issuer.isPresent() || compcode.isPresent()) {
			result_arch_list = poarchrepo.getAllPageablePurchaseOrder(s,
					new PageRequest(evalPage, evalPageSize, sorttemp));
		} else {
			result_arch_list = poarchrepo
					.getAllPageablePurchaseOrder(new PageRequest(evalPage, evalPageSize, sorttemp));
		}

		Pager pager = new Pager(result_arch_list.getTotalPages(), result_arch_list.getNumber(), BUTTONS_TO_SHOW);
		model.addAttribute("archlist", result_arch_list);
		model.addAttribute("selectedPageSize", evalPageSize);
		model.addAttribute("pageSizes", PAGE_SIZES);
		model.addAttribute("pager", pager);
		model.addAttribute("totalRecords", result_arch_list.getTotalElements());
		model.addAttribute("poNo", poNo.isPresent() ? poNo.get() : "");
		model.addAttribute("issuerPsid", issuer.isPresent() ? issuer.get() : "");
		model.addAttribute("compCode", compcode.isPresent() ? compcode.get() : "");
		model.addAttribute("pcCode", pccode.isPresent() ? pccode.get() : "");
		model.addAttribute("vendorCode", vendorcode.isPresent() ? vendorcode.get() : "");
		model.addAttribute("vendorName", vendorname.isPresent() ? vendorname.get() : "");

		model.addAttribute("sortingorder", sortingorder.isPresent() ? sortingorder.get() : "DESC");
		model.addAttribute("sortingkey", sortingkey.isPresent() ? sortingkey.get() : "poNo");

		return "/main/archivesearch_result::result-table";

	}

}