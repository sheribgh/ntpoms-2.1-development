/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.pomain.web.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

import my.com.parkson.ntpoms.pomain.login.User;

/**
 * The Class AbstractSiteController.
 */
public abstract class AbstractMainController {

	public static Optional<String> getCurrentUser() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String username = null;
		if (auth != null && !auth.getClass().equals(AnonymousAuthenticationToken.class)) {
			User user = (User) auth.getPrincipal();
			username = user.getUsername();
		}
		return Optional.ofNullable(username);
	}

	public static List<String> getRoleListCurrentUser() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		List<String> rolelist = new ArrayList<>();
		if (auth != null && !auth.getClass().equals(AnonymousAuthenticationToken.class)) {
			User user = (User) auth.getPrincipal();
			user.getAuthorities().forEach(r -> rolelist.add(r.getAuthority()));
		}
		return rolelist;
	}

	
	
	
	public static boolean isLoggedIn() {
		return getCurrentUser().isPresent();
	}

	public static Optional<String> getCompany() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String company = null;
		if (auth != null && !auth.getClass().equals(AnonymousAuthenticationToken.class)) {
			User user = (User) auth.getPrincipal();
			company = user.getCompany();
		}
		return Optional.ofNullable(company);
	}

	public static Optional<String> getYear() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String year = null;
		if (auth != null && !auth.getClass().equals(AnonymousAuthenticationToken.class)) {
			User user = (User) auth.getPrincipal();
			year = user.getYear();
		}
		return Optional.ofNullable(year);
	}

	@Autowired
	protected MessageSource messageSource;

	protected abstract String getHeaderTitle();

	/**
	 * Gets the message.
	 *
	 * @param code
	 *            the code
	 * @return the message
	 */
	public String getMessage(String code) {
		return messageSource.getMessage(code, null, null);
	}

	/**
	 * Gets the message.
	 *
	 * @param code
	 *            the code
	 * @param defaultMsg
	 *            the default msg
	 * @return the message
	 */
	public String getMessage(String code, String defaultMsg) {
		return messageSource.getMessage(code, null, defaultMsg, null);
	}

	/**
	 * Header title.
	 *
	 * @return the string
	 */
	@ModelAttribute("headerTitle")
	public String headerTitle() {
		return getHeaderTitle();
	}

}
