package my.com.parkson.ntpoms.pomain.web.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import my.com.parkson.ntpoms.common.entities.EmployeeMaster;
import my.com.parkson.ntpoms.common.entities.ProfitCenter;
import my.com.parkson.ntpoms.common.services.EmployeeMasterService;
import my.com.parkson.ntpoms.common.services.ProfitCenterService;
import my.com.parkson.ntpoms.main.entities.Invoice;
import my.com.parkson.ntpoms.main.entities.ProfitCenterLastPONumber;
import my.com.parkson.ntpoms.main.entities.PurchaseOrder;
import my.com.parkson.ntpoms.main.entities.PurchaseOrderUserAccess;
import my.com.parkson.ntpoms.main.entities.PurchaseOrderUserAccessPk;
//import my.com.parkson.ntpoms.main.entities.PurchaseOrderUserAccessPk;
import my.com.parkson.ntpoms.main.entities.VendorBranch;
import my.com.parkson.ntpoms.main.entities.VendorBranchId;
import my.com.parkson.ntpoms.main.services.InvoiceService;
import my.com.parkson.ntpoms.main.services.PcLastPONumberService;
import my.com.parkson.ntpoms.main.services.PurchaseOrderService;
import my.com.parkson.ntpoms.main.services.PurchaseOrderUserAccessService;
import my.com.parkson.ntpoms.main.services.VendorBranchService;
import my.com.parkson.ntpoms.pomain.web.controllers.AbstractMainController;
import my.com.parkson.ntpoms.utils.ApplicationContextProvider;

public class PurchaseOrderActivitiesUtil {
	private static final Logger logger = LoggerFactory.getLogger(PurchaseOrderActivitiesUtil.class);

	private static final String AUTHORIZED_STATUS = "Authorized";

	private String currentUser;

	private EmployeeMasterService employeeMasterService;
	private ProfitCenterService profitCenterService;
	private VendorBranchService vendorBranchService;
	private PurchaseOrderService purchaseOrderService;
	private PurchaseOrderUserAccessService purchaseOrderUserAccessService;
	private InvoiceService invoiceService;
	private PcLastPONumberService pcLastPONumberService;

	public PurchaseOrderActivitiesUtil() {
		if (employeeMasterService == null) {
			employeeMasterService = ApplicationContextProvider.getContext().getBean(EmployeeMasterService.class);
		}
		if (profitCenterService == null) {
			profitCenterService = ApplicationContextProvider.getContext().getBean(ProfitCenterService.class);
		}
		if (vendorBranchService == null) {
			vendorBranchService = ApplicationContextProvider.getContext().getBean(VendorBranchService.class);
		}
		if (purchaseOrderService == null) {
			purchaseOrderService = ApplicationContextProvider.getContext().getBean(PurchaseOrderService.class);
		}
		if (purchaseOrderUserAccessService == null) {
			purchaseOrderUserAccessService = ApplicationContextProvider.getContext()
					.getBean(PurchaseOrderUserAccessService.class);
		}
		if (invoiceService == null) {
			invoiceService = ApplicationContextProvider.getContext().getBean(InvoiceService.class);
		}
		if (pcLastPONumberService == null) {
			pcLastPONumberService = ApplicationContextProvider.getContext().getBean(PcLastPONumberService.class);
		}

	}

	public void savePurchaseOrder(PurchaseOrder purchaseOrder) {
		EmployeeMaster employeeMaster = employeeMasterService.getEmployeeMasterByPsId(purchaseOrder.getContactPerson());
		if (employeeMaster != null) {
			employeeMasterService.createEmployeeMaster(toEmployeeMasterObject(purchaseOrder, employeeMaster));
		}
		VendorBranchId vendorBranchId = new VendorBranchId(purchaseOrder.getVendorcode(),
				purchaseOrder.getVendorbranchcode(), purchaseOrder.getCompCode());
		VendorBranch vendorBranch = vendorBranchService.getVendorBranchbyVendorId(vendorBranchId);
		if (vendorBranch != null) {
			vendorBranchService.createVendorBranch(toVendorBranchObject(purchaseOrder, vendorBranch));
		}
		purchaseOrderService.createPurchaseOrder(purchaseOrder);
	}

	private VendorBranch toVendorBranchObject(PurchaseOrder purchaseOrder, VendorBranch vendorBranch) {
		if (purchaseOrder.getVendorName() != null) {
			vendorBranch.setName(purchaseOrder.getVendorName());
		}
		if (purchaseOrder.getVendorPersonInCharge() != null) {
			vendorBranch.setLastContactPersonName(purchaseOrder.getVendorPersonInCharge());
		}
		if (purchaseOrder.getVendorPersonInChargePhone() != null) {
			vendorBranch.setLastContactPersonContactNumber(purchaseOrder.getVendorPersonInChargePhone());
		}
		if (purchaseOrder.getVendorPersonInChargeEmail() != null) {
			vendorBranch.setLastContactPersonEmailAddress(purchaseOrder.getVendorPersonInChargeEmail());
		}
		if (purchaseOrder.getVendorAddressLine1() != null) {
			vendorBranch.setAddressLine1(purchaseOrder.getVendorAddressLine1());
		}
		if (purchaseOrder.getVendorAddressLine2() != null) {
			vendorBranch.setAddressLine2(purchaseOrder.getVendorAddressLine2());
		}
		if (purchaseOrder.getVendorAddressLine3() != null) {
			vendorBranch.setAddressLine3(purchaseOrder.getVendorAddressLine3());
		}
		if (purchaseOrder.getVendorPostCode() != null) {
			vendorBranch.setPostCode(purchaseOrder.getVendorPostCode());
		}
		if (purchaseOrder.getVendorPostArea() != null) {
			vendorBranch.setPostCodeArea(purchaseOrder.getVendorPostArea());
		}
		if (purchaseOrder.getCurrency() != null) {
			vendorBranch.setLastPOCurrency(purchaseOrder.getCurrency());
		}
		if (purchaseOrder.getCreditTermsCode() != null) {
			vendorBranch.setCreditTermsCode(purchaseOrder.getCreditTermsCode());
		}
		return vendorBranch;
	}

	private EmployeeMaster toEmployeeMasterObject(PurchaseOrder purchaseOrder, EmployeeMaster employeeMaster) {
		if (purchaseOrder.getContactPersonName() != null) {
			employeeMaster.setName(purchaseOrder.getContactPersonName());
		}
		if (purchaseOrder.getContactPersonPhone() != null) {
			employeeMaster.setPhoneNo(purchaseOrder.getContactPersonPhone());
		}
		if (purchaseOrder.getContactPersonEmail() != null) {
			employeeMaster.setEmail(purchaseOrder.getContactPersonEmail());
		}
		return employeeMaster;
	}

	public void draftPurchaseOrder(PurchaseOrder purchaseOrder) {
		if (StringUtils.isNotEmpty(purchaseOrder.getStatus())) {
			purchaseOrder.setStatus("Draft");
		}
		if (StringUtils.isEmpty(purchaseOrder.getPoNo())) {
			Calendar calendar = new GregorianCalendar();
			String yearToSend = Integer.toString(calendar.get(Calendar.YEAR));
			String pcCode = purchaseOrder.getPcCode();
			ProfitCenter pc = profitCenterService.getProfitCenterByCode(pcCode);
			ProfitCenterLastPONumber pcLastPo = getRunningNo(pcCode, yearToSend);
			String year = yearToSend.substring(2);
			String storeId = pc.getStore().getCode();
			String deptId = pc.getDepartment().getAbbrName();
			int runningNo = pcLastPo.getRunningNumber() + 1;
			String poNumber = null;
			if (StringUtils.isNotEmpty(deptId) && StringUtils.isNotEmpty(storeId)) {
				poNumber = purchaseOrder.getCompCode() + "PO" + year + storeId + deptId
						+ String.format("%05d", (runningNo));
				purchaseOrder.setPoNo(poNumber);
				pcLastPo.setLastPoNumber(poNumber);
				pcLastPo.setRunningNumber(runningNo);
				pcLastPONumberService.createProfitCenterLastPONumber(pcLastPo);
			} else {
				logger.debug("Department ID or Store ID is empty.");
			}
		}
		savePurchaseOrder(purchaseOrder);
		if (StringUtils.isNotEmpty(purchaseOrder.getIssuer())) {
			// PurchaseOrderUserAccessPk poUserAccessId = new
			// PurchaseOrderUserAccessPk(purchaseOrder.getIssuer(),
			// purchaseOrder.getPoNo(), "D");
			PurchaseOrderUserAccess drafter2 = new PurchaseOrderUserAccess();
			PurchaseOrderUserAccessPk pkey = new PurchaseOrderUserAccessPk(purchaseOrder.getIssuer(),
					purchaseOrder.getPoNo(), "D");
			// drafter2.setPoUserAccessId(poUserAccessId);
			drafter2.setPurchaseorderuseraccessId(pkey);
			/*
			 * drafter2.setPsID(purchaseOrder.getIssuer());
			 * drafter2.setPoNo(purchaseOrder.getPoNo());
			 * drafter2.setPoAction("D");
			 */
			purchaseOrderUserAccessService.createPurchaseOrderUserAccess(drafter2);
		}
	}

	public void submitPurchaseOrder(PurchaseOrder purchaseOrder) {
		purchaseOrder.setStatus("Issued");
		if (!purchaseOrder.getCompCode().contains("superadmin")) {
			purchaseOrder.setIssuer(currentUser);
		}
		purchaseOrder.setIssuedOn(new Date());
		savePurchaseOrder(purchaseOrder);
		PurchaseOrderUserAccess purchaseOrderUserAccess = new PurchaseOrderUserAccess();
		PurchaseOrderUserAccessPk pkey = new PurchaseOrderUserAccessPk(purchaseOrder.getVerifier(),
				purchaseOrder.getPoNo(), "V");
		PurchaseOrderUserAccessPk pkey_auth = new PurchaseOrderUserAccessPk(purchaseOrder.getAuthorizer(),
				purchaseOrder.getPoNo(), "A");
		// PurchaseOrderUserAccessPk poUserAccessId = new
		// PurchaseOrderUserAccessPk(purchaseOrder.getVerifier(),
		// purchaseOrder.getPoNo(), "V");
		if (StringUtils.isNotEmpty(purchaseOrder.getVerifier())) {

			purchaseOrderUserAccess.setPurchaseorderuseraccessId(pkey);
			// purchaseOrderUserAccess.setPoUserAccessId(poUserAccessId);
			/*
			 * purchaseOrderUserAccess.setPsID(purchaseOrder.getVerifier());
			 * purchaseOrderUserAccess.setPoNo(purchaseOrder.getPoNo());
			 * purchaseOrderUserAccess.setPoAction("V");
			 */
			purchaseOrderUserAccessService.createPurchaseOrderUserAccess(purchaseOrderUserAccess);
		}
		if (StringUtils.isNotEmpty(purchaseOrder.getAuthorizer())) {
			purchaseOrderUserAccess = new PurchaseOrderUserAccess();
			purchaseOrderUserAccess.setPurchaseorderuseraccessId(pkey_auth);
			// poUserAccessId = new
			// PurchaseOrderUserAccessPk(purchaseOrder.getAuthorizer(),
			// purchaseOrder.getPoNo(), "A");
			// purchaseOrderUserAccess.setPoUserAccessId(poUserAccessId);
			/*
			 * purchaseOrderUserAccess.setPsID(purchaseOrder.getAuthorizer());
			 * purchaseOrderUserAccess.setPoNo(purchaseOrder.getPoNo());
			 * purchaseOrderUserAccess.setPoAction("A");
			 */
			purchaseOrderUserAccessService.createPurchaseOrderUserAccess(purchaseOrderUserAccess);
		}
	}

	public void verifyPurchaseOrder(PurchaseOrder purchaseOrder, String verifierRemark) {
		purchaseOrder.setStatus("Verified");
		purchaseOrder.setVerifier(currentUser);
		purchaseOrder.setVerifiedOn(new Date());
		purchaseOrder.setVerifyRemark(verifierRemark);
		savePurchaseOrder(purchaseOrder);
		/***
		 * Here updating Authorizer- This is for cases where the verifier
		 * updates Authorizer
		 ***/
		if (StringUtils.isNotEmpty(purchaseOrder.getAuthorizer())) {
			PurchaseOrderUserAccess authorizer = new PurchaseOrderUserAccess();
			PurchaseOrderUserAccessPk pkey = new PurchaseOrderUserAccessPk(purchaseOrder.getAuthorizer(),
					purchaseOrder.getPoNo(), "A");
			// PurchaseOrderUserAccessPk poUserAccessId = new
			// PurchaseOrderUserAccessPk(purchaseOrder.getAuthorizer(),
			// purchaseOrder.getPoNo(), "A");
			// authorizer.setPoUserAccessId(poUserAccessId);

			authorizer.setPurchaseorderuseraccessId(pkey);
			/*
			 * authorizer.setPoNo(purchaseOrder.getPoNo());
			 * authorizer.setPoAction("A");
			 */
			purchaseOrderUserAccessService.createPurchaseOrderUserAccess(authorizer);
		}
	}

	public void authorizePurchaseOrder(PurchaseOrder purchaseOrder, String authorizerRemark) {
		purchaseOrder.setStatus(AUTHORIZED_STATUS);
		purchaseOrder.setAuthorizer(currentUser);
		purchaseOrder.setAuthorizedOn(new Date());
		purchaseOrder.setAuthorizeRemark(authorizerRemark);
		savePurchaseOrder(purchaseOrder);
		PurchaseOrderSendMailUtil email = new PurchaseOrderSendMailUtil(purchaseOrder.getPoNo(),
				AbstractMainController.getCompany().get());
		email.authorizedPurchaseOrder();
	}

	public void addInvoices(PurchaseOrder purchaseOrder, List<Invoice> invoiceList) {
		for (Invoice invoice : invoiceList) {
			invoiceService.createInvoice(invoice);
		}
		purchaseOrder.setStatus("Invoiced");
		purchaseOrderService.createPurchaseOrder(purchaseOrder);
		PurchaseOrderSendMailUtil email = new PurchaseOrderSendMailUtil(purchaseOrder.getPoNo(),
				AbstractMainController.getCompany().get());
		email.invoicePurchaseOrder();
	}

	public void closePurchaseOrder(PurchaseOrder purchaseOrder, String closeException) {
		purchaseOrder.setStatus("Closed");
		if (StringUtils.isNotEmpty(closeException)) {
			purchaseOrder.setStatus("Closed with Exception");
		}
		PurchaseOrderSendMailUtil email = new PurchaseOrderSendMailUtil(purchaseOrder.getPoNo(),
				AbstractMainController.getCompany().get());
		email.closePurchaseOrderWithoutException();
		purchaseOrder.setCloseException(closeException);
		purchaseOrder.setClosedBy(currentUser);
		purchaseOrder.setClosedOn(new Date());
		savePurchaseOrder(purchaseOrder);
	}

	public void redraftPurchaseOrder(PurchaseOrder purchaseOrder, String redraftRemark) {
		purchaseOrder.setStatus("Redraft");
		purchaseOrder.setRedraftRemark(redraftRemark);
		savePurchaseOrder(purchaseOrder);
		if (purchaseOrder.getIssuer() != null) {
			PurchaseOrderUserAccess redrafter = new PurchaseOrderUserAccess();
			PurchaseOrderUserAccessPk pkey = new PurchaseOrderUserAccessPk(purchaseOrder.getIssuer(),
					purchaseOrder.getPoNo(), "R");
			// PurchaseOrderUserAccessPk poUserAccessId = new
			// PurchaseOrderUserAccessPk(purchaseOrder.getIssuer(),
			// purchaseOrder.getPoNo(), "R");
			// redrafter.setPoUserAccessId(poUserAccessId);
			redrafter.setPurchaseorderuseraccessId(pkey);
			/*
			 * redrafter.setPsID(purchaseOrder.getIssuer());
			 * redrafter.setPoNo(purchaseOrder.getPoNo());
			 * redrafter.setPoAction("R");
			 */
			purchaseOrderUserAccessService.createPurchaseOrderUserAccess(redrafter);
		}
		PurchaseOrderSendMailUtil email = new PurchaseOrderSendMailUtil(purchaseOrder.getPoNo(),
				AbstractMainController.getCompany().get());
		email.redraftPurchaseOrder();
	}

	public void rejectPurchaseOrder(PurchaseOrder purchaseOrder, String rejectRemark) {
		purchaseOrder.setStatus("Rejected");
		purchaseOrder.setRejectRemark(rejectRemark);
		purchaseOrder.setRejectedBy(currentUser);
		purchaseOrder.setRejectedOn(new Date());
		savePurchaseOrder(purchaseOrder);
		PurchaseOrderSendMailUtil email = new PurchaseOrderSendMailUtil(purchaseOrder.getPoNo(),
				AbstractMainController.getCompany().get());
		email.rejectPurchaseOrder();
	}

	public void forwardVerifier(PurchaseOrder purchaseOrder, EmployeeMaster verifier) {
		if (purchaseOrder.getVerifier() != null) {
			PurchaseOrderUserAccess forwardVerifier = new PurchaseOrderUserAccess();
			PurchaseOrderUserAccessPk poUserAccessId = new PurchaseOrderUserAccessPk(verifier.getPsId(),
					purchaseOrder.getPoNo(), "V");
			forwardVerifier.setPurchaseorderuseraccessId(poUserAccessId);
			/*
			 * forwardVerifier.setPsID(verifier.getPsId());
			 * forwardVerifier.setPoNo(purchaseOrder.getPoNo());
			 * forwardVerifier.setPoAction("V");
			 */
			purchaseOrderUserAccessService.createPurchaseOrderUserAccess(forwardVerifier);
		}
		purchaseOrder.setStatus("Issued");
		purchaseOrder.setVerifier(verifier.getPsId());
		savePurchaseOrder(purchaseOrder);
		PurchaseOrderSendMailUtil email = new PurchaseOrderSendMailUtil(purchaseOrder.getPoNo(),
				AbstractMainController.getCompany().get());
		email.forwardPurchaseOrder();
	}

	public void forwardAuthorizer(PurchaseOrder purchaseOrder, EmployeeMaster authorizer) {
		if (StringUtils.isNotEmpty(purchaseOrder.getAuthorizer())) {
			PurchaseOrderUserAccess forwardAuthorizer = new PurchaseOrderUserAccess();
			PurchaseOrderUserAccessPk poUserAccessId = new PurchaseOrderUserAccessPk(authorizer.getPsId(),
					purchaseOrder.getPoNo(), "A");
			forwardAuthorizer.setPurchaseorderuseraccessId(poUserAccessId);
			/*
			 * forwardAuthorizer.setPsID(authorizer.getPsId());
			 * forwardAuthorizer.setPoNo(purchaseOrder.getPoNo());
			 * forwardAuthorizer.setPoAction("A");
			 */
			purchaseOrderUserAccessService.createPurchaseOrderUserAccess(forwardAuthorizer);
		}
		purchaseOrder.setStatus("Verified");
		purchaseOrder.setAuthorizer(authorizer.getPsId());
		savePurchaseOrder(purchaseOrder);
		PurchaseOrderSendMailUtil email = new PurchaseOrderSendMailUtil(purchaseOrder.getPoNo(),
				AbstractMainController.getCompany().get());
		email.forwardPurchaseOrder();
	}

	public void forwardInvoicer(PurchaseOrder purchaseOrder, EmployeeMaster invoicer) {
		if (invoicer.getPsId() != null) {
			PurchaseOrderUserAccess forwardInvoicer = new PurchaseOrderUserAccess();
			PurchaseOrderUserAccessPk poUserAccessId = new PurchaseOrderUserAccessPk(invoicer.getPsId(),
					purchaseOrder.getPoNo(), "I");
			forwardInvoicer.setPurchaseorderuseraccessId(poUserAccessId);
			/*
			 * forwardInvoicer.setPsID(invoicer.getPsId());
			 * forwardInvoicer.setPoNo(purchaseOrder.getPoNo());
			 * forwardInvoicer.setPoAction("I");
			 */
			purchaseOrderUserAccessService.createPurchaseOrderUserAccess(forwardInvoicer);
		}
	}

	public void deletePurchaseOrder(PurchaseOrder purchaseOrder, String deleteRemark) {
		purchaseOrder.setStatus("Deleted");
		purchaseOrder.setDeleteRemark(deleteRemark);
		purchaseOrder.setDeletedBy(currentUser);
		purchaseOrder.setDeletedOn(new Date());
		savePurchaseOrder(purchaseOrder);
	}

	public void cancelPurchaseOrder(String cancelRemark, PurchaseOrder purchaseOrder) {
		purchaseOrder.setStatus("Cancelled");
		purchaseOrder.setCancelRemark(cancelRemark);
		purchaseOrder.setCancelledBy(currentUser);
		purchaseOrder.setCancelledOn(new Date());
		savePurchaseOrder(purchaseOrder);
		PurchaseOrderSendMailUtil email = new PurchaseOrderSendMailUtil(purchaseOrder.getPoNo(),
				AbstractMainController.getCompany().get());
		email.cancelPurchaseOrder();
	}

	public void reopenPurchaseOrder(PurchaseOrder purchaseOrder) {
		purchaseOrder.setStatus("Invoiced");
		savePurchaseOrder(purchaseOrder);
	}

	public synchronized ProfitCenterLastPONumber getRunningNo(String pcCode, String year) {
		ProfitCenterLastPONumber pcLastPo = pcLastPONumberService.getPcLastPONumberByPcCodeAndYear(pcCode, year);
		synchronized (this) {
			if (pcLastPo == null) {
				pcLastPo = new ProfitCenterLastPONumber();
				pcLastPo.setPcCode(pcCode);
				pcLastPo.setYear(year);
				pcLastPo.setRunningNumber(0);
			}
			return pcLastPo;
		}
	}

	public double getTotalInvoice(PurchaseOrder purchaseOrder) {
		double totalInvoice = 0;
		List<Invoice> invoices = invoiceService.getInvoiceByPurchaseOrderNo(purchaseOrder.getPoNo());
		if (invoices != null) {
			for (Invoice invoice : invoices) {
				totalInvoice += invoice.getAmount();
			}
		}
		return totalInvoice;
	}

	public double getPOBalance(PurchaseOrder purchaseOrder) {
		double totalInvoice = getTotalInvoice(purchaseOrder);
		double poBalance = 0;
		poBalance = purchaseOrder.getTotalInclGST() - totalInvoice;
		return poBalance;
	}

	public double poGrandTotal(String pc) {
		List<PurchaseOrder> grandTotalList = purchaseOrderService.getPurchaseOrderByPcCode(pc);
		double grandTotal = 0;
		for (PurchaseOrder po : grandTotalList) {
			grandTotal += po.getTotalInclGST();
		}
		return grandTotal;
	}

	public double poCompanyTotal() {
		List<PurchaseOrder> companyTotal = purchaseOrderService.getAllPurchaseOrder();
		double grandTotal = 0;
		for (PurchaseOrder po : companyTotal) {
			grandTotal += po.getTotalInclGST();
		}
		return grandTotal;
	}

	public List<Number> getAuthorizedPurchaseOrderCount() {
		int jan, feb, mar, apr, may, jun, jul, aug, sep, oct, nov, dec;
		jan = feb = mar = apr = may = jun = jul = aug = sep = oct = nov = dec = 0;
		List<Number> poCount = new ArrayList<>();
		Calendar calendar = Calendar.getInstance();
		List<PurchaseOrder> authorizedPO = purchaseOrderService.getPurchaseOrderByStatus(AUTHORIZED_STATUS);
		for (PurchaseOrder purchaseOrder : authorizedPO) {
			calendar.setTime(purchaseOrder.getAuthorizedOn());
			int month = calendar.get(Calendar.MONTH);
			if (month == 0) {
				jan += 1;
			} else if (month == 1) {
				feb += 1;
			} else if (month == 2) {
				mar += 1;
			} else if (month == 3) {
				apr += 1;
			} else if (month == 4) {
				may += 1;
			} else if (month == 5) {
				jun += 1;
			} else if (month == 6) {
				jul += 1;
			} else if (month == 7) {
				aug += 1;
			} else if (month == 8) {
				sep += 1;
			} else if (month == 9) {
				oct += 1;
			} else if (month == 10) {
				nov += 1;
			} else if (month == 11) {
				dec += 1;
			}
		}
		poCount.add(jan);
		poCount.add(feb);
		poCount.add(mar);
		poCount.add(apr);
		poCount.add(may);
		poCount.add(jun);
		poCount.add(jul);
		poCount.add(aug);
		poCount.add(sep);
		poCount.add(oct);
		poCount.add(nov);
		poCount.add(dec);
		return poCount;
	}

	public List<Number> getAuthorizedPurchaseOrderSpent() {
		double jan, feb, mar, apr, may, jun, jul, aug, sep, oct, nov, dec;
		jan = feb = mar = apr = may = jun = jul = aug = sep = oct = nov = dec = 0;
		List<Number> poAmount = new ArrayList<>();
		List<PurchaseOrder> authorizedPO = purchaseOrderService.getPurchaseOrderByStatus(AUTHORIZED_STATUS);
		Calendar calendar = Calendar.getInstance();
		for (PurchaseOrder purchaseOrder : authorizedPO) {
			calendar.setTime(purchaseOrder.getAuthorizedOn());
			int month = calendar.get(Calendar.MONTH);
			if (month == 0) {
				jan += purchaseOrder.getTotalInclGST();
			} else if (month == 1) {
				feb += purchaseOrder.getTotalInclGST();
			} else if (month == 2) {
				mar += purchaseOrder.getTotalInclGST();
			} else if (month == 3) {
				apr += purchaseOrder.getTotalInclGST();
			} else if (month == 4) {
				may += purchaseOrder.getTotalInclGST();
			} else if (month == 5) {
				jun += purchaseOrder.getTotalInclGST();
			} else if (month == 6) {
				jul += purchaseOrder.getTotalInclGST();
			} else if (month == 7) {
				aug += purchaseOrder.getTotalInclGST();
			} else if (month == 8) {
				sep += purchaseOrder.getTotalInclGST();
			} else if (month == 9) {
				oct += purchaseOrder.getTotalInclGST();
			} else if (month == 10) {
				nov += purchaseOrder.getTotalInclGST();
			} else if (month == 11) {
				dec += purchaseOrder.getTotalInclGST();
			}
		}
		poAmount.add(jan);
		poAmount.add(feb);
		poAmount.add(mar);
		poAmount.add(apr);
		poAmount.add(may);
		poAmount.add(jun);
		poAmount.add(jul);
		poAmount.add(aug);
		poAmount.add(sep);
		poAmount.add(oct);
		poAmount.add(nov);
		poAmount.add(dec);
		return poAmount;
	}

	public double calculateTotal(List<PurchaseOrder> purchaseOrderList) {
		double total = 0;
		for (PurchaseOrder po : purchaseOrderList) {
			total += po.getTotalInclGST();
		}
		return total;
	}
}