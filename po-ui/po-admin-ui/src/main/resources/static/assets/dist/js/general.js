
$(document).ready(function(){ 
	
    var today = new Date();
    var month = today.getMonth() + 1;
    var day = today.getDate();
    var year = today.getFullYear();
    if(month < 10)
        month = '0' + month.toString();
    if(day < 10)
        day = '0' + day.toString();    
    var maxDate = year + '-' + month + '-' + day;
    
    $( '#startDate' ).datepicker({dateFormat: 'dd-MM-yy', minDate : new Date()});
    $('<style type="text/css"> .ui-datepicker-current { display: none; } </style>').appendTo("head");
    $( '#pc_startDate' ).datepicker({
    	showButtonPanel: true,
        closeText: 'Clear',
        onClose: function (dateText, inst) {
            if ($(window.event.srcElement).hasClass('ui-datepicker-close')) {
                document.getElementById(this.id).value = '';
                $('#pc_endDate').val('');
            }
        },
    	dateFormat: 'dd-MM-yy', 
    	setDate : new Date()
        });
    
    $( '#pc_endDate' ).datepicker({
    	showButtonPanel: true,
        closeText: 'Clear',
        onClose: function (dateText, inst) {
            if ($(window.event.srcElement).hasClass('ui-datepicker-close')) {
                document.getElementById(this.id).value = '';
            }
        },
    	dateFormat: 'dd-MM-yy', 
    	minDate : new Date()
        
    });
   
    $( '#endDate' ).datepicker({dateFormat: 'dd-MM-yy', minDate : new Date()});
    
    
	
	$('#startDate').blur(function(){
		/*var Date = new Date($('#startDate').val());*/
		var Date_val = new Date($('#startDate').val());
	    $( '#endDate' ).datepicker({dateFormat: 'dd-MM-yy', minDate :  Date_val});
		var myDate = new Date(Date_val);
		var calcDate = new Date(myDate.getTime() + 30*24*60*60*1000);
		month = calcDate.getMonth() + 1;
	    day = calcDate.getDate();
	    year = calcDate.getFullYear();
	    if(month < 10)
	        month = '0' + month.toString();
	    if(day < 10)
	        day = '0' + day.toString();    
	    maxDate = year + '-' + month + '-' + day;
		$('#endDate').val($('#startDate').val());
	});
	
	$('#pc_startDate').blur(function(){
		var Date_val = new Date($('#pc_startDate').val());
	    $( '#pc_endDate' ).datepicker({dateFormat: 'dd-MM-yy', minDate :  Date_val});
		var myDate = new Date(Date_val);
		var calcDate = new Date(myDate.getTime() + 30*24*60*60*1000);
		month = calcDate.getMonth() + 1;
	    day = calcDate.getDate();
	    year = calcDate.getFullYear();
	    if(month < 10)
	        month = '0' + month.toString();
	    if(day < 10)
	        day = '0' + day.toString();    
	    maxDate = year + '-' + month + '-' + day;
		$('#pc_endDate').val($('#pc_startDate').val());
	});
		
	$('#pc_startDate').on('change', function(){
		
		$('#pc_endDate').removeClass('calendarclass');
		$('#pc_endDate').removeClass('hasDatepicker');
		$('#pc_endDate').unbind();
		
		$('#pc_endDate').val('');
		
		var myDate = new Date($('#pc_startDate').val());
	    $( '#pc_endDate' ).datepicker({
	    	showButtonPanel: true,
	        closeText: 'Clear',
	        onClose: function (dateText, inst) {
	            if ($(window.event.srcElement).hasClass('ui-datepicker-close')) {
	                document.getElementById(this.id).value = '';
	            }
	        },
	    	
	    	dateFormat: 'dd-MM-yy', minDate : myDate});
	   
	
	});

	
	$('#startDate').on('change', function(){
		
		$('#endDate').removeClass('calendarclass');
		$('#endDate').removeClass('hasDatepicker');
		$('#endDate').unbind();
		
	
		$('#endDate').val('');
		
		var myDate = new Date($('#startDate').val());
	    $( '#endDate' ).datepicker({dateFormat: 'dd-MM-yy', minDate : myDate});
	    
	    if(myDate !=null){
	    	console.log(myDate);
	    	$('#startDate').datepicker({dateFormat: 'dd-MM-yy', minDate : myDate});
	    	$('#adminform').data('bootstrapValidator').updateStatus('startDate', 'VALID', null);
	    	$('#endDate').val($('#startDate').val());
	    	$('#adminform').data('bootstrapValidator').updateStatus('endDate', 'VALID', null);
	    }
	
	});
	
	

	
	$('#reset').on('click', function(){
		
		$('#endDate').val('');
		$('#startDate').val('');
		$('#pc_startDate').val('');
		$('#pc_endDate').val('');
		
	    
	});
	

	});