function changePageAndSize() {
	$('#pageSizeSelect').change(
			function(evt) {
				var url = "" + window.location.href;
				var currentPageSizeText="";
				var currentPageNoText="";
				
				if (url.includes("pageSize=")){
					currentPageSizeText = url.substring(url.indexOf("pageSize="), url.indexOf("&"));
				}
				
				if (url.includes("page=")){
					currentPageNoText = url.substring(url.indexOf("page="), url.split("&", 2).join("&").length);
				}
				
				if (url.includes("pageSize=")) {
					window.location.replace(url.replace(currentPageSizeText,"pageSize=" + this.value).replace(currentPageNoText,"page=1"));
				} else {
					window.location.replace(url + "?pageSize=" + this.value
							+ "&page=1");
				}
			});
}

function changePageAndSizeTop() {
	$('#pageSizeSelectTop').change(
			function(evt) {
				var url = "" + window.location.href;
				var currentPageSizeText="";
				var currentPageNoText="";
				
				if (url.includes("pageSize=")){
					currentPageSizeText = url.substring(url.indexOf("pageSize="), url.indexOf("&"));
				}
				
				if (url.includes("page=")){
					currentPageNoText = url.substring(url.indexOf("page="), url.split("&", 2).join("&").length);
				}
				
				if (url.includes("pageSize=")) {
					window.location.replace(url.replace(currentPageSizeText,"pageSize=" + this.value).replace(currentPageNoText,"page=1"));
				} else {
					window.location.replace(url + "?pageSize=" + this.value
							+ "&page=1");
				}
			});
}

function filterTableAudit(username, auditId, operation, message,createdon) {
	window.location.href = "/audit/audittraillist/?pageSize=5&page=0" + "&username="
			+ username + "&auditId=" + auditId + "&operation=" + operation
			+ "&message=" + message
			+ "&createdon=" + createdon;
}

function filterTableLog(classFileName, lineNo, id, level, message, logTime,
		screenId) {
	window.location.href = "/audit/logs/?pageSize=5&page=0"
			+ "&classFileName=" + classFileName + "&lineNo=" + lineNo + "&id="
			+ id + "&level=" + level + "&message=" + message + "&logTime=" + logTime
			+ "&screenId=" + screenId;
}

function filterTableCompany(code, codeHRIS, name, isActive) {
	window.location.href = "/master/company/?pageSize=5&page=0" + "&codefil="
			+ code + "&codeHRISfil=" + codeHRIS + "&namefil=" + name
			+ "&isActive=" + isActive;
}

function filterTableStore(code, name, compCode, isActive) {
	window.location.href = "/master/store/?pageSize=5&page=0" + "&codefil="
			+ code + "&namefil=" + name + "&compCodefil=" + compCode
			+ "&isActive=" + isActive;
}

function filterTableDepartment(code, name, isActive) {
	window.location.href = "/master/department/?pageSize=5&page=0"
			+ "&codefil=" + code + "&namefil=" + name + "&isActive=" + isActive;
}

function filterTableProfitcenter(code, name, company, department, store,
		isActive) {
	window.location.href = "/master/profitcenter/?pageSize=5&page=0"
			+ "&codefil=" + code + "&namefil=" + name + "&companyfil="
			+ company + "&departmentfil=" + department + "&storefil=" + store
			+ "&isIsactive=" + isActive;
}

function filterTableGST(code, abbrDesc,effectiveStartDate,effectiveEndDate, isActive) {
	window.location.href = "/master/gst/?pageSize=5&page=0" + "&codefil="
			+ code + "&abbrDescfil=" + abbrDesc +
			"&effectiveStartDatefil=" + effectiveStartDate + "&effectiveEndDatefil=" + effectiveEndDate +
			
			"&isActive=" + isActive;
}

function filterTablePurchase(typeCode, abbrName, isActive) {
	window.location.href = "/master/purchase/?pageSize=5&page=0"
			+ "&typeCodefil=" + typeCode + "&abbrNamefil=" + abbrName
			+ "&isActive=" + isActive;
}

function filterTableOrderCat(code, purchaseRequisition, catabbrfil, isActive) {
	window.location.href = "/master/ordercategory/?pageSize=5&page=0"
			+ "&codefil=" + code + "&purchaseRequisitionfil="
			+ purchaseRequisition + "&catabbrfil=" + catabbrfil + "&isActive="
			+ isActive;
}

function filterTableOrderSubCat(ordersubCategory, code, desc, isActive) {
	var incodedValue = encodeURIComponent(ordersubCategory);
	var incodedValueordCode = encodeURIComponent(code);
	window.location.href = "/master/ordersubcategory/?pageSize=5&page=0"
			+ "&orderCategoryCfil=" + incodedValue + "&codefil=" + incodedValueordCode
			+ "&descfil=" + desc + "&isActive=" + isActive;
}

function filterTablescheduler(task, scheduleType, createdOn) {
	window.location.href = "/master/scheduler/?pageSize=5&page=0" + "&taskfil="
			+ task + "&scheduleTypefil=" + scheduleType;
}

function filterTableemplist(psId, altId, name, posTitle, email, paygrp,
		isActive, company, profitCenter) {
	window.location.href = "/employee/emplist/?pageSize=5&page=0" + "&psId="
			+ psId + "&altId=" + altId + "&name=" + name + "&posTitle="
			+ posTitle + "&email=" + email + "&paygrp=" + paygrp + "&isActive="
			+ isActive + "&company=" + company + "&profitCenter=" + profitCenter;
}

function filterTablejobmaster(code, abbrName, isActive) {
	window.location.href = "/employee/jobmaster/?pageSize=5&page=0"
			+ "&codefil=" + code + "&abbrNamefil=" + abbrName + "&isActive="
			+ isActive;
}

function filterTablemasterdata(codeType, codeValue, isActive) {
	window.location.href = "/system/masterdata/?pageSize=5&page=0"
			+ "&codeTypefil=" + codeType + "&codeValuefil=" + codeValue
			+ "&isActive=" + isActive;
}

function filterTableparameter(propName, propValue, isActive) {
	window.location.href = "/system/parameter/?pageSize=5&page=0&propNamefil="
			+ propName + "&propValuefil=" + propValue + "&isActive=" + isActive;
}

function filterTablepouser(id, empName,firstTimeLogin,lastLogin, isActive) {
	window.location.href = "/users/pouser/?pageSize=5&page=0&psidfil=" + id+
	"&empNamefil="+ empName +
	"&firstTimeLoginfil="+ firstTimeLogin +"&lastLoginfil="+ lastLogin +
	        "&isActive=" + isActive;
}

function filterTableadminuser(username,startDate,endDate, isActive) {
	
	window.location.href = "/users/adminuser/?pageSize=5&page=0&usrname="
			+ username + "&startDatefil=" + startDate + "&endDatefil=" + endDate + "&isActive=" + isActive;
}