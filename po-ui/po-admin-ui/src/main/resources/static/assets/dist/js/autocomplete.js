$(document).ready(function() {
	$('#storeselect').typeahead({
	
		// data source
		source : function(query, result) {
			$.ajax({
				url : "/master/profitcenter/suggestionstore",
				method : "GET",
				dataType : "json",
				data : {
					query : query
				},
				success : function(data) {
					result($.map(data, function(item) {
						return item;
					}));
				}
			})
		},
		// how many items to display
		items : 10,

		// enable scrollbar
		scrollBar : false,

		// equalize the dropdown width
		alignWidth : true,

		// typeahead dropdown template
		menu : '<ul class="typeahead dropdown-menu"></ul>',
		item : '<li><a href="#"></a></li>',

		// The object property that is returned when an item is selected.
		valueField : 'id',

		// The object property to match the query against and highlight in the
		// results.
		displayField : 'name',

		// auto select
		autoSelect : true,

		afterSelect : function() {
			alert('Selected '+document.getElementById('storeselect').value)
		}

	});
	
	$('#companyselect').typeahead({		
		// data source
		source : function(query, result) {
			$.ajax({
				url : "/master/profitcenter/suggestioncompany",
				method : "GET",
				dataType : "json",
				data : {
					query : query
				},
				success : function(data) {
					result($.map(data, function(item) {
						return item;
					}));
				}
			})
		},
		// how many items to display
		items : 10,

		// enable scrollbar
		scrollBar : false,

		// equalize the dropdown width
		alignWidth : true,

		// typeahead dropdown template
		menu : '<ul class="typeahead dropdown-menu"></ul>',
		item : '<li><a href="#"></a></li>',

		// The object property that is returned when an item is selected.
		valueField : 'id',

		// The object property to match the query against and highlight in the
		// results.
		displayField : 'name',

		// auto select
		autoSelect : true,

		afterSelect : function() {
			alert('Selected '+document.getElementById('companyselect').value)
		}

	});

$('#departmentselect').typeahead({
		
		
		// data source
		source : function(query, result) {
			$.ajax({
				url : "/master/profitcenter/suggestiondepartment",
				method : "GET",
				dataType : "json",
				data : {
					query : query
				},
				success : function(data) {
					result($.map(data, function(item) {
						return item;
					}));
				}
			})
		},
		// how many items to display
		items : 10,

		// enable scrollbar
		scrollBar : false,

		// equalize the dropdown width
		alignWidth : true,

		// typeahead dropdown template
		menu : '<ul class="typeahead dropdown-menu"></ul>',
		item : '<li><a href="#"></a></li>',

		// The object property that is returned when an item is selected.
		valueField : 'id',

		// The object property to match the query against and highlight in the
		// results.
		displayField : 'name',

		// auto select
		autoSelect : true,

		/*afterSelect : function() {
			alert('Selected '+document.getElementById('demo').value)
		}*/

	});


$('#companyselectinstore').typeahead({
	
	
	// data source
	source : function(query, result) {
		$.ajax({
			url : "/master/store/suggestioncompany",
			method : "GET",
			dataType : "json",
			data : {
				query : query
			},
			success : function(data) {
				result($.map(data, function(item) {
					return item;
				}));
			}
		})
	},
	// how many items to display
	items : 10,

	// enable scrollbar
	scrollBar : false,

	// equalize the dropdown width
	alignWidth : true,

	// typeahead dropdown template
	menu : '<ul class="typeahead dropdown-menu"></ul>',
	item : '<li><a href="#"></a></li>',

	// The object property that is returned when an item is selected.
	valueField : 'id',

	// The object property to match the query against and highlight in the
	// results.
	displayField : 'name',

	// auto select
	autoSelect : true,

	/*afterSelect : function() {
		alert('Selected '+document.getElementById('demo').value)
	}*/

});

$('#prtypeselect').typeahead({
	
	
	// data source
	source : function(query, result) {
		$.ajax({
			url : "/master/ordercategory/suggestionprtypeCode",
			method : "GET",
			dataType : "json",
			data : {
				query : query
			},
			success : function(data) {
				result($.map(data, function(item) {
					return item;
				}));
			}
		})
	},
	// how many items to display
	items : 10,

	// enable scrollbar
	scrollBar : false,

	// equalize the dropdown width
	alignWidth : true,

	// typeahead dropdown template
	menu : '<ul class="typeahead dropdown-menu"></ul>',
	item : '<li><a href="#"></a></li>',

	// The object property that is returned when an item is selected.
	valueField : 'id',

	// The object property to match the query against and highlight in the
	// results.
	displayField : 'name',

	// auto select
	autoSelect : true,

	/*afterSelect : function() {
		alert('Selected '+document.getElementById('demo').value)
	}*/

});

$('#ordercatselect').typeahead({
	
	
	// data source
	source : function(query, result) {
		$.ajax({
			url : "/master/ordersubcategory/suggestionordercatCode",
			method : "GET",
			dataType : "json",
			data : {
				query : query
			},
			success : function(data) {
				result($.map(data, function(item) {
					return item;
				}));
			}
		})
	},
	// how many items to display
	items : 10,

	// enable scrollbar
	scrollBar : false,

	// equalize the dropdown width
	alignWidth : true,

	// typeahead dropdown template
	menu : '<ul class="typeahead dropdown-menu"></ul>',
	item : '<li><a href="#"></a></li>',

	// The object property that is returned when an item is selected.
	valueField : 'id',

	// The object property to match the query against and highlight in the
	// results.
	displayField : 'name',

	// auto select
	autoSelect : true,

	/*afterSelect : function() {
		alert('Selected '+document.getElementById('demo').value)
	}*/

});

$('#psidselect').typeahead({
	
	
	// data source
	source : function(query, result) {
		$.ajax({
			url : "/users/pouser/suggestionpsid",
			method : "GET",
			dataType : "json",
			data : {
				query : query
			},
			success : function(data) {
				result($.map(data, function(item) {
					return item;
				}));
			}
		})
	},
	// how many items to display
	items : 10,

	// enable scrollbar
	scrollBar : false,

	// equalize the dropdown width
	alignWidth : true,

	// typeahead dropdown template
	menu : '<ul class="typeahead dropdown-menu"></ul>',
	item : '<li><a href="#"></a></li>',

	// The object property that is returned when an item is selected.
	valueField : 'id',

	// The object property to match the query against and highlight in the
	// results.
	displayField : 'name',

	// auto select
	autoSelect : true,

	/*afterSelect : function() {
		alert('Selected '+document.getElementById('demo').value)
	}*/

});

});