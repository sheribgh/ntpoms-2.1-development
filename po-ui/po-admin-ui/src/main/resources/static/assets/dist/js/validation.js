//https://getbootstrap.com/docs/4.0/getting-started/browsers-devices/#validators
//https://careydevelopment.us/2017/05/24/implement-form-field-validation-spring-boot-thymeleaf/
//http://bootstrapvalidator.votintsev.ru/getting-started/
$(document)
		.ready(
				function() {
				 
					
					
					$('.adminuserform')
							.bootstrapValidator(
									{
										message : 'This value is not valid',
										feedbackIcons : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											username : {
												message : 'The username is not valid',
												validators : {
													notEmpty : {
														message : 'The username is required and cannot be empty'
													},
													stringCase: {
								                        message: 'The Username should be in uppercase',
								                        'case': 'upper'
								                    },
													stringLength : {
														
														max : 7,
														message : 'The username must be 7 characters long'
													},
													/*regexp : {
														regexp : /^(A_|a_)[A-Z0-9]{5}/, 
														message : 'Please start with \"A_\" followed by 5 alphanumerics. Example : A_BC123',
									
													},*/
													callback: {
														message: 'The username is not valid',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															if (value === 'superadmin'){
								                                return {
								                                    valid: false,
								                                    message: 'Superadmin already exists'
								                                }
								                            }
															
															  // The password doesn't contain any uppercase character
								                           
														//If found return true the above error message is displayed
														 if (value.search(/^(A_)[A-Z0-9]{5}/) < 0) {
															 
								                                return {
								                                    valid: false,
								                                    message: 'The Username should be 7 characters long, Please start with \"A_\" followed by 5 alphanumerics. Example : A_BC123'
								                                };
								                            } else {
								                            	return true;
								                            }
														 
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: 'The username should not start or end with empty spaces.'
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
													
							
												}

											},
											password : {
												message : 'The password is not valid',
												validators : {
													notEmpty : {
														message : 'The password is required and cannot be empty'
													},
													regexp : {
														regexp : /^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!_%*#?&])[A-Za-z\d$@$!%*#_?&]{8,}$/,
														message : 'Invalid format! Please enter at least 8 alphanumerics with at least 1 special characters, 1 alphabet and 1 number.'
													}
												}
											},
											
											
											
											recentPassword : {
												message : 'The Confirm password is not valid',
												validators : {
													notEmpty : {
														message : 'The Confirm password is required and cannot be empty'
													},
													identical: {
								                        field: 'password',
								                        message: 'The password and its confirm are not the same'
								                    }
												}
											}
										}
									});
					
					
					$('.resetpwdform')
					.bootstrapValidator(
							{
								message : 'This value is not valid',
								feedbackIcons : {
									valid : 'glyphicon glyphicon-ok',
									invalid : 'glyphicon glyphicon-remove',
									validating : 'glyphicon glyphicon-refresh'
								},
								fields : {
									
									newpassword : {
									message : 'The password is not valid',
										validators : {
											notEmpty : {
												message : 'The new password is required and cannot be empty'
											},
											regexp : {
												regexp : /^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!_%*#?&])[A-Za-z\d$@$!%*#_?&]{8,}$/,
												message : 'Invalid format! Please enter at least 8 alphanumerics with at least 1 special characters, 1 alphabet and 1 number.'
											},
											
											different: {
						                        field: 'currentPassword',
						                        message: 'The New Password cannot be the same with the current password'
						                    }
										}
									},
									confirmpassword : {
										message : 'The Confirm password is not valid',
										validators : {
											notEmpty : {
												message : 'The Confirm password is required and cannot be empty'
											},
											identical: {
						                        field: 'newpassword',
						                        message: 'The password and its confirm are not the same'
						                    },
						                    different: {
						                        field: 'currentPassword',
						                        message: 'The Confirm Password cannot be the same with the current password'
						                    }
										}
									}
								}
							});

					$('.jobmasterform')
							.bootstrapValidator(
									{
										message : 'This value required',
										feedbackIcons : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											jobCode : {
												message : 'The Job Code is required',
												validators : {
													notEmpty : {
														message : 'The Job Code is required and cannot be empty'
													},
													regexp : {
														regexp : /[A-Za-z0-9]/, 
														message : 'The Job Code can only consist of alphabetical and numbers without special character. Example : 012345',
													},
													stringLength : {
														max : 6,
														message : 'The Job Code should be maximum 6 characters long'
													},
													callback: {
														message: 'The Job Code should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											jobname : {
												message : 'The jobname is not valid',
												validators : {
													stringLength : {
														max : 30,
														message : 'The Job Name should be maximum 30 characters long'
													},
													callback: {
														message: 'The Job Name should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											abbreviation : {
												message : 'The Abbreviation is not valid',
												validators : {
													stringLength : {
														max : 15,
														message : 'The Job Abbreviation should be maximum 15 characters long'
													},
													callback: {
														message: 'The Job Abbreviation should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											}
										}
									});
					// master data form

					$('.masterdataform')
							.bootstrapValidator(
									{
										message : 'This value required',
										feedbackIcons : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											codetype : {
												message : 'The Code Type is required',
												validators : {
													notEmpty : {
														message : 'The Code Type cannot be empty'
													},
													stringLength : {
														min :1,
														max : 45,
														message : 'The Code Type should be maximum 45 characters long'
													},
													callback: {
														message: 'The Code Type should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											codeValue : {
												message : 'The Code Value is required',
												validators : {
													stringLength : {
														max : 255,
														message : 'The Code Value should be maximum 255  characters long'
													},
													callback: {
														message: 'The Code Value should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											codeDesc : {
												message : 'The Code Desc is required',
												validators : {
													stringLength : {
														max : 255,
														message : 'The Code Desc should be maximum 255  characters long'
													},
													callback: {
														message: 'The Code Desc should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											}
										}
									});

					// parameter

					$('.systemparamform')
					.bootstrapValidator(
							{
								message : 'This value required',
								feedbackIcons : {
									valid : 'glyphicon glyphicon-ok',
									invalid : 'glyphicon glyphicon-remove',
									validating : 'glyphicon glyphicon-refresh'
								},
								fields : {
									propName : {
										message : 'The Property Name is required',
										validators : {
											notEmpty : {
												message : 'The Property Name cannot be empty'
											},
											stringLength : {
												min :1,
												max : 30,
												message : 'The Property Name should be maximum 30 characters long'
											},
											
											callback: {
												message: 'The Property Name should not start or end with empty spaces',
												callback: function(value, validator, $field){
						                         
													if (value === '') {
						                                return true;
						                            }
													
													
												//If found return true the above error message is displayed
												 if (value.search(/^\s|\s$/) < 0) {
													 
						                                return {
						                                    valid: true,
						                                    message: ''
						                                };
						                            } else {
						                            	return false;
						                            }
												 
											
											
												return true;
											}
										}
										}
									},
									propValue : {
										message : 'The Property Value is required',
										validators : {
											notEmpty : {
												message : 'The Property Value is required and cannot be empty'
											},
											stringLength : {
												min :1,
												max : 255,
												message : 'The Property Value should be maximum 255 characters long'
											},
											
											callback: {
												message: 'The Property Value should not start or end with empty spaces',
												callback: function(value, validator, $field){
						                         
													if (value === '') {
						                                return true;
						                            }
													
													
												//If found return true the above error message is displayed
												 if (value.search(/^\s|\s$/) < 0) {
													 
						                                return {
						                                    valid: true,
						                                    message: ''
						                                };
						                            } else {
						                            	return false;
						                            }
												 
											
											
												return true;
											}
										}
										}
									},
									sysName:{
										message : 'The Property Value is not Valid',
										validators : {
											stringLength : {
												max : 10,
												message : 'The System Name should be maximum 10 characters long'
											},
											
											callback: {
												message: 'The System Name should not start or end with empty spaces',
												callback: function(value, validator, $field){
						                         
													if (value === '') {
						                                return true;
						                            }
												
												
												 if (value.search(/^\s|\s$/) < 0) {
													 
						                                return {
						                                    valid: true,
						                                    message: ''
						                                };
						                            } else {
						                            	return false;
						                            }
												 
											
											
												return true;
											}
										}
											
										}
										
									},
									desc:{
										message : 'The Description is not Valid',
										validators : {
											stringLength : {
												max : 255,
												message : 'The Description should be maximum 255 characters long'
											},
											
											callback: {
												message: 'The Description should not start or end with empty spaces',
												callback: function(value, validator, $field){
						                         
													if (value === '') {
						                                return true;
						                            }
												
												
												 if (value.search(/^\s|\s$/) < 0) {
													 
						                                return {
						                                    valid: true,
						                                    message: ''
						                                };
						                            } else {
						                            	return false;
						                            }
												 
											
											
												return true;
											}
										}
											
										}
										
									}
								}
							});

					$('.storeform')
							.bootstrapValidator(
									{
										message : 'This value is not valid',
										feedbackIcons : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											storeAbbrName : {
												message : 'The Store Abbreviation Name is not valid',
												validators : {
													stringLength : {
														min :1,
														max : 15,
														message : 'The Store Abbreviation Name should be less than 15 characters long'
													},
													callback: {
														message: 'The Abbr Name should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											storeName : {
												message : 'The Store Name is not valid',
												validators : {
													/*notEmpty : {
														message : 'The Store Name is required and cannot be empty'
													},*/
													stringLength : {
														min :1,
														max : 30,
														message : 'The Store Name should be more than 1 and less than 30 characters long'
													},
													callback: {
														message: 'The Name should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											storeCode : {
												message : 'The Code is not valid',
												validators : {
													notEmpty : {
														message : 'The Code is required and cannot be empty'
													},
													stringLength : {
														
														max : 4,
														message : 'The Store Code should be 4 characters long'
													},
													stringCase: {
								                        message: 'The Code should be in uppercase',
								                        'case': 'upper'
								                    },
													
													regexp : {
														regexp : /[A-Z0-9]{4}/, 
														message : 'Please enter 4 alphanumerics without special character. Example : 012A',
									
													},
													callback: {
														message: 'The code should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											}
										}
									});

					$('.purchaseform')
							.bootstrapValidator(
									{
										message : 'This value is not valid',
										feedbackIcons : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											prAbbrName : {
												message : 'The Abbreviation is required and cannot be empty',
												validators : {
													stringLength : {
														min :1,
														max : 30,
														message : 'Please enter up to 30 alphanumerics.'
													},
													callback: {
														message: 'The Code Abbr should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											prDesc : {
												message : 'The Desc is is required and cannot be empty',
												validators : {
													stringLength : {
														min :1,
														max : 255,
														message : 'Please enter up to 255 alphanumerics.'
													},
													callback: {
														message: 'The Code Desc should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											prTypeCode : {
												message : 'The Code is not valid',
												validators : {
													notEmpty : {
														message : 'The Code is required and cannot be empty'
													},
													stringLength : {
														min :1,
														max : 10,
														message : 'Please enter up to 10 alphanumerics. Example : A43287DD@J'
													},
													stringCase: {
								                        message: 'The Code should be in uppercase',
								                        'case': 'upper'
								                    },
													
													callback: {
														message: 'The Code should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											}
										}
									});

					$('.profitcenterform')
							.bootstrapValidator(
									{
										message : 'This value is not valid',
										feedbackIcons : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											pcName : {
												message : 'The Name is required and cannot be empty',
												validators : {
													notEmpty : {
														message : 'The Name is required and cannot be empty'
													},
		
													stringLength : {
														min :1,
														max : 30,
														message : 'The Profit Center Name should be more than 1 and less than 30 characters long'
													},
													callback: {
														message: 'The Profit Center Name should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											pcAbbrName : {
			
												validators : {
													
													stringLength : {
														min :1,
														max : 15,
														message : 'The Profit Center Abbr Name should be more than 1 and less than 15 characters long'
													},
													callback: {
														message: 'The Profit Center Abbr Name should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											
											addressLine1 :{
												validators : {
													stringLength : {
														min :1,
														max : 70,
														message : 'The Profit Center Address should be more than 1 and less than 70 characters long'
													},
													callback: {
														message: 'The Profit Center Address should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											
											addressLine2 :{
												validators : {
													stringLength : {
														min :1,
														max : 70,
														message : 'The Profit Center Address should be more than 1 and less than 70 characters long'
													},
													callback: {
														message: 'The Profit Center Address should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											
											addressLine3 :{
												validators : {
													stringLength : {
														min :1,
														max : 70,
														message : 'The Profit Center Address should be more than 1 and less than 70 characters long'
													},
													callback: {
														message: 'The Profit Center Address should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											
											postArea :{
												validators : {
													stringLength : {
														min :1,
														max : 70,
														message : 'The Profit Center Post Area should be more than 1 and less than 70 characters long'
													},
													callback: {
														message: 'The Profit Center Post Area should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											
											postCode :{
												validators : {
												/*	stringLength : {
														min :1,
														max : 10,
														message : ''
													},*/
													regexp : {
														regexp : /^[a-zA-Z0-9]{1,10}$/, 
														message : 'Please enter up to 10 alphanumerics without special character. Example : 57000A'
									
													},
													callback: {
														message: 'The Profit Center Post Code should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											
											
											company : {
												message : 'Select Company',
												validators : {
													notEmpty : {
														message : 'Select Company'
													}
												}
											},
											store : {
												message : 'Select Store',
												validators : {
													notEmpty : {
														message : 'Select Store'
													}
												}
											},
											deartment : {
												message : 'Select Department',
												validators : {
													notEmpty : {
														message : 'Select Department'
													}
												}
											}
										}
									});

					$('.pouserform')
							.bootstrapValidator(
									{
										message : 'This value is not valid',
										feedbackIcons : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											empName : {
												message : 'The empname is not valid',
												validators : {
													notEmpty : {
														message : 'The empname is required and cannot be empty'
													}
												}
											},
											name : {
												message : 'Select PSID',
												validators : {
													notEmpty : {
														message : 'Select PSID'
													}
												}
											}
										}
									});

					$('.ordercategoryform')
							.bootstrapValidator(
									{
										message : 'This value is not valid',
										feedbackIcons : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											ordCatCode : {
												message : 'The Code is not valid',
												validators : {
													notEmpty : {
														message : 'The Code is required and cannot be empty'
													},
													stringLength : {
														max : 70,
														message : 'The code should be less than 70 characters long'
													},
													callback: {
														message: 'The Code should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											ordCatDesc : {
								
												validators : {
													
													stringLength : {
														max : 255,
														message : 'The Desc should be less than 255 characters long'
													},
													callback: {
														message: 'The Desc should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											ordCatAbbrDesc : {
												
												validators : {
													stringLength : {
														max : 30,
														message : 'The Abbr Desc should be less than 30 characters long'
													},
													callback: {
														message: 'The Abbr Desc should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											authgrid : {
												message : 'The Order Category Authorization Grid is not valid',
												validators : {
													/*notEmpty : {
														message : 'Please Select Order Category Authorization Grid File.'
													},*/
													 file: {
									                        extension: 'jpeg,png,jpg,pdf',
									                        type: 'image/jpeg,image/png,image/jpg,application/pdf',
									                        maxSize: 2048 * 1024,
									                        message: 'Order Category Authorization Grid should only be in JPEG,PNG,JPG or PDF Format'
									                    }
												}
											},
											
									authgrid_hidden : {
												message : 'The Order Category Authorization Grid is not valid',
												validators : {
													notEmpty : {
														message : 'Please Select Order Category Authorization Grid File again to keep the data safe.'
													},
													 file: {
									                        extension: 'jpeg,png,jpg,pdf',
									                        type: 'image/jpeg,image/png,image/jpg,application/pdf',
									                        maxSize: 2048 * 1024,
									                        message: 'Order Category Authorization Grid should only be in JPEG,PNG,JPG or PDF Format'
									                    }
												}
											},
											
											prtypecode : {
												 
												validators : {
													notEmpty : {
														message : 'Please Select Purchase Requisition Type Code.'
													}
													
												}
											}
										}
									});

					$('.ordersubcategoryform')
							.bootstrapValidator(
									{
										message : 'This value is not valid',
										feedbackIcons : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											ordSubCCode : {
												message : 'The Code is not valid',
												validators : {
													notEmpty : {
														message : 'The Code is required and cannot be empty'
													},
													stringLength : {
														max : 15,
														message : 'The code should be less than 15 characters long'
													},
													callback: {
														message: 'The Code should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											ordcat : {
												message : 'Select Order Category',
												validators : {
													notEmpty : {
														message : 'Select Order Category'
													}
												}
											},
											ordSubCDesc : {
												message : 'The Desc is not valid',
												validators : {
													
													stringLength : {
														max : 255,
														message : 'The Desc should be less than 255 characters long'
													},
													callback: {
														message: 'The Desc should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											ordSubCAbbrDesc : {
												message : 'The Abbr Desc is not valid',
												validators : {
													
													stringLength : {
														max : 30,
														message : 'The Abbr Desc should be less than 30 characters long'
													},
													callback: {
														message: 'The Abbr Desc should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											}
										}
									});

					$('.companyform')
							.bootstrapValidator(
									{
										message : 'This value is not valid',
										feedbackIcons : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											compCode : {
												message : 'The Company Code is not valid',
												validators : {
													notEmpty : {
														message : 'The Company Code is required and cannot be empty'
													},
													stringLength : {
														min : 3,
														max : 3,
														message : 'The Company Code should be 3 characters long'
													},
													regexp : {
														regexp : /[0-9]{3}/, 
														message : 'Please enter numerics without special character. Example : 012',
									
													},
													callback: {
														message: 'The Code should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											compAbbrName : {
												message : 'The Company Abbreviation Name is required',
												validators : {
													notEmpty : {
														message : 'The Company Abbreviation Name is required and cannot be empty'
													},
													stringLength : {
														min : 1,
														max : 15,
														message : 'The Company Abbreviation Name must be more than 1 and less than 15 characters long'
													},
				
													callback: {
														message: 'The Company Abbreviation Name should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											compName : {
												message : 'The Company Name is required',
												validators : {
													notEmpty : {
														message : 'The company name cannot be empty'
													},
													stringLength : {
														min : 1,
														max : 70,
														message : 'The Company Name must be more than 1 and less than 70 characters long'
													},
				
													callback: {
														message: 'The Company Name should not start or end with empty spaces',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											compCodeHRIS : {
												message : 'The Company Code HRIS is required',
												validators : {
													notEmpty : {
														message : 'The Company Code HRIS is required and cannot be empty'
													},
													stringCase: {
								                        message: 'The Company Code HRIS should be in uppercase',
								                        'case': 'upper'
								                    },
													regexp : {
														regexp : /[A-Z0-9]{3}/, 
														message : 'The Company Code HRIS can only consist of alphabetical and numbers. Example : PC1',
									
													},
													callback: {
														message: 'The Company Code HRIS should not have empty spaces at start/between/end.',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											
											compRegNo : {
												/*message : 'The Company Reg No is required',*/
												validators : {
													/*notEmpty : {
														message : 'The Company Reg No is required and cannot be empty'
													},*/
													stringLength : {
														min : 1,
														max : 20,
														message : 'The Company Reg No must be less than 20 characters long'
													},
													callback: {
														message: 'The Company Reg No should not start or end with empty spaces.',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											compLogofile:{
											    validators: {
											   
								                    file: {
								                        extension: 'jpeg,png,jpg',
								                        type: 'image/jpeg,image/png,image/jpg',
								                        maxSize: 2048 * 1024,
								                        message: 'Company Logo should only be in JPEG,PNG or JPG Format'
								                    }
								                }
												
											},
											compLetterheadfile:{
											    validators: {
								                    file: {
								                        extension: 'jpeg,png,jpg,pdf',
								                        type: 'image/jpeg,image/png,image/jpg,application/pdf',
								                        maxSize: 2048 * 1024,
								                        message: 'Company Letter Heading should only be in JPEG,PNG,JPG or PDF Format'
								                    }
								                }
												
											},
											
											termsLandscapefile:{
											    validators: {
								                    file: {
								                        extension: 'pdf',
								                        type: 'application/pdf',
								              
								                        message: 'Terms and Conditions Landscape should only be in PDF Format'
								                    }
								                }
												
											},
											
											termsPortraitfile:{
											    validators: {
								                    file: {
								                        extension: 'pdf',
								                        type: 'application/pdf',
								              
								                        message: 'Terms and Conditions Portrait should only be in PDF Format'
								                    }
								                }
												
											}
										}
									});

					$('.depatform')
							.bootstrapValidator(
									{
										message : 'This value is not valid',
										feedbackIcons : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											deptAbbrName : {
												message : 'The Department ID is not valid',
												validators : {
													notEmpty : {
														message : 'The Department ID is required and cannot be empty'
													},
													stringLength : {
														min : 2,
														max : 2,
														message : 'The Department ID must be 2 characters long'
													},
													regexp : {
														regexp : /[A-Za-z0-9]{2}/, 
														message : 'The Department ID can only consist of alphabetical and numbers without special character. Example : A1',
									
													},
													callback: {
														message: 'The Department ID should not start or end with empty spaces.',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											deptName : {
												message : 'The Name is required',
												validators : {
													notEmpty : {
														message : 'The Department Name is required and cannot be empty'
													},
													stringLength : {
														min : 1,
														max : 30,
														message : 'The Department Name should be less than 30 characters long'
													},
													callback: {
														message: 'The Department Name should not start or end with empty spaces.',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											deptCode : {
												message : 'The Department Code is not valid',
												validators : {
													notEmpty : {
														message : 'The Department Code is required and cannot be empty'
													},
													regexp : {
														regexp : /[A-Za-z0-9]{3}/, 
														message : 'The Department Code can only consist of alphabetical and numbers without special character. Example : 014',
									
													},
													callback: {
														message: 'The Department Code should not start or end with empty spaces.',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											}
										}
									});

					$('.gstform')
							.bootstrapValidator(
									{
										message : 'This value is not valid',
										feedbackIcons : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											taxRate : {
												message : 'The Tax Rate should be number',
												validators : {
													notEmpty : {
														message : 'The Tax Rate is required and cannot be empty'
													},
													/*between: {
									                    min: 0,
									                    max: 100,
									                    message: 'The Tax Rate must be between 0 and 100'
									                },*/
									                regexp : {
														regexp : /^\d{1,2}(\.\d{1,2})?$/, 
														message : 'The Allowed Variance can only between 0 to 100 with only two desimal place. Example : 99.22',
									
													}
												}
											},
											allowedVariance : {
												message : 'The Allowed Variance is not valid',
												validators : {
													notEmpty : {
														message : 'The Allowed Variance is required and cannot be empty'
													},
													
									                regexp : {
														regexp : /^\d{1,2}(\.\d{1,2})?$/, 
														message : 'The Allowed Variance can only between 0 to 100 with only two desimal place. Example : 99.22',
									
													}
									                
												}
											},
											taxAbbrDesc : {
												message : 'The Tax Abbreviation Desc is not valid',
												validators : {
													stringLength : {
														min : 1,
														max : 30,
														message : 'The  Tax Abbreviation Desc must be less than 30 characters long'
													},
													callback: {
														message: 'The Tax Abbreviation Desc should not start or end with empty spaces.',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											taxDesc : {
												message : 'The tax Desc is not valid',
												validators : {
													stringLength : {
														min : 1,
														max : 255,
														message : 'The  Tax Desc must be less than 255 characters long'
													},
													callback: {
														message: 'The Tax Desc should not start or end with empty spaces.',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
															
															
														//If found return true the above error message is displayed
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
												}
											},
											
											taxCode : {
												message : 'The The taxCode is not valid',
												validators : {
													notEmpty : {
														message : 'The Tax Code is required and cannot be empty'
													},
													stringCase: {
								                        message: 'The Tax Code should be in uppercase',
								                        'case': 'upper'
								                    },
													stringLength : {
														min : 1,
														max : 20,
														message : 'The Tax Code should be between 1 and 20 characters long'
													},
													regexp : {
														regexp : /(^[\w\-]+$){1,20}/, 
														message : 'Please enter up to 20 alphanumerics, Only \"-\" Allowed as special character. Example : GST0003321',
									
													},
													callback: {
														message: 'The Tax Code should not start or end with empty spaces.',
														callback: function(value, validator, $field){
								                         
															if (value === '') {
								                                return true;
								                            }
														
												
														 if (value.search(/^\s|\s$/) < 0) {
															 
								                                return {
								                                    valid: true,
								                                    message: ''
								                                };
								                            } else {
								                            	return false;
								                            }
														 
													
													
														return true;
													}
												}
													
							
												}

											}
										
										}
									});
					
			
			

				});