package my.com.parkson.ntpoms.poadmin.web.scheduler;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import my.com.parkson.ntpoms.common.entities.Scheduler;
import my.com.parkson.ntpoms.common.services.AccessLogService;
import my.com.parkson.ntpoms.common.services.AuditService;
import my.com.parkson.ntpoms.common.services.SchedulerService;
import my.com.parkson.ntpoms.utils.ApplicationContextProvider;

@Component
@Configuration
@PropertySource("classpath:application-dev.properties")
public class YearEndDbAct implements Job {
	private static Logger LOGGER = Logger.getLogger(YearEndDbAct.class);

	@Autowired
	private AccessLogService log;

	@Autowired
	private AuditService audit;

	@Autowired
	private SchedulerService schedulerService;

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		String dbUrl = null;
		String user = null;
		String pwd = null;
		String archiveDB = null;
		String schema = null;
		String jdbcDriver = null;
		String status = "Executed";
		if (log == null) {
			log = ApplicationContextProvider.getContext().getBean(AccessLogService.class);
		}
		if (audit == null) {
			audit = ApplicationContextProvider.getContext().getBean(AuditService.class);
		}
		if (schedulerService == null) {
			schedulerService = ApplicationContextProvider.getContext().getBean(SchedulerService.class);
		}
		InputStream inputStream2 = null;
		Properties scheduleProperties = new Properties();
		try {
			inputStream2 = getClass().getClassLoader().getResourceAsStream("application-dev.properties");
			scheduleProperties.load(inputStream2);
			dbUrl = scheduleProperties.getProperty("spring.datasource.dburl");
			user = scheduleProperties.getProperty("spring.datasource.username");
			pwd = scheduleProperties.getProperty("spring.datasource.password");
			archiveDB = scheduleProperties.getProperty("spring.datasource.archivedb");
			schema = scheduleProperties.getProperty("spring.jpa.properties.hibernate.default_schema");
			jdbcDriver = scheduleProperties.getProperty("spring.datasource.driver-class-name");
		} catch (IOException ex) {
			LOGGER.error("Scheduler - Failed to read property file.", ex);
		} finally {
			if (inputStream2 != null) {
				try {
					inputStream2.close();
				} catch (IOException e) {
					LOGGER.error("Scheduler - Failed to close property file.", e);
				}
			}
		}
		JobDataMap dataMap = context.getJobDetail().getJobDataMap();
		String jobName = dataMap.getString("jobName");
		Connection conn2 = null;
		Statement stmt = null;
		String archiveUpdate = "select mfmaindb.archive_ntpoms();";
		try {
			Class.forName(jdbcDriver);
			conn2 = DriverManager.getConnection(dbUrl + archiveDB, user, pwd);
			stmt = conn2.createStatement();
			int resultArchive = stmt.executeUpdate(archiveUpdate);
			if (resultArchive != 0) {
				status = "Failed";
			}
			conn2.close();
			stmt.close();
		} catch (ClassNotFoundException e1) {
			LOGGER.error("Failed connecting to database - ClassNotFoundException.", e1);
		} catch (SQLException ex) {
			LOGGER.error("Failed connecting to database - SQLException occured.", ex);
		} catch (Exception e) {
			status = "Failed";
			LOGGER.error("Failed Dropping Database.", e);
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
				LOGGER.error("Failed closing to connection.", e);
			}
			try {
				if (conn2 != null) {
					conn2.close();
				}
			} catch (SQLException e) {
				LOGGER.error("Failed connecting to database - SQLException occured.", e);
			}
		}
		Scheduler sched = schedulerService.getSchedulerByJobname(jobName);
		sched.setStatus(status);
		schedulerService.createScheduler(sched);
	}
}