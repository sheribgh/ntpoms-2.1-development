package my.com.parkson.ntpoms.poadmin.web.scheduler;

import java.util.Date;

import org.apache.log4j.Logger;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.services.AccessLogService;
import my.com.parkson.ntpoms.common.services.AuditService;
import my.com.parkson.ntpoms.utils.ApplicationContextProvider;

@SuppressWarnings("unchecked")
public class QuartzJob {
	private static Logger LOGGER = Logger.getLogger(QuartzJob.class);
	private static final String DEBUG = "DEBUG";

	@Autowired
	private static AuditService audit;

	@SuppressWarnings("rawtypes")
	public static void schedOneTime(String user, String jobName, java.util.Date startDate, java.util.Date endDate,
			Class className) throws SchedulerException {
		try {
			if (audit == null) {
				audit = ApplicationContextProvider.getContext().getBean(AuditService.class);
			}
			JobDetail job = JobBuilder.newJob(className).withIdentity(new JobKey(jobName)).build();
			Trigger trigger = TriggerBuilder.newTrigger().withIdentity(jobName).startAt(startDate).endAt(endDate)
					.build();
			job.getJobDataMap().put("jobName", jobName);
			job.getJobDataMap().put("user", user);
			Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
			scheduler.start();
			scheduler.scheduleJob(job, trigger);
			LOGGER.debug(DEBUG + ": Successfully created - quartz job- one time.");
			audit.putAudit(new Date(), "Schedule job Created Successfully - Schedule for one time.", null,
					"SCHEDULE-ONE-TIME");
		} catch (Exception ex) {
			LOGGER.equals(ex);
		}
	}

	@SuppressWarnings("rawtypes")
	public static void schedRec(String user, String jobName, java.util.Date startDate, String cron, Class className)
			throws SchedulerException {
		if (audit == null) {
			audit = ApplicationContextProvider.getContext().getBean(AuditService.class);
		}
		try {
			JobDetail job = JobBuilder.newJob(className).withIdentity(new JobKey(jobName)).build();
			Trigger trigger = TriggerBuilder.newTrigger().withIdentity(jobName).startAt(startDate)
					.withSchedule(CronScheduleBuilder.cronSchedule(cron)).build();
			job.getJobDataMap().put("jobName", jobName);
			job.getJobDataMap().put("user", user);
			Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
			scheduler.start();
			scheduler.scheduleJob(job, trigger);
			LOGGER.debug(DEBUG + ": Successfully created - quartz job- recurring.");
			audit.putAudit(new Date(), "Schedule job Created Successfully - Schedule for recurring.", null,
					"SCHEDULE-RECURRING");
		} catch (POException ex) {
			LOGGER.error(ex);
		}
	}

	private QuartzJob() {
	}
}