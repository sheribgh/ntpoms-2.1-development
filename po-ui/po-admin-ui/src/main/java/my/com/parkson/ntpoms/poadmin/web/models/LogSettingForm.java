/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.poadmin.web.models;

import java.io.Serializable;

public class LogSettingForm implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int logSettingId;

	private int logSetting;

	/**
	 * @return the logSettingId
	 */
	public int getLogSettingId() {
		return logSettingId;
	}

	/**
	 * @param logSettingId
	 *            the logSettingId to set
	 */
	public void setLogSettingId(int logSettingId) {
		this.logSettingId = logSettingId;
	}

	/**
	 * @return the logSetting
	 */
	public int getLogSetting() {
		return logSetting;
	}

	/**
	 * @param logSetting
	 *            the logSetting to set
	 */
	public void setLogSetting(int logSetting) {
		this.logSetting = logSetting;
	}

}
