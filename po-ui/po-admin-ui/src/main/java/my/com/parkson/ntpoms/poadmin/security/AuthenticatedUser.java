/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.poadmin.security;

import static java.util.stream.Collectors.toSet;

import java.util.Collection;
import java.util.stream.Stream;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import lombok.EqualsAndHashCode;
import lombok.Getter;

import my.com.parkson.ntpoms.common.entities.User;

/**
 * The Class AuthenticatedUser.
 */

@EqualsAndHashCode(callSuper = true)
public class AuthenticatedUser extends org.springframework.security.core.userdetails.User {

	private static final long serialVersionUID = 1L;

	/**
	 * Gets the authorities.
	 *
	 * @param user the user
	 * @return the authorities
	 */
	private static Collection<? extends GrantedAuthority> getAuthorities(User user) {
		return user.getRoles().stream()
				.flatMap(role -> Stream.concat(Stream.of(new SimpleGrantedAuthority(role.getName())),
						role.getPermissions().stream()
								.map(permission -> new SimpleGrantedAuthority("ROLE_" + permission.getName()))))
				.collect(toSet());

	}

	@Getter
	private User user;

	/**
	 * Instantiates a new authenticated user.
	 *
	 * @param user the user
	 */
	public AuthenticatedUser(User user) {
		super(user.getUsername(), user.getPassword(), getAuthorities(user));
		this.user = user;
	}

	public User getUser() {
		return user;
	}

}
