/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.poadmin.web.models;

import java.io.Serializable;

public class OrderSubCategoryForm implements Serializable {

	private static final long serialVersionUID = 1L;

	private String ordSubCCode;

	private String ordSubCDesc;

	private String ordSubCAbbrDesc;

	private String createdBy;

	private String createdOn;

	private String lastModifiedBy;

	private String lastModifiedOn;

	private String deactivatedBy;

	private String deactivatedOn;

	private String reactivatedBy;

	private String reactivatedOn;

	private String isActive;

	private String oderCategory;

	/**
	 * @return the ordSubCCode
	 */
	public String getOrdSubCCode() {
		return ordSubCCode;
	}

	/**
	 * @param ordSubCCode
	 *            the ordSubCCode to set
	 */
	public void setOrdSubCCode(String ordSubCCode) {
		this.ordSubCCode = ordSubCCode;
	}

	/**
	 * @return the ordSubCDesc
	 */
	public String getOrdSubCDesc() {
		return ordSubCDesc;
	}

	/**
	 * @param ordSubCDesc
	 *            the ordSubCDesc to set
	 */
	public void setOrdSubCDesc(String ordSubCDesc) {
		this.ordSubCDesc = ordSubCDesc;
	}

	/**
	 * @return the ordSubCAbbrDesc
	 */
	public String getOrdSubCAbbrDesc() {
		return ordSubCAbbrDesc;
	}

	/**
	 * @param ordSubCAbbrDesc
	 *            the ordSubCAbbrDesc to set
	 */
	public void setOrdSubCAbbrDesc(String ordSubCAbbrDesc) {
		this.ordSubCAbbrDesc = ordSubCAbbrDesc;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdOn
	 */
	public String getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn
	 *            the createdOn to set
	 */
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the lastModifiedBy
	 */
	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	/**
	 * @param lastModifiedBy
	 *            the lastModifiedBy to set
	 */
	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	/**
	 * @return the lastModifiedOn
	 */
	public String getLastModifiedOn() {
		return lastModifiedOn;
	}

	/**
	 * @param lastModifiedOn
	 *            the lastModifiedOn to set
	 */
	public void setLastModifiedOn(String lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

	/**
	 * @return the deactivatedBy
	 */
	public String getDeactivatedBy() {
		return deactivatedBy;
	}

	/**
	 * @param deactivatedBy
	 *            the deactivatedBy to set
	 */
	public void setDeactivatedBy(String deactivatedBy) {
		this.deactivatedBy = deactivatedBy;
	}

	/**
	 * @return the deactivatedOn
	 */
	public String getDeactivatedOn() {
		return deactivatedOn;
	}

	/**
	 * @param deactivatedOn
	 *            the deactivatedOn to set
	 */
	public void setDeactivatedOn(String deactivatedOn) {
		this.deactivatedOn = deactivatedOn;
	}

	/**
	 * @return the reactivatedBy
	 */
	public String getReactivatedBy() {
		return reactivatedBy;
	}

	/**
	 * @param reactivatedBy
	 *            the reactivatedBy to set
	 */
	public void setReactivatedBy(String reactivatedBy) {
		this.reactivatedBy = reactivatedBy;
	}

	/**
	 * @return the reactivatedOn
	 */
	public String getReactivatedOn() {
		return reactivatedOn;
	}

	/**
	 * @param reactivatedOn
	 *            the reactivatedOn to set
	 */
	public void setReactivatedOn(String reactivatedOn) {
		this.reactivatedOn = reactivatedOn;
	}

	/**
	 * @return the isActive
	 */
	public String getIsActive() {
		return isActive;
	}

	/**
	 * @param isActive
	 *            the isActive to set
	 */
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getOderCategory() {
		return oderCategory;
	}

	public void setOderCategory(String oderCategory) {
		this.oderCategory = oderCategory;
	}
	

}