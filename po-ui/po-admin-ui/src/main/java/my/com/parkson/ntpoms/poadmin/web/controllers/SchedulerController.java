/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.poadmin.web.controllers;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;

import javax.mail.Authenticator;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.sql.DataSource;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.quartz.CronExpression;
import org.quartz.JobDataMap;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.cronutils.QuartzCronExpressionFormatter;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.entities.EmployeeMaster;
import my.com.parkson.ntpoms.common.entities.Scheduler;
import my.com.parkson.ntpoms.common.entities.SystemParameter;
import my.com.parkson.ntpoms.common.entities.User;
import my.com.parkson.ntpoms.common.model.EmailCriteria;
import my.com.parkson.ntpoms.common.services.AccessLogService;
import my.com.parkson.ntpoms.common.services.AuditService;
import my.com.parkson.ntpoms.common.services.EmailService;
import my.com.parkson.ntpoms.common.services.SchedulerService;
import my.com.parkson.ntpoms.common.services.SystemParameterService;
import my.com.parkson.ntpoms.common.services.UsersService;
import my.com.parkson.ntpoms.poadmin.security.SecurityUtil;
import my.com.parkson.ntpoms.poadmin.web.models.FileUploadParam;
import my.com.parkson.ntpoms.poadmin.web.models.Pager;
import my.com.parkson.ntpoms.poadmin.web.models.SchedulerForm;
import my.com.parkson.ntpoms.poadmin.web.models.UsersForm;
import my.com.parkson.ntpoms.poadmin.web.scheduler.MasterFileUpload;
import my.com.parkson.ntpoms.poadmin.web.utils.QuartzSchedulerUtils;
import my.com.parkson.ntpoms.utils.ApplicationContextProvider;
import my.com.parkson.ntpoms.utils.EmailServiceUtils;

@Slf4j
@Controller
@Secured(SecurityUtil.MANAGE_SCHEDULER)
@RequiredArgsConstructor
public class SchedulerController extends AbstractAdminController {
	private static Logger LOGGER = Logger.getLogger(SchedulerController.class);

	private static final String ERROR = "ERROR";

	private static final String REDIRECT_MASTER_SCHEDULER = "redirect:/master/scheduler";

	private List<User> expiredPasswords = new ArrayList<>();
	private List<User> expiringPasswords = new ArrayList<>();

	private static final int BUTTONS_TO_SHOW = 5;
	private static final int INITIAL_PAGE = 0;
	private static final int INITIAL_PAGE_SIZE = 5;
	private static final int[] PAGE_SIZES = { 5, 10, 20 };
	private static final String DATE_FORMAT_KEY = "DATE_FORMAT";
	private static final String DATE_FORMAT_VALUE = "dd-MMM-yyyy";
	private static final String DATE_TIME_FORMAT_KEY = "DATE_TIME_FORMAT";
	private static final String DATE_TIME_FORMAT_VALUE = "dd-MMM-yyyy hh:mm:ss";

	@Autowired
	private AuditService audit;

	@Autowired
	DataSource dataSource;

	@Autowired
	private UsersService usersService;

	@Autowired
	private SystemParameterService systemParameterService;

	@Autowired
	private SchedulerService schedulerService;

	/**
	 * On page load scheduler.
	 *
	 * @param pageSize
	 *            the page size
	 * @param page
	 *            the page
	 * @param task
	 *            the task
	 * @param scheduleType
	 *            the schedule type
	 * @return the model and view
	 */
	@GetMapping("/master/scheduler")
	public ModelAndView onPageLoadScheduler() {
		ModelAndView modelAndView = new ModelAndView("/master/scheduler");
		modelAndView.getModelMap().addAttribute("schedulerobj", new SchedulerForm());

		return modelAndView;
	}

	@RequestMapping(value = "/master/schedulergrid", method = RequestMethod.GET)
	public String schedulerGrid(@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page, @RequestParam("task") Optional<String> task,
			@RequestParam("startdate") Optional<String> startdate, @RequestParam("status") Optional<String> isActive,
			@RequestParam("scheduleType") Optional<String> scheduleType, Model model,
			@RequestParam("sortingkey") Optional<String> sortingkey,
			@RequestParam("sortingorder") Optional<String> sortingorder) {

		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
		Page<Scheduler> list;
		Specification<Scheduler> spec = new Specification<Scheduler>() {
			@Override
			public Predicate toPredicate(Root<Scheduler> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> ls = new ArrayList<>();
				if (task.isPresent() && !task.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("task")), "%" + task.get().toLowerCase() + "%"));
				}
				if (scheduleType.isPresent() && !scheduleType.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("scheduleType")), "%" + scheduleType.get().toLowerCase() + "%"));
				}
				if (startdate.isPresent() && !startdate.get().isEmpty()) {
					Expression<String> dateStringExpr = cb.function("to_char", String.class, root.get("startdate"),
							cb.literal("YYYY-MM-DD HH12:MI:SS"));
					ls.add(cb.like(cb.lower(dateStringExpr), "%" + startdate.get().toLowerCase() + "%"));
				}
				if (isActive.isPresent() && !isActive.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("status")), "%" + isActive.get().toLowerCase() + "%"));
				}
				return cb.and(ls.toArray(new Predicate[0]));
			}
		};
		Sort sorttemp = new Sort(Sort.Direction.DESC, "id");
		if (sortingkey.isPresent() && sortingorder.isPresent()) {
			sorttemp = new Sort(sortingorder.get().equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC,
					sortingkey.get());
		}
		if (task.isPresent() || scheduleType.isPresent() || startdate.isPresent() || isActive.isPresent()) {
			list = schedulerService.getAllPageableScheduler(spec, new PageRequest(evalPage, evalPageSize, sorttemp));
		} else {
			list = schedulerService.getAllPageableScheduler(new PageRequest(evalPage, evalPageSize, sorttemp));
		}
		int serialid = 0;
		int pagesize = evalPage + 1;
		if (pagesize > 1) {
			int start = evalPageSize * pagesize;
			serialid = start - evalPageSize;
		}
		Pager pager = new Pager(list.getTotalPages(), list.getNumber(), BUTTONS_TO_SHOW);
		model.addAttribute("serialid", serialid);
		model.addAttribute("list", list);
		model.addAttribute("selectedPageSize", evalPageSize);
		model.addAttribute("pageSizes", PAGE_SIZES);
		model.addAttribute("pager", pager);
		model.addAttribute("totalRecords", list.getTotalElements());
		model.addAttribute("task", task.isPresent() ? task.get() : "");
		model.addAttribute("scheduleType", scheduleType.isPresent() ? scheduleType.get() : "");
		model.addAttribute("startdate", startdate.isPresent() ? startdate.get() : "");
		model.addAttribute("status", isActive.isPresent() ? isActive.get() : "");
		model.addAttribute("sortingorder", sortingorder.isPresent() ? sortingorder.get() : "DESC");
		model.addAttribute("sortingkey", sortingkey.isPresent() ? sortingkey.get() : "id");

		return "/master/scheduler :: result-table";
	}

	/**
	 * Creates the scheduelr.
	 *
	 * @param obj
	 *            the obj
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @param redirectAttributes
	 *            the redirect attributes
	 * @return the string
	 * @throws ParseException
	 */
	@PostMapping(value = "/master/createscheduler")
	public String createScheduelr(@Valid @ModelAttribute("schedulerobj") SchedulerForm obj, BindingResult result,
			Model model, RedirectAttributes redirectAttributes) throws ParseException {
		try {
			if (obj.getTask() != null) {
				if (obj.getTask().contains("MasterTableUpload")) {
					SystemParameter uploadFolderPath = systemParameterService
							.getSystemParameterByPropName("UPLOAD_FOLDER_PATH");
					SystemParameter logFolderPath = systemParameterService
							.getSystemParameterByPropName("LOG_FOLDER_PATH");
					SystemParameter successFolderPath = systemParameterService
							.getSystemParameterByPropName("SUCCESS_FOLDER_PATH");
					SystemParameter failedFolderPath = systemParameterService
							.getSystemParameterByPropName("FAILED_FOLDER_PATH");
					if (uploadFolderPath == null) {
						redirectAttributes.addFlashAttribute("error",
								"System Parameter UPLOAD_FOLDER_PATH does not exist.");
						return REDIRECT_MASTER_SCHEDULER;
					}
					if (logFolderPath == null) {
						redirectAttributes.addFlashAttribute("error",
								"System Parameter LOG_FOLDER_PATH does not exist.");
						return REDIRECT_MASTER_SCHEDULER;
					}
					if (successFolderPath == null) {
						redirectAttributes.addFlashAttribute("error",
								"System Parameter SUCCESS_FOLDER_PATH does not exist.");
						return REDIRECT_MASTER_SCHEDULER;
					}
					if (failedFolderPath == null) {
						redirectAttributes.addFlashAttribute("error",
								"System Parameter FAILED_FOLDER_PATH does not exist.");
						return REDIRECT_MASTER_SCHEDULER;
					}
				} else if (obj.getTask().contains("PasswordExpiry")) {
					SystemParameter pwdLifetimeLength = systemParameterService
							.getSystemParameterByPropName("PASSWORD_LIFETIME_LENGTH");
					SystemParameter pwdNotifLength = systemParameterService
							.getSystemParameterByPropName("PASSWORD_NOTIF_LENGTH");
					if (pwdLifetimeLength == null) {
						redirectAttributes.addFlashAttribute("error",
								"System Parameter PASSWORD_LIFETIME_LENGTH does not exist.");
						return REDIRECT_MASTER_SCHEDULER;
					}
					if (pwdNotifLength == null) {
						redirectAttributes.addFlashAttribute("error",
								"System Parameter PASSWORD_NOTIF_LENGTH does not exist.");
						return REDIRECT_MASTER_SCHEDULER;
					}
				}
			} else {
				redirectAttributes.addFlashAttribute("error", "Error creating scheduler");
			}
			if (obj.getCronfrequency_amount() == null || obj.getCronfrequency_amount().isEmpty()) {
				redirectAttributes.addFlashAttribute("error", "Please generate cron expression.");
				return REDIRECT_MASTER_SCHEDULER;
			}
			if (schedulerService.getSchedulerByJobname(obj.getTask()) != null) {
				redirectAttributes.addFlashAttribute("error", "Scheduler Task:  " + obj.getTask() + " already exists.");
				return REDIRECT_MASTER_SCHEDULER;
			}
			if (!result.hasErrors()) {
				Scheduler ad = new Scheduler();
				ad.setTask(obj.getTask());
				ad.setStartdate(obj.getStartdate());
				ad.setCreatedBy(getCurrentUser().getUsername());
				ad.setCreatedOn(new Date());
				ad.setStatus("Scheduled");
				ad.setJobname(ad.getTask());
				ad.setScheduleType(obj.getScheduleType());
				ad.setCronexpression(obj.getCronfrequency_amount());
				ad.setUpload(obj.getUpload());
				ad.setActive(!obj.getIsActive().contains("InActive"));
				ad.setCreatedOn(new Date());
				CronExpression c = new CronExpression(obj.getCronfrequency_amount());
				ad.setEndDate(c.getNextValidTimeAfter(new Date()));
				ad.setScheduleType(obj.getScheduleType());
				QuartzCronExpressionFormatter formatter = new QuartzCronExpressionFormatter();
				ad.setCrondetails(formatter.print(new CronExpression(obj.getCronfrequency_amount()), Locale.UK));
				schedulerService.createScheduler(ad);
				/* Scheduling job */
				if (obj.getTask().contains("MasterTableUpload")) {
					QuartzSchedulerUtils.startSchedulerBulkUpload(getCurrentUser().getUsername(), obj.getTask(), ad);
				} else if (obj.getTask().contains("PasswordExpiry")) {
					QuartzSchedulerUtils.startSchedulerPasswordExpiry(getCurrentUser().getUsername(), obj.getTask(),
							ad);
				} else if (obj.getTask().contains("PurchaseOrderExpiry")) {
					QuartzSchedulerUtils.startSchedulerPoExpiry(getCurrentUser().getUsername(), obj.getTask(), ad);
				} else if (obj.getTask().contains("YearEndDBActivity")) {
					QuartzSchedulerUtils.startSchedulerYearEndDb(getCurrentUser().getUsername(), obj.getTask(), ad);
				}
				audit.putAudit(new Date(), "Scheduler : " + ad.getJobname() + "created successfully",
						getCurrentUser().getUsername(), " Create Scheduler");
				redirectAttributes.addFlashAttribute("info", "Scheduler created successfully");
			} else {
				redirectAttributes.addFlashAttribute("error", "Error creating scheduler");
			}
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return REDIRECT_MASTER_SCHEDULER;
	}

	/**
	 * Update scheduer.
	 *
	 * @param obj
	 *            the obj
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @param redirectAttributes
	 *            the redirect attributes
	 * @return the string
	 * @throws ParseException
	 */
	@PostMapping(value = "/master/updatescheduler")
	public String updateScheduer(@Valid @ModelAttribute("schedulerobj") SchedulerForm obj, BindingResult result,
			Model model, RedirectAttributes redirectAttributes) throws ParseException {
		Scheduler ad = schedulerService.getSchedulerById(obj.getSchedulerId());
		boolean isActive = true;
		if (obj.getIsActive().contains("InActive")) {
			isActive = false;
		}
		try {
			if (!ad.getCronexpression().equals(obj.getCronfrequency_amount())) {
				LOGGER.info(ad.getCronexpression() + " = " + obj.getCronfrequency_amount());
				ad.setDeactivatedOn(new Date());
				ad.setDeactivatedBy(getCurrentUser().getUser().getUsername());
				ad.setEndDate(new Date());
				QuartzCronExpressionFormatter formatter = new QuartzCronExpressionFormatter();
				ad.setCrondetails(formatter.print(new CronExpression(obj.getCronfrequency_amount()), Locale.UK));
				ad.setCronexpression(obj.getCronfrequency_amount());
				CronExpression c = new CronExpression(obj.getCronfrequency_amount());
				ad.setEndDate(c.getNextValidTimeAfter(new Date()));
				QuartzSchedulerUtils.reSchedule(ad);
				audit.putAudit(new Date(),
						"Updated " + ad.getTask() + " successfully changed Cron to '" + ad.getCrondetails()
								+ "' - SchedulerController.",
						getCurrentUser().getUser().getUsername(), "Updated Scheduler");
			}
			if (isActive != ad.isActive()) {
				ad.setActive(isActive);
				ad.setDeactivatedOn(new Date());
				ad.setDeactivatedBy(getCurrentUser().getUser().getUsername());
				audit.putAudit(new Date(),
						"Updated " + ad.getTask() + " successfully changed to " + obj.getIsActive()
								+ " - SchedulerController.",
						getCurrentUser().getUser().getUsername(), "Updated Scheduler");
				if (!isActive) {
					QuartzSchedulerUtils.pauseScheduler(ad);
				}
			}
			ad.setStatus("Scheduled");
			schedulerService.createScheduler(ad);
			redirectAttributes.addFlashAttribute("info", "Scheduler Updated successfully");
		} catch (Exception e) {
			LOGGER.error(e);
			redirectAttributes.addFlashAttribute("error", "Error while update scheduler");
		}

		return REDIRECT_MASTER_SCHEDULER;
	}

	/**
	 * Edits the scheduler.
	 *
	 * @param schid
	 *            the schid
	 * @param model
	 *            the model
	 * @return the string
	 */
	@GetMapping(value = "/master/editviewscheduler/{schid}")
	public String editScheduler(@PathVariable Integer schid, Model model) {
		Scheduler ad = schedulerService.getSchedulerById(schid);
		SchedulerForm obj = new SchedulerForm();

		String dateFormat = DATE_TIME_FORMAT_VALUE;
		SystemParameter dateObj = systemParameterService.getSystemParameterByPropName(DATE_TIME_FORMAT_KEY);
		if (dateObj != null && !dateObj.getPropValue().isEmpty()) {
			dateFormat = dateObj.getPropValue();
		}
		DateFormat formatter = new SimpleDateFormat(dateFormat);
		String dateFormat2 = DATE_FORMAT_VALUE;
		SystemParameter dateObj2 = systemParameterService.getSystemParameterByPropName(DATE_FORMAT_KEY);
		if (dateObj2 != null && !dateObj2.getPropValue().isEmpty()) {
			dateFormat2 = dateObj2.getPropValue();
		}

		DateFormat format = new SimpleDateFormat(dateFormat2);
		String createdOn = "";
		String modifiedOn = "";

		if (ad.getCreatedOn() != null) {
			createdOn = formatter.format(ad.getCreatedOn());
		}
		if (ad.getDeactivatedOn() != null) {
			modifiedOn = formatter.format(ad.getDeactivatedOn());
		}

		if (ad != null) {
			obj.setCreatedBy(ad.getCreatedBy());
			obj.setCreatedOn(createdOn);
			obj.setCronfrequency_amount(ad.getCronexpression());
			obj.setDeactivatedBy(ad.getDeactivatedBy());
			obj.setDeactivatedOn(modifiedOn);
			obj.setTask(ad.getTask());
			obj.setStatus(ad.getStatus());
			obj.setCronfrequency_occurrence(ad.getCrondetails());
			obj.setIsActive(ad.isActive() ? "Active" : "InActive");
			obj.setSchedulerId(ad.getId());
			obj.setUpload(ad.getUpload());
			obj.setEndDate(ad.getEndDate());
			model.addAttribute("schedulerobj", obj);
			model.addAttribute("edit", true);
			model.addAttribute("schid", schid);
			return "/master/edit_scheduler";
		} else {
			return REDIRECT_MASTER_SCHEDULER;
		}

	}

	@GetMapping(value = "/master/stopScheduler/{schid}")
	public String stopScheduler(@PathVariable Integer schid, Model model, RedirectAttributes redirectAttributes)
			throws SchedulerException {
		try {
			Scheduler ad = schedulerService.getSchedulerById(schid);
			if (ad != null) {
				// if (ad.getStatus().contains("Scheduled")) {
				// boolean stopped = scheduler.deleteJob(new
				// JobKey(ad.getJobname()));
				// if (stopped) {
				// redirectAttributes.addFlashAttribute("info", "Job is stopped
				// successfully");
				// ad.setStatus("Stopped");
				// ad.setDeactivatedBy("superadmin");
				// ad.setDeactivatedOn(new Date());
				// } else {
				// redirectAttributes.addFlashAttribute("info", "Job failed to
				// stop.");
				// }
				// } else if (ad.getStatus().contains("Executed")) {
				// redirectAttributes.addFlashAttribute("info", "Job was already
				// executed and
				// could not be stopped.");
				// } else if (ad.getStatus().contains("Stopped")) {
				// redirectAttributes.addFlashAttribute("info", "Job has already
				// been
				// stopped.");
				// } else {
				// redirectAttributes.addFlashAttribute("info", "Job already
				// executed and
				// failed. Job could not be stopped.");
				// }
				// audit.putAudit(new Date(), "Scheduler : " + ad.getJobname() +
				// "stopped and
				// deleted successfully", getCurrentUser().getUsername(), " Stop
				// Scheduler");
			} else {
				schedulerService.deleteSched(ad);
				return REDIRECT_MASTER_SCHEDULER;
			}
			schedulerService.deleteSched(ad);
			QuartzSchedulerUtils.deleteScheduler(ad);
			redirectAttributes.addFlashAttribute("info", "Job was successfully stopped and deleted.");
		} catch (Exception ex) {
			LOGGER.error(ex);
			redirectAttributes.addFlashAttribute("error", "Error while stoping scheduler");
		}
		return REDIRECT_MASTER_SCHEDULER;
	}

	@RequestMapping(value = "/master/populateCronDetails", method = RequestMethod.GET)
	public @ResponseBody String getCronDetails(@RequestParam("cron") String cron) throws ParseException {
		QuartzCronExpressionFormatter formatter = new QuartzCronExpressionFormatter();
		String crondetails = formatter.print(new CronExpression(cron), Locale.UK);
		return crondetails;
	}

	@GetMapping(value = "/master/triggerNowScheduler/{schid}")
	public String triggerNowScheduler(@PathVariable Integer schid, Model model, RedirectAttributes redirectAttributes) {

		try {
			Scheduler ad = schedulerService.getSchedulerById(schid);
			if (ad != null) {
				if (ad.getTask().contains("MasterTableUpload")) {

					String status = "Executed";
					FileUploadParam fileUploadParam = new FileUploadParam();

					try {
						SystemParameter uploadFolderPath = systemParameterService
								.getSystemParameterByPropName("UPLOAD_FOLDER_PATH");
						SystemParameter logFolderPath = systemParameterService
								.getSystemParameterByPropName("LOG_FOLDER_PATH");
						SystemParameter successFolderPath = systemParameterService
								.getSystemParameterByPropName("SUCCESS_FOLDER_PATH");
						SystemParameter failedFolderPath = systemParameterService
								.getSystemParameterByPropName("FAILED_FOLDER_PATH");
						boolean isNullFlag = true;
						if (uploadFolderPath != null) {
							fileUploadParam.setUploadFolder(uploadFolderPath.getPropValue());
							isNullFlag = false;
						}
						if (logFolderPath != null) {
							fileUploadParam.setLogFolder(logFolderPath.getPropValue());
							isNullFlag = false;
						}
						if (successFolderPath != null) {
							fileUploadParam.setSuccessFolder(successFolderPath.getPropValue());
							isNullFlag = false;
						}
						if (failedFolderPath != null) {
							fileUploadParam.setFailedFolder(failedFolderPath.getPropValue());
							isNullFlag = false;
						}
						if (isNullFlag) {
							status = "FAILED";
						}
					} catch (Exception ex) {
						status = "FAILED";
						LOGGER.error(ex);
					}
					try {

						String user = getCurrentUser().getUser().getUsername();
						String jobName = ad.getTask();
						fileUploadParam.setUsername(user);
						fileUploadParam.setJobName(jobName);
						fileUploadParam.setDataSource(dataSource);
						MasterFileUpload masterFileUpload = new MasterFileUpload(fileUploadParam);
						masterFileUpload.bulkUpload();
						if (status.contains("FAILED")) {

							redirectAttributes.addFlashAttribute("info", ad.getJobname() + " trigger Failed. ");

						} else {

							redirectAttributes.addFlashAttribute("info",
									ad.getJobname() + " has been triggered successfully. ");

						}

					} catch (IOException e) {
						LOGGER.error(e);
					}

					audit.putAudit(new Date(), "Scheduler : " + ad.getJobname() + "triggered successfully",
							getCurrentUser().getUsername(), " Scheduler");

				} else if (ad.getTask().contains("PasswordExpiry")) {

					String status = "Executed";

					try {
						SystemParameter pwdLifetimeLength = systemParameterService
								.getSystemParameterByPropName("PASSWORD_LIFETIME_LENGTH");
						SystemParameter pwdNotifLength = systemParameterService
								.getSystemParameterByPropName("PASSWORD_NOTIF_LENGTH");
						if (pwdLifetimeLength == null || pwdNotifLength == null) {
							status = "Failed";
						}
					} catch (NullPointerException ex) {
						LOGGER.error(ex);
						status = "Failed";
					}
					List<User> admins = usersService.getAllUsers();
					Date today = new Date();
					int difference = 0;
					for (User admin : admins) {
						difference = daysBetween(admin.getCreatedOn(), today);
						if (difference >= 30) {
							admin.setBlocked(difference);
							expiredPasswords.add(admin);
						} else {
							LOGGER.info("USERNAME: " + admin.getUsername() + " DIFFERENCE : " + difference);
							if (difference < 3) {
								admin.setBlocked(difference);
								expiringPasswords.add(admin);
							}
						}
					}
					for (User expired : expiredPasswords) {
						accountExpUpdate(expired);
					}
					for (User expiring : expiringPasswords) {
						passwordExpUpdate(expiring);
					}

					if (status.contains("Failed")) {

						redirectAttributes.addFlashAttribute("info", ad.getJobname() + " trigger Failed. ");

					} else {

						redirectAttributes.addFlashAttribute("info",
								ad.getJobname() + " has been triggered successfully. ");

					}

					audit.putAudit(new Date(), "Scheduler : " + ad.getJobname() + "triggered successfully",
							getCurrentUser().getUsername(), " Scheduler");

					/*************************
					 * END OF PASSWORDEXP TRIGGER
					 ***************************************/

				} else if (ad.getTask().contains("POExpiry")) {
					QuartzSchedulerUtils.triggerNowSchedulerPoExpiry(ad);
				} else {
					QuartzSchedulerUtils.triggerNowSchedulerYearEndDb(ad);
				}
			} else {
			}
		} catch (Exception ex) {
			redirectAttributes.addFlashAttribute("error", "Error while trigger scheduler");
			LOGGER.error(ex);
		}
		return REDIRECT_MASTER_SCHEDULER;
	}

	public void passwordExpUpdate(User admin) {
		try {
			EmailService emailService = ApplicationContextProvider.getContext().getBean(EmailService.class);
			Map<String, String> context = new HashMap<>();
			context.put("user_name", admin.getUsername());
			context.put("num_days", "" + admin.getBlocked());
			context.put("class_name", "PoEmailGenerator");
			context.put("system_name", "NTPOMS");
			context.put("proprietor", "parkson.com");
			Set<String> cc = new HashSet<>();
			EmailCriteria email = new EmailCriteria();
			email.setRecipient(admin.getEmail());
			email.setSubject("Password Change Notification.");
			email.setCc(cc);
			Properties props = EmailServiceUtils.loadEmailPropertyFile();
			email.setTemplateName(props.getProperty("mail.vm.template.pwdexpiry"));
			email.setEmailUser(props.getProperty("mail.user"));
			email.setEmailPwd(props.getProperty("mail.passwd"));
			Session session = Session.getDefaultInstance(props, new Authenticator() {
				@Override
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(email.getEmailUser(), email.getEmailPwd());
				}
			});
			emailService.sendEmail(session, context, null, email);
		} catch (MessagingException | IOException e) {
			LOGGER.error(e);
		}
	}

	public void accountExpUpdate(User admin) {
		admin.setActive(false);
		usersService.createUsers(admin);
		audit.putAudit(new Date(),
				"Password de-activated succefully. [" + admin.getUsername() + "] - Password Account Expiry.",
				admin.getUsername(), "PASSWORD_DEACTIVATED");
		LOGGER.info("Password de-activated succefully.");
	}

	public int daysBetween(Date expiration, Date today) {
		return (int) ((today.getTime() - expiration.getTime()) / (1000 * 60 * 60 * 24));
	}

	@Override
	protected String getHeaderTitle() {
		return "Manage Scheduler";
	}
}