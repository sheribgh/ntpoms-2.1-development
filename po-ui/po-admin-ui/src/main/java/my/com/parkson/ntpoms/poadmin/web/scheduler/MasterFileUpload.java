/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-admin" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.poadmin.web.scheduler;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import javax.mail.Authenticator;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import my.com.parkson.ntpoms.common.entities.Scheduler;
import my.com.parkson.ntpoms.common.entities.User;
import my.com.parkson.ntpoms.common.model.EmailCriteria;
import my.com.parkson.ntpoms.common.model.FailUpload;
import my.com.parkson.ntpoms.common.services.AuditService;
import my.com.parkson.ntpoms.common.services.EmailService;
import my.com.parkson.ntpoms.common.services.SchedulerService;
import my.com.parkson.ntpoms.common.services.UsersService;
import my.com.parkson.ntpoms.poadmin.web.models.FileUploadParam;
import my.com.parkson.ntpoms.utils.ApplicationContextProvider;
import my.com.parkson.ntpoms.utils.EmailServiceUtils;

public class MasterFileUpload {
	private static Logger LOGGER = Logger.getLogger(MasterFileUpload.class);

	private static final String ERROR = "ERROR";
	private static final String WARNING = "WARNING";
	private static final String FAILED = "Failed";
	private static final String PARTIAL = "Partial";

	private String pathSeperator = "\\";

	private int totalFiles;
	private int totalFilesSuccess = 0;
	private int totalFilesFailed = 0;

	private FileUploadParam fileUploadParam;
	private IFileTypeMaster fileTypeMaster;

	private static List<FailUpload> failFileList = new ArrayList<>();

	@Autowired
	private EmailService emailService;

	@Autowired
	private UsersService usersService;

	@Autowired
	private SchedulerService schedulerService;

	@Autowired
	private AuditService audit;

	public MasterFileUpload(FileUploadParam fileUploadParam) {
		String os = System.getProperty("os.name");
		if (os.contains("Linux")) {
			this.pathSeperator = "/";
		}
		this.fileUploadParam = fileUploadParam;
	}

	public List<String> bulkUpload() throws IOException {
		if (schedulerService == null) {
			schedulerService = ApplicationContextProvider.getContext().getBean(SchedulerService.class);
		}

		if (audit == null) {
			audit = ApplicationContextProvider.getContext().getBean(AuditService.class);
		}
		FileWriter logFileWriter = null;
		PrintWriter logPrintWriter = null;
		List<String> message = new ArrayList<>();
		try {
			List<String> adFiles = new ArrayList<>();
			List<String> ctFiles = new ArrayList<>();
			List<String> pcFiles = new ArrayList<>();
			List<String> smFiles = new ArrayList<>();
			List<String> vbFiles = new ArrayList<>();
			List<String> vcFiles = new ArrayList<>();
			File folder = new File(this.fileUploadParam.getUploadFolder());
			File[] listOfFiles = folder.listFiles();
			totalFiles = listOfFiles.length;
			for (int i = 0; i < listOfFiles.length; i++) {
				if (listOfFiles[i].isFile()) {
					String filenames = listOfFiles[i].getName();
					String extractedFileType = filenames.substring(5, 7);
					String extractedFileDate = filenames.substring(7, 13);
					LOGGER.info(filenames);
					int extractedfiledate = Integer.parseInt(extractedFileDate);
					DateFormat dateFormat = new SimpleDateFormat("yyMMdd");
					String currentdate = dateFormat.format(new Date());
					int currentdateint = Integer.parseInt(currentdate);
					if (extractedfiledate <= currentdateint) {
						switch (extractedFileType) {
						case "AD":
							adFiles.add(filenames);
							break;
						case "VC":
							vcFiles.add(filenames);
							break;
						case "CT":
							ctFiles.add(filenames);
							break;
						case "PC":
							pcFiles.add(filenames);
							break;
						case "SM":
							smFiles.add(filenames);
							break;
						case "VB":
							vbFiles.add(filenames);
							break;
						default:
							break;
						}
					}
				}

			}
			Map<String, List<String>> items = new HashMap<>();
			Collections.sort(adFiles);
			items.put("AD", adFiles);
			Collections.sort(vcFiles);
			items.put("VC", vcFiles);
			Collections.sort(ctFiles);
			items.put("CT", ctFiles);
			Collections.sort(pcFiles);
			items.put("PC", pcFiles);
			Collections.sort(smFiles);
			items.put("SM", smFiles);
			Collections.sort(vbFiles);
			items.put("VB", vbFiles);
			String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
			String logTextFile = this.fileUploadParam.getLogFolder() + pathSeperator + "log_" + timeStamp + ".txt";
			boolean islogFileCreated = true;
			try {
				logFileWriter = new FileWriter(logTextFile, true);
				logPrintWriter = new PrintWriter(logFileWriter);
			} catch (IOException e) {
				Scheduler scheduler = schedulerService.getSchedulerByJobname(this.fileUploadParam.getJobName());
				scheduler.setStatus(FAILED);
				schedulerService.createScheduler(scheduler);
				islogFileCreated = false;
				LOGGER.error(e);
			}
			uploadFiles(items, message, islogFileCreated, logPrintWriter);
			if (islogFileCreated) {
				sendMail(logTextFile);
				logFileWriter.close();
			}
		} catch (Exception ex) {
			LOGGER.error(ex);
		} finally {
			try {
				if (logPrintWriter != null) {
					logPrintWriter.close();
				}
			} catch (Exception e) {
				LOGGER.error(e);
			}
			try {
				if (logFileWriter != null) {
					logFileWriter.close();
				}
			} catch (IOException e) {
				LOGGER.error(e);
			}
		}
		return message;
	}


	private String getMessageHeader(String message) {
		String header = message;
		if (message.contains("Failed rows are")) {
			header = message.substring(0, message.indexOf(FAILED));
		}
		return header;
	}

	private void sendMail(String logTextFile) {
		if (emailService == null) {
			emailService = ApplicationContextProvider.getContext().getBean(EmailService.class);
		}
		if (usersService == null) {
			usersService = ApplicationContextProvider.getContext().getBean(UsersService.class);
		}
		try {
			User admin = usersService.getUsersByUsername(this.fileUploadParam.getUsername());
			Map<String, String> context = new HashMap<>();
			context.put("user_name", this.fileUploadParam.getUsername());
			context.put("run_date", "" + new Date());
			context.put("upload_status", "scheduled");
			context.put("log_path", this.fileUploadParam.getLogFolder());
			context.put("class_name", "PoEmailGenerator");
			context.put("system_name", "NTPOMS");
			context.put("proprietor", "parkson.com");
			context.put("total_upload", "" + totalFiles);
			context.put("total_success", "" + totalFilesSuccess);
			context.put("total_fail", "" + totalFilesFailed);
			Map<String, List> contextlist = new HashMap<>();
			contextlist.put("failFileList", failFileList);
			Set<String> cc = new HashSet<>();
			Set<String> attachment = new HashSet<String>();
			attachment.add(logTextFile);
			EmailCriteria email = new EmailCriteria();
			email.setRecipient(admin.getEmail());
			email.setSubject("Master File Upload Notification.");
			email.setCc(cc);
			email.setAttachment(attachment);
			Properties props = EmailServiceUtils.loadEmailPropertyFile();
			email.setTemplateName(props.getProperty("mail.vm.template.masterfileupload"));
			email.setEmailUser(props.getProperty("mail.user"));
			email.setEmailPwd(props.getProperty("mail.passwd"));
			Session session = Session.getInstance(props, new Authenticator() {
				@Override
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(email.getEmailUser(), email.getEmailPwd());
				}
			});
			emailService.sendEmail(session, context, contextlist, email);
		} catch (MessagingException | IOException e) {
			LOGGER.error(e);
		}
	}

	private void uploadFiles(Map<String, List<String>> items, List<String> message, boolean islogFileCreated,
			PrintWriter logPrintWriter) throws IOException {
		if (!items.isEmpty()) {
			for (Entry<String, List<String>> pairItem : items.entrySet()) {
				getMasterObject(pairItem.getKey());
				if (pairItem.getValue() != null) {
					for (String currentFileName : pairItem.getValue()) {
						String result = fileTypeMaster.process(currentFileName);
						message.add(getMessageHeader(result));
						if (islogFileCreated) {
							logPrintWriter.println(result);
							uploadLogFiles(currentFileName, result);
						}
					}
				}
			}
		}
		logPrintWriter.close();
	}

	private void uploadLogFiles(String currentFileName, String result) throws IOException {
		Path sourcePath = Paths.get(this.fileUploadParam.getUploadFolder() + pathSeperator + currentFileName);
		Path targetPath = Paths.get(this.fileUploadParam.getSuccessFolder() + pathSeperator);
		FailUpload add = new FailUpload();
		if (result.contains(FAILED) || result.contains(PARTIAL)) {
			targetPath = Paths.get(this.fileUploadParam.getFailedFolder() + pathSeperator);
			totalFilesFailed += 1;
			add.setStatus(FAILED);
			add.setFilename(currentFileName);
			String failmsg = findMessage(result);
			add.setMessage(failmsg);
			failFileList.add(add);
		} else {
			add.setStatus("Successful");
			totalFilesSuccess += 1;
		}
		Files.move(sourcePath, targetPath.resolve(sourcePath.getFileName()), StandardCopyOption.REPLACE_EXISTING);
	}

	private String findMessage(String result) {

		StringBuilder msg = new StringBuilder();
		if (result.contains("database")) {
			msg.append("Database does not exists.");
			msg.append('\n');
		}
		if (result.contains("Company Code")) {
			msg.append("Company Codes does not exists.");
			msg.append('\n');
		}
		if (result.contains("Data Validation")) {
			msg.append("Failed validating columns.");
			msg.append('\n');
		}
		else {
			String[] parts = result.split(":");
			String error = parts[1];
			return error.toString();
		}
		
		return msg.toString();
	}
	
	private void getMasterObject(String extractedFileType) {
		if ("AD".equals(extractedFileType)) {
			fileTypeMaster = new DepartmentMaster(this.fileUploadParam);
		} else if ("VC".equals(extractedFileType)) {
			fileTypeMaster = new VendorCreditTermsMaster(this.fileUploadParam);
		} else if ("CT".equals(extractedFileType)) {
			fileTypeMaster = new CreditTermsMaster(this.fileUploadParam);
		} else if ("PC".equals(extractedFileType)) {
			fileTypeMaster = new ProfitCenterMaster(this.fileUploadParam);
		} else if ("SM".equals(extractedFileType)) {
			fileTypeMaster = new StoreMaster(this.fileUploadParam);
		} else if ("VB".equals(extractedFileType)) {
			fileTypeMaster = new VendorBranchMaster(this.fileUploadParam);
		}
	}
}