/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.poadmin.web.controllers;

import static java.util.Objects.nonNull;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.ModelAttribute;

import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.services.MessageService;
import my.com.parkson.ntpoms.poadmin.security.AuthenticatedUser;

/**
 * The Class AbstractAdminController.
 */
public abstract class AbstractAdminController {

	private static Logger LOGGER = Logger.getLogger(AbstractAdminController.class);

	@Autowired
	protected MessageService messageService;

	public static AuthenticatedUser getCurrentUser() {
		try {
			if (SecurityContextHolder.getContext().getAuthentication() != null) {
				Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
				if (principal instanceof AuthenticatedUser) {
					return (AuthenticatedUser) principal;
				}
			}
		} catch (POException e) {
			LOGGER.error("getCurrentUser() Error, {}", e);
			return null;
		}
		return null;
	}

	public static boolean isLoggedIn() {
		return nonNull(getCurrentUser());
	}

	/**
	 * Authenticated user.
	 *
	 * @param authenticatedUser
	 *            the authenticated user
	 * @return the authenticated user
	 */
	@ModelAttribute("authenticatedUser")
	public AuthenticatedUser authenticatedUser(@AuthenticationPrincipal AuthenticatedUser authenticatedUser) {
		return authenticatedUser;
	}

	/**
	 * Gets the message.
	 *
	 * @param code
	 *            the code
	 * @return the message
	 */
	public String getMessage(String code) {
		return messageService.getMessage(code);
	}

	/**
	 * Gets the message.
	 *
	 * @param code
	 *            the code
	 * @param defaultMsg
	 *            the default msg
	 * @return the message
	 */
	public String getMessage(String code, String defaultMsg) {
		return messageService.getMessage(code, defaultMsg);
	}

	protected abstract String getHeaderTitle();
}
