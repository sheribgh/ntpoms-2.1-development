package my.com.parkson.ntpoms.poadmin.web.scheduler;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.LogMessages;
import my.com.parkson.ntpoms.common.entities.Department;
import my.com.parkson.ntpoms.common.services.DepartmentService;
import my.com.parkson.ntpoms.poadmin.web.models.FileUploadParam;
import my.com.parkson.ntpoms.poadmin.web.utils.SchedulerUtils;
import my.com.parkson.ntpoms.utils.ApplicationContextProvider;

public class DepartmentMaster extends AbstractFileTypeMaster {
	private static Logger LOGGER = Logger.getLogger(BulkUpload.class);

	private String os = "Windows";
	private String pathSeperator = "\\";
	private String fileNameToBeProceed = null;
	private FileUploadParam fileUploadParam;

	@Autowired
	private DepartmentService departmentService;

	public DepartmentMaster(FileUploadParam fileUploadParam) {
		this.os = System.getProperty("os.name");
		if (this.os.contains("Linux")) {
			this.pathSeperator = "/";
		}
		this.fileUploadParam = fileUploadParam;
	}

	public String process(String filename) {
		if (departmentService == null) {
			departmentService = ApplicationContextProvider.getContext().getBean(DepartmentService.class);
		}
		if (StringUtils.isNotEmpty(filename)) {
			this.currentFileName = filename;
			this.fileNameToBeProceed = this.fileUploadParam.getUploadFolder() + this.pathSeperator + filename;
		}
		String resultMessage = "";
		boolean databaseExists = SchedulerUtils.checkDatabaseConnection(this.fileUploadParam.getDataSource(),
				this.fileUploadParam.getJobName());
		if (!databaseExists) {
			resultMessage = RESULT_MESSAGE_PREFIX + currentFileName
					+ "] Failed to be uploaded because database does not exist.";
			return resultMessage;
		}
		BufferedReader bufferreader = null;
		try {
			bufferreader = new BufferedReader(new FileReader(this.fileNameToBeProceed));
		} catch (FileNotFoundException e) {
			LOGGER.error("ERROR : FileNotFoundException : " + e.getMessage());
		}
		if (bufferreader != null) {
			try {
				int row = 0;
				String line = "";
				String csvSplitBy = "|";
				int dataRowCounter = 0;
				int tableColumnCount = 0;
				boolean validationSuccess = true;
				ArrayList<Integer> failedRows = new ArrayList<>();
				int dataColumnCount = 0;
				String dataSplitBy = "\\s*\\|\\s*";
				String[] dataItem = new String[0];
				addMessage = new StringBuilder();
				int lineCount = getLintCount(bufferreader);
				bufferreader.close();
				bufferreader = new BufferedReader(new FileReader(this.fileNameToBeProceed));
				while ((line = bufferreader.readLine()) != null) {
					if ((row == 0)) {
						validationSuccess = checkFileHeader(line, csvSplitBy, validationSuccess);
					}
					if (row == 1) {
						if (CSVTools.isTableHeader(line, csvSplitBy)) {
							tableColumnCount = CSVTools.getColumnCount(line, csvSplitBy);
						} else {
							validationSuccess = false;
							addMessage.append("Header does not exist.");
							addMessage.append('\n');
						}
					}
					if (row > 1 && row < (lineCount - 1)) {
						if (CSVTools.isTableData(line, csvSplitBy)) {
							dataRowCounter++;
						} else {
							validationSuccess = false;
							addMessage.append("Row count does not match number of rows.");
							addMessage.append('\n');
						}
					}
					if ((row == lineCount - 1)) {
						validationSuccess = checkRowCount(line, csvSplitBy, dataRowCounter, validationSuccess);
					}
					row++;
				}
				bufferreader.close();
				if (validationSuccess) {
					dataColumnCount = 0;
					try {
						bufferreader = new BufferedReader(new FileReader(this.fileNameToBeProceed));
					} catch (FileNotFoundException e) {
						LOGGER.error("ERROR : FileNotFoundException : " + e.getMessage());
					}
					row = 1;
					dataItem = new String[0];
					while ((line = bufferreader.readLine()) != null) {
						if (row > 2 && row < lineCount) {
							validationSuccess = true;
							dataColumnCount = CSVTools.getColumnCount(line, csvSplitBy);
							dataItem = line.split(dataSplitBy);
							if (dataColumnCount == tableColumnCount) {
								validationSuccess = validateDataItems(dataRowCounter, validationSuccess, dataItem, null,
										null);
								if (validationSuccess) {
									if (!upsert(dataItem)) {
										failedRows.add(row);
									}
								} else {
									failedRows.add(row);
								}
							} else {
								failedRows.add(row);
							}
						}
						row++;
					}
					resultMessage = getFailedRowMessages(dataRowCounter, failedRows);
				} else {
					resultMessage = RESULT_MESSAGE_PREFIX + currentFileName
							+ "] Failed to be uploaded because of wrong format suspect, captured message : "
							+ addMessage.toString();
				}
				bufferreader.close();
				return resultMessage;
			} catch (IOException e) {
				LOGGER.error("ERROR : FileNotFoundException : " + e.getMessage());
				return "error happened processing file " + currentFileName + ".";
			} catch (Exception e) {
				LOGGER.error("ERROR : Exception : The Master File [" + currentFileName + "] upload failed : "
						+ e.getMessage());
				return "error occured while processing file [" + currentFileName + "].";
			} finally {
				if (bufferreader != null) {
					try {
						bufferreader.close();
					} catch (IOException e) {
						LOGGER.error("ERROR : IOException : " + e.getMessage());
					}
				}
			}
		} else {
			resultMessage = RESULT_MESSAGE_PREFIX + currentFileName + "] can not be found.";
			return resultMessage;
		}
	}

	@Override
	protected boolean validateDataItems(int row, boolean validationSuccess, String[] dataItem, String storeCode,
			String depCode) {
		if (dataItem[1].isEmpty()) {
			validationSuccess = false;
			addMessage.append(ROW_MESSSAGE_PREFIX).append((row - 2))
					.append("] : Validation failed Department Code is Empty.").append('\n');
		}
		if (dataItem[1].equals("null")) {
			validationSuccess = false;
			addMessage.append(ROW_MESSSAGE_PREFIX).append((row - 2))
					.append("] : Validation failed Department Code is NUll.").append('\n');
		}
		if (dataItem[1].length() > 3) {
			validationSuccess = false;
			addMessage.append(ROW_MESSSAGE_PREFIX).append((row - 2))
					.append("] : Validation failed Department Code is more than 3 char.").append('\n');
		}
		if (!dataItem[1].matches("^[a-zA-Z0-9]*$+")) {
			validationSuccess = false;
			addMessage.append(ROW_MESSSAGE_PREFIX).append((row - 2)).append("] : Validation failed. Format invalid.")
					.append('\n');
		}
		if (dataItem[2].length() > 30) {
			validationSuccess = false;
			addMessage.append(ROW_MESSSAGE_PREFIX).append((row - 2))
					.append("] : Validation failed Department Name is more than 30 char.").append('\n');
		}
		if (dataItem[3].length() > 15) {
			validationSuccess = false;
			addMessage.append(ROW_MESSSAGE_PREFIX).append((row - 2))
					.append("] : Validation failed Department Abbr Name is more than 15 char.").append('\n');
		}
		if (dataItem[4].isEmpty()) {
			validationSuccess = false;
			addMessage.append(ROW_MESSSAGE_PREFIX).append((row - 2)).append("] : Validation failed isActive is empty.")
					.append('\n');
		}
		if (dataItem[4].equals("null")) {
			validationSuccess = false;
			addMessage.append(ROW_MESSSAGE_PREFIX).append((row - 2)).append("] : Validation failed isActive is null.")
					.append('\n');
		}
		if (!dataItem[4].equals("TRUE") && !dataItem[4].equals("FALSE")) {
			validationSuccess = false;
			addMessage.append(ROW_MESSSAGE_PREFIX).append((row - 2))
					.append("] : Validation failed isActive is not boolean.").append('\n');
		}
		return validationSuccess;
	}

	public boolean upsert(String[] dataItem) {
		boolean flag = true;
		String deptCode = dataItem[1];
		String deptName = dataItem[2];
		String deptAbbrName = dataItem[3];
		boolean isActive = Boolean.parseBoolean(dataItem[4]);
		Department department = null;
		try {
			department = departmentService.getDepartmentByCode(deptCode);
			if (department != null) {
				department.setCode(deptCode);
				department.setName(deptName);
				department.setAbbrName(deptAbbrName);
				department.setActive(isActive);
				department.setLastModifiedOn(new Date());
				department.setLastModifiedBy(this.fileUploadParam.getUsername());
			} else {
				department = new Department();
				department.setCode(deptCode);
				department.setName(deptName);
				department.setAbbrName(deptAbbrName);
				department.setActive(isActive);
				department.setCreatedOn(new Date());
				department.setCreatedBy(this.fileUploadParam.getUsername());
				department.setLastModifiedOn(new Date());
				department.setLastModifiedBy(this.fileUploadParam.getUsername());
			}
			departmentService.createDepartment(department);
		} catch (POException e) {
			flag = false;
			addMessage.append("Data insertion problem.");
			addMessage.append('\n');
			LOGGER.error(e);
		}
		return flag;
	}
}