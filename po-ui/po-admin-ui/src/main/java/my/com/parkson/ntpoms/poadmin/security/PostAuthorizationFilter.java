/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.poadmin.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

/**
 * The Class PostAuthorizationFilter.
 */
@Component
public class PostAuthorizationFilter extends OncePerRequestFilter {

	protected String[] ignoreURIs = { "/assets/" };

	/* (non-Javadoc)
	 * @see org.springframework.web.filter.OncePerRequestFilter#doFilterInternal(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, javax.servlet.FilterChain)
	 */
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws ServletException, IOException {
		String uri = request.getRequestURI();
		if (!isIgnorableURI(uri)) {
			String menu = MenuConfiguration.getMatchingMenu(uri);
			request.setAttribute("CURRENT_MENU", menu);
		}
		chain.doFilter(request, response);
	}

	/**
	 * Checks if is ignorable URI.
	 *
	 * @param uri the uri
	 * @return true, if is ignorable URI
	 */
	private boolean isIgnorableURI(String uri) {
		for (String u : ignoreURIs) {
			if (uri.startsWith(u)) {
				return true;
			}
		}
		return false;
	}
}
