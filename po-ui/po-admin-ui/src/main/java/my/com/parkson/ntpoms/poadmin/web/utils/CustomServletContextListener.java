package my.com.parkson.ntpoms.poadmin.web.utils;

import java.util.List;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import my.com.parkson.ntpoms.common.entities.Scheduler;
import my.com.parkson.ntpoms.common.services.SchedulerService;

@WebListener
public class CustomServletContextListener extends SpringBeanAutowiringSupport implements ServletContextListener {

	@Autowired
	private SchedulerService schedulerservice;

	@Override
	public void contextInitialized(ServletContextEvent e) {
		if (schedulerservice != null) {
			List<Scheduler> temp = schedulerservice.getAllScheduler();
			if (temp != null) {
				for (Scheduler s : temp) {
					if ("PasswordExpiry".equals(s.getJobname())) {
						QuartzSchedulerUtils.startSchedulerPasswordExpiry(s.getCreatedBy(), s.getJobname(), s);
					} else if ("MasterTableUpload".equals(s.getJobname())) {
						QuartzSchedulerUtils.startSchedulerBulkUpload(s.getCreatedBy(), s.getJobname(), s);
					}
				}
			}
		}
	}

	@Override
	public void contextDestroyed(ServletContextEvent e) {
		if (schedulerservice != null) {
			List<Scheduler> temp = schedulerservice.getAllScheduler();
			if (temp != null) {
				for (Scheduler s : temp) {
					QuartzSchedulerUtils.deleteScheduler(s);
				}
			}
		}
	}

}