package my.com.parkson.ntpoms.poadmin.web.scheduler;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.LogMessages;
import my.com.parkson.ntpoms.common.services.CompanyService;
import my.com.parkson.ntpoms.main.entities.VendorCreditTerms;
import my.com.parkson.ntpoms.main.entities.VendorCreditTermsId;
import my.com.parkson.ntpoms.main.services.VendorCreditTermsService;
import my.com.parkson.ntpoms.poadmin.web.models.FileUploadParam;
import my.com.parkson.ntpoms.poadmin.web.utils.SchedulerUtils;
import my.com.parkson.ntpoms.utils.ApplicationContextProvider;

public class VendorCreditTermsMaster extends AbstractFileTypeMaster {
	private static Logger logger = Logger.getLogger(VendorBranchMaster.class);

	private String companyCode;

	private String fileNameToBeProceed = null;

	private String os = "Windows";
	private String pathSeperator = "\\";
	private FileUploadParam fileUploadParam;

	@Autowired
	private VendorCreditTermsService vendorCreditTermsService;

	@Autowired
	private CompanyService companyService;

	public VendorCreditTermsMaster(FileUploadParam fileUploadParam) {
		this.os = System.getProperty("os.name");
		if (this.os.contains("Linux")) {
			this.pathSeperator = "/";
		}
		this.fileUploadParam = fileUploadParam;
	}

	public String process(String filename) {
		if (vendorCreditTermsService == null) {
			vendorCreditTermsService = ApplicationContextProvider.getContext().getBean(VendorCreditTermsService.class);
		}
		if (companyService == null) {
			companyService = ApplicationContextProvider.getContext().getBean(CompanyService.class);
		}
		if (StringUtils.isNotEmpty(filename)) {
			this.currentFileName = filename;
			this.fileNameToBeProceed = this.fileUploadParam.getUploadFolder() + this.pathSeperator + filename;
			this.companyCode = CSVTools.getCompanyCode(filename);
		}
		String resultMessage = "";
		boolean databaseExists = SchedulerUtils.checkDatabaseConnection(this.fileUploadParam.getDataSource(),
				this.fileUploadParam.getJobName());
		if (!databaseExists) {
			resultMessage = "The Master File [" + currentFileName
					+ "] Failed to be uploaded because database does not exist.";
			return resultMessage;
		}
		if (companyService.getCompanyByCode(companyCode) == null) {
			resultMessage = "The Master File [" + currentFileName + "] Failed. Company Code [" + companyCode
					+ "] does not exist.";
			return resultMessage;
		}
		BufferedReader bufferreader = null;
		try {
			bufferreader = new BufferedReader(new FileReader(this.fileNameToBeProceed));
		} catch (FileNotFoundException e) {
			logger.error("ERROR : FileNotFoundException : " + e.getMessage());
		}
		if (bufferreader != null) {
			try {
				int row = 0;
				String line = "";
				String csvSplitBy = "|";
				int dataRowCounter = 0;
				int tableColumnCount = 0;
				boolean validationSuccess = true;
				ArrayList<Integer> failedRows = new ArrayList<>();
				int dataColumnCount = 0;
				String dataSplitBy = "\\s*\\|\\s*";
				String[] dataItem = new String[0];
				addMessage = new StringBuilder();
				int lineCount = getLintCount(bufferreader);
				bufferreader.close();
				bufferreader = new BufferedReader(new FileReader(this.fileNameToBeProceed));
				while ((line = bufferreader.readLine()) != null) {
					if ((row == 0)) {
						validationSuccess = checkFileHeader(line, csvSplitBy, validationSuccess);
					}
					if (row == 1) {
						if (CSVTools.isTableHeader(line, csvSplitBy)) {
							tableColumnCount = CSVTools.getColumnCount(line, csvSplitBy);
						} else {
							validationSuccess = false;
							addMessage.append("Header does not exist.");
							addMessage.append('\n');
						}
					}
					if (row > 1 && row < (lineCount - 1)) {
						if (CSVTools.isTableData(line, csvSplitBy)) {
							dataRowCounter++;
						} else {
							validationSuccess = false;
							addMessage.append("Row count does not match number of rows.");
							addMessage.append('\n');
						}
					}
					if ((row == lineCount - 1)) {
						validationSuccess = checkRowCount(line, csvSplitBy, dataRowCounter, validationSuccess);
					}
					row++;
				}
				bufferreader.close();
				if (validationSuccess) {
					dataColumnCount = 0;
					try {
						bufferreader = new BufferedReader(new FileReader(this.fileNameToBeProceed));
					} catch (FileNotFoundException e) {
						logger.error("ERROR : FileNotFoundException : " + e.getMessage());
					}
					row = 1;
					dataItem = new String[0];
					while ((line = bufferreader.readLine()) != null) {
						if (row > 2 && row < lineCount) {
							validationSuccess = true;
							dataColumnCount = CSVTools.getColumnCount(line, csvSplitBy);
							dataItem = line.split(dataSplitBy);
							if (dataColumnCount == tableColumnCount) {
								validationSuccess = validateDataItems(dataRowCounter, validationSuccess, dataItem, null,
										null);
								if (validationSuccess) {
									if (!upsert(dataItem)) {
										failedRows.add(row);
									}
								} else {
									failedRows.add(row);
								}
							} else {
								failedRows.add(row);
							}
						}
						row++;
					}
					resultMessage = getFailedRowMessages(dataRowCounter, failedRows);
				} else {
					resultMessage = RESULT_MESSAGE_PREFIX + currentFileName
							+ "] Failed to be uploaded because of wrong format suspect, captured message : "
							+ addMessage.toString();
					bufferreader.close();
				}
				bufferreader.close();
				return resultMessage;
			} catch (IOException e) {
				logger.error("ERROR :  FileNotFoundException : " + e.getMessage());
				return "error happened processing file " + currentFileName + ".";
			} catch (Exception e) {
				logger.error("ERROR :  Exception : The Master File [" + currentFileName + "] upload failed : "
						+ e.getMessage());
				return "error occured while processing file [" + currentFileName + "].";
			} finally {
				if (bufferreader != null) {
					try {
						bufferreader.close();
					} catch (IOException e) {
						logger.error("ERROR :  IOException : " + e.getMessage());
					}
				}
			}
		} else {
			resultMessage = RESULT_MESSAGE_PREFIX + currentFileName + "] can not be found.";
			return resultMessage;
		}
	}

	@Override
	protected boolean validateDataItems(int row, boolean validationSuccess, String[] dataItem, String storeCode,
			String depCode) {
		if (dataItem[1].length() > 6) {
			validationSuccess = false;
			addMessage.append(ROW_MESSSAGE_PREFIX).append((row - 2))
					.append("] : Validation failed Vendor Code is Empty.").append('\n');
		}
		if (!dataItem[1].matches("[0-9]+")) {
			validationSuccess = false;
			addMessage.append(ROW_MESSSAGE_PREFIX).append((row - 2))
					.append("] : Validation failed Vendor Code invalid format.").append('\n');
		}
		if (dataItem[2].length() > 2) {
			validationSuccess = false;
			addMessage.append(ROW_MESSSAGE_PREFIX).append((row - 2))
					.append("] : Validation failed Credit terms Code is more than 2 chars.").append('\n');
		}
		if (!dataItem[3].equals("TRUE") && !dataItem[3].equals("FALSE")) {
			validationSuccess = false;
			addMessage.append(ROW_MESSSAGE_PREFIX).append((row - 2))
					.append("] : Validation failed. IsActive is not boolean.").append('\n');
		}
		return validationSuccess;
	}

	public boolean upsert(String[] dataItem) {
		boolean flag = true;
		String vendorCode = dataItem[1];
		String vendorBranchCode = dataItem[1];
		String creditTermsCode = dataItem[2];
		boolean isActive = Boolean.parseBoolean(dataItem[3]);
		try {
			VendorCreditTerms vendorCreditTerms = null;
			VendorCreditTermsId vendorCreditTermsId = new VendorCreditTermsId(vendorCode, vendorBranchCode,
					creditTermsCode, this.companyCode);
			vendorCreditTerms = vendorCreditTermsService.getVendorCreditTermsById(vendorCreditTermsId);
			if (vendorCreditTerms != null) {
				vendorCreditTerms.setVendcrtId(vendorCreditTermsId);
				vendorCreditTerms.setActive(isActive);
				vendorCreditTerms.setLastModifiedOn(new Date());
				vendorCreditTerms.setLastmodifiedby(this.fileUploadParam.getUsername());
			} else {
				vendorCreditTerms = new VendorCreditTerms();
				vendorCreditTerms.setVendcrtId(vendorCreditTermsId);
				vendorCreditTerms.setActive(isActive);
				vendorCreditTerms.setCreatedon(new Date());
				vendorCreditTerms.setCreatedby(this.fileUploadParam.getUsername());
				vendorCreditTerms.setLastModifiedOn(new Date());
				vendorCreditTerms.setLastmodifiedby(this.fileUploadParam.getUsername());
			}
			vendorCreditTermsService.createVendorCreditTerms(vendorCreditTerms);
		} catch (Exception e) {
			flag = false;
			addMessage.append("Data insertion problem.");
			addMessage.append('\n');
			logger.error("Failed to save record into the database.", e);
		}
		return flag;
	}
}