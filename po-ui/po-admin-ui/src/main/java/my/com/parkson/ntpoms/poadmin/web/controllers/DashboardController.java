/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.poadmin.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.common.entities.UserRole;
import my.com.parkson.ntpoms.common.services.AccessLogService;
import my.com.parkson.ntpoms.common.services.AuditService;
import my.com.parkson.ntpoms.common.services.CompanyService;
import my.com.parkson.ntpoms.common.services.DepartmentService;
import my.com.parkson.ntpoms.common.services.EmployeeMasterService;
import my.com.parkson.ntpoms.common.services.GoodsServicesTaxService;
import my.com.parkson.ntpoms.common.services.JobService;
import my.com.parkson.ntpoms.common.services.MasterDataService;
import my.com.parkson.ntpoms.common.services.OrderCategoryService;
import my.com.parkson.ntpoms.common.services.OrderSubCategoryService;
import my.com.parkson.ntpoms.common.services.ProfitCenterService;
import my.com.parkson.ntpoms.common.services.PurchaseOrderUserService;
import my.com.parkson.ntpoms.common.services.PurchaseRequisitionService;
import my.com.parkson.ntpoms.common.services.SchedulerService;
import my.com.parkson.ntpoms.common.services.StoreService;
import my.com.parkson.ntpoms.common.services.SystemParameterService;
import my.com.parkson.ntpoms.common.services.UsersService;

@Slf4j
@Controller
@RequiredArgsConstructor
public class DashboardController extends AbstractAdminController {
	
	@Autowired
	private UsersService adminrepo;
	
	@Autowired
	private PurchaseOrderUserService pouser;

	@Autowired
	private EmployeeMasterService emprepo;

	@Autowired
	private SystemParameterService sysRepo;
	
	@Autowired
	private StoreService storerepo;
	
	@Autowired
	private SchedulerService schedulerService;
	
	
	@Autowired
	private PurchaseRequisitionService purrepo;
	
	@Autowired
	private ProfitCenterService pcrepo;
	
	@Autowired
	private OrderCategoryService orderrepo;
	
	@Autowired
	private OrderSubCategoryService ordersubrepo;
	
	@Autowired
	private GoodsServicesTaxService gstrepo;
	
	@Autowired
	private DepartmentService deptrepo;
	
	@Autowired
	private JobService jobmasrepo;
	
	@Autowired
	private CompanyService comrepo;
	
	@Autowired
	private AccessLogService log;
	
	
	@Autowired
	private AuditService auditService;
	
	@Autowired
	private MasterDataService masterrepo;
	
	
	
	/**
	 * On page load GST.
	 *
	 * @param pageSize the page size
	 * @param page the page
	 * @param code the code
	 * @param abbrDesc the abbr desc
	 * @param isActive the is active
	 * @return the model and view
	 */
	
	
	@GetMapping(value = "/master/dashboard")
	public String onPageLoadDashboard(Model model) {
		
		long totalAdminUsers = adminrepo.getCount()-1;
		
		long totalActiveAdminUsers =adminrepo.getCountActive()-1;
		long totalInactiveAdminUsers = totalAdminUsers - totalActiveAdminUsers ;
		
		long totalPoUsers = pouser.getCount();
		long totalActivePoUsers = pouser.getCountActive();
		long totalInActivePoUsers = totalPoUsers - totalActivePoUsers;
		
		long totalLogs = log.getCount();
		
		long totalAudit = auditService.getCount();
		
		long totalcompany = comrepo.getCount();
		long totalactivecompany = comrepo.getCountActive();
		long totalinactivecompany = totalcompany - totalactivecompany ;
		
		long totalstore = storerepo.getCount();
		long totalactivestore = storerepo.getAllActiveStore().size();
		long totalinactivestore = totalstore - totalactivestore;
		
		long totaldept = deptrepo.getCount();
		long totalactivedept = deptrepo.getAllActiveDepartment().size();
		long totalinactivedept = totaldept - totalactivedept;
		
		long totalpc = pcrepo.getCount();
		long totalactivepc = pcrepo.getAllActiveProfitCenter().size();
		long totalinactivepc = totalpc - totalactivepc;
		
		long totalgst = gstrepo.getCount();
		long totalactivegst = gstrepo.getAllActiveGoodsServicesTax().size();
		long totalinactivegst = totalgst - totalactivegst;
		
		long totalpr = purrepo.getCount();
		long totalactive = purrepo.getAllActivePurchaseRequisition().size();
		long totalinactivepr = totalpr - totalactive;
		
		long totalordercat = orderrepo.getCount();
		long totalactiveordercat = orderrepo.getAllActiveOrderCategory().size();
		long totalinactiveordercat = totalordercat - totalactiveordercat;
		
		long totalordersubcat = ordersubrepo.getCount();
		long totalactiveordersubcat = ordersubrepo.getAllActiveOrderSubCategory().size();
		long totalinactiveordersubcat = totalordersubcat - totalactiveordersubcat;
		
		long totalsched = schedulerService.getCount();
	
		long totalemp = emprepo.getCount();
		long totalempActive = emprepo.getAllActiveEmployeeMaster().size();
		long totalempInActive = totalemp - totalempActive;	
		
		long totaljobmast = jobmasrepo.getCount();
		long totalactivejobmast = jobmasrepo.getAllActiveJob().size();
		long totalinactivejobmast = totaljobmast - totalactivejobmast;
		
		long totalmastdata = masterrepo.getCount();
		long totalactivemastdata = masterrepo.getAllActiveMasterData().size();
		long totalinactivemastdata = totalmastdata - totalactivemastdata ;
		
		long totalsysparam = sysRepo.getCount();
		long totalactivesysparam =sysRepo.getAllActiveSystemParameter().size();
		long totalinactivesysparam = totalsysparam - totalactivesysparam ;
		
		if(getCurrentUser()!=null){
			UserRole userRole = adminrepo.getUsersByUser(getCurrentUser().getUsername());
			if (userRole != null) {
				model.addAttribute("roleId", userRole.getRoleId());
			}
			
		}

		model.addAttribute("totalAdminUsers",totalAdminUsers);
		model.addAttribute("totalInactiveAdminUsers",totalInactiveAdminUsers);
		model.addAttribute("totalActiveAdminUsers",totalActiveAdminUsers);
		
		model.addAttribute("totalPoUsers",totalPoUsers);
		model.addAttribute("totalActivePoUsers",totalActivePoUsers);
		model.addAttribute("totalInActivePoUsers",totalInActivePoUsers);
		
		model.addAttribute("totalLogs",totalLogs);
		
		model.addAttribute("totalAudit",totalAudit);
		
		model.addAttribute("totalcompany",totalcompany);
		model.addAttribute("totalactivecompany",totalactivecompany);
		model.addAttribute("totalinactivecompany",totalinactivecompany);
		
		model.addAttribute("totalstore",totalstore);
		model.addAttribute("totalactivestore",totalactivestore);
		model.addAttribute("totalinactivestore",totalinactivestore);
		
		model.addAttribute("totaldept",totaldept);
		model.addAttribute("totalactivedept",totalactivedept);
		model.addAttribute("totalinactivedept",totalinactivedept);
		
		model.addAttribute("totalpc",totalpc);
		model.addAttribute("totalactivepc",totalactivepc);
		model.addAttribute("totalinactivepc",totalinactivepc);
		
		model.addAttribute("totalgst",totalgst);
		model.addAttribute("totalactivegst",totalactivegst);
		model.addAttribute("totalinactivegst",totalinactivegst);
		
		model.addAttribute("totalpr",totalpr);
		model.addAttribute("totalactive",totalactive);
		model.addAttribute("totalinactivepr",totalinactivepr);
		
		model.addAttribute("totalordercat",totalordercat);
		model.addAttribute("totalactiveordercat",totalactiveordercat);
		model.addAttribute("totalinactiveordercat",totalinactiveordercat);
		
		model.addAttribute("totalordersubcat",totalordersubcat);
		model.addAttribute("totalactiveordersubcat",totalactiveordersubcat);
		model.addAttribute("totalinactiveordersubcat",totalinactiveordersubcat);
		
		model.addAttribute("totalsched",totalsched);
		
		model.addAttribute("totalemp",totalemp);
		model.addAttribute("totalempActive",totalempActive);
		model.addAttribute("totalempInActive",totalempInActive);
		
		model.addAttribute("totaljobmast",totaljobmast);
		model.addAttribute("totalactivejobmast",totalactivejobmast);
		model.addAttribute("totalinactivejobmast",totalinactivejobmast);
		
		model.addAttribute("totalmastdata",totalmastdata);
		model.addAttribute("totalactivemastdata",totalactivemastdata);
		model.addAttribute("totalinactivemastdata",totalinactivemastdata);
		
		model.addAttribute("totalsysparam",totalsysparam);
		model.addAttribute("totalactivesysparam",totalactivesysparam);
		model.addAttribute("totalinactivesysparam",totalinactivesysparam);
		
		
		return  "/master/dashboard";

	}

	@Override
	protected String getHeaderTitle() {
		return "Dashboard";
	}
	
}