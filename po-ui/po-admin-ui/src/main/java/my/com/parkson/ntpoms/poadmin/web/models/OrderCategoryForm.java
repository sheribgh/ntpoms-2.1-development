/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.poadmin.web.models;

import java.io.Serializable;

public class OrderCategoryForm implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String ordCatCode;

	private String ordCatDesc;

	private String ordCatAbbrDesc;

	private String ordCatAuthorizationGrid;
	
	private String authgrid_hidden;

	public String getAuthgrid_hidden() {
		return authgrid_hidden;
	}

	public void setAuthgrid_hidden(String authgrid_hidden) {
		this.authgrid_hidden = authgrid_hidden;
	}

	private String createdBy;

	private String createdOn;

	private String lastModifiedBy;

	private String lastModifiedOn;

	private String deactivatedBy;

	private String deactivatedOn;

	private String reactivatedBy;

	private String reactivatedOn;

	private String isActive;

	private String mf15MPrmast_mf15_prTypeCode;

	/**
	 * @return the ordCatCode
	 */
	public String getOrdCatCode() {
		return ordCatCode;
	}

	/**
	 * @param ordCatCode
	 *            the ordCatCode to set
	 */
	public void setOrdCatCode(String ordCatCode) {
		this.ordCatCode = ordCatCode;
	}

	/**
	 * @return the ordCatDesc
	 */
	public String getOrdCatDesc() {
		return ordCatDesc;
	}

	/**
	 * @param ordCatDesc
	 *            the ordCatDesc to set
	 */
	public void setOrdCatDesc(String ordCatDesc) {
		this.ordCatDesc = ordCatDesc;
	}

	/**
	 * @return the ordCatAbbrDesc
	 */
	public String getOrdCatAbbrDesc() {
		return ordCatAbbrDesc;
	}

	/**
	 * @param ordCatAbbrDesc
	 *            the ordCatAbbrDesc to set
	 */
	public void setOrdCatAbbrDesc(String ordCatAbbrDesc) {
		this.ordCatAbbrDesc = ordCatAbbrDesc;
	}

	/**
	 * @return the ordCatAuthorizationGrid
	 */
	public String getOrdCatAuthorizationGrid() {
		return ordCatAuthorizationGrid;
	}

	/**
	 * @param ordCatAuthorizationGrid
	 *            the ordCatAuthorizationGrid to set
	 */
	public void setOrdCatAuthorizationGrid(String ordCatAuthorizationGrid) {
		this.ordCatAuthorizationGrid = ordCatAuthorizationGrid;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdOn
	 */
	public String getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn
	 *            the createdOn to set
	 */
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the lastModifiedBy
	 */
	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	/**
	 * @param lastModifiedBy
	 *            the lastModifiedBy to set
	 */
	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	/**
	 * @return the lastModifiedOn
	 */
	public String getLastModifiedOn() {
		return lastModifiedOn;
	}

	/**
	 * @param lastModifiedOn
	 *            the lastModifiedOn to set
	 */
	public void setLastModifiedOn(String lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

	/**
	 * @return the deactivatedBy
	 */
	public String getDeactivatedBy() {
		return deactivatedBy;
	}

	/**
	 * @param deactivatedBy
	 *            the deactivatedBy to set
	 */
	public void setDeactivatedBy(String deactivatedBy) {
		this.deactivatedBy = deactivatedBy;
	}

	/**
	 * @return the deactivatedOn
	 */
	public String getDeactivatedOn() {
		return deactivatedOn;
	}

	/**
	 * @param deactivatedOn
	 *            the deactivatedOn to set
	 */
	public void setDeactivatedOn(String deactivatedOn) {
		this.deactivatedOn = deactivatedOn;
	}

	/**
	 * @return the reactivatedBy
	 */
	public String getReactivatedBy() {
		return reactivatedBy;
	}

	/**
	 * @param reactivatedBy
	 *            the reactivatedBy to set
	 */
	public void setReactivatedBy(String reactivatedBy) {
		this.reactivatedBy = reactivatedBy;
	}

	/**
	 * @return the reactivatedOn
	 */
	public String getReactivatedOn() {
		return reactivatedOn;
	}

	/**
	 * @param reactivatedOn
	 *            the reactivatedOn to set
	 */
	public void setReactivatedOn(String reactivatedOn) {
		this.reactivatedOn = reactivatedOn;
	}

	/**
	 * @return the isActive
	 */
	public String getIsActive() {
		return isActive;
	}

	/**
	 * @param isActive
	 *            the isActive to set
	 */
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	/**
	 * @return the mf15MPrmast_mf15_prTypeCode
	 */
	public String getMf15MPrmast_mf15_prTypeCode() {
		return mf15MPrmast_mf15_prTypeCode;
	}

	/**
	 * @param mf15mPrmast_mf15_prTypeCode
	 *            the mf15MPrmast_mf15_prTypeCode to set
	 */
	public void setMf15MPrmast_mf15_prTypeCode(String mf15mPrmast_mf15_prTypeCode) {
		mf15MPrmast_mf15_prTypeCode = mf15mPrmast_mf15_prTypeCode;
	}

}
