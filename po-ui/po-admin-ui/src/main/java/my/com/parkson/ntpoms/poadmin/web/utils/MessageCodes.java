/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.poadmin.web.utils;

/**
 * The Class MessageCodes.
 */
public final class MessageCodes {
	public static final String LABEL_PASSWRD_RESET_EMAIL_SUBJECT = "label.password_reset_email_subject";
	public static final String INFO_PASSWRD_RESET_LINK_SENT = "info.password_reset_link_sent";
	public static final String ERROR_INVALID_PASSWRD_RESET_REQUEST = "error.invalid_password_reset_request";
	public static final String ERROR_PASSWRD_CONF_PASSWRD_MISMATCH = "error.password_conf_password_mismatch";
	public static final String INFO_PASSWRD_UPDATED_SUCCESS = "info.password_updated_success";
	public static final String FAILED_SENDMAIL_EXCEPTION = "exception.sendmail.failed";
	public static final String ERROR_PASSWRD_ISEMPTY = "error.password_isempty";
	public static final String ERROR_CONFIRM_PASSWRD_ISEMPTY = "error.confirm_password_isempty";

	/**
	 * Instantiates a new message codes.
	 */
	private MessageCodes() {
		super();
	}
}
