/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.poadmin.web.controllers;

import static my.com.parkson.ntpoms.common.MessageCodes.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

import javax.validation.Valid;
import java.util.*;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.LogMessages;
import my.com.parkson.ntpoms.common.entities.GoodsServicesTax;
import my.com.parkson.ntpoms.common.entities.SystemParameter;
import my.com.parkson.ntpoms.common.repositories.GoodsServicesTaxRepository;
import my.com.parkson.ntpoms.common.services.AccessLogService;
import my.com.parkson.ntpoms.common.services.AuditService;
import my.com.parkson.ntpoms.common.services.GoodsServicesTaxService;
import my.com.parkson.ntpoms.common.services.MessageService;
import my.com.parkson.ntpoms.common.services.SystemParameterService;
import my.com.parkson.ntpoms.poadmin.security.SecurityUtil;
import my.com.parkson.ntpoms.poadmin.web.models.GSTForm;
import my.com.parkson.ntpoms.poadmin.web.models.Pager;

@Slf4j
@Controller
@Secured(SecurityUtil.MANAGE_GST)
@RequiredArgsConstructor
public class GSTController extends AbstractAdminController {

	private static final int BUTTONS_TO_SHOW = 5;
	private static final int INITIAL_PAGE = 0;
	private static final int INITIAL_PAGE_SIZE = 5;
	private static final int[] PAGE_SIZES = { 5, 10, 20 };
	private static final String DATE_FORMAT_KEY = "DATE_FORMAT";
	private static final String DATE_FORMAT_VALUE = "dd-MM-yyyy";
	private static final String REDIRECT_MASTER_GST = "redirect:/master/gst";
	boolean edit = false;

	private static Logger LOGGER = Logger.getLogger(GSTController.class);

	/** The messages. */
	@Autowired
	private MessageService messages;

	@Autowired
	private AuditService auditService;

	@Autowired
	MessageService messageService;

	@Autowired
	private GoodsServicesTaxService gstrepo;

	@Autowired
	private GoodsServicesTaxRepository gstrepository;

	@Autowired
	private SystemParameterService sysRepo;

	/**
	 * On page load GST.
	 *
	 * @param pageSize
	 *            the page size
	 * @param page
	 *            the page
	 * @param code
	 *            the code
	 * @param abbrDesc
	 *            the abbr desc
	 * @param isActive
	 *            the is active
	 * @return the model and view
	 */

	@GetMapping("/master/gst")
	public String onPageLoadGST(Model model) {
		String dateFormat = DATE_FORMAT_VALUE;
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName(DATE_FORMAT_KEY);
		if (dateObj != null && !dateObj.getPropValue().isEmpty()) {
			dateFormat = dateObj.getPropValue();
		}
		model.addAttribute("date_format", dateFormat);
		model.addAttribute("gstobj", new GSTForm());
		return "/master/gst";

	}

	@RequestMapping(value = "/gst/gstgrid", method = RequestMethod.GET)
	public String gstGrid(@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page, @RequestParam("code") Optional<String> code,
			@RequestParam("abbrDesc") Optional<String> abbrDesc,
			@RequestParam("effectiveStartDate") Optional<String> effectiveStartDate,
			@RequestParam("effectiveEndDate") Optional<String> effectiveEndDate,
			@RequestParam("status") Optional<String> isActive, @RequestParam("sortingkey") Optional<String> sortingkey,
			@RequestParam("sortingorder") Optional<String> sortingorder, Model model) {

		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
		String dateFormat = DATE_FORMAT_VALUE;
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName(DATE_FORMAT_KEY);
		if (dateObj != null && !dateObj.getPropValue().isEmpty()) {
			dateFormat = dateObj.getPropValue();
		}
		Specification<GoodsServicesTax> s = new Specification<GoodsServicesTax>() {
			@Override
			public Predicate toPredicate(Root<GoodsServicesTax> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> ls = new ArrayList<>();
				if (code.isPresent() && !code.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("code")), "%" + code.get().toLowerCase() + "%"));
				}
				if (abbrDesc.isPresent() && !abbrDesc.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("abbrDesc")), "%" + abbrDesc.get().toLowerCase() + "%"));
				}
				if (effectiveStartDate.isPresent() && !effectiveStartDate.get().isEmpty()) {
					Expression<String> dateStringExpr = cb.function("to_char", String.class,
							root.get("effectiveStartDate"), cb.literal("YYYY-MM-DD HH12:MI:SS"));
					ls.add(cb.like(cb.lower(dateStringExpr), "%" + effectiveStartDate.get().toLowerCase() + "%"));
				}
				if (effectiveEndDate.isPresent() && !effectiveEndDate.get().isEmpty()) {
					Expression<String> dateStringExpr = cb.function("to_char", String.class,
							root.get("effectiveEndDate"), cb.literal("YYYY-MM-DD HH12:MI:SS"));
					ls.add(cb.like(cb.lower(dateStringExpr), "%" + effectiveEndDate.get().toLowerCase() + "%"));
				}
				if (isActive.isPresent() && !isActive.get().isEmpty() && !isActive.get().equals("All")) {
					ls.add(cb.equal(root.get("isActive"), "Active".equals(isActive.get())));
				}
				return cb.and(ls.toArray(new Predicate[0]));
			}
		};
		Page<GoodsServicesTax> list;
		Sort sorttemp = new Sort(Sort.Direction.DESC, "lastModifiedOn");
		if (sortingkey.isPresent() && sortingorder.isPresent()) {
			sorttemp = new Sort(sortingorder.get().equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC,
					sortingkey.get());
		}
		if (code.isPresent() || abbrDesc.isPresent() || effectiveStartDate.isPresent() || effectiveEndDate.isPresent()
				|| isActive.isPresent()) {
			list = gstrepo.getAllPagableGoodsServicesTax(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		} else {
			list = gstrepo.getAllPagableGoodsServicesTaxOrdered(new PageRequest(evalPage, evalPageSize, sorttemp));
		}
		int serialid = 0;
		int pagesize = evalPage + 1;
		if (pagesize > 1) {
			int start = evalPageSize * pagesize;
			serialid = start - evalPageSize;
		}
		Pager pager = new Pager(list.getTotalPages(), list.getNumber(), BUTTONS_TO_SHOW);
		model.addAttribute("serialid", serialid);
		model.addAttribute("list", list);
		model.addAttribute("selectedPageSize", evalPageSize);
		model.addAttribute("pageSizes", PAGE_SIZES);
		model.addAttribute("pager", pager);
		model.addAttribute("totalRecords", list.getTotalElements());
		model.addAttribute("date_format", dateFormat);

		model.addAttribute("code", code.isPresent() ? code.get() : "");
		model.addAttribute("abbrDesc", abbrDesc.isPresent() ? abbrDesc.get() : "");
		model.addAttribute("effectiveStartDate", effectiveStartDate.isPresent() ? effectiveStartDate.get() : "");
		model.addAttribute("effectiveEndDate", effectiveEndDate.isPresent() ? effectiveEndDate.get() : "");
		model.addAttribute("status", isActive.isPresent() ? isActive.get() : "");
		model.addAttribute("sortingorder", sortingorder.isPresent() ? sortingorder.get() : "DESC");
		model.addAttribute("sortingkey", sortingkey.isPresent() ? sortingkey.get() : "lastModifiedOn");

		return "/master/gst :: result-table";
	}

	/**
	 * Creates the GST.
	 *
	 * @param obj
	 *            the obj
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @param redirectAttributes
	 *            the redirect attributes
	 * @return the string
	 * @throws ParseException
	 *             the parse exception
	 */
	@PostMapping(value = "/master/creategst")
	public String createGST(@Valid @ModelAttribute("gstobj") GSTForm obj, BindingResult result, Model model,
			RedirectAttributes redirectAttributes) throws ParseException {
		if (!result.hasErrors() && obj.getTaxCode() != null
				&& gstrepo.getGoodsServicesTaxByCode(obj.getTaxCode()) == null) {
			SystemParameter dateObj = sysRepo.getSystemParameterByPropName(DATE_FORMAT_KEY);
			String dateFormat = DATE_FORMAT_VALUE;
			if (dateObj != null && !dateObj.getPropValue().isEmpty()) {
				dateFormat = dateObj.getPropValue();
			}
			DateFormat formatter = new SimpleDateFormat(dateFormat);
			GoodsServicesTax ad = new GoodsServicesTax();
			ad.setCode(obj.getTaxCode());
			ad.setAbbrDesc(obj.getTaxAbbrDesc());
			ad.setDesc(obj.getTaxDesc());
			ad.setAllowedVariance(obj.getAllowedVariance());
			ad.setRate(obj.getTaxRate());
			if (!obj.getEffectiveStartingDate().isEmpty()) {
				ad.setEffectiveStartDate(formatter.parse(obj.getEffectiveStartingDate()));
			}

			if (!obj.getEffectiveEndingDate().isEmpty()) {
				ad.setEffectiveEndDate(formatter.parse(obj.getEffectiveEndingDate()));
			}
			ad.setCreatedBy(getCurrentUser() != null ? getCurrentUser().getUsername() : null);
			ad.setCreatedOn(new Date());
			ad.setLastModifiedBy(getCurrentUser() != null ? getCurrentUser().getUsername() : null);
			ad.setActive(true);
			ad.setLastModifiedOn(new Date());
			try {
				gstrepo.createGoodsServicesTax(ad);
				model.addAttribute("gstobj", new GSTForm());
				auditService.putAudit(new Date(), "Tax Code: " + obj.getTaxCode() + " created successfully",
						getCurrentUser().getUsername(), "Create GST");
				redirectAttributes.addFlashAttribute("info", "GST created successfully");
			} catch (Exception e) {
				LOGGER.error(e);
			}
		} else {
			redirectAttributes.addFlashAttribute("error", "GST Tax code already exists");
		}
		return REDIRECT_MASTER_GST;
	}

	@GetMapping(value = "/master/deletegst/{code}")
	public String deleteGst(@PathVariable String code, Model model) {
		GoodsServicesTax ad = gstrepo.getGoodsServicesTaxByCode(code);
		if (ad != null) {
			gstrepository.delete(ad);
		}
		return REDIRECT_MASTER_GST;
	}

	/**
	 * Update GST.
	 *
	 * @param obj
	 *            the obj
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @param redirectAttributes
	 *            the redirect attributes
	 * @return the string
	 */
	@PostMapping(value = "/master/updategst")
	public String updateGST(@Valid @ModelAttribute("gstobj") GSTForm obj, BindingResult result, Model model,
			RedirectAttributes redirectAttributes) {
		if (!result.hasErrors()) {
			GoodsServicesTax ad = gstrepo.getGoodsServicesTaxByCode(obj.getTaxCode());
			try {
				boolean isActive = true;
				SystemParameter dateObj = sysRepo.getSystemParameterByPropName(DATE_FORMAT_KEY);
				String dateFormat = DATE_FORMAT_VALUE;
				if (dateObj != null && !dateObj.getPropValue().isEmpty()) {
					dateFormat = dateObj.getPropValue();
				}
				DateFormat formatter = new SimpleDateFormat(dateFormat);
				if (ad != null) {
					ad.setCode(obj.getTaxCode());
					ad.setAbbrDesc(obj.getTaxAbbrDesc());
					ad.setDesc(obj.getTaxDesc());
					ad.setAllowedVariance(obj.getAllowedVariance());
					ad.setRate(obj.getTaxRate());
					if (!obj.getEffectiveStartingDate().isEmpty()) {
						ad.setEffectiveStartDate(formatter.parse(obj.getEffectiveStartingDate()));
					}

					if (!obj.getEffectiveEndingDate().isEmpty()) {
						ad.setEffectiveEndDate(formatter.parse(obj.getEffectiveEndingDate()));
					}
					ad.setLastModifiedBy(getCurrentUser() != null ? getCurrentUser().getUsername() : null);
					ad.setLastModifiedOn(new Date());
					if (obj.getIsActive().contains("InActive")) {
						isActive = false;
					}
					if (isActive != ad.isActive()) {
						if (obj.getIsActive().contains("InActive")) {
							ad.setDeactivatedBy(getCurrentUser() != null ? getCurrentUser().getUsername() : null);
							ad.setDeactivatedOn(new Date());
						}
						if (obj.getIsActive().equals("Active")) {
							ad.setReactivatedOn(new Date());
							ad.setReactivatedBy(getCurrentUser() != null ? getCurrentUser().getUsername() : null);
						}
					}
					ad.setActive("Active".equals(obj.getIsActive()));
					gstrepo.createGoodsServicesTax(ad);
					auditService.putAudit(new Date(), "Tax Code: " + obj.getTaxCode() + " updated successfully",
							getCurrentUser().getUsername(), "Update GST");
					redirectAttributes.addFlashAttribute("info", "GST updated successfully");
				} else {
					redirectAttributes.addFlashAttribute("error", "GST Object not exists");
					LOGGER.info("Object does not exist");
					return "/master/edit_gst";
				}
			} catch (Exception e) {
				LOGGER.error(e);
			}
		}
		return REDIRECT_MASTER_GST;
	}

	/**
	 * Edits the GST.
	 *
	 * @param taxcode
	 *            the taxcode
	 * @param model
	 *            the model
	 * @return the string
	 */
	@GetMapping(value = "/master/editgst/{taxcode}")
	public String editGST(@PathVariable String taxcode, Model model) {
		GoodsServicesTax tobj = gstrepo.getGoodsServicesTaxByCode(taxcode);
		edit = true;

		if (tobj != null) {
			String dateFormat = "dd-MM-yyyy hh:mm:ss";
			SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_TIME_FORMAT");
			if (dateObj != null && !dateObj.getPropValue().isEmpty()) {
				dateFormat = dateObj.getPropValue();
			}
			DateFormat formatter = new SimpleDateFormat(dateFormat);
			String dateFormat2 = DATE_FORMAT_VALUE;
			SystemParameter dateObj2 = sysRepo.getSystemParameterByPropName(DATE_FORMAT_KEY);
			if (dateObj2 != null && !dateObj2.getPropValue().isEmpty()) {
				dateFormat2 = dateObj2.getPropValue();
			}
			DateFormat format = new SimpleDateFormat(dateFormat2);
			String createdOn = "";
			String modifiedOn = "";
			String reactivatedOn = "";
			String deactivatedOn = "";
			if (tobj.getCreatedOn() != null) {
				createdOn = formatter.format(tobj.getCreatedOn());
			}
			if (tobj.getLastModifiedOn() != null) {
				modifiedOn = formatter.format(tobj.getLastModifiedOn());
			}
			if (tobj.getReactivatedOn() != null) {
				reactivatedOn = formatter.format(tobj.getReactivatedOn());
			}
			if (tobj.getDeactivatedOn() != null) {
				deactivatedOn = formatter.format(tobj.getDeactivatedOn());
			}
			GSTForm obj = new GSTForm();
			obj.setCreatedOn(createdOn);
			obj.setDeactivatedOn(deactivatedOn);
			obj.setReactivatedOn(reactivatedOn);
			obj.setLastModifiedOn(modifiedOn);
			obj.setTaxCode(tobj.getCode());
			obj.setTaxAbbrDesc(tobj.getAbbrDesc());
			obj.setTaxDesc(tobj.getDesc());
			obj.setCreatedBy(tobj.getCreatedBy());
			obj.setLastModifiedBy(tobj.getLastModifiedBy());
			obj.setDeactivatedBy(tobj.getDeactivatedBy());
			obj.setReactivatedBy(tobj.getReactivatedBy());
			obj.setAllowedVariance(tobj.getAllowedVariance());
			obj.setTaxRate(tobj.getRate());
			if (tobj.getEffectiveStartDate() != null) {
				obj.setEffectiveStartingDate("" + format.format(tobj.getEffectiveStartDate()));
			}
			if (tobj.getEffectiveEndDate() != null) {
				obj.setEffectiveEndingDate("" + format.format(tobj.getEffectiveEndDate()));
			}
			obj.setIsActive(tobj.isActive() ? "Active" : "InActive");
			model.addAttribute("gstobj", obj);
			model.addAttribute("edit", edit);
			model.addAttribute("date_format", dateFormat2);
			return "/master/edit_gst";
		} else {
			return REDIRECT_MASTER_GST;
		}
	}

	@Override
	protected String getHeaderTitle() {
		return "Manage GST";
	}

	/**
	 * Gets the store based on company.
	 *
	 * @param companycode
	 *            the companycode
	 * @return the store based on company
	 */
	@RequestMapping(value = "/master/gst/existinggst", method = RequestMethod.GET)
	public @ResponseBody GoodsServicesTax checkGST(@RequestParam("gstcode") String gstcode,
			RedirectAttributes redirectAttributes, Model model) {
		return gstrepo.getGoodsServicesTaxByCode(gstcode);
	}
}