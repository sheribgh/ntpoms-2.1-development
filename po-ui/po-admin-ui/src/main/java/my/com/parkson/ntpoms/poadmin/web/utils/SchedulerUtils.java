/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.poadmin.web.utils;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;

import my.com.parkson.ntpoms.common.entities.Scheduler;
import my.com.parkson.ntpoms.common.services.AccessLogService;
import my.com.parkson.ntpoms.common.services.SchedulerService;
import my.com.parkson.ntpoms.utils.ApplicationContextProvider;

/**
 * The Class SchedulerUtils.
 */
public final class SchedulerUtils {
	private static final String ERROR = "ERROR";
	private static final String WARNING = "WARNING";
	private static final String FAILED = "Failed";
	private static Logger LOGGER = Logger.getLogger(SchedulerUtils.class);

	@Autowired
	private static SchedulerService schedulerService;

	public static boolean checkDatabaseConnection(DataSource dataSource, String jobName) {

		if (schedulerService == null) {
			schedulerService = ApplicationContextProvider.getContext().getBean(SchedulerService.class);
		}

		Connection connection = null;
		boolean isConnected = false;
		try {
			System.out.println(jobName);
			connection = DataSourceUtils.getConnection(dataSource);
			if (connection != null) {
				isConnected = true;
			}
		} catch (Exception ex) {
			Scheduler scheduler = schedulerService.getSchedulerByJobname(jobName);
			scheduler.setStatus(FAILED);
			schedulerService.createScheduler(scheduler);
			LOGGER.error(ex);
		} finally {
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException e) {
				LOGGER.error(e);
			}
		}
		return isConnected;
	}

	/**
	 * Instantiates a new scheduler utils.
	 */
	private SchedulerUtils() {
		super();
	}
}