package my.com.parkson.ntpoms.poadmin.web.scheduler;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.LogMessages;
import my.com.parkson.ntpoms.common.entities.ProfitCenter;
import my.com.parkson.ntpoms.common.services.CompanyService;
import my.com.parkson.ntpoms.common.services.DepartmentService;
import my.com.parkson.ntpoms.common.services.ProfitCenterService;
import my.com.parkson.ntpoms.common.services.StoreService;
import my.com.parkson.ntpoms.poadmin.web.models.FileUploadParam;
import my.com.parkson.ntpoms.poadmin.web.utils.SchedulerUtils;
import my.com.parkson.ntpoms.utils.ApplicationContextProvider;

public class ProfitCenterMaster extends AbstractFileTypeMaster {
	private static Logger LOGGER = Logger.getLogger(ProfitCenterMaster.class);

	private String companyCode;
	private String fileNameToBeProceed = null;

	private String os = "Windows";
	private String pathSeperator = "\\";
	private FileUploadParam fileUploadParam;

	@Autowired
	private ProfitCenterService profitCenterService;

	@Autowired
	private CompanyService companyService;

	@Autowired
	private DepartmentService departmentService;

	@Autowired
	private StoreService storeService;

	public ProfitCenterMaster(FileUploadParam fileUploadParam) {
		this.os = System.getProperty("os.name");
		if (this.os.contains("Linux")) {
			this.pathSeperator = "/";
		}
		this.fileUploadParam = fileUploadParam;
	}

	public String process(String filename) {
		if (profitCenterService == null) {
			profitCenterService = ApplicationContextProvider.getContext().getBean(ProfitCenterService.class);
		}
		if (companyService == null) {
			companyService = ApplicationContextProvider.getContext().getBean(CompanyService.class);
		}
		if (departmentService == null) {
			departmentService = ApplicationContextProvider.getContext().getBean(DepartmentService.class);
		}
		if (storeService == null) {
			storeService = ApplicationContextProvider.getContext().getBean(StoreService.class);
		}
		if (StringUtils.isNotEmpty(filename)) {
			this.currentFileName = filename;
			this.fileNameToBeProceed = this.fileUploadParam.getUploadFolder() + this.pathSeperator + filename;
			this.companyCode = CSVTools.getCompanyCode(filename);
		}
		String resultMessage = "";
		boolean databaseExists = SchedulerUtils.checkDatabaseConnection(this.fileUploadParam.getDataSource(),
				this.fileUploadParam.getJobName());
		if (!databaseExists) {
			resultMessage = "The Master File [" + currentFileName
					+ "] Failed to be uploaded because database does not exist.";
			return resultMessage;
		}
		if (companyService.getCompanyByCode(companyCode) == null) {
			resultMessage = "The Master File [" + currentFileName + "] Failed. Company Code [" + companyCode
					+ "] does not exist.";
			return resultMessage;
		}
		BufferedReader bufferreader = null;
		try {
			bufferreader = new BufferedReader(new FileReader(this.fileNameToBeProceed));
		} catch (FileNotFoundException e) {
			LOGGER.error("ERROR :  FileNotFoundException : " + e.getMessage());
		}
		if (bufferreader != null) {
			try {
				int row = 0;
				String line = "";
				String csvSplitBy = "|";
				int dataRowCounter = 0;
				int tableColumnCount = 0;
				boolean validationSuccess = true;
				ArrayList<Integer> failedRows = new ArrayList<>();
				int dataColumnCount = 0;
				String dataSplitBy = "\\s*\\|\\s*";
				String[] dataItem = new String[0];
				addMessage = new StringBuilder();
				int lineCount = getLintCount(bufferreader);
				bufferreader.close();
				bufferreader = new BufferedReader(new FileReader(this.fileNameToBeProceed));
				while ((line = bufferreader.readLine()) != null) {
					if ((row == 0)) {
						validationSuccess = checkFileHeader(line, csvSplitBy, validationSuccess);
					}
					if (row == 1) {
						if (CSVTools.isTableHeader(line, csvSplitBy)) {
							tableColumnCount = CSVTools.getColumnCount(line, csvSplitBy);
						} else {
							validationSuccess = false;
							addMessage.append("Header does not exist.");
							addMessage.append('\n');
						}
					}
					if (row > 1 && row < (lineCount - 1)) {
						if (CSVTools.isTableData(line, csvSplitBy)) {
							dataRowCounter++;
						} else {
							validationSuccess = false;
							addMessage.append("Row count does not match number of rows.");
							addMessage.append('\n');
						}
					}
					if ((row == lineCount - 1)) {
						validationSuccess = checkRowCount(line, csvSplitBy, dataRowCounter, validationSuccess);
					}
					row++;
				}
				bufferreader.close();
				if (validationSuccess) {
					dataColumnCount = 0;
					try {
						bufferreader = new BufferedReader(new FileReader(this.fileNameToBeProceed));
					} catch (FileNotFoundException e) {
						LOGGER.error("ERROR : FileNotFoundException : " + e.getMessage());
					}
					row = 1;
					dataItem = new String[0];
					while ((line = bufferreader.readLine()) != null) {
						if (row > 2 && row < lineCount) {
							validationSuccess = true;
							dataColumnCount = CSVTools.getColumnCount(line, csvSplitBy);
							dataItem = line.split(dataSplitBy);
							if (dataColumnCount == tableColumnCount) {
								String storeCode = dataItem[1].substring(0, 4);
								String depCode = dataItem[1].substring(4, 7);
								validationSuccess = validateDataItems(dataRowCounter, validationSuccess, dataItem,
										storeCode, depCode);
								if (validationSuccess) {
									if (!upsert(dataItem)) {
										failedRows.add(row);
									}
								} else {
									failedRows.add(row);
								}
							} else {
								failedRows.add(row);
							}
						}
						row++;
					}
					resultMessage = getFailedRowMessages(dataRowCounter, failedRows);
				} else {
					resultMessage = RESULT_MESSAGE_PREFIX + currentFileName
							+ "] Failed to be uploaded because of wrong format suspect, captured message : "
							+ addMessage.toString();
					bufferreader.close();
				}
				resultMessage = getFailedRowMessages(dataRowCounter, failedRows);
				bufferreader.close();
				return resultMessage;
			} catch (IOException e) {
				LOGGER.error("ERROR : FileNotFoundException : " + e.getMessage());
				return "error happened processing file " + currentFileName + ".";
			} catch (Exception e) {
				LOGGER.error("ERROR : Exception : The Master File [" + currentFileName + "] upload failed : "
						+ e.getMessage());
				return "error occured while processing file [" + currentFileName + "].";
			} finally {
				if (bufferreader != null) {
					try {
						bufferreader.close();
					} catch (IOException e) {
						LOGGER.error("ERROR : IOException : " + e.getMessage());
					}
				}
			}
		} else {
			resultMessage = RESULT_MESSAGE_PREFIX + currentFileName + "] can not be found.";
			return resultMessage;
		}
	}

	@Override
	protected boolean validateDataItems(int row, boolean validationSuccess, String[] dataItem, String storeCode,
			String depCode) {
		if (dataItem[1].isEmpty()) {
			validationSuccess = false;
			addMessage.append(ROW_MESSSAGE_PREFIX).append((row - 2))
					.append("] : Validation failed Profit Center Code is Empty.").append('\n');
		} else {
			if (storeService.getStoreByCode(storeCode) == null) {
				addMessage.append("Store Code [ ").append(storeCode).append(" ] does not exist.").append('\n');
				validationSuccess = false;
			}
			if (departmentService.getDepartmentByCode(depCode) == null) {
				addMessage.append("Department Code [ ").append(depCode).append(" ] does not exist.").append('\n');
				validationSuccess = false;
			}
		}
		if (dataItem[1].equals("null")) {
			validationSuccess = false;
			addMessage.append(ROW_MESSSAGE_PREFIX).append((row - 2))
					.append("] : Validation failed Profit Center Code is null.").append('\n');
		}
		if (dataItem[1].length() > 15) {
			validationSuccess = false;
			addMessage.append(ROW_MESSSAGE_PREFIX).append((row - 2))
					.append("] : Validation failed Profit Center Code is more than 15 chars.").append('\n');
		}
		if (!dataItem[1].matches("[0-9]+")) {
			validationSuccess = false;
			addMessage.append(ROW_MESSSAGE_PREFIX).append((row - 2))
					.append("] : Validation failed Profit Center Code invalid format.").append('\n');
		}
		if (dataItem[2].length() > 15) {
			validationSuccess = false;
			addMessage.append(ROW_MESSSAGE_PREFIX).append((row - 2))
					.append("] : Validation failed Profit Center Abbr Name is more than 15 chars.").append('\n');
		}
		if (!dataItem[3].equals("TRUE") && !dataItem[3].equals("FALSE")) {
			validationSuccess = false;
			addMessage.append(ROW_MESSSAGE_PREFIX).append((row - 2))
					.append("] : Validation failed isActive is not boolean.").append('\n');
		}
		if (dataItem[4].length() > 70) {
			validationSuccess = false;
			addMessage.append(ROW_MESSSAGE_PREFIX).append((row - 2))
					.append("] : Validation failed Address 1 is more than 70 chars.").append('\n');
		}
		if (dataItem[5].length() > 70) {
			validationSuccess = false;
			addMessage.append(ROW_MESSSAGE_PREFIX).append((row - 2))
					.append("] : Validation failed Address 2 is more than 70 chars.").append('\n');
		}
		if (dataItem[6].length() > 70) {
			validationSuccess = false;
			addMessage.append(ROW_MESSSAGE_PREFIX).append((row - 2))
					.append("] : Validation failed Address 3 is more than 70 chars.").append('\n');
		}
		if (dataItem[7].length() > 10) {
			validationSuccess = false;
			addMessage.append(ROW_MESSSAGE_PREFIX).append((row - 2))
					.append("] : Validation failed Post Code is more than 10 chars.").append('\n');
		}
		if (!dataItem[7].matches("^[a-zA-Z0-9]*$+")) {
			validationSuccess = false;
			addMessage.append(ROW_MESSSAGE_PREFIX).append((row - 2))
					.append("] : Validation failed Post Code Invalid Format.").append('\n');
		}
		if (dataItem[8].length() > 70) {
			validationSuccess = false;
			addMessage.append(ROW_MESSSAGE_PREFIX).append((row - 2))
					.append("] : Validation failed Post Code Area is more than 70 chars.").append('\n');
		}
		return validationSuccess;
	}

	public boolean upsert(String[] dataItem) {
		boolean flag = true;
		String pcCode = dataItem[1];
		String pcAbbr = dataItem[2];
		boolean isActive = Boolean.parseBoolean(dataItem[3]);
		String address1 = dataItem[4];
		String address2 = dataItem[5];
		String address3 = dataItem[6];
		String postCode = dataItem[7];
		String postCodeArea = dataItem[8];
		ProfitCenter profitCenter = null;
		try {
			profitCenter = profitCenterService.getProfitCenterByCode(pcCode);
			if (profitCenter != null) {
				profitCenter.setCode(pcCode);
				profitCenter.setAbbrName(pcAbbr);
				profitCenter.setIsactive(isActive);
				profitCenter.setAddressLine1(address1);
				profitCenter.setAddressLine2(address2);
				profitCenter.setAddressLine3(address3);
				profitCenter.setPostCode(postCode);
				profitCenter.setPostArea(postCodeArea);
				profitCenter.setLastModifiedOn(new Date());
				profitCenter.setLastModifiedBy(this.fileUploadParam.getUsername());
			} else {
				profitCenter = new ProfitCenter();
				profitCenter.setCode(pcCode);
				profitCenter.setAbbrName(pcAbbr);
				profitCenter.setIsactive(isActive);
				profitCenter.setIsactive(isActive);
				profitCenter.setCreatedOn(new Date());
				profitCenter.setCreatedBy(this.fileUploadParam.getUsername());
				profitCenter.setLastModifiedOn(new Date());
				profitCenter.setLastModifiedBy(this.fileUploadParam.getUsername());
			}
			profitCenterService.createProfitCenter(profitCenter);
		} catch (POException e) {
			flag = false;
			addMessage.append("Data insertion problem.");
			addMessage.append('\n');
			LOGGER.error(e);
		}
		return flag;
	}
}