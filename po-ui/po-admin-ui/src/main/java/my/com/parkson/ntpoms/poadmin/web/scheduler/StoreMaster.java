package my.com.parkson.ntpoms.poadmin.web.scheduler;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.LogMessages;
import my.com.parkson.ntpoms.common.entities.Store;
import my.com.parkson.ntpoms.common.services.CompanyService;
import my.com.parkson.ntpoms.common.services.StoreService;
import my.com.parkson.ntpoms.poadmin.web.models.FileUploadParam;
import my.com.parkson.ntpoms.poadmin.web.utils.SchedulerUtils;
import my.com.parkson.ntpoms.utils.ApplicationContextProvider;

public class StoreMaster extends AbstractFileTypeMaster {
	private static Logger LOGGER = Logger.getLogger(StoreMaster.class);

	private String fileNameToBeProceed = null;

	private String os = "Windows";
	private String pathSeperator = "\\";
	private FileUploadParam fileUploadParam;

	@Autowired
	private StoreService storeService;

	@Autowired
	private CompanyService companyService;

	public StoreMaster(FileUploadParam fileUploadParam) {
		this.os = System.getProperty("os.name");
		if (this.os.contains("Linux")) {
			this.pathSeperator = "/";
		}
		this.fileUploadParam = fileUploadParam;
	}

	public String process(String filename) {
		if (storeService == null) {
			storeService = ApplicationContextProvider.getContext().getBean(StoreService.class);
		}
		if (companyService == null) {
			companyService = ApplicationContextProvider.getContext().getBean(CompanyService.class);
		}
		if (StringUtils.isNotEmpty(filename)) {
			this.currentFileName = filename;
			this.fileNameToBeProceed = this.fileUploadParam.getUploadFolder() + this.pathSeperator + filename;
		}
		String resultMessage = "";
		boolean databaseExists = SchedulerUtils.checkDatabaseConnection(this.fileUploadParam.getDataSource(),
				this.fileUploadParam.getJobName());
		if (!databaseExists) {
			resultMessage = "The Master File [" + currentFileName
					+ "] Failed to be uploaded because database does not exist.";
			return resultMessage;
		}
		BufferedReader bufferreader = null;
		try {
			bufferreader = new BufferedReader(new FileReader(this.fileNameToBeProceed));
		} catch (FileNotFoundException e) {
			LOGGER.error("ERROR : FileNotFoundException : " + e.getMessage());
		}
		if (bufferreader != null) {
			try {
				int row = 0;
				String line = "";
				String csvSplitBy = "|";
				int dataRowCounter = 0;
				int tableColumnCount = 0;
				boolean validationSuccess = true;
				ArrayList<Integer> failedRows = new ArrayList<>();
				int dataColumnCount = 0;
				String dataSplitBy = "\\s*\\|\\s*";
				String[] dataItem = new String[0];
				addMessage = new StringBuilder();
				int lineCount = getLintCount(bufferreader);
				bufferreader.close();
				bufferreader = new BufferedReader(new FileReader(this.fileNameToBeProceed));
				while ((line = bufferreader.readLine()) != null) {
					if ((row == 0)) {
						validationSuccess = checkFileHeader(line, csvSplitBy, validationSuccess);
					}
					if (row == 1) {
						if (CSVTools.isTableHeader(line, csvSplitBy)) {
							tableColumnCount = CSVTools.getColumnCount(line, csvSplitBy);
						} else {
							validationSuccess = false;
							addMessage.append("Header does not exist.");
							addMessage.append('\n');
						}
					}
					if (row > 1 && row < (lineCount - 1)) {
						if (CSVTools.isTableData(line, csvSplitBy)) {
							dataRowCounter++;
						} else {
							validationSuccess = false;
							addMessage.append("Row count does not match number of rows.");
							addMessage.append('\n');
						}
					}
					if ((row == lineCount - 1)) {
						validationSuccess = checkRowCount(line, csvSplitBy, dataRowCounter, validationSuccess);
					}
					row++;
				}
				bufferreader.close();
				if (validationSuccess) {
					dataColumnCount = 0;
					try {
						bufferreader = new BufferedReader(new FileReader(this.fileNameToBeProceed));
					} catch (FileNotFoundException e) {
						LOGGER.error("ERROR : FileNotFoundException : " + e.getMessage());
					}
					row = 1;
					dataItem = new String[0];
					while ((line = bufferreader.readLine()) != null) {
						if (row > 2 && row < lineCount) {
							validationSuccess = true;
							dataColumnCount = CSVTools.getColumnCount(line, csvSplitBy);
							dataItem = line.split(dataSplitBy);
							if (dataColumnCount == tableColumnCount) {
								validationSuccess = validateDataItems(dataRowCounter, validationSuccess, dataItem, null,
										null);
								if (validationSuccess) {
									if (!upsert(dataItem)) {
										failedRows.add(row);
									}
								} else {
									failedRows.add(row);
								}
							} else {
								failedRows.add(row);
							}
						}
						row++;
					}
					resultMessage = getFailedRowMessages(dataRowCounter, failedRows);
				} else {
					resultMessage = RESULT_MESSAGE_PREFIX + currentFileName
							+ "] Failed to be uploaded because of wrong format suspect, captured message : "
							+ addMessage.toString();
				}
				bufferreader.close();
				return resultMessage;
			} catch (IOException e) {
				LOGGER.error("ERROR : StoreMaster > FileNotFoundException : " + e.getMessage());
				return "error happened processing file " + currentFileName + ".";
			} catch (Exception e) {
				LOGGER.error("ERROR : StoreMaster > Exception : The Master File [" + currentFileName
						+ "] upload failed : " + e.getMessage());
				return "error occured while processing file [" + currentFileName + "].";
			} finally {
				if (bufferreader != null) {
					try {
						bufferreader.close();
					} catch (IOException e) {
						LOGGER.error("ERROR : DepartmentMaster > IOException : " + e.getMessage());
					}
				}
			}
		} else {
			resultMessage = "The Master File [" + currentFileName + "] can not be found.";
			return resultMessage;
		}
	}

	@Override
	protected boolean validateDataItems(int row, boolean validationSuccess, String[] dataItem, String storeCode,
			String depCode) {
		if (dataItem[1].isEmpty()) {
			validationSuccess = false;
			addMessage.append(ROW_MESSSAGE_PREFIX).append((row - 2))
					.append("] : Validation failed Store Code is Empty.").append('\n');
		}
		if (dataItem[1].equals("null")) {
			validationSuccess = false;
			addMessage.append(ROW_MESSSAGE_PREFIX).append((row - 2))
					.append("] :  Validation failed Store Code is NULL.").append('\n');
		}
		if (dataItem[1].length() > 4) {
			validationSuccess = false;
			addMessage.append(ROW_MESSSAGE_PREFIX).append((row - 2))
					.append("] : Validation failed Store Code is more than 4 chars.").append('\n');
		}
		if (!dataItem[1].matches("^[a-zA-Z0-9]*$+")) {
			validationSuccess = false;
			addMessage.append(ROW_MESSSAGE_PREFIX).append((row - 2))
					.append("] :  Validation failed Store Code invalid format.").append('\n');
		}
		if (dataItem[2].length() > 30) {
			validationSuccess = false;
			addMessage.append(ROW_MESSSAGE_PREFIX).append((row - 2))
					.append("] : Validation failed Store Name is more than 30 chars.").append('\n');
		}
		if (dataItem[3].length() > 15) {
			validationSuccess = false;
			addMessage.append(ROW_MESSSAGE_PREFIX).append((row - 2))
					.append("] :  Validation failed Store Abbr Name is more than 30 chars.").append('\n');
		}
		if (dataItem[4].isEmpty()) {
			validationSuccess = false;
			addMessage.append(ROW_MESSSAGE_PREFIX).append((row - 2)).append("] : Validation failed Comp Code is Empty.")
					.append('\n');
		} else {
			if (companyService.getCompanyByCode(dataItem[4]) == null) {
				addMessage.append(ROW_MESSSAGE_PREFIX).append(row - 2).append(" : Company Code [ ").append(dataItem[4])
						.append(" ] does not exist.").append('\n');
				validationSuccess = false;
			}
		}
		if (dataItem[4].equals("null")) {
			validationSuccess = false;
			addMessage.append(ROW_MESSSAGE_PREFIX).append((row - 2)).append("] : Validation failed Comp Code is NULL.")
					.append('\n');
		}
		if (!dataItem[5].equals("TRUE") && !dataItem[5].equals("FALSE")) {
			validationSuccess = false;
			addMessage.append(ROW_MESSSAGE_PREFIX).append((row - 2))
					.append("] : Validation failed. IsActive is not boolean.").append('\n');
		}
		return validationSuccess;
	}

	public boolean upsert(String[] dataItem) {
		boolean flag = true;
		String storeCode = dataItem[1];
		String storeName = dataItem[2];
		String storeAbbrName = dataItem[3];
		String companyCode = dataItem[4];
		boolean isActive = Boolean.parseBoolean(dataItem[5]);
		Store store = null;
		try {
			store = storeService.getStoreByCode(storeCode);
			if (store != null) {
				store.setCode(storeCode);
				store.setName(storeName);
				store.setAbbrName(storeAbbrName);
				store.setCompCode(companyCode);
				store.setActive(isActive);
				store.setLastModifiedOn(new Date());
				store.setLastModifiedBy(this.fileUploadParam.getUsername());
			} else {
				store = new Store();
				store.setCode(storeCode);
				store.setName(storeName);
				store.setAbbrName(storeAbbrName);
				store.setCompCode(companyCode);
				store.setActive(isActive);
				store.setCreatedOn(new Date());
				store.setCreatedBy(this.fileUploadParam.getUsername());
				store.setLastModifiedOn(new Date());
				store.setLastModifiedBy(this.fileUploadParam.getUsername());
			}
			storeService.createStore(store);
		} catch (POException e) {
			flag = false;
			addMessage.append("Data insertion problem.");
			addMessage.append('\n');
			LOGGER.error(e);
		}
		return flag;
	}
}