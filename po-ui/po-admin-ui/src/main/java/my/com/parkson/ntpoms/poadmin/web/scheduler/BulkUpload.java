package my.com.parkson.ntpoms.poadmin.web.scheduler;

import java.io.IOException;
import java.util.Date;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import my.com.parkson.ntpoms.common.entities.Scheduler;
import my.com.parkson.ntpoms.common.entities.SystemParameter;
import my.com.parkson.ntpoms.common.services.AccessLogService;
import my.com.parkson.ntpoms.common.services.AuditService;
import my.com.parkson.ntpoms.common.services.SchedulerService;
import my.com.parkson.ntpoms.common.services.SystemParameterService;
import my.com.parkson.ntpoms.poadmin.web.models.FileUploadParam;
import my.com.parkson.ntpoms.utils.ApplicationContextProvider;

@Component
@Configuration
@PropertySource("classpath:application-dev.properties")
public class BulkUpload implements Job {
	private static Logger LOGGER = Logger.getLogger(BulkUpload.class);
	
	private static final String ERROR = "ERROR";
	private static final String FAILED = "Failed";	
	
	@Autowired
	DataSource dataSource;	
	
	@Autowired
	SystemParameterService systemParameterService;
	
	@Autowired
	private SchedulerService schedulerService;
	
	@Autowired
	private AccessLogService log;
	
	@Autowired
	private AuditService audit;
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		String status = "Executed";
		FileUploadParam fileUploadParam = new FileUploadParam();
		if(dataSource == null) {
			dataSource = ApplicationContextProvider.getContext().getBean(DataSource.class);
		}
		if(systemParameterService == null) {
			systemParameterService = ApplicationContextProvider.getContext().getBean(SystemParameterService.class);
		}
		if(schedulerService == null) {
			schedulerService = ApplicationContextProvider.getContext().getBean(SchedulerService.class);
		}
		if(log == null) {
			log = ApplicationContextProvider.getContext().getBean(AccessLogService.class);
		}
		if(audit == null) {
			audit = ApplicationContextProvider.getContext().getBean(AuditService.class);
		}
		try {
			SystemParameter uploadFolderPath = systemParameterService.getSystemParameterByPropName("UPLOAD_FOLDER_PATH");
			SystemParameter logFolderPath = systemParameterService.getSystemParameterByPropName("LOG_FOLDER_PATH");
			SystemParameter successFolderPath = systemParameterService.getSystemParameterByPropName("SUCCESS_FOLDER_PATH");
			SystemParameter failedFolderPath = systemParameterService.getSystemParameterByPropName("FAILED_FOLDER_PATH");
			boolean isNullFlag = true;
			if(uploadFolderPath != null) {
				fileUploadParam.setUploadFolder(uploadFolderPath.getPropValue());
				isNullFlag = false;
			}
			if(logFolderPath != null) {
				fileUploadParam.setLogFolder(logFolderPath.getPropValue());
				isNullFlag = false;
			}
			if(successFolderPath != null) {
				fileUploadParam.setSuccessFolder(successFolderPath.getPropValue());
				isNullFlag = false;
			}
			if(failedFolderPath != null) {
				fileUploadParam.setFailedFolder(failedFolderPath.getPropValue());
				isNullFlag = false;
			}
			if(isNullFlag) {
				status = FAILED;
			}
		} catch (Exception ex) {			
			status = FAILED;
			log.log(ERROR, "Failed to get value by property name.", null, 72, "BulkUpload");
		}	
		try {
			JobDataMap dataMap = context.getJobDetail().getJobDataMap();
			String user = dataMap.getString("user");
			String jobName = dataMap.getString("jobName");
			fileUploadParam.setUsername(user);
			fileUploadParam.setJobName(jobName);
			fileUploadParam.setDataSource(dataSource);
			MasterFileUpload masterFileUpload = new MasterFileUpload(fileUploadParam);
			masterFileUpload.bulkUpload();			
			LOGGER.debug("Scheduler jobName : " + jobName);
			Scheduler scheduler = schedulerService.getSchedulerByJobname(jobName);
			if(scheduler != null ) {
				scheduler.setStatus(status);
				scheduler.setEndDate(context.getNextFireTime());
				schedulerService.createScheduler(scheduler);
				audit.putAudit(new Date(), "Successful Scheduler - Bulk Upload.", user, "CREATE SCHEDULER");
			} else {			
				log.log(ERROR, "Failed to find Scheduler job [" + jobName + "] in database.", null, 103, "BulkUpload");

			}			
		} catch (IOException e) {
			log.log(ERROR, "Failed uploading data.", null, 108, "BulkUpload");
		} 
	}
}