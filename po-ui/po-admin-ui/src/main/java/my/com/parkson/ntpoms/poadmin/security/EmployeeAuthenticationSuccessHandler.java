package my.com.parkson.ntpoms.poadmin.security;

import java.io.IOException;
import java.util.Collection;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import my.com.parkson.ntpoms.common.entities.User;
import my.com.parkson.ntpoms.common.services.AuditService;
import my.com.parkson.ntpoms.common.services.UsersService;

@Component
public class EmployeeAuthenticationSuccessHandler extends SimpleUrlAuthenticationFailureHandler
		implements AuthenticationSuccessHandler {

	@Autowired
	private AuditService auditService;

	@Autowired
	private UsersService user;

	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

	@Override
	public void onAuthenticationSuccess(HttpServletRequest arg0, HttpServletResponse arg1,
			Authentication authentication) throws IOException, ServletException {

		/* Fetching Client's IP Address */
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
				.getRequest();

		if (request.getParameter("username") == "superadmin") {
			User us = user.getUsersByUsername("superadmin");
			if (us.getLogincount() == 3) {
				setDefaultFailureUrl("/login?error");
				setDefaultFailureUrl("/login?error=4");
			} else {
				String ip = request.getRemoteAddr();
				auditService.putAudit(new Date(),
						"User " + authentication.getName() + " with IP address " + ip + " is signed in",
						authentication.getName(), "Log In");
				redirectStrategy.sendRedirect(arg0, arg1, "/master/dashboard");
			}

		} else {
			String ip = request.getRemoteAddr();
			auditService.putAudit(new Date(),
					"User " + authentication.getName() + " with IP address " + ip + " is signed in",
					authentication.getName(), "Log In");
			redirectStrategy.sendRedirect(arg0, arg1, "/master/dashboard");
		}

	}

}
