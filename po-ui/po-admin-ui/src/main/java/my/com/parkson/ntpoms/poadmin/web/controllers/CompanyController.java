/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.poadmin.web.controllers;

import static my.com.parkson.ntpoms.common.MessageCodes.FAILED_PERSIST_EXCEPTION;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.Valid;

import static my.com.parkson.ntpoms.common.MessageCodes.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.UriUtils;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.LogMessages;
import my.com.parkson.ntpoms.common.entities.Company;
import my.com.parkson.ntpoms.common.entities.LoginList;
import my.com.parkson.ntpoms.common.entities.SystemParameter;
import my.com.parkson.ntpoms.common.repositories.CompanyRepository;
import my.com.parkson.ntpoms.common.services.AccessLogService;
import my.com.parkson.ntpoms.common.services.AuditService;
import my.com.parkson.ntpoms.common.services.CompanyService;
import my.com.parkson.ntpoms.common.services.LogInService;
import my.com.parkson.ntpoms.common.services.MessageService;
import my.com.parkson.ntpoms.common.services.SystemParameterService;
import my.com.parkson.ntpoms.poadmin.security.SecurityUtil;
import my.com.parkson.ntpoms.poadmin.web.models.CompanyForm;
import my.com.parkson.ntpoms.poadmin.web.models.Pager;

@Slf4j
@Controller
@Secured(SecurityUtil.MANAGE_COMPANY)
@RequiredArgsConstructor
public class CompanyController extends AbstractAdminController {

	private static final int BUTTONS_TO_SHOW = 5;
	private static final int INITIAL_PAGE = 0;
	private static final int INITIAL_PAGE_SIZE = 5;
	private static final int[] PAGE_SIZES = { 5, 10, 20 };

	private static String LOGO_UPLOAD_PATH = "LOGO_FOLDER_PATH";
	private static String LETTERHEAD_UPLOAD_PATH = "LETTERHEAD_FOLDER_PATH";
	private static String TCP_L_UPLOAD_PATH = "TERMS_FOLDER_PATH";

	private String logoDir;
	private String letterDir;
	private String termspDir;
	private String termsLDir;
	boolean edit = false;

	private static Logger LOGGER = Logger.getLogger(CompanyController.class);

	/** The messages. */
	@Autowired
	private MessageService messages;

	@Autowired
	private AuditService auditService;

	private CompanyForm tempobj;

	@Autowired
	private CompanyService comrepo;

	@Autowired
	private CompanyRepository comrepository;

	@Autowired
	private SystemParameterService sysrepo;

	@Autowired
	private LogInService dblistrepo;

	/**
	 * On page load company.
	 *
	 * @param pageSize
	 *            the page size
	 * @param page
	 *            the page
	 * @param code
	 *            the code
	 * @param codeHRIS
	 *            the code HRIS
	 * @param name
	 *            the name
	 * @param isActive
	 *            the is active
	 * @return the model and view
	 */
	@GetMapping("/master/company")
	public ModelAndView onPageLoadCompany() {
		logoDir = sysrepo.getSystemParameterValueByPropName(LOGO_UPLOAD_PATH);
		letterDir = sysrepo.getSystemParameterValueByPropName(LETTERHEAD_UPLOAD_PATH);
		termspDir = sysrepo.getSystemParameterValueByPropName(TCP_L_UPLOAD_PATH);
		termsLDir = sysrepo.getSystemParameterValueByPropName(TCP_L_UPLOAD_PATH);
		ModelAndView modelAndView = new ModelAndView("/master/company");

		modelAndView.getModelMap().addAttribute("companyobj", tempobj != null ? tempobj : new CompanyForm());

		return modelAndView;
	}

	@RequestMapping(value = "/master/companygrid", method = RequestMethod.GET)
	public String companyGrid(@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page, @RequestParam("code") Optional<String> code,
			@RequestParam("codeHRIS") Optional<String> codeHRIS, @RequestParam("name") Optional<String> name,
			@RequestParam("status") Optional<String> isActive, @RequestParam("sortingkey") Optional<String> sortingkey,
			@RequestParam("sortingorder") Optional<String> sortingorder, Model model) {

		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
		Page<Company> list;

		Specification<Company> s = new Specification<Company>() {
			@Override
			public Predicate toPredicate(Root<Company> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> ls = new ArrayList<>();
				if (code.isPresent() && !code.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("code")), "%" + code.get().toLowerCase() + "%"));
				}
				if (codeHRIS.isPresent() && !codeHRIS.get().isEmpty()) {

					ls.add(cb.like(cb.lower(root.get("codeHRIS")), "%" + codeHRIS.get().toLowerCase() + "%"));
				}
				if (name.isPresent() && !name.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("name")), "%" + name.get().toLowerCase() + "%"));
				}
				if (isActive.isPresent() && !isActive.get().isEmpty() && !isActive.get().equals("All")) {
					ls.add(cb.equal(root.get("isActive"), "Active".equals(isActive.get())));
				}

				return cb.and(ls.toArray(new Predicate[0]));
			}
		};
		Sort sorttemp = new Sort(Sort.Direction.DESC, "lastModifiedOn");
		if (sortingkey.isPresent() && sortingorder.isPresent()) {
			sorttemp = new Sort(sortingorder.get().equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC,
					sortingkey.get());
		}

		if (code.isPresent() || codeHRIS.isPresent() || name.isPresent() || isActive.isPresent()) {
			list = comrepo.getAllPageableCompany(s, new PageRequest(evalPage, evalPageSize, sorttemp));

		} else {
			list = comrepo.getAllPageableCompanyOrdered(new PageRequest(evalPage, evalPageSize, sorttemp));
		}

		int serialid = 0;

		int pagesize = evalPage + 1;

		if (pagesize > 1) {
			int start = evalPageSize * pagesize;
			serialid = start - evalPageSize;
		}

		Pager pager = new Pager(list.getTotalPages(), list.getNumber(), BUTTONS_TO_SHOW);
		model.addAttribute("serialid", serialid);
		model.addAttribute("list", list);
		model.addAttribute("selectedPageSize", evalPageSize);
		model.addAttribute("pageSizes", PAGE_SIZES);
		model.addAttribute("pager", pager);
		model.addAttribute("totalRecords", list.getTotalElements());
		model.addAttribute("code", code.isPresent() ? code.get() : "");
		model.addAttribute("codeHRIS", codeHRIS.isPresent() ? codeHRIS.get() : "");
		model.addAttribute("name", name.isPresent() ? name.get() : "");
		model.addAttribute("status", isActive.isPresent() ? isActive.get() : "");
		model.addAttribute("sortingorder", sortingorder.isPresent() ? sortingorder.get() : "DESC");
		model.addAttribute("sortingkey", sortingkey.isPresent() ? sortingkey.get() : "lastModifiedOn");

		return "/master/company :: result-table";
	}

	/**
	 * Creates the company.
	 *
	 * @param obj
	 *            the obj
	 * @param compLogo
	 *            the comp logo
	 * @param compLetterhead
	 *            the comp letterhead
	 * @param termsPortrait
	 *            the terms portrait
	 * @param termsLandscape
	 *            the terms landscape
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @param redirectAttributes
	 *            the redirect attributes
	 * @return the string
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@PostMapping(value = "/master/creatcompany")
	public String createCompany(@Valid @ModelAttribute("companyobj") CompanyForm obj,
			@RequestParam("compLogofile") MultipartFile compLogo,
			@RequestParam("compLetterheadfile") MultipartFile compLetterhead,
			@RequestParam("termsPortraitfile") MultipartFile termsPortrait,
			@RequestParam("termsLandscapefile") MultipartFile termsLandscape, BindingResult result, Model model,
			RedirectAttributes redirectAttributes) throws IOException {

		logoDir = sysrepo.getSystemParameterValueByPropName(LOGO_UPLOAD_PATH);
		letterDir = sysrepo.getSystemParameterValueByPropName(LETTERHEAD_UPLOAD_PATH);
		termspDir = sysrepo.getSystemParameterValueByPropName(TCP_L_UPLOAD_PATH);
		File terms_file_path = new File(termspDir);
		File logo_file_path = new File(logoDir);
		File letter_file_path = new File(letterDir);

		
		if (logoDir == null || !logo_file_path.exists() ) {
			redirectAttributes.addFlashAttribute("error", "System Parameter LOGO_UPLOAD_PATH does not exist.");
			return "redirect:/master/company";
		}
		if (letterDir == null || !letter_file_path.exists()) {
			redirectAttributes.addFlashAttribute("error", "System Parameter LETTERHEAD_UPLOAD_PATH does not exist.");
			return "redirect:/master/company";
		}
		if (termspDir == null || !terms_file_path.exists()) {
			redirectAttributes.addFlashAttribute("error", "System Parameter TCP_L_UPLOAD_PATH does not exist.");
			return "redirect:/master/company";
		}

		if (!result.hasErrors() && obj.getCompCode() != null && comrepo.getCompanyByCode(obj.getCompCode()) == null) {
			Company ad = new Company();
			LoginList dblist = new LoginList();

			Calendar now = Calendar.getInstance(); // Gets the current date and
													// time
			int year = now.get(Calendar.YEAR);

			dblist.setCompCode(obj.getCompCode());
			dblist.setCompName(obj.getCompAbbrName());
			dblist.setYear(String.valueOf(year));

			ad.setCreatedBy(getCurrentUser() != null ? getCurrentUser().getUsername() : null);
			ad.setCreatedOn(new Date());
			ad.setLastModifiedBy(getCurrentUser() != null ? getCurrentUser().getUsername() : null);
			ad.setLastModifiedOn(new Date());
			ad.setActive(true);
			ad.setCode(obj.getCompCode());
			ad.setCodeHRIS(obj.getCompCodeHRIS());
			ad.setName(obj.getCompName());
			ad.setAbbrName(obj.getCompAbbrName());
			ad.setRegNo(obj.getCompRegNo());

			try{
			if (compLogo != null && !compLogo.isEmpty()) {

				String[] compLogoSplit = compLogo.getContentType().split("/");
				if (compLogoSplit.length > 1) {

					if (logoDir == null || !logo_file_path.exists()) {
						redirectAttributes.addFlashAttribute("error",
								"System parameter " + LOGO_UPLOAD_PATH + " Does not exist.");
						return "redirect:/master/company";
					} else {
						ad.setLogo(obj.getCompCode() + "_LOGO" + "." + compLogoSplit[1]);
						File destination = new File(logoDir + obj.getCompCode() + "_LOGO" + "." + compLogoSplit[1]);
						compLogo.transferTo(destination);
					}
				}

			}
			}catch(Exception e){
				
				LOGGER.error(e);
			}
			
			try{
			if (compLetterhead != null && !compLetterhead.isEmpty()) {
				String[] compLetterHeadSplit = compLetterhead.getContentType().split("/");
				if (compLetterHeadSplit.length > 1) {

					
					letterDir = sysrepo.getSystemParameterValueByPropName(LETTERHEAD_UPLOAD_PATH);
					

					if (letterDir == null || !letter_file_path.exists()) {
						redirectAttributes.addFlashAttribute("error",
								"System parameter " + LETTERHEAD_UPLOAD_PATH + " does not exist.");
						return "redirect:/master/company";
					} else {
						ad.setLetterHeading(obj.getCompCode() + "_LETTERHEAD" + "." + compLetterHeadSplit[1]);
						File destination = new File(
								letterDir + obj.getCompCode() + "_LETTERHEAD" + "." + compLetterHeadSplit[1]);
						
						
						compLetterhead.transferTo(destination);
					}
				}
			}
			}catch(Exception e){
				LOGGER.error(e);
			}
			
			try{
			if (termsPortrait != null && !termsPortrait.isEmpty()) {
				String[] compTCPSplit = termsPortrait.getContentType().split("/");
				if (compTCPSplit.length > 1) {

					termspDir = sysrepo.getSystemParameterValueByPropName(TCP_L_UPLOAD_PATH);
					if (termspDir == null || !terms_file_path.exists()) {
						redirectAttributes.addFlashAttribute("error",
								"System parameter " + TCP_L_UPLOAD_PATH + " does not exist.");
						return "redirect:/master/company";
					} else {
						ad.setPoTermsAndConditionsPortrait(obj.getCompCode() + "_TERMSP" + "." + compTCPSplit[1]);
						File destination = new File(termspDir + obj.getCompCode() + "_TERMSP" + "." + compTCPSplit[1]);
						
						termsPortrait.transferTo(destination);
					}
				}
			}
			}catch(Exception e){
				LOGGER.error(e);
			}

			try{
			if (termsLandscape != null && !termsLandscape.isEmpty()) {
				String[] compTCLSplit = termsLandscape.getContentType().split("/");
				if (compTCLSplit.length > 1) {

					termsLDir = sysrepo.getSystemParameterValueByPropName("TERMS_FOLDER_PATH");
					if (termsLDir == null || !terms_file_path.exists()) {
						redirectAttributes.addFlashAttribute("error",
								"System parameter " + TCP_L_UPLOAD_PATH + " does not exist.");
						return "redirect:/master/company";
					} else {
						ad.setPoTermsAndConditionsLandscape(obj.getCompCode() + "_TERMSL" + "." + compTCLSplit[1]);
						File destination = new File(termsLDir + obj.getCompCode() + "_TERMSL" + "." + compTCLSplit[1]);
						
						termsLandscape.transferTo(destination);
					}
				}
			}
			}catch(Exception e){
				LOGGER.error(e);
			}
			if (comrepo.getCompanyByCodeHRIS(obj.getCompCodeHRIS()) != null) {
				redirectAttributes.addFlashAttribute("error", "Company HRIS already exists");
				tempobj = obj;
			} else {
				try {
					comrepo.createCompany(ad);
					dblistrepo.createCompany(dblist);
					auditService.putAudit(new Date(), "Company: " + obj.getCompCode() + " created successfully",
							getCurrentUser().getUsername(), "Create Company");
					redirectAttributes.addFlashAttribute("info", "Company created successfully");
					tempobj = null;
				} catch (Exception e) {
					LOGGER.error(e);
				}
			}

		} else {
			redirectAttributes.addFlashAttribute("error", "Company code already exists");
			tempobj = obj;
		}
		return "redirect:/master/company";
	}

	@GetMapping(value = "/api/image")
	public ResponseEntity<InputStreamResource> getImage(@RequestParam("filepath") Optional<String> filepath) {
		ResponseEntity<InputStreamResource> temp = ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG)
				.body(new InputStreamResource(new ByteArrayInputStream(new byte[0])));
		try {
			if (filepath.isPresent()) {

				File file = new File(filepath.get());
				if (file.exists()) {
					return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG)
							.body(new InputStreamResource(new FileInputStream(file)));
				} else {
					return temp;
				}
			} else {
				return temp;
			}
		} catch (Exception e) {
			LOGGER.error(e);
			return temp;
		}
	}

	@GetMapping(value = "/api/pdf")
	public ResponseEntity<InputStreamResource> getPDF(@RequestParam("filepath") Optional<String> filepath) {
		ResponseEntity<InputStreamResource> temp = ResponseEntity.ok().contentType(MediaType.APPLICATION_PDF)
				.body(new InputStreamResource(new ByteArrayInputStream(new byte[0])));
		try {
			if (filepath.isPresent()) {
				File file = new File(filepath.get());
				if (file.exists()) {
					return ResponseEntity.ok().contentType(MediaType.APPLICATION_PDF)
							.body(new InputStreamResource(new FileInputStream(file)));
				} else {
					return temp;
				}
			} else {
				return temp;
			}
		} catch (Exception e) {
			LOGGER.error(e);
			return temp;
		}
	}

	/**
	 * Update company.
	 *
	 * @param obj
	 *            the obj
	 * @param compLogo
	 *            the comp logo
	 * @param compLetterhead
	 *            the comp letterhead
	 * @param termsPortrait
	 *            the terms portrait
	 * @param termsLandscape
	 *            the terms landscape
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @param redirectAttributes
	 *            the redirect attributes
	 * @return the string
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@PostMapping(value = "/master/updatecompany")
	public String updateCompany(@Valid @ModelAttribute("companyobj") CompanyForm obj,
			@RequestParam("compLogofile") MultipartFile compLogo,
			@RequestParam("compLetterheadfile") MultipartFile compLetterhead,
			@RequestParam("termsPortraitfile") MultipartFile termsPortrait,
			@RequestParam("termsLandscapefile") MultipartFile termsLandscape, BindingResult result, Model model,
			RedirectAttributes redirectAttributes) throws IOException {
		
		File terms_file_path = new File(termspDir);
		File logo_file_path = new File(logoDir);
		File letter_file_path = new File(letterDir);
		
		if (!result.hasErrors()) {
			Company ad = comrepo.getCompanyByCode(obj.getCompCode());
			boolean isActive;
			try {
				if (ad != null) {
					ad.setCreatedBy(getCurrentUser() != null ? getCurrentUser().getUsername() : null);
					ad.setCreatedOn(new Date());
					ad.setLastModifiedBy(getCurrentUser() != null ? getCurrentUser().getUsername() : null);
					ad.setLastModifiedOn(new Date());
					if (obj.getIsActive().contains("InActive")) {
						isActive = false;
					} else {
						isActive = true;
					}

					if (isActive != ad.isActive()) {

						if (obj.getIsActive().contains("InActive")) {
							ad.setDeactivatedBy(getCurrentUser() != null ? getCurrentUser().getUsername() : null);
							ad.setDeactivatedOn(new Date());
						}

						if (obj.getIsActive().equals("Active")) {
							ad.setReactivatedOn(new Date());
							ad.setReactivatedBy(getCurrentUser() != null ? getCurrentUser().getUsername() : null);
						}
					}
					ad.setActive("Active".equals(obj.getIsActive()));
					ad.setCode(obj.getCompCode());
					ad.setCodeHRIS(obj.getCompCodeHRIS());
					ad.setName(obj.getCompName());
					ad.setAbbrName(obj.getCompAbbrName());
					ad.setRegNo(obj.getCompRegNo());

					try{
					if (compLogo != null && !compLogo.isEmpty()) {

						String[] compLogoSplit = compLogo.getContentType().split("/");
						if (compLogoSplit.length > 1) {

							if (logoDir == null || !logo_file_path.exists()) {
								redirectAttributes.addFlashAttribute("error",
										"System parameter " + LOGO_UPLOAD_PATH + "does not exist.");
								return "redirect:/master/company";
							} else {
								ad.setLogo(obj.getCompCode() + "_LOGO" + "." + compLogoSplit[1]);
								File destination = new File(
										logoDir + obj.getCompCode() + "_LOGO" + "." + compLogoSplit[1]);
								compLogo.transferTo(destination);
							}
						}

					}
					}catch(UnsupportedEncodingException e){
						
						LOGGER.error(e);
						
					}

					try{
					if (compLetterhead != null && !compLetterhead.isEmpty()) {
						String[] compLetterHeadSplit = compLetterhead.getContentType().split("/");
						if (compLetterHeadSplit.length > 1) {

							if (letterDir == null || !letter_file_path.exists()) {

								redirectAttributes.addFlashAttribute("error",
										"System parameter " + LETTERHEAD_UPLOAD_PATH + "does not exist.");
								return "redirect:/master/company";
							} else {
								ad.setLetterHeading(obj.getCompCode() + "_LETTERHEAD" + "." + compLetterHeadSplit[1]);
								File destination = new File(
										letterDir + obj.getCompCode() + "_LETTERHEAD" + "." + compLetterHeadSplit[1]);
								compLetterhead.transferTo(destination);
							}
						}
					}
					}catch(UnsupportedEncodingException e){
					
						LOGGER.error(e);	
					}

					try{
					if (termsPortrait != null && !termsPortrait.isEmpty()) {
						String[] compTCPSplit = termsPortrait.getContentType().split("/");
						if (compTCPSplit.length > 1) {

							if (termspDir == null || !terms_file_path.exists()) {
								redirectAttributes.addFlashAttribute("error",
										"System parameter " + TCP_L_UPLOAD_PATH + "does not exist.");
								return "redirect:/master/company";
							} else {
								ad.setPoTermsAndConditionsPortrait(
										obj.getCompCode() + "_TERMSP" + "." + compTCPSplit[1]);
								File destination = new File(
										termspDir + obj.getCompCode() + "_TERMSP" + "." + compTCPSplit[1]);
								
								termsPortrait.transferTo(destination);
							}
						}
					}
					}catch(UnsupportedEncodingException e){
						LOGGER.error(e);
					}
					
					try{
					if (termsLandscape != null && !termsLandscape.isEmpty()) {
						String[] compTCLSplit = termsLandscape.getContentType().split("/");
						if (compTCLSplit.length > 1) {

							if (termsLDir == null || !terms_file_path.exists()) {
								redirectAttributes.addFlashAttribute("error",
										"System parameter " + TCP_L_UPLOAD_PATH + "does not exist.");
								return "redirect:/master/company";
							} else {
								ad.setPoTermsAndConditionsLandscape(
										obj.getCompCode() + "_TERMSL" + "." + compTCLSplit[1]);
								File destination = new File(
										termsLDir + obj.getCompCode() + "_TERMSL" + "." + compTCLSplit[1]);
								
								termsLandscape.transferTo(destination);
							}
						}
					}
					}catch(UnsupportedEncodingException e){
						LOGGER.error(e);
					}
					comrepo.createCompany(ad);
					auditService.putAudit(new Date(), "Company: " + obj.getCompCode() + " updated successfully",
							getCurrentUser().getUsername(), "Update Company");
					redirectAttributes.addFlashAttribute("info", "Company Updated successfully");

				} else {
					redirectAttributes.addFlashAttribute("error", "Company Object not exists");
					return "/master/edit_company";
				}
			} catch (Exception e) {
				LOGGER.error(e);
			}
		}
		return "redirect:/master/company";
	}

	/**
	 * Edits the company.
	 *
	 * @param code
	 *            the code
	 * @param model
	 *            the model
	 * @return the string
	 */
	@GetMapping(value = "/master/editcompany/{code}")
	public String editCompany(@PathVariable String code, Model model, RedirectAttributes redirectAttributes) {
		Company ad = comrepo.getCompanyByCode(code);
		edit = true;

		if (ad != null) {
			CompanyForm obj = new CompanyForm();
			String date_format = "dd-MM-yyyy hh:mm:ss";
			SystemParameter dateObj = sysrepo.getSystemParameterByPropName("DATE_TIME_FORMAT");
			if (dateObj != null) {
				if (!dateObj.getPropValue().isEmpty()) {
					date_format = dateObj.getPropValue();
				}
			}
			DateFormat formatter = new SimpleDateFormat(date_format);
			String createdOn = "";
			String modifiedOn = "";
			String reactivatedOn = "";
			String deactivatedOn = "";
			if (ad.getCreatedOn() != null) {
				createdOn = formatter.format(ad.getCreatedOn());
			}
			if (ad.getLastModifiedOn() != null) {
				modifiedOn = formatter.format(ad.getLastModifiedOn());
			}
			if (ad.getReactivatedOn() != null) {
				reactivatedOn = formatter.format(ad.getReactivatedOn());
			}
			if (ad.getDeactivatedOn() != null) {
				deactivatedOn = formatter.format(ad.getDeactivatedOn());
			}
			obj.setCreatedOn(createdOn);
			obj.setDeactivatedOn(deactivatedOn);
			obj.setReactivatedOn(reactivatedOn);
			obj.setLastModifiedOn(modifiedOn);
			obj.setIsActive(ad.isActive() ? "Active" : "InActive");
			obj.setCompCode(ad.getCode());
			obj.setCreatedBy(ad.getCreatedBy());
			obj.setLastModifiedBy(ad.getLastModifiedBy());
			obj.setReactivatedBy(ad.getReactivatedBy());
			obj.setDeactivatedBy(ad.getDeactivatedBy());
			obj.setCompCodeHRIS(ad.getCodeHRIS());
			obj.setCompName(ad.getName());
			obj.setCompAbbrName(ad.getAbbrName());
			obj.setCompRegNo(ad.getRegNo());

			if (ad.getLogo() != null) {
				obj.setCompLogo(ad.getLogo());
			}

			if (ad.getLetterHeading() != null) {
				obj.setCompanyLetterHeading(ad.getLetterHeading());
			}

			if (ad.getPoTermsAndConditionsPortrait() != null) {
				obj.setPoTAndCPortrait(ad.getPoTermsAndConditionsPortrait());
			}

			if (ad.getPoTermsAndConditionsLandscape() != null) {
				obj.setPoTAndCLandscape(ad.getPoTermsAndConditionsLandscape());
			}
			model.addAttribute("edit", edit);
			model.addAttribute("companyobj", obj);
			if (obj.getCompLogo() != null) {

				try {
					String filedir = logoDir;
					String fileName = obj.getCompLogo();
					String newPath = UriUtils.encodePath(filedir + fileName, "UTF-8");

					model.addAttribute("company_logo_name", fileName);
					model.addAttribute("company_logo_file", newPath);

					
					boolean chklogoExist = logoExist(new File(ad.getLogo()));
					if (chklogoExist == false) {

						model.addAttribute("missedLogo", chklogoExist);
						
						
						ad.setLogo(null);
						comrepo.createCompany(ad);
						obj.setCompLogo(null);
						return "redirect:/master/company";

					}

				} catch (UnsupportedEncodingException e) {
					LOGGER.error(e);
					redirectAttributes.addFlashAttribute("error", "Unable to find LOGO File");
				}

			}

			if (obj.getCompanyLetterHeading() != null) {

				try {
					String filedir = letterDir;
					String fileName = obj.getCompanyLetterHeading();

					File chkFileType = new File(fileName);

					String filetype = getFileExtension(chkFileType);

					String newPath = UriUtils.encodePath(filedir + fileName, "UTF-8");
					model.addAttribute("company_letter_name", fileName);
					model.addAttribute("company_letter_file", newPath);
					model.addAttribute("File_Letter_Head_ExtensionType", filetype);

					boolean chkletterExist = letterExist(new File(ad.getLetterHeading()));
					if (chkletterExist == false) {

						model.addAttribute("missedLetter", chkletterExist);
						ad.setLetterHeading(null);
						comrepo.createCompany(ad);
						obj.setCompanyLetterHeading(null);
						redirectAttributes.addFlashAttribute("error", "Unable to find LETTER HEAD PATH File");

					}
				} catch (UnsupportedEncodingException e) {
					LOGGER.error(e);
					redirectAttributes.addFlashAttribute("error", "Unable to find Letter Head File");
				}
			}

			if (obj.getPoTAndCPortrait() != null) {
				try {
					String filedir = termspDir;

					String fileName = obj.getPoTAndCPortrait();

					String newPath = UriUtils.encodePath(filedir + fileName, "UTF-8");

					model.addAttribute("company_termsp_name", fileName);
					model.addAttribute("company_termsp_file", newPath);

					boolean chktcpExist = tcpExist(new File(ad.getPoTermsAndConditionsPortrait()));
					if (chktcpExist == false) {

						model.addAttribute("missedTcp", chktcpExist);
						ad.setPoTermsAndConditionsPortrait(null);
						comrepo.createCompany(ad);
						obj.setPoTAndCPortrait(null);
						redirectAttributes.addFlashAttribute("error", "Unable to find TERMS PATH File");

					}
				} catch (UnsupportedEncodingException e) {
					LOGGER.error(e);
					redirectAttributes.addFlashAttribute("error", "Unable to find Terms and Condition Portrait File");
				}
			}

			if (obj.getPoTAndCLandscape() != null) {
				try {
					String filedir = termsLDir;
					String fileName = obj.getPoTAndCLandscape();
					String newPath = UriUtils.encodePath(filedir + fileName, "UTF-8");

					model.addAttribute("company_termsl_name", fileName);
					model.addAttribute("company_termsl_file", newPath);

					boolean chktclExist = tclExist(new File(ad.getPoTermsAndConditionsLandscape()));
					if (chktclExist == false) {

						model.addAttribute("missedTcl", chktclExist);
						ad.setPoTermsAndConditionsLandscape(null);
						comrepo.createCompany(ad);
						obj.setPoTAndCLandscape(null);
						redirectAttributes.addFlashAttribute("error", "Unable to find TERMS PATH File");

					}
				} catch (UnsupportedEncodingException e) {
					LOGGER.error(e);
					redirectAttributes.addFlashAttribute("error", "Unable to find Terms and Condition Landscape File");
				}
			}
			return "/master/edit_company";
		} else {
			return "redirect:/master/company";
		}
	}

	@GetMapping(value = "/master/deletecompany/{code}")
	public String deleteCompany(@PathVariable String code, Model model) {

		Company ad = comrepo.getCompanyByCode(code);
		if (ad != null) {
			comrepository.delete(ad);

		}
		return "redirect:/master/company";
	}

	private boolean logoExist(File fileName) {
		String[] filename = fileName.getName().split("_");
		boolean logoexist = false;
		

		if (logoDir != null) {
			File[] files = new File(logoDir).listFiles();
			File logo_file_path = new File(logoDir); // If file path exists
			if(logo_file_path.exists()){
			for (File file : files) {

				if (file.isFile()) {

					if (file.getName().contains(filename[0])) {
						logoexist = true;

					}

				}

			}
			}
		}
		return logoexist;

	}

	private boolean letterExist(File fileName) {
		String[] filename = fileName.getName().split("_");
		boolean letterexist = false;

		if (letterDir != null) {
			//File letter_file_path = new File(letterDir); // If file path exists
			File[] files = new File(letterDir).listFiles();
			//if(letter_file_path.exists()){
			for (File file : files) {

				if (file.isFile()) {

					if (file.getName().contains(filename[0])) {
						letterexist = true;

					}

				}

			}
			//}
			
		}
		return letterexist;

	}

	private boolean tcpExist(File fileName) {

		String[] filename = fileName.getName().split("_");
		boolean tcpexist = false;

		if (termspDir != null) {
			File[] files = new File(termspDir).listFiles();
			File termsp_file_path = new File(termspDir); // If file path exists
			if(termsp_file_path.exists()){
			for (File file : files) {

				if (file.isFile()) {

					if (file.getName().contains(filename[0])) {
						tcpexist = true;

					}

				}

			}
			}
		}
		return tcpexist;

	}

	private boolean tclExist(File fileName) {
		String[] filename = fileName.getName().split("_");
		boolean tclexist = false;

		if (termsLDir != null) {
			File[] files = new File(termsLDir).listFiles();
			File termsl_file_path = new File(termsLDir); // If file path exists
			if(termsl_file_path.exists()){
			for (File file : files) {

				if (file.isFile()) {

					if (file.getName().contains(filename[0])) {
						tclexist = true;

					}

				}

			}
			}
		}
		return tclexist;

	}

	@Override
	protected String getHeaderTitle() {
		return "Manage Company";
	}

	private String getFileExtension(File file) {
		String fileName = file.getName();
		if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
			return fileName.substring(fileName.lastIndexOf(".") + 1);
		else
			return "";
	}

	/**
	 * Gets the store based on company.
	 *
	 * @param companycode
	 *            the companycode
	 * @return the store based on company
	 */
	@RequestMapping(value = "/master/company/existingcompany", method = RequestMethod.GET)
	public @ResponseBody Company checkCompanyCode(@RequestParam("companycode") String companycode,
			RedirectAttributes redirectAttributes, Model model) {

		return comrepo.getCompanyByCode(companycode);

	}

	/**
	 * Gets the store based on company.
	 *
	 * @param companycode
	 *            the companycode
	 * @return the store based on company
	 */
	@RequestMapping(value = "/master/company/existingcompanyHRIS", method = RequestMethod.GET)
	public @ResponseBody Company checkCompanyHRIS(@RequestParam("companycodeHRIS") String companycodeHRIS,
			RedirectAttributes redirectAttributes, Model model) {

		return comrepo.getCompanyByCodeHRIS(companycodeHRIS);

	}

	@RequestMapping(value = "/master/company/deletecompanyLogo", method = RequestMethod.GET)
	public @ResponseBody Company deleteLogo(@RequestParam("deleteLogo") boolean deleteLogo,
			@RequestParam("compCode") String compCode, RedirectAttributes redirectAttributes, Model model) {

		if(compCode!=null){
		Company companyCode = comrepo.getCompanyByCode(compCode);

		if(companyCode.getLogo()!=null){
		File logoFilename = new File(companyCode.getLogo());

		File[] files = new File(logoDir).listFiles();
		for (File file : files) {
			if (file.isFile()) {
				if (file.getName().contains(logoFilename.getName())) {
					file.delete();
					companyCode.setLogo(null);

				}
			}
		}
		}

		comrepo.createCompany(companyCode);

		return companyCode;
		}
		
		return null;

	}

	@RequestMapping(value = "/master/company/deletecompanyLetter", method = RequestMethod.GET)
	public @ResponseBody Company deleteLetter(@RequestParam("deleteLetter") boolean deleteLetter,
			@RequestParam("compCode") String compCode, RedirectAttributes redirectAttributes, Model model) {

		if(compCode!=null){
		Company companyCode = comrepo.getCompanyByCode(compCode);
		
		if(companyCode.getLetterHeading()!=null){
		File letterFilename = new File(companyCode.getLetterHeading());

		File[] files = new File(letterDir).listFiles();
		for (File file : files) {
			if (file.isFile()) {
				if (file.getName().contains(letterFilename.getName())) {
					file.delete();
					companyCode.setLetterHeading(null);

				}
			}
		}
		}
		comrepo.createCompany(companyCode);

		return companyCode;
		}
		return null;
	}

	@RequestMapping(value = "/master/company/deletecompanytcp", method = RequestMethod.GET)
	public @ResponseBody Company deleteTcp(@RequestParam("deleteTcp") boolean deleteTcp,
			@RequestParam("compCode") String compCode, RedirectAttributes redirectAttributes, Model model) {

		if(compCode!=null){
		Company companyCode = comrepo.getCompanyByCode(compCode);
		
		if(companyCode.getPoTermsAndConditionsPortrait()!=null){
		File tcpFilename = new File(companyCode.getPoTermsAndConditionsPortrait());

		File[] files = new File(termspDir).listFiles();
		for (File file : files) {
			if (file.isFile()) {
				if (file.getName().contains(tcpFilename.getName())) {
					file.delete();
					companyCode.setPoTermsAndConditionsPortrait(null);

				}
			}
		}
		}

		comrepo.createCompany(companyCode);

		return companyCode;
		}
		
		return null;

	}

	@RequestMapping(value = "/master/company/deletecompanytcl", method = RequestMethod.GET)
	public @ResponseBody Company deleteTcl(@RequestParam("deleteTcl") boolean deleteTcl,
			@RequestParam("compCode") String compCode, RedirectAttributes redirectAttributes, Model model) {

		if(compCode!=null){
		Company companyCode = comrepo.getCompanyByCode(compCode);
		
		if(companyCode.getLetterHeading()!=null){
		File tclFilename = new File(companyCode.getLetterHeading());

		File[] files = new File(letterDir).listFiles();
		for (File file : files) {
			if (file.isFile()) {
				if (file.getName().contains(tclFilename.getName())) {
					file.delete();
					companyCode.setPoTermsAndConditionsLandscape(null);

				}
			}
		}
		}

		comrepo.createCompany(companyCode);

		return companyCode;
		}
		return null;

	}

}
