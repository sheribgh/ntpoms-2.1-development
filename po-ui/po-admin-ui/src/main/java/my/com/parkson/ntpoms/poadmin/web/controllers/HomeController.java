/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.poadmin.web.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.poadmin.web.utils.SessionUtils;

@Slf4j
@Controller
@RequiredArgsConstructor
public class HomeController extends AbstractAdminController {	
	private static final String HOME = "home";
	private static Logger LOGGER = Logger.getLogger(HomeController.class);
	
	@Autowired
    private SessionUtils sessionUtils;

	/**
	 * Home.
	 *
	 * @param model the model
	 * @return the string
	 */
	@GetMapping("/home")
	public String home(Model model) {
		sessionUtils.printSession();
//		if(getCurrentUser() != null) {
//			sessionUtils.expireUserSessions(getCurrentUser().getUsername());
//		}
		model.addAttribute("message", "Welcome");
		return HOME;
	}

	@Override
	protected String getHeaderTitle() {
		return "Manage Home";
	}
}