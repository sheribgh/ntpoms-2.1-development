/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.poadmin.web.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

import flexjson.JSONSerializer;
import my.com.parkson.ntpoms.common.entities.UserRole;
import my.com.parkson.ntpoms.common.model.UserMenu;
import my.com.parkson.ntpoms.common.services.UsersService;

import java.io.IOException;

@ControllerAdvice
@SessionAttributes(types = UserMenu.class)
public class MenuController extends AbstractAdminController {
	private static Logger LOGGER = Logger.getLogger(MenuController.class);
	@Autowired
	private UsersService adminrepo;

	/**
	 * Adds the attributes.
	 *
	 * @param model
	 *            the model
	 */
	@ModelAttribute
	public void addAttributes(Model model) {
		UserRole userRole = adminrepo.getUsersByUser(getCurrentUser() != null
				? (getCurrentUser().getUser() != null ? getCurrentUser().getUser().getUsername() : null) : null);
		if (userRole != null) {
			model.addAttribute("roleId", userRole.getRoleId());
		}
		if (getCurrentUser() != null) {
			model.addAttribute("userid", getCurrentUser().getUsername());
		}

	}

	/**
	 * Gets the json menu.
	 *
	 * @param userMenu
	 *            the user menu
	 * @return the json menu
	 */
	@SuppressWarnings("unused")
	private static ObjectNode getJsonMenu(UserMenu userMenu) {
		ObjectNode result = JsonNodeFactory.instance.objectNode();
		JSONSerializer userMenuSerializer = new JSONSerializer().exclude("pid").exclude("class").exclude("menu.class")
				.exclude("menu.listMenu.class");

		ObjectMapper mapper = new ObjectMapper();
		JsonFactory factory = mapper.getFactory();
		JsonParser parser;
		try {
			parser = factory.createParser(userMenuSerializer.deepSerialize(userMenu));
			JsonNode json = mapper.readTree(parser);
			result.set("usermenu", json);
		} catch (JsonParseException e) {
			LOGGER.error(e);
		} catch (IOException e) {
			LOGGER.error(e);
		}

		return result;
	}

	@Override
	protected String getHeaderTitle() {
		return "Menu Advice";
	}
}