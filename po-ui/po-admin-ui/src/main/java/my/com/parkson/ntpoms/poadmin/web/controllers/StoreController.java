/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.poadmin.web.controllers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.common.entities.Company;
import my.com.parkson.ntpoms.common.entities.Store;
import my.com.parkson.ntpoms.common.entities.SystemParameter;
import my.com.parkson.ntpoms.common.repositories.StoreRepository;
import my.com.parkson.ntpoms.common.services.AuditService;
import my.com.parkson.ntpoms.common.services.CompanyService;
import my.com.parkson.ntpoms.common.services.MessageService;
import my.com.parkson.ntpoms.common.services.StoreService;
import my.com.parkson.ntpoms.common.services.SystemParameterService;
import my.com.parkson.ntpoms.poadmin.security.SecurityUtil;
import my.com.parkson.ntpoms.poadmin.web.models.Pager;
import my.com.parkson.ntpoms.poadmin.web.models.StoreForm;

@Slf4j
@Controller
@Secured(SecurityUtil.MANAGE_STORE)
@RequiredArgsConstructor
public class StoreController extends AbstractAdminController {

	private static Logger LOGGER = Logger.getLogger(StoreController.class);

	private static final int BUTTONS_TO_SHOW = 5;
	private static final int INITIAL_PAGE = 0;
	private static final int INITIAL_PAGE_SIZE = 5;
	private static final int[] PAGE_SIZES = { 5, 10, 20 };
	boolean edit = false;
	SimpleDateFormat dt = new SimpleDateFormat("MM-dd-yyyy hh:mm:ss");

	@Autowired
	private StoreService storerepo;

	@Autowired
	private CompanyService cmpservice;

	private StoreForm tempobj;

	/** The messages. */
	@Autowired
	private MessageService messages;

	@Autowired
	private AuditService auditService;

	@Autowired
	private SystemParameterService sysRepo;

	@Autowired
	private StoreRepository storeRepository;

	/**
	 * On page load store.
	 *
	 * @param pageSize
	 *            the page size
	 * @param page
	 *            the page
	 * @param code
	 *            the code
	 * @param compCode
	 *            the comp code
	 * @param name
	 *            the name
	 * @param isActive
	 *            the is active
	 * @return the model and view
	 */
	@GetMapping("/master/store")
	public ModelAndView onPageLoadStore() {
		ModelAndView modelAndView = new ModelAndView("/master/store");
		modelAndView.addObject("companylist", getOrderCodeAndCompName());
		modelAndView.getModelMap().addAttribute("storeobj", tempobj != null ? tempobj : new StoreForm());
		return modelAndView;
	}

	@RequestMapping(value = "/master/storegrid", method = RequestMethod.GET)
	public String storeGrid(@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page, @RequestParam("code") Optional<String> code,
			@RequestParam("compCode") Optional<String> compCode, @RequestParam("name") Optional<String> name,
			@RequestParam("status") Optional<String> isActive, @RequestParam("sortingkey") Optional<String> sortingkey,
			@RequestParam("sortingorder") Optional<String> sortingorder, Model model) {

		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
		Page<Store> list;
		Specification<Store> s = new Specification<Store>() {
			@Override
			public Predicate toPredicate(Root<Store> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> ls = new ArrayList<>();
				if (code.isPresent() && !code.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("code")), "%" + code.get().toLowerCase() + "%"));
				}
				if (compCode.isPresent() && !compCode.get().isEmpty()) {

					ls.add(cb.like(cb.lower(root.get("compCode")), "%" + compCode.get().toLowerCase() + "%"));
				}
				if (name.isPresent() && !name.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("name")), "%" + name.get().toLowerCase() + "%"));
				}
				if (isActive.isPresent() && !isActive.get().isEmpty() && !isActive.get().equals("All")) {
					ls.add(cb.equal(root.get("isActive"), "Active".equals(isActive.get())));
				}
				return cb.and(ls.toArray(new Predicate[0]));
			}
		};

		Sort sorttemp = new Sort(Sort.Direction.DESC, "lastModifiedOn");

		if (sortingkey.isPresent() && sortingorder.isPresent()) {
			sorttemp = new Sort(sortingorder.get().equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC,
					sortingkey.get());
		}

		if (code.isPresent() || compCode.isPresent() || name.isPresent() || isActive.isPresent()) {
			list = storerepo.getAllPageableStore(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		} else {
			list = storerepo.getAllPageableStoreOrdered(new PageRequest(evalPage, evalPageSize, sorttemp));
		}

		int serialid = 0;

		int pagesize = evalPage + 1;

		if (pagesize > 1) {
			int start = evalPageSize * pagesize;
			serialid = start - evalPageSize;
		}

		Pager pager = new Pager(list.getTotalPages(), list.getNumber(), BUTTONS_TO_SHOW);
		model.addAttribute("serialid", serialid);
		model.addAttribute("list", list);
		model.addAttribute("selectedPageSize", evalPageSize);
		model.addAttribute("pageSizes", PAGE_SIZES);
		model.addAttribute("pager", pager);
		model.addAttribute("totalRecords", list.getTotalElements());
		model.addAttribute("code", code.isPresent() ? code.get() : "");
		model.addAttribute("compCode", compCode.isPresent() ? compCode.get() : "");
		model.addAttribute("name", name.isPresent() ? name.get() : "");
		model.addAttribute("status", isActive.isPresent() ? isActive.get() : "");
		model.addAttribute("companylist", cmpservice.getAllActiveCompany());
		model.addAttribute("sortingorder", sortingorder.isPresent() ? sortingorder.get() : "DESC");
		model.addAttribute("sortingkey", sortingkey.isPresent() ? sortingkey.get() : "lastModifiedOn");

		return "/master/store :: result-table";
	}

	/**
	 * Creates the store.
	 *
	 * @param obj
	 *            the obj
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @param redirectAttributes
	 *            the redirect attributes
	 * @return the string
	 */
	@PostMapping(value = "/master/creatstore")
	public String createStore(@Valid @ModelAttribute("storeobj") StoreForm obj, BindingResult result, Model model,
			RedirectAttributes redirectAttributes) {
		if (!result.hasErrors() && obj.getStoreCode() != null && storerepo.getStoreByCode(obj.getStoreCode()) == null) {
			Store ad = new Store();
			String objprtypecode = obj.getMf11MCompmast_mf11_compCode();
			String[] output = objprtypecode.split("-");
			ad.setCompCode(output[0]);
			ad.setCreatedBy(getCurrentUser() != null ? getCurrentUser().getUsername() : null);
			ad.setCreatedOn(new Date());
			ad.setLastModifiedBy(getCurrentUser() != null ? getCurrentUser().getUsername() : null);
			ad.setLastModifiedOn(new Date());
			ad.setActive(true);
			ad.setCode(obj.getStoreCode());
			ad.setName(obj.getStoreName());
			ad.setAbbrName(obj.getStoreAbbrName());
			try {
				storerepo.createStore(ad);
				auditService.putAudit(new Date(), "Store: " + obj.getStoreCode() + " created successfully",
						getCurrentUser().getUsername(), "Create Store");
				redirectAttributes.addFlashAttribute("info", "Store created successfully");
				tempobj = null;

			} catch (Exception e) {
				LOGGER.error(e);
			}

		} else {
			redirectAttributes.addFlashAttribute("error", "Store already exists");
			tempobj = obj;
		}
		return "redirect:/master/store";
	}

	/**
	 * Update store.
	 *
	 * @param obj
	 *            the obj
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @param redirectAttributes
	 *            the redirect attributes
	 * @return the string
	 */
	@PostMapping(value = "/master/updatestore")
	public String updateStore(@Valid @ModelAttribute("storeobj") StoreForm obj, BindingResult result, Model model,
			RedirectAttributes redirectAttributes) {
		if (!result.hasErrors()) {
			Store ad = storerepo.getStoreByCode(obj.getStoreCode());
			boolean isActive;

			try {
				if (ad != null) {
					ad.setLastModifiedBy(getCurrentUser() != null ? getCurrentUser().getUsername() : null);
					ad.setLastModifiedOn(new Date());
					ad.setCode(obj.getStoreCode());
					ad.setName(obj.getStoreName());
					ad.setAbbrName(obj.getStoreAbbrName());
					String objprtypecode = obj.getMf11MCompmast_mf11_compCode();
					String[] output = objprtypecode.split("-");
					ad.setCompCode(output[0]);
					if (obj.getIsActive().contains("InActive")) {
						isActive = false;
					} else {
						isActive = true;
					}

					if (isActive != ad.isActive()) {

						if (obj.getIsActive().contains("InActive")) {
							ad.setDeactivatedBy(getCurrentUser().getUsername());
							ad.setDeactivatedOn(new Date());
						} else {
							ad.setReactivatedOn(new Date());
							ad.setReactivatedBy(getCurrentUser().getUsername());
						}
					}
					ad.setActive("Active".equals(obj.getIsActive()));
					storerepo.createStore(ad);
					auditService.putAudit(new Date(), "Store: " + obj.getStoreCode() + " updated successfully",
							getCurrentUser().getUsername(), "Update Store ");
					redirectAttributes.addFlashAttribute("info", "Store updated successfully");
				} else {
					redirectAttributes.addFlashAttribute("error", "Store object does not exist");
					return "/master/edit_store";
				}
			} catch (Exception e) {
				LOGGER.error(e);
			}
		}
		return "redirect:/master/store";
	}

	/**
	 * Edits the company.
	 *
	 * @param code
	 *            the code
	 * @param model
	 *            the model
	 * @return the string
	 */
	@GetMapping(value = "/master/editstore/{code}")
	public String editCompany(@PathVariable String code, Model model) {
		edit = true;

		Store ad = storerepo.getStoreByCode(code);
		if (ad != null) {
			StoreForm obj = new StoreForm();
			String date_format = "dd-MM-yyyy hh:mm:ss";
			SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_TIME_FORMAT");
			if (dateObj != null) {
				if (!dateObj.getPropValue().isEmpty()) {
					date_format = dateObj.getPropValue();
				}
			}
			DateFormat formatter = new SimpleDateFormat(date_format);
			String createdOn = "";
			String modifiedOn = "";
			String reactivatedOn = "";
			String deactivatedOn = "";
			if (ad.getCreatedOn() != null) {
				createdOn = formatter.format(ad.getCreatedOn());
			}
			if (ad.getLastModifiedOn() != null) {
				modifiedOn = formatter.format(ad.getLastModifiedOn());
			}
			if (ad.getReactivatedOn() != null) {
				reactivatedOn = formatter.format(ad.getReactivatedOn());
			}
			if (ad.getDeactivatedOn() != null) {
				deactivatedOn = formatter.format(ad.getDeactivatedOn());
			}
			obj.setIsActive(ad.isActive() ? "Active" : "InActive");

			Company comp = cmpservice.getCompanyByCode(ad.getCompCode());

			obj.setMf11MCompmast_mf11_compCode(comp.getCode() + "-" + comp.getName());
			obj.setStoreCode(ad.getCode());
			obj.setStoreAbbrName(ad.getAbbrName());
			obj.setStoreName(ad.getName());
			obj.setCreatedBy(ad.getCreatedBy());
			obj.setCreatedOn(createdOn);
			obj.setDeactivatedOn(deactivatedOn);
			obj.setReactivatedOn(reactivatedOn);
			obj.setLastModifiedOn(modifiedOn);
			obj.setLastModifiedBy(ad.getLastModifiedBy());
			obj.setReactivatedBy(ad.getReactivatedBy());
			obj.setDeactivatedBy(ad.getDeactivatedBy());
			model.addAttribute("companylist", getOrderCodeAndCompName());
			model.addAttribute("storeobj", obj);
			model.addAttribute("edit", edit);
			return "/master/edit_store";
		} else {
			return "redirect:/master/store";
		}
	}

	@GetMapping(value = "/master/deletestore/{code}")
	public String deleteStore(@PathVariable String code, Model model) {

		Store ad = storerepo.getStoreByCode(code);
		if (ad != null) {
			storeRepository.delete(ad);

		}
		return "redirect:/master/store";
	}

	@Override
	protected String getHeaderTitle() {
		return "Manage Store";
	}

	/**
	 * Gets the store based on company.
	 *
	 * @param companycode
	 *            the companycode
	 * @return the store based on company
	 */
	@RequestMapping(value = "/master/store/existingstore", method = RequestMethod.GET)
	public @ResponseBody Store checkCompanyHRIS(@RequestParam("strcode") String storecode,
			RedirectAttributes redirectAttributes, Model model) {

		return storerepo.getStoreByCode(storecode);

	}

	private List<String> getOrderCodeAndCompName() {
		List<String> lis = new ArrayList<>();
		List<Company> temp = cmpservice.getAllActiveCompany();
		if (temp != null) {
			for (Company e : temp) {
				StringBuilder s = new StringBuilder();
				s.append(e.getCode());
				s.append("-");
				s.append(e.getName());
				lis.add(s.toString());
			}
		}
		return lis;
	}

}
