package my.com.parkson.ntpoms.poadmin.web.controllers;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.activation.DataHandler;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import my.com.parkson.ntpoms.common.model.EmailCriteria;
import my.com.parkson.ntpoms.common.services.EmailService;
import my.com.parkson.ntpoms.utils.EmailServiceUtils;

@Controller
@RequestMapping("/")
public class MailController {

	@Autowired
	private EmailService emailService;

	@GetMapping("/mail")
	public String sendMail(RedirectAttributes redirect) throws AddressException, MessagingException, FileNotFoundException, IOException {
		String to = "sagar.subhash@yopmail.com";
		String from = "sagar.subhash@gmail.com";
		Properties props = System.getProperties();
		InputStream inputStream = this.getClass().getResourceAsStream("/mail-settings.properties");
		props.load(inputStream);
		Session session = Session.getDefaultInstance(props, new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(props.getProperty("mail.user"), props.getProperty("mail.passwd"));
			}
		});
		MimeMessage message = new MimeMessage(session);
		message.setFrom(new InternetAddress(from));
		message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
		message.setSubject("Test mail through simple java API");
		VelocityEngine ve = new VelocityEngine();
		ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "class,file");
	    ve.setProperty(RuntimeConstants.RUNTIME_LOG_LOGSYSTEM_CLASS, "org.apache.velocity.runtime.log.Log4JLogChute");
	    ve.setProperty("runtime.log.logsystem.log4j.logger", "VELLOGGER");
	    ve.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
	    ve.setProperty("runtime.log.logsystem.class", "org.apache.velocity.runtime.log.NullLogSystem");
		ve.init();
		Template t = ve.getTemplate("templates/mail/mail-body-template.vm");
		VelocityContext context = new VelocityContext();
		context.put("fname", "subhash");
		context.put("lname", "sagar");
		context.put("proprietor", "siroi.com");
		StringWriter out = new StringWriter();
		t.merge(context, out);
		BodyPart body = new MimeBodyPart();
		body.setContent(out.toString(), "text/html");
		Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(body);
		body = new MimeBodyPart();
        InputStream is = this.getClass().getResourceAsStream("/templates/mail/mail-attachment-template.csv");
        ByteArrayDataSource ds = new ByteArrayDataSource(is, "text/csv");
		body.setDataHandler(new DataHandler(ds));
		body.setFileName("attachment.csv");
		multipart.addBodyPart(body);
		message.setContent(multipart);
		Transport.send(message);
		redirect.addFlashAttribute("sm", "Email send successfull");

		return "redirect:/users/adminuser";
	}

	@GetMapping("/email")
	public String sendEmail(RedirectAttributes redirect) throws MessagingException, IOException {
		String to = "sagar.subhash@yopmail.com";
		Map<String, String> context = new HashMap<>();
		context.put("fname", "subhash");
		context.put("lname", "sagar");
		context.put("proprietor", "siroi.com");
		Set<String> cc = new HashSet<>();
		EmailCriteria emailCriteria = new EmailCriteria();
		emailCriteria.setRecipient(to);
		emailCriteria.setSubject("Test mail through simple java API");
		emailCriteria.setCc(cc);
		Properties props = EmailServiceUtils.loadEmailPropertyFile();
		emailCriteria.setTemplateName(props.getProperty("mail.vm.template.default"));
		emailCriteria.setEmailUser(props.getProperty("mail.user"));
		emailCriteria.setEmailPwd(props.getProperty("mail.passwd"));
		Session session = Session.getDefaultInstance(props, new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(emailCriteria.getEmailUser(), emailCriteria.getEmailPwd());
			}
		});
		emailService.sendEmail(session, context,null, emailCriteria);
		redirect.addFlashAttribute("sm", "Email send successfull.");

		return "redirect:/users/adminuser";
	}
}