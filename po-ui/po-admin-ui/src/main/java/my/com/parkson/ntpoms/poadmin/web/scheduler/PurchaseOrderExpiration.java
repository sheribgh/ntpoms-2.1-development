package my.com.parkson.ntpoms.poadmin.web.scheduler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.entities.Scheduler;
import my.com.parkson.ntpoms.common.entities.SystemParameter;
import my.com.parkson.ntpoms.common.model.EmailCriteria;
import my.com.parkson.ntpoms.common.services.AuditService;
import my.com.parkson.ntpoms.common.services.EmailService;
import my.com.parkson.ntpoms.common.services.SchedulerService;
import my.com.parkson.ntpoms.common.services.SystemParameterService;
import my.com.parkson.ntpoms.main.entities.PurchaseOrder;
import my.com.parkson.ntpoms.main.services.PurchaseOrderService;
import my.com.parkson.ntpoms.utils.ApplicationContextProvider;
import my.com.parkson.ntpoms.utils.EmailServiceUtils;

public class PurchaseOrderExpiration implements Job {
	private static Logger LOGGER = Logger.getLogger(PurchaseOrderExpiration.class);

	@Autowired
	private AuditService audit;

	@Autowired
	private SystemParameterService systemParameterService;

	@Autowired
	private PurchaseOrderService purchaseOrderService;

	@Autowired
	private SchedulerService schedulerService;

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		try {
			if (audit == null) {
				audit = ApplicationContextProvider.getContext().getBean(AuditService.class);
			}
			if (systemParameterService == null) {
				systemParameterService = ApplicationContextProvider.getContext().getBean(SystemParameterService.class);
			}
			if (purchaseOrderService == null) {
				purchaseOrderService = ApplicationContextProvider.getContext().getBean(PurchaseOrderService.class);
			}
			if (schedulerService == null) {
				schedulerService = ApplicationContextProvider.getContext().getBean(SchedulerService.class);
			}
			String status = "Failed";
			SystemParameter expirationParameter = systemParameterService
					.getSystemParameterByPropName("MF_EXPIRATION_LENGTH");
			List<PurchaseOrder> expirelist = new ArrayList<>();
			String expirationValue = null;
			if (expirationParameter != null)
				expirationValue = expirationParameter.getPropValue();
			if (StringUtils.isNotEmpty(expirationValue) && StringUtils.isNumeric(expirationValue)) {
				Calendar c = Calendar.getInstance();
				c.add(Calendar.DATE, -(Integer.parseInt(expirationValue)));
				expirelist.addAll(purchaseOrderService.getPurchaseOrderExpiryDetails(c.getTime(), "Submitted"));
				expirelist.addAll(purchaseOrderService.getPurchaseOrderExpiryDetails(c.getTime(), "Verified"));
				expirelist.addAll(purchaseOrderService.getPurchaseOrderExpiryDetails(c.getTime(), "Draft"));
				expirelist.addAll(purchaseOrderService.getPurchaseOrderExpiryDetails(c.getTime(), "Re-draft"));
				for (PurchaseOrder p : expirelist) {
					p.setStatus("Expired PO");
					p.setExpiryDays(daysBetween(p.getLastModifiedOn(), new Date()));
					p.setExpiryStatus(true);
					p.setExpireDate(new Date());
					purchaseOrderService.createPurchaseOrder(p);
				}
				status = "Success";
			} else {
				LOGGER.info("po expiry days not configured!!!");
			}
			JobDataMap dataMap = context.getJobDetail().getJobDataMap();
			String jobName = dataMap.getString("jobName");
			Scheduler scheduler = schedulerService.getSchedulerByJobname(jobName);
			scheduler.setStatus(status);
			scheduler.setEndDate(context.getNextFireTime());
			schedulerService.createScheduler(scheduler);
			poExpiryEmail(status, expirationValue, expirelist);
			LOGGER.info(": Scheduler successfully created - PurchaseOrderExpiration.");
			audit.putAudit(new Date(), "Scheduler Created Successfully - PurchaseOrderExpiration.", null,
					"PROCESS_FILE");
		} catch (Exception ex) {
			LOGGER.error(": Failed : " + ex.getMessage());
		}
	}

	public void poExpiryEmail(String status, String noofdays, List<PurchaseOrder> polist) {
		try {
			EmailService emailService = ApplicationContextProvider.getContext().getBean(EmailService.class);
			Map<String, String> context = new HashMap<>();
			context.put("status", status);
			context.put("num_days", noofdays);
			context.put("class_name", "PurchaseOrderExpiration");
			context.put("system_name", "NTPOMS");
			Map<String, List> contextlist = new HashMap<>();
			contextlist.put("poList", polist);
			EmailCriteria email = new EmailCriteria();
			email.setSubject("PO Expire Scheduler Notification.");
			Properties props = EmailServiceUtils.loadEmailPropertyFile();
			email.setRecipient(props.getProperty("mail.poexpiry.email"));
			email.setTemplateName(props.getProperty("mail.vm.template.poexpiry"));
			email.setEmailUser(props.getProperty("mail.user"));
			email.setEmailPwd(props.getProperty("mail.passwd"));
			Session session = Session.getDefaultInstance(props, new Authenticator() {
				@Override
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(email.getEmailUser(), email.getEmailPwd());
				}
			});
			emailService.sendEmail(session, context, contextlist, email);
		} catch (POException | MessagingException | IOException e) {
			LOGGER.error(e);
		}
	}

	public int daysBetween(Date expiration, Date today) {
		return (int) ((today.getTime() - expiration.getTime()) / (1000 * 60 * 60 * 24));
	}
}