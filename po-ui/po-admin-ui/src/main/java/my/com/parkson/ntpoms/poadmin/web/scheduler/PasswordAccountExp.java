package my.com.parkson.ntpoms.poadmin.web.scheduler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.mail.Authenticator;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import my.com.parkson.ntpoms.common.entities.Scheduler;
import my.com.parkson.ntpoms.common.entities.SystemParameter;
import my.com.parkson.ntpoms.common.entities.User;
import my.com.parkson.ntpoms.common.model.EmailCriteria;
import my.com.parkson.ntpoms.common.services.AuditService;
import my.com.parkson.ntpoms.common.services.EmailService;
import my.com.parkson.ntpoms.common.services.MessageService;
import my.com.parkson.ntpoms.common.services.SchedulerService;
import my.com.parkson.ntpoms.common.services.SystemParameterService;
import my.com.parkson.ntpoms.common.services.UsersService;
import my.com.parkson.ntpoms.utils.ApplicationContextProvider;
import my.com.parkson.ntpoms.utils.EmailServiceUtils;

@Component
public class PasswordAccountExp implements Job {
	private static Logger LOGGER = Logger.getLogger(PasswordAccountExp.class);
	private String status = "Executed";
	private List<User> expiredPasswords = new ArrayList<>();
	private List<User> expiringPasswords = new ArrayList<>();

	@Autowired
	private UsersService usersService;

	@Autowired
	protected MessageService messageService;

	@Autowired
	private AuditService audit;

	@Autowired
	private SchedulerService schedulerService;

	@Autowired
	private SystemParameterService systemParameterService;

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		if (audit == null) {
			audit = ApplicationContextProvider.getContext().getBean(AuditService.class);
		}
		if (schedulerService == null) {
			schedulerService = ApplicationContextProvider.getContext().getBean(SchedulerService.class);
		}
		if (systemParameterService == null) {
			systemParameterService = ApplicationContextProvider.getContext().getBean(SystemParameterService.class);
		}
		if (usersService == null) {
			usersService = ApplicationContextProvider.getContext().getBean(UsersService.class);
		}
		LOGGER.debug("IN - execute");
		try {
			SystemParameter pwdLifetimeLength = systemParameterService
					.getSystemParameterByPropName("PASSWORD_LIFETIME_LENGTH");
			SystemParameter pwdNotifLength = systemParameterService
					.getSystemParameterByPropName("PASSWORD_NOTIF_LENGTH");
			if (pwdLifetimeLength == null || pwdNotifLength == null) {
				this.status = "Failed";
			}
		} catch (NullPointerException ex) {
			LOGGER.error(ex);
			this.status = "Failed";
		}
		List<User> admins = usersService.getAllUsers();
		Date today = new Date();
		int difference = 0;
		for (User admin : admins) {
			difference = daysBetween(admin.getCreatedOn(), today);
			if (difference >= 30) {
				admin.setBlocked(difference);
				expiredPasswords.add(admin);
			} else {
				if (difference < 3) {
					admin.setBlocked(difference);
					expiringPasswords.add(admin);
				}
			}
		}
		for (User expired : expiredPasswords) {
			accountExpUpdate(expired);
		}
		for (User expiring : expiringPasswords) {
			passwordExpUpdate(expiring);
		}
		JobDataMap dataMap = context.getJobDetail().getJobDataMap();
		String jobName = dataMap.getString("jobName");
		Scheduler scheduler = schedulerService.getSchedulerByJobname(jobName);
		scheduler.setStatus(this.status);
		scheduler.setEndDate(context.getNextFireTime());
		schedulerService.createScheduler(scheduler);
		audit.putAudit(new Date(), "Successful created scheduler for job [" + jobName + "] - Password Account Expiry.",
				null, "CREATE_SCHEDULER");
	}

	public void passwordExpUpdate(User admin) {
		try {
			EmailService emailService = ApplicationContextProvider.getContext().getBean(EmailService.class);
			Map<String, String> context = new HashMap<>();
			context.put("user_name", admin.getUsername());
			context.put("num_days", "" + admin.getBlocked());
			context.put("class_name", "PoEmailGenerator");
			context.put("system_name", "NTPOMS");
			context.put("proprietor", "parkson.com");
			Set<String> cc = new HashSet<>();
			EmailCriteria email = new EmailCriteria();
			email.setRecipient(admin.getEmail());
			email.setSubject("Password Change Notification.");
			email.setCc(cc);
			Properties props = EmailServiceUtils.loadEmailPropertyFile();
			email.setTemplateName(props.getProperty("mail.vm.template.pwdexpiry"));
			email.setEmailUser(props.getProperty("mail.user"));
			email.setEmailPwd(props.getProperty("mail.passwd"));
			Session session = Session.getDefaultInstance(props, new Authenticator() {
				@Override
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(email.getEmailUser(), email.getEmailPwd());
				}
			});
			emailService.sendEmail(session, context, null, email);
		} catch (MessagingException | IOException e) {
			LOGGER.error(e);
		}
	}

	public void accountExpUpdate(User admin) {
		admin.setActive(false);
		usersService.createUsers(admin);
		audit.putAudit(new Date(),
				"Password de-activated succefully. [" + admin.getUsername() + "] - Password Account Expiry.",
				admin.getUsername(), "PASSWORD_DEACTIVATED");
		LOGGER.debug("Password de-activated succefully.");
	}

	public int daysBetween(Date expiration, Date today) {
		return (int) ((today.getTime() - expiration.getTime()) / (1000 * 60 * 60 * 24));
	}
}