/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.poadmin.security;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import lombok.Getter;

/**
 * The Class MenuConfiguration.
 */
public final class MenuConfiguration {

	@Getter
	private static Map<String, String> menuUrlPatternMap = new HashMap<>();

	static {
		menuUrlPatternMap.put("/home", "Home");
		menuUrlPatternMap.put("/audit", "Audits");
		menuUrlPatternMap.put("/employee", "Employee Management");
		menuUrlPatternMap.put("/permissions", "Permissions");
		menuUrlPatternMap.put("/roles", "Roles");
		menuUrlPatternMap.put("/system", "System Parameters");
		menuUrlPatternMap.put("/users", "Users");
	}

	/**
	 * Gets the matching menu.
	 *
	 * @param uri the uri
	 * @return the matching menu
	 */
	public static String getMatchingMenu(String uri) {
		Set<String> keySet = menuUrlPatternMap.keySet();
		for (String key : keySet) {
			if (uri.startsWith(key)) {
				return menuUrlPatternMap.get(key);
			}
		}
		return "";
	}

	/**
	 * Instantiates a new menu configuration.
	 */
	private MenuConfiguration() {
		super();
	}
}
