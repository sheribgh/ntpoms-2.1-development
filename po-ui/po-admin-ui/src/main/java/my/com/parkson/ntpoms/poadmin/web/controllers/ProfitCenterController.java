/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.poadmin.web.controllers;

import static my.com.parkson.ntpoms.common.MessageCodes.FAILED_FETCH_RECORDS_EXCEPTION;
import static my.com.parkson.ntpoms.common.MessageCodes.FAILED_PERSIST_EXCEPTION;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.LogMessages;
import my.com.parkson.ntpoms.common.entities.Company;
import my.com.parkson.ntpoms.common.entities.Department;
import my.com.parkson.ntpoms.common.entities.EmployeeMaster;
import my.com.parkson.ntpoms.common.entities.ProfitCenter;
import my.com.parkson.ntpoms.common.entities.Store;
import my.com.parkson.ntpoms.common.entities.SystemParameter;
import my.com.parkson.ntpoms.common.repositories.ProfitCenterRepository;
import my.com.parkson.ntpoms.common.services.AccessLogService;
import my.com.parkson.ntpoms.common.services.AuditService;
import my.com.parkson.ntpoms.common.services.CompanyService;
import my.com.parkson.ntpoms.common.services.DepartmentService;
import my.com.parkson.ntpoms.common.services.EmployeeMasterService;
import my.com.parkson.ntpoms.common.services.MessageService;
import my.com.parkson.ntpoms.common.services.ProfitCenterService;
import my.com.parkson.ntpoms.common.services.StoreService;
import my.com.parkson.ntpoms.common.services.SystemParameterService;
import my.com.parkson.ntpoms.poadmin.security.SecurityUtil;
import my.com.parkson.ntpoms.poadmin.web.models.AutocompleteForm;
import my.com.parkson.ntpoms.poadmin.web.models.Pager;
import my.com.parkson.ntpoms.poadmin.web.models.ProfitCenterForm;

@Slf4j
@Controller
@Secured(SecurityUtil.MANAGE_PROFIT_CENTER)
@RequiredArgsConstructor
public class ProfitCenterController extends AbstractAdminController {

	private static Logger LOGGER = Logger.getLogger(ProfitCenterController.class);

	private static final int BUTTONS_TO_SHOW = 5;
	private static final int INITIAL_PAGE = 0;
	private static final int INITIAL_PAGE_SIZE = 5;
	private static final int[] PAGE_SIZES = { 5, 10, 20 };

	@Autowired
	private SystemParameterService sysRepo;

	@Autowired
	private ProfitCenterService pcrepo;

	@Autowired
	private ProfitCenterRepository pcrepository;

	@Autowired
	private CompanyService comrepo;

	@Autowired
	private CompanyService cmpservice;

	@Autowired
	private StoreService storerepo;

	@Autowired
	private DepartmentService deptrepo;

	@Autowired
	private EmployeeMasterService empservice;

	/** The messages. */
	@Autowired
	private MessageService messages;

	@Autowired
	private AuditService auditService;

	/**
	 * On page load PC.
	 *
	 * @param pageSize
	 *            the page size
	 * @param page
	 *            the page
	 * @param code
	 *            the code
	 * @param company
	 *            the company
	 * @param name
	 *            the name
	 * @param isActive
	 *            the is active
	 * @param department
	 *            the department
	 * @param store
	 *            the store
	 * @return the model and view
	 */
	@GetMapping("/master/profitcenter")
	public String onPageLoadPC(Model model) {
		String dateFormat = "dd-MM-yyyy";
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_FORMAT");
		if (dateObj != null && !dateObj.getPropValue().isEmpty()) {
			dateFormat = dateObj.getPropValue();
		}
		model.addAttribute("storetypelist", storeTypeforPC());
		model.addAttribute("depatlist", getCodeAndDempName());
		model.addAttribute("cmplist", getOrderCodeAndCompName());
		model.addAttribute("pcheads", getProfitCenterMultiSelect());
		model.addAttribute("pcrights", getProfitCenterMultiSelect());
		model.addAttribute("pcobj", new ProfitCenterForm());
		model.addAttribute("date_format", dateFormat);
		return "/master/profitcenter";
	}

	@RequestMapping(value = "/master/profitcentergrid", method = RequestMethod.GET)
	public String pcgrid(@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page, @RequestParam("code") Optional<String> code,
			@RequestParam("name") Optional<String> name, @RequestParam("company") Optional<String> company,
			@RequestParam("department") Optional<String> department, @RequestParam("store") Optional<String> store,
			@RequestParam("status") Optional<String> isActive, @RequestParam("sortingkey") Optional<String> sortingkey,
			@RequestParam("sortingorder") Optional<String> sortingorder, Model model) {

		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
		Page<ProfitCenter> list;

		Specification<ProfitCenter> s = new Specification<ProfitCenter>() {
			@Override
			public Predicate toPredicate(Root<ProfitCenter> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> ls = new ArrayList<>();
				if (code.isPresent() && !code.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("code")), "%" + code.get().toLowerCase() + "%"));
				}
				if (company.isPresent() && !company.get().isEmpty()) {

					ls.add(cb.like(cb.lower(root.get("company").get("code")), "%" + company.get().toLowerCase() + "%"));
				}
				if (name.isPresent() && !name.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("name")), "%" + name.get().toLowerCase() + "%"));
				}
				if (department.isPresent() && !department.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("department").get("code")),
							"%" + department.get().toLowerCase() + "%"));
				}
				if (store.isPresent() && !store.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("store").get("code")), "%" + store.get().toLowerCase() + "%"));
				}
				if (isActive.isPresent() && !isActive.get().isEmpty() && !isActive.get().equals("All")) {
					ls.add(cb.equal(root.get("isactive"), "Active".equals(isActive.get())));
				}

				return cb.and(ls.toArray(new Predicate[0]));
			}
		};

		Sort sorttemp = new Sort(Sort.Direction.DESC, "lastModifiedOn");

		if (sortingkey.isPresent() && sortingorder.isPresent()) {
			sorttemp = new Sort(sortingorder.get().equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC,
					sortingkey.get());
		}
		if (code.isPresent() || company.isPresent() || name.isPresent() || isActive.isPresent()
				|| department.isPresent() || store.isPresent()) {
			list = pcrepo.getAllPageableProfitCenter(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		} else {
			list = pcrepo.getAllPageableProfitCenterOrdered(new PageRequest(evalPage, evalPageSize, sorttemp));
		}

		int serialid = 0;

		int pagesize = evalPage + 1;

		if (pagesize > 1) {
			int start = evalPageSize * pagesize;
			serialid = start - evalPageSize;
		}

		Pager pager = new Pager(list.getTotalPages(), list.getNumber(), BUTTONS_TO_SHOW);
		model.addAttribute("serialid", serialid);
		model.addAttribute("list", list);
		model.addAttribute("selectedPageSize", evalPageSize);
		model.addAttribute("pageSizes", PAGE_SIZES);
		model.addAttribute("pager", pager);
		model.addAttribute("totalRecords", list.getTotalElements());
		model.addAttribute("storetypelist", storeTypeforPC());
		model.addAttribute("depatlist", deptrepo.getAllActiveDepartment());
		model.addAttribute("cmplist", comrepo.getAllActiveCompany());
		model.addAttribute("pcheads", getProfitCenterMultiSelect());
		model.addAttribute("pcrights", getProfitCenterMultiSelect());
		model.addAttribute("code", code.isPresent() ? code.get() : "");
		model.addAttribute("company", company.isPresent() ? company.get() : "");
		model.addAttribute("name", name.isPresent() ? name.get() : "");
		model.addAttribute("status", isActive.isPresent() ? isActive.get() : "");
		model.addAttribute("department", department.isPresent() ? department.get() : "");
		model.addAttribute("store", store.isPresent() ? store.get() : "");
		model.addAttribute("sortingorder", sortingorder.isPresent() ? sortingorder.get() : "DESC");
		model.addAttribute("sortingkey", sortingkey.isPresent() ? sortingkey.get() : "lastModifiedOn");

		return "/master/profitcenter :: result-table";
	}

	/**
	 * Gets the store based on company.
	 *
	 * @param companycode
	 *            the companycode
	 * @return the store based on company
	 */
	@RequestMapping(value = "/master/profitcenter/storebasedoncompany", method = RequestMethod.GET)
	public @ResponseBody List<Store> getStoreBasedOnCompany(@RequestParam("companycode") String companycode) {

		String objprtypecode = companycode;
		String[] output = objprtypecode.split("-");

		return storerepo.getAllActiveStoreByCompanyCode(output[0]);
	}

	/**
	 * Creates the PC.
	 *
	 * @param obj
	 *            the obj
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @param redirectAttributes
	 *            the redirect attributes
	 * @return the string
	 * @throws ParseException
	 */
	@PostMapping(value = "/master/createprofitcenter")
	public String createPC(@Valid @ModelAttribute("pcobj") ProfitCenterForm obj, BindingResult result, Model model,
			RedirectAttributes redirectAttributes) throws ParseException {
		if (!result.hasErrors() && obj.getCompany() != null && obj.getStore() != null && obj.getDeartment() != null
				&& pcrepo.getProfitCenterByCode((obj.getStore() + obj.getDeartment())) == null) {
			ProfitCenter tobj = pojoToEntity(obj);
			String objdeptcode = obj.getDeartment();
			String[] output1 = objdeptcode.split("-");
			tobj.setCode(obj.getStore() + output1[0]);
			tobj.setCreatedBy(getCurrentUser() != null ? getCurrentUser().getUsername() : null);
			tobj.setCreatedOn(new Date());
			tobj.setIsactive(true);
			tobj.setLastModifiedBy(getCurrentUser() != null ? getCurrentUser().getUsername() : null);
			tobj.setLastModifiedOn(new Date());
			try {
				pcrepo.createProfitCenter(tobj);
				auditService.putAudit(new Date(), "Profit Center: " + obj.getPcCode() + " created successfully",
						getCurrentUser().getUsername(), "Create Profit Center");
				redirectAttributes.addFlashAttribute("info", "Profit Center created successfully");
			} catch (Exception e) {
				LOGGER.error(e);
			}
		} else {
			redirectAttributes.addFlashAttribute("error",
					"Select Company, Store and Department / Same Store and Department already exists");
		}
		return "redirect:/master/profitcenter";
	}

	/**
	 * Update profit center.
	 *
	 * @param obj
	 *            the obj
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @param redirectAttributes
	 *            the redirect attributes
	 * @return the string
	 */
	@PostMapping(value = "/master/updateprofitcenter")
	public String updateProfitCenter(@Valid @ModelAttribute("pcobj") ProfitCenterForm obj, BindingResult result,
			Model model, RedirectAttributes redirectAttributes) {
		if (!result.hasErrors() && obj.getCompany() != null && obj.getStore() != null && obj.getDeartment() != null) {
			ProfitCenter tobj = pcrepo.getProfitCenterByCode(obj.getPcCode());
			SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_FORMAT");
			String dateFormat = "dd-MM-yyyy";
			if (dateObj != null && !dateObj.getPropValue().isEmpty()) {
				dateFormat = dateObj.getPropValue();
			}
			DateFormat formatter = new SimpleDateFormat(dateFormat);
			try {
				if (tobj != null) {
					boolean isActive = true;
					tobj.setLastModifiedBy(getCurrentUser() != null ? getCurrentUser().getUsername() : null);
					tobj.setAddressLine1(obj.getAddressLine1());
					tobj.setStoreType(obj.getStoreType());
					tobj.setAddressLine2(obj.getAddressLine2());
					tobj.setAddressLine3(obj.getAddressLine3());
					tobj.setStore(storerepo.getStoreByCode(obj.getStore()));
					tobj.setCompany(comrepo.getCompanyByCode(obj.getCompany()));
					tobj.setDepartment(deptrepo.getDepartmentByCode(obj.getDeartment()));
					tobj.setViewRights(obj.getPcViewRightsto());
					tobj.setAbbrName(obj.getPcAbbrName());
					tobj.setName(obj.getPcName());
					tobj.setHead(obj.getPcHeadto());
					tobj.setPostArea(obj.getPostArea());
					tobj.setPostCode(obj.getPostCode());
					tobj.setLastModifiedOn(new Date());
					if (obj.getStartDate() != "" && !obj.getStartDate().isEmpty()) {
						tobj.setStartDate(formatter.parse(obj.getStartDate()));
					}
					if (obj.getEndDate() != "" && !obj.getEndDate().isEmpty()) {
						tobj.setEndDate(formatter.parse(obj.getEndDate()));
					}
					if (obj.getIsactive().contains("InActive")) {
						isActive = false;
					}
					if (isActive != tobj.isIsactive()) {
						if (obj.getIsactive().contains("InActive")) {
							tobj.setDeactivatedBy(getCurrentUser().getUsername());
							tobj.setDeactivatedOn(new Date());
						} else {
							tobj.setReactivatedOn(new Date());
							tobj.setReactivatedBy(getCurrentUser().getUsername());
						}
					}
					tobj.setIsactive("Active".equals(obj.getIsactive()));
					pcrepo.createProfitCenter(tobj);
					auditService.putAudit(new Date(), "Profit Center: " + obj.getPcCode() + " updated successfully",
							getCurrentUser().getUsername(), "Update Profit Center");
					redirectAttributes.addFlashAttribute("info", "Profit Center updated successfully");
				} else {
					redirectAttributes.addFlashAttribute("error", "Object does not exists");
					return "/master/edit_profitcenter";
				}
			} catch (Exception e) {
				LOGGER.error(e);
			}
		}

		return "redirect:/master/profitcenter";
	}

	/**
	 * Edits the PO user.
	 *
	 * @param pccode
	 *            the pccode
	 * @param model
	 *            the model
	 * @return the string
	 */
	@GetMapping(value = "/master/editprofitcenter/{pccode}")
	public String editPOUser(@PathVariable String pccode, Model model) {
		ProfitCenter tobj = pcrepo.getProfitCenterByCode(pccode);
		String dateFormat2 = "dd-MM-yyyy";
		SystemParameter dateObj2 = sysRepo.getSystemParameterByPropName("DATE_FORMAT");
		if (dateObj2 != null && !dateObj2.getPropValue().isEmpty()) {
			dateFormat2 = dateObj2.getPropValue();
		}
		DateFormat format = new SimpleDateFormat(dateFormat2);
		if (tobj != null) {
			model.addAttribute("pcobj", entityToPojo(tobj));
			model.addAttribute("storetypelist", storeTypeforPC());
			model.addAttribute("cmplist", comrepo.getAllActiveCompany());
			model.addAttribute("pcheads", getProfitCenterMultiSelect());
			model.addAttribute("pcrights", getProfitCenterMultiSelect());
			if (tobj.getHead() != null && !tobj.getHead().isEmpty())
				model.addAttribute("pcheadsto", Arrays.asList(tobj.getHead().split("\\s*,\\s*")));
			if (tobj.getViewRights() != null && !tobj.getViewRights().isEmpty())
				model.addAttribute("pcrightsto", Arrays.asList(tobj.getViewRights().split("\\s*,\\s*")));
			if (tobj.getCompany() != null) {
				model.addAttribute("strlist", storerepo.getAllActiveStoreByCompanyCode(tobj.getCompany().getCode()));
			} else {
				model.addAttribute("strlist", new ArrayList<>());
			}
			model.addAttribute("depatlist", deptrepo.getAllActiveDepartment());
			model.addAttribute("cmplist", comrepo.getAllActiveCompany());
			model.addAttribute("date_format", dateFormat2);
			if (tobj.getStartDate() != null) {
				model.addAttribute("start_date", format.format(tobj.getStartDate()));
			}
			if (tobj.getEndDate() != null) {
				model.addAttribute("end_date", format.format(tobj.getEndDate()));
			}
			model.addAttribute("edit", true);
			return "/master/edit_profitcenter";
		} else {
			return "redirect:/master/profitcenter";
		}
	}

	/**
	 * Entity to pojo.
	 *
	 * @param tobj
	 *            the tobj
	 * @return the profit center form
	 */
	private ProfitCenterForm entityToPojo(ProfitCenter tobj) {
		ProfitCenterForm obj = new ProfitCenterForm();
		String dateFormat = "dd-MM-yyyy hh:mm:ss";
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_TIME_FORMAT");
		if (dateObj != null && !dateObj.getPropValue().isEmpty()) {
			dateFormat = dateObj.getPropValue();
		}
		DateFormat formatter = new SimpleDateFormat(dateFormat);
		String dateFormat2 = "dd-MM-yyyy";
		SystemParameter dateObj2 = sysRepo.getSystemParameterByPropName("DATE_FORMAT");
		if (dateObj2 != null && !dateObj2.getPropValue().isEmpty()) {
			dateFormat2 = dateObj2.getPropValue();
		}
		DateFormat format = new SimpleDateFormat(dateFormat2);
		String createdOn = "";
		String modifiedOn = "";
		String reactivatedOn = "";
		String deactivatedOn = "";
		if (tobj.getCreatedOn() != null) {
			createdOn = formatter.format(tobj.getCreatedOn());
		}
		if (tobj.getLastModifiedOn() != null) {
			modifiedOn = formatter.format(tobj.getLastModifiedOn());
		}
		if (tobj.getReactivatedOn() != null) {
			reactivatedOn = formatter.format(tobj.getReactivatedOn());
		}
		if (tobj.getDeactivatedOn() != null) {
			deactivatedOn = formatter.format(tobj.getDeactivatedOn());
		}
		obj.setCreatedOn(createdOn);
		obj.setDeactivatedOn(deactivatedOn);
		obj.setReactivatedOn(reactivatedOn);
		obj.setLastModifiedOn(modifiedOn);
		obj.setPcCode(tobj.getCode());
		obj.setAddressLine1(tobj.getAddressLine1());
		obj.setAddressLine2(tobj.getAddressLine2());
		obj.setAddressLine3(tobj.getAddressLine3());
		obj.setCreatedBy(tobj.getCreatedBy());
		obj.setStoreType(tobj.getStoreType());
		obj.setDeactivatedBy(tobj.getDeactivatedBy());
		obj.setIsactive(tobj.isIsactive() ? "Active" : "InActive");
		obj.setLastModifiedBy(tobj.getLastModifiedBy());
		obj.setStore(tobj.getStore() != null ? tobj.getStore().getCode() : null);
		obj.setStoreType(tobj.getStoreType());
		obj.setCompany(tobj.getCompany() != null ? tobj.getCompany().getCode() : null);
		obj.setDeartment(tobj.getDepartment() != null ? tobj.getDepartment().getCode() : null);
		obj.setPcAbbrName(tobj.getAbbrName());
		obj.setPcHeadto(tobj.getHead());
		obj.setPcName(tobj.getName());
		obj.setPostArea(tobj.getPostArea());
		obj.setPostCode(tobj.getPostCode());
		obj.setPcViewRightsto(tobj.getViewRights());
		obj.setReactivatedBy(tobj.getReactivatedBy());
		if (tobj.getStartDate() != null) {
			obj.setStartDate("" + format.format(tobj.getStartDate()));
		}
		if (tobj.getEndDate() != null) {
			obj.setEndDate("" + format.format(tobj.getEndDate()));
		}
		return obj;
	}

	/**
	 * Pojo to entity.
	 *
	 * @param tobj
	 *            the tobj
	 * @return the profit center
	 * @throws ParseException
	 */
	private ProfitCenter pojoToEntity(ProfitCenterForm tobj) throws ParseException {
		ProfitCenter obj = new ProfitCenter();
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_FORMAT");
		String dateFormat = "dd-MM-yyyy";
		if (dateObj != null && !dateObj.getPropValue().isEmpty()) {
			dateFormat = dateObj.getPropValue();
		}
		DateFormat formatter = new SimpleDateFormat(dateFormat);
		obj.setCode(tobj.getPcCode());
		obj.setAddressLine1(tobj.getAddressLine1());
		obj.setAddressLine2(tobj.getAddressLine2());
		obj.setAddressLine3(tobj.getAddressLine3());
		obj.setCreatedBy(tobj.getCreatedBy());
		obj.setLastModifiedBy(tobj.getLastModifiedBy());
		obj.setStore(storerepo.getStoreByCode(tobj.getStore()));
		obj.setStoreType(tobj.getStoreType());
		String objprtypecode = tobj.getCompany();
		String[] output = objprtypecode.split("-");
		obj.setCompany(comrepo.getCompanyByCode(output[0]));

		String objdeptcode = tobj.getDeartment();
		String[] output1 = objdeptcode.split("-");

		obj.setDepartment(deptrepo.getDepartmentByCode(output1[0]));
		obj.setAbbrName(tobj.getPcAbbrName());
		obj.setHead(tobj.getPcHeadto());
		obj.setName(tobj.getPcName());
		obj.setPostArea(tobj.getPostArea());
		obj.setPostCode(tobj.getPostCode());
		obj.setViewRights(tobj.getPcViewRightsto());
		obj.setReactivatedBy(tobj.getReactivatedBy());

		if (!tobj.getEndDate().isEmpty()) {
			obj.setEndDate(formatter.parse(tobj.getEndDate()));
		}

		if (!tobj.getStartDate().isEmpty()) {
			obj.setStartDate(formatter.parse(tobj.getStartDate()));
		}

		return obj;
	}

	@GetMapping(value = "/master/deleteprofitcenter/{code}")
	public String deletePc(@PathVariable String code, Model model) {

		ProfitCenter ad = pcrepo.getProfitCenterByCode(code);
		if (ad != null) {
			pcrepository.delete(ad);

		}
		return "redirect:/master/profitcenter";
	}

	/**
	 * Store typefor PC.
	 *
	 * @return the list
	 */
	private List<String> storeTypeforPC() {
		List<String> lis = new ArrayList<>();
		lis.add("S");
		lis.add("W");
		lis.add("H");
		lis.add("O");
		return lis;
	}

	private List<String> getProfitCenterMultiSelect() {
		List<String> lis = new ArrayList<>();
		Page<EmployeeMaster> list = empservice.getAllPagableEmployee(new PageRequest(0, 20));
		for (EmployeeMaster e : list.getContent()) {
			StringBuilder s = new StringBuilder();
			s.append(e.getPsId());
			s.append("-");
			s.append(e.getName());
			s.append("(");
			s.append(e.getPosTitle());
			s.append(")");
			lis.add(s.toString());
		}
		return lis;
	}

	@Override
	protected String getHeaderTitle() {
		return "Manage ProfitCenter";
	}

	/**
	 * Autocomplete suggestions.
	 *
	 * @param searchstr
	 *            the searchstr
	 * @return the list
	 */
	@RequestMapping(value = "/master/profitcenter/companyautocomplete", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<AutocompleteForm> autocompleteSuggestions(@RequestParam("query") String searchstr) {
		List<AutocompleteForm> lis = new ArrayList<>();
		for (Company m : comrepo.findBySearchCode(searchstr)) {
			AutocompleteForm a = new AutocompleteForm();
			a.setId(m.getName());
			a.setName(m.getCode());
			lis.add(a);
		}
		return lis;
	}

	/**
	 * Gets the store based on company.
	 *
	 * @param companycode
	 *            the companycode
	 * @return the store based on company
	 */
	@RequestMapping(value = "/master/profitcenter/existingprofitcenter", method = RequestMethod.GET)
	public @ResponseBody ProfitCenter checkCompanyHRIS(@RequestParam("pccode") String pccode,
			RedirectAttributes redirectAttributes, Model model) {
		return pcrepo.getProfitCenterByCode(pccode);
	}

	/**
	 * 
	 * @param searchval
	 * @param redirectAttributes
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/master/profitcenter/employeesearch", method = RequestMethod.GET)
	public @ResponseBody List<String> employeeSearch(@RequestParam("searchval") String searchval,
			RedirectAttributes redirectAttributes, Model model) {
		Specification<EmployeeMaster> sp = new Specification<EmployeeMaster>() {
			@Override
			public Predicate toPredicate(Root<EmployeeMaster> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> ls = new ArrayList<>();
				ls.add(cb.like(cb.lower(root.get("psId")), "%" + searchval.toLowerCase() + "%"));
				ls.add(cb.like(cb.lower(root.get("name")), "%" + searchval.toLowerCase() + "%"));
				ls.add(cb.like(cb.lower(root.get("posTitle")), "%" + searchval.toLowerCase() + "%"));
				return cb.or(ls.toArray(new Predicate[0]));
			}
		};
		Page<EmployeeMaster> list;
		if (searchval != null && !searchval.isEmpty()) {
			list = empservice.getAllPagableEmployee(sp, new PageRequest(0, 20));
		} else {
			list = empservice.getAllPagableEmployee(new PageRequest(0, 20));
		}
		List<String> temp = new ArrayList<>();
		for (EmployeeMaster e : list.getContent()) {
			StringBuilder s = new StringBuilder();
			s.append(e.getPsId());
			s.append("-");
			s.append(e.getName());
			s.append("(");
			s.append(e.getPosTitle());
			s.append(")");
			temp.add(s.toString());
		}
		return temp;
	}

	private List<String> getOrderCodeAndCompName() {
		List<String> lis = new ArrayList<>();
		List<Company> temp = cmpservice.getAllActiveCompany();
		if (temp != null) {
			for (Company e : temp) {
				StringBuilder s = new StringBuilder();
				s.append(e.getCode());
				s.append("-");
				s.append(e.getName());
				lis.add(s.toString());
			}
		}
		return lis;
	}

	private List<String> getCodeAndDempName() {
		List<String> lis = new ArrayList<>();
		List<Department> temp = deptrepo.getAllActiveDepartment();
		if (temp != null) {
			for (Department e : temp) {
				StringBuilder s = new StringBuilder();
				s.append(e.getCode());
				s.append("-");
				s.append(e.getName());
				lis.add(s.toString());
			}
		}
		return lis;
	}

}
