/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.poadmin.security;

/**
 * The Class SecurityUtil.
 */
public final class SecurityUtil {
	
	public static final String MANAGE_AUDIT = "ROLE_MANAGE_AUDIT";
	public static final String MANAGE_COMPANY = "ROLE_MANAGE_COMPANY";
	public static final String MANAGE_DEPARTMENT = "ROLE_MANAGE_DEPARTMENT";
	public static final String MANAGE_EMPLOYEE_MANAGEMENT = "ROLE_MANAGE_EMPLOYEE_MANAGEMENT";
	public static final String MANAGE_GST = "ROLE_MANAGE_GST";
	public static final String MANAGE_ORDER = "ROLE_MANAGE_ORDER";
	public static final String MANAGE_ROLE = "ROLE_MANAGE_ROLE";
	public static final String MANAGE_PERMISSION = "ROLE_MANAGE_PERMISSION";
	public static final String MANAGE_SETTING = "ROLE_MANAGE_SETTING";
	public static final String MANAGE_PROFIT_CENTER = "ROLE_MANAGE_PROFIT_CENTER";
	public static final String MANAGE_PURCHASE = "ROLE_MANAGE_PURCHASE";
	public static final String MANAGE_SCHEDULER = "ROLE_MANAGE_SCHEDULER";
	public static final String MANAGE_STORE = "ROLE_MANAGE_STORE";
	public static final String MANAGE_SYSTEM_PARAMETER = "ROLE_MANAGE_SYSTEM_PARAMETER";
	public static final String MANAGE_USER = "ROLE_MANAGE_USER";
	public static final String MANAGE_MASTER_DATA = "ROLE_MASTER_DATA";

	/**
	 * Instantiates a new security util.
	 */
	private SecurityUtil() {
		super();
	}

}
