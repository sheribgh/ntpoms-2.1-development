/*
 * ********************************************************************************
 * Copyright (c) 2018 - Brentin Services, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.brent-in.com
 * @Copyright  : Copyright 2018 Brentin Services, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.poadmin.web.controllers;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.UriUtils;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.common.entities.MasterData;
import my.com.parkson.ntpoms.common.entities.SystemParameter;
import my.com.parkson.ntpoms.common.repositories.MasterDataRepository;
import my.com.parkson.ntpoms.common.repositories.SystemParameterRepository;
import my.com.parkson.ntpoms.common.services.AuditService;
import my.com.parkson.ntpoms.common.services.MasterDataService;
import my.com.parkson.ntpoms.common.services.MessageService;
import my.com.parkson.ntpoms.common.services.SystemParameterService;
import my.com.parkson.ntpoms.poadmin.security.SecurityUtil;
import my.com.parkson.ntpoms.poadmin.web.models.MasterDataForm;
import my.com.parkson.ntpoms.poadmin.web.models.Pager;
import my.com.parkson.ntpoms.poadmin.web.models.SystemParamForm;

@Slf4j
@Controller
@Secured(SecurityUtil.MANAGE_SYSTEM_PARAMETER)
@RequiredArgsConstructor
public class SystemParameterController extends AbstractAdminController {

	private static Logger LOGGER = Logger.getLogger(SystemParameterController.class);

	/** The log. */
	@Autowired
	private AuditService auditService;

	/** The messages. */
	@Autowired
	private MessageService messages;

	@Autowired
	private MasterDataService masterrepo;

	@Autowired
	private MasterDataRepository masterrepository;

	@Autowired
	private SystemParameterService systemrepo;

	@Autowired
	private SystemParameterRepository systemrepository;

	private static final int BUTTONS_TO_SHOW = 5;
	private static final int INITIAL_PAGE = 0;
	private static final int INITIAL_PAGE_SIZE = 5;
	private static final int[] PAGE_SIZES = { 5, 10, 20 };
	boolean edit = false;

	private MasterDataForm tempobj;

	private SystemParamForm systempobj;

	/**
	 * On page load master data.
	 *
	 * @param pageSize
	 *            the page size
	 * @param page
	 *            the page
	 * @param codeType
	 *            the code type
	 * @param codeValue
	 *            the code value
	 * @param isActive
	 *            the is active
	 * @return the model and view
	 */
	@GetMapping("/system/masterdata")
	public ModelAndView onPageLoadMasterData() {
		ModelAndView modelAndView = new ModelAndView("/system/masterdata");

		modelAndView.getModelMap().addAttribute("masterobj", tempobj != null ? tempobj : new MasterDataForm());
		return modelAndView;
	}

	@RequestMapping(value = "/system/masterdatagrid", method = RequestMethod.GET)
	public String masterdataGrid(@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page, @RequestParam("codeType") Optional<String> codeType,
			@RequestParam("codeValue") Optional<String> codeValue, @RequestParam("status") Optional<String> isActive,
			@RequestParam("sortingkey") Optional<String> sortingkey,
			@RequestParam("sortingorder") Optional<String> sortingorder, Model model) {

		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
		Page<MasterData> list;
		Specification<MasterData> s = new Specification<MasterData>() {
			@Override
			public Predicate toPredicate(Root<MasterData> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> ls = new ArrayList<>();
				if (codeType.isPresent() && !codeType.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("codeType")), "%" + codeType.get().toLowerCase() + "%"));
				}
				if (codeValue.isPresent() && !codeValue.get().isEmpty()) {

					ls.add(cb.like(cb.lower(root.get("codeValue")), "%" + codeValue.get().toLowerCase() + "%"));
				}

				if (isActive.isPresent() && !isActive.get().isEmpty() && !isActive.get().equals("All")) {
					ls.add(cb.equal(root.get("isActive"), "Active".equals(isActive.get())));
				}

				return cb.and(ls.toArray(new Predicate[0]));
			}
		};
		Sort sorttemp = new Sort(Sort.Direction.DESC, "lastModifiedOn");

		if (sortingkey.isPresent() && sortingorder.isPresent()) {
			sorttemp = new Sort(sortingorder.get().equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC,
					sortingkey.get());
		}
		if (codeType.isPresent() || codeValue.isPresent() || isActive.isPresent()) {
			list = masterrepo.getAllPageableMasterData(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		} else {
			list = masterrepo.getAllPageableMasterDataOrdered(new PageRequest(evalPage, evalPageSize, sorttemp));
		}

		int serialid = 0;

		int pagesize = evalPage + 1;

		if (pagesize > 1) {
			int start = evalPageSize * pagesize;
			serialid = start - evalPageSize;
		}

		Pager pager = new Pager(list.getTotalPages(), list.getNumber(), BUTTONS_TO_SHOW);
		model.addAttribute("serialid", serialid);
		model.addAttribute("list", list);
		model.addAttribute("selectedPageSize", evalPageSize);
		model.addAttribute("pageSizes", PAGE_SIZES);
		model.addAttribute("pager", pager);
		model.addAttribute("totalRecords", list.getTotalElements());
		model.addAttribute("codeType", codeType.isPresent() ? codeType.get() : "");
		model.addAttribute("codeValue", codeValue.isPresent() ? codeValue.get() : "");
		model.addAttribute("status", isActive.isPresent() ? isActive.get() : "");
		model.addAttribute("sortingorder", sortingorder.isPresent() ? sortingorder.get() : "DESC");
		model.addAttribute("sortingkey", sortingkey.isPresent() ? sortingkey.get() : "lastModifiedOn");

		return "/system/masterdata :: result-table";

	}

	/**
	 * Creates the master data parameter.
	 *
	 * @param obj
	 *            the obj
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @param redirectAttributes
	 *            the redirect attributes
	 * @return the string
	 */
	@PostMapping(value = "/system/createmasterdata")
	public String createMasterDataParameter(@Valid @ModelAttribute("masterobj") MasterDataForm obj,
			BindingResult result, Model model, RedirectAttributes redirectAttributes) {
		if (!result.hasErrors()) {
			MasterData ad = new MasterData();
			ad.setCreatedOn(new Date());
			ad.setLastModifiedBy(getCurrentUser() != null ? getCurrentUser().getUsername() : null);
			ad.setLastModifiedOn(new Date());
			ad.setCreatedBy(getCurrentUser() != null ? getCurrentUser().getUsername() : null);
			ad.setActive(true);
			ad.setCodeDesc(obj.getCodeDesc());
			ad.setCodeType(obj.getCodeType());
			ad.setCodeValue(obj.getCodeValue());
			ad.setSysName(obj.getSysName());

			MasterData master = masterrepo.getMasterDataByCodeType(ad.getCodeType());
			if (master != null) {

				redirectAttributes.addFlashAttribute("error", "Code Type already exists!");
				tempobj = null;

			} else {
				try {
					masterrepo.createMasterData(ad);
					auditService.putAudit(new Date(), "Master Data: " + obj.getCodeType() + " created successfully",
							getCurrentUser().getUsername(), "Create Master Data");
					redirectAttributes.addFlashAttribute("info", "Master Data updated successfully");
					tempobj = null;
				} catch (Exception e) {
					LOGGER.error(e);
				}
			}

		} else {
			tempobj = obj;
		}
		return "redirect:/system/masterdata";
	}

	/**
	 * Edits the master data parameter.
	 *
	 * @param code
	 *            the code
	 * @param model
	 *            the model
	 * @return the string
	 */
	@GetMapping(value = "/system/editmasterdata/{code}")
	public String editMasterDataParameter(@PathVariable int code, Model model) {
		MasterData tobj = masterrepo.getMasterDataById(code);
		edit = true;
		if (tobj != null) {
			MasterDataForm obj = new MasterDataForm();
			String date_format = "dd-MM-yyyy hh:mm:ss";
			SystemParameter dateObj = systemrepo.getSystemParameterByPropName("DATE_TIME_FORMAT");

			if (dateObj != null) {
				if (!dateObj.getPropValue().isEmpty()) {
					date_format = dateObj.getPropValue();
				}
			}
			DateFormat formatter = new SimpleDateFormat(date_format);

			String createdOn = "";
			String modifiedOn = "";
			String reactivatedOn = "";
			String deactivatedOn = "";
			if (tobj.getCreatedOn() != null) {
				createdOn = formatter.format(tobj.getCreatedOn());
			}
			if (tobj.getLastModifiedOn() != null) {
				modifiedOn = formatter.format(tobj.getLastModifiedOn());
			}
			if (tobj.getReactivatedOn() != null) {
				reactivatedOn = formatter.format(tobj.getReactivatedOn());
			}
			if (tobj.getDeactivatedOn() != null) {
				deactivatedOn = formatter.format(tobj.getDeactivatedOn());
			}
			obj.setCreatedon(createdOn);
			obj.setDeactivatedOn(deactivatedOn);
			obj.setReactivatedOn(reactivatedOn);
			obj.setUpdatedon(modifiedOn);
			obj.setCodeDesc(tobj.getCodeDesc());
			obj.setCodeType(tobj.getCodeType());
			obj.setCodeValue(tobj.getCodeValue());
			obj.setMastDataId(tobj.getId());
			obj.setCreatedby(tobj.getCreatedBy());
			obj.setUpdatedby(tobj.getLastModifiedBy());
			obj.setDeactivatedBy(tobj.getDeactivatedBy());
			obj.setReactivatedBy(tobj.getReactivatedBy());
			obj.setSysName(tobj.getSysName());
			obj.setIsActive(tobj.isActive() ? "Active" : "InActive");
			model.addAttribute("masterobj", obj);
			model.addAttribute("edit", edit);
			return "/system/edit_masterdata";

		} else {
			return "redirect:/system/masterdata";
		}
	}

	/**
	 * Update master data parameter.
	 *
	 * @param obj
	 *            the obj
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @param redirectAttributes
	 *            the redirect attributes
	 * @return the string
	 */
	@PostMapping(value = "/system/updatemasterdata")
	public String updateMasterDataParameter(@Valid @ModelAttribute("masterobj") MasterDataForm obj,
			BindingResult result, Model model, RedirectAttributes redirectAttributes) {
		if (!result.hasErrors()) {
			MasterData ad = masterrepo.getMasterDataById(obj.getMastDataId());
			boolean isActive;
			if (ad != null) {
				ad.setLastModifiedBy(getCurrentUser() != null ? getCurrentUser().getUsername() : null);
				ad.setLastModifiedOn(new Date());
				if (obj.getIsActive().contains("InActive")) {
					isActive = false;
				} else {
					isActive = true;
				}

				if (isActive != ad.isActive()) {

					if (obj.getIsActive().contains("InActive")) {
						ad.setDeactivatedBy(getCurrentUser().getUsername());
						ad.setDeactivatedOn(new Date());
					} else {
						ad.setReactivatedOn(new Date());
						ad.setReactivatedBy(getCurrentUser().getUsername());
					}
				}

				ad.setActive("Active".equals(obj.getIsActive()));
				ad.setCodeType(obj.getCodeType());
				ad.setCodeDesc(obj.getCodeDesc());
				ad.setCodeValue(obj.getCodeValue());
				ad.setSysName(obj.getSysName());

				if (ad.getCodeType() != obj.getCodeType()) {

					MasterData master = masterrepo.getMasterDataByCodeType(obj.getCodeType());

					if (master != null && obj.getMastDataId() != master.getId()) {

						redirectAttributes.addFlashAttribute("error", "Code type already exists!");
					} else {
						try {
							masterrepo.createMasterData(ad);
							auditService.putAudit(new Date(),
									"Master Data: " + obj.getCodeType() + " updated successfully",
									getCurrentUser().getUsername(), "Update Master Data");
						} catch (Exception e) {
							LOGGER.error(e);
						}
						redirectAttributes.addFlashAttribute("info", "Master Data updated successfully");
					}

				} else {
					ad.setCodeType(obj.getCodeType());
					try {
						masterrepo.createMasterData(ad);
						auditService.putAudit(new Date(), "Master Data: " + obj.getCodeType() + " updated successfully",
								getCurrentUser().getUsername(), "Update Master Data");
					} catch (Exception e) {
						LOGGER.error(e);
					}
					redirectAttributes.addFlashAttribute("info", "Master Data updated successfully");

				}

			} else {
				redirectAttributes.addFlashAttribute("error", "Object not exists");
				return "/system/edit_masterdata";
			}
		}
		return "redirect:/system/masterdata";
	}

	@GetMapping(value = "/master/deletemasterdata/{code}")
	public String deletemasterdata(@PathVariable String code, Model model) {
		MasterData ad = masterrepo.getMasterDataByCodeType(code);
		if (ad != null) {
			masterrepository.delete(ad);
		}

		return "redirect:/system/masterdata";
	}

	/**
	 * On page load system param.
	 *
	 * @param pageSize
	 *            the page size
	 * @param page
	 *            the page
	 * @param propName
	 *            the prop name
	 * @param propValue
	 *            the prop value
	 * @param isActive
	 *            the is active
	 * @return the model and view
	 */
	@GetMapping("/system/parameter")
	public ModelAndView onPageLoadSystemParam() {
		ModelAndView modelAndView = new ModelAndView("/system/parameter");

		modelAndView.getModelMap().addAttribute("paramobj", systempobj != null ? systempobj : new SystemParamForm());

		return modelAndView;
	}

	@RequestMapping(value = "/system/parametergrid", method = RequestMethod.GET)
	public String systemGrid(@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page, @RequestParam("propName") Optional<String> propName,
			@RequestParam("propValue") Optional<String> propValue, @RequestParam("status") Optional<String> isActive,
			@RequestParam("sortingkey") Optional<String> sortingkey,
			@RequestParam("sortingorder") Optional<String> sortingorder, Model model) {

		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
		Page<SystemParameter> list;
		Specification<SystemParameter> s = new Specification<SystemParameter>() {
			@Override
			public Predicate toPredicate(Root<SystemParameter> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> ls = new ArrayList<>();
				if (propName.isPresent() && !propName.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("propName")), "%" + propName.get().toLowerCase() + "%"));
				}
				if (propValue.isPresent() && !propValue.get().isEmpty()) {

					ls.add(cb.like(cb.lower(root.get("propValue")), "%" + propValue.get().toLowerCase() + "%"));
				}

				if (isActive.isPresent() && !isActive.get().isEmpty() && !isActive.get().equals("All")) {
					ls.add(cb.equal(root.get("isActive"), "Active".equals(isActive.get())));
				}

				return cb.and(ls.toArray(new Predicate[0]));
			}
		};
		Sort sorttemp = new Sort(Sort.Direction.DESC, "lastModifiedOn");

		if (sortingkey.isPresent() && sortingorder.isPresent()) {
			sorttemp = new Sort(sortingorder.get().equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC,
					sortingkey.get());
		}
		if (propName.isPresent() || propValue.isPresent() || isActive.isPresent()) {
			list = systemrepo.getAllPageableSystemParameter(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		} else {
			list = systemrepo.getAllPageableSystemParameterOrdered(new PageRequest(evalPage, evalPageSize, sorttemp));
		}

		int serialid = 0;

		int pagesize = evalPage + 1;

		if (pagesize > 1) {
			int start = evalPageSize * pagesize;
			serialid = start - evalPageSize;
		}

		Pager pager = new Pager(list.getTotalPages(), list.getNumber(), BUTTONS_TO_SHOW);
		model.addAttribute("serialid", serialid);
		model.addAttribute("list", list);
		model.addAttribute("selectedPageSize", evalPageSize);
		model.addAttribute("pageSizes", PAGE_SIZES);
		model.addAttribute("pager", pager);
		model.addAttribute("totalRecords", list.getTotalElements());

		model.addAttribute("propName", propName.isPresent() ? propName.get() : "");
		model.addAttribute("propValue", propValue.isPresent() ? propValue.get() : "");

		model.addAttribute("status", isActive.isPresent() ? isActive.get() : "");
		model.addAttribute("sortingorder", sortingorder.isPresent() ? sortingorder.get() : "DESC");
		model.addAttribute("sortingkey", sortingkey.isPresent() ? sortingkey.get() : "lastModifiedOn");
		return "/system/parameter :: result-table";
	}

	/**
	 * Creates the parameter.
	 *
	 * @param obj
	 *            the obj
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @param redirectAttributes
	 *            the redirect attributes
	 * @return the string
	 */
	@PostMapping(value = "/system/createparameter")
	public String createParameter(@Valid @ModelAttribute("paramobj") SystemParamForm obj, BindingResult result,
			Model model, RedirectAttributes redirectAttributes) {
		if (!result.hasErrors()) {
			SystemParameter ad = new SystemParameter();
			ad.setCreatedOn(new Date());
			ad.setCreatedBy(getCurrentUser() != null ? getCurrentUser().getUsername() : null);
			ad.setLastModifiedBy(getCurrentUser() != null ? getCurrentUser().getUsername() : null);
			ad.setLastModifiedOn(new Date());
			ad.setPropName(obj.getPropName());
			ad.setPropValue(obj.getPropValue());
			ad.setSysName(obj.getSysName());

			ad.setDesc(obj.getDesc());
			ad.setActive(true);
			try { // add try catch
				if (systemrepo.getSystemParameterByPropName(obj.getPropName()) != null) {
					redirectAttributes.addFlashAttribute("error", "System Prop Name already exists.");
					systempobj = null;
					LOGGER.info("System Prop Name already exists.");
				} else {
					systemrepo.createSystemParameter(ad);
					auditService.putAudit(new Date(),
							"System Parameter: " + obj.getPropName() + " created successfully",
							getCurrentUser().getUsername(), "Create System Parameter");
					redirectAttributes.addFlashAttribute("info", "System Parameter created successfully");
					systempobj = null;

				}
			} catch (Exception e) {
				LOGGER.error(e);
			}

		} else {
			systempobj = obj;
		}
		return "redirect:/system/parameter";
	}

	/**
	 * Edits the master.
	 *
	 * @param syscode
	 *            the syscode
	 * @param model
	 *            the model
	 * @return the string
	 */
	@GetMapping(value = "/system/editparameter/{syscode}")
	public String editMaster(@PathVariable int syscode, Model model) {
		SystemParameter tobj = systemrepo.getSystemParameterByCode(syscode);
		edit = true;
		if (tobj != null) {
			SystemParamForm obj = new SystemParamForm();
			String date_format = "dd-MM-yyyy hh:mm:ss";
			SystemParameter dateObj = systemrepo.getSystemParameterByPropName("DATE_TIME_FORMAT");

			if (dateObj != null) {
				if (!dateObj.getPropValue().isEmpty()) {
					date_format = dateObj.getPropValue();
				}
			}
			DateFormat formatter = new SimpleDateFormat(date_format);
			String createdOn = "";
			String modifiedOn = "";
			String reactivatedOn = "";
			String deactivatedOn = "";
			if (tobj.getCreatedOn() != null) {
				createdOn = formatter.format(tobj.getCreatedOn());
			}
			if (tobj.getLastModifiedOn() != null) {
				modifiedOn = formatter.format(tobj.getLastModifiedOn());
			}
			if (tobj.getReactivatedOn() != null) {
				reactivatedOn = formatter.format(tobj.getReactivatedOn());
			}
			if (tobj.getDeactivatedOn() != null) {
				deactivatedOn = formatter.format(tobj.getDeactivatedOn());
			}
			obj.setCreatedon(createdOn);
			obj.setDeactivatedOn(deactivatedOn);
			obj.setReactivatedOn(reactivatedOn);
			obj.setUpdatedon(modifiedOn);
			obj.setDesc(tobj.getDesc());
			obj.setIsActive(tobj.isActive() ? "Active" : "InActive");
			obj.setPropName(tobj.getPropName());
			obj.setPropValue(tobj.getPropValue());
			obj.setSysName(tobj.getSysName());
			obj.setCreatedby(tobj.getCreatedBy());
			obj.setUpdatedby(tobj.getLastModifiedBy());
			obj.setDeactivatedBy(tobj.getDeactivatedBy());
			obj.setReactivatedBy(tobj.getReactivatedBy());
			obj.setSysParamCode(tobj.getCode());
			model.addAttribute("paramobj", obj);
			model.addAttribute("edit", edit);
			return "/system/edit_parameter";
		} else {
			return "redirect:/system/parameter";
		}
	}

	@GetMapping(value = "/master/deleteparameter/{code}")
	public String deleteparameter(@PathVariable int syscode, Model model) {
		SystemParameter ad = systemrepo.getSystemParameterByCode(syscode);
		if (ad != null) {
			systemrepository.delete(ad);
		}

		return "redirect:/system/parameter";
	}

	/**
	 * Update parameter.
	 *
	 * @param obj
	 *            the obj
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @param redirectAttributes
	 *            the redirect attributes
	 * @return the string
	 */
	@PostMapping(value = "/system/updateparameter")
	public String updateParameter(@Valid @ModelAttribute("paramobj") SystemParamForm obj, BindingResult result,
			Model model, RedirectAttributes redirectAttributes) {
		if (!result.hasErrors()) {
			SystemParameter tobj = systemrepo.getSystemParameterByCode(obj.getSysParamCode());
			boolean isActive;
			try { // add this try catch
				if (tobj != null) {
					tobj.setLastModifiedBy(getCurrentUser() != null ? getCurrentUser().getUsername() : null);
					tobj.setLastModifiedOn(new Date());
					tobj.setPropName(obj.getPropName());
					tobj.setPropValue(obj.getPropValue());
					tobj.setSysName(obj.getSysName());
					tobj.setDesc(obj.getDesc());
					if (obj.getIsActive().contains("InActive")) {
						isActive = false;
					} else {
						isActive = true;
					}

					if (isActive != tobj.isActive()) {

						if (obj.getIsActive().contains("InActive")) {
							tobj.setDeactivatedBy(getCurrentUser().getUsername());
							tobj.setDeactivatedOn(new Date());
						} else {
							tobj.setReactivatedOn(new Date());
							tobj.setReactivatedBy(getCurrentUser().getUsername());
						}
					}
					tobj.setActive("Active".equals(obj.getIsActive()));
					systemrepo.createSystemParameter(tobj);
					redirectAttributes.addFlashAttribute("info", "System Parameter updated successfully");
					auditService.putAudit(new Date(),
							"System Parameter: " + obj.getPropName() + " updated successfully",
							getCurrentUser().getUsername(), "Update System Parameter");

				} else {
					redirectAttributes.addFlashAttribute("error", "Object does not exist");
					LOGGER.info("Object does not exist");
					return "/system/edit_parameter";
				}
			} catch (Exception e) {
				LOGGER.error(e);
			}
		}
		return "redirect:/system/parameter";
	}

	@Override
	protected String getHeaderTitle() {
		return "Manage System";
	}

	/**
	 * Gets the store based on company.
	 *
	 * @param companycode
	 *            the companycode
	 * @return the store based on company
	 */
	@RequestMapping(value = "/system/masterdata/existingmaster", method = RequestMethod.GET)
	public @ResponseBody MasterData checkOrderSub(@RequestParam("Mastercode") String Mastercode,
			RedirectAttributes redirectAttributes, Model model) {

		return masterrepo.getMasterDataByCodeType(Mastercode);

	}

	/**
	 * Gets the store based on company.
	 *
	 * @param companycode
	 *            the companycode
	 * @return the store based on company
	 */
	@RequestMapping(value = "/system/parameter/existingparamname", method = RequestMethod.GET)
	public @ResponseBody SystemParameter checksysname(@RequestParam("sysname") String sysname,
			RedirectAttributes redirectAttributes, Model model) {

		return systemrepo.getSystemParameterByPropName(sysname);

	}

	/**
	 * Gets the store based on company.
	 *
	 * @param companycode
	 *            the companycode
	 * @return the store based on company
	 */
	@RequestMapping(value = "/system/parameter/existingparamvalue", method = RequestMethod.GET)
	public @ResponseBody SystemParameter checksysvalue(@RequestParam("propvalue") String propvalue,
			RedirectAttributes redirectAttributes, Model model) {

		String decodeValue = "";
		try {
			decodeValue = UriUtils.decode(propvalue, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			LOGGER.error(e);
		}
		return systemrepo.getSystemParameterByPropValue(decodeValue);

	}

}
