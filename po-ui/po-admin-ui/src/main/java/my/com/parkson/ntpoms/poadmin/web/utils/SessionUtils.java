/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.poadmin.web.utils;

import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.poadmin.security.AuthenticatedUser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class SessionUtils {
	
	private final Logger log = LoggerFactory.getLogger(SessionUtils.class);
	
    @Autowired
    private SessionRegistry sessionRegistry;

    /**
     * Prints the session.
     */
    public void printSession() {
        for (Object principal : sessionRegistry.getAllPrincipals()) {
            if (principal instanceof AuthenticatedUser) {
                UserDetails userDetails = (AuthenticatedUser) principal;
                for (SessionInformation information : sessionRegistry.getAllSessions(userDetails, true)) {
                    log.info("####### SESSION User : {} - isExpired:{} - sessionId:{}",
                            userDetails.getUsername(),
                            information.isExpired(),
                            information.getSessionId());
                }
            }
        }
    }

    /**
     * Expire user sessions.
     *
     * @param username the username
     */
    public void expireUserSessions(String username) {
        for (Object principal : sessionRegistry.getAllPrincipals()) {
            if (principal instanceof AuthenticatedUser) {
                UserDetails userDetails = (AuthenticatedUser) principal;
                if (userDetails.getUsername().equals(username)) {
                    for (SessionInformation information : sessionRegistry.getAllSessions(userDetails, true)) {
                        information.expireNow();
                    }
                }
            }
        }
    }
}