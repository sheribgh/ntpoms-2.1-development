package my.com.parkson.ntpoms.poadmin.web.scheduler;

public interface IFileTypeMaster {
	public String process(String filename);
	public boolean upsert(String[] dataItem);
}