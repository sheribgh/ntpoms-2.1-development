/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.poadmin.web.models;

import java.io.Serializable;

public class CompanyForm implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String compCode;

	private String compCodeHRIS;

	private String compAbbrName;

	private String compRegNo;

	private String compName;

	private String compLogo;

	private String isActive;
	
	private String createdOn;

	private String createdBy;

	private String lastModifiedOn;

	private String lastModifiedBy;

	private String companyLetterHeading;

	private String poTAndCPortrait;

	private String poTAndCLandscape;

	private String deactivatedBy;
	
	private String deactivatedOn;

	private String reactivatedBy;

	private String reactivatedOn;

	/**
	 * @return the compCode
	 */
	public String getCompCode() {
		return compCode;
	}

	/**
	 * @param compCode
	 *            the compCode to set
	 */
	public void setCompCode(String compCode) {
		this.compCode = compCode;
	}

	/**
	 * @return the compCodeHRIS
	 */
	public String getCompCodeHRIS() {
		return compCodeHRIS;
	}

	/**
	 * @param compCodeHRIS
	 *            the compCodeHRIS to set
	 */
	public void setCompCodeHRIS(String compCodeHRIS) {
		this.compCodeHRIS = compCodeHRIS;
	}

	/**
	 * @return the compAbbrName
	 */
	public String getCompAbbrName() {
		return compAbbrName;
	}

	/**
	 * @param compAbbrName
	 *            the compAbbrName to set
	 */
	public void setCompAbbrName(String compAbbrName) {
		this.compAbbrName = compAbbrName;
	}

	/**
	 * @return the compRegNo
	 */
	public String getCompRegNo() {
		return compRegNo;
	}

	/**
	 * @param compRegNo
	 *            the compRegNo to set
	 */
	public void setCompRegNo(String compRegNo) {
		this.compRegNo = compRegNo;
	}

	/**
	 * @return the compName
	 */
	public String getCompName() {
		return compName;
	}

	/**
	 * @param compName
	 *            the compName to set
	 */
	public void setCompName(String compName) {
		this.compName = compName;
	}

	/**
	 * @return the compLogo
	 */
	public String getCompLogo() {
		return compLogo;
	}

	/**
	 * @param compLogo
	 *            the compLogo to set
	 */
	public void setCompLogo(String compLogo) {
		this.compLogo = compLogo;
	}

	/**
	 * @return the isActive
	 */
	public String getIsActive() {
		return isActive;
	}

	/**
	 * @param isActive
	 *            the isActive to set
	 */
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	/**
	 * @return the createdOn
	 */
	public String getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn
	 *            the createdOn to set
	 */
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the lastModifiedOn
	 */
	public String getLastModifiedOn() {
		return lastModifiedOn;
	}

	/**
	 * @param lastModifiedOn
	 *            the lastModifiedOn to set
	 */
	public void setLastModifiedOn(String lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

	/**
	 * @return the lastModifiedBy
	 */
	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	/**
	 * @param lastModifiedBy
	 *            the lastModifiedBy to set
	 */
	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	/**
	 * @return the companyLetterHeading
	 */
	public String getCompanyLetterHeading() {
		return companyLetterHeading;
	}

	/**
	 * @param companyLetterHeading
	 *            the companyLetterHeading to set
	 */
	public void setCompanyLetterHeading(String companyLetterHeading) {
		this.companyLetterHeading = companyLetterHeading;
	}

	/**
	 * @return the poTAndCPortrait
	 */
	public String getPoTAndCPortrait() {
		return poTAndCPortrait;
	}

	/**
	 * @param poTAndCPortrait
	 *            the poTAndCPortrait to set
	 */
	public void setPoTAndCPortrait(String poTAndCPortrait) {
		this.poTAndCPortrait = poTAndCPortrait;
	}

	/**
	 * @return the poTAndCLandscape
	 */
	public String getPoTAndCLandscape() {
		return poTAndCLandscape;
	}

	/**
	 * @param poTAndCLandscape
	 *            the poTAndCLandscape to set
	 */
	public void setPoTAndCLandscape(String poTAndCLandscape) {
		this.poTAndCLandscape = poTAndCLandscape;
	}

	/**
	 * @return the deactivatedBy
	 */
	public String getDeactivatedBy() {
		return deactivatedBy;
	}

	/**
	 * @param deactivatedBy
	 *            the deactivatedBy to set
	 */
	public void setDeactivatedBy(String deactivatedBy) {
		this.deactivatedBy = deactivatedBy;
	}

	/**
	 * @return the deactivatedOn
	 */
	public String getDeactivatedOn() {
		return deactivatedOn;
	}

	/**
	 * @param deactivatedOn
	 *            the deactivatedOn to set
	 */
	public void setDeactivatedOn(String deactivatedOn) {
		this.deactivatedOn = deactivatedOn;
	}

	/**
	 * @return the reactivatedBy
	 */
	public String getReactivatedBy() {
		return reactivatedBy;
	}

	/**
	 * @param reactivatedBy
	 *            the reactivatedBy to set
	 */
	public void setReactivatedBy(String reactivatedBy) {
		this.reactivatedBy = reactivatedBy;
	}

	/**
	 * @return the reactivatedOn
	 */
	public String getReactivatedOn() {
		return reactivatedOn;
	}

	/**
	 * @param reactivatedOn
	 *            the reactivatedOn to set
	 */
	public void setReactivatedOn(String reactivatedOn) {
		this.reactivatedOn = reactivatedOn;
	}

}
