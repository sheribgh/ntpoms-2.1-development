/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.poadmin.web.controllers;

import static my.com.parkson.ntpoms.poadmin.web.utils.HeaderTitleConstants.PERMISSIONTITLE;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.common.entities.Permission;
import my.com.parkson.ntpoms.poadmin.security.SecurityUtil;
import my.com.parkson.ntpoms.common.services.SecurityService;

/**
 * The Class PermissionController.
 */
@Slf4j
@Controller
@Secured(SecurityUtil.MANAGE_PERMISSION)
@RequiredArgsConstructor
public class PermissionController extends AbstractAdminController {
	private static final String VIEWPREFIX = "permissions/";
	
	@Autowired
	private SecurityService securityService;

	@Override
	protected String getHeaderTitle() {
		return PERMISSIONTITLE;
	}

	/**
	 * List permissions.
	 *
	 * @param model the model
	 * @return the string
	 */
	@GetMapping(value = "/permissions")
	public String listPermissions(Model model) {
		List<Permission> list = securityService.getAllPermissions();
		model.addAttribute("permissions", list);
		return VIEWPREFIX + "permissions";
	}

}
