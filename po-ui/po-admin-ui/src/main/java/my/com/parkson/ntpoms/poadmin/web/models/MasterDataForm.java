/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.poadmin.web.models;

import java.io.Serializable;

public class MasterDataForm implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int mastDataId;

	private String codeType;

	private String codeValue;

	private String codeDesc;

	private String sysName;

	private String isActive;

	private String createdby;

	private String updatedby;

	private String createdon;

	private String updatedon;
	
	private String deactivatedBy;
	
	private String deactivatedOn;

	private String reactivatedBy;
	
	private String reactivatedOn;

	public String getDeactivatedBy() {
		return deactivatedBy;
	}

	public void setDeactivatedBy(String deactivatedBy) {
		this.deactivatedBy = deactivatedBy;
	}

	public String getDeactivatedOn() {
		return deactivatedOn;
	}

	public void setDeactivatedOn(String deactivatedOn) {
		this.deactivatedOn = deactivatedOn;
	}

	public String getReactivatedBy() {
		return reactivatedBy;
	}

	public void setReactivatedBy(String reactivatedBy) {
		this.reactivatedBy = reactivatedBy;
	}

	public String getReactivatedOn() {
		return reactivatedOn;
	}

	public void setReactivatedOn(String reactivatedOn) {
		this.reactivatedOn = reactivatedOn;
	}

	/**
	 * @return the mastDataId
	 */
	public int getMastDataId() {
		return mastDataId;
	}

	/**
	 * @param mastDataId
	 *            the mastDataId to set
	 */
	public void setMastDataId(int mastDataId) {
		this.mastDataId = mastDataId;
	}

	/**
	 * @return the createdby
	 */
	public String getCreatedby() {
		return createdby;
	}

	/**
	 * @param createdby
	 *            the createdby to set
	 */
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	/**
	 * @return the updatedby
	 */
	public String getUpdatedby() {
		return updatedby;
	}

	/**
	 * @param updatedby
	 *            the updatedby to set
	 */
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}

	/**
	 * @return the createdon
	 */
	public String getCreatedon() {
		return createdon;
	}

	/**
	 * @param createdon
	 *            the createdon to set
	 */
	public void setCreatedon(String createdon) {
		this.createdon = createdon;
	}

	/**
	 * @return the updatedon
	 */
	public String getUpdatedon() {
		return updatedon;
	}

	/**
	 * @param updatedon
	 *            the updatedon to set
	 */
	public void setUpdatedon(String updatedon) {
		this.updatedon = updatedon;
	}

	/**
	 * @return the codeType
	 */
	public String getCodeType() {
		return codeType;
	}

	/**
	 * @param codeType
	 *            the codeType to set
	 */
	public void setCodeType(String codeType) {
		this.codeType = codeType;
	}

	/**
	 * @return the codeValue
	 */
	public String getCodeValue() {
		return codeValue;
	}

	/**
	 * @param codeValue
	 *            the codeValue to set
	 */
	public void setCodeValue(String codeValue) {
		this.codeValue = codeValue;
	}

	/**
	 * @return the codeDesc
	 */
	public String getCodeDesc() {
		return codeDesc;
	}

	/**
	 * @param codeDesc
	 *            the codeDesc to set
	 */
	public void setCodeDesc(String codeDesc) {
		this.codeDesc = codeDesc;
	}

	/**
	 * @return the sysName
	 */
	public String getSysName() {
		return sysName;
	}

	/**
	 * @param sysName
	 *            the sysName to set
	 */
	public void setSysName(String sysName) {
		this.sysName = sysName;
	}

	/**
	 * @return the isActive
	 */
	public String getIsActive() {
		return isActive;
	}

	/**
	 * @param isActive
	 *            the isActive to set
	 */
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

}
