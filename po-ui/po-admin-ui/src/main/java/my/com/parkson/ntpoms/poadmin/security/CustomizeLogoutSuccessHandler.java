package my.com.parkson.ntpoms.poadmin.security;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import my.com.parkson.ntpoms.common.services.AuditService;

@Component
public class CustomizeLogoutSuccessHandler implements LogoutSuccessHandler {

	@Autowired
	private AuditService auditService;
	
	@Override
	public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
			throws IOException, ServletException {
		/* Fetching Client's IP Address*/
		 request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
		        .getRequest();

		String ip = request.getRemoteAddr();
		if(authentication != null)
			auditService.putAudit(new Date(), "User " + authentication.getName() + " with IP address " + ip + " has logged out", authentication.getName(), "Log Out");
		response.setStatus(HttpServletResponse.SC_OK);
		// redirect to login
		response.sendRedirect("/login");
	}

}