/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.poadmin.web.controllers;

import static my.com.parkson.ntpoms.common.MessageCodes.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.LogMessages;
import my.com.parkson.ntpoms.common.entities.PurchaseRequisition;
import my.com.parkson.ntpoms.common.entities.SystemParameter;
import my.com.parkson.ntpoms.common.repositories.PurchaseRequisitionRepository;
import my.com.parkson.ntpoms.common.services.AccessLogService;
import my.com.parkson.ntpoms.common.services.AuditService;
import my.com.parkson.ntpoms.common.services.MessageService;
import my.com.parkson.ntpoms.common.services.PurchaseRequisitionService;
import my.com.parkson.ntpoms.common.services.SystemParameterService;
import my.com.parkson.ntpoms.poadmin.security.SecurityUtil;
import my.com.parkson.ntpoms.poadmin.web.models.Pager;
import my.com.parkson.ntpoms.poadmin.web.models.PurchaseReqForm;

@Slf4j
@Controller
@Secured(SecurityUtil.MANAGE_PURCHASE)
@RequiredArgsConstructor
public class PurchaseController extends AbstractAdminController {

	private static Logger LOGGER = Logger.getLogger(PurchaseController.class);

	private static final int BUTTONS_TO_SHOW = 5;
	private static final int INITIAL_PAGE = 0;
	private static final int INITIAL_PAGE_SIZE = 5;
	private static final int[] PAGE_SIZES = { 5, 10, 20 };
	boolean edit = false;

	@Autowired
	private PurchaseRequisitionService purrepo;

	@Autowired
	private PurchaseRequisitionRepository prRepository;

	private PurchaseReqForm tempobj;

	/** The messages. */
	@Autowired
	private MessageService messages;

	@Autowired
	private AuditService auditService;

	@Autowired
	MessageService messageService;

	@Autowired
	private SystemParameterService sysRepo;

	/**
	 * On page load purchase.
	 *
	 * @param pageSize
	 *            the page size
	 * @param page
	 *            the page
	 * @param typeCode
	 *            the type code
	 * @param abbrName
	 *            the abbr name
	 * @param isActive
	 *            the is active
	 * @return the model and view
	 */
	@GetMapping("/master/purchase")
	public ModelAndView onPageLoadPurchase() {
		ModelAndView modelAndView = new ModelAndView("/master/purchase");

		modelAndView.getModelMap().addAttribute("purchaseobj", tempobj != null ? tempobj : new PurchaseReqForm());
		return modelAndView;
	}

	@RequestMapping(value = "/master/purchasegrid", method = RequestMethod.GET)
	public String purchaseGrid(@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page, @RequestParam("typeCode") Optional<String> typeCode,
			@RequestParam("abbrName") Optional<String> abbrName, @RequestParam("status") Optional<String> isActive,
			@RequestParam("sortingkey") Optional<String> sortingkey,
			@RequestParam("sortingorder") Optional<String> sortingorder, Model model) {

		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
		Page<PurchaseRequisition> list;

		Specification<PurchaseRequisition> s = new Specification<PurchaseRequisition>() {
			@Override
			public Predicate toPredicate(Root<PurchaseRequisition> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> ls = new ArrayList<>();
				if (typeCode.isPresent() && !typeCode.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("typeCode")), "%" + typeCode.get().toLowerCase() + "%"));
				}
				if (abbrName.isPresent() && !abbrName.get().isEmpty()) {

					ls.add(cb.like(cb.lower(root.get("abbrName")), "%" + abbrName.get().toLowerCase() + "%"));
				}

				if (isActive.isPresent() && !isActive.get().isEmpty() && !isActive.get().equals("All")) {
					ls.add(cb.equal(root.get("isActive"), "Active".equals(isActive.get())));
				}

				return cb.and(ls.toArray(new Predicate[0]));
			}
		};
		Sort sorttemp = new Sort(Sort.Direction.DESC, "lastModifiedOn");

		if (sortingkey.isPresent() && sortingorder.isPresent()) {
			sorttemp = new Sort(sortingorder.get().equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC,
					sortingkey.get());
		}
		if (typeCode.isPresent() || abbrName.isPresent() || isActive.isPresent()) {
			list = purrepo.getAllPageablePurchaseRequisition(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		} else {
			list = purrepo.getAllPageablePurchaseRequisitionOrdered(new PageRequest(evalPage, evalPageSize, sorttemp));
		}

		int serialid = 0;

		int pagesize = evalPage + 1;

		if (pagesize > 1) {
			int start = evalPageSize * pagesize;
			serialid = start - evalPageSize;
		}

		Pager pager = new Pager(list.getTotalPages(), list.getNumber(), BUTTONS_TO_SHOW);
		model.addAttribute("list", list);
		model.addAttribute("serialid", serialid);
		model.addAttribute("selectedPageSize", evalPageSize);
		model.addAttribute("pageSizes", PAGE_SIZES);
		model.addAttribute("pager", pager);
		model.addAttribute("totalRecords", list.getTotalElements());
		model.addAttribute("typeCode", typeCode.isPresent() ? typeCode.get() : "");
		model.addAttribute("abbrName", abbrName.isPresent() ? abbrName.get() : "");
		model.addAttribute("status", isActive.isPresent() ? isActive.get() : "");
		model.addAttribute("sortingorder", sortingorder.isPresent() ? sortingorder.get() : "DESC");
		model.addAttribute("sortingkey", sortingkey.isPresent() ? sortingkey.get() : "lastModifiedOn");

		return "/master/purchase :: result-table";
	}

	/**
	 * Creates the purchase req.
	 *
	 * @param obj
	 *            the obj
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @param redirectAttributes
	 *            the redirect attributes
	 * @return the string
	 * @throws ParseException
	 *             the parse exception
	 */
	@PostMapping(value = "/master/createpurchase")
	public String createPurchaseReq(@Valid @ModelAttribute("purchaseobj") PurchaseReqForm obj, BindingResult result,
			Model model, RedirectAttributes redirectAttributes) throws ParseException {
		if (!result.hasErrors() && obj.getPrTypeCode() != null
				&& purrepo.getPurchaseRequisitionByTypeCode(obj.getPrTypeCode()) == null) {
			PurchaseRequisition ad = new PurchaseRequisition();
			ad.setTypeCode(obj.getPrTypeCode());
			ad.setDesc(obj.getPrDesc());
			ad.setAbbrName(obj.getPrAbbrName());
			ad.setActive(true);
			ad.setCreatedBy(getCurrentUser() != null ? getCurrentUser().getUsername() : null);
			ad.setCreatedOn(new Date());
			ad.setLastModifiedBy(getCurrentUser() != null ? getCurrentUser().getUsername() : null);
			ad.setLastModifiedOn(new Date());
			try {
				purrepo.createPurchaseRequisition(ad);
				auditService.putAudit(new Date(),
						"Purchase Requisition: " + obj.getPrTypeCode() + " created successfully",
						getCurrentUser().getUsername(), "Create Purchase Requisition");
				redirectAttributes.addFlashAttribute("info", "Purchase Requisition created successfully");
				tempobj = null;
			} catch (Exception e) {
				LOGGER.error(e);
			}

		} else {
			redirectAttributes.addFlashAttribute("error", "Purchase Requisition code already exists");
			tempobj = obj;
		}
		return "redirect:/master/purchase";
	}

	/**
	 * Update purchase req.
	 *
	 * @param obj
	 *            the obj
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @param redirectAttributes
	 *            the redirect attributes
	 * @return the string
	 * @throws ParseException
	 *             the parse exception
	 */
	@PostMapping(value = "/master/updatepurchase")
	public String updatePurchaseReq(@Valid @ModelAttribute("purchaseobj") PurchaseReqForm obj, BindingResult result,
			Model model, RedirectAttributes redirectAttributes) throws ParseException {
		if (!result.hasErrors()) {
			boolean isActive;
			PurchaseRequisition ad = purrepo.getPurchaseRequisitionByTypeCode(obj.getPrTypeCode());
			try {
				if (ad != null) {
					ad.setTypeCode(obj.getPrTypeCode());
					ad.setDesc(obj.getPrDesc());
					ad.setAbbrName(obj.getPrAbbrName());
					if (obj.getIsActive().contains("InActive")) {
						isActive = false;
					} else {
						isActive = true;
					}

					if (isActive != ad.isActive()) {

						if (obj.getIsActive().contains("InActive")) {
							ad.setDeactivatedBy(getCurrentUser().getUsername());
							ad.setDeactivatedOn(new Date());
						} else {
							ad.setReactivatedOn(new Date());
							ad.setReactivatedBy(getCurrentUser().getUsername());
						}
					}
					ad.setActive("Active".equals(obj.getIsActive()));
					ad.setLastModifiedBy(getCurrentUser() != null ? getCurrentUser().getUsername() : null);
					ad.setLastModifiedOn(new Date());
					purrepo.createPurchaseRequisition(ad);
					auditService.putAudit(new Date(),
							"Purchase Requisition: " + obj.getPrTypeCode() + " updated successfully",
							getCurrentUser().getUsername(), "Update Purchase Requisition");
					redirectAttributes.addFlashAttribute("info", "Purchase Requisition updated successfully");
				} else {
					redirectAttributes.addFlashAttribute("error", "Purchase Requisition Object not exists");
					return "/master/edit_purchase";
				}
			} catch (Exception e) {
				LOGGER.error(e);
			}
		}
		return "redirect:/master/purchase";
	}

	@GetMapping(value = "/master/deletepurchasereq/{code}")
	public String deletepurchasereq(@PathVariable String code, Model model) {

		PurchaseRequisition ad = purrepo.getPurchaseRequisitionByTypeCode(code);
		if (ad != null) {
			prRepository.delete(ad);

		}
		return "redirect:/master/purchase";
	}

	/**
	 * Edits the purchase req.
	 *
	 * @param taxcode
	 *            the taxcode
	 * @param model
	 *            the model
	 * @return the string
	 */
	@GetMapping(value = "/master/editpurchasereq/{taxcode}")
	public String editPurchaseReq(@PathVariable String taxcode, Model model) {
		PurchaseRequisition tobj = purrepo.getPurchaseRequisitionByTypeCode(taxcode);
		edit = true;
		if (tobj != null) {
			PurchaseReqForm obj = new PurchaseReqForm();
			String date_format = "dd-MM-yyyy hh:mm:ss";
			SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_TIME_FORMAT");
			if (dateObj != null) {
				if (!dateObj.getPropValue().isEmpty()) {
					date_format = dateObj.getPropValue();
				}
			}
			DateFormat formatter = new SimpleDateFormat(date_format);
			String createdOn = "";
			String modifiedOn = "";
			String reactivatedOn = "";
			String deactivatedOn = "";
			if (tobj.getCreatedOn() != null) {
				createdOn = formatter.format(tobj.getCreatedOn());
			}
			if (tobj.getLastModifiedOn() != null) {
				modifiedOn = formatter.format(tobj.getLastModifiedOn());
			}
			if (tobj.getReactivatedOn() != null) {
				reactivatedOn = formatter.format(tobj.getReactivatedOn());
			}
			if (tobj.getDeactivatedOn() != null) {
				deactivatedOn = formatter.format(tobj.getDeactivatedOn());
			}
			obj.setCreatedOn(createdOn);
			obj.setDeactivatedOn(deactivatedOn);
			obj.setReactivatedOn(reactivatedOn);
			obj.setLastModifiedOn(modifiedOn);
			obj.setPrTypeCode(tobj.getTypeCode());
			obj.setPrDesc(tobj.getDesc());
			obj.setCreatedBy(tobj.getCreatedBy());
			obj.setLastModifiedBy(tobj.getLastModifiedBy());
			obj.setReactivatedBy(tobj.getReactivatedBy());
			obj.setDeactivatedBy(tobj.getDeactivatedBy());
			obj.setPrAbbrName(tobj.getAbbrName());
			obj.setIsActive(tobj.isActive() ? "Active" : "InActive");
			model.addAttribute("purchaseobj", obj);
			model.addAttribute("edit", edit);
			return "/master/edit_purchase";
		} else {
			return "redirect:/master/purchase";
		}
	}

	@Override
	protected String getHeaderTitle() {
		return "Manage Purchase";
	}

	public int getTotalRecords() {
		List<PurchaseRequisition> totalRecords = purrepo.getAllActivePurchaseRequisition();
		int size = totalRecords.size();
		return size;
	}

	/**
	 * Gets the store based on company.
	 *
	 * @param companycode
	 *            the companycode
	 * @return the store based on company
	 */
	@RequestMapping(value = "/master/pr/existingpr", method = RequestMethod.GET)
	public @ResponseBody PurchaseRequisition checkPR(@RequestParam("prcode") String prcode,
			RedirectAttributes redirectAttributes, Model model) {

		return purrepo.getPurchaseRequisitionByTypeCode(prcode);

	}

}
