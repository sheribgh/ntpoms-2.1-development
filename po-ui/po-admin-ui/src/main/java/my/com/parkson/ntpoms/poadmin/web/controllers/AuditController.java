/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.poadmin.web.controllers;

import static my.com.parkson.ntpoms.common.MessageCodes.FAILED_PERSIST_EXCEPTION;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.LogMessages;
import my.com.parkson.ntpoms.common.entities.AccessLog;
import my.com.parkson.ntpoms.common.entities.AccessLogSetting;
import my.com.parkson.ntpoms.common.entities.Audit;
import my.com.parkson.ntpoms.common.entities.SystemParameter;
import my.com.parkson.ntpoms.common.services.AccessLogService;
import my.com.parkson.ntpoms.common.services.AccessLogSettingService;
import my.com.parkson.ntpoms.common.services.AuditService;
import my.com.parkson.ntpoms.common.services.MessageService;
import my.com.parkson.ntpoms.common.services.SystemParameterService;
import my.com.parkson.ntpoms.poadmin.security.SecurityUtil;
import my.com.parkson.ntpoms.poadmin.web.models.LogSettingForm;
import my.com.parkson.ntpoms.poadmin.web.models.Pager;

@Slf4j
@Controller
@Secured(SecurityUtil.MANAGE_AUDIT)
@RequiredArgsConstructor
public class AuditController extends AbstractAdminController {

	private static final int BUTTONS_TO_SHOW = 5;
	private static final int INITIAL_PAGE = 0;
	private static final int INITIAL_PAGE_SIZE = 5;
	private static final int[] PAGE_SIZES = { 5, 10, 20 };

	private static Logger LOGGER = Logger.getLogger(AuditController.class);
	/** The messages. */
	@Autowired
	private MessageService messages;

	@Autowired
	private AuditService auditService;

	@Autowired
	private AuditService auditrepo;

	@Autowired
	private AccessLogService logrepo;

	@Autowired
	private AccessLogSettingService logsettingrepo;

	@Autowired
	private SystemParameterService sysRepo;

	/**
	 * Show persons page.
	 *
	 * @param pageSize
	 *            the page size
	 * @param page
	 *            the page
	 * @param classFileName
	 *            the class file name
	 * @param lineNo
	 *            the line no
	 * @param id
	 *            the id
	 * @param level
	 *            the level
	 * @param message
	 *            the message
	 * @param screenId
	 *            the screen id
	 * @return the model and view
	 */
	@GetMapping("/audit/audittraillist")
	public ModelAndView showPersonsPage() {
		ModelAndView modelAndView = new ModelAndView("/audit/audittrail");
		return modelAndView;
	}

	@RequestMapping(value = "/audit/audittraillistgrid", method = RequestMethod.GET)
	public String auditGrid(@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page, @RequestParam("username") Optional<String> username,
			@RequestParam("auditId") Optional<Integer> auditId, @RequestParam("operation") Optional<String> operation,
			@RequestParam("message") Optional<String> message, @RequestParam("createdon") Optional<String> createdon,
			@RequestParam("sortingkey") Optional<String> sortingkey,
			@RequestParam("sortingorder") Optional<String> sortingorder, Model model) {
		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
		Page<Audit> list;
		Specification<Audit> s = new Specification<Audit>() {
			@Override
			public Predicate toPredicate(Root<Audit> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

				List<Predicate> ls = new ArrayList<>();

				if (username.isPresent() && !username.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("username")), "%" + username.get().toLowerCase() + "%"));
				}
				if (auditId.isPresent()) {

					ls.add(cb.equal(root.get("id"), auditId.get()));
				}
				if (message.isPresent() && !message.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("message")), "%" + message.get().toLowerCase() + "%"));
				}

				if (createdon.isPresent() && !createdon.get().isEmpty()) {
					Expression<String> dateStringExpr = cb.function("to_char", String.class, root.get("createdOn"),
							cb.literal("YYYY-MM-DD HH12:MI:SS"));
					ls.add(cb.like(cb.lower(dateStringExpr), "%" + createdon.get().toLowerCase() + "%"));

				}

				if (operation.isPresent() && !operation.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("operation")), "%" + operation.get().toLowerCase() + "%"));
				}

				return cb.and(ls.toArray(new Predicate[0]));
			}
		};
		Sort sorttemp = new Sort(Sort.Direction.DESC, "id");
		if (sortingkey.isPresent() && sortingorder.isPresent()) {
			sorttemp = new Sort(sortingorder.get().equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC,
					sortingkey.get());
		}
		if (username.isPresent() || auditId.isPresent() || message.isPresent() || operation.isPresent()
				|| createdon.isPresent()) {
			list = auditrepo.getAllPagableAudit(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		} else {
			list = auditrepo.getAllPagableAudit(new PageRequest(evalPage, evalPageSize, sorttemp));
		}

		int serialid = 0;

		int pagesize = evalPage + 1;

		if (pagesize > 1) {
			int start = evalPageSize * pagesize;
			serialid = start - evalPageSize;
		}

		Pager pager = new Pager(list.getTotalPages(), list.getNumber(), BUTTONS_TO_SHOW);
		model.addAttribute("serialid", serialid);
		model.addAttribute("list", list);
		model.addAttribute("selectedPageSize", evalPageSize);
		model.addAttribute("pageSizes", PAGE_SIZES);
		model.addAttribute("pager", pager);
		model.addAttribute("totalRecords", list.getTotalElements());
		model.addAttribute("username", username.isPresent() ? username.get() : "");
		model.addAttribute("operation", operation.isPresent() ? operation.get() : "");
		model.addAttribute("message", message.isPresent() ? message.get() : "");
		model.addAttribute("auditId", auditId.isPresent() ? auditId.get() : "");
		model.addAttribute("createdon", createdon.isPresent() ? createdon.get() : "");
		model.addAttribute("sortingorder", sortingorder.isPresent() ? sortingorder.get() : "DESC");
		model.addAttribute("sortingkey", sortingkey.isPresent() ? sortingkey.get() : "id");
		return "audit/audittrail :: result-table";
	}

	/**
	 * Logs page.
	 *
	 * @param pageSize
	 *            the page size
	 * @param page
	 *            the page
	 * @param username
	 *            the username
	 * @param auditId
	 *            the audit id
	 * @param operation
	 *            the operation
	 * @param message
	 *            the message
	 * @return the model and view
	 */
	@GetMapping("/audit/logs")
	public ModelAndView logsPage() {
		ModelAndView modelAndView = new ModelAndView("/audit/logs");

		AccessLogSetting templist = logsettingrepo.getLastStagingLog();
		LogSettingForm obj = new LogSettingForm();
		if (templist != null) {
			obj.setLogSetting(templist.getSetting());
			obj.setLogSettingId(templist.getId());
		}
		modelAndView.getModelMap().addAttribute("auditobj", obj);
		return modelAndView;
	}

	@RequestMapping(value = "/audit/loggrid", method = RequestMethod.GET)
	public String logsGrid(@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page, @RequestParam("classFileName") Optional<String> classFileName,
			@RequestParam("lineNo") Optional<Integer> lineNo, @RequestParam("id") Optional<Integer> id,
			@RequestParam("level") Optional<String> level, @RequestParam("message") Optional<String> message,
			@RequestParam("screenId") Optional<String> screenId, @RequestParam("logTime") Optional<String> logTime,
			@RequestParam("sortingkey") Optional<String> sortingkey,
			@RequestParam("sortingorder") Optional<String> sortingorder, Model model) {

		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
		Page<AccessLog> list;

		String date_format = "dd-MM-yyyy hh:mm:ss";
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_TIME_FORMAT");
		if (dateObj != null) {
			if (!dateObj.getPropValue().isEmpty()) {
				date_format = dateObj.getPropValue();
			}
		}
		Specification<AccessLog> s = new Specification<AccessLog>() {
			@Override
			public Predicate toPredicate(Root<AccessLog> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> ls = new ArrayList<>();
				if (classFileName.isPresent() && !classFileName.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("classFileName")), "%" + classFileName.get().toLowerCase() + "%"));
				}
				if (lineNo.isPresent()) {

					ls.add(cb.equal(root.get("lineNo"), lineNo.get()));
				}
				if (id.isPresent()) {
					ls.add(cb.equal(root.get("id"), id.get()));
				}
				if (level.isPresent() && !level.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("level")), "%" + level.get().toLowerCase() + "%"));
				}

				if (message.isPresent() && !message.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("message")), "%" + message.get().toLowerCase() + "%"));
				}
				if (screenId.isPresent() && !screenId.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("screenId")), "%" + screenId.get().toLowerCase() + "%"));
				}

				if (logTime.isPresent() && !logTime.get().isEmpty()) {

					Expression<String> dateStringExpr = cb.function("to_char", String.class, root.get("time"),
							cb.literal("YYYY-MM-DD HH12:MI:SS"));
					ls.add(cb.like(cb.lower(dateStringExpr), "%" + logTime.get().toLowerCase() + "%"));

				}

				return cb.and(ls.toArray(new Predicate[0]));
			}
		};
		Sort sorttemp = new Sort(Sort.Direction.DESC, "id");
		if (sortingkey.isPresent() && sortingorder.isPresent()) {
			sorttemp = new Sort(sortingorder.get().equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC,
					sortingkey.get());
		}
		if (classFileName.isPresent() || lineNo.isPresent() || id.isPresent() || level.isPresent()
				|| message.isPresent() || screenId.isPresent() || logTime.isPresent()) {
			list = logrepo.getAllPageableAccessLog(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		} else {
			list = logrepo.getAllPageableLogOrdered(new PageRequest(evalPage, evalPageSize, sorttemp));
		}

		int serialid = 0;

		int pagesize = evalPage + 1;

		if (pagesize > 1) {
			int start = evalPageSize * pagesize;
			serialid = start - evalPageSize;
		}

		List<AccessLog> totalRecords = logrepo.getAllAccessLog();
		int size = totalRecords.size();

		Pager pager = new Pager(list.getTotalPages(), list.getNumber(), BUTTONS_TO_SHOW);
		model.addAttribute("serialid", serialid);
		model.addAttribute("list", list);
		model.addAttribute("totalRecords", size);
		model.addAttribute("selectedPageSize", evalPageSize);
		model.addAttribute("pageSizes", PAGE_SIZES);
		model.addAttribute("pager", pager);
		model.addAttribute("date_format", date_format);
		model.addAttribute("classFileName", classFileName.isPresent() ? classFileName.get() : "");
		model.addAttribute("lineNo", lineNo.isPresent() ? lineNo.get() : "");
		model.addAttribute("id", id.isPresent() ? id.get() : "");
		model.addAttribute("level", level.isPresent() ? level.get() : "");
		model.addAttribute("message", message.isPresent() ? message.get() : "");
		model.addAttribute("screenId", screenId.isPresent() ? screenId.get() : "");
		model.addAttribute("logTime", logTime.isPresent() ? logTime.get() : "");
		model.addAttribute("sortingorder", sortingorder.isPresent() ? sortingorder.get() : "DESC");
		model.addAttribute("sortingkey", sortingkey.isPresent() ? sortingkey.get() : "id");
		AccessLogSetting templist = logsettingrepo.getLastStagingLog();
		LogSettingForm obj = new LogSettingForm();
		if (templist != null) {
			obj.setLogSetting(templist.getSetting());
			obj.setLogSettingId(templist.getId());
		}

		return "audit/logs :: result-table";
	}

	/**
	 * Update setting.
	 *
	 * @param obj
	 *            the obj
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @param redirectAttributes
	 *            the redirect attributes
	 * @return the string
	 */
	@PostMapping(value = "/audit/updatesetting")
	public String updateSetting(@Valid @ModelAttribute("auditobj") LogSettingForm obj, BindingResult result,
			Model model, RedirectAttributes redirectAttributes) {
		if (!result.hasErrors()) {
			List<AccessLogSetting> templist = logsettingrepo.getAllStagingLog();
			AccessLogSetting tobj;
			if (templist != null && !templist.isEmpty()) {
				tobj = templist.get(0);
			} else {
				tobj = new AccessLogSetting();
			}
			tobj.setSetting(obj.getLogSetting());
			try {
				logsettingrepo.createStagingLog(tobj);
				auditService.putAudit(new Date(), "Log Setting: " + obj.getLogSetting() + " updated successfully",
						getCurrentUser().getUsername(), "Update Log Settings");
				redirectAttributes.addFlashAttribute("info", "Log Settings updated successfully");
			} catch (Exception e) {
				LOGGER.error(e);
			}
		}
		return "redirect:/audit/logs";
	}

	@Override
	protected String getHeaderTitle() {
		return "Manage Audits";
	}
}
