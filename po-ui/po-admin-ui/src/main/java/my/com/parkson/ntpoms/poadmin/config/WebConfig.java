/*
 * ********************************************************************************
 * Copyright (c) 2018 - Brentin Services, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.brent-in.com
 * @Copyright  : Copyright 2018 Brentin Services, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.poadmin.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.undertow.UndertowBuilderCustomizer;
import org.springframework.boot.context.embedded.undertow.UndertowEmbeddedServletContainerFactory;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.thymeleaf.extras.springsecurity4.dialect.SpringSecurityDialect;

import my.com.parkson.ntpoms.poadmin.security.PostAuthorizationFilter;

/**
 * The Class WebConfig.
 */
/**
 * @author Subhash
 *
 */
@Configuration
public class WebConfig extends WebMvcConfigurerAdapter {

	private PostAuthorizationFilter postAuthorizationFilter;

	private MessageSource messageSource;

	static final Logger logger = LoggerFactory.getLogger(WebConfig.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter
	 * #addViewControllers(org.springframework.web.servlet.config.annotation.
	 * ViewControllerRegistry)
	 */
	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		super.addViewControllers(registry);
		registry.addViewController("/login").setViewName("public/login");
		registry.addViewController("/403").setViewName("403");
		registry.addRedirectViewController("/", "users/adminuser");

	}

	/**
	 * Instantiates a new web config.
	 *
	 * @param postAuthorizationFilter
	 *            the post authorization filter
	 * @param messageSource
	 *            the message source
	 */
	@Autowired
	public WebConfig(PostAuthorizationFilter postAuthorizationFilter, MessageSource messageSource) {
		this.postAuthorizationFilter = postAuthorizationFilter;
		this.messageSource = messageSource;
	}

	/**
	 * Jsr validator.
	 *
	 * @return the validator
	 */
	@Bean
	public Validator jsrValidator() {
		LocalValidatorFactoryBean factory = new LocalValidatorFactoryBean();
		factory.setValidationMessageSource(messageSource);
		return factory;
	}

	/**
	 * Post authorization filter registration bean.
	 *
	 * @return the filter registration bean
	 */
	@Bean
	public FilterRegistrationBean postAuthorizationFilterRegistrationBean() {
		FilterRegistrationBean registrationBean = new FilterRegistrationBean();
		registrationBean.setFilter(postAuthorizationFilter);
		registrationBean.setOrder(Integer.MAX_VALUE);
		return registrationBean;
	}

	/**
	 * Security dialect.
	 *
	 * @return the spring security dialect
	 */
	@Bean
	public SpringSecurityDialect securityDialect() {
		return new SpringSecurityDialect();
	}

	/*
	@Bean
	public UndertowEmbeddedServletContainerFactory servletContainer() {
		UndertowEmbeddedServletContainerFactory undertow = new UndertowEmbeddedServletContainerFactory();
		undertow.addBuilderCustomizers(builder -> builder.addHttpListener(8080, "0.0.0.0"));
		undertow.addDeploymentInfoCustomizers(deploymentInfo -> {
			deploymentInfo
					.addSecurityConstraint(new SecurityConstraint()
							.addWebResourceCollection(new WebResourceCollection().addUrlPattern("/*"))
							.setTransportGuaranteeType(TransportGuaranteeType.CONFIDENTIAL)
							.setEmptyRoleSemantic(SecurityInfo.EmptyRoleSemantic.PERMIT))
					.setConfidentialPortManager(exchange -> 8443);
		});
		return undertow;
	}
	*/
}