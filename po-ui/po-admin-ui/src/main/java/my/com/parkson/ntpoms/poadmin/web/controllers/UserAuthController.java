/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-admin-ui-2.0-SNAPSHOT.war
 * @Version    : 2.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.poadmin.web.controllers;

import static my.com.parkson.ntpoms.poadmin.web.utils.MessageCodes.ERROR_INVALID_PASSWRD_RESET_REQUEST;
import static my.com.parkson.ntpoms.poadmin.web.utils.MessageCodes.ERROR_PASSWRD_CONF_PASSWRD_MISMATCH;
import static my.com.parkson.ntpoms.poadmin.web.utils.MessageCodes.INFO_PASSWRD_UPDATED_SUCCESS;
import static my.com.parkson.ntpoms.poadmin.web.utils.MessageCodes.ERROR_PASSWRD_ISEMPTY;
import static my.com.parkson.ntpoms.poadmin.web.utils.MessageCodes.ERROR_CONFIRM_PASSWRD_ISEMPTY;

import java.io.IOException;
import java.security.SecureRandom;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;

import javax.mail.Authenticator;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.LogMessages;
import my.com.parkson.ntpoms.common.entities.User;
import my.com.parkson.ntpoms.common.model.EmailCriteria;
import my.com.parkson.ntpoms.common.services.AccessLogService;
import my.com.parkson.ntpoms.common.services.AuditService;
import my.com.parkson.ntpoms.common.services.EmailService;
import my.com.parkson.ntpoms.common.services.SecurityService;
import my.com.parkson.ntpoms.common.services.UsersService;
import my.com.parkson.ntpoms.utils.EmailServiceUtils;

/**
 * The Class UserAuthController.
 */
@Slf4j
@Controller
@RequiredArgsConstructor
public class UserAuthController extends AbstractAdminController {

	private static Logger LOGGER = Logger.getLogger(UserAuthController.class);
	private static final String VIEWPREFIX = "public/";

	private static final String ALPHA_CAPS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private static final String ALPHA = "abcdefghijklmnopqrstuvwxyz";
	private static final String NUMERIC = "0123456789";
    private static final String SPECIAL_CHARS = "!@#$%^&*_=+-/";

	private static SecureRandom random = new SecureRandom();

	@Autowired
	private UsersService adminrepo;

	@Autowired
	private SecurityService securityService;

	@Autowired
	private EmailService emailService;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private AuditService auditService;

	/**
	 * Forgot pwd.
	 *
	 * @return the string
	 */
	@GetMapping(value = "/forgotPwd")
	public String forgotPwd() {
		return VIEWPREFIX + "forgotPwd";
	}

	/**
	 * Handle forgot pwd.
	 *
	 * @param request
	 *            the request
	 * @param redirectAttributes
	 *            the redirect attributes
	 * @return the string
	 */
	@PostMapping(value = "/forgotPwd")
	public String handleForgotPwd(HttpServletRequest request, RedirectAttributes redirectAttributes) {
		String password = generatePassword(9, ALPHA_CAPS + ALPHA +SPECIAL_CHARS +NUMERIC);
		String userEmail = request.getParameter("email");
		String userPSID = request.getParameter("psid");
		String link = "http://192.168.1.150:8080/poMain";
		
		boolean isValid = true;
		User tobj = null;
		try {
			tobj = adminrepo.getUsersByUsername(userPSID);
			if (tobj == null) {
				isValid = false;
				redirectAttributes.addFlashAttribute("msg", "Username " + userPSID + " does not belong to any user.");
			}
		} catch (Exception e1) {
			isValid = false;
			redirectAttributes.addFlashAttribute("msg", "Username " + userPSID + " does not belong to any user.");
			LOGGER.error(e1);
		}
		User user = null;
		try {
			user = adminrepo.getUsersByUsername(userPSID);
			if (user == null) {
				isValid = false;
				redirectAttributes.addFlashAttribute("msg", "Email " + userEmail + "  does not belong to any user.");
			}
		} catch (Exception e) {
			isValid = false;
			redirectAttributes.addFlashAttribute("msg", "Email " + userEmail + " does not belong to any user.");
			LOGGER.error(e);
		}
		if (tobj != null && user != null && user.getEmail() != tobj.getEmail()) {
			isValid = false;
			redirectAttributes.addFlashAttribute("msg", "Email and Password did not match.");
		}
		if (isValid) {
			try {
				user.setRecentPassword(user.getPassword());
				user.setPassword(passwordEncoder.encode(password));
				if (adminrepo.saveUser(user)) {
					Map<String, String> context = new HashMap<>();
					context.put("fName", user.getUsername());
					context.put("pwd", password);
					context.put("class_name", "EmailService");
					context.put("pwd", password);
					context.put("appUrl",link);
					context.put("appName", "NTPOMS");
					context.put("psid", userPSID);
					context.put("system_name", "NTPOMS");
					context.put("proprietor", "parkson.com");
					Set<String> cc = new HashSet<>();
					EmailCriteria email = new EmailCriteria();
					email.setRecipient(userEmail);
					email.setSubject("Forgot Password Notification.");
					email.setCc(cc);
					Properties props = EmailServiceUtils.loadEmailPropertyFile();
					email.setTemplateName(props.getProperty("mail.vm.template.fgtpwd"));
					email.setEmailUser(props.getProperty("mail.user"));
					email.setEmailPwd(props.getProperty("mail.passwd"));
					Session session = Session.getInstance(props, new Authenticator() {
						@Override
						protected PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication(email.getEmailUser(), email.getEmailPwd());
						}
					});
					emailService.sendEmail(session, context, null, email);
					redirectAttributes.addFlashAttribute("msg", "Please check your email for your new password.");
				} else {
					redirectAttributes.addFlashAttribute("msg", "Unable to update password & recentPassword values.");
				}
			} catch (MessagingException | IOException e) {
				LOGGER.error(e);
			}
		}
		return "redirect:/forgotPwd";
	}

	/**
	 * Handle reset pwd.
	 *
	 * @param request
	 *            the request
	 * @param model
	 *            the model
	 * @param redirectAttributes
	 *            the redirect attributes
	 * @return the string
	 */
	@RequestMapping(value = "/resetPwdget", method = RequestMethod.GET)
	@ResponseBody
	public String handleResetPwd(@RequestParam("currentPassword") Optional<String> currentPasswordt,
			@RequestParam("newpassword") Optional<String> passwordt,
			@RequestParam("confirmpassword") Optional<String> confirmpasswordt, RedirectAttributes redirectAttributes,
			HttpServletRequest request, HttpServletResponse response) {
		try {
			String currentPassword = currentPasswordt.isPresent() ? currentPasswordt.get() : null;
			String password = passwordt.isPresent() ? passwordt.get() : null;
			String confPassword = confirmpasswordt.isPresent() ? confirmpasswordt.get() : null;
			boolean isCurrentPwdMatch = passwordEncoder.matches(currentPassword,
					getCurrentUser().getUser().getPassword());
			if (isCurrentPwdMatch) {
				if (StringUtils.isEmpty(password)) {
					redirectAttributes.addFlashAttribute("msg", "Password can not be empty or null.");
					return getMessage(ERROR_PASSWRD_ISEMPTY);
				}
				if (StringUtils.isEmpty(confPassword)) {
					redirectAttributes.addFlashAttribute("msg", "Confirm password can not be empty or null.");
					return getMessage(ERROR_CONFIRM_PASSWRD_ISEMPTY);
				}
				if (!password.equals(confPassword)) {
					redirectAttributes.addFlashAttribute("msg", "Confirm password does not match.");
					return getMessage(ERROR_PASSWRD_CONF_PASSWRD_MISMATCH);
				}
				String encodedPwd = passwordEncoder.encode(password);
				securityService.updatePassword(getCurrentUser().getUsername(), "", encodedPwd);
				auditService.putAudit(new Date(), "" + getCurrentUser().getUsername() + ": Password has been changed.",
						getCurrentUser().getUsername(), "Password Reset");
				User tobj = adminrepo.getUsersByUsername(getCurrentUser().getUsername());
				if (tobj != null) {
					Map<String, String> context = new HashMap<>();
					context.put("fName", getCurrentUser().getUsername());
					context.put("class_name", "EmailService");
					context.put("system_name", "NTPOMS");
					context.put("proprietor", "parkson.com");
					Set<String> cc = new HashSet<>();
					EmailCriteria email = new EmailCriteria();
					email.setRecipient(tobj.getEmail());
					email.setSubject("Password Change Notification.");
					email.setCc(cc);
					Properties props = EmailServiceUtils.loadEmailPropertyFile();
					email.setTemplateName(props.getProperty("mail.vm.template.resetpwd"));
					email.setEmailUser(props.getProperty("mail.user"));
					email.setEmailPwd(props.getProperty("mail.passwd"));
					Session session = Session.getInstance(props, new Authenticator() {
						@Override
						protected PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication(email.getEmailUser(), email.getEmailPwd());
						}
					});
					emailService.sendEmail(session, context, null, email);
				}
				return getMessage(INFO_PASSWRD_UPDATED_SUCCESS);
			} else {
				redirectAttributes.addFlashAttribute("msg", "Current password is invalid.");
				return getMessage(ERROR_INVALID_PASSWRD_RESET_REQUEST);
			}
		} catch (MessagingException | IOException e) {
			LOGGER.error(e);
			return e.getMessage();
		}
	}

	/**
	 * Reset pwd.
	 *
	 * @param request
	 *            the request
	 * @param model
	 *            the model
	 * @param redirectAttributes
	 *            the redirect attributes
	 * @return the string
	 */
	@GetMapping(value = "/resetPwd")
	public String resetPwd(HttpServletRequest request, Model model, RedirectAttributes redirectAttributes) {
		String username = request.getParameter("username");
		String token = request.getParameter("token");
		boolean valid = securityService.verifyPasswordResetToken(username, token);
		if (valid) {
			model.addAttribute("username", username);
			model.addAttribute("token", token);
			return VIEWPREFIX + "resetPwd";
		} else {
			redirectAttributes.addFlashAttribute("msg", getMessage(ERROR_INVALID_PASSWRD_RESET_REQUEST));
			return "redirect:/login";
		}
	}

	/**
	 * Send forgot password email.
	 *
	 * @param email
	 *            the email
	 * @param resetPwdURL
	 *            the reset pwd URL
	 */

	public static String generatePassword(int len, String dic) {
		String result = "";
		for (int i = 0; i < len; i++) {
			int index = random.nextInt(dic.length());
			result += dic.charAt(index);
		}
		return result;
	}

	@Override
	protected String getHeaderTitle() {
		return "User";
	}
}