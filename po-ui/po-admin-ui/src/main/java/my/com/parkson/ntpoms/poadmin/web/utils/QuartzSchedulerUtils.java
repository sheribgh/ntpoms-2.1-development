package my.com.parkson.ntpoms.poadmin.web.utils;

import org.apache.log4j.Logger;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.quartz.impl.StdSchedulerFactory;

import my.com.parkson.ntpoms.common.entities.Scheduler;
import my.com.parkson.ntpoms.poadmin.web.scheduler.BulkUpload;
import my.com.parkson.ntpoms.poadmin.web.scheduler.PasswordAccountExp;
import my.com.parkson.ntpoms.poadmin.web.scheduler.PurchaseOrderExpiration;
import my.com.parkson.ntpoms.poadmin.web.scheduler.YearEndDbAct;

/**
 * 
 * @author siroi
 *
 */
public class QuartzSchedulerUtils {

	private static Logger LOGGER = Logger.getLogger(QuartzSchedulerUtils.class);

	private QuartzSchedulerUtils() {

	}

	private static final String GROUP_NAME = "NTPOMS_GROUP";

	public static boolean startSchedulerPoExpiry(String user, String jobName, Scheduler obj) {
		try {
			if (obj != null && obj.getJobname() != null && obj.getCronexpression() != null) {
				org.quartz.Scheduler s = new StdSchedulerFactory().getScheduler();
				JobKey jk = new JobKey(Integer.toString(obj.getId()), GROUP_NAME);
				JobDetail jd = JobBuilder.newJob(PurchaseOrderExpiration.class).withIdentity(jk).build();
				Trigger t = TriggerBuilder.newTrigger().withIdentity(obj.getJobname(), GROUP_NAME)
						.withSchedule(CronScheduleBuilder.cronSchedule(obj.getCronexpression())).build();
				jd.getJobDataMap().put("jobName", jobName);
				jd.getJobDataMap().put("user", user);
				s.start();
				s.scheduleJob(jd, t);
				if (!obj.isActive()) {
					pauseScheduler(obj);
				}
				return true;
			} else {
				return false;
			}
		} catch (SchedulerException e) {
			LOGGER.error(e);
			return false;
		}
	}

	public static boolean startSchedulerYearEndDb(String user, String jobName, Scheduler obj) {
		try {
			if (obj != null && obj.getJobname() != null && obj.getCronexpression() != null) {
				org.quartz.Scheduler s = new StdSchedulerFactory().getScheduler();
				JobKey jk = new JobKey(Integer.toString(obj.getId()), GROUP_NAME);
				JobDetail jd = JobBuilder.newJob(YearEndDbAct.class).withIdentity(jk).build();
				Trigger t = TriggerBuilder.newTrigger().withIdentity(obj.getJobname(), GROUP_NAME)
						.withSchedule(CronScheduleBuilder.cronSchedule(obj.getCronexpression())).build();
				jd.getJobDataMap().put("jobName", jobName);
				jd.getJobDataMap().put("user", user);
				s.start();
				s.scheduleJob(jd, t);
				if (!obj.isActive()) {
					pauseScheduler(obj);
				}
				return true;
			} else {
				return false;
			}
		} catch (SchedulerException e) {
			LOGGER.error(e);
			return false;
		}

	}

	/**
	 * 
	 * @param obj
	 * @return
	 */
	public static boolean startSchedulerPasswordExpiry(String user, String jobName, Scheduler obj) {
		try {
			if (obj != null && obj.getJobname() != null && obj.getCronexpression() != null) {
				org.quartz.Scheduler s = new StdSchedulerFactory().getScheduler();
				JobKey jk = new JobKey(Integer.toString(obj.getId()), GROUP_NAME);
				JobDetail jd = JobBuilder.newJob(PasswordAccountExp.class).withIdentity(jk).build();
				Trigger t = TriggerBuilder.newTrigger().withIdentity(obj.getJobname(), GROUP_NAME)
						.withSchedule(CronScheduleBuilder.cronSchedule(obj.getCronexpression())).build();
				jd.getJobDataMap().put("jobName", jobName);
				jd.getJobDataMap().put("user", user);
				s.start();
				s.scheduleJob(jd, t);
				if (!obj.isActive()) {
					pauseScheduler(obj);
				}
				return true;
			} else {
				return false;
			}
		} catch (SchedulerException e) {
			LOGGER.error(e);
			return false;
		}

	}

	public static boolean startSchedulerBulkUpload(String user, String jobName, Scheduler obj) {
		try {
			if (obj != null && obj.getJobname() != null && obj.getCronexpression() != null) {
				org.quartz.Scheduler s = new StdSchedulerFactory().getScheduler();
				JobKey jk = new JobKey(Integer.toString(obj.getId()), GROUP_NAME);
				JobDetail jd = JobBuilder.newJob(BulkUpload.class).withIdentity(jk).build();
				Trigger t = TriggerBuilder.newTrigger().withIdentity(obj.getJobname(), GROUP_NAME)
						.withSchedule(CronScheduleBuilder.cronSchedule(obj.getCronexpression())).build();
				jd.getJobDataMap().put("jobName", jobName);
				jd.getJobDataMap().put("user", user);

				s.start();
				s.scheduleJob(jd, t);
				if (!obj.isActive()) {
					pauseScheduler(obj);
				}
				return true;
			} else {
				return false;
			}
		} catch (SchedulerException e) {
			LOGGER.error(e);
			return false;
		}

	}

	public static boolean triggerNowSchedulerPasswordExpiry(String user, String jobName, Scheduler obj) {
		try {
			if (obj != null && obj.getJobname() != null && obj.getCronexpression() != null) {
				org.quartz.Scheduler s = new StdSchedulerFactory().getScheduler();
				JobKey jk = new JobKey(Integer.toString(obj.getId()), GROUP_NAME);
				JobDetail jd = JobBuilder.newJob(PasswordAccountExp.class).withIdentity(jk).build();
				Trigger t = TriggerBuilder.newTrigger().withIdentity(obj.getJobname(), GROUP_NAME).startNow().build();
				jd.getJobDataMap().put("jobName", jobName);
				jd.getJobDataMap().put("user", user);
				s.start();
				s.scheduleJob(jd, t);

				return true;
			} else {
				return false;
			}
		} catch (SchedulerException e) {
			LOGGER.error(e);
			return false;
		}

	}

	public static boolean triggerNowSchedulerBulkUpload(String user, String jobName, Scheduler obj) {
		try {
			if (obj != null && obj.getJobname() != null && obj.getCronexpression() != null) {
				org.quartz.Scheduler s = new StdSchedulerFactory().getScheduler();
				JobKey jk = new JobKey(Integer.toString(obj.getId()), GROUP_NAME);
				JobDetail jd = JobBuilder.newJob(BulkUpload.class).withIdentity(jk).build();
				Trigger t = TriggerBuilder.newTrigger().withIdentity(obj.getJobname(), GROUP_NAME).startNow().build();
				jd.getJobDataMap().put("jobName", jobName);
				jd.getJobDataMap().put("user", user);

				s.start();
				s.scheduleJob(jd, t);

				return true;
			} else {
				return false;
			}
		} catch (SchedulerException e) {
			LOGGER.error(e);
			return false;
		}

	}

	public static boolean triggerNowSchedulerYearEndDb(Scheduler obj) {
		try {
			if (obj != null && obj.getJobname() != null && obj.getCronexpression() != null) {
				org.quartz.Scheduler s = new StdSchedulerFactory().getScheduler();
				JobKey jk = new JobKey(Integer.toString(obj.getId()), GROUP_NAME);
				JobDetail jd = JobBuilder.newJob(YearEndDbAct.class).withIdentity(jk).build();
				Trigger t = TriggerBuilder.newTrigger().withIdentity(obj.getJobname(), GROUP_NAME).startNow().build();

				s.start();
				s.scheduleJob(jd, t);

				return true;
			} else {
				return false;
			}
		} catch (SchedulerException e) {
			LOGGER.error(e);
			return false;
		}

	}

	public static boolean triggerNowSchedulerPoExpiry(Scheduler obj) {
		try {
			if (obj != null && obj.getJobname() != null && obj.getCronexpression() != null) {
				org.quartz.Scheduler s = new StdSchedulerFactory().getScheduler();
				JobKey jk = new JobKey(Integer.toString(obj.getId()), GROUP_NAME);
				JobDetail jd = JobBuilder.newJob(PurchaseOrderExpiration.class).withIdentity(jk).build();
				Trigger t = TriggerBuilder.newTrigger().withIdentity(obj.getJobname(), GROUP_NAME)
						.withSchedule(CronScheduleBuilder.cronSchedule(obj.getCronexpression())).build();
				s.start();
				s.scheduleJob(jd, t);

				return true;
			} else {
				return false;
			}
		} catch (SchedulerException e) {
			LOGGER.error(e);
			return false;
		}

	}

	/**
	 * 
	 * @param obj
	 * @return
	 */
	public static boolean reSchedule(Scheduler obj) {
		try {
			if (obj != null && obj.getJobname() != null && obj.getCronexpression() != null) {
				org.quartz.Scheduler s = new StdSchedulerFactory().getScheduler();
				Trigger t = TriggerBuilder.newTrigger().withIdentity(obj.getJobname(), GROUP_NAME)
						.withSchedule(CronScheduleBuilder.cronSchedule(obj.getCronexpression())).build();
				s.rescheduleJob(TriggerKey.triggerKey(obj.getJobname(), GROUP_NAME), t);
				return true;
			} else {
				return false;
			}
		} catch (SchedulerException e) {
			LOGGER.error(e);
			return false;
		}
	}

	/**
	 * 
	 * @param obj
	 * @return
	 */
	public static boolean pauseScheduler(Scheduler obj) {
		try {
			if (obj != null && obj.getJobname() != null) {
				org.quartz.Scheduler s = new StdSchedulerFactory().getScheduler();
				s.pauseTrigger(TriggerKey.triggerKey(obj.getJobname(), GROUP_NAME));
				return true;
			} else {
				return false;
			}
		} catch (SchedulerException e) {
			LOGGER.error(e);
			return false;
		}
	}

	/**
	 * 
	 * @param obj
	 * @return
	 */
	public static boolean deleteScheduler(Scheduler obj) {
		try {
			if (obj != null) {
				org.quartz.Scheduler s = new StdSchedulerFactory().getScheduler();
				s.deleteJob(new JobKey(Integer.toString(obj.getId()), GROUP_NAME));
				return true;
			} else {
				return false;
			}
		} catch (SchedulerException e) {
			LOGGER.error(e);
			return false;
		}
	}

}
