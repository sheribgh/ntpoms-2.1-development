/*
 * ********************************************************************************
s * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.poadmin.web.controllers;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static my.com.parkson.ntpoms.common.MessageCodes.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.UriUtils;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.LogMessages;
import my.com.parkson.ntpoms.common.entities.OrderCategory;
import my.com.parkson.ntpoms.common.entities.OrderSubCategory;
import my.com.parkson.ntpoms.common.entities.ProfitCenter;
import my.com.parkson.ntpoms.common.entities.PurchaseRequisition;
import my.com.parkson.ntpoms.common.entities.SystemParameter;
import my.com.parkson.ntpoms.common.repositories.OrderCategoryRepository;
import my.com.parkson.ntpoms.common.repositories.OrderSubCategoryRepository;
import my.com.parkson.ntpoms.common.services.AccessLogService;
import my.com.parkson.ntpoms.common.services.AuditService;
import my.com.parkson.ntpoms.common.services.MessageService;
import my.com.parkson.ntpoms.common.services.OrderCategoryService;
import my.com.parkson.ntpoms.common.services.OrderSubCategoryService;
import my.com.parkson.ntpoms.common.services.PurchaseRequisitionService;
import my.com.parkson.ntpoms.common.services.SystemParameterService;
import my.com.parkson.ntpoms.poadmin.security.SecurityUtil;
import my.com.parkson.ntpoms.poadmin.web.models.OrderCategoryForm;
import my.com.parkson.ntpoms.poadmin.web.models.OrderSubCategoryForm;
import my.com.parkson.ntpoms.poadmin.web.models.Pager;

@Slf4j
@Controller
@Secured(SecurityUtil.MANAGE_ORDER)
@RequiredArgsConstructor
public class OrderController extends AbstractAdminController {

	private static final int BUTTONS_TO_SHOW = 5;
	private static final int INITIAL_PAGE = 0;
	private static final int INITIAL_PAGE_SIZE = 5;
	private static final int[] PAGE_SIZES = { 5, 10, 20 };
	private static String AUTHGRID_UPLOAD_PATH = "AUTHGRID_FOLDER_PATH";

	private String authDir;

	private static Logger LOGGER = Logger.getLogger(OrderController.class);

	/** The messages. */
	@Autowired
	private MessageService messages;

	@Autowired
	private AuditService auditService;

	@Autowired
	MessageService messageService;

	@Autowired
	private OrderCategoryService orderrepo;
	@Autowired
	private OrderSubCategoryService ordersubrepo;

	@Autowired
	private OrderCategoryRepository orderrepository;

	@Autowired
	private OrderSubCategoryRepository ordersubrepository;

	@Autowired
	private PurchaseRequisitionService purreqrepo;

	private OrderCategoryForm tempobj;

	private OrderSubCategoryForm tempsubobj;

	@Autowired
	private SystemParameterService sysrepo;

	boolean edit = false;

	/**
	 * On page load order.
	 *
	 * @param pageSize
	 *            the page size
	 * @param page
	 *            the page
	 * @param code
	 *            the code
	 * @param purchaseRequisition
	 *            the purchase requisition
	 * @param codeabbr
	 *            the codeabbr
	 * @param isActive
	 *            the is active
	 * @return the model and view
	 */
	@GetMapping("/master/ordercategory")
	public ModelAndView onPageLoadOrder() {

		authDir = sysrepo.getSystemParameterValueByPropName(AUTHGRID_UPLOAD_PATH);
		ModelAndView modelAndView = new ModelAndView("/master/ordercategory");
		modelAndView.addObject("purrelist", getProfitCenterCodeAndProfitcenterName());

		modelAndView.getModelMap().addAttribute("orderobj", tempobj != null ? tempobj : new OrderCategoryForm());
		return modelAndView;
	}

	@RequestMapping(value = "/master/ordercategorygrid", method = RequestMethod.GET)
	public String orderGrid(@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page, @RequestParam("code") Optional<String> code,
			@RequestParam("purchaseRequisition") Optional<String> purchaseRequisition,
			@RequestParam("abbrDesc") Optional<String> codeabbr, @RequestParam("status") Optional<String> isActive,
			@RequestParam("sortingkey") Optional<String> sortingkey,
			@RequestParam("sortingorder") Optional<String> sortingorder, Model model) {

		authDir = sysrepo.getSystemParameterValueByPropName(AUTHGRID_UPLOAD_PATH);

		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
		Page<OrderCategory> list;

		Specification<OrderCategory> s = new Specification<OrderCategory>() {
			@Override
			public Predicate toPredicate(Root<OrderCategory> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> ls = new ArrayList<>();
				if (code.isPresent() && !code.get().isEmpty()) {

					ls.add(cb.like(cb.lower(root.get("code")), "%" + code.get().toLowerCase() + "%"));
				}
				if (purchaseRequisition.isPresent() && !purchaseRequisition.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("purchaseRequisition").get("typeCode")),
							"%" + purchaseRequisition.get().toLowerCase() + "%"));
				}
				if (codeabbr.isPresent() && !codeabbr.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("abbrDesc")), "%" + codeabbr.get().toLowerCase() + "%"));
				}
				if (isActive.isPresent() && !isActive.get().isEmpty() && !isActive.get().equals("All")) {
					ls.add(cb.equal(root.get("isActive"), "Active".equals(isActive.get())));
				}
				return cb.and(ls.toArray(new Predicate[0]));
			}
		};
		Sort sorttemp = new Sort(Sort.Direction.DESC, "lastModifiedOn");

		if (sortingkey.isPresent() && sortingorder.isPresent()) {
			sorttemp = new Sort(sortingorder.get().equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC,
					sortingkey.get());
		}
		if (code.isPresent() || purchaseRequisition.isPresent() || codeabbr.isPresent() || isActive.isPresent()) {
			list = orderrepo.getAllPageableOrderCategory(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		} else {
			list = orderrepo.getAllPageableOrderCategoryOrdered(new PageRequest(evalPage, evalPageSize, sorttemp));
		}

		int serialid = 0;

		int pagesize = evalPage + 1;

		if (pagesize > 1) {
			int start = evalPageSize * pagesize;
			serialid = start - evalPageSize;
		}

		Pager pager = new Pager(list.getTotalPages(), list.getNumber(), BUTTONS_TO_SHOW);
		model.addAttribute("serialid", serialid);
		model.addAttribute("list", list);
		model.addAttribute("selectedPageSize", evalPageSize);
		model.addAttribute("pageSizes", PAGE_SIZES);
		model.addAttribute("pager", pager);
		model.addAttribute("totalRecords", list.getTotalElements());
		model.addAttribute("purrelist", purreqrepo.getAllActivePurchaseRequisition());
		model.addAttribute("code", code.isPresent() ? code.get() : "");
		model.addAttribute("abbrDesc", codeabbr.isPresent() ? codeabbr.get() : "");
		model.addAttribute("purchaseRequisition", purchaseRequisition.isPresent() ? purchaseRequisition.get() : "");
		model.addAttribute("status", isActive.isPresent() ? isActive.get() : "");
		model.addAttribute("sortingorder", sortingorder.isPresent() ? sortingorder.get() : "DESC");
		model.addAttribute("sortingkey", sortingkey.isPresent() ? sortingkey.get() : "lastModifiedOn");

		return "/master/ordercategory :: result-table";
	}

	/**
	 * Creates the order category.
	 *
	 * @param obj
	 *            the obj
	 * @param authgrid
	 *            the authgrid
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @param redirectAttributes
	 *            the redirect attributes
	 * @return the string
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@PostMapping(value = "/master/createordercategory")
	public String createOrderCategory(@Valid @ModelAttribute("orderobj") OrderCategoryForm obj,
			@RequestParam("authgrid") MultipartFile authgrid, BindingResult result, Model model,
			RedirectAttributes redirectAttributes) throws IOException {
		if (!result.hasErrors() && orderrepo.getOrderCategoryByCode(obj.getOrdCatCode()) == null) {
			OrderCategory ad = new OrderCategory();
			ad.setCreatedBy(getCurrentUser() != null ? getCurrentUser().getUsername() : null);
			ad.setCreatedOn(new Date());
			ad.setLastModifiedBy(getCurrentUser() != null ? getCurrentUser().getUsername() : null);
			if (authgrid != null && !authgrid.isEmpty()) {
				String[] orderAuthSplit = authgrid.getContentType().split("/");
				if (orderAuthSplit.length > 1) {

					authDir = sysrepo.getSystemParameterValueByPropName(AUTHGRID_UPLOAD_PATH);

					if (authDir == null) {
						redirectAttributes.addFlashAttribute("error",
								"System Parameter " + AUTHGRID_UPLOAD_PATH + " does not exist.");
						return "redirect:/master/ordercategory";
					} else {
						ad.setAuthorizationGrid(obj.getOrdCatCode() + "_AUTHGRID" + "." + orderAuthSplit[1]);
						File destination = new File(
								authDir + obj.getOrdCatCode() + "_AUTHGRID" + "." + orderAuthSplit[1]);
						authgrid.transferTo(destination);
					}
				}

			}
			ad.setLastModifiedOn(new Date());

			String objprtypecode = obj.getMf15MPrmast_mf15_prTypeCode();
			String[] output = objprtypecode.split("-");

			ad.setPurchaseRequisition(purreqrepo.getPurchaseRequisitionByTypeCode(output[0]));
			ad.setCode(obj.getOrdCatCode());
			ad.setDesc(obj.getOrdCatDesc());
			ad.setActive(true);
			ad.setAbbrDesc(obj.getOrdCatAbbrDesc());

			try {
				orderrepo.createOrderCategory(ad);
				auditService.putAudit(new Date(), "Order Category: " + obj.getOrdCatCode() + " created successfully",
						getCurrentUser().getUsername(), "Create Order Category");
				redirectAttributes.addFlashAttribute("info", "Order Category created successfully");
				tempobj = null;
			} catch (Exception e) {
				LOGGER.error(e);
			}

		} else {
			redirectAttributes.addFlashAttribute("error", "Order Category already exists");
			tempobj = obj;
		}
		return "redirect:/master/ordercategory";
	}

	/**
	 * Update order category.
	 *
	 * @param obj
	 *            the obj
	 * @param authgrid
	 *            the authgrid
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @param redirectAttributes
	 *            the redirect attributes
	 * @return the string
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@PostMapping(value = "/master/updateordercategory")
	public String updateOrderCategory(@Valid @ModelAttribute("orderobj") OrderCategoryForm obj,
			@RequestParam("authgrid") MultipartFile authgrid, BindingResult result, Model model,
			RedirectAttributes redirectAttributes) throws IOException {
		if (!result.hasErrors()) {
			OrderCategory ad = orderrepo.getOrderCategoryByCode(obj.getOrdCatCode());
			boolean isActive;
			try {
				if (ad != null) {
					ad.setLastModifiedBy(getCurrentUser() != null ? getCurrentUser().getUsername() : null);
					ad.setLastModifiedOn(new Date());
					if (obj.getIsActive().contains("InActive")) {
						isActive = false;
					} else {
						isActive = true;
					}

					if (isActive != ad.isActive()) {

						if (obj.getIsActive().contains("InActive")) {
							ad.setDeactivatedBy(getCurrentUser().getUsername());
							ad.setDeactivatedOn(new Date());
						} else {
							ad.setReactivatedOn(new Date());
							ad.setReactivatedBy(getCurrentUser().getUsername());
						}
					}

					if (authgrid != null && !authgrid.isEmpty()) {

						String[] orderAuthSplit = authgrid.getContentType().split("/");

						if (orderAuthSplit.length > 1) {
							try {
								if (orderAuthSplit[1].contains("jpg") || orderAuthSplit[1].contains("jpeg")
										|| orderAuthSplit[1].contains("png") || orderAuthSplit[1].contains("pdf")) {
									if (authDir == null) {
										redirectAttributes.addFlashAttribute("error",
												"Order Category Object not found");
										return "redirect:/master/ordercategory";

									} else {

										ad.setAuthorizationGrid(
												obj.getOrdCatCode() + "_AUTHGRID" + "." + orderAuthSplit[1]);
										File destination = new File(
												authDir + obj.getOrdCatCode() + "_AUTHGRID" + "." + orderAuthSplit[1]);

										authgrid.transferTo(destination);

									}
								}
							} catch (Exception e) {
								LOGGER.error(e);
							}
						}
					}
					ad.setPurchaseRequisition(
							purreqrepo.getPurchaseRequisitionByTypeCode(obj.getMf15MPrmast_mf15_prTypeCode()));
					ad.setCode(obj.getOrdCatCode());
					ad.setDesc(obj.getOrdCatDesc());
					ad.setActive("Active".equals(obj.getIsActive()));
					ad.setAbbrDesc(obj.getOrdCatAbbrDesc());
					orderrepo.createOrderCategory(ad);
					auditService.putAudit(new Date(),
							"Order Category: " + obj.getOrdCatCode() + " updated successfully",
							getCurrentUser().getUsername(), "Updated Order Category");
					redirectAttributes.addFlashAttribute("info", "Order Category updated successfully");
				} else {
					redirectAttributes.addFlashAttribute("error", "Order Category Object not found");
					return "/master/edit_ordercategory";
				}
			} catch (Exception e) {
				LOGGER.error(e);
			}
		}
		return "redirect:/master/ordercategory";
	}

	@RequestMapping(value = "/master/order/setauthGrid", method = RequestMethod.GET)
	public @ResponseBody OrderCategory setAuthGrid(@RequestParam("ordcatcode") String ordcatcode,
			@RequestParam("authgrid") String authgrid, RedirectAttributes redirectAttributes, Model model) {

		OrderCategory ord = orderrepo.getOrderCategoryByCode(ordcatcode);

		ord.setAuthorizationGrid(authgrid);
		orderrepo.createOrderCategory(ord);

		return ord;

	}

	/**
	 * Edits the order category.
	 *
	 * @param catcode
	 *            the catcode
	 * @param model
	 *            the model
	 * @return the string
	 */
	@GetMapping(value = "/master/editordercategory/{catcode}")
	public String editOrderCategory(@PathVariable String catcode, Model model, RedirectAttributes redirectAttributes) {
		OrderCategory ad = orderrepo.getOrderCategoryByCode(catcode);
		edit = true;
		if (ad != null) {
			OrderCategoryForm obj = new OrderCategoryForm();
			String date_format = "dd-MM-yyyy hh:mm:ss";
			SystemParameter dateObj = sysrepo.getSystemParameterByPropName("DATE_TIME_FORMAT");
			if (dateObj != null) {
				if (!dateObj.getPropValue().isEmpty()) {
					date_format = dateObj.getPropValue();
				}
			}
			DateFormat formatter = new SimpleDateFormat(date_format);
			String createdOn = "";
			String modifiedOn = "";
			String reactivatedOn = "";
			String deactivatedOn = "";
			if (ad.getCreatedOn() != null) {
				createdOn = formatter.format(ad.getCreatedOn());
			}
			if (ad.getLastModifiedOn() != null) {
				modifiedOn = formatter.format(ad.getLastModifiedOn());
			}
			if (ad.getReactivatedOn() != null) {
				reactivatedOn = formatter.format(ad.getReactivatedOn());
			}
			if (ad.getDeactivatedOn() != null) {
				deactivatedOn = formatter.format(ad.getDeactivatedOn());
			}
			obj.setCreatedOn(createdOn);
			obj.setDeactivatedOn(deactivatedOn);
			obj.setReactivatedOn(reactivatedOn);
			obj.setLastModifiedOn(modifiedOn);
			obj.setMf15MPrmast_mf15_prTypeCode(
					ad.getPurchaseRequisition() != null ? ad.getPurchaseRequisition().getTypeCode() : null);
			obj.setIsActive(ad.isActive() ? "Active" : "InActive");
			obj.setOrdCatCode(ad.getCode());
			obj.setOrdCatAbbrDesc(ad.getAbbrDesc());
			obj.setOrdCatDesc(ad.getDesc());
			obj.setCreatedBy(ad.getCreatedBy());

			if (ad.getAuthorizationGrid() != null) {
				obj.setOrdCatAuthorizationGrid(ad.getAuthorizationGrid());
				obj.setAuthgrid_hidden(new File(ad.getAuthorizationGrid()).getName());
			}

			obj.setLastModifiedBy(ad.getLastModifiedBy());
			obj.setReactivatedBy(ad.getReactivatedBy());
			obj.setDeactivatedBy(ad.getDeactivatedBy());

			model.addAttribute("edit", edit);
			model.addAttribute("orderobj", obj);
			model.addAttribute("purrelist", purreqrepo.getAllActivePurchaseRequisition());

			if (obj.getOrdCatAuthorizationGrid() != null) {

				try {
					String filedir = authDir;
					String fileName = obj.getOrdCatAuthorizationGrid();

					File chkFileType = new File(fileName);

					String filetype = getFileExtension(chkFileType);

					String newPath = UriUtils.encodePath(filedir + fileName, "UTF-8");

					model.addAttribute("order_auth_name", fileName);
					model.addAttribute("order_auth_file", newPath);
					model.addAttribute("File_Auth_Grid_ExtensionType", filetype);

					boolean chkauthExist = authExist(new File(ad.getAuthorizationGrid()));
					if (chkauthExist == false) {

						model.addAttribute("missed", chkauthExist);
					}
				} catch (UnsupportedEncodingException e) {
					redirectAttributes.addFlashAttribute("error", "Unable to find Authentication Grid File");
				}
			}

			return "/master/edit_ordercategory";
		} else {
			return "redirect:/master/ordercategory";
		}
	}

	/**
	 * On page load sub order.
	 *
	 * @param pageSize
	 *            the page size
	 * @param page
	 *            the page
	 * @param orderCategory
	 *            the order category
	 * @param code
	 *            the code
	 * @param desc
	 *            the desc
	 * @param isActive
	 *            the is active
	 * @return the model and view
	 */
	@GetMapping("/master/ordersubcategory")
	public ModelAndView onPageLoadSubOrder() {

		ModelAndView modelAndView = new ModelAndView("/master/ordersubcategory");
		modelAndView.addObject("orderlist", getOrderCodeAndProfitcenterName());
		modelAndView.getModelMap().addAttribute("ordersubobj",
				tempsubobj != null ? tempsubobj : new OrderSubCategoryForm());
		return modelAndView;
	}

	@RequestMapping(value = "/master/ordersubcategorygrid", method = RequestMethod.GET)

	public String ordersubGrid(@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page, @RequestParam("code") Optional<String> incodedValue,
			@RequestParam("orderCategory") Optional<String> incodedValueordCode,
			@RequestParam("abbrDesc") Optional<String> desc, @RequestParam("status") Optional<String> isActive,
			@RequestParam("sortingkey") Optional<String> sortingkey,
			@RequestParam("sortingorder") Optional<String> sortingorder, Model model) {

		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
		Page<OrderSubCategory> list;

		Specification<OrderSubCategory> s = new Specification<OrderSubCategory>() {
			@Override
			public Predicate toPredicate(Root<OrderSubCategory> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> ls = new ArrayList<>();

				if (incodedValueordCode.isPresent() && !incodedValueordCode.get().isEmpty()) {

					ls.add(cb.like(cb.lower(root.get("orderCategory").get("code")),
							"%" + incodedValueordCode.get().toLowerCase() + "%"));
				}
				if (incodedValue.isPresent() && !incodedValue.get().isEmpty()) {

					ls.add(cb.like(cb.lower(root.get("code")), "%" + incodedValue.get().toLowerCase() + "%"));
				}

				if (desc.isPresent() && !desc.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("desc")), "%" + desc.get().toLowerCase() + "%"));
				}
				if (isActive.isPresent() && !isActive.get().isEmpty() && !isActive.get().equals("All")) {
					ls.add(cb.equal(root.get("isActive"), "Active".equals(isActive.get())));
				}

				return cb.and(ls.toArray(new Predicate[0]));
			}
		};
		Sort sorttemp = new Sort(Sort.Direction.DESC, "lastModifiedOn");

		if (sortingkey.isPresent() && sortingorder.isPresent()) {
			sorttemp = new Sort(sortingorder.get().equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC,
					sortingkey.get());
		}
		if (incodedValueordCode.isPresent() || incodedValue.isPresent() || desc.isPresent() || isActive.isPresent()) {
			list = ordersubrepo.getAllPageableOrderSubCategory(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		} else {
			list = ordersubrepo
					.getAllPageableOrderSubCategoryOrdered(new PageRequest(evalPage, evalPageSize, sorttemp));
		}

		int serialid = 0;

		int pagesize = evalPage + 1;

		if (pagesize > 1) {
			int start = evalPageSize * pagesize;
			serialid = start - evalPageSize;
		}

		Pager pager = new Pager(list.getTotalPages(), list.getNumber(), BUTTONS_TO_SHOW);
		model.addAttribute("serialid", serialid);
		model.addAttribute("list", list);
		model.addAttribute("selectedPageSize", evalPageSize);
		model.addAttribute("pageSizes", PAGE_SIZES);
		model.addAttribute("pager", pager);
		model.addAttribute("totalRecords", list.getTotalElements());
		model.addAttribute("orderlist", orderrepo.getAllActiveOrderCategory());
		model.addAttribute("code", incodedValue.isPresent() ? incodedValue.get() : "");
		model.addAttribute("orderCategory", incodedValueordCode.isPresent() ? incodedValueordCode.get() : "");
		model.addAttribute("abbrDesc", desc.isPresent() ? desc.get() : "");
		model.addAttribute("status", isActive.isPresent() ? isActive.get() : "");
		model.addAttribute("sortingorder", sortingorder.isPresent() ? sortingorder.get() : "DESC");
		model.addAttribute("sortingkey", sortingkey.isPresent() ? sortingkey.get() : "lastModifiedOn");

		return "/master/ordersubcategory :: result-table";
	}

	@GetMapping(value = "/master/deleteordercategory/{code}")
	public String deleteordercategory(@PathVariable String code, Model model) {

		OrderCategory ad = orderrepo.getOrderCategoryByCode(code);
		if (ad != null) {
			orderrepository.delete(ad);

		}
		return "redirect:/master/ordercategory";
	}

	@GetMapping(value = "/master/deleteordersubcategory/{code}")
	public String deleteordersubcategory(@PathVariable String code, Model model) {

		OrderSubCategory ad = ordersubrepo.getOrderSubCategoryByCode(code);
		if (ad != null) {
			ordersubrepository.delete(ad);

		}
		return "redirect:/master/ordersubcategory";
	}

	/**
	 * Creates the order sub category.
	 *
	 * @param obj
	 *            the obj
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @param redirectAttributes
	 *            the redirect attributes
	 * @return the string
	 */
	@PostMapping(value = "/master/createordersubcategory")
	public String createOrderSubCategory(@Valid @ModelAttribute("ordersubobj") OrderSubCategoryForm obj,
			BindingResult result, Model model, RedirectAttributes redirectAttributes) {

		OrderSubCategory ad = new OrderSubCategory();
		ad.setActive(true);
		ad.setCreatedBy(getCurrentUser() != null ? getCurrentUser().getUsername() : null);
		ad.setCreatedOn(new Date());
		ad.setLastModifiedBy(getCurrentUser() != null ? getCurrentUser().getUsername() : null);
		ad.setLastModifiedOn(new Date());

		String objcode = obj.getOderCategory();
		String[] output = objcode.split("-");
		ad.setOrderCategory(orderrepo.getOrderCategoryByCode(output[0]));
		ad.setCode(obj.getOrdSubCCode());
		ad.setDesc(obj.getOrdSubCDesc());
		ad.setAbbrDesc(obj.getOrdSubCAbbrDesc());
		try {
			ordersubrepo.createOrderSubCategory(ad);
			auditService.putAudit(new Date(), "Order Sub Category: " + obj.getOrdSubCCode() + " created successfully",
					getCurrentUser().getUsername(), "Create Order Sub Category");
			redirectAttributes.addFlashAttribute("info", "Order Sub Category created successfully");
			tempobj = null;
		} catch (Exception e) {
			LOGGER.error(e);
		}

		return "redirect:/master/ordersubcategory";
	}

	/**
	 * Update order sub category.
	 *
	 * @param obj
	 *            the obj
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @param redirectAttributes
	 *            the redirect attributes
	 * @return the string
	 */
	@PostMapping(value = "/master/updateordersubcategory")
	public String updateOrderSubCategory(@Valid @ModelAttribute("ordersubobj") OrderSubCategoryForm obj,
			BindingResult result, Model model, RedirectAttributes redirectAttributes) {
		if (!result.hasErrors()) {
			OrderSubCategory ad = ordersubrepo.getOrderSubCategoryByCode(obj.getOrdSubCCode());
			boolean isActive;

			try {
				if (ad != null) {
					if (obj.getIsActive().contains("InActive")) {
						isActive = false;
					} else {
						isActive = true;
					}

					if (isActive != ad.isActive()) {

						if (obj.getIsActive().contains("InActive")) {
							ad.setDeactivatedBy(getCurrentUser().getUsername());
							ad.setDeactivatedOn(new Date());
						} else {
							ad.setReactivatedOn(new Date());
							ad.setReactivatedBy(getCurrentUser().getUsername());
						}
					}
					ad.setActive("Active".equals(obj.getIsActive()));
					ad.setLastModifiedBy(getCurrentUser() != null ? getCurrentUser().getUsername() : null);
					ad.setLastModifiedOn(new Date());
					ad.setOrderCategory(orderrepo.getOrderCategoryByCode(obj.getOderCategory()));
					ad.setCode(obj.getOrdSubCCode());
					ad.setDesc(obj.getOrdSubCDesc());
					ad.setAbbrDesc(obj.getOrdSubCAbbrDesc());
					ordersubrepo.createOrderSubCategory(ad);
					auditService.putAudit(new Date(),
							"Order Sub Category: " + obj.getOrdSubCCode() + " updated successfully",
							getCurrentUser().getUsername(), "Update Order Sub Category");
					redirectAttributes.addFlashAttribute("info", "Order Sub Category updated successfully");
				} else {
					redirectAttributes.addFlashAttribute("error", "Order Sub Category object not exists");
					return "/master/edit_ordersubcategory";
				}
			} catch (Exception e) {
				LOGGER.error(e);
			}
		}
		return "redirect:/master/ordersubcategory";
	}

	/**
	 * Edits the ordersub category.
	 *
	 * @param catcode
	 *            the catcode
	 * @param model
	 *            the model
	 * @return the string
	 */
	@GetMapping(value = "/master/editordersubcategory/{catcode}")
	public String editOrdersubCategory(@PathVariable String catcode, Model model) {
		OrderSubCategory ad = ordersubrepo.getOrderSubCategoryByCode(catcode);
		edit = true;
		if (ad != null) {
			OrderSubCategoryForm obj = new OrderSubCategoryForm();
			String date_format = "dd-MM-yyyy hh:mm:ss";
			SystemParameter dateObj = sysrepo.getSystemParameterByPropName("DATE_TIME_FORMAT");
			if (dateObj != null) {
				if (!dateObj.getPropValue().isEmpty()) {
					date_format = dateObj.getPropValue();
				}
			}
			DateFormat formatter = new SimpleDateFormat(date_format);
			String createdOn = "";
			String modifiedOn = "";
			String reactivatedOn = "";
			String deactivatedOn = "";
			if (ad.getCreatedOn() != null) {
				createdOn = formatter.format(ad.getCreatedOn());
			}
			if (ad.getLastModifiedOn() != null) {
				modifiedOn = formatter.format(ad.getLastModifiedOn());
			}
			if (ad.getReactivatedOn() != null) {
				reactivatedOn = formatter.format(ad.getReactivatedOn());
			}
			if (ad.getDeactivatedOn() != null) {
				deactivatedOn = formatter.format(ad.getDeactivatedOn());
			}
			obj.setCreatedOn(createdOn);
			obj.setDeactivatedOn(deactivatedOn);
			obj.setReactivatedOn(reactivatedOn);
			obj.setLastModifiedOn(modifiedOn);
			obj.setIsActive(ad.isActive() ? "Active" : "InnActive");
			obj.setOrdSubCCode(ad.getCode());
			obj.setOrdSubCDesc(ad.getDesc());
			obj.setOrdSubCAbbrDesc(ad.getAbbrDesc());
			OrderCategory temp = orderrepo.getOrderCategoryByCode(ad.getOrderCategory().toString());
			obj.setOderCategory(temp.getCode() + "-" + temp.getAbbrDesc());
			obj.setCreatedBy(ad.getCreatedBy());
			obj.setLastModifiedBy(ad.getLastModifiedBy());
			obj.setDeactivatedBy(ad.getDeactivatedBy());
			obj.setReactivatedBy(ad.getReactivatedBy());
			model.addAttribute("ordersubobj", obj);
			model.addAttribute("orderlist", getOrderCodeAndProfitcenterName());
			model.addAttribute("edit", edit);
			return "/master/edit_ordersubcategory";

		} else {
			return "redirect:/master/ordersubcategory";
		}
	}

	@Override
	protected String getHeaderTitle() {
		return "Manage Order";
	}

	private String getFileExtension(File file) {
		String fileName = file.getName();
		if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
			return fileName.substring(fileName.lastIndexOf(".") + 1);
		else
			return "";
	}

	private boolean authExist(File fileName) {
		String[] filename = fileName.getName().split("_");

		boolean authexist = false;

		if (authDir != null) {
			File[] files = new File(authDir).listFiles();
			for (File file : files) {
				if (file.isFile()) {

					if (file.getName().contains(filename[0])) {
						authexist = true;

					}

				}

			}

		}
		return authexist;
	}

	/**
	 * Gets the store based on company.
	 *
	 * @param companycode
	 *            the companycode
	 * @return the store based on company
	 */
	@RequestMapping(value = "/master/ordercat/existingordcat", method = RequestMethod.GET)
	public @ResponseBody OrderCategory checkOrder(@RequestParam("ordcat") String ordcat,
			RedirectAttributes redirectAttributes, Model model) {

		return orderrepo.getOrderCategoryByCode(ordcat);

	}

	/**
	 * Gets the store based on company.
	 *
	 * @param companycode
	 *            the companycode
	 * @return the store based on company
	 */
	@RequestMapping(value = "/master/ordersubcat/existingordsubcat", method = RequestMethod.GET)
	public @ResponseBody OrderSubCategory checkOrderSub(@RequestParam("ordsubcat") String ordsubcat,
			RedirectAttributes redirectAttributes, Model model) {

		String decodeValue = "";
		try {
			decodeValue = UriUtils.decode(ordsubcat, "UTF-8");

		} catch (UnsupportedEncodingException e) {
			LOGGER.error(e);
		}

		return ordersubrepo.getOrderSubCategoryByCode(decodeValue);

	}

	private List<String> getProfitCenterCodeAndProfitcenterName() {
		List<String> lis = new ArrayList<>();
		List<PurchaseRequisition> temp = purreqrepo.getAllActivePurchaseRequisition();
		if (temp != null) {
			for (PurchaseRequisition e : temp) {
				StringBuilder s = new StringBuilder();
				s.append(e.getTypeCode());
				s.append("-");
				s.append(e.getAbbrName());
				lis.add(s.toString());
			}
		}
		return lis;
	}

	private List<String> getOrderCodeAndProfitcenterName() {
		List<String> lis = new ArrayList<>();
		List<OrderCategory> temp = orderrepo.getAllActiveOrderCategory();
		if (temp != null) {
			for (OrderCategory e : temp) {
				StringBuilder s = new StringBuilder();
				s.append(e.getCode());
				s.append("-");
				s.append(e.getAbbrDesc());
				lis.add(s.toString());
			}
		}
		return lis;
	}

}
