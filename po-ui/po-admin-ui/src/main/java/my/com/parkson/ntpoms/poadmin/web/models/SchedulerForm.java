/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.poadmin.web.models;

import java.io.Serializable;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class SchedulerForm implements Serializable {
	/**
		 * 
		 */
	private static final long serialVersionUID = -6029367793245107404L;

	private int schedulerId;

	private String task;

	private String status;

	@DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
	private Date startdate;

	private String cronfrequency_amount;

	private String cronfrequency_occurrence;

	private String table;

	private String database;

	private String scheduleType;
	@DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
	private Date endDate;

	private Boolean noenddate;

	private String upload;

	private String path;

	private String createdBy;

	private String createdOn;

	private String deactivatedBy;

	private String deactivatedOn;

	private String jobName;
	
	private String isActive;
	
	
	
	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	/**
	 * @return the schedulerId
	 */
	public int getSchedulerId() {
		return schedulerId;
	}

	/**
	 * @param schedulerId
	 *            the schedulerId to set
	 */
	public void setSchedulerId(int schedulerId) {
		this.schedulerId = schedulerId;
	}

	/**
	 * @return the task
	 */
	public String getTask() {
		return task;
	}

	/**
	 * @param task
	 *            the task to set
	 */
	public void setTask(String task) {
		this.task = task;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the startdate
	 */
	public Date getStartdate() {
		return startdate;
	}

	/**
	 * @param startdate
	 *            the startdate to set
	 */
	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}

	/**
	 * @return the cronfrequency_amount
	 */
	public String getCronfrequency_amount() {
		return cronfrequency_amount;
	}

	/**
	 * @param cronfrequency_amount
	 *            the cronfrequency_amount to set
	 */
	public void setCronfrequency_amount(String cronfrequency_amount) {
		this.cronfrequency_amount = cronfrequency_amount;
	}

	/**
	 * @return the cronfrequency_occurrence
	 */
	public String getCronfrequency_occurrence() {
		return cronfrequency_occurrence;
	}

	/**
	 * @param cronfrequency_occurrence
	 *            the cronfrequency_occurrence to set
	 */
	public void setCronfrequency_occurrence(String cronfrequency_occurrence) {
		this.cronfrequency_occurrence = cronfrequency_occurrence;
	}

	/**
	 * @return the table
	 */
	public String getTable() {
		return table;
	}

	/**
	 * @param table
	 *            the table to set
	 */
	public void setTable(String table) {
		this.table = table;
	}

	/**
	 * @return the database
	 */
	public String getDatabase() {
		return database;
	}

	/**
	 * @param database
	 *            the database to set
	 */
	public void setDatabase(String database) {
		this.database = database;
	}

	/**
	 * @return the scheduleType
	 */
	public String getScheduleType() {
		return scheduleType;
	}

	/**
	 * @param scheduleType
	 *            the scheduleType to set
	 */
	public void setScheduleType(String scheduleType) {
		this.scheduleType = scheduleType;
	}

	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate
	 *            the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the noenddate
	 */
	public Boolean getNoenddate() {
		return noenddate;
	}

	/**
	 * @param noenddate
	 *            the noenddate to set
	 */
	public void setNoenddate(Boolean noenddate) {
		this.noenddate = noenddate;
	}

	/**
	 * @return the upload
	 */
	public String getUpload() {
		return upload;
	}

	/**
	 * @param upload
	 *            the upload to set
	 */
	public void setUpload(String upload) {
		this.upload = upload;
	}

	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @param path
	 *            the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdOn
	 */
	public String getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn
	 *            the createdOn to set
	 */
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the deactivatedBy
	 */
	public String getDeactivatedBy() {
		return deactivatedBy;
	}

	/**
	 * @param deactivatedBy
	 *            the deactivatedBy to set
	 */
	public void setDeactivatedBy(String deactivatedBy) {
		this.deactivatedBy = deactivatedBy;
	}

	/**
	 * @return the deactivatedOn
	 */
	public String getDeactivatedOn() {
		return deactivatedOn;
	}

	/**
	 * @param deactivatedOn
	 *            the deactivatedOn to set
	 */
	public void setDeactivatedOn(String deactivatedOn) {
		this.deactivatedOn = deactivatedOn;
	}

	/**
	 * @return the jobName
	 */
	public String getJobName() {
		return jobName;
	}

	/**
	 * @param jobName
	 *            the jobName to set
	 */
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

}
