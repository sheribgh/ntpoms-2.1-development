/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.poadmin.web.controllers;

import static my.com.parkson.ntpoms.common.MessageCodes.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.LogMessages;
import my.com.parkson.ntpoms.common.entities.Department;
import my.com.parkson.ntpoms.common.entities.SystemParameter;
import my.com.parkson.ntpoms.common.repositories.DepartmentRepository;
import my.com.parkson.ntpoms.common.services.AccessLogService;
import my.com.parkson.ntpoms.common.services.AuditService;
import my.com.parkson.ntpoms.common.services.DepartmentService;
import my.com.parkson.ntpoms.common.services.MessageService;
import my.com.parkson.ntpoms.common.services.SystemParameterService;
import my.com.parkson.ntpoms.poadmin.security.SecurityUtil;
import my.com.parkson.ntpoms.poadmin.web.models.DepartmentForm;
import my.com.parkson.ntpoms.poadmin.web.models.Pager;

@Slf4j
@Controller
@Secured(SecurityUtil.MANAGE_DEPARTMENT)
@RequiredArgsConstructor
public class DepartmentController extends AbstractAdminController {

	private static final int BUTTONS_TO_SHOW = 5;
	private static final int INITIAL_PAGE = 0;
	private static final int INITIAL_PAGE_SIZE = 5;
	private static final int[] PAGE_SIZES = { 5, 10, 20 };

	private static Logger LOGGER = Logger.getLogger(DepartmentController.class);

	/** The messages. */
	@Autowired
	private MessageService messages;

	@Autowired
	private AuditService auditService;

	@Autowired
	MessageService messageService;

	private DepartmentForm tempobj;

	@Autowired
	private DepartmentService deptrepo;

	@Autowired
	private DepartmentRepository deptrepository;

	@Autowired
	private SystemParameterService sysRepo;

	boolean edit = false;

	/**
	 * On page load dept.
	 *
	 * @param pageSize
	 *            the page size
	 * @param page
	 *            the page
	 * @param code
	 *            the code
	 * @param name
	 *            the name
	 * @param isActive
	 *            the is active
	 * @return the model and view
	 */
	@GetMapping("/master/department")
	public ModelAndView onPageLoadDept() {
		ModelAndView modelAndView = new ModelAndView("/master/department");

		modelAndView.getModelMap().addAttribute("deptobj", tempobj != null ? tempobj : new DepartmentForm());
		return modelAndView;
	}

	@RequestMapping(value = "/master/departmentgrid", method = RequestMethod.GET)
	public String deptGrid(@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page, @RequestParam("code") Optional<String> code,
			@RequestParam("name") Optional<String> name, @RequestParam("status") Optional<String> isActive,
			@RequestParam("sortingkey") Optional<String> sortingkey,
			@RequestParam("sortingorder") Optional<String> sortingorder, Model model) {

		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
		Page<Department> list;
		Specification<Department> s = new Specification<Department>() {
			@Override
			public Predicate toPredicate(Root<Department> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> ls = new ArrayList<>();
				if (code.isPresent() && !code.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("code")), "%" + code.get().toLowerCase() + "%"));
				}

				if (name.isPresent() && !name.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("name")), "%" + name.get().toLowerCase() + "%"));
				}
				if (isActive.isPresent() && !isActive.get().isEmpty() && !isActive.get().equals("All")) {
					ls.add(cb.equal(root.get("isActive"), "Active".equals(isActive.get())));
				}

				return cb.and(ls.toArray(new Predicate[0]));
			}
		};

		Sort sorttemp = new Sort(Sort.Direction.DESC, "lastModifiedOn");

		if (sortingkey.isPresent() && sortingorder.isPresent()) {
			sorttemp = new Sort(sortingorder.get().equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC,
					sortingkey.get());
		}
		if (code.isPresent() || name.isPresent() || isActive.isPresent()) {
			list = deptrepo.getAllPagableDepartment(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		} else {
			list = deptrepo.getAllPageableDepartmentOrdered(new PageRequest(evalPage, evalPageSize, sorttemp));
		}

		int serialid = 0;

		int pagesize = evalPage + 1;

		if (pagesize > 1) {
			int start = evalPageSize * pagesize;
			serialid = start - evalPageSize;
		}

		Pager pager = new Pager(list.getTotalPages(), list.getNumber(), BUTTONS_TO_SHOW);
		model.addAttribute("serialid", serialid);
		model.addAttribute("list", list);
		model.addAttribute("selectedPageSize", evalPageSize);
		model.addAttribute("pageSizes", PAGE_SIZES);
		model.addAttribute("pager", pager);
		model.addAttribute("totalRecords", list.getTotalElements());
		model.addAttribute("code", code.isPresent() ? code.get() : "");
		model.addAttribute("name", name.isPresent() ? name.get() : "");
		model.addAttribute("status", isActive.isPresent() ? isActive.get() : "");
		model.addAttribute("sortingorder", sortingorder.isPresent() ? sortingorder.get() : "DESC");
		model.addAttribute("sortingkey", sortingkey.isPresent() ? sortingkey.get() : "lastModifiedOn");

		return "/master/department :: result-table";
	}

	/**
	 * Creates the department.
	 *
	 * @param obj
	 *            the obj
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @param redirectAttributes
	 *            the redirect attributes
	 * @return the string
	 */
	@PostMapping(value = "/master/createdepartment")
	public String createDepartment(@Valid @ModelAttribute("deptobj") DepartmentForm obj, BindingResult result,
			Model model, RedirectAttributes redirectAttributes) {
		if (!result.hasErrors() && obj.getDeptCode() != null
				&& deptrepo.getDepartmentByCode(obj.getDeptCode()) == null) {
			Department ad = new Department();
			ad.setCreatedBy(getCurrentUser() != null ? getCurrentUser().getUsername() : null);
			ad.setCreatedOn(new Date());
			ad.setLastModifiedBy(getCurrentUser() != null ? getCurrentUser().getUsername() : null);
			ad.setLastModifiedOn(new Date());
			ad.setActive(true);
			ad.setCode(obj.getDeptCode());
			ad.setAbbrName(obj.getDeptAbbrName());
			ad.setName(obj.getDeptName());
			try {
				deptrepo.createDepartment(ad);
				auditService.putAudit(new Date(), "Department: " + obj.getDeptCode() + "created successfully",
						getCurrentUser().getUsername(), " Create Department");
				redirectAttributes.addFlashAttribute("info", "Department created successfully");
				tempobj = null;
			} catch (Exception e) {
				LOGGER.error(e);
			}
		} else {
			redirectAttributes.addFlashAttribute("error", "Department code already exists");
			tempobj = obj;
		}
		return "redirect:/master/department";
	}

	/**
	 * Update deparment.
	 *
	 * @param obj
	 *            the obj
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @param redirectAttributes
	 *            the redirect attributes
	 * @return the string
	 */
	@PostMapping(value = "/master/updatedepartment")
	public String updateDeparment(@Valid @ModelAttribute("deptobj") DepartmentForm obj, BindingResult result,
			Model model, RedirectAttributes redirectAttributes) {
		if (!result.hasErrors()) {
			Department ad = deptrepo.getDepartmentByCode(obj.getDeptCode());
			boolean isActive;

			try {
				if (ad != null) {
					ad.setLastModifiedBy(getCurrentUser() != null ? getCurrentUser().getUsername() : null);
					ad.setLastModifiedOn(new Date());
					if (obj.getIsActive().contains("InActive")) {
						isActive = false;
					} else {
						isActive = true;
					}

					if (isActive != ad.isActive()) {

						if (obj.getIsActive().contains("InActive")) {
							ad.setDeactivatedBy(getCurrentUser().getUsername());
							ad.setDeactivatedOn(new Date());
						} else {
							ad.setReactivatedOn(new Date());
							ad.setReactivatedBy(getCurrentUser().getUsername());
						}
					}
					ad.setActive("Active".equals(obj.getIsActive()));
					ad.setCode(obj.getDeptCode());
					ad.setAbbrName(obj.getDeptAbbrName());
					ad.setName(obj.getDeptName());
					deptrepo.createDepartment(ad);
					auditService.putAudit(new Date(), "Department: " + obj.getDeptCode() + " updated successfully",
							getCurrentUser().getUsername(), " Update Department");

					redirectAttributes.addFlashAttribute("info", "Department updated successfully");
				} else {
					redirectAttributes.addFlashAttribute("error", "Department Object not exists");
					return "/master/edit_department";
				}
			} catch (Exception e) {
				LOGGER.error(e);
			}
		}
		return "redirect:/master/department";
	}

	/**
	 * Edits the deparment.
	 *
	 * @param code
	 *            the code
	 * @param model
	 *            the model
	 * @return the string
	 */
	@GetMapping(value = "/master/editdepartment/{code}")
	public String editDeparment(@PathVariable String code, Model model) {
		Department ad = deptrepo.getDepartmentByCode(code);
		edit = true;
		if (ad != null) {
			DepartmentForm obj = new DepartmentForm();
			String date_format = "dd-MM-yyyy hh:mm:ss";
			SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_TIME_FORMAT");
			if (dateObj != null) {
				if (!dateObj.getPropValue().isEmpty()) {
					date_format = dateObj.getPropValue();
				}
			}
			DateFormat formatter = new SimpleDateFormat(date_format);
			String createdOn = "";
			String modifiedOn = "";
			String reactivatedOn = "";
			String deactivatedOn = "";

			if (ad.getCreatedOn() != null) {
				createdOn = formatter.format(ad.getCreatedOn());
			}
			if (ad.getLastModifiedOn() != null) {
				modifiedOn = formatter.format(ad.getLastModifiedOn());
			}
			if (ad.getReactivatedOn() != null) {
				reactivatedOn = formatter.format(ad.getReactivatedOn());
			}
			if (ad.getDeactivatedOn() != null) {
				deactivatedOn = formatter.format(ad.getDeactivatedOn());
			}
			obj.setCreatedOn(createdOn);
			obj.setDeactivatedOn(deactivatedOn);
			obj.setReactivatedOn(reactivatedOn);
			obj.setLastModifiedOn(modifiedOn);
			obj.setDeptCode(ad.getCode());
			obj.setDeptAbbrName(ad.getAbbrName());
			obj.setDeptName(ad.getName());
			obj.setCreatedBy(ad.getCreatedBy());
			obj.setLastModifiedBy(ad.getLastModifiedBy());
			obj.setReactivatedBy(ad.getReactivatedBy());
			obj.setDeactivatedBy(ad.getDeactivatedBy());
			obj.setIsActive(ad.isActive() ? "Active" : "InActive");
			model.addAttribute("deptobj", obj);
			model.addAttribute("edit", edit);
			return "/master/edit_department";
		} else {
			return "redirect:/master/department";
		}
	}

	@GetMapping(value = "/master/deletedepartment/{code}")
	public String deleteDeparment(@PathVariable String code, Model model) {
		Department ad = deptrepo.getDepartmentByCode(code);
		if (ad != null) {
			deptrepository.delete(ad);
		}
		return "redirect:/master/department";

	}

	@Override
	protected String getHeaderTitle() {
		return "Manage Department";
	}

	/**
	 * Gets the store based on company.
	 *
	 * @param companycode
	 *            the companycode
	 * @return the store based on company
	 */
	@RequestMapping(value = "/master/department/existingdepartment", method = RequestMethod.GET)
	public @ResponseBody Department checkCompanyHRIS(@RequestParam("deptcode") String deptcode,
			RedirectAttributes redirectAttributes, Model model) {

		return deptrepo.getDepartmentByCode(deptcode);

	}

}
