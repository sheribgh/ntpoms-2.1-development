/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.poadmin.web.scheduler;

import java.util.regex.Pattern;

import my.com.parkson.ntpoms.POException;

public class CSVTools {
	
	/**
	 * Checks if is file header.
	 *
	 * @param s the s
	 * @param splitter the splitter
	 * @return true, if successful
	 */
	public static boolean isFileHeader(String s,String splitter) {
		boolean flag = false;
		if(s.substring(0, 2).equals("*"+splitter)) {
			flag = true;
		}
		return flag;
	}
	
	/**
	 * Checks if is table header.
	 *
	 * @param s the s
	 * @param splitter the splitter
	 * @return true, if successful
	 */
	public static boolean isTableHeader(String s,String splitter) {
		boolean flag = false;
		if(s.substring(0, 2).equals("T"+splitter)) {
			flag = true;
		}
		return flag;
	}
	
	/**
	 * Checks if is table data.
	 *
	 * @param s the s
	 * @param splitter the splitter
	 * @return true, if successful
	 */
	public static boolean isTableData(String s,String splitter) {
		boolean flag = false;
		if(s.substring(0, 2).equals("D"+splitter)) {
			flag = true;
		}
		return flag;
	}
	
	/**
	 * Checks if is data counter.
	 *
	 * @param s the s
	 * @param splitter the splitter
	 * @return true, if successful
	 */
	public static boolean isDataCounter(String s,String splitter) {
		boolean flag = false;
		if(s.substring(0, 2).equals("#"+splitter)) {
			flag = true;
		}
		return flag;
	}
	
	/**
	 * Gets the filename.
	 *
	 * @param header the header
	 * @param splitter the splitter
	 * @return the filename
	 */
	public static String getFilename(String header, String splitter) {
		try{
			String[] item = header.split(Pattern.quote(splitter));
			return item[1];
		} catch(POException e){
			return "";
		}
	}
	
	public static String getCompanyCode(String fileName) {
		if(fileName.length()>=21) {
			return fileName.substring(18,21);
		} else {
			return "";
		}
	}
	
	/**
	 * Gets the file type.
	 *
	 * @param str the str
	 * @return the file type
	 */
	public static String getFileType(String str) {
		if(str.length() < 2) {
			return str;
		}		
		return str;	
	}
	
	/**
	 * Gets the column count.
	 *
	 * @param line the line
	 * @param splitter the splitter
	 * @return the column count
	 */
	public static int getColumnCount(String line, String splitter) {
		try {
			int count = 0;
			String[] item = line.split(Pattern.quote(splitter));
			count = item.length;
			return count-1;
		} catch(POException e){
			return 0;
		}
	}
	
	/**
	 * Gets the data row count.
	 *
	 * @param line the line
	 * @param splitter the splitter
	 * @return the data row count
	 */
	public static int getDataRowCount(String line, String splitter) {
		try {
			String[] item = line.split(Pattern.quote(splitter));
			return Integer.parseInt(item[1]);
		} catch(POException e){
			return 0;
		}
	}
	
	/**
	 * Gets the last line type.
	 *
	 * @param str the str
	 * @return the last line type
	 */
	public static boolean getLastLineType(String str) {
		boolean flag = false;
		if(str.equals("#")) {
			flag = true;
		}
		return flag;
	}
}