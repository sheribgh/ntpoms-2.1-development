package my.com.parkson.ntpoms.poadmin.web.scheduler;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractFileTypeMaster implements IFileTypeMaster {
	protected static final String RESULT_MESSAGE_PREFIX = "The Master File [";
	protected static final String ROW_MESSSAGE_PREFIX = "Row [";
	
	protected String currentFileName = null;
	protected StringBuilder addMessage;
	
	protected boolean checkFailedRows(int row, int tableColumnCount, boolean validationSuccess,
			List<Integer> failedRows, int dataColumnCount, String[] dataItem) {
		if(dataColumnCount == tableColumnCount) {
			String storeCode = dataItem[1].substring(0, 4);
			String depCode = dataItem[1].substring(4, 7);
			validationSuccess = validateDataItems(row, validationSuccess, dataItem, storeCode, depCode);
			if(validationSuccess) {
				if(!upsert(dataItem)) {
					failedRows.add(row);
				}
			} else {
				failedRows.add(row);
			}
		} else {
			failedRows.add(row);
		}
		return validationSuccess;
	}

	protected boolean checkFileHeader(String line, String csvSplitBy, boolean validationSuccess) {
		if (CSVTools.isFileHeader(line, csvSplitBy)) {
			if (!currentFileName.equals(CSVTools.getFilename(line, csvSplitBy))) {
				validationSuccess = false;
				addMessage.append("File name does not match.");
				addMessage.append('\n');
			}
		} else {
			validationSuccess = false;
			addMessage.append("Header does not have proper format.");
			addMessage.append('\n');
		}
		return validationSuccess;
	}

	protected boolean checkRowCount(String line, String csvSplitBy, int dataRowCounter, boolean validationSuccess) {
		if (CSVTools.isDataCounter(line, csvSplitBy)) {
			if (CSVTools.getDataRowCount(line, csvSplitBy) != dataRowCounter) {
				validationSuccess = false;
				addMessage.append("Row count line does not have proper format.");
				addMessage.append('\n');
			}
		} else {
			validationSuccess = false;
			addMessage.append("Row count does not exist.");
			addMessage.append('\n');
		}
		return validationSuccess;
	}

	protected int getLintCount(BufferedReader bufferreader) throws IOException {
		int lineCount = 0;
		while ((bufferreader.readLine()) != null) {
			lineCount++;
		}
		return lineCount;
	}

	protected String getFailedRowMessages(int dataRowCounter, ArrayList<Integer> failedRows) {
		String resultMessage;
		if(failedRows.isEmpty()) {
			resultMessage = RESULT_MESSAGE_PREFIX + currentFileName + "] uploaded successfully." ;
		} else {
			if(failedRows.size() == dataRowCounter) {
				resultMessage = RESULT_MESSAGE_PREFIX + currentFileName + "] Failed to be uploaded. All rows failed, captured messages : " + addMessage.toString();
			} else {
				resultMessage = RESULT_MESSAGE_PREFIX + currentFileName + "] uploaded partially. Failed rows are: ";
				StringBuilder strBuilder = new StringBuilder(); 
				for(Integer failedRow : failedRows) {
					strBuilder.append(failedRow.toString()).append(", ");
				}
				resultMessage = resultMessage + strBuilder.toString();
			}
			if (resultMessage.lastIndexOf(',') == resultMessage.length()-2) {
				resultMessage = resultMessage.substring(0, resultMessage.length()-2);						
			}
		}
		return resultMessage;
	}

	protected abstract boolean validateDataItems(int row, boolean validationSuccess, String[] dataItem, String param1, String param2);
}