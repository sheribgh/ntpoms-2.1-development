/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.poadmin.web.controllers;

import static my.com.parkson.ntpoms.common.MessageCodes.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.LogMessages;
import my.com.parkson.ntpoms.common.entities.EmployeeMaster;
import my.com.parkson.ntpoms.common.entities.Job;
import my.com.parkson.ntpoms.common.entities.SystemParameter;
import my.com.parkson.ntpoms.common.repositories.JobRepository;
import my.com.parkson.ntpoms.common.services.AccessLogService;
import my.com.parkson.ntpoms.common.services.AuditService;
import my.com.parkson.ntpoms.common.services.EmployeeMasterService;
import my.com.parkson.ntpoms.common.services.JobService;
import my.com.parkson.ntpoms.common.services.MessageService;
import my.com.parkson.ntpoms.common.services.SystemParameterService;
import my.com.parkson.ntpoms.poadmin.security.SecurityUtil;
import my.com.parkson.ntpoms.poadmin.web.models.JobMasterForm;
import my.com.parkson.ntpoms.poadmin.web.models.Pager;

@Slf4j
@Controller
@Secured(SecurityUtil.MANAGE_EMPLOYEE_MANAGEMENT)
@RequiredArgsConstructor
public class EmployeeManagementController extends AbstractAdminController {

	private static final int BUTTONS_TO_SHOW = 5;
	private static final int INITIAL_PAGE = 0;
	private static final int INITIAL_PAGE_SIZE = 5;
	private static final int[] PAGE_SIZES = { 5, 10, 20 };

	private static Logger LOGGER = Logger.getLogger(EmployeeManagementController.class);

	/** The messages. */
	@Autowired
	private MessageService messages;

	@Autowired
	private AuditService auditService;

	@Autowired
	MessageService messageService;

	@Autowired
	private EmployeeMasterService emprepo;

	@Autowired
	private JobService jobmasrepo;

	@Autowired
	private JobRepository jobRepository;

	private JobMasterForm tempobj;

	@Autowired
	private SystemParameterService sysRepo;
	boolean edit = false;

	/**
	 * Emplist page.
	 *
	 * @param pageSize
	 *            the page size
	 * @param page
	 *            the page
	 * @param psId
	 *            the ps id
	 * @param altId
	 *            the alt id
	 * @param name
	 *            the name
	 * @param posTitle
	 *            the pos title
	 * @param email
	 *            the email
	 * @param paygrp
	 *            the paygrp
	 * @param isActive
	 *            the is active
	 * @param company
	 *            the company
	 * @param profitCenter
	 *            the profit center
	 * @return the model and view
	 */
	@GetMapping("/employee/emplist")
	public ModelAndView emplistPage() {
		ModelAndView modelAndView = new ModelAndView("/employee/emplist");
		return modelAndView;
	}

	@RequestMapping(value = "/employee/emplistgrid", method = RequestMethod.GET)
	public String emplistGrid(@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page, @RequestParam("psId") Optional<String> psId,
			@RequestParam("altId") Optional<String> altId, @RequestParam("name") Optional<String> name,
			@RequestParam("posTitle") Optional<String> posTitle, @RequestParam("email") Optional<String> email,
			@RequestParam("paygrp") Optional<String> paygrp, @RequestParam("status") Optional<String> isActive,
			@RequestParam("company") Optional<String> company,
			@RequestParam("profitCenter") Optional<String> profitCenter,
			@RequestParam("sortingkey") Optional<String> sortingkey,
			@RequestParam("sortingorder") Optional<String> sortingorder, Model model) {

		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
		Page<EmployeeMaster> list;
		Specification<EmployeeMaster> s = new Specification<EmployeeMaster>() {
			@Override
			public Predicate toPredicate(Root<EmployeeMaster> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> ls = new ArrayList<>();
				if (psId.isPresent() && !psId.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("psId")), "%" + psId.get().toLowerCase() + "%"));
				}
				if (altId.isPresent() && !altId.get().isEmpty()) {

					ls.add(cb.like(cb.lower(root.get("altId")), "%" + altId.get().toLowerCase() + "%"));
				}
				if (name.isPresent() && !name.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("name")), "%" + name.get().toLowerCase() + "%"));
				}
				if (posTitle.isPresent() && !posTitle.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("posTitle")), "%" + posTitle.get().toLowerCase() + "%"));
				}
				if (email.isPresent() && !email.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("email")), "%" + email.get().toLowerCase() + "%"));
				}
				if (paygrp.isPresent() && !paygrp.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("paygrp")), "%" + paygrp.get().toLowerCase() + "%"));
				}
				if (isActive.isPresent() && !isActive.get().isEmpty() && !isActive.get().equals("All")) {
					ls.add(cb.equal(root.get("isActive"), "Active".equals(isActive.get())));
				}
				if (company.isPresent() && !company.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("company").get("code")),
							"%" + company.get().toLowerCase() + "%"));
				}

				if (profitCenter.isPresent() && !profitCenter.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("profitCenter").get("code")),
							"%" + profitCenter.get().toLowerCase() + "%"));
				}

				return cb.and(ls.toArray(new Predicate[0]));
			}
		};

		Sort sorttemp = new Sort(Sort.Direction.DESC, "psId");

		if (sortingkey.isPresent() && sortingorder.isPresent()) {
			sorttemp = new Sort(sortingorder.get().equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC,
					sortingkey.get());
		}
		if (psId.isPresent() || altId.isPresent() || name.isPresent() || isActive.isPresent() || posTitle.isPresent()
				|| email.isPresent() || paygrp.isPresent() || company.isPresent() || profitCenter.isPresent()

		) {
			list = emprepo.getAllPagableEmployee(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		} else {
			list = emprepo.getAllPagableEmployee(new PageRequest(evalPage, evalPageSize, sorttemp));
		}

		int serialid = 0;

		int pagesize = evalPage + 1;

		if (pagesize > 1) {
			int start = evalPageSize * pagesize;
			serialid = start - evalPageSize;
		}

		Pager pager = new Pager(list.getTotalPages(), list.getNumber(), BUTTONS_TO_SHOW);
		model.addAttribute("serialid", serialid);
		model.addAttribute("list", list);
		model.addAttribute("selectedPageSize", evalPageSize);
		model.addAttribute("pageSizes", PAGE_SIZES);
		model.addAttribute("pager", pager);
		model.addAttribute("totalRecords", list.getTotalElements());
		model.addAttribute("psId", psId.isPresent() ? psId.get() : "");
		model.addAttribute("altId", altId.isPresent() ? altId.get() : "");
		model.addAttribute("name", name.isPresent() ? name.get() : "");
		model.addAttribute("status", isActive.isPresent() ? isActive.get() : "");
		model.addAttribute("posTitle", posTitle.isPresent() ? posTitle.get() : "");
		model.addAttribute("email", email.isPresent() ? email.get() : "");

		model.addAttribute("paygrp", paygrp.isPresent() ? paygrp.get() : "");
		model.addAttribute("company", company.isPresent() ? company.get() : "");
		model.addAttribute("profitCenter", profitCenter.isPresent() ? profitCenter.get() : "");
		model.addAttribute("sortingorder", sortingorder.isPresent() ? sortingorder.get() : "DESC");
		model.addAttribute("sortingkey", sortingkey.isPresent() ? sortingkey.get() : "psId");
		return "employee/emplist :: result-table";
	}

	/**
	 * View employee.
	 *
	 * @param psid
	 *            the psid
	 * @param model
	 *            the model
	 * @return the string
	 */
	@GetMapping("/employee/viewemployee/{psid}")
	public String viewEmployee(@PathVariable String psid, Model model) {
		EmployeeMaster tobj = emprepo.getEmployeeMasterByPsId(psid);
		edit = true;

		String date_format = "dd-MM-yyyy";
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_FORMAT");
		if (dateObj != null) {
			if (!dateObj.getPropValue().isEmpty()) {
				date_format = dateObj.getPropValue();
			}
		}

		if (tobj != null) {

			String status = String.valueOf(tobj.isActive()).toUpperCase();

			model.addAttribute("isactive", status);
			model.addAttribute("date_format", date_format);
			model.addAttribute("empobj", tobj);
			model.addAttribute("edit", edit);
			return "/employee/empview";
		} else {
			return "redirect:/employee/emplist";
		}
	}

	/**
	 * Jobmaster.
	 *
	 * @param pageSize
	 *            the page size
	 * @param page
	 *            the page
	 * @param code
	 *            the code
	 * @param abbrName
	 *            the abbr name
	 * @param isActive
	 *            the is active
	 * @return the model and view
	 */
	@GetMapping("/employee/jobmaster")
	public ModelAndView jobmaster() {
		ModelAndView modelAndView = new ModelAndView("/employee/jobmaster");

		modelAndView.getModelMap().addAttribute("jobobj", tempobj != null ? tempobj : new JobMasterForm());
		return modelAndView;
	}

	@RequestMapping(value = "/employee/jobmastergrid", method = RequestMethod.GET)
	public String jobmasterGrid(@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page, @RequestParam("code") Optional<String> code,
			@RequestParam("abbrName") Optional<String> abbrName, @RequestParam("status") Optional<String> isActive,
			@RequestParam("sortingkey") Optional<String> sortingkey,
			@RequestParam("sortingorder") Optional<String> sortingorder, Model model) {

		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
		Page<Job> list;
		Specification<Job> s = new Specification<Job>() {
			@Override
			public Predicate toPredicate(Root<Job> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> ls = new ArrayList<>();
				if (code.isPresent() && !code.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("code")), "%" + code.get().toLowerCase() + "%"));
				}
				if (abbrName.isPresent() && !abbrName.get().isEmpty()) {

					ls.add(cb.like(cb.lower(root.get("name")), "%" + abbrName.get().toLowerCase() + "%"));
				}

				if (isActive.isPresent() && !isActive.get().isEmpty() && !isActive.get().equals("All")) {
					ls.add(cb.equal(root.get("isActive"), "Active".equals(isActive.get())));
				}

				return cb.and(ls.toArray(new Predicate[0]));
			}
		};
		Sort sorttemp = new Sort(Sort.Direction.DESC, "lastModifiedOn");

		if (sortingkey.isPresent() && sortingorder.isPresent()) {
			sorttemp = new Sort(sortingorder.get().equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC,
					sortingkey.get());
		}
		if (code.isPresent() || abbrName.isPresent() || isActive.isPresent()) {
			list = jobmasrepo.getAllPagableJob(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		} else {
			list = jobmasrepo.getAllPagableJobOrdered(new PageRequest(evalPage, evalPageSize, sorttemp));
		}

		int serialid = 0;

		int pagesize = evalPage + 1;

		if (pagesize > 1) {
			int start = evalPageSize * pagesize;
			serialid = start - evalPageSize;
		}

		Pager pager = new Pager(list.getTotalPages(), list.getNumber(), BUTTONS_TO_SHOW);
		model.addAttribute("serialid", serialid);
		model.addAttribute("list", list);
		model.addAttribute("selectedPageSize", evalPageSize);
		model.addAttribute("pageSizes", PAGE_SIZES);
		model.addAttribute("totalRecords", list.getTotalElements());
		model.addAttribute("pager", pager);
		model.addAttribute("code", code.isPresent() ? code.get() : "");
		model.addAttribute("abbrName", abbrName.isPresent() ? abbrName.get() : "");
		model.addAttribute("status", isActive.isPresent() ? isActive.get() : "");
		model.addAttribute("sortingorder", sortingorder.isPresent() ? sortingorder.get() : "DESC");
		model.addAttribute("sortingkey", sortingkey.isPresent() ? sortingkey.get() : "lastModifiedOn");
		return "/employee/jobmaster :: result-table";
	}

	/**
	 * Update master.
	 *
	 * @param obj
	 *            the obj
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @param redirectAttributes
	 *            the redirect attributes
	 * @return the string
	 */
	@PostMapping(value = "/employee/creatjob")
	public String createMaster(@Valid @ModelAttribute("jobobj") JobMasterForm obj, BindingResult result, Model model,
			RedirectAttributes redirectAttributes) {
		if (!result.hasErrors() && obj.getJobCode() != null && jobmasrepo.getJobByCode(obj.getJobCode()) == null) {
			Job ad = new Job();
			ad.setCode(obj.getJobCode());
			ad.setName(obj.getJobName());
			ad.setAbbrName(obj.getJobAbbrName());
			ad.setLastModifiedOn(new Date());
			ad.setLastModifiedBy(getCurrentUser() != null ? getCurrentUser().getUsername() : null);
			ad.setActive(true);
			ad.setCreatedOn(new Date());
			ad.setCreatedBy(getCurrentUser() != null ? getCurrentUser().getUsername() : null);

			try {
				jobmasrepo.createJob(ad);
				auditService.putAudit(new Date(), "Job: " + obj.getJobCode() + " created successfully",
						getCurrentUser().getUsername(), "Create Job");
				redirectAttributes.addFlashAttribute("info", "Job Master created successfully");
				tempobj = null;
			} catch (Exception e) {
				LOGGER.error(e);
			}

		} else {
			redirectAttributes.addFlashAttribute("error", "Job Master code already exists");
			tempobj = obj;
		}
		return "redirect:/employee/jobmaster";
	}

	/**
	 * Edits the job master.
	 *
	 * @param jobcode
	 *            the jobcode
	 * @param model
	 *            the model
	 * @return the string
	 */
	@GetMapping(value = "/employee/edtjob/{jobcode}")
	public String editJobMaster(@PathVariable String jobcode, Model model) {
		Job tobj = jobmasrepo.getJobByCode(jobcode);
		edit = true;
		if (tobj != null) {
			JobMasterForm obj = new JobMasterForm();
			String date_format = "dd-MM-yyyy hh:mm:ss";
			SystemParameter dateObj = sysRepo.getSystemParameterByPropName("DATE_TIME_FORMAT");
			if (dateObj != null) {
				if (!dateObj.getPropValue().isEmpty()) {
					date_format = dateObj.getPropValue();
				}
			}
			DateFormat formatter = new SimpleDateFormat(date_format);
			String createdOn = "";
			String modifiedOn = "";
			String reactivatedOn = "";
			String deactivatedOn = "";
			if (tobj.getCreatedOn() != null) {
				createdOn = formatter.format(tobj.getCreatedOn());
			}
			if (tobj.getLastModifiedOn() != null) {
				modifiedOn = formatter.format(tobj.getLastModifiedOn());
			}
			if (tobj.getReactivatedOn() != null) {
				reactivatedOn = formatter.format(tobj.getReactivatedOn());
			}
			if (tobj.getDeactivatedOn() != null) {
				deactivatedOn = formatter.format(tobj.getDeactivatedOn());
			}
			obj.setCreatedon(createdOn);
			obj.setDeactivatedOn(deactivatedOn);
			obj.setReactivatedOn(reactivatedOn);
			obj.setUpdatedon(modifiedOn);
			obj.setJobCode(tobj.getCode());
			obj.setJobAbbrName(tobj.getAbbrName());
			obj.setJobName(tobj.getName());
			obj.setCreatedby(tobj.getCreatedBy());
			obj.setUpdatedby(tobj.getLastModifiedBy());
			obj.setDeactivatedBy(tobj.getDeactivatedBy());
			obj.setReactivatedBy(tobj.getReactivatedBy());
			obj.setIsActive(tobj.isActive() ? "Active" : "InActive");
			model.addAttribute("jobobj", obj);
			model.addAttribute("edit", edit);
			return "/employee/edit_jobmaster";
		} else {
			return "redirect:/employee/jobmaster";
		}
	}

	@GetMapping(value = "/master/deletejob/{code}")
	public String deleteJob(@PathVariable String code, Model model) {

		Job ad = jobmasrepo.getJobByCode(code);
		if (ad != null) {
			jobRepository.delete(ad);

		}
		return "redirect:/employee/jobmaster";
	}

	/**
	 * Edits the master.
	 *
	 * @param obj
	 *            the obj
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @param redirectAttributes
	 *            the redirect attributes
	 * @return the string
	 */
	@PostMapping(value = "/employee/updatejob")
	public String updateMaster(@Valid @ModelAttribute("jobobj") JobMasterForm obj, BindingResult result, Model model,
			RedirectAttributes redirectAttributes) {
		if (!result.hasErrors()) {
			boolean isActive;
			Job ad = jobmasrepo.getJobByCode(obj.getJobCode());

			try {
				if (ad != null) {
					ad.setCode(obj.getJobCode());
					ad.setName(obj.getJobName());
					ad.setAbbrName(obj.getJobAbbrName());
					ad.setLastModifiedOn(new Date());
					ad.setLastModifiedBy(getCurrentUser().getUsername());
					if (obj.getIsActive().contains("InActive")) {
						isActive = false;
					} else {
						isActive = true;
					}

					if (isActive != ad.isActive()) {

						if (obj.getIsActive().contains("InActive")) {
							ad.setDeactivatedBy(getCurrentUser().getUsername());
							ad.setDeactivatedOn(new Date());
						} else {
							ad.setReactivatedOn(new Date());
							ad.setReactivatedBy(getCurrentUser().getUsername());
						}
					}
					ad.setActive(obj.getIsActive().equals("Active"));
					jobmasrepo.createJob(ad);
					auditService.putAudit(new Date(), "Job: " + obj.getJobCode() + "updated successfully",
							getCurrentUser().getUsername(), "Update Job");
					redirectAttributes.addFlashAttribute("info", "Job Master updated successfully");
				} else {
					redirectAttributes.addFlashAttribute("error", "Object not exists");
					return "/employee/edit_jobmaster";
				}
			} catch (Exception e) {
				LOGGER.error(e);
			}
		}
		return "redirect:/employee/jobmaster";
	}

	@Override
	protected String getHeaderTitle() {
		return "Manage Employee";
	}

	/**
	 * Gets the store based on company.
	 *
	 * @param companycode
	 *            the companycode
	 * @return the store based on company
	 */
	@RequestMapping(value = "/employee/jobmaster/existingjobmaster", method = RequestMethod.GET)
	public @ResponseBody Job checkOrderSub(@RequestParam("job") String job, RedirectAttributes redirectAttributes,
			Model model) {

		return jobmasrepo.getJobByCode(job);

	}
}
