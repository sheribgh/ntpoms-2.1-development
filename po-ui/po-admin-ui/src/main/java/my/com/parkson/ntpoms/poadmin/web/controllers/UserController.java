/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.poadmin.web.controllers;

import static my.com.parkson.ntpoms.common.MessageCodes.FAILED_FETCH_RECORDS_EXCEPTION;
import static my.com.parkson.ntpoms.common.MessageCodes.FAILED_PERSIST_EXCEPTION;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.LogMessages;
import my.com.parkson.ntpoms.common.entities.EmployeeMaster;
import my.com.parkson.ntpoms.common.entities.PurchaseOrderUser;
import my.com.parkson.ntpoms.common.entities.Role;
import my.com.parkson.ntpoms.common.entities.SystemParameter;
import my.com.parkson.ntpoms.common.entities.User;
import my.com.parkson.ntpoms.common.entities.UserRole;
import my.com.parkson.ntpoms.common.services.AccessLogService;
import my.com.parkson.ntpoms.common.services.AuditService;
import my.com.parkson.ntpoms.common.services.EmployeeMasterService;
import my.com.parkson.ntpoms.common.services.MessageService;
import my.com.parkson.ntpoms.common.services.PurchaseOrderUserService;
import my.com.parkson.ntpoms.common.services.SecurityService;
import my.com.parkson.ntpoms.common.services.SystemParameterService;
import my.com.parkson.ntpoms.common.services.UsersService;
import my.com.parkson.ntpoms.poadmin.security.SecurityUtil;
import my.com.parkson.ntpoms.poadmin.web.models.AutocompleteForm;
import my.com.parkson.ntpoms.poadmin.web.models.POUserForm;
import my.com.parkson.ntpoms.poadmin.web.models.Pager;
import my.com.parkson.ntpoms.poadmin.web.models.UsersForm;
import my.com.parkson.ntpoms.poadmin.web.validators.UserValidator;

@Slf4j
@Controller
@Secured(SecurityUtil.MANAGE_USER)
@RequiredArgsConstructor
public class UserController extends AbstractAdminController {

	private static Logger LOGGER = Logger.getLogger(UserController.class);
	private static final String VIEWPREFIX = "users/";

	private static final int BUTTONS_TO_SHOW = 5;
	private static final int INITIAL_PAGE = 0;
	private static final int INITIAL_PAGE_SIZE = 5;
	private static final int[] PAGE_SIZES = { 5, 10, 20 };
	private static final String DATE_FORMAT_KEY = "DATE_FORMAT";
	private static final String DATE_FORMAT_VALUE = "dd-MM-yyyy";
	private static final String DATE_TIME_FORMAT_KEY = "DATE_TIME_FORMAT";
	private static final String DATE_TIME_FORMAT_VALUE = "dd-MM-yyyy hh:mm:ss";

	private POUserForm potempobj;

	@Autowired
	private UsersService adminrepo;

	@Autowired
	private PurchaseOrderUserService pouser;

	@Autowired
	private EmployeeMasterService emprepo;

	@Autowired
	private SecurityService securityService;

	@Autowired
	private UserValidator userValidator;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private SystemParameterService sysRepo;

	/** The messages. */
	@Autowired
	private MessageService messages;

	@Autowired
	private AuditService auditService;

	@GetMapping("/users/adminuser")
	public String onPageLoad(Model model) {
		String dateFormat = DATE_FORMAT_VALUE;
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName(DATE_FORMAT_KEY);
		if (dateObj != null && !dateObj.getPropValue().isEmpty()) {
			dateFormat = dateObj.getPropValue();
		}
		UsersForm obj = new UsersForm();
		model.addAttribute("masterobj", obj);
		model.addAttribute("date_format", dateFormat);

		return "/users/adminuser";
	}

	/**
	 * On page load.
	 *
	 * @param pageSize
	 *            the page size
	 * @param page
	 *            the page
	 * @param username
	 *            the username
	 * @param isActive
	 *            the is active
	 * @return the model and view
	 */
	@RequestMapping(value = "/users/adminusergrid", method = RequestMethod.GET)
	public String adminGrid(@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page, @RequestParam("username") Optional<String> username,
			@RequestParam("adminStartDate") Optional<String> startDate,
			@RequestParam("adminEndDate") Optional<String> endDate, @RequestParam("status") Optional<String> isActive,
			@RequestParam("sortingkey") Optional<String> sortingkey,
			@RequestParam("sortingorder") Optional<String> sortingorder, Model model) {

		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;

		String dateFormat = DATE_FORMAT_VALUE;
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName(DATE_FORMAT_KEY);
		if (dateObj != null && !dateObj.getPropValue().isEmpty()) {
			dateFormat = dateObj.getPropValue();
		}
		Page<User> list;
		Specification<User> s = new Specification<User>() {
			@Override
			public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> ls = new ArrayList<>();
				if (username.isPresent() && !username.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("username")), "%" + username.get().toLowerCase() + "%"));
				}
				if (startDate.isPresent() && !startDate.get().isEmpty()) {
					Expression<String> startdateStringExpr = cb.function("to_char", String.class, root.get("startDate"),
							cb.literal("YYYY-MM-DD HH12:MI:SS"));
					ls.add(cb.like(cb.lower(startdateStringExpr), "%" + startDate.get().toLowerCase() + "%"));
				}
				if (endDate.isPresent() && !endDate.get().isEmpty()) {
					Expression<String> enddateStringExpr = cb.function("to_char", String.class, root.get("endDate"),
							cb.literal("YYYY-MM-DD HH12:MI:SS"));
					ls.add(cb.like(cb.lower(enddateStringExpr), "%" + endDate.get().toLowerCase() + "%"));
				}
				if (isActive.isPresent() && !isActive.get().isEmpty() && !isActive.get().equals("All")) {
					ls.add(cb.equal(root.get("isActive"), isActive.get().equals("Active")));
				}
				ls.add(cb.notEqual(root.get("username"), "superadmin"));
				return cb.and(ls.toArray(new Predicate[0]));
			}
		};
		Specification<User> sadmin = new Specification<User>() {
			@Override
			public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> ls = new ArrayList<>();
				ls.add(cb.notEqual(root.get("username"), "superadmin"));
				return cb.and(ls.toArray(new Predicate[0]));
			}
		};
		Sort sorttemp = new Sort(Sort.Direction.DESC, "lastModifiedOn");
		if (sortingkey.isPresent() && sortingorder.isPresent()) {
			sorttemp = new Sort(sortingorder.get().equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC,
					sortingkey.get());
		}
		if (username.isPresent() || startDate.isPresent() || endDate.isPresent() || isActive.isPresent()) {
			list = adminrepo.getAllPagableUsers(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		} else {
			list = adminrepo.getAllPageableUserOrdered(sadmin, new PageRequest(evalPage, evalPageSize, sorttemp));
		}
		int serialid = 0;
		int pagesize = evalPage + 1;
		if (pagesize > 1) {
			int start = evalPageSize * pagesize;
			serialid = start - evalPageSize;
		}
		Pager pager = new Pager(list.getTotalPages(), list.getNumber(), BUTTONS_TO_SHOW);
		model.addAttribute("list", list);
		model.addAttribute("selectedPageSize", evalPageSize);
		model.addAttribute("pageSizes", PAGE_SIZES);
		model.addAttribute("pager", pager);
		model.addAttribute("totalRecords", list.getTotalElements());
		model.addAttribute("date_format", dateFormat);
		model.addAttribute("serialid", serialid);
		model.addAttribute("username", username.isPresent() ? username.get() : "");
		model.addAttribute("adminStartDate", startDate.isPresent() ? startDate.get() : "");
		model.addAttribute("adminEndDate", endDate.isPresent() ? endDate.get() : "");
		model.addAttribute("status", isActive.isPresent() ? isActive.get() : "");
		model.addAttribute("sortingorder", sortingorder.isPresent() ? sortingorder.get() : "DESC");
		model.addAttribute("sortingkey", sortingkey.isPresent() ? sortingkey.get() : "lastModifiedOn");

		return "/users/adminuser :: result-table";
	}

	/**
	 * Create master.
	 *
	 * @param obj
	 *            the obj
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @param redirectAttributes
	 *            the redirect attributes
	 * @return the string
	 * @throws ParseException
	 *             the parse exception
	 */
	@PostMapping(value = "/users/createmaster")
	public String createMaster(@Valid @ModelAttribute("masterobj") UsersForm obj, BindingResult result, Model model,
			RedirectAttributes redirectAttributes) throws ParseException {
		if (!result.hasErrors() && obj.getUsername() != null
				&& adminrepo.getUsersByUsername(obj.getUsername()) == null) {
			SystemParameter dateObj = sysRepo.getSystemParameterByPropName(DATE_FORMAT_KEY);
			String dateFormat = DATE_FORMAT_VALUE;
			if (dateObj != null && !dateObj.getPropValue().isEmpty()) {
				dateFormat = dateObj.getPropValue();
			}
			DateFormat formatter = new SimpleDateFormat(dateFormat);
			User ad = new User();
			ad.setUsername(obj.getUsername());
			ad.setPassword(passwordEncoder.encode(obj.getPassword()));
			ad.setRecentPassword(passwordEncoder.encode(obj.getPassword()));
			ad.setCreatedOn(new Date());
			ad.setCreatedBy(getCurrentUser().getUsername());
			ad.setLastModifiedOn(new Date());
			ad.setLastModifiedBy(getCurrentUser().getUsername());
			ad.setActive(true);
			ad.setLocked(false);
			ad.setEmail(obj.getEmail());
			ad.setStartDate(formatter.parse(obj.getStartDate()));
			ad.setEndDate(formatter.parse(obj.getEndDate()));

			try {

				UserRole role = new UserRole();
				role.setUser(ad.getUsername());
				role.setRoleId(2);
				adminrepo.createUsers(ad);
				adminrepo.addUserRole(role);
				auditService.putAudit(new Date(),
						"Admin User Management : " + obj.getUsername() + " created successfully",
						getCurrentUser().getUsername(), "Create Admin User");
				model.addAttribute("masterobj", new UsersForm());
				redirectAttributes.addFlashAttribute("info", "Admin User created successfully");

			} catch (Exception e) {
				redirectAttributes.addFlashAttribute("error", "Unable to save Admin User.");
				LOGGER.error(e);
				return "redirect:/users/adminuser";
			}
		} else if (obj != null && !obj.getPassword().equals(obj.getRecentPassword())) {
			redirectAttributes.addFlashAttribute("error", "Password must be same / Username already exists!!!");
		}
		return "redirect:/users/adminuser";
	}

	/**
	 * Edits the master.
	 *
	 * @param username
	 *            the username
	 * @param model
	 *            the model
	 * @return the string
	 */
	@GetMapping(value = "/users/editmaster/{username}")
	public String editMaster(@PathVariable String username, Model model) {
		User tobj = adminrepo.getUsersByUsername(username);
		if (tobj != null) {
			UsersForm obj = new UsersForm();
			String dateFormat = DATE_TIME_FORMAT_VALUE;
			SystemParameter dateObj = sysRepo.getSystemParameterByPropName(DATE_TIME_FORMAT_KEY);
			if (dateObj != null && !dateObj.getPropValue().isEmpty()) {
				dateFormat = dateObj.getPropValue();
			}
			DateFormat formatter = new SimpleDateFormat(dateFormat);
			String dateFormat2 = DATE_FORMAT_VALUE;
			SystemParameter dateObj2 = sysRepo.getSystemParameterByPropName(DATE_FORMAT_KEY);
			if (dateObj2 != null && !dateObj2.getPropValue().isEmpty()) {
				dateFormat2 = dateObj2.getPropValue();
			}

			DateFormat format = new SimpleDateFormat(dateFormat2);
			String createdOn = "";
			String modifiedOn = "";
			String reactivatedOn = "";
			String deactivatedOn = "";
			String password = "@Unchngd123";
			if (tobj.getCreatedOn() != null) {
				createdOn = formatter.format(tobj.getCreatedOn());
			}
			if (tobj.getLastModifiedOn() != null) {
				modifiedOn = formatter.format(tobj.getLastModifiedOn());
			}
			if (tobj.getReactivatedOn() != null) {
				reactivatedOn = formatter.format(tobj.getReactivatedOn());
			}
			if (tobj.getDeactivatedOn() != null) {
				deactivatedOn = formatter.format(tobj.getDeactivatedOn());
			}
			obj.setCreatedon(createdOn);
			obj.setDeactivatedOn(deactivatedOn);
			obj.setReactivatedOn(reactivatedOn);
			obj.setUpdatedon(modifiedOn);
			obj.setCreatedby(tobj.getCreatedBy());
			obj.setUpdatedby(tobj.getLastModifiedBy());
			obj.setUsername(tobj.getUsername());
			obj.setPassword(password);
			obj.setStartDate("" + format.format(tobj.getStartDate()));
			obj.setEndDate("" + format.format(tobj.getEndDate()));
			obj.setIsActive((tobj.isActive() ? "Active" : "InActive"));
			obj.setIsLocked(tobj.isLocked() ? "Locked" : "UnLocked");
			obj.setRecentPassword(password);
			obj.setDeactivatedBy(tobj.getDeactivatedBy());
			obj.setReactivatedBy(tobj.getReactivatedBy());
			obj.setEmail(tobj.getEmail());
			model.addAttribute("masterobj", obj);
			model.addAttribute("date_format", dateFormat2);
			model.addAttribute("start_date", format.format(tobj.getStartDate()));
			model.addAttribute("end_date", format.format(tobj.getEndDate()));
			model.addAttribute("edit", true);
			return "/users/edit_adminuser";
		} else {
			return "redirect:/users/adminuser";
		}
	}

	/**
	 * Updates the master.
	 *
	 * @param obj
	 *            the obj
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @param redirectAttributes
	 *            the redirect attributes
	 * @return the string
	 */
	@PostMapping(value = "/users/updatemaster")
	public String updateMaster(@Valid @ModelAttribute("masterobj") UsersForm obj, BindingResult result, Model model,
			RedirectAttributes redirectAttributes) {
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName(DATE_FORMAT_KEY);
		String dateFormat = DATE_FORMAT_VALUE;
		if (dateObj != null && !dateObj.getPropValue().isEmpty()) {
			dateFormat = dateObj.getPropValue();
		}
		DateFormat formatter = new SimpleDateFormat(dateFormat);
		if (!result.hasErrors()) {
			boolean isActive = true;
			try {
				User ad = adminrepo.getUsersByUsername(obj.getUsername());
				if (ad != null) {
					ad.setLastModifiedOn(new Date());
					ad.setLastModifiedBy(getCurrentUser().getUsername());
					if (obj.getIsActive().contains("InActive")) {
						isActive = false;
					}
					if (isActive != ad.isActive()) {
						if (obj.getIsActive().contains("InActive")) {
							ad.setDeactivatedBy(getCurrentUser().getUsername());
							ad.setDeactivatedOn(new Date());
						} else {
							ad.setReactivatedOn(new Date());
							ad.setReactivatedBy(getCurrentUser().getUsername());
						}
					}
					ad.setActive(obj.getIsActive().equals("Active"));
					ad.setLocked(obj.getIsLocked().equals("Locked"));
					ad.setStartDate(formatter.parse(obj.getStartDate()));
					ad.setEndDate(formatter.parse(obj.getEndDate()));
					ad.setEmail(obj.getEmail());
					if (ad.isActive()) {
						ad.setReactivatedOn(new Date());
					} else {
						ad.setDeactivatedOn(new Date());
					}
					if (!obj.getPassword().contains("@Unchngd123")) {
						ad.setPassword(passwordEncoder.encode(obj.getPassword()));
						ad.setRecentPassword(passwordEncoder.encode(obj.getPassword()));
					}
					adminrepo.createUsers(ad);
					auditService.putAudit(new Date(),
							"Admin User Management: " + obj.getUsername() + " Updated successfully",
							getCurrentUser().getUsername(), "Updated Admin User Management");
					model.addAttribute("masterobj", new UsersForm());
					redirectAttributes.addFlashAttribute("info", "Admin Updated successfully");
				} else {
					redirectAttributes.addFlashAttribute("error", "Object not exists");
				}
			} catch (Exception e) {
				LOGGER.error(e);
			}
		} else if (obj != null && !obj.getPassword().equals(obj.getRecentPassword())) {
			redirectAttributes.addFlashAttribute("error", "Password must be same!!!");
			return "/users/edit_adminuser";
		}
		return "redirect:/users/adminuser";
	}

	/**
	 * On page load PO user.
	 *
	 * @param pageSize
	 *            the page size
	 * @param page
	 *            the page
	 * @param psid
	 *            the psid
	 * @param empname
	 *            the empname
	 * @return the model and view
	 */
	@GetMapping("/users/pouser")
	public ModelAndView onPageLoadPOUser() {
		ModelAndView modelAndView = new ModelAndView("/users/pouser");
		modelAndView.addObject("emplist", emprepo.getAllActiveEmployeeMaster());
		modelAndView.getModelMap().addAttribute("pouserbj", potempobj != null ? potempobj : new POUserForm());
		return modelAndView;
	}

	@RequestMapping(value = "/users/pousergrid", method = RequestMethod.GET)
	public String pouserGrid(@RequestParam("pageSize") Optional<Integer> pageSize,
			@RequestParam("page") Optional<Integer> page, @RequestParam("psid") Optional<String> psid,
			@RequestParam("empName") Optional<String> empname,
			@RequestParam("firstTimeLogin") Optional<String> firstTimeLogin,
			@RequestParam("lastLogin") Optional<String> lastLogin, @RequestParam("status") Optional<String> isActive,
			@RequestParam("sortingkey") Optional<String> sortingkey,
			@RequestParam("sortingorder") Optional<String> sortingorder, Model model) {

		int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
		int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
		Page<PurchaseOrderUser> list;
		String dateTimeFormat = DATE_TIME_FORMAT_VALUE;
		SystemParameter dateObj = sysRepo.getSystemParameterByPropName(DATE_TIME_FORMAT_KEY);
		if (dateObj != null && !dateObj.getPropValue().isEmpty()) {
			dateTimeFormat = dateObj.getPropValue();
		}
		Specification<PurchaseOrderUser> s = new Specification<PurchaseOrderUser>() {
			@Override
			public Predicate toPredicate(Root<PurchaseOrderUser> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> ls = new ArrayList<>();
				if (psid.isPresent() && !psid.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("id")), "%" + psid.get().toLowerCase() + "%"));
				}
				if (empname.isPresent() && !empname.get().isEmpty()) {
					ls.add(cb.like(cb.lower(root.get("empName")), "%" + empname.get().toLowerCase() + "%"));
				}
				if (firstTimeLogin.isPresent() && !firstTimeLogin.get().isEmpty()) {
					Expression<String> dateStringExpr = cb.function("to_char", String.class, root.get("firstTimeLogin"),
							cb.literal("YYYY-MM-DD HH12:MI:SS"));
					ls.add(cb.like(cb.lower(dateStringExpr), "%" + firstTimeLogin.get().toLowerCase() + "%"));
				}
				if (lastLogin.isPresent() && !lastLogin.get().isEmpty()) {
					Expression<String> dateStringExpr = cb.function("to_char", String.class, root.get("lastLogin"),
							cb.literal("YYYY-MM-DD HH12:MI:SS"));
					ls.add(cb.like(cb.lower(dateStringExpr), "%" + lastLogin.get().toLowerCase() + "%"));
				}
				if (isActive.isPresent() && !isActive.get().isEmpty() && !isActive.get().equals("All")) {
					ls.add(cb.equal(root.get("isActive"), "Active".equals(isActive.get())));
				}
				return cb.and(ls.toArray(new Predicate[0]));
			}
		};
		Sort sorttemp = new Sort(Sort.Direction.DESC, "lastModifiedOn");
		if (sortingkey.isPresent() && sortingorder.isPresent()) {
			sorttemp = new Sort(sortingorder.get().equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC,
					sortingkey.get());
		}
		if (psid.isPresent() || empname.isPresent() || firstTimeLogin.isPresent() || lastLogin.isPresent()
				|| isActive.isPresent()) {
			list = pouser.getAllPageablePurchaseOrderUser(s, new PageRequest(evalPage, evalPageSize, sorttemp));
		} else {
			list = pouser.getAllPageablePurchaseOrderUserOrdered(new PageRequest(evalPage, evalPageSize, sorttemp));
		}
		int serialid = 0;
		int pagesize = evalPage + 1;
		if (pagesize > 1) {
			int start = evalPageSize * pagesize;
			serialid = start - evalPageSize;
		}
		Pager pager = new Pager(list.getTotalPages(), list.getNumber(), BUTTONS_TO_SHOW);
		model.addAttribute("serialid", serialid);
		model.addAttribute("list", list);
		model.addAttribute("selectedPageSize", evalPageSize);
		model.addAttribute("pageSizes", PAGE_SIZES);
		model.addAttribute("pager", pager);
		model.addAttribute("totalRecords", list.getTotalElements());
		model.addAttribute("date_format", dateTimeFormat);
		model.addAttribute("psid", psid.isPresent() ? psid.get() : "");
		model.addAttribute("empName", empname.isPresent() ? empname.get() : "");
		model.addAttribute("firstTimeLogin", firstTimeLogin.isPresent() ? firstTimeLogin.get() : "");
		model.addAttribute("lastLogin", lastLogin.isPresent() ? lastLogin.get() : "");
		model.addAttribute("status", isActive.isPresent() ? isActive.get() : "");
		model.addAttribute("emplist", emprepo.getAllActiveEmployeeMaster());
		model.addAttribute("sortingorder", sortingorder.isPresent() ? sortingorder.get() : "DESC");
		model.addAttribute("sortingkey", sortingkey.isPresent() ? sortingkey.get() : "lastModifiedOn");
		return "/users/pouser :: result-table";
	}

	/**
	 * Creates the PO user.
	 *
	 * @param obj
	 *            the obj
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @param redirectAttributes
	 *            the redirect attributes
	 * @return the string
	 */
	@PostMapping(value = "/users/createpouser")
	public String createPOUser(@Valid @ModelAttribute("pouserbj") POUserForm obj, BindingResult result, Model model,
			RedirectAttributes redirectAttributes) {
		if (!result.hasErrors() && obj.getPsID() != null) {
			PurchaseOrderUser ad = new PurchaseOrderUser();
			ad.setId(obj.getPsID());
			ad.setEmpName(obj.getEmpName());
			ad.setCreatedOn(new Date());
			ad.setCreatedBy(getCurrentUser() != null ? getCurrentUser().getUsername() : null);
			ad.setLastModifiedBy(getCurrentUser() != null ? getCurrentUser().getUsername() : null);
			ad.setActive(true);
			ad.setLastModifiedOn(new Date());
			try {
				if (pouser.getPurchaseOrderUserById(obj.getPsID()) != null) {
					redirectAttributes.addFlashAttribute("error", "This PO user already exists");
					potempobj = obj;
					LOGGER.info("Po User already exists.");
				} else {
					pouser.createPurchaseOrderUser(ad);
					model.addAttribute("pouserbj", new POUserForm());
					auditService.putAudit(new Date(),
							"PO User: " + obj.getPsID() + " : " + obj.getEmpName() + " created successfully",
							getCurrentUser().getUsername(), "Create PO User");
					redirectAttributes.addFlashAttribute("info", "PO User created successfully");
					potempobj = null;
				}
			} catch (Exception e) {
				LOGGER.error(e);
			}
		} else {
			potempobj = obj;
		}
		return "redirect:/users/pouser";
	}

	/**
	 * Gets the store based on company.
	 *
	 * @param companycode
	 *            the companycode
	 * @return the store based on company
	 */
	@RequestMapping(value = "/users/pouser/populateEmpName", method = RequestMethod.GET)
	public @ResponseBody List<EmployeeMaster> getEmpNameByPsID(@RequestParam("psid") String psid) {
		return emprepo.getAllEmpNameByPsId(psid);
	}

	/**
	 * Edits the PO user.
	 *
	 * @param psid
	 *            the psid
	 * @param model
	 *            the model
	 * @return the string
	 */
	@GetMapping(value = "/users/editpouser/{psid}")
	public String editPOUser(@PathVariable String psid, Model model) {
		PurchaseOrderUser tobj = pouser.getPurchaseOrderUserById(psid);
		if (tobj != null) {
			POUserForm obj = new POUserForm();
			String dateFormat = DATE_TIME_FORMAT_VALUE;
			SystemParameter dateObj = sysRepo.getSystemParameterByPropName(DATE_TIME_FORMAT_KEY);
			if (dateObj != null && !dateObj.getPropValue().isEmpty()) {
				dateFormat = dateObj.getPropValue();
			}
			DateFormat formatter = new SimpleDateFormat(dateFormat);
			String createdOn = "";
			String modifiedOn = "";
			String reactivatedOn = "";
			String deactivatedOn = "";
			String firstTimeLogIn = "";
			String lastLogIn = "";
			if (tobj.getCreatedOn() != null) {
				createdOn = formatter.format(tobj.getCreatedOn());
			}
			if (tobj.getLastModifiedOn() != null) {
				modifiedOn = formatter.format(tobj.getLastModifiedOn());
			}
			if (tobj.getReactivatedOn() != null) {
				reactivatedOn = formatter.format(tobj.getReactivatedOn());
			}
			if (tobj.getDeactivatedOn() != null) {
				deactivatedOn = formatter.format(tobj.getDeactivatedOn());
			}

			if (tobj.getFirstTimeLogin() != null) {
				firstTimeLogIn = formatter.format(tobj.getFirstTimeLogin());
			}
			if (tobj.getLastLogin() != null) {
				lastLogIn = formatter.format(tobj.getLastLogin());
			}
			obj.setCreatedon(createdOn);
			obj.setDeactivatedOn(deactivatedOn);
			obj.setReactivatedOn(reactivatedOn);
			obj.setUpdatedon(modifiedOn);
			obj.setLastLogin(lastLogIn);
			obj.setFirstTimeLogin(firstTimeLogIn);
			obj.setEmpName(tobj.getEmpName());
			obj.setPsID(tobj.getId());
			obj.setCreatedby(tobj.getCreatedBy());
			obj.setUpdatedby(tobj.getLastModifiedBy());
			obj.setDeactivatedBy(tobj.getDeactivatedBy());
			obj.setReactivatedBy(tobj.getReactivatedBy());
			obj.setIsActive(tobj.isActive() ? "Active" : "InActive");
			model.addAttribute("emplist", emprepo.getAllActiveEmployeeMaster());
			model.addAttribute("pouserbj", obj);
			model.addAttribute("date_format", dateFormat);
			model.addAttribute("edit", true);
			return "/users/edit_pouser";
		} else {
			return "redirect:/users/pouser";
		}
	}

	/**
	 * Update PO user.
	 *
	 * @param obj
	 *            the obj
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @param redirectAttributes
	 *            the redirect attributes
	 * @return the string
	 */
	@PostMapping(value = "/users/updatepouser")
	public String updatePOUser(@Valid @ModelAttribute("pouserbj") POUserForm obj, BindingResult result, Model model,
			RedirectAttributes redirectAttributes) {
		if (!result.hasErrors()) {
			try {
				if (obj.getPsID() != null && obj.getEmpName() != null && !"Select".equals(obj.getPsID())) {
					PurchaseOrderUser ad = pouser.getPurchaseOrderUserById(obj.getPsID());
					boolean isActive = true;
					ad.setId(obj.getPsID());
					ad.setEmpName(obj.getEmpName());
					ad.setLastModifiedBy(getCurrentUser() != null ? getCurrentUser().getUsername() : null);
					if (obj.getIsActive().contains("InActive")) {
						isActive = false;
					}
					if (isActive != ad.isActive()) {
						if (obj.getIsActive().contains("InActive")) {
							ad.setDeactivatedBy(getCurrentUser().getUsername());
							ad.setDeactivatedOn(new Date());
						} else {
							ad.setReactivatedOn(new Date());
							ad.setReactivatedBy(getCurrentUser().getUsername());
						}
					}
					ad.setActive(obj.getIsActive().equals("Active"));
					ad.setLastModifiedOn(new Date());
					pouser.createPurchaseOrderUser(ad);
					auditService.putAudit(new Date(),
							"PO User: " + obj.getPsID() + " : " + obj.getEmpName() + " updated successfully",
							getCurrentUser().getUsername(), "Update PO User");
					model.addAttribute("pouserbj", new POUserForm());
					redirectAttributes.addFlashAttribute("info", "PO User Updated successfully");
				} else {
					redirectAttributes.addFlashAttribute("error", "Select PS ID and enter Emp Name");
					return "/users/edit_pouser";
				}
			} catch (Exception e) {
				LOGGER.error(e);
			}
		}
		return "redirect:/users/pouser";
	}

	/**
	 * Creates the user.
	 *
	 * @param user
	 *            the user
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @param redirectAttributes
	 *            the redirect attributes
	 * @return the string
	 */
	@PostMapping(value = "/users")
	public String createUser(@Valid @ModelAttribute("user") User user, BindingResult result, Model model,
			RedirectAttributes redirectAttributes) {
		userValidator.validate(user, result);
		if (result.hasErrors()) {
			return VIEWPREFIX + "create_user";
		}
		String password = user.getPassword();
		String encodedPwd = passwordEncoder.encode(password);
		user.setPassword(encodedPwd);
		try {
			User persistedUser = securityService.createUser(user);
			auditService.putAudit(new Date(), "User: " + persistedUser.getUsername() + " created successfully",
					getCurrentUser().getUsername(), "Create User");
			redirectAttributes.addFlashAttribute("info",
					"User [" + persistedUser.getUsername() + "] created successfully");
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return "redirect:/users";
	}

	/**
	 * Creates the user form.
	 *
	 * @param model
	 *            the model
	 * @return the string
	 */
	@GetMapping(value = "/users/new")
	public String createUserForm(Model model) {
		User user = new User();
		model.addAttribute("user", user);
		return VIEWPREFIX + "create_user";
	}

	/**
	 * Edits the user form.
	 *
	 * @param username
	 *            the username
	 * @param model
	 *            the model
	 * @return the string
	 */
	@GetMapping(value = "/users/{username}")
	public String editUserForm(@PathVariable String username, Model model) {
		User user = securityService.getUserByUsername(username);
		Map<Integer, Role> assignedRoleMap = new HashMap<>();
		List<Role> roles = user.getRoles();
		for (Role role : roles) {
			assignedRoleMap.put(role.getId(), role);
		}
		List<Role> userRoles = new ArrayList<>();
		List<Role> allRoles = securityService.getAllRoles();
		for (Role role : allRoles) {
			if (assignedRoleMap.containsKey(role.getId())) {
				userRoles.add(role);
			} else {
				userRoles.add(null);
			}
		}
		user.setRoles(userRoles);
		model.addAttribute("user", user);
		return VIEWPREFIX + "edit_user";
	}

	/**
	 * List users.
	 *
	 * @param model
	 *            the model
	 * @return the string
	 */
	@GetMapping(value = "/users")
	public String listUsers(Model model) {
		List<User> list = securityService.getAllUsers();
		model.addAttribute("users", list);
		return VIEWPREFIX + "users";
	}

	/**
	 * My account.
	 *
	 * @param model
	 *            the model
	 * @return the string
	 */
	@GetMapping(value = "/myAccount")
	public String myAccount(Model model) {
		String username = getCurrentUser().getUser().getUsername();
		User user = securityService.getUserByUsername(username);
		model.addAttribute("user", user);
		return "myAccount";
	}

	/**
	 * Roles list.
	 *
	 * @return the list
	 */
	@ModelAttribute("rolesList")
	public List<Role> rolesList() {
		return securityService.getAllRoles();
	}

	/**
	 * Update user.
	 *
	 * @param user
	 *            the user
	 * @param result
	 *            the result
	 * @param model
	 *            the model
	 * @param redirectAttributes
	 *            the redirect attributes
	 * @return the string
	 */
	@PostMapping(value = "/users/{id}")
	public String updateUser(@ModelAttribute("user") User user, BindingResult result, Model model,
			RedirectAttributes redirectAttributes) {
		if (result.hasErrors()) {
			return VIEWPREFIX + "edit_user";
		}
		try {
			User persistedUser = securityService.updateUser(user);
			auditService.putAudit(new Date(), "User: " + persistedUser.getUsername() + " updated successfully",
					getCurrentUser().getUsername(), "Update User");
			redirectAttributes.addFlashAttribute("info",
					"User [" + persistedUser.getUsername() + "] updates successfully");
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return "redirect:/users";
	}

	@Override
	protected String getHeaderTitle() {
		return "Manage Users";
	}

	/**
	 * Gets the store based on company.
	 *
	 * @param companycode
	 *            the companycode
	 * @return the store based on company
	 */
	@RequestMapping(value = "/users/adminuser/existingadmin", method = RequestMethod.GET)
	public @ResponseBody String checkCompanyCode(@RequestParam("admin") String admin,
			RedirectAttributes redirectAttributes, Model model) {
		User user = adminrepo.getUsersByUsername(admin);
		try {
			if (user != null) {
				return user.getUsername();
			} else {
				return "";
			}
		} catch (NullPointerException e) {
			LOGGER.error(e);
			return "";
		}
	}

	/**
	 * Gets the store based on company.
	 *
	 * @param companycode
	 *            the companycode
	 * @return the store based on company
	 */
	@RequestMapping(value = "/users/adminuseremail/existingemail", method = RequestMethod.GET)
	public @ResponseBody String checkAdminEmail(@RequestParam("email") String email,
			@RequestParam("username") String username, RedirectAttributes redirectAttributes, Model model) {
		User user = adminrepo.getUsersByUserEmail(email);
		try {

			if (user != null) {

				if (user.getUsername().matches(username)) {
					return "";
				} else {
					return user.getEmail();
				}

			} else {
				return "";
			}
		} catch (NullPointerException e) {
			LOGGER.error(e);
			return "";
		}

	}

	/**
	 * 
	 * @param searchval
	 * @param redirectAttributes
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/users/adminuser/employeeautocomplete", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody List<AutocompleteForm> employeeSearch(@RequestParam("query") String searchval) {
		Specification<EmployeeMaster> sp = new Specification<EmployeeMaster>() {
			@Override
			public Predicate toPredicate(Root<EmployeeMaster> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> ls = new ArrayList<>();
				ls.add(cb.like(cb.lower(root.get("psId")), "%" + searchval.toLowerCase() + "%"));
				ls.add(cb.like(cb.lower(root.get("altId")), "%" + searchval.toLowerCase() + "%"));
				ls.add(cb.like(cb.lower(root.get("name")), "%" + searchval.toLowerCase() + "%"));
				ls.add(cb.like(cb.lower(root.get("posTitle")), "%" + searchval.toLowerCase() + "%"));
				return cb.or(ls.toArray(new Predicate[0]));
			}
		};
		Page<EmployeeMaster> list;
		if (searchval != null && !searchval.isEmpty()) {
			list = emprepo.getAllPagableEmployee(sp, new PageRequest(0, 20));
		} else {
			list = emprepo.getAllPagableEmployee(new PageRequest(0, 20));
		}
		List<AutocompleteForm> temp = new ArrayList<>();
		for (EmployeeMaster e : list.getContent()) {
			AutocompleteForm a = new AutocompleteForm();
			StringBuilder s = new StringBuilder();
			s.append(e.getPsId());
			s.append("-");
			s.append(e.getAltId());
			s.append("-");
			s.append(e.getName());
			s.append("(");
			s.append(e.getPosTitle());
			s.append(")");
			a.setName(s.toString());
			a.setId(e.getPsId());
			temp.add(a);
		}
		return temp;
	}
}