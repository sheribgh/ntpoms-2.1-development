/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.poadmin.web.controllers;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javassist.NotFoundException;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.entities.Store;
import my.com.parkson.ntpoms.common.services.StoreService;
import my.com.parkson.ntpoms.poadmin.web.models.StoreForm;

@RunWith(SpringRunner.class)
@WebMvcTest(StoreController.class)
public class StoreControllerTest {

	@Autowired
	private WebApplicationContext context;

	MockMvc mockMvc;

	@MockBean
	private StoreService storeService;

	private StoreController storeController;
	
	private Integer pageSize = 5;
	private Integer page= 0;
	private String isActive = "JUNIT isActive";
	private String code = "JUNIT";
	private String name = "JUNIT Name";
	private String companyCode = "JUNIT companyCode";

	private String codeUpdate = "JUNIT Code Update";
	private String nameUpdate = "JUNIT Name Update";

	private String codeEdit = "JUNIT Code  Edit";
	private String nameEdit = "JUNIT Name Edit";

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
		Store store = new Store();
		store.setCode(code);
		store.setName(name);
		store.setCompCode(companyCode);
		List<Store> storeList = Arrays.asList(store);
		when(storeService.getAllStore()).thenReturn(storeList);
		storeController = new StoreController();
	}

	/**
	 * Test on page load store.
	 */
	@Test
	@WithMockUser(username = "superadmin")
	public void testOnPageLoadStore() {
		try {
			this.mockMvc.perform(get("/store")).andExpect(status().isOk())
					.andExpect(content().string(containsString(name)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Test create store.
	 */
	@Test(expected = POException.class)
	public void testCreateStore() {
		try {
			mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
			Store store = new Store();
			store.setCode(code);
			store.setName(name);
			store.setCompCode(companyCode);
			// storeController.createStore(store);
			HttpServletResponse response = new MockHttpServletResponse();
			storeController.onPageLoadStore();
			assertEquals("store", response.getContentType());
		} catch (Exception ex) {
			assert ((ex instanceof NullPointerException) || (ex instanceof SQLException)
					|| (ex instanceof NotFoundException));
		}
	}

	/**
	 * Test update store.
	 */
	@Test(expected = POException.class)
	public void testUpdateStore() {
		try {
			mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
			Store store = storeService.getStoreByCode(code);
			store.setCode(codeUpdate);
			store.setName(nameUpdate);
			// storeController.updateStore(store);
			HttpServletResponse response = new MockHttpServletResponse();
			storeController.onPageLoadStore();
			assertEquals("store", response.getContentType());
		} catch (Exception ex) {
			assert ((ex instanceof NullPointerException) || (ex instanceof SQLException)
					|| (ex instanceof NotFoundException));
		}
	}

	/**
	 * Test edit company.
	 */
	@Test(expected = POException.class)
	public void testEditCompany() {
		try {
			mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
			Store store = storeService.getStoreByCode(code);
			store.setCode(codeEdit);
			store.setName(nameEdit);
			// storeController.editStore(store);
			HttpServletResponse response = new MockHttpServletResponse();
			storeController.onPageLoadStore();
			assertEquals("store", response.getContentType());
		} catch (Exception ex) {
			assert ((ex instanceof NullPointerException) || (ex instanceof SQLException)
					|| (ex instanceof NotFoundException));
		}
	}
}