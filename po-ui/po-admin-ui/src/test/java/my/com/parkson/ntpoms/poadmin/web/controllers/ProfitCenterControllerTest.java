/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.poadmin.web.controllers;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.WebApplicationContext;
import javassist.NotFoundException;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.entities.Company;
import my.com.parkson.ntpoms.common.entities.ProfitCenter;
import my.com.parkson.ntpoms.common.services.ProfitCenterService;

import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.security.test.context.support.WithMockUser;

import javax.servlet.http.HttpServletResponse;
import org.springframework.mock.web.MockHttpServletResponse;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(SpringRunner.class)
@WebMvcTest(ProfitCenterController.class)
public class ProfitCenterControllerTest {

	@Autowired
	private WebApplicationContext context;

	MockMvc mockMvc;

	@MockBean
	private ProfitCenterService profitCenterService;

	private ProfitCenterController profitcenterController;
	
	private Integer pageSize = 5;
	private Integer page= 0;
	private String company = "JUNIT purchaseRequisition";
	private String isActive = "JUNIT isActive";
	private String department = "JUNIT department";
	private String store = "JUNIT store";
	private String code = "JUNIT";
	private String name = "JUNIT Name";

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
		ProfitCenter profitcenter = new ProfitCenter();
		profitcenter.setCode(code);
		profitcenter.setName(name);
		profitcenter.setCompany(new Company());
		List<ProfitCenter> profitcenterList = Arrays.asList(profitcenter);
		when(profitCenterService.getAllProfitCenter()).thenReturn(profitcenterList);
		profitcenterController = new ProfitCenterController();
	}

	/**
	 * Test on page load PC.
	 */
	@Test
	@WithMockUser(username = "superadmin")
	public void testOnPageLoadPC() {
		try {
			this.mockMvc.perform(get("/profitcenter")).andExpect(status().isOk())
					.andExpect(content().string(containsString(name)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Test create PC.
	 */
	@Test(expected = POException.class)
	public void testCreatePC() {
		try {
			mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
			ProfitCenter profitcenter = new ProfitCenter();
			profitcenter.setCode(code);
			profitcenter.setName(name);
			profitcenter.setCompany(new Company());			
			// profitcenterController.createPC(profitcenter);
			HttpServletResponse response = new MockHttpServletResponse();
			profitcenterController.onPageLoadPC(null);
			assertEquals("profitcenter", response.getContentType());
		} catch (Exception ex) {
			assert ((ex instanceof NullPointerException) || (ex instanceof SQLException)
					|| (ex instanceof NotFoundException));
		}
	}
}