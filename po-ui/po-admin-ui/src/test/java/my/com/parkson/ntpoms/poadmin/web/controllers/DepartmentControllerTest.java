/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.poadmin.web.controllers;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javassist.NotFoundException;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.entities.Department;
import my.com.parkson.ntpoms.common.services.DepartmentService;

@RunWith(SpringRunner.class)
@WebMvcTest(DepartmentController.class)
public class DepartmentControllerTest {

	@Autowired
	private WebApplicationContext context;

	MockMvc mockMvc;

	@MockBean
	private DepartmentService departmentService;
	
	private DepartmentController departmentController;

	private Integer pageSize = 5;
	private Integer page= 0;
	private String isActive = "JUNIT isActive";
	private String name = "JUNIT Name";
	private String code = "JUNIT";

	private String nameUpdate = "JUNIT Name Update";
	private String codeUpdate = "JUNIT Code Update";

	private String nameEdit = "JUNIT Name Edit";
	private String codeEdit = "JUNIT Code Edit";

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
		Department department = new Department();
		department.setCode(code);
		department.setName(name);
		List<Department> departmentList = Arrays.asList(department);
		when(departmentService.getAllDepartment()).thenReturn(departmentList);
		departmentController = new DepartmentController();
	}

	/**
	 * Test on page load dept.
	 */
	@Test
	@WithMockUser(username = "superadmin")
	public void testOnPageLoadDept() {
		try {
			this.mockMvc.perform(get("/department")).andExpect(status().isOk())
					.andExpect(content().string(containsString(name)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Test create department.
	 */
	@Test(expected = POException.class)
	public void testCreateDepartment() {
		try {
			mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
			Department Department = new Department();
			Department.setCode(code);
			Department.setName(name);
			// departmentController.createDepartment(Department);
			HttpServletResponse response = new MockHttpServletResponse();
			departmentController.onPageLoadDept();
			assertEquals("department", response.getContentType());
		} catch (Exception ex) {
			assert ((ex instanceof NullPointerException) || (ex instanceof SQLException)
					|| (ex instanceof NotFoundException));
		}
	}

	/**
	 * Test update deparment.
	 */
	@Test(expected = POException.class)
	public void testUpdateDeparment() {
		try {
			mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
			Department Department = new Department();
			Department.setCode(codeUpdate);
			Department.setName(nameUpdate);
			// departmentController.updateDepartment(Department);
			HttpServletResponse response = new MockHttpServletResponse();
			departmentController.onPageLoadDept();
			assertEquals("department", response.getContentType());
		} catch (Exception ex) {
			assert ((ex instanceof NullPointerException) || (ex instanceof SQLException)
					|| (ex instanceof NotFoundException));
		}
	}

	/**
	 * Test edit deparment.
	 */
	@Test(expected = POException.class)
	public void testEditDeparment() {
		try {
			mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
			Department Department = new Department();
			Department.setCode(codeEdit);
			Department.setName(nameEdit);
			// departmentController.editDepartment(Department);
			HttpServletResponse response = new MockHttpServletResponse();
			departmentController.onPageLoadDept();
			assertEquals("department", response.getContentType());
		} catch (Exception ex) {
			assert ((ex instanceof NullPointerException) || (ex instanceof SQLException)
					|| (ex instanceof NotFoundException));
		}
	}
}