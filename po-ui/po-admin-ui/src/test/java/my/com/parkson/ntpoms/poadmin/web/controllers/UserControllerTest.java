/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.poadmin.web.controllers;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import my.com.parkson.ntpoms.common.entities.User;
import my.com.parkson.ntpoms.common.services.UsersService;

@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
public class UserControllerTest {

	@Autowired
	private WebApplicationContext context;

	MockMvc mockMvc;

    @MockBean
    private UsersService usersService;

    private UserController userController;
    
    private String username = "JUNIT";
	private String password = "JUNIT Passsword";
	private boolean isActive = true;

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
		User user = new User();
		user.setUsername(username);
		user.setPassword(password);
		user.setActive(isActive);
		List<User> userList = Arrays.asList(user);
		when(usersService.getAllActiveUsers()).thenReturn(userList);
		userController = new UserController();
	}

	/**
	 * Test on page load.
	 */
	@Test
	public void testOnPageLoad() {
		fail("Not yet implemented");
	}

	/**
	 * Test update master.
	 */
	@Test
	public void testUpdateMaster() {
		fail("Not yet implemented");
	}

	/**
	 * Test edit master string model.
	 */
	@Test
	public void testEditMasterStringModel() {
		fail("Not yet implemented");
	}

	/**
	 * Test edit master admin master form binding result model redirect attributes.
	 */
	@Test
	public void testEditMasterAdminMasterFormBindingResultModelRedirectAttributes() {
		fail("Not yet implemented");
	}

	/**
	 * Test on page load PO user.
	 */
	@Test
	public void testOnPageLoadPOUser() {
		fail("Not yet implemented");
	}

	/**
	 * Test create PO user.
	 */
	@Test
	public void testCreatePOUser() {
		fail("Not yet implemented");
	}

	/**
	 * Test edit PO user.
	 */
	@Test
	public void testEditPOUser() {
		fail("Not yet implemented");
	}

	/**
	 * Test update PO user.
	 */
	@Test
	public void testUpdatePOUser() {
		fail("Not yet implemented");
	}
}