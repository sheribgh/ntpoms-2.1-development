/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.poadmin.web.controllers;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javassist.NotFoundException;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.entities.GoodsServicesTax;
import my.com.parkson.ntpoms.common.services.GoodsServicesTaxService;

@RunWith(SpringRunner.class)
@WebMvcTest(GSTController.class)
public class GSTControllerTest {

	@Autowired
	private WebApplicationContext context;

	MockMvc mockMvc;

	@MockBean
	private GoodsServicesTaxService goodsServicesTaxService;

	private GSTController gstController;
	
	private Integer pageSize = 5;
	private Integer page= 0;
	private String abbrDesc = "JUNIT abbrDesc";
	private String isActive = "JUNIT isActive";

	private String code = "JUNIT";
	private String desc = "JUNIT Description";
	private Double rate = 2.5;

	private String codeUpdate = "JUNIT Code Update";
	private String descUpdate = "JUNIT Description Update";
	private Double rateUpdate = 2.7;

	private String codeEdit = "JUNIT Code Edit";
	private String descEdit = "JUNIT Description Edit";
	private Double rateEdit = 2.7;

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
		GoodsServicesTax GST = new GoodsServicesTax();
		GST.setCode(code);
		GST.setDesc(desc);
		GST.setRate(rate);
		List<GoodsServicesTax> GSTList = Arrays.asList(GST);
		when(goodsServicesTaxService.getAllGoodsServicesTax()).thenReturn(GSTList);
		gstController = new GSTController();
	}

	/**
	 * Test on page load GST.
	 */
	@Test
	@WithMockUser(username = "superadmin")
	public void testOnPageLoadGST() {
		try {
			this.mockMvc.perform(get("/gst")).andExpect(status().isOk())
					.andExpect(content().string(containsString(code)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Test create GST.
	 */
	@Test(expected = POException.class)
	public void testCreateGST() {
		try {
			mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
			GoodsServicesTax GST = new GoodsServicesTax();
			GST.setCode(code);
			GST.setDesc(desc);
			GST.setRate(rate);			
			// gstController.createGST(GST);
			HttpServletResponse response = new MockHttpServletResponse();
			//gstController.onPageLoadGST(Optional.of(pageSize), Optional.of(page), Optional.of(code), Optional.of(abbrDesc), Optional.of(isActive));
			assertEquals("GST", response.getContentType());
		} catch (Exception ex) {
			assert ((ex instanceof NullPointerException) || (ex instanceof SQLException)
					|| (ex instanceof NotFoundException));
		}
	}

	/**
	 * Test update GST.
	 */
	@Test(expected = POException.class)
	public void testUpdateGST() {
		try {
			mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
			GoodsServicesTax GST = goodsServicesTaxService.getGoodsServicesTaxByCode(code);
			GST.setCode(codeUpdate);
			GST.setDesc(descUpdate);
			GST.setRate(rateUpdate);			
			// gstController.updateGST(GST);
			HttpServletResponse response = new MockHttpServletResponse();
			//gstController.onPageLoadGST(Optional.of(pageSize), Optional.of(page), Optional.of(code), Optional.of(abbrDesc), Optional.of(isActive));
			assertEquals("GST", response.getContentType());
		} catch (Exception ex) {
			assert ((ex instanceof NullPointerException) || (ex instanceof SQLException)
					|| (ex instanceof NotFoundException));
		}
	}

	/**
	 * Test edit GST.
	 */
	@Test(expected = POException.class)
	public void testEditGST() {
		try {
			mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
			GoodsServicesTax GST = goodsServicesTaxService.getGoodsServicesTaxByCode(code);
			GST.setCode(codeEdit);
			GST.setDesc(descEdit);
			GST.setRate(rateEdit);			
			// gstController.editGST(GST);
			HttpServletResponse response = new MockHttpServletResponse();
			//gstController.onPageLoadGST(Optional.of(pageSize), Optional.of(page), Optional.of(code), Optional.of(abbrDesc), Optional.of(isActive));
			assertEquals("GST", response.getContentType());
		} catch (Exception ex) {
			assert ((ex instanceof NullPointerException) || (ex instanceof SQLException)
					|| (ex instanceof NotFoundException));
		}
	}
}