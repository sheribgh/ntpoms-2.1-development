/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.poadmin.web.controllers;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javassist.NotFoundException;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.entities.OrderCategory;
import my.com.parkson.ntpoms.common.entities.OrderSubCategory;
import my.com.parkson.ntpoms.common.services.OrderCategoryService;
import my.com.parkson.ntpoms.common.services.OrderSubCategoryService;

@RunWith(SpringRunner.class)
@WebMvcTest(OrderController.class)
public class OrderCategoryControllerTest {

	@Autowired
	private WebApplicationContext context;

	MockMvc mockMvc;

    @MockBean
    private OrderCategoryService orderCategoryService;

    @MockBean
    private OrderSubCategoryService orderSubCategoryService;


    private OrderController orderController;
	
	private Integer pageSize = 5;
	private Integer page= 0;
	private String code = "JUNIT abbrDesc";
	private String purchaseRequisition = "JUNIT purchaseRequisition";
	private String abbrCode = "JUNIT abbrCode";
	private String isActive = "JUNIT isActive";
	
	private String name = "JUNIT Name";	
    private String ordercode = "JUNIT Order Code";
	private String orderdesc = "JUNIT Order Description";

	private String ordercodeUpdate = "JUNIT Order Code Update";
	private String orderdescEdit = "JUNIT Order Description Edit";

	private String ordercodeEdit = "JUNIT Order Code Edit";
	private String orderdescUpdate = "JUNIT Order Description Update";

	private String ordersubcode = "JUNIT Order Sub Code";
	private String ordersubdesc = "JUNIT Order Sub Description";

	private String ordersubcodeUpdate = "JUNIT Order Sub Code Update";
	private String ordersubdescUpdate = "JUNIT Order Sub Description Update";

	private String ordersubcodeEdit = "JUNIT Order Sub Code Edit";
	private String ordersubdescEdit = "JUNIT Order Sub Description Edit";


	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
		OrderCategory orderCategoryForm = new OrderCategory();
		orderCategoryForm.setCode(ordercode);
		orderCategoryForm.setDesc(orderdesc);
		List<OrderCategory> ordercatList = Arrays.asList(orderCategoryForm);
		when(orderCategoryService.getAllOrderCategory()).thenReturn(ordercatList);

		OrderSubCategory ordersubCategoryForm = new OrderSubCategory();
		ordersubCategoryForm.setCode(ordersubcode);
		ordersubCategoryForm.setDesc(ordersubdesc);
		List<OrderSubCategory> ordersubcatList = Arrays.asList(ordersubCategoryForm);
		when(orderSubCategoryService.getAllOrderSubCategory()).thenReturn(ordersubcatList);

		orderController = new OrderController();
	}

	/**
	 * Test on page load order.
	 */
	@Test
	@WithMockUser(username = "superadmin")
	public void testOnPageLoadOrder() {
		try {
			this.mockMvc.perform(get("/ordercategory")).andExpect(status().isOk())
					.andExpect(content().string(containsString(name)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Test create order category.
	 */
	@Test(expected=POException.class)
	public void testCreateOrderCategory() {
		try{
			mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
			OrderCategory orderCategoryForm = new OrderCategory();
			orderCategoryForm.setCode(ordercode);
			orderCategoryForm.setDesc(orderdesc);
			
			//orderController.createOrderCategory(orderCategoryForm);
			HttpServletResponse response = new MockHttpServletResponse();
			orderController.onPageLoadOrder();
			assertEquals("ordercategory", response.getContentType());
		}catch(Exception ex){
			assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
		}
	}

	/**
	 * Test update order category.
	 */
	@Test(expected=POException.class)
	public void testUpdateOrderCategory() {
		try{
			mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
			OrderCategory orderCategoryForm = orderCategoryService.getOrderCategoryByCode(ordercode);
			orderCategoryForm.setCode(ordercodeUpdate);
			orderCategoryForm.setDesc(orderdescUpdate);
			
			//orderController.updateOrderCategory(orderCategoryForm);
			HttpServletResponse response = new MockHttpServletResponse();
			orderController.onPageLoadOrder();
			assertEquals("ordercategory", response.getContentType());
		}catch(Exception ex){
			assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
		}
	}

	/**
	 * Test edit order category.
	 */
	@Test(expected=POException.class)
	public void testEditOrderCategory() {
		try{
			mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
			OrderCategory orderCategoryForm = orderCategoryService.getOrderCategoryByCode(ordercode);
			orderCategoryForm.setCode(ordercodeEdit);
			orderCategoryForm.setDesc(orderdescEdit);
			
			//orderController.editOrderCategory(orderCategoryForm);
			HttpServletResponse response = new MockHttpServletResponse();
			orderController.onPageLoadOrder();
			assertEquals("ordercategory", response.getContentType());
		}catch(Exception ex){
			assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
		}
	}

	/**
	 * Test on page load sub order.
	 */
	@Test
	@WithMockUser(username = "superadmin")
	public void testOnPageLoadSubOrder() {
		try {
			this.mockMvc.perform(get("/ordersubcategory")).andExpect(status().isOk())
					.andExpect(content().string(containsString(name)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Test create order sub category.
	 */
	@Test(expected=POException.class)
	public void testCreateOrderSubCategory() {
		try{
			mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
			OrderSubCategory ordersubCategoryForm = new OrderSubCategory();
			ordersubCategoryForm.setCode(ordersubcode);
			ordersubCategoryForm.setDesc(ordersubdesc);
			
			//orderController.createOrderSubCategory(ordersubCategoryForm);
			HttpServletResponse response = new MockHttpServletResponse();
			orderController.onPageLoadSubOrder();
			assertEquals("ordersubcategory", response.getContentType());
		}catch(Exception ex){
			assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
		}
	}

	/**
	 * Test update order sub category.
	 */
	@Test(expected=POException.class)
	public void testUpdateOrderSubCategory() {
		try{
			mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
			OrderSubCategory ordersubCategoryForm = orderSubCategoryService.getOrderSubCategoryByCode(ordersubcodeUpdate);
			ordersubCategoryForm.setCode(ordersubcodeUpdate);
			ordersubCategoryForm.setDesc(ordersubdescUpdate);
			
			//orderController.updateOrderSubCategory(ordersubCategoryForm);
			HttpServletResponse response = new MockHttpServletResponse();
			orderController.onPageLoadSubOrder();
			assertEquals("ordersubcategory", response.getContentType());
		}catch(Exception ex){
			assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
		}
	}

	/**
	 * Test edit ordersub category.
	 */
	@Test(expected=POException.class)
	public void testEditOrdersubCategory() {
		try{
			mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
			OrderSubCategory ordersubCategoryForm = orderSubCategoryService.getOrderSubCategoryByCode(ordersubcode);
			ordersubCategoryForm.setCode(ordersubcodeEdit);
			ordersubCategoryForm.setDesc(ordersubdescEdit);
			
			//orderController.updateOrderSubCategory(ordersubCategoryForm);
			HttpServletResponse response = new MockHttpServletResponse();
			orderController.onPageLoadSubOrder();
			assertEquals("ordersubcategory", response.getContentType());
		}catch(Exception ex){
			assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
		}
	}
}