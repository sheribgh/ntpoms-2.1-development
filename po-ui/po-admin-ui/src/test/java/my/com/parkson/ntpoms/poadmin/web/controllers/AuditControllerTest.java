/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.poadmin.web.controllers;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import my.com.parkson.ntpoms.common.entities.Audit;
import my.com.parkson.ntpoms.common.services.AuditService;
import my.com.parkson.ntpoms.poadmin.web.utils.HeaderTitleConstants;

@RunWith(SpringRunner.class)
@WebMvcTest(AuditController.class)
public class AuditControllerTest {

	@Autowired
	private WebApplicationContext context;

	MockMvc mockMvc;

	@MockBean
	private AuditService auditService;

	private AuditController auditController;

	private String username = "JUNIT_USERNAME";
	private String message = "JUNIT Message";

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
		Audit audit = new Audit();
		audit.setUsername(username);
		audit.setMessage(message);
		List<Audit> auditList = Arrays.asList(audit);
		when(auditService.getAllAudit()).thenReturn(auditList);
		auditController = new AuditController();
	}

	/**
	 * Test get header title.
	 */
	@Test
	public void testGetHeaderTitle() {
		String headerTitle = auditController.getHeaderTitle();
		assertNotNull(headerTitle);
		assertEquals(HeaderTitleConstants.PERMISSIONTITLE, headerTitle);
	}

	/**
	 * Test show persons page.
	 */
	@Test
	@WithMockUser(username = "superadmin")
	public void testShowPersonsPage() {
		try {
			this.mockMvc.perform(get("/audittraillist")).andExpect(status().isOk())
					.andExpect(content().string(containsString(username)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Test logs page.
	 */
	@Test
	@WithMockUser(username = "superadmin")
	public void testLogsPage() {
		try {
			this.mockMvc.perform(get("/logs")).andExpect(status().isOk())
					.andExpect(content().string(containsString(username)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Test update setting.
	 */
	@Test
	public void testUpdateSetting() {
		fail("Not yet implemented");
	}
}