/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.poadmin.web.controllers;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import my.com.parkson.ntpoms.common.entities.Permission;
import my.com.parkson.ntpoms.common.services.SecurityService;
import my.com.parkson.ntpoms.poadmin.web.utils.HeaderTitleConstants;

/**
 * The Class PermissionControllerTest.
 */
@RunWith(SpringRunner.class)
@WebMvcTest(PermissionController.class)
public class PermissionControllerTest {

	@Autowired
	private WebApplicationContext context;

	MockMvc mockMvc;

	@MockBean
	private SecurityService securityService;

	private PermissionController permissionController;

	private String name = "JUNIT Name";
	private String description = "JUNIT Description";

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
		Permission permission = new Permission();
		permission.setName(name);
		permission.setDescription(description);
		List<Permission> permissionList = Arrays.asList(permission);
		when(securityService.getAllPermissions()).thenReturn(permissionList);
		permissionController = new PermissionController();
	}

	/**
	 * Test get header title.
	 */
	@Test
	public void testGetHeaderTitle() {
		String headerTitle = permissionController.getHeaderTitle();
		assertNotNull(headerTitle);
		assertEquals(HeaderTitleConstants.PERMISSIONTITLE, headerTitle);
	}

	/**
	 * Test is logged in.
	 */
	@Test
	@WithMockUser(username = "admin", roles = { "MANAGE_PERMISSIONS" })
	public void testIsLoggedIn() {
		assertFalse(AbstractAdminController.isLoggedIn());
	}

	/**
	 * Test list permissions.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@WithMockUser(username = "admin", roles = { "USER", "MANAGE_PERMISSIONS" })
	public void testListPermissions() throws Exception {
		this.mockMvc.perform(get("/permissions")).andExpect(status().isOk())
				.andExpect(content().string(containsString(name)));
	}

	/**
	 * Test list permissions with anonymous user.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@WithAnonymousUser
	public void testListPermissionsWithAnonymousUser() throws Exception {
		this.mockMvc.perform(get("/permissions")).andDo(print()).andExpect(status().is4xxClientError());
	}
}