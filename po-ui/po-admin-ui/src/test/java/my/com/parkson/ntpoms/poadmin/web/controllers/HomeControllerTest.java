/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.poadmin.web.controllers;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@WebMvcTest(HomeController.class)
public class HomeControllerTest {

	@Autowired
	private WebApplicationContext context;

	MockMvc mockMvc;

	private HomeController homeController;

	private String name = "JUNIT Name";

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
		homeController = new HomeController();
	}

	/**
	 * Test home 1.
	 */
	@Test
	@WithMockUser(username = "superadmin")
	public void testHome1() {
		try {
			this.mockMvc.perform(get("/")).andExpect(status().isOk()).andExpect(content().string(containsString(name)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Test home.
	 */
	@Test
	@WithMockUser(username = "superadmin")
	public void testHome() {
		try {
			this.mockMvc.perform(get("/home")).andExpect(status().isOk())
					.andExpect(content().string(containsString(name)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Test forgot pwd.
	 */
	@Test
	@WithMockUser(username = "superadmin")
	public void testForgotPwd() {
		try {
			this.mockMvc.perform(get("/public/forgotPwd")).andExpect(status().isOk())
					.andExpect(content().string(containsString(name)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}