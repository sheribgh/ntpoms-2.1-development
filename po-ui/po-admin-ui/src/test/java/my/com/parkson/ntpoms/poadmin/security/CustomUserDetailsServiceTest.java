/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.poadmin.security;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.junit4.SpringRunner;

import my.com.parkson.ntpoms.common.entities.Permission;
import my.com.parkson.ntpoms.common.entities.Role;
import my.com.parkson.ntpoms.common.entities.User;
import my.com.parkson.ntpoms.common.services.SecurityService;

/**
 * The Class CustomUserDetailsServiceTest.
 */
@RunWith(SpringRunner.class)
public class CustomUserDetailsServiceTest {

	@Rule
	public final ExpectedException exception = ExpectedException.none();

	@MockBean
	private SecurityService securityService;

	private CustomUserDetailsService customUserDetailsService;

	private String username = "JUNIT_USERNAME";

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		User user = new User();
		user.setUsername(username);
		user.setPassword("passwrd");
		Role role = new Role();
		role.setName("JUNIT_ROLE");
		Permission permission = new Permission();
		permission.setName("JUNIT_PERMISSION");
		List<Permission> permissions = Collections.singletonList(permission);
		role.setPermissions(permissions);
		List<Role> roles = Collections.singletonList(role);
		user.setRoles(roles);
		when(securityService.findUserByUsername(username)).thenReturn(user);
		customUserDetailsService = new CustomUserDetailsService();
	}

	/**
	 * Test load user by email.
	 */
	@Test(expected = UsernameNotFoundException.class)
	public void testLoadUserByEmail() {
		try {
			when(securityService.findUserByUsername(username)).thenReturn(null);
			UserDetails userDetails = customUserDetailsService.loadUserByUsername(username);
			exception.expectMessage("Email JUNIT_EMAIL not found");
			assertNull(userDetails);
		} catch (Exception ex) {
	        assert((ex instanceof NullPointerException) || (ex instanceof UsernameNotFoundException));
	    }
	}

	/**
	 * Test load user by username.
	 */
	@Test(expected = NullPointerException.class)
	public void testLoadUserByUsername() {
		UserDetails userDetails = customUserDetailsService.loadUserByUsername(username);
		assertEquals(username, userDetails.getUsername());
		assertThat(userDetails.getPassword()).isEqualTo("passwrd");
	}

}
