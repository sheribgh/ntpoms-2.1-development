/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.poadmin.web.controllers;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javassist.NotFoundException;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.entities.EmployeeMaster;
import my.com.parkson.ntpoms.common.entities.Job;
import my.com.parkson.ntpoms.common.services.EmployeeMasterService;
import my.com.parkson.ntpoms.common.services.JobService;

@RunWith(SpringRunner.class)
@WebMvcTest(EmployeeManagementController.class)
public class EmployeeManagementControllerTest {

	@Autowired
	private WebApplicationContext context;

	MockMvc mockMvc;

	@MockBean
	private JobService jobService;

	@MockBean
	private EmployeeMasterService employeeMasterService;

	private EmployeeManagementController employeeManagementController;

	private Integer pageSize = 5;
	private Integer page= 0;
	private String code = "JUNIT code";
	private String abbrName = "JUNIT abbrName";
	private String isActive = "JUNIT isActive";
	private String name = "JUNIT Name";
	private String newIcNo = "JK01111111";
	private String gender = "Male";
	private String psId = "PO011111";

	private String jobcode = "JUNIT JOB Code";
	private String jobname = "JUNIT JOB Name";

	private String jobcodeedit = "JUNIT JOB Code Edit";
	private String jobnameupdate = "JUNIT JOB Name Update";

	private String jobcodeupdate = "JUNIT JOB Code Update";
	private String jobnameedit = "JUNIT JOB Name Edit";

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
		EmployeeMaster employeeMaster = new EmployeeMaster();
		employeeMaster.setName(name);
		employeeMaster.setNewIcNo(newIcNo);
		employeeMaster.setGender(gender);
		List<EmployeeMaster> employeeList = Arrays.asList(employeeMaster);
		when(employeeMasterService.getAllActiveEmployeeMaster()).thenReturn(employeeList);
		Job jobMaster = new Job();
		jobMaster.setCode(jobcode);
		jobMaster.setName(jobname);
		List<Job> jobList = Arrays.asList(jobMaster);
		when(jobService.getAllJob()).thenReturn(jobList);
		employeeManagementController = new EmployeeManagementController();
	}

	/**
	 * Test emplist page.
	 */
	@Test
	@WithMockUser(username = "superadmin")
	public void testEmplistPage() {
		try {
			this.mockMvc.perform(get("/emplist")).andExpect(status().isOk())
					.andExpect(content().string(containsString(name)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Test view employee.
	 */
	@Test
	@WithMockUser(username = "superadmin")
	public void testViewEmployee() {
		EmployeeMaster employeeMaster = employeeMasterService.getEmployeeMasterByPsId(psId);
		try {
			this.mockMvc.perform(get("/viewemployee")).andExpect(status().isOk())
					.andExpect(content().string(containsString(name)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Test jobmaster.
	 */
	@Test
	@WithMockUser(username = "superadmin")
	public void testJobmaster() {
		try {
			this.mockMvc.perform(get("/viewemployee")).andExpect(status().isOk())
					.andExpect(content().string(containsString(name)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Test update master.
	 */
	@Test(expected = POException.class)
	public void testUpdateMaster() {
		try {
			mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
			Job jobMaster = new Job();
			jobMaster.setCode(jobcode);
			jobMaster.setName(jobname);			
			// employeeManagementController.updateMaster(jobMaster);
			HttpServletResponse response = new MockHttpServletResponse();
			employeeManagementController.jobmaster();
			assertEquals("job", response.getContentType());
		} catch (Exception ex) {
			assert ((ex instanceof NullPointerException) || (ex instanceof SQLException)
					|| (ex instanceof NotFoundException));
		}
	}

	/**
	 * Test edit job master.
	 */
	@Test(expected = POException.class)
	public void testEditJobMaster() {
		try {
			mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
			Job jobMaster = jobService.getJobByCode(jobcode);
			jobMaster.setCode(jobcodeedit);
			jobMaster.setName(jobnameedit);			
			// employeeManagementController.editJobMaster(jobMaster);
			HttpServletResponse response = new MockHttpServletResponse();
			employeeManagementController.jobmaster();
			assertEquals("job", response.getContentType());
		} catch (Exception ex) {
			assert ((ex instanceof NullPointerException) || (ex instanceof SQLException)
					|| (ex instanceof NotFoundException));
		}
	}

	/**
	 * Test edit master.
	 */
	@Test(expected = POException.class)
	public void testEditMaster() {
		try {
			mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
			Job jobMaster = jobService.getJobByCode(jobcode);
			jobMaster.setCode(jobcodeupdate);
			jobMaster.setName(jobnameupdate);			
			// employeeManagementController.editMaster(jobMaster);
			HttpServletResponse response = new MockHttpServletResponse();
			employeeManagementController.jobmaster();
			assertEquals("job", response.getContentType());
		} catch (Exception ex) {
			assert ((ex instanceof NullPointerException) || (ex instanceof SQLException)
					|| (ex instanceof NotFoundException));
		}
	}
}