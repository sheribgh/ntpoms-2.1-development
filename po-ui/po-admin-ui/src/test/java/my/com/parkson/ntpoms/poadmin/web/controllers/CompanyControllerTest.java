/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.poadmin.web.controllers;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javassist.NotFoundException;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.entities.Company;
import my.com.parkson.ntpoms.common.services.CompanyService;

@RunWith(SpringRunner.class)
@WebMvcTest(CompanyController.class)
public class CompanyControllerTest {

	@Autowired
	private WebApplicationContext context;

	MockMvc mockMvc;

	@MockBean
	private CompanyService companyService;

	private CompanyController companyController;

	private Integer pageSize = 5;
	private Integer page= 0;
	private String isActive = "JUNIT isActive";

	private String code = "JUNIT";
	private String codeHRIS = "JUNIT HRIS";
	private String name = "JUNIT Name";

	private String codeHRISEdit = "JUNIT HRIS Edit";
	private String codeHRISUpdate = "JUNIT HRIS Update";

	private String nameEdit = "JUNIT Name Edit";
	private String nameUpdate = "JUNIT Name Update";

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
		Company company = new Company();
		company.setCode(code);
		company.setCodeHRIS(codeHRIS);
		company.setName(name);
		List<Company> companyList = Arrays.asList(company);
		when(companyService.getAllCompany()).thenReturn(companyList);
		companyController = new CompanyController();
	}

	/**
	 * Test on page load company.
	 */
	@Test
	@WithMockUser(username = "superadmin")
	public void testOnPageLoadCompany() {
		try {
			this.mockMvc.perform(get("/company")).andExpect(status().isOk())
					.andExpect(content().string(containsString(name)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Test create company.
	 */
	@Test(expected=POException.class)
	public void testCreateCompany() {
		try{
			mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
			Company companyForm = new Company();
			companyForm.setCode(code);
			companyForm.setCodeHRIS(codeHRIS);
			companyForm.setName(name);
			companyService.createCompany(companyForm);
			HttpServletResponse response = new MockHttpServletResponse();
			companyController.onPageLoadCompany();
			assertEquals("company", response.getContentType());
		} catch(Exception ex){
			assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
		}									
	}

	/**
	 * Test update company.
	 */
	@Test(expected = POException.class)
	public void testUpdateCompany() {
		try {
			mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
			Company company = companyService.getCompanyByCode(code);
			company.setCodeHRIS(codeHRISUpdate);
			company.setName(nameUpdate);
			//companyController.updateCompany(company);
			HttpServletResponse response = new MockHttpServletResponse();
			companyController.onPageLoadCompany();
			assertEquals("company", response.getContentType());
		} catch (Exception ex) {
			assert ((ex instanceof NullPointerException) || (ex instanceof SQLException)
					|| (ex instanceof NotFoundException));
		}
	}

	/**
	 * Test edit company.
	 */
	@Test(expected = POException.class)
	public void testEditCompany() {
		try {
			mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
			Company company = companyService.getCompanyByCode(code);
			company.setCodeHRIS(codeHRISEdit);
			company.setName(nameEdit);
			//companyController.editCompany(company);
			HttpServletResponse response = new MockHttpServletResponse();
			companyController.onPageLoadCompany();
			assertEquals("company", response.getContentType());
		} catch (Exception ex) {
			assert ((ex instanceof NullPointerException) || (ex instanceof SQLException)
					|| (ex instanceof NotFoundException));
		}
	}
}