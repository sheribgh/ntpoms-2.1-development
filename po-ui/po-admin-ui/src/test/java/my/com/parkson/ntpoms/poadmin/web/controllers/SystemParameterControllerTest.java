/*
 * ********************************************************************************
 * Copyright (c) 2018 - Siroi Solutions, all rights reserved - Developed By SUBHASH.
 *
 * A library "po-core" is used for purchase order core functionalities & utilities.
 *
 * @Author     : SUBHASH CHANDRA SAGAR
 * @Dept       : CSS
 * @Desc       : Customer System Solution
 * @File       : po-core-1.0-SNAPSHOT.jar
 * @Version    : 1.0-SNAPSHOT
 * @Url        : www.siroi-solutions.com.my
 * @Copyright  : Copyright 2018 Siroi Solutions, all rights reserved.
 *
 * This Library Source Code Form is subject to the terms of the Public License,
 *  v. 1.0-SNAPSHOT, without the "Incompatible With Secondary Licenses" notice.
 * ********************************************************************************
 */
package my.com.parkson.ntpoms.poadmin.web.controllers;

import static org.junit.Assert.*;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.WebApplicationContext;
import javassist.NotFoundException;
import my.com.parkson.ntpoms.POException;
import my.com.parkson.ntpoms.common.entities.MasterData;
import my.com.parkson.ntpoms.common.entities.SystemParameter;
import my.com.parkson.ntpoms.common.services.MasterDataService;
import my.com.parkson.ntpoms.common.services.SystemParameterService;

import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.security.test.context.support.WithMockUser;

import javax.servlet.http.HttpServletResponse;
import org.springframework.mock.web.MockHttpServletResponse;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(SpringRunner.class)
@WebMvcTest(SystemParameterController.class)
public class SystemParameterControllerTest {

	@Autowired
	private WebApplicationContext context;

	MockMvc mockMvc;

    @MockBean
    private MasterDataService masterDataService;

    @MockBean
    private SystemParameterService systemParameterService;

    private SystemParameterController systemParameterController;
	
	private Integer pageSize = 5;
	private Integer page= 0;
	private String isActive = "JUNIT isActive";

    private String name = "JUNIT Name";    
    private String propName = "JUNIT propName";
	private String propValue = "JUNIT propValue";

	private String codeDesc = "JUNIT Desc";
	private String codeType = "JUNIT Type";
	private String codeValue = "JUNIT Value";

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
		SystemParameter sysparam = new SystemParameter();
		sysparam.setPropName(propName);
		sysparam.setPropValue(propValue);
		List<SystemParameter> sysparamList = Arrays.asList(sysparam);
		when(systemParameterService.getAllSystemParameter()).thenReturn(sysparamList);
		MasterData mastdata = new MasterData();
		mastdata.setCodeType(codeType);
		mastdata.setCodeValue(codeValue);
		mastdata.setCodeDesc(codeDesc);
		List<MasterData> mastdataList = Arrays.asList(mastdata);
		when(masterDataService.getAllMasterData()).thenReturn(mastdataList);
		systemParameterController = new SystemParameterController();
	}

	/**
	 * Test on page load master data.
	 */
	@Test
	@WithMockUser(username = "superadmin")
	public void testOnPageLoadMasterData() {
		try {
			this.mockMvc.perform(get("/masterdata")).andExpect(status().isOk())
					.andExpect(content().string(containsString(name)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Test create master data parameter.
	 */
	@Test(expected=POException.class)
	public void testCreateMasterDataParameter() {
		try {
			mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
			MasterData mastdata = new MasterData();
			mastdata.setCodeType(codeType);
			mastdata.setCodeValue(codeValue);
			mastdata.setCodeDesc(codeDesc);			
			//systemParameterController.createMasterDataParameter(mastdata);
			HttpServletResponse response = new MockHttpServletResponse();
			systemParameterController.onPageLoadMasterData();
			assertEquals("masterdata", response.getContentType());
		} catch(Exception ex){
			assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
		}
	}

	/**
	 * Test edit master data parameter.
	 */
	@Test(expected=POException.class)
	public void testEditMasterDataParameter() {
		try {
			mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
			MasterData mastdata = masterDataService.getMasterDataById(1);
			mastdata.setCodeType(codeType);
			mastdata.setCodeValue(codeValue);
			mastdata.setCodeDesc(codeDesc);			
			//systemParameterController.editMasterDataParameter(mastdata);
			HttpServletResponse response = new MockHttpServletResponse();
			systemParameterController.onPageLoadMasterData();
			assertEquals("masterdata", response.getContentType());
		} catch(Exception ex){
			assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
		}
	}

	/**
	 * Test update master data parameter.
	 */
	@Test(expected=POException.class)
	public void testUpdateMasterDataParameter() {
		try {
			mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
			MasterData mastdata = masterDataService.getMasterDataById(1);
			mastdata.setCodeType(codeType);
			mastdata.setCodeValue(codeValue);
			mastdata.setCodeDesc(codeDesc);			
			//systemParameterController.updateMasterDataParameter(mastdata);
			HttpServletResponse response = new MockHttpServletResponse();
			systemParameterController.onPageLoadMasterData();
			assertEquals("masterdata", response.getContentType());
		} catch(Exception ex){
			assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
		}
	}

	/**
	 * Test on page load system param.
	 */
	@Test
	@WithMockUser(username = "superadmin")
	public void testOnPageLoadSystemParam() {
		try {
			this.mockMvc.perform(get("/parameter")).andExpect(status().isOk())
					.andExpect(content().string(containsString(name)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Test create parameter.
	 */
	@Test(expected=POException.class)
	public void testCreateParameter() {
		try {
			mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
			SystemParameter systemParam = new SystemParameter();
			systemParam.setPropName(propName);
			systemParam.setPropValue(propValue);			
			//systemParameterController.createParameter(systemParam);
			HttpServletResponse response = new MockHttpServletResponse();
			systemParameterController.onPageLoadMasterData();
			assertEquals("masterdata", response.getContentType());
		} catch(Exception ex){
			assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
		}
	}

	/**
	 * Test edit master.
	 */
	@Test(expected=POException.class)
	public void testEditMaster() {
		try {
			mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
			SystemParameter systemParam = systemParameterService.getSystemParameterByCode(1);
			systemParam.setPropName(propName);
			systemParam.setPropValue(propValue);
			
			//systemParameterController.editMaster(systemParam);
			HttpServletResponse response = new MockHttpServletResponse();
			systemParameterController.onPageLoadMasterData();
			assertEquals("masterdata", response.getContentType());
		} catch(Exception ex){
			assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
		}
	}

	/**
	 * Test update parameter.
	 */
	@Test(expected=POException.class)
	public void testUpdateParameter() {
		try {
			mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
			SystemParameter systemParam = systemParameterService.getSystemParameterByCode(1);
			systemParam.setPropName(propName);
			systemParam.setPropValue(propValue);			
			//systemParameterController.updateParameter(systemParam);
			HttpServletResponse response = new MockHttpServletResponse();
			systemParameterController.onPageLoadMasterData();
			assertEquals("masterdata", response.getContentType());
		} catch(Exception ex){
			assert((ex instanceof NullPointerException) || (ex instanceof SQLException) || (ex instanceof NotFoundException));
		}
	}
}